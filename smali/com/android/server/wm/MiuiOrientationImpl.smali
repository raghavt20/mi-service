.class public Lcom/android/server/wm/MiuiOrientationImpl;
.super Ljava/lang/Object;
.source "MiuiOrientationImpl.java"

# interfaces
.implements Lcom/android/server/wm/MiuiOrientationStub;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;,
        Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;,
        Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;,
        Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;,
        Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    }
.end annotation


# static fields
.field private static final ADAPT_CUTOUT_DISABLE:I = 0x0

.field private static final ADAPT_CUTOUT_ENABLE:I = 0x1

.field private static final ADAPT_CUTOUT_UNAVAILABLE:I = -0x1

.field private static ENABLED:Z = false

.field private static final FLIP_SCREEN_ROTATION_OUTER:I = 0x4

.field private static final FOLD_DEVICE_TYPE:I = 0x2

.field private static IS_FOLD_SCREEN_DEVICE:Z = false

.field private static IS_MIUI_OPTIMIZATION:Z = false

.field private static final KEY_INDEX:I = 0x0

.field private static final KEY_SMART_ROTATION:Ljava/lang/String; = "miui_smart_rotation"

.field private static final METADATA_IGNORE_ORIENTATION_REQUEST:Ljava/lang/String; = "miui.isIgnoreOrientationRequest"

.field private static final MIUI_CTS:Ljava/lang/String; = "ro.miui.cts"

.field private static final MIUI_OPTIMIZATION:Ljava/lang/String; = "persist.sys.miui_optimization"

.field private static final MIUI_ORIENTATION_ENABLE:Ljava/lang/String; = "ro.config.miui_orientation_enable"

.field private static final MIUI_SMART_ORIENTATION_ENABLE:Ljava/lang/String; = "ro.config.miui_smart_orientation_enable"

.field private static final MUILTDISPLAY_TYPE:Ljava/lang/String; = "persist.sys.muiltdisplay_type"

.field private static final ORIENTATION_ARRAY_LENGTH:I = 0x2

.field private static final ORIENTATION_ATTR_PACKAGE_NAME:Ljava/lang/String; = "name"

.field private static final ORIENTATION_ATTR_PACKAGE_VALUE:Ljava/lang/String; = "value"

.field private static final ORIENTATION_COMMIT_TAG:Ljava/lang/String; = "miui_orientations"

.field private static final ORIENTATION_FILE_PATH:Ljava/lang/String; = "system/miui_orientation_settings.xml"

.field private static final ORIENTATION_INDEX:I = 0x1

.field private static final ORIENTATION_TAG:Ljava/lang/String; = "orientation-settings"

.field private static final ORIENTATION_TAG_PACKAGE:Ljava/lang/String; = "package"

.field private static final ORIENTATION_USER_SETTINGS_REMOVE:Ljava/lang/String; = "--remove"

.field public static final POLICY_FULLSCREEN_BY_BLOCK_LIST:I = 0x20

.field public static final POLICY_FULLSCREEN_CAMERA_ROTATE:I = 0x200

.field public static final POLICY_FULLSCREEN_CAMERA_ROTATE_ALL:I = 0x400

.field public static final POLICY_FULLSCREEN_DISPLAYCUTOUT_DISABLE:I = 0x2000

.field public static final POLICY_FULLSCREEN_DISPLAYCUTOUT_ENABLE:I = 0x1000

.field public static final POLICY_FULLSCREEN_NOT_ROTATE_APP:I = 0x40

.field public static final POLICY_FULLSCREEN_NOT_ROTATE_SENSOR:I = 0x80

.field public static final POLICY_FULLSCREEN_OUTER_BY_ALLOW_LIST:I = 0x10

.field public static final POLICY_FULLSCREEN_RESUME_CAMERA_ROTATE:I = 0x100

.field public static final POLICY_FULLSCREEN_UPDATE_CONFIG:I = 0x800

.field public static final POLICY_FULL_SCREEN_INVALID:I = -0x1

.field public static final POLICY_FULL_SCREEN_NOT_RELAUNCH:I = 0x1

.field public static final POLICY_FULL_SCREEN_RELAUNCH:I = 0x2

.field public static final POLICY_FULL_SCREEN_RELAUNCH_INTERACTIVE:I = 0x4

.field public static final POLICY_FULL_SCREEN_TOAST:I = 0x8

.field private static final SCREEN_ROTATION_INNER:I = 0x1

.field private static final SCREEN_ROTATION_OUTER:I = 0x2

.field private static final SCREEN_ROTATION_PAD:I = 0x3

.field private static SMART_ORIENTATION_ENABLE:Z = false

.field private static final TAG:Ljava/lang/String; = "MiuiOrientationImpl"


# instance fields
.field private mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

.field private mCompatLandToast:Landroid/widget/Toast;

.field private final mComponentMapBySystem:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDemoMode:Z

.field private mDemoModePolicy:I

.field private mEmbeddedFullRules:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/server/wm/MiuiOrientationImpl$FullRule;",
            ">;"
        }
    .end annotation
.end field

.field private mFile:Landroid/util/AtomicFile;

.field private mFileHandler:Landroid/os/Handler;

.field private mFullScreen3AppCameraStrategy:Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;

.field private mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

.field private mHandler:Landroid/os/Handler;

.field private mLastResumeApp:Lcom/android/server/wm/ActivityRecord;

.field private mMiuiSettingsObserver:Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;

.field private final mPackagesMapBySystem:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPackagesMapByUserSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field private mShowRotationSuggestion:I

.field private mSmartOrientationPolicy:Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;

.field private mSmartRotationEnabled:Z

.field private mToastVisible:Z

.field private mWriteSettingsRunnable:Ljava/lang/Runnable;


# direct methods
.method public static synthetic $r8$lambda$YqC3mw8dr1ljFxLHkurCcsK6mc8(Lcom/android/server/wm/MiuiOrientationImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->lambda$showToastWhenLandscapeIfNeed$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$YtcGp13sbZ3UkmSjcthGaoul8WI(Lcom/android/server/wm/MiuiOrientationImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->lambda$showToastWhenLandscapeIfNeed$1()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmComponentMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mComponentMapBySystem:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmEmbeddedFullRules(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFile(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/util/AtomicFile;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFile:Landroid/util/AtomicFile;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFileHandler(Lcom/android/server/wm/MiuiOrientationImpl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFileHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackagesMapBySystem(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mPackagesMapBySystem:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackagesMapByUserSettings(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mPackagesMapByUserSettings:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSmartRotationEnabled(Lcom/android/server/wm/MiuiOrientationImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWriteSettingsRunnable(Lcom/android/server/wm/MiuiOrientationImpl;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mWriteSettingsRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmSmartRotationEnabled(Lcom/android/server/wm/MiuiOrientationImpl;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$misDisplayFolded(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misFixedAspectRatio(Lcom/android/server/wm/MiuiOrientationImpl;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misNeedCameraRotate(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotate(Lcom/android/server/wm/ActivityRecord;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misNeedCameraRotateAll(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateAll(Lcom/android/server/wm/ActivityRecord;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misNeedCameraRotateInPad(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateInPad(Lcom/android/server/wm/ActivityRecord;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misNeedRotateWhenCameraResume(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedRotateWhenCameraResume(Lcom/android/server/wm/ActivityRecord;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misNeedRotateWhenCameraResumeInPad(Lcom/android/server/wm/MiuiOrientationImpl;Lcom/android/server/wm/ActivityRecord;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedRotateWhenCameraResumeInPad(Lcom/android/server/wm/ActivityRecord;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetENABLED()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->ENABLED:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetIS_MIUI_OPTIMIZATION()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_MIUI_OPTIMIZATION:Z

    return v0
.end method

.method static bridge synthetic -$$Nest$smwriteSettings(Landroid/util/AtomicFile;Ljava/util/Map;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->writeSettings(Landroid/util/AtomicFile;Ljava/util/Map;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 164
    nop

    .line 165
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 164
    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "persist.sys.miui_optimization"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_MIUI_OPTIMIZATION:Z

    .line 166
    const-string v0, "persist.sys.muiltdisplay_type"

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    sput-boolean v1, Lcom/android/server/wm/MiuiOrientationImpl;->IS_FOLD_SCREEN_DEVICE:Z

    .line 167
    const-string v0, "ro.config.miui_orientation_enable"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->ENABLED:Z

    .line 168
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 920
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mPackagesMapBySystem:Ljava/util/Map;

    .line 921
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mComponentMapBySystem:Ljava/util/Map;

    .line 922
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mPackagesMapByUserSettings:Ljava/util/Map;

    .line 923
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    .line 1430
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$2;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mWriteSettingsRunnable:Ljava/lang/Runnable;

    .line 171
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->ENABLED:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_MIUI_OPTIMIZATION:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z

    .line 172
    return-void
.end method

.method static fullScreenModeToString(I)Ljava/lang/String;
    .locals 2
    .param p0, "mode"    # I

    .line 1438
    packed-switch p0, :pswitch_data_0

    .line 1446
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnKnown("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1442
    :pswitch_1
    const-string v0, "fixedorientation"

    return-object v0

    .line 1440
    :pswitch_2
    const-string v0, "landscape"

    return-object v0

    .line 1444
    :pswitch_3
    const-string/jumbo v0, "unspecified"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method static fullScreenPolicyToString(I)Ljava/lang/String;
    .locals 2
    .param p0, "policy"    # I

    .line 1533
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1534
    .local v0, "sb":Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    .line 1535
    const-string v1, "nr:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1537
    :cond_0
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_1

    .line 1538
    const-string v1, "r:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1540
    :cond_1
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_2

    .line 1541
    const-string v1, "ri:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1543
    :cond_2
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_3

    .line 1544
    const-string/jumbo v1, "t:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1546
    :cond_3
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_4

    .line 1547
    const-string/jumbo v1, "w:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1549
    :cond_4
    and-int/lit8 v1, p0, 0x20

    if-eqz v1, :cond_5

    .line 1550
    const-string v1, "b:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1552
    :cond_5
    and-int/lit8 v1, p0, 0x40

    if-eqz v1, :cond_6

    .line 1553
    const-string v1, "nra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1555
    :cond_6
    and-int/lit16 v1, p0, 0x80

    if-eqz v1, :cond_7

    .line 1556
    const-string v1, "nrs:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1558
    :cond_7
    and-int/lit16 v1, p0, 0x100

    if-eqz v1, :cond_8

    .line 1559
    const-string v1, "rcr:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1561
    :cond_8
    and-int/lit16 v1, p0, 0x200

    if-eqz v1, :cond_9

    .line 1562
    const-string v1, "cr:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1564
    :cond_9
    and-int/lit16 v1, p0, 0x400

    if-eqz v1, :cond_a

    .line 1565
    const-string v1, "cra:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1567
    :cond_a
    and-int/lit16 v1, p0, 0x800

    if-eqz v1, :cond_b

    .line 1568
    const-string/jumbo v1, "uc:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1570
    :cond_b
    and-int/lit16 v1, p0, 0x1000

    if-eqz v1, :cond_c

    .line 1571
    const-string v1, "de:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1573
    :cond_c
    and-int/lit16 v1, p0, 0x2000

    if-eqz v1, :cond_d

    .line 1574
    const-string v1, "dd:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1576
    :cond_d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_e

    .line 1577
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1579
    :cond_e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static fullScreenStringToPolicy(Ljava/lang/String;)I
    .locals 2
    .param p0, "policyStr"    # Ljava/lang/String;

    .line 1482
    const/4 v0, 0x0

    .line 1483
    .local v0, "policy":I
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string v1, "rcr"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    goto/16 :goto_1

    :sswitch_1
    const-string v1, "nrs"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    goto/16 :goto_1

    :sswitch_2
    const-string v1, "nra"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    goto/16 :goto_1

    :sswitch_3
    const-string v1, "cra"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    goto/16 :goto_1

    :sswitch_4
    const-string/jumbo v1, "uc"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xb

    goto :goto_1

    :sswitch_5
    const-string v1, "ri"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_6
    const-string v1, "nr"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_7
    const-string v1, "de"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xc

    goto :goto_1

    :sswitch_8
    const-string v1, "dd"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xd

    goto :goto_1

    :sswitch_9
    const-string v1, "cr"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    goto :goto_1

    :sswitch_a
    const-string/jumbo v1, "w"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_b
    const-string/jumbo v1, "t"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_c
    const-string v1, "r"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_d
    const-string v1, "b"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 1524
    :pswitch_0
    const/16 v0, 0x2000

    .line 1525
    goto :goto_2

    .line 1521
    :pswitch_1
    const/16 v0, 0x1000

    .line 1522
    goto :goto_2

    .line 1518
    :pswitch_2
    const/16 v0, 0x800

    .line 1519
    goto :goto_2

    .line 1515
    :pswitch_3
    const/16 v0, 0x400

    .line 1516
    goto :goto_2

    .line 1512
    :pswitch_4
    const/16 v0, 0x200

    .line 1513
    goto :goto_2

    .line 1509
    :pswitch_5
    const/16 v0, 0x100

    .line 1510
    goto :goto_2

    .line 1506
    :pswitch_6
    const/16 v0, 0x80

    .line 1507
    goto :goto_2

    .line 1503
    :pswitch_7
    const/16 v0, 0x40

    .line 1504
    goto :goto_2

    .line 1500
    :pswitch_8
    const/16 v0, 0x20

    .line 1501
    goto :goto_2

    .line 1497
    :pswitch_9
    const/16 v0, 0x10

    .line 1498
    goto :goto_2

    .line 1494
    :pswitch_a
    const/16 v0, 0x8

    .line 1495
    goto :goto_2

    .line 1491
    :pswitch_b
    const/4 v0, 0x4

    .line 1492
    goto :goto_2

    .line 1488
    :pswitch_c
    const/4 v0, 0x2

    .line 1489
    goto :goto_2

    .line 1485
    :pswitch_d
    const/4 v0, 0x1

    .line 1486
    nop

    .line 1529
    :goto_2
    return v0

    :sswitch_data_0
    .sparse-switch
        0x62 -> :sswitch_d
        0x72 -> :sswitch_c
        0x74 -> :sswitch_b
        0x77 -> :sswitch_a
        0xc6f -> :sswitch_9
        0xc80 -> :sswitch_8
        0xc81 -> :sswitch_7
        0xdc4 -> :sswitch_6
        0xe37 -> :sswitch_5
        0xe8e -> :sswitch_4
        0x181d2 -> :sswitch_3
        0x1ab1d -> :sswitch_2
        0x1ab2f -> :sswitch_1
        0x1b861 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getDisplayCutoutSetting(I)I
    .locals 1
    .param p1, "policy"    # I

    .line 884
    and-int/lit16 v0, p1, 0x1000

    if-eqz v0, :cond_0

    .line 885
    const/4 v0, 0x1

    return v0

    .line 886
    :cond_0
    and-int/lit16 v0, p1, 0x2000

    if-eqz v0, :cond_1

    .line 887
    const/4 v0, 0x0

    return v0

    .line 889
    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method static getInstance()Lcom/android/server/wm/MiuiOrientationImpl;
    .locals 1

    .line 175
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationStub;->get()Lcom/android/server/wm/MiuiOrientationStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl;

    return-object v0
.end method

.method private static getIntAttribute(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;I)I
    .locals 1
    .param p0, "parser"    # Lcom/android/modules/utils/TypedXmlPullParser;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .line 989
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1, p2}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeInt(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I
    .locals 4
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 556
    const/4 v0, -0x1

    .line 557
    .local v0, "mode":I
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 558
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v1

    .line 559
    .local v1, "activityPolicy":I
    const/4 v2, -0x1

    if-eq v2, v1, :cond_1

    .line 560
    and-int/lit8 v2, v1, 0x20

    if-nez v2, :cond_0

    .line 561
    const/4 v0, 0x1

    .line 563
    :cond_0
    return v0

    .line 565
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v2, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v2

    .line 566
    .local v2, "packagePolicy":I
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->isInBlackListFromPolicy(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 567
    const/4 v0, -0x1

    goto :goto_0

    .line 568
    :cond_2
    invoke-direct {p0, p2}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 569
    const/4 v0, 0x2

    goto :goto_0

    .line 571
    :cond_3
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v3, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationMode(Ljava/lang/String;)I

    move-result v0

    .line 573
    :goto_0
    return v0
.end method

.method private getOuterOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I
    .locals 5
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 313
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 315
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v0

    .line 316
    .local v0, "activityPolicy":I
    const/4 v2, 0x1

    if-eq v1, v0, :cond_0

    and-int/lit8 v3, v0, 0x10

    if-eqz v3, :cond_0

    .line 318
    return v2

    .line 320
    :cond_0
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v3, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v3

    .line 321
    .local v3, "packagePolicy":I
    if-eq v1, v3, :cond_1

    and-int/lit8 v4, v3, 0x10

    if-eqz v4, :cond_1

    .line 323
    return v2

    .line 326
    .end local v0    # "activityPolicy":I
    .end local v3    # "packagePolicy":I
    :cond_1
    return v1
.end method

.method private getRelaunchModeFromBundle(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 588
    const/4 v0, 0x0

    .line 589
    .local v0, "relaunchMode":I
    sget-boolean v1, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 590
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    .line 591
    .local v1, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mNr:Z

    if-eqz v2, :cond_0

    .line 592
    const/4 v0, 0x2

    goto :goto_0

    .line 593
    :cond_0
    iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mR:Z

    if-eqz v2, :cond_1

    .line 594
    const/4 v0, 0x1

    goto :goto_0

    .line 595
    :cond_1
    iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mRi:Z

    if-eqz v2, :cond_2

    .line 596
    const/4 v0, 0x4

    .line 598
    .end local v1    # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    :cond_2
    :goto_0
    return v0
.end method

.method private getRelaunchModeFromPolicy(I)I
    .locals 2
    .param p1, "policy"    # I

    .line 577
    const/4 v0, 0x0

    .line 578
    .local v0, "relaunchMode":I
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_0

    .line 579
    const/4 v0, 0x2

    goto :goto_0

    .line 580
    :cond_0
    and-int/lit8 v1, p1, 0x2

    if-eqz v1, :cond_1

    .line 581
    const/4 v0, 0x1

    goto :goto_0

    .line 582
    :cond_1
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_2

    .line 583
    const/4 v0, 0x4

    .line 584
    :cond_2
    :goto_0
    return v0
.end method

.method private getRotationOptionsFromBundle(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 638
    const/4 v0, 0x0

    .line 639
    .local v0, "rotationOptions":I
    sget-boolean v1, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 640
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    .line 641
    .local v1, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mNrs:Z

    if-eqz v2, :cond_0

    .line 642
    or-int/lit8 v0, v0, 0x1

    .line 643
    :cond_0
    iget-boolean v2, v1, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mNra:Z

    if-eqz v2, :cond_1

    .line 644
    or-int/lit8 v0, v0, 0x2

    .line 646
    .end local v1    # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    :cond_1
    return v0
.end method

.method private getRotationOptionsFromPolicy(I)I
    .locals 2
    .param p1, "policy"    # I

    .line 629
    const/4 v0, 0x0

    .line 630
    .local v0, "rotationOptions":I
    and-int/lit16 v1, p1, 0x80

    if-eqz v1, :cond_0

    .line 631
    or-int/lit8 v0, v0, 0x1

    .line 632
    :cond_0
    and-int/lit8 v1, p1, 0x40

    if-eqz v1, :cond_1

    .line 633
    or-int/lit8 v0, v0, 0x2

    .line 634
    :cond_1
    return v0
.end method

.method private static getSettingsFile()Landroid/util/AtomicFile;
    .locals 3

    .line 927
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "system/miui_orientation_settings.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 928
    .local v0, "settingsFile":Ljava/io/File;
    new-instance v1, Landroid/util/AtomicFile;

    const-string v2, "miui_orientations"

    invoke-direct {v1, v0, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 275
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    return v0
.end method

.method private isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 443
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v0

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 444
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isEmbeddingEnabledForPackage(Ljava/lang/String;)Z

    move-result v0

    .line 443
    return v0
.end method

.method private isEmbedded(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 448
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v0

    .line 449
    invoke-interface {v0, p1}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isEmbeddingEnabledForPackage(Ljava/lang/String;)Z

    move-result v0

    .line 448
    return v0
.end method

.method private isFixedAspectRatio(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 416
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 417
    return v1

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getAspectRatio(Ljava/lang/String;)F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private isFixedAspectRatioInPad(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 424
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 425
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/sizecompat/MiuiSizeCompatManager;->getMiuiSizeCompatAppRatio(Ljava/lang/String;)F

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 424
    :cond_0
    return v0

    .line 426
    :catch_0
    move-exception v1

    .line 427
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 429
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method private isFixedAspectRatioInPad(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 434
    nop

    .line 435
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1}, Landroid/sizecompat/MiuiSizeCompatManager;->getMiuiSizeCompatAppRatio(Ljava/lang/String;)F

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 434
    :cond_0
    return v0

    .line 436
    :catch_0
    move-exception v1

    .line 437
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 439
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method private isInBlackList(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 551
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isInBlackListFromPolicy(I)Z
    .locals 1
    .param p1, "policy"    # I

    .line 546
    const/4 v0, -0x1

    if-eq v0, p1, :cond_0

    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isLandEnabledForPackagePad(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 453
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    .line 454
    .local v0, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mEnable:Z

    return v1

    .line 455
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private isNeedCameraRotate(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 6
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 486
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isInBlackList(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 488
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v0

    .line 489
    .local v0, "activityPolicy":I
    const/4 v2, 0x1

    const/4 v3, -0x1

    if-eq v3, v0, :cond_1

    and-int/lit16 v4, v0, 0x200

    if-eqz v4, :cond_1

    .line 491
    return v2

    .line 493
    :cond_1
    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 494
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v4

    .line 495
    .local v4, "policy":I
    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    if-eq v3, v4, :cond_3

    and-int/lit16 v3, v4, 0x200

    if-eqz v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    return v1

    .line 486
    .end local v0    # "activityPolicy":I
    .end local v4    # "policy":I
    :cond_4
    :goto_0
    return v1
.end method

.method private isNeedCameraRotate(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 501
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_FOLD:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v0

    .line 503
    .local v0, "policy":I
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, -0x1

    if-eq v2, v0, :cond_2

    and-int/lit16 v2, v0, 0x200

    if-eqz v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1

    .line 501
    .end local v0    # "policy":I
    :cond_3
    :goto_0
    return v1
.end method

.method private isNeedCameraRotateAll(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 6
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 509
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isInBlackList(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 511
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v0

    .line 512
    .local v0, "activityPolicy":I
    const/4 v2, 0x1

    const/4 v3, -0x1

    if-eq v3, v0, :cond_1

    and-int/lit16 v4, v0, 0x400

    if-eqz v4, :cond_1

    .line 514
    return v2

    .line 516
    :cond_1
    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 517
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v4

    .line 518
    .local v4, "policy":I
    if-eq v3, v4, :cond_2

    and-int/lit16 v3, v4, 0x400

    if-eqz v3, :cond_2

    move v1, v2

    :cond_2
    return v1

    .line 509
    .end local v0    # "activityPolicy":I
    .end local v4    # "policy":I
    :cond_3
    :goto_0
    return v1
.end method

.method private isNeedCameraRotateInPad(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 6
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 531
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 532
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 533
    .local v0, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    .line 534
    .local v2, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatioInPad(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    .line 535
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isAppSupportCameraPreview(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    goto :goto_0

    :cond_1
    move v3, v1

    .line 536
    .local v3, "isAppSupportCameraPreview":Z
    :goto_0
    if-eqz v2, :cond_2

    iget-boolean v5, v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mCr:Z

    if-nez v5, :cond_3

    :cond_2
    if-eqz v3, :cond_4

    :cond_3
    move v1, v4

    :cond_4
    return v1
.end method

.method private isNeedCameraRotateInPad(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 523
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 524
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    .line 525
    .local v0, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatioInPad(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    .line 526
    invoke-static {}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->get()Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/android/server/wm/MiuiEmbeddingWindowServiceStub;->isAppSupportCameraPreview(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v1

    .line 527
    .local v2, "isAppSupportCameraPreview":Z
    :goto_0
    if-eqz v0, :cond_2

    iget-boolean v4, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mCr:Z

    if-nez v4, :cond_3

    :cond_2
    if-eqz v2, :cond_4

    :cond_3
    move v1, v3

    :cond_4
    return v1
.end method

.method private isNeedRotateWhenCameraResume(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 6
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 468
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 469
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 470
    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v0

    .line 471
    .local v0, "activityPolicy":I
    const/4 v2, 0x1

    const/4 v3, -0x1

    if-eq v3, v0, :cond_2

    and-int/lit16 v4, v0, 0x200

    if-eqz v4, :cond_2

    .line 473
    and-int/lit16 v3, v0, 0x100

    if-eqz v3, :cond_1

    .line 474
    return v2

    .line 476
    :cond_1
    return v1

    .line 479
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 480
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v4

    .line 481
    .local v4, "policy":I
    if-eq v3, v4, :cond_3

    and-int/lit16 v3, v4, 0x100

    if-eqz v3, :cond_3

    move v1, v2

    :cond_3
    return v1
.end method

.method private isNeedRotateWhenCameraResumeInPad(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 540
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateInPad(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    .line 542
    .local v0, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mRcr:Z

    if-nez v2, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatioInPad(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1

    .line 540
    .end local v0    # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    :cond_4
    :goto_0
    return v1
.end method

.method private synthetic lambda$showToastWhenLandscapeIfNeed$0()V
    .locals 2

    .line 769
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mCompatLandToast:Landroid/widget/Toast;

    const v1, 0x110f029c

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 770
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mToastVisible:Z

    .line 771
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mCompatLandToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 772
    return-void
.end method

.method private synthetic lambda$showToastWhenLandscapeIfNeed$1()V
    .locals 1

    .line 778
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mToastVisible:Z

    .line 779
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mCompatLandToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 780
    return-void
.end method

.method private needShowToast(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 7
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 739
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 741
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    if-nez v0, :cond_7

    .line 742
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_7

    .line 743
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 744
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    .line 745
    invoke-virtual {p1, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I

    .line 746
    invoke-static {v0}, Landroid/content/pm/ActivityInfo;->isFixedOrientationPortrait(I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 747
    invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getRotation()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 749
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    move v1, v2

    :cond_1
    return v1

    .line 750
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 751
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v0

    .line 752
    .local v0, "policy":I
    const/4 v3, 0x0

    .line 753
    .local v3, "showToast":Z
    const/4 v4, -0x1

    if-eq v4, v0, :cond_4

    .line 754
    and-int/lit8 v4, v0, 0x8

    if-eqz v4, :cond_3

    move v1, v2

    :cond_3
    move v3, v1

    goto :goto_0

    .line 756
    :cond_4
    iget-object v5, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v0

    .line 757
    if-eq v4, v0, :cond_6

    .line 758
    and-int/lit8 v4, v0, 0x8

    if-eqz v4, :cond_5

    move v1, v2

    :cond_5
    move v3, v1

    .line 760
    :cond_6
    :goto_0
    return v3

    .line 748
    .end local v0    # "policy":I
    .end local v3    # "showToast":Z
    :cond_7
    :goto_1
    return v1
.end method

.method static parseAppRequestOrientation(Ljava/lang/String;)Landroid/util/SparseArray;
    .locals 7
    .param p0, "value"    # Ljava/lang/String;

    .line 1452
    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/String;

    .line 1453
    .local v1, "orientationStr":[Ljava/lang/String;
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 1454
    .local v2, "orientationArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;"
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1455
    const-string v3, ","

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1458
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v1

    if-ge v3, v4, :cond_2

    .line 1459
    aget-object v4, v1, v3

    const-string v5, "\\."

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1460
    .local v4, "orientationForDiffDevice":[Ljava/lang/String;
    array-length v5, v4

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 1461
    aget-object v5, v4, v0

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    aget-object v6, v4, v6

    .line 1462
    invoke-static {v6}, Lcom/android/server/wm/MiuiOrientationImpl;->screenOrientationFromString(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1461
    invoke-virtual {v2, v5, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 1458
    .end local v4    # "orientationForDiffDevice":[Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1465
    .end local v3    # "i":I
    :cond_2
    return-object v2
.end method

.method static parseFullScreenPolicy(Ljava/lang/String;)I
    .locals 4
    .param p0, "value"    # Ljava/lang/String;

    .line 1469
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 1470
    .local v0, "policys":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 1471
    .local v1, "policy":I
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1472
    const-string v2, ":"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1475
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_1

    .line 1476
    aget-object v3, v0, v2

    invoke-static {v3}, Lcom/android/server/wm/MiuiOrientationImpl;->fullScreenStringToPolicy(Ljava/lang/String;)I

    move-result v3

    or-int/2addr v1, v3

    .line 1475
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1478
    .end local v2    # "i":I
    :cond_1
    return v1
.end method

.method private static readPackage(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/util/HashMap;)V
    .locals 3
    .param p0, "parser"    # Lcom/android/modules/utils/TypedXmlPullParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/modules/utils/TypedXmlPullParser;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 994
    .local p1, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    const-string v1, "name"

    invoke-interface {p0, v0, v1}, Lcom/android/modules/utils/TypedXmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 995
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 996
    const-string/jumbo v1, "value"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getIntAttribute(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/lang/String;I)I

    move-result v1

    .line 997
    .local v1, "value":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 999
    .end local v1    # "value":I
    :cond_0
    invoke-static {p0}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 1000
    return-void
.end method

.method private static readSettings(Landroid/util/AtomicFile;)Ljava/util/HashMap;
    .locals 11
    .param p0, "file"    # Landroid/util/AtomicFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/AtomicFile;",
            ")",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 935
    const-string v0, "MiuiOrientationImpl"

    :try_start_0
    invoke-virtual {p0}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 939
    .local v1, "stream":Ljava/io/InputStream;
    nop

    .line 940
    const/4 v2, 0x0

    .line 941
    .local v2, "success":Z
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 943
    .local v3, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :try_start_1
    invoke-static {v1}, Landroid/util/Xml;->resolvePullParser(Ljava/io/InputStream;)Lcom/android/modules/utils/TypedXmlPullParser;

    move-result-object v4

    .line 945
    .local v4, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    :goto_0
    invoke-interface {v4}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v5

    move v6, v5

    .local v6, "type":I
    const/4 v7, 0x1

    const/4 v8, 0x2

    if-eq v5, v8, :cond_0

    if-eq v6, v7, :cond_0

    goto :goto_0

    .line 950
    :cond_0
    if-ne v6, v8, :cond_6

    .line 954
    invoke-interface {v4}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v5

    .line 955
    .local v5, "outerDepth":I
    :cond_1
    :goto_1
    invoke-interface {v4}, Lcom/android/modules/utils/TypedXmlPullParser;->next()I

    move-result v8

    move v6, v8

    if-eq v8, v7, :cond_5

    const/4 v8, 0x3

    if-ne v6, v8, :cond_2

    .line 956
    invoke-interface {v4}, Lcom/android/modules/utils/TypedXmlPullParser;->getDepth()I

    move-result v9

    if-le v9, v5, :cond_5

    .line 957
    :cond_2
    if-eq v6, v8, :cond_1

    const/4 v8, 0x4

    if-ne v6, v8, :cond_3

    .line 958
    goto :goto_1

    .line 961
    :cond_3
    invoke-interface {v4}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v8

    .line 962
    .local v8, "tagName":Ljava/lang/String;
    const-string v9, "package"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 963
    invoke-static {v4, v3}, Lcom/android/server/wm/MiuiOrientationImpl;->readPackage(Lcom/android/modules/utils/TypedXmlPullParser;Ljava/util/HashMap;)V

    goto :goto_2

    .line 965
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown element under <orientation-settings>: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 966
    invoke-interface {v4}, Lcom/android/modules/utils/TypedXmlPullParser;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 965
    invoke-static {v0, v9}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    invoke-static {v4}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 969
    .end local v8    # "tagName":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .line 970
    :cond_5
    const/4 v2, 0x1

    .line 976
    .end local v4    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v5    # "outerDepth":I
    .end local v6    # "type":I
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 979
    :goto_3
    goto :goto_4

    .line 977
    :catch_0
    move-exception v4

    .line 978
    .local v4, "ignored":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 980
    .end local v4    # "ignored":Ljava/io/IOException;
    goto :goto_4

    .line 951
    .local v4, "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .restart local v6    # "type":I
    :cond_6
    :try_start_3
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v7, "no start tag found"

    invoke-direct {v5, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .end local v1    # "stream":Ljava/io/InputStream;
    .end local v2    # "success":Z
    .end local v3    # "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local p0    # "file":Landroid/util/AtomicFile;
    throw v5
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 975
    .end local v4    # "parser":Lcom/android/modules/utils/TypedXmlPullParser;
    .end local v6    # "type":I
    .restart local v1    # "stream":Ljava/io/InputStream;
    .restart local v2    # "success":Z
    .restart local v3    # "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local p0    # "file":Landroid/util/AtomicFile;
    :catchall_0
    move-exception v0

    goto :goto_5

    .line 971
    :catch_1
    move-exception v4

    .line 973
    .local v4, "e":Ljava/lang/Exception;
    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed parsing "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 976
    .end local v4    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_3

    .line 981
    :goto_4
    if-nez v2, :cond_7

    .line 982
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 984
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "read orientation settings: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    return-object v3

    .line 976
    :goto_5
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 979
    goto :goto_6

    .line 977
    :catch_2
    move-exception v4

    .line 978
    .local v4, "ignored":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 980
    .end local v4    # "ignored":Ljava/io/IOException;
    :goto_6
    throw v0

    .line 936
    .end local v1    # "stream":Ljava/io/InputStream;
    .end local v2    # "success":Z
    .end local v3    # "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catch_3
    move-exception v1

    .line 937
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "No existing orientation settings, starting empty"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    const/4 v0, 0x0

    return-object v0
.end method

.method private retrieveScreenOrientationInner(Lcom/android/server/wm/ActivityRecord;Landroid/content/pm/ActivityInfo;Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "activityInfo"    # Landroid/content/pm/ActivityInfo;
    .param p3, "appMetaData"    # Landroid/os/Bundle;

    .line 678
    const-string v0, "miui.screenInnerOrientation"

    const-string/jumbo v1, "unset"

    if-eqz p3, :cond_0

    .line 679
    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 681
    :cond_0
    nop

    :goto_0
    nop

    .line 682
    .local v1, "screenInnerOrientation":Ljava/lang/String;
    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->screenOrientationFromString(Ljava/lang/String;)I

    move-result v2

    .line 684
    .local v2, "appSpecOrientation":I
    iget-object v3, p2, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "invalid"

    if-eqz v3, :cond_1

    .line 685
    iget-object v3, p2, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 687
    :cond_1
    nop

    :goto_1
    move-object v0, v4

    .line 688
    .local v0, "activityScreenOrientation":Ljava/lang/String;
    nop

    .line 689
    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->screenOrientationFromString(Ljava/lang/String;)I

    move-result v3

    .line 690
    .local v3, "activitySpecOrientation":I
    const/4 v4, -0x2

    const/4 v5, 0x1

    if-lt v3, v4, :cond_2

    .line 691
    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v4, v5, v3}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->setAppOrientation(II)V

    .line 692
    return v5

    .line 693
    :cond_2
    if-eq v2, v4, :cond_3

    .line 694
    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    invoke-interface {v4, v5, v2}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->setAppOrientation(II)V

    .line 695
    return v5

    .line 697
    :cond_3
    const/4 v4, 0x0

    return v4
.end method

.method public static screenOrientationFromString(Ljava/lang/String;)I
    .locals 18
    .param p0, "orientation"    # Ljava/lang/String;

    .line 810
    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0xe

    const/16 v3, 0xd

    const/16 v4, 0xc

    const/16 v5, 0xb

    const/16 v6, 0xa

    const/16 v7, 0x9

    const/16 v8, 0x8

    const/4 v9, 0x7

    const/4 v10, 0x6

    const/4 v11, 0x5

    const/4 v12, 0x4

    const/4 v13, 0x3

    const/4 v14, 0x2

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, -0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string v1, "nosensor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v9

    goto/16 :goto_1

    :sswitch_1
    const-string v1, "landscape"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v14

    goto/16 :goto_1

    :sswitch_2
    const-string v1, "fullUser"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_1

    :sswitch_3
    const-string v1, "portrait"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v13

    goto/16 :goto_1

    :sswitch_4
    const-string/jumbo v1, "unset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move/from16 v1, v16

    goto/16 :goto_1

    :sswitch_5
    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v12

    goto/16 :goto_1

    :sswitch_6
    const-string/jumbo v1, "userPortrait"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    goto/16 :goto_1

    :sswitch_7
    const-string/jumbo v1, "sensorPortrait"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v7

    goto/16 :goto_1

    :sswitch_8
    const-string v1, "reversePortrait"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v5

    goto :goto_1

    :sswitch_9
    const-string v1, "fullSensor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_1

    :sswitch_a
    const-string/jumbo v1, "userLandscape"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v3

    goto :goto_1

    :sswitch_b
    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v10

    goto :goto_1

    :sswitch_c
    const-string v1, "locked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x10

    goto :goto_1

    :sswitch_d
    const-string v1, "behind"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v11

    goto :goto_1

    :sswitch_e
    const-string/jumbo v1, "unspecified"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v15

    goto :goto_1

    :sswitch_f
    const-string v1, "reverseLandscape"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v6

    goto :goto_1

    :sswitch_10
    const-string/jumbo v1, "sensorLandscape"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v8

    goto :goto_1

    :goto_0
    move/from16 v1, v17

    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 846
    const/4 v1, -0x3

    return v1

    .line 844
    :pswitch_0
    return v2

    .line 842
    :pswitch_1
    return v3

    .line 840
    :pswitch_2
    return v4

    .line 838
    :pswitch_3
    return v5

    .line 836
    :pswitch_4
    return v6

    .line 834
    :pswitch_5
    return v7

    .line 832
    :pswitch_6
    return v8

    .line 830
    :pswitch_7
    return v9

    .line 828
    :pswitch_8
    return v10

    .line 826
    :pswitch_9
    return v11

    .line 824
    :pswitch_a
    return v12

    .line 822
    :pswitch_b
    return v13

    .line 820
    :pswitch_c
    return v14

    .line 818
    :pswitch_d
    return v15

    .line 816
    :pswitch_e
    return v16

    .line 814
    :pswitch_f
    return v17

    .line 812
    :pswitch_10
    const/4 v1, -0x2

    return v1

    :sswitch_data_0
    .sparse-switch
        -0x703eafdf -> :sswitch_10
        -0x66042207 -> :sswitch_f
        -0x60ed74c9 -> :sswitch_e
        -0x5304eec6 -> :sswitch_d
        -0x4169ccf6 -> :sswitch_c
        -0x35ffac46 -> :sswitch_b
        -0x35a9c4d0 -> :sswitch_a
        -0x2ff104d7 -> :sswitch_9
        -0x2d278f63 -> :sswitch_8
        -0x1cf7e68b -> :sswitch_7
        -0x24def7a -> :sswitch_6
        0x36ebcb -> :sswitch_5
        0x6a47b29 -> :sswitch_4
        0x2b77bb9b -> :sswitch_3
        0x4f56a2fa -> :sswitch_2
        0x5545f2bb -> :sswitch_1
        0x5c954cbb -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static writeSettings(Landroid/util/AtomicFile;Ljava/util/Map;)V
    .locals 12
    .param p0, "file"    # Landroid/util/AtomicFile;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/AtomicFile;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1005
    .local p1, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v0, "package"

    const-string v1, "orientation-settings"

    const-string v2, "MiuiOrientationImpl"

    :try_start_0
    invoke-virtual {p0}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1009
    .local v3, "stream":Ljava/io/FileOutputStream;
    nop

    .line 1011
    const/4 v4, 0x0

    .line 1013
    .local v4, "success":Z
    :try_start_1
    invoke-static {v3}, Landroid/util/Xml;->resolveSerializer(Ljava/io/OutputStream;)Lcom/android/modules/utils/TypedXmlSerializer;

    move-result-object v5

    .line 1014
    .local v5, "out":Lcom/android/modules/utils/TypedXmlSerializer;
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v5, v7, v6}, Lcom/android/modules/utils/TypedXmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1015
    invoke-interface {v5, v7, v1}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1017
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 1018
    .local v8, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 1019
    .local v9, "packageName":Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 1021
    .local v10, "value":I
    invoke-interface {v5, v7, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1022
    const-string v11, "name"

    invoke-interface {v5, v7, v11, v9}, Lcom/android/modules/utils/TypedXmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1023
    const-string/jumbo v11, "value"

    invoke-interface {v5, v7, v11, v10}, Lcom/android/modules/utils/TypedXmlSerializer;->attributeInt(Ljava/lang/String;Ljava/lang/String;I)Lorg/xmlpull/v1/XmlSerializer;

    .line 1024
    invoke-interface {v5, v7, v0}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1025
    nop

    .end local v8    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "value":I
    goto :goto_0

    .line 1027
    :cond_0
    invoke-interface {v5, v7, v1}, Lcom/android/modules/utils/TypedXmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1028
    invoke-interface {v5}, Lcom/android/modules/utils/TypedXmlSerializer;->endDocument()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1029
    const/4 v4, 0x1

    .line 1033
    .end local v5    # "out":Lcom/android/modules/utils/TypedXmlSerializer;
    if-eqz v4, :cond_1

    .line 1034
    :goto_1
    invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    goto :goto_2

    .line 1036
    :cond_1
    invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 1038
    goto :goto_2

    .line 1033
    :catchall_0
    move-exception v0

    goto :goto_3

    .line 1030
    :catch_0
    move-exception v0

    .line 1031
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    const-string v1, "Failed to write orientation user settings."

    invoke-static {v2, v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1033
    nop

    .end local v0    # "e":Ljava/io/IOException;
    if-eqz v4, :cond_1

    .line 1034
    goto :goto_1

    .line 1039
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "write orientation settings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    return-void

    .line 1033
    :goto_3
    if-eqz v4, :cond_2

    .line 1034
    invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    goto :goto_4

    .line 1036
    :cond_2
    invoke-virtual {p0, v3}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    .line 1038
    :goto_4
    throw v0

    .line 1006
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .end local v4    # "success":Z
    :catch_1
    move-exception v0

    .line 1007
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to write orientation settings: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1008
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;IZZLjava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;
    .param p4, "opti"    # I
    .param p5, "dumpAll"    # Z
    .param p6, "dumpClient"    # Z
    .param p7, "dumpPackage"    # Ljava/lang/String;

    .line 895
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 896
    const-string v0, "miuiSmartRotation && miuiSmartOrientation not enabled"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 897
    return-void

    .line 899
    :cond_0
    const-string v0, "miuiSmartRotation PACKAGE SETTINGS MANAGER"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 900
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    const-string v1, ""

    invoke-static {v0, p2, v1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->-$$Nest$mdump(Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 901
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 902
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartOrientationPolicy:Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;

    invoke-virtual {v0, p2, v1}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 903
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 904
    return-void
.end method

.method public getOrientationMode(Lcom/android/server/wm/ActivityRecord;I)I
    .locals 4
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "orientation"    # I

    .line 286
    const/4 v0, -0x1

    .line 287
    .local v0, "mode":I
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v1, :cond_2

    .line 288
    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFlipDevice()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 289
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    return v0

    .line 290
    :cond_3
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 291
    .local v1, "packageName":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z

    if-eqz v2, :cond_4

    .line 292
    const/4 v0, 0x1

    goto :goto_0

    .line 293
    :cond_4
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFlipDevice()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->inMiuiGameSizeCompat(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 294
    invoke-static {p2}, Landroid/content/pm/ActivityInfo;->isFixedOrientationLandscape(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 295
    :cond_5
    const/4 v0, -0x1

    goto :goto_0

    .line 296
    :cond_6
    sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v2, :cond_7

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 297
    const/4 v0, 0x1

    goto :goto_0

    .line 298
    :cond_7
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFlipDevice()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 299
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isDisplayFolded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-static {p2}, Landroid/content/pm/ActivityInfo;->isFixedOrientationLandscape(I)Z

    move-result v2

    if-nez v2, :cond_9

    .line 300
    const/4 v0, 0x3

    goto :goto_0

    .line 303
    :cond_8
    invoke-direct {p0, p1, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->getOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I

    move-result v0

    .line 306
    :cond_9
    :goto_0
    sget-object v2, Lcom/android/internal/protolog/ProtoLogGroup;->WM_DEBUG_ORIENTATION:Lcom/android/internal/protolog/ProtoLogGroup;

    invoke-virtual {v2}, Lcom/android/internal/protolog/ProtoLogGroup;->isLogToLogcat()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 307
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getOrientationMode packageName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 308
    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->fullScreenModeToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 307
    const-string v3, "MiuiOrientationImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :cond_a
    return v0
.end method

.method public getPackageManager()Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    return-object v0
.end method

.method public getRelaunchMode(Lcom/android/server/wm/ActivityRecord;)I
    .locals 5
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 603
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 604
    .local v0, "packageName":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromBundle(Ljava/lang/String;)I

    move-result v1

    .line 605
    .local v1, "relaunchMode":I
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 607
    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v2

    if-nez v2, :cond_4

    .line 608
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v2

    if-nez v2, :cond_4

    .line 609
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 610
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 611
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z

    if-eqz v2, :cond_1

    .line 612
    iget v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I

    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromPolicy(I)I

    move-result v2

    return v2

    .line 614
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 615
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v2

    .line 616
    .local v2, "policy":I
    const/4 v3, -0x1

    if-eq v3, v2, :cond_2

    .line 617
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromPolicy(I)I

    move-result v1

    goto :goto_0

    .line 619
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v4, v0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v2

    .line 620
    if-eq v3, v2, :cond_3

    .line 621
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRelaunchModeFromPolicy(I)I

    move-result v1

    .line 624
    :cond_3
    :goto_0
    return v1

    .line 610
    .end local v2    # "policy":I
    :cond_4
    :goto_1
    return v1
.end method

.method public getRotationOptions(Lcom/android/server/wm/ActivityRecord;)I
    .locals 6
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 651
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 652
    .local v0, "packageName":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromBundle(Ljava/lang/String;)I

    move-result v1

    .line 653
    .local v1, "rotationOptions":I
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 655
    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v2

    if-nez v2, :cond_4

    .line 656
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v2

    if-nez v2, :cond_4

    .line 657
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 658
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 659
    :cond_0
    iget-boolean v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z

    if-eqz v2, :cond_1

    .line 660
    iget v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I

    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromPolicy(I)I

    move-result v2

    return v2

    .line 662
    :cond_1
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 663
    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v2

    .line 664
    .local v2, "policy":I
    const/4 v3, -0x1

    if-eq v3, v2, :cond_2

    .line 665
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromPolicy(I)I

    move-result v1

    goto :goto_0

    .line 667
    :cond_2
    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v5, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v2

    .line 668
    if-eq v3, v2, :cond_3

    .line 669
    invoke-direct {p0, v2}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptionsFromPolicy(I)I

    move-result v1

    .line 672
    :cond_3
    :goto_0
    return v1

    .line 658
    .end local v2    # "policy":I
    :cond_4
    :goto_1
    return v1
.end method

.method public getShowRotationSuggestion()I
    .locals 1

    .line 280
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_FOLD:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 281
    :cond_0
    iget v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mShowRotationSuggestion:I

    return v0

    .line 280
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public gotoDemoDebugMode(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "value"    # Ljava/lang/String;

    .line 907
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 908
    const-string v0, "miuiSmartRotation not enabled"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 909
    return-void

    .line 911
    :cond_0
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z

    .line 912
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I

    .line 913
    if-eqz v0, :cond_1

    .line 914
    invoke-static {p2}, Lcom/android/server/wm/MiuiOrientationImpl;->parseFullScreenPolicy(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I

    .line 916
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "goto or leave debug mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " policy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mDemoModePolicy:I

    .line 917
    invoke-static {v1}, Lcom/android/server/wm/MiuiOrientationImpl;->fullScreenPolicyToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 916
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 918
    return-void
.end method

.method public inFullScreenRelaunchMode(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 1640
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/android/server/wm/ActivityRecord;->mRelaunchInteractive:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public init(Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceImpl"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;
    .param p3, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 179
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mContext:Landroid/content/Context;

    .line 180
    iput-object p2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 181
    iput-object p3, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 182
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiFgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mHandler:Landroid/os/Handler;

    .line 183
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFileHandler:Landroid/os/Handler;

    .line 184
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;

    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mMiuiSettingsObserver:Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;

    .line 185
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    .line 186
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreen3AppCameraStrategy:Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;

    .line 187
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;

    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartOrientationPolicy:Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;

    .line 188
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mContext:Landroid/content/Context;

    const-string v1, ""

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mCompatLandToast:Landroid/widget/Toast;

    .line 189
    invoke-static {}, Lcom/android/server/wm/MiuiOrientationImpl;->getSettingsFile()Landroid/util/AtomicFile;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFile:Landroid/util/AtomicFile;

    .line 190
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 191
    const v1, 0x110b003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mShowRotationSuggestion:I

    .line 192
    const-string v0, "ro.config.miui_smart_orientation_enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->SMART_ORIENTATION_ENABLE:Z

    .line 193
    return-void
.end method

.method public isEnabled()Z
    .locals 1

    .line 244
    iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartRotationEnabled:Z

    return v0
.end method

.method public isFlipDevice()Z
    .locals 1

    .line 806
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    return v0
.end method

.method public isIgnoreRequestedOrientation(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 4
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 258
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 261
    :cond_0
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 262
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 261
    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 264
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_1

    return v1

    .line 266
    :cond_1
    iget-object v2, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 267
    .local v2, "appMetaData":Landroid/os/Bundle;
    if-eqz v2, :cond_2

    .line 268
    const-string v3, "miui.isIgnoreOrientationRequest"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_0

    .line 269
    :cond_2
    move v3, v1

    :goto_0
    nop

    .line 270
    .local v3, "ignoreOrientationRequest":Z
    if-eqz v3, :cond_3

    const/4 v1, 0x1

    return v1

    .line 271
    :cond_3
    return v1

    .line 259
    .end local v0    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v2    # "appMetaData":Landroid/os/Bundle;
    .end local v3    # "ignoreOrientationRequest":Z
    :cond_4
    :goto_1
    return v1
.end method

.method public isInOuterEnableList(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 252
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getOuterOrientationMode(Lcom/android/server/wm/ActivityRecord;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isLandEnableForPackage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .line 459
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 460
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 461
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationMode(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    move v1, v2

    :cond_1
    return v1

    .line 461
    :cond_2
    :goto_0
    return v1
.end method

.method public isNeedAccSensor()Z
    .locals 3

    .line 797
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mLastResumeApp:Lcom/android/server/wm/ActivityRecord;

    if-nez v0, :cond_0

    goto :goto_0

    .line 798
    :cond_0
    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    .line 799
    .local v0, "activityName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartOrientationPolicy:Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;

    invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->isNeedAccSensor(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 800
    const/4 v1, 0x1

    return v1

    .line 802
    :cond_1
    return v1

    .line 797
    .end local v0    # "activityName":Ljava/lang/String;
    :cond_2
    :goto_0
    return v1
.end method

.method public isNeedRotateCameraInFullScreen(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 408
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    return v1

    .line 409
    :cond_2
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotate(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 410
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 411
    :cond_3
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isNeedCameraRotateInPad(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    .line 412
    .local v0, "rotate3AppCamera":Z
    :goto_0
    if-nez v0, :cond_6

    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnableForPackage(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    move v1, v2

    :cond_7
    return v1
.end method

.method public isNeedSetDisplayCutout(Ljava/lang/String;)I
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 874
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 875
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 879
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v0

    .line 880
    .local v0, "policy":I
    invoke-direct {p0, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getDisplayCutoutSetting(I)I

    move-result v1

    return v1

    .line 876
    .end local v0    # "policy":I
    :cond_1
    :goto_0
    const/4 v0, -0x1

    return v0
.end method

.method public isNeedUpdateConfig(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 7
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 379
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 380
    :cond_0
    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "packageName":Ljava/lang/String;
    sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 382
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;

    .line 383
    .local v2, "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    iget-boolean v4, v2, Lcom/android/server/wm/MiuiOrientationImpl$FullRule;->mUc:Z

    if-eqz v4, :cond_1

    return v3

    .line 385
    .end local v2    # "fullRule":Lcom/android/server/wm/MiuiOrientationImpl$FullRule;
    :cond_1
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mWindowManager:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 386
    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy;->isDisplayFolded()Z

    move-result v2

    if-nez v2, :cond_5

    .line 387
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v2

    if-nez v2, :cond_5

    .line 389
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMiuiHoverWindowingMode()Z

    move-result v2

    if-nez v2, :cond_5

    .line 391
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 392
    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isFixedAspectRatio(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 393
    :cond_2
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 394
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicyByComponent(Ljava/lang/String;)I

    move-result v2

    .line 395
    .local v2, "activityPolicy":I
    const/4 v4, -0x1

    if-eq v4, v2, :cond_3

    and-int/lit16 v5, v2, 0x800

    if-eqz v5, :cond_3

    .line 397
    return v3

    .line 400
    :cond_3
    iget-object v5, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    iget-object v6, p1, Lcom/android/server/wm/ActivityRecord;->mActivityComponent:Landroid/content/ComponentName;

    .line 401
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v5

    .line 402
    .local v5, "policy":I
    if-eq v4, v5, :cond_4

    and-int/lit16 v4, v5, 0x800

    if-eqz v4, :cond_4

    move v0, v3

    :cond_4
    return v0

    .line 392
    .end local v2    # "activityPolicy":I
    .end local v5    # "policy":I
    :cond_5
    :goto_0
    return v0
.end method

.method public isSmartOrientEnable()Z
    .locals 1

    .line 248
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->SMART_ORIENTATION_ENABLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_MIUI_OPTIMIZATION:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public needUpdateOrientationForPad()Z
    .locals 3

    .line 1601
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mRootWindowContainer:Lcom/android/server/wm/RootWindowContainer;

    invoke-virtual {v0}, Lcom/android/server/wm/RootWindowContainer;->getTopResumedActivity()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    .line 1602
    .local v0, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 1603
    .local v1, "packageName":Ljava/lang/String;
    :goto_0
    sget-boolean v2, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1604
    invoke-direct {p0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->isLandEnabledForPackagePad(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1605
    const/4 v2, 0x1

    return v2

    .line 1607
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method onSystemReady()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartOrientationPolicy:Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->init()V

    .line 197
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mMiuiSettingsObserver:Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl$MiuiSettingsObserver;->observe()V

    .line 198
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFile:Landroid/util/AtomicFile;

    invoke-static {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->readSettings(Landroid/util/AtomicFile;)Ljava/util/HashMap;

    move-result-object v0

    .line 199
    .local v0, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mPackagesMapByUserSettings:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 202
    :cond_0
    return-void
.end method

.method public overrideOrientationBeforeSensorChanged(Lcom/android/server/wm/ActivityRecord;)V
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 1620
    iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOrientationOptions:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1622
    .local v0, "nonIgnoreSensor":Z
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->setOrientationForAppRequest()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1623
    return-void

    .line 1626
    :cond_1
    iget-object v2, p1, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getDisplayRotation()Lcom/android/server/wm/DisplayRotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayRotation;->getUserRotationMode()I

    move-result v2

    if-ne v2, v1, :cond_2

    .line 1628
    invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->getRotationOptions(Lcom/android/server/wm/ActivityRecord;)I

    move-result v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    .line 1629
    return-void

    .line 1632
    :cond_2
    const-string/jumbo v1, "sensor"

    invoke-virtual {p1, v1}, Lcom/android/server/wm/ActivityRecord;->overrideOrRestoreOrientationIfNeed(Ljava/lang/String;)V

    .line 1633
    return-void
.end method

.method public overrideOrientationForFoldChanged(Lcom/android/server/wm/ActivityRecord;)V
    .locals 1
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 1613
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->setOrientationForAppRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1614
    :cond_0
    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_FOLD:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 1616
    :cond_1
    const-string v0, "fold"

    invoke-virtual {p1, v0}, Lcom/android/server/wm/ActivityRecord;->overrideOrRestoreOrientationIfNeed(Ljava/lang/String;)V

    .line 1617
    return-void

    .line 1614
    :cond_2
    :goto_0
    return-void
.end method

.method public reportEvent(Lcom/android/server/wm/DisplayContent;II)V
    .locals 6
    .param p1, "mDisplayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p2, "oldRotation"    # I
    .param p3, "rotation"    # I

    .line 1583
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getLastOrientationSource()Lcom/android/server/wm/WindowContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1584
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getLastOrientationSource()Lcom/android/server/wm/WindowContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/WindowContainer;->asActivityRecord()Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1585
    .local v0, "activityRecord":Lcom/android/server/wm/ActivityRecord;
    :goto_0
    invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayRotation()Lcom/android/server/wm/DisplayRotation;

    move-result-object v1

    .line 1586
    .local v1, "displayRotation":Lcom/android/server/wm/DisplayRotation;
    invoke-virtual {v1, p2}, Lcom/android/server/wm/DisplayRotation;->isAnyPortrait(I)Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 1587
    invoke-virtual {v1, p3}, Lcom/android/server/wm/DisplayRotation;->isLandscapeOrSeascape(I)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto :goto_1

    :cond_1
    move v2, v4

    .line 1588
    .local v2, "isToLandscape":Z
    :goto_1
    invoke-virtual {v1, p2}, Lcom/android/server/wm/DisplayRotation;->isLandscapeOrSeascape(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1589
    invoke-virtual {v1, p3}, Lcom/android/server/wm/DisplayRotation;->isAnyPortrait(I)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_2

    :cond_2
    move v3, v4

    .line 1590
    .local v3, "isToPortrait":Z
    :goto_2
    if-eqz v0, :cond_4

    .line 1591
    if-eqz v2, :cond_3

    .line 1592
    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    const/16 v5, 0x20

    invoke-virtual {v4, v0, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->updateActivityUsageStats(Lcom/android/server/wm/ActivityRecord;I)V

    .line 1594
    :cond_3
    if-eqz v3, :cond_4

    .line 1595
    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    const/16 v5, 0x21

    invoke-virtual {v4, v0, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->updateActivityUsageStats(Lcom/android/server/wm/ActivityRecord;I)V

    .line 1598
    :cond_4
    return-void
.end method

.method public retrieveScreenOrientation(Lcom/android/server/wm/ActivityRecord;)V
    .locals 9
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 702
    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->intent:Landroid/content/Intent;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, v1, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 703
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 702
    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 705
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_0

    return-void

    .line 707
    :cond_0
    iget-object v1, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 708
    .local v1, "appMetaData":Landroid/os/Bundle;
    invoke-direct {p0, p1, v0, v1}, Lcom/android/server/wm/MiuiOrientationImpl;->retrieveScreenOrientationInner(Lcom/android/server/wm/ActivityRecord;Landroid/content/pm/ActivityInfo;Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    .line 710
    :cond_1
    const-string v2, "miui.screenOrientation"

    const-string/jumbo v3, "unset"

    if-eqz v1, :cond_2

    .line 711
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 713
    :cond_2
    move-object v4, v3

    :goto_0
    nop

    .line 714
    .local v4, "appScreenOrientation":Ljava/lang/String;
    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v5, :cond_3

    .line 715
    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v5, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 717
    :cond_3
    move-object v2, v3

    :goto_1
    nop

    .line 719
    .local v2, "activityScreenOrientation":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 720
    nop

    .line 721
    invoke-static {v2}, Lcom/android/server/wm/MiuiOrientationImpl;->parseAppRequestOrientation(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v3

    .line 722
    .local v3, "activitySpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v5, v6, :cond_4

    .line 723
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 724
    .local v6, "key":I
    iget-object v7, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    .line 725
    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 724
    invoke-interface {v7, v6, v8}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->setAppOrientation(II)V

    .line 722
    .end local v6    # "key":I
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .end local v3    # "activitySpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    :cond_4
    goto :goto_4

    .line 727
    :cond_5
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 728
    nop

    .line 729
    invoke-static {v4}, Lcom/android/server/wm/MiuiOrientationImpl;->parseAppRequestOrientation(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v3

    .line 730
    .local v3, "appSpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v5, v6, :cond_7

    .line 731
    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 732
    .restart local v6    # "key":I
    iget-object v7, p1, Lcom/android/server/wm/ActivityRecord;->mActivityRecordStub:Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;

    .line 733
    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 732
    invoke-interface {v7, v6, v8}, Lcom/android/server/wm/ActivityRecordStub$MutableActivityRecordStub;->setAppOrientation(II)V

    .line 730
    .end local v6    # "key":I
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 727
    .end local v3    # "appSpecOrientation":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/lang/Integer;>;"
    .end local v5    # "i":I
    :cond_6
    :goto_4
    nop

    .line 736
    :cond_7
    return-void
.end method

.method public setEmbeddedFullRuleData(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .line 330
    .local p1, "fullRuleMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;>;"
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mEmbeddedFullRules:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 331
    new-instance v0, Lcom/android/server/wm/MiuiOrientationImpl$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiOrientationImpl$1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V

    invoke-interface {p1, v0}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 376
    return-void
.end method

.method public setOrientationOptions(Lcom/android/server/wm/ActivityRecord;I)V
    .locals 0
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "options"    # I

    .line 1636
    iput p2, p1, Lcom/android/server/wm/ActivityRecord;->mOrientationOptions:I

    .line 1637
    return-void
.end method

.method public setUserSettings(Ljava/io/PrintWriter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .line 231
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v0, p2, p3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->setUserSettings(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method public setUserSettings(Ljava/io/PrintWriter;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "pkgNames"    # [Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v0, p2, p3}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->setUserSettings([Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method public showToastWhenLandscapeIfNeed(Lcom/android/server/wm/ActivityRecord;)V
    .locals 2
    .param p1, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 765
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->needShowToast(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/server/wm/ActivityRecord;->mOriginRequestOrientation:I

    .line 766
    invoke-virtual {p0, p1, v0}, Lcom/android/server/wm/MiuiOrientationImpl;->getOrientationMode(Lcom/android/server/wm/ActivityRecord;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 768
    invoke-static {}, Lcom/android/server/DisplayThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 773
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mToastVisible:Z

    if-eqz v0, :cond_1

    .line 775
    invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->inMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/wm/ActivityRecord$State;->RESUMED:Lcom/android/server/wm/ActivityRecord$State;

    .line 776
    invoke-virtual {p1, v0}, Lcom/android/server/wm/ActivityRecord;->isState(Lcom/android/server/wm/ActivityRecord$State;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 777
    invoke-static {}, Lcom/android/server/DisplayThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiOrientationImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 782
    :cond_1
    :goto_0
    return-void
.end method

.method public update3appCameraWhenResume(Lcom/android/server/wm/ActivityRecord;)V
    .locals 3
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;

    .line 852
    if-nez p1, :cond_0

    return-void

    .line 853
    :cond_0
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mLastResumeApp:Lcom/android/server/wm/ActivityRecord;

    .line 854
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v0, :cond_3

    .line 855
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 856
    invoke-static {v0}, Lmiui/app/MiuiFreeFormManager;->openCameraInFreeForm(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 857
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreen3AppCameraStrategy:Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;

    invoke-virtual {v0, p1}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->update3appCameraRotate(Lcom/android/server/wm/ActivityRecord;)V

    .line 858
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreen3AppCameraStrategy:Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;

    iget-object v1, p1, Lcom/android/server/wm/ActivityRecord;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    .line 859
    invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getRotation()I

    move-result v1

    .line 858
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2, v2}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->update3appCameraRotation(Lcom/android/server/wm/ActivityRecord;IZZ)V

    .line 860
    return-void

    .line 856
    :cond_3
    :goto_0
    return-void
.end method

.method public update3appCameraWhenRotateOrFold(Lcom/android/server/wm/ActivityRecord;IZZ)V
    .locals 1
    .param p1, "r"    # Lcom/android/server/wm/ActivityRecord;
    .param p2, "rotation"    # I
    .param p3, "isRotated"    # Z
    .param p4, "isFoldChanged"    # Z

    .line 865
    if-nez p1, :cond_0

    return-void

    .line 866
    :cond_0
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/wm/MiuiOrientationImpl;->IS_TABLET:Z

    if-eqz v0, :cond_3

    .line 867
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiOrientationImpl;->isEmbedded(Lcom/android/server/wm/ActivityRecord;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 868
    invoke-static {v0}, Lmiui/app/MiuiFreeFormManager;->openCameraInFreeForm(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 869
    :cond_2
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreen3AppCameraStrategy:Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;

    .line 870
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreen3AppCameraStrategy;->update3appCameraRotation(Lcom/android/server/wm/ActivityRecord;IZZ)V

    .line 871
    return-void

    .line 868
    :cond_3
    :goto_0
    return-void
.end method

.method public updateApplicationInfo(Landroid/content/pm/ApplicationInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;

    .line 785
    iget-object v0, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 786
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 787
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mFullScreenPackageManager:Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;

    invoke-virtual {v1, v0}, Lcom/android/server/wm/MiuiOrientationImpl$FullScreenPackageManager;->getOrientationPolicy(Ljava/lang/String;)I

    move-result v1

    .line 788
    .local v1, "packagePolicy":I
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl;->mSmartOrientationPolicy:Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;

    invoke-virtual {v2, v0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->isSupportSmartOrientation(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 789
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSupportSmartOrientation packageName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiOrientationImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    iget v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I

    const/high16 v3, 0x200000

    or-int/2addr v2, v3

    iput v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I

    goto :goto_0

    .line 792
    :cond_1
    iget v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I

    const v3, -0x200001

    and-int/2addr v2, v3

    iput v2, p1, Landroid/content/pm/ApplicationInfo;->privateFlagsExt:I

    .line 794
    :goto_0
    return-void
.end method
