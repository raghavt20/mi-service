class com.android.server.wm.AppTransitionInjector$ScaleWallPaperAnimation extends android.view.animation.Animation {
	 /* .source "AppTransitionInjector.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/AppTransitionInjector; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ScaleWallPaperAnimation" */
} // .end annotation
/* # instance fields */
private Boolean mEnter;
private Float mPivotX;
private Float mPivotY;
/* # direct methods */
public com.android.server.wm.AppTransitionInjector$ScaleWallPaperAnimation ( ) {
/* .locals 0 */
/* .param p1, "pivotX" # F */
/* .param p2, "pivotY" # F */
/* .param p3, "enter" # Z */
/* .line 1336 */
/* invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V */
/* .line 1338 */
/* iput p1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;->mPivotX:F */
/* .line 1339 */
/* iput p2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;->mPivotY:F */
/* .line 1340 */
/* iput-boolean p3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;->mEnter:Z */
/* .line 1341 */
return;
} // .end method
/* # virtual methods */
protected void applyTransformation ( Float p0, android.view.animation.Transformation p1 ) {
/* .locals 8 */
/* .param p1, "interpolatedTime" # F */
/* .param p2, "t" # Landroid/view/animation/Transformation; */
/* .line 1345 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1346 */
/* .local v0, "scale":F */
/* iget-boolean v1, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;->mEnter:Z */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 1348 */
	 /* float-to-double v4, p1 */
	 /* const-wide/high16 v6, 0x3fe0000000000000L # 0.5 */
	 /* sub-double/2addr v4, v6 */
	 java.lang.Math .abs ( v4,v5 );
	 /* move-result-wide v4 */
	 /* div-double/2addr v4, v6 */
	 /* sub-double v4, v2, v4 */
	 /* const-wide/16 v6, 0x0 */
	 java.lang.Math .max ( v6,v7,v4,v5 );
	 /* move-result-wide v4 */
	 /* const-wide v6, 0x3fd6666666666666L # 0.35 */
	 java.lang.Math .pow ( v4,v5,v6,v7 );
	 /* move-result-wide v4 */
	 /* const-wide v6, 0x3fb999999999999aL # 0.1 */
	 /* mul-double/2addr v4, v6 */
	 /* sub-double v4, v2, v4 */
	 /* double-to-float v0, v4 */
	 /* .line 1350 */
} // :cond_0
/* float-to-double v4, v0 */
/* cmpl-double v1, v4, v2 */
/* if-lez v1, :cond_1 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 1351 */
} // :cond_1
(( android.view.animation.Transformation ) p2 ).getMatrix ( ); // invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
/* iget v2, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;->mPivotX:F */
/* iget v3, p0, Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;->mPivotY:F */
(( android.graphics.Matrix ) v1 ).setScale ( v0, v0, v2, v3 ); // invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FFFF)V
/* .line 1352 */
return;
} // .end method
