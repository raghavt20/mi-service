.class public Lcom/android/server/wm/AppTransitionInjector;
.super Ljava/lang/Object;
.source "AppTransitionInjector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;,
        Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;,
        Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;,
        Lcom/android/server/wm/AppTransitionInjector$BezierYAnimation;,
        Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;,
        Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator;,
        Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator;,
        Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator;,
        Lcom/android/server/wm/AppTransitionInjector$ScaleWallPaperAnimation;,
        Lcom/android/server/wm/AppTransitionInjector$ScaleTaskAnimation;
    }
.end annotation


# static fields
.field static final APP_TRANSITION_SPECS_PENDING_TIMEOUT:I

.field static final BEZIER_ALLPAPER_OPEN_DURATION:I = 0x190

.field private static final BLACK_LIST_NOT_ALLOWED_SNAPSHOT:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final BLACK_LIST_NOT_ALLOWED_SNAPSHOT_COMPONENT:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final DEFAULT_ACTIVITY_SCALE_DOWN_ALPHA_DELAY:I = 0x64

.field private static final DEFAULT_ACTIVITY_SCALE_DOWN_ALPHA_DURATION:I = 0xc8

.field private static final DEFAULT_ACTIVITY_SCALE_DOWN_DURATION:I = 0x1c2

.field private static final DEFAULT_ACTIVITY_SCALE_UP_ALPHA_DURATION:I = 0x64

.field private static final DEFAULT_ACTIVITY_SCALE_UP_DURATION:I = 0x1f4

.field private static final DEFAULT_ACTIVITY_TRANSITION_DURATION:I = 0x1f4

.field private static final DEFAULT_ALPHA_DURATION:I = 0xd2

.field private static final DEFAULT_ALPHA_OFFSET:I = 0x28

.field private static final DEFAULT_ANIMATION_DURATION:I = 0x12c

.field static final DEFAULT_APP_TRANSITION_ROUND_CORNER_RADIUS:I = 0x3c

.field private static final DEFAULT_BACK_TO_SCREEN_CENTER_SCALE:F = 0.6f

.field private static final DEFAULT_ENTER_ACTIVITY_END_ALPHA:F = 1.0f

.field private static final DEFAULT_ENTER_ACTIVITY_START_ALPHA:F = 0.0f

.field private static final DEFAULT_EXIT_ACTIVITY_END_ALPHA:F = 0.8f

.field private static final DEFAULT_EXIT_ACTIVITY_START_ALPHA:F = 1.0f

.field private static final DEFAULT_LAUNCH_FORM_HOME_DURATION:I = 0x12c

.field private static final DEFAULT_REENTER_ACTIVITY_END_ALPHA:F = 1.0f

.field private static final DEFAULT_REENTER_ACTIVITY_START_ALPHA:F = 0.8f

.field private static final DEFAULT_RETURN_ACTIVITY_END_ALPHA:F = 0.0f

.field private static final DEFAULT_RETURN_ACTIVITY_START_ALPHA:F = 1.0f

.field private static final DEFAULT_TASK_TRANSITION_DURATION:I = 0x28a

.field private static final DEFAULT_WALLPAPER_EXIT_SCALE_X:F = 0.4f

.field private static final DEFAULT_WALLPAPER_EXIT_SCALE_Y:F = 0.4f

.field static final DEFAULT_WALLPAPER_OPEN_DURATION:I = 0x12c

.field private static final DEFAULT_WALLPAPER_TRANSITION_DURATION:I = 0x226

.field private static final DEFAULT_WALLPAPER_TRANSITION_R_CTS_DURATION:I = 0x190

.field static DISPLAY_ROUND_CORNER_RADIUS:I = 0x0

.field private static final IGNORE_LAUNCHED_FROM_SYSTEM_SURFACE:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final IS_E10:Z

.field private static final LAUNCHER_DEFAULT_ALPHA:F = 1.0f

.field private static final LAUNCHER_DEFAULT_SCALE:F = 1.0f

.field private static final LAUNCHER_TRANSITION_ALPHA:F = 0.0f

.field private static final LAUNCHER_TRANSITION_SCALE:F = 0.8f

.field static final NEXT_TRANSIT_TYPE_BACK_HOME:I = 0x66

.field static final NEXT_TRANSIT_TYPE_BACK_WITH_SCALED_THUMB:I = 0x6a

.field static final NEXT_TRANSIT_TYPE_LAUNCH_BACK_ROUNDED_VIEW:I = 0x68

.field static final NEXT_TRANSIT_TYPE_LAUNCH_FROM_HOME:I = 0x65

.field static final NEXT_TRANSIT_TYPE_LAUNCH_FROM_ROUNDED_VIEW:I = 0x67

.field static final NEXT_TRANSIT_TYPE_LAUNCH_WITH_SCALED_THUMB:I = 0x69

.field static final PENDING_EXECUTE_APP_TRANSITION_TIMEOUT:I = 0x64

.field private static final QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final QUINT_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final SCALE_UP_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final TAG:Ljava/lang/String; = "AppTransitionInjector"

.field static final THUMBNAIL_ANIMATION_TIMEOUT_DURATION:I = 0x3e8

.field private static final WHITE_LIST_ALLOW_CUSTOM_ANIMATION:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final WHITE_LIST_ALLOW_CUSTOM_APPLICATION_TRANSITION:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

.field private static sMiuiAnimSupportInset:Landroid/graphics/Rect;


# direct methods
.method static bridge synthetic -$$Nest$smdoAnimationCallback(Landroid/os/IRemoteCallback;)V
    .locals 0

    invoke-static {p0}, Lcom/android/server/wm/AppTransitionInjector;->doAnimationCallback(Landroid/os/IRemoteCallback;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    .line 75
    const/16 v0, 0x3c

    sput v0, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    .line 76
    const-string v0, "beryllium"

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/AppTransitionInjector;->IS_E10:Z

    .line 81
    const-string v0, "cactus"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x320

    goto :goto_0

    :cond_0
    const/16 v0, 0x64

    :goto_0
    sput v0, Lcom/android/server/wm/AppTransitionInjector;->APP_TRANSITION_SPECS_PENDING_TIMEOUT:I

    .line 114
    new-instance v0, Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator;-><init>(Lcom/android/server/wm/AppTransitionInjector$CubicEaseOutInterpolator-IA;)V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 115
    new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator;

    const v2, 0x3f4ccccd    # 0.8f

    const v3, 0x3f733333    # 0.95f

    invoke-direct {v0, v3, v2}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->SCALE_UP_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 117
    new-instance v0, Lcom/android/server/wm/PhysicBasedInterpolator;

    const v2, 0x3f47ae14    # 0.78f

    invoke-direct {v0, v3, v2}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 119
    new-instance v0, Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator;

    invoke-direct {v0, v1}, Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator;-><init>(Lcom/android/server/wm/AppTransitionInjector$QuartEaseOutInterpolator-IA;)V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 120
    new-instance v0, Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator;

    invoke-direct {v0, v1}, Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator;-><init>(Lcom/android/server/wm/AppTransitionInjector$QuintEaseOutInterpolator-IA;)V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->QUINT_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 121
    sput-object v1, Lcom/android/server/wm/AppTransitionInjector;->sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

    .line 123
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    .line 125
    new-instance v0, Lcom/android/server/wm/AppTransitionInjector$1;

    invoke-direct {v0}, Lcom/android/server/wm/AppTransitionInjector$1;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->WHITE_LIST_ALLOW_CUSTOM_ANIMATION:Ljava/util/ArrayList;

    .line 134
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->BLACK_LIST_NOT_ALLOWED_SNAPSHOT_COMPONENT:Ljava/util/HashSet;

    .line 138
    const-string v1, "com.tencent.mm/.plugin.offline.ui.WalletOfflineCoinPurseUI"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->BLACK_LIST_NOT_ALLOWED_SNAPSHOT:Ljava/util/ArrayList;

    .line 145
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/wm/AppTransitionInjector;->IGNORE_LAUNCHED_FROM_SYSTEM_SURFACE:Ljava/util/ArrayList;

    .line 149
    const-string v2, "com.android.camera"

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    const-string v3, "com.android.browser"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v3, "com.miui.securitycenter"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v3, "com.mi.globalbrowser"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    const-string v3, "com.miui.home"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    const-string v3, "com.mi.android.globallauncher"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    const-string v3, "com.android.contacts"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    const-string v3, "com.android.quicksearchbox"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    const-string v0, "com.mfashiongallery.emag"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    new-instance v0, Lcom/android/server/wm/AppTransitionInjector$3;

    invoke-direct {v0}, Lcom/android/server/wm/AppTransitionInjector$3;-><init>()V

    sput-object v0, Lcom/android/server/wm/AppTransitionInjector;->WHITE_LIST_ALLOW_CUSTOM_APPLICATION_TRANSITION:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static addAnimationListener(Landroid/view/animation/Animation;Landroid/os/Handler;Landroid/os/IRemoteCallback;Landroid/os/IRemoteCallback;)V
    .locals 3
    .param p0, "a"    # Landroid/view/animation/Animation;
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "mAnimationStartCallback"    # Landroid/os/IRemoteCallback;
    .param p3, "mAnimationFinishCallback"    # Landroid/os/IRemoteCallback;

    .line 383
    move-object v0, p2

    .line 384
    .local v0, "exitStartCallback":Landroid/os/IRemoteCallback;
    move-object v1, p3

    .line 385
    .local v1, "exitFinishCallback":Landroid/os/IRemoteCallback;
    new-instance v2, Lcom/android/server/wm/AppTransitionInjector$2;

    invoke-direct {v2, v0, p1, v1}, Lcom/android/server/wm/AppTransitionInjector$2;-><init>(Landroid/os/IRemoteCallback;Landroid/os/Handler;Landroid/os/IRemoteCallback;)V

    invoke-virtual {p0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 405
    return-void
.end method

.method static allowCustomAnimation(Landroid/util/ArraySet;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArraySet<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;)Z"
        }
    .end annotation

    .line 1082
    .local p0, "closingApps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;"
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 1083
    return v0

    .line 1086
    :cond_0
    invoke-virtual {p0}, Landroid/util/ArraySet;->size()I

    move-result v1

    .line 1087
    .local v1, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_2

    .line 1088
    invoke-virtual {p0, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/ActivityRecord;

    .line 1089
    .local v3, "atoken":Lcom/android/server/wm/ActivityRecord;
    if-eqz v3, :cond_1

    .line 1090
    invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->findMainWindow()Lcom/android/server/wm/WindowState;

    move-result-object v4

    .line 1091
    .local v4, "win":Lcom/android/server/wm/WindowState;
    if-eqz v4, :cond_1

    sget-object v5, Lcom/android/server/wm/AppTransitionInjector;->WHITE_LIST_ALLOW_CUSTOM_ANIMATION:Ljava/util/ArrayList;

    iget-object v6, v4, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget-object v6, v6, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    .line 1092
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1093
    const/4 v0, 0x1

    return v0

    .line 1087
    .end local v3    # "atoken":Lcom/android/server/wm/ActivityRecord;
    .end local v4    # "win":Lcom/android/server/wm/WindowState;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1097
    .end local v2    # "i":I
    :cond_2
    return v0
.end method

.method static calculateGestureThumbnailSpec(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Matrix;FLandroid/view/SurfaceControl$Transaction;Landroid/view/SurfaceControl;)V
    .locals 20
    .param p0, "appRect"    # Landroid/graphics/Rect;
    .param p1, "thumbnailRect"    # Landroid/graphics/Rect;
    .param p2, "curSpec"    # Landroid/graphics/Matrix;
    .param p3, "alpha"    # F
    .param p4, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p5, "leash"    # Landroid/view/SurfaceControl;

    .line 1039
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    if-eqz v1, :cond_1

    if-eqz v9, :cond_1

    if-nez v8, :cond_0

    move/from16 v2, p3

    goto/16 :goto_0

    .line 1044
    :cond_0
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    move-object v10, v2

    .line 1045
    .local v10, "tmpMatrix":Landroid/graphics/Matrix;
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v10, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1046
    const/16 v2, 0x9

    new-array v11, v2, [F

    .line 1047
    .local v11, "tmp":[F
    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1049
    const/4 v2, 0x0

    aget v12, v11, v2

    .line 1050
    .local v12, "curScaleX":F
    const/4 v2, 0x4

    aget v13, v11, v2

    .line 1051
    .local v13, "curScaleY":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float v14, v2, v12

    .line 1052
    .local v14, "curWidth":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    mul-float v15, v2, v13

    .line 1053
    .local v15, "curHeight":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v16, v14, v2

    .line 1055
    .local v16, "newScaleX":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v17, v15, v2

    .line 1057
    .local v17, "newScaleY":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float v18, v16, v2

    .line 1060
    .local v18, "curThumbnailHeight":F
    const/4 v2, 0x2

    aget v2, v11, v2

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    mul-float v3, v3, v16

    sub-float v7, v2, v3

    .line 1062
    .local v7, "curTranslateX":F
    const/4 v2, 0x5

    aget v2, v11, v2

    sget-object v3, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    mul-float v3, v3, v17

    sub-float/2addr v2, v3

    sub-float v3, v15, v18

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float v6, v2, v3

    .line 1065
    .local v6, "curTranslateY":F
    const/4 v2, 0x3

    aget v5, v11, v2

    const/4 v2, 0x1

    aget v19, v11, v2

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move/from16 v4, v16

    move v0, v6

    .end local v6    # "curTranslateY":F
    .local v0, "curTranslateY":F
    move/from16 v6, v19

    move v1, v7

    .end local v7    # "curTranslateX":F
    .local v1, "curTranslateX":F
    move/from16 v7, v16

    invoke-virtual/range {v2 .. v7}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    .line 1066
    invoke-virtual {v8, v9, v1, v0}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 1067
    move/from16 v2, p3

    invoke-virtual {v8, v9, v2}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 1068
    return-void

    .line 1039
    .end local v0    # "curTranslateY":F
    .end local v1    # "curTranslateX":F
    .end local v10    # "tmpMatrix":Landroid/graphics/Matrix;
    .end local v11    # "tmp":[F
    .end local v12    # "curScaleX":F
    .end local v13    # "curScaleY":F
    .end local v14    # "curWidth":F
    .end local v15    # "curHeight":F
    .end local v16    # "newScaleX":F
    .end local v17    # "newScaleY":F
    .end local v18    # "curThumbnailHeight":F
    :cond_1
    move/from16 v2, p3

    .line 1041
    :goto_0
    return-void
.end method

.method static calculateMiuiActivityThumbnailSpec(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Matrix;FFLandroid/view/SurfaceControl$Transaction;Landroid/view/SurfaceControl;)V
    .locals 14
    .param p0, "appRect"    # Landroid/graphics/Rect;
    .param p1, "thumbnailRect"    # Landroid/graphics/Rect;
    .param p2, "curSpec"    # Landroid/graphics/Matrix;
    .param p3, "alpha"    # F
    .param p4, "radius"    # F
    .param p5, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p6, "leash"    # Landroid/view/SurfaceControl;

    .line 1008
    move-object/from16 v0, p2

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    if-nez v1, :cond_0

    move/from16 v6, p3

    goto :goto_0

    .line 1012
    :cond_0
    const/16 v3, 0x9

    new-array v3, v3, [F

    .line 1013
    .local v3, "tmp":[F
    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1014
    const/4 v4, 0x2

    aget v4, v3, v4

    .line 1015
    .local v4, "curTranslateX":F
    const/4 v5, 0x5

    aget v5, v3, v5

    .line 1016
    .local v5, "curTranslateY":F
    const/4 v6, 0x0

    aget v7, v3, v6

    .line 1017
    .local v7, "curScaleX":F
    const/4 v8, 0x4

    aget v8, v3, v8

    .line 1018
    .local v8, "curScaleY":F
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v7

    .line 1019
    .local v9, "curWidth":F
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v10, v8

    .line 1020
    .local v10, "curHeight":F
    new-instance v11, Landroid/graphics/Rect;

    float-to-int v12, v9

    float-to-int v13, v10

    invoke-direct {v11, v6, v6, v12, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2, v11}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    .line 1021
    invoke-virtual {v1, v2, v4, v5}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 1022
    move/from16 v6, p3

    invoke-virtual {v1, v2, v6}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 1023
    return-void

    .line 1008
    .end local v3    # "tmp":[F
    .end local v4    # "curTranslateX":F
    .end local v5    # "curTranslateY":F
    .end local v7    # "curScaleX":F
    .end local v8    # "curScaleY":F
    .end local v9    # "curWidth":F
    .end local v10    # "curHeight":F
    :cond_1
    move/from16 v6, p3

    .line 1010
    :goto_0
    return-void
.end method

.method static calculateMiuiThumbnailSpec(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Matrix;FLandroid/view/SurfaceControl$Transaction;Landroid/view/SurfaceControl;)V
    .locals 17
    .param p0, "appRect"    # Landroid/graphics/Rect;
    .param p1, "thumbnailRect"    # Landroid/graphics/Rect;
    .param p2, "curSpec"    # Landroid/graphics/Matrix;
    .param p3, "alpha"    # F
    .param p4, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p5, "leash"    # Landroid/view/SurfaceControl;

    .line 978
    move-object/from16 v0, p2

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    if-eqz v8, :cond_1

    if-nez v7, :cond_0

    move/from16 v1, p3

    goto/16 :goto_0

    .line 983
    :cond_0
    const/16 v1, 0x9

    new-array v9, v1, [F

    .line 984
    .local v9, "tmp":[F
    invoke-virtual {v0, v9}, Landroid/graphics/Matrix;->getValues([F)V

    .line 986
    const/4 v1, 0x0

    aget v10, v9, v1

    .line 987
    .local v10, "curScaleX":F
    const/4 v1, 0x4

    aget v11, v9, v1

    .line 988
    .local v11, "curScaleY":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    mul-float v12, v1, v10

    .line 989
    .local v12, "curWidth":F
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    mul-float v13, v1, v11

    .line 990
    .local v13, "curHeight":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sget-object v2, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    sget-object v2, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float v14, v12, v1

    .line 992
    .local v14, "newScaleX":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v2, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    sget-object v2, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float v15, v13, v1

    .line 995
    .local v15, "newScaleY":F
    const/4 v1, 0x2

    aget v1, v9, v1

    sget-object v2, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    mul-float/2addr v2, v14

    sub-float v6, v1, v2

    .line 997
    .local v6, "curTranslateX":F
    const/4 v1, 0x5

    aget v1, v9, v1

    sget-object v2, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    mul-float/2addr v2, v15

    sub-float v5, v1, v2

    .line 1000
    .local v5, "curTranslateY":F
    const/4 v1, 0x3

    aget v4, v9, v1

    const/4 v1, 0x1

    aget v16, v9, v1

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move v3, v14

    move v0, v5

    .end local v5    # "curTranslateY":F
    .local v0, "curTranslateY":F
    move/from16 v5, v16

    move-object/from16 v16, v9

    move v9, v6

    .end local v6    # "curTranslateX":F
    .local v9, "curTranslateX":F
    .local v16, "tmp":[F
    move v6, v15

    invoke-virtual/range {v1 .. v6}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    .line 1001
    invoke-virtual {v7, v8, v9, v0}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 1002
    move/from16 v1, p3

    invoke-virtual {v7, v8, v1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 1003
    return-void

    .line 978
    .end local v0    # "curTranslateY":F
    .end local v9    # "curTranslateX":F
    .end local v10    # "curScaleX":F
    .end local v11    # "curScaleY":F
    .end local v12    # "curWidth":F
    .end local v13    # "curHeight":F
    .end local v14    # "newScaleX":F
    .end local v15    # "newScaleY":F
    .end local v16    # "tmp":[F
    :cond_1
    move/from16 v1, p3

    .line 980
    :goto_0
    return-void
.end method

.method static calculateScaleUpDownThumbnailSpec(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Matrix;Landroid/view/SurfaceControl$Transaction;Landroid/view/SurfaceControl;)V
    .locals 2
    .param p0, "appClipRect"    # Landroid/graphics/Rect;
    .param p1, "thumbnailRect"    # Landroid/graphics/Rect;
    .param p2, "curSpec"    # Landroid/graphics/Matrix;
    .param p3, "t"    # Landroid/view/SurfaceControl$Transaction;
    .param p4, "leash"    # Landroid/view/SurfaceControl;

    .line 1027
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-eqz p4, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    .line 1031
    :cond_0
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 1032
    .local v0, "tmp":[F
    invoke-virtual {p3, p4, p2, v0}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;Landroid/graphics/Matrix;[F)Landroid/view/SurfaceControl$Transaction;

    .line 1033
    invoke-virtual {p3, p4, p0}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;

    .line 1034
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p3, p4, v1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;

    .line 1035
    return-void

    .line 1029
    .end local v0    # "tmp":[F
    :cond_1
    :goto_0
    return-void
.end method

.method static createActivityOpenCloseTransition(ZLandroid/graphics/Rect;ZFLcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;
    .locals 30
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "isOpenOrClose"    # Z
    .param p3, "freeformScale"    # F
    .param p4, "container"    # Lcom/android/server/wm/WindowContainer;

    .line 780
    move/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v12, p4

    new-instance v3, Landroid/view/animation/AnimationSet;

    const/4 v13, 0x1

    invoke-direct {v3, v13}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v14, v3

    .line 781
    .local v14, "set":Landroid/view/animation/AnimationSet;
    const/4 v15, 0x0

    .line 782
    .local v15, "translateAnimation":Landroid/view/animation/Animation;
    const/16 v16, 0x0

    .line 783
    .local v16, "alphaAnimation":Landroid/view/animation/Animation;
    const/16 v17, 0x0

    .line 784
    .local v17, "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    const/4 v11, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    const/high16 v5, 0x3f800000    # 1.0f

    if-eqz v2, :cond_5

    .line 785
    if-eqz v0, :cond_0

    .line 786
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/16 v19, 0x0

    move-object/from16 v3, v18

    move/from16 v5, p3

    move/from16 v11, v19

    invoke-direct/range {v3 .. v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 790
    .end local v15    # "translateAnimation":Landroid/view/animation/Animation;
    .local v3, "translateAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    goto/16 :goto_0

    .line 794
    .end local v3    # "translateAnimation":Landroid/view/animation/Animation;
    .restart local v15    # "translateAnimation":Landroid/view/animation/Animation;
    :cond_0
    new-instance v6, Landroid/view/animation/TranslateAnimation;

    const/16 v22, 0x1

    const/16 v23, 0x0

    const/16 v24, 0x1

    const/high16 v25, -0x41800000    # -0.25f

    const/16 v26, 0x1

    const/16 v27, 0x0

    const/16 v28, 0x1

    const/16 v29, 0x0

    move-object/from16 v21, v6

    invoke-direct/range {v21 .. v29}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 798
    .end local v15    # "translateAnimation":Landroid/view/animation/Animation;
    .local v6, "translateAnimation":Landroid/view/animation/Animation;
    cmpg-float v7, p3, v5

    if-gez v7, :cond_1

    .line 799
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 800
    .end local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    .local v3, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v16, v3

    move-object v3, v6

    goto/16 :goto_0

    .line 801
    .end local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    .restart local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    :cond_1
    if-nez v12, :cond_2

    .line 802
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 803
    .end local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    .restart local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v16, v3

    move-object v3, v6

    goto/16 :goto_0

    .line 804
    .end local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    .restart local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    :cond_2
    iget-boolean v5, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z

    if-eqz v5, :cond_3

    .line 805
    new-instance v5, Lcom/android/server/wm/AnimationDimmer;

    invoke-direct {v5, v3, v4, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v3, v5

    .line 806
    .end local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .local v3, "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 807
    invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setTaskAnimationLevel(Z)V

    .line 808
    const/4 v11, 0x0

    iput-boolean v11, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z

    move-object/from16 v17, v3

    move-object v3, v6

    goto/16 :goto_0

    .line 810
    .end local v3    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .restart local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    :cond_3
    const/4 v11, 0x0

    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/WindowContainer;->fillsParent()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 811
    new-instance v5, Lcom/android/server/wm/AnimationDimmer;

    invoke-direct {v5, v3, v4, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v3, v5

    .line 812
    .end local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .restart local v3    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v17, v3

    move-object v3, v6

    goto/16 :goto_0

    .line 810
    .end local v3    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .restart local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    :cond_4
    move-object v3, v6

    goto/16 :goto_0

    .line 817
    .end local v6    # "translateAnimation":Landroid/view/animation/Animation;
    .restart local v15    # "translateAnimation":Landroid/view/animation/Animation;
    :cond_5
    if-eqz v0, :cond_a

    .line 818
    new-instance v6, Landroid/view/animation/TranslateAnimation;

    const/16 v19, 0x1

    const/high16 v20, -0x41800000    # -0.25f

    const/16 v21, 0x1

    const/16 v22, 0x0

    const/16 v23, 0x1

    const/16 v24, 0x0

    const/16 v25, 0x1

    const/16 v26, 0x0

    move-object/from16 v18, v6

    invoke-direct/range {v18 .. v26}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 822
    .end local v15    # "translateAnimation":Landroid/view/animation/Animation;
    .restart local v6    # "translateAnimation":Landroid/view/animation/Animation;
    cmpg-float v7, p3, v5

    if-gez v7, :cond_6

    .line 823
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v5, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 824
    .end local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    .local v3, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v16, v3

    move-object v3, v6

    goto :goto_0

    .line 825
    .end local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    .restart local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    :cond_6
    if-nez v12, :cond_7

    .line 826
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 827
    .end local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    .restart local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v16, v3

    move-object v3, v6

    goto :goto_0

    .line 828
    .end local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    .restart local v16    # "alphaAnimation":Landroid/view/animation/Animation;
    :cond_7
    iget-boolean v5, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z

    if-eqz v5, :cond_8

    .line 829
    new-instance v5, Lcom/android/server/wm/AnimationDimmer;

    invoke-direct {v5, v4, v3, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v3, v5

    .line 830
    .end local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .local v3, "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 831
    invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setTaskAnimationLevel(Z)V

    .line 832
    iput-boolean v11, v12, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z

    move-object/from16 v17, v3

    move-object v3, v6

    goto :goto_0

    .line 834
    .end local v3    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .restart local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    :cond_8
    invoke-virtual/range {p4 .. p4}, Lcom/android/server/wm/WindowContainer;->fillsParent()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 835
    new-instance v5, Lcom/android/server/wm/AnimationDimmer;

    invoke-direct {v5, v4, v3, v1, v12}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v3, v5

    .line 836
    .end local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .restart local v3    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v17, v3

    move-object v3, v6

    goto :goto_0

    .line 834
    .end local v3    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    .restart local v17    # "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    :cond_9
    move-object v3, v6

    goto :goto_0

    .line 840
    .end local v6    # "translateAnimation":Landroid/view/animation/Animation;
    .restart local v15    # "translateAnimation":Landroid/view/animation/Animation;
    :cond_a
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/16 v19, 0x0

    move-object/from16 v3, v18

    move/from16 v7, p3

    move/from16 v11, v19

    invoke-direct/range {v3 .. v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 844
    .end local v15    # "translateAnimation":Landroid/view/animation/Animation;
    .local v3, "translateAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 847
    :goto_0
    new-instance v4, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;

    const v5, 0x3f4ccccd    # 0.8f

    const v6, 0x3f733333    # 0.95f

    invoke-direct {v4, v5, v6}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V

    sput-object v4, Lcom/android/server/wm/AppTransitionInjector;->sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

    .line 848
    invoke-virtual {v14, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 849
    sget-object v4, Lcom/android/server/wm/AppTransitionInjector;->sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v14, v4}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 850
    const-wide/16 v4, 0x1f4

    invoke-virtual {v14, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 851
    invoke-virtual {v14, v13}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V

    .line 852
    if-eq v2, v0, :cond_b

    .line 853
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V

    .line 855
    :cond_b
    return-object v14
.end method

.method static createActivityOpenCloseTransitionForDimmer(ZLandroid/graphics/Rect;ZLcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;
    .locals 18
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "isOpenOrClose"    # Z
    .param p3, "container"    # Lcom/android/server/wm/WindowContainer;
    .param p4, "service"    # Lcom/android/server/wm/WindowManagerService;

    .line 861
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    new-instance v2, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 862
    .local v2, "set":Landroid/view/animation/AnimationSet;
    const/4 v4, 0x0

    .line 863
    .local v4, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    const/4 v5, 0x0

    .line 864
    .local v5, "animationDimmer":Lcom/android/server/wm/AnimationDimmer;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {v0, v8, v8, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 865
    const/4 v6, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    if-eqz p2, :cond_2

    .line 866
    if-eqz p0, :cond_5

    .line 867
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    const/4 v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v8, v17

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object/from16 v4, v17

    .line 871
    instance-of v8, v1, Lcom/android/server/wm/ActivityRecord;

    if-eqz v8, :cond_0

    .line 872
    new-instance v8, Lcom/android/server/wm/AnimationDimmer;

    move-object v9, v1

    check-cast v9, Lcom/android/server/wm/ActivityRecord;

    .line 873
    invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v9

    invoke-direct {v8, v6, v7, v0, v9}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v5, v8

    .line 875
    :cond_0
    instance-of v8, v1, Lcom/android/server/wm/Task;

    if-eqz v8, :cond_1

    .line 876
    new-instance v8, Lcom/android/server/wm/AnimationDimmer;

    invoke-direct {v8, v6, v7, v0, v1}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v5, v8

    .line 879
    :cond_1
    invoke-virtual {v4}, Landroid/view/animation/TranslateAnimation;->getZAdjustment()I

    move-result v6

    sub-int/2addr v6, v3

    invoke-virtual {v5, v6}, Lcom/android/server/wm/AnimationDimmer;->setZAdjustment(I)V

    .line 880
    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 881
    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 884
    :cond_2
    if-nez p0, :cond_5

    .line 885
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v8, v17

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object/from16 v4, v17

    .line 889
    instance-of v8, v1, Lcom/android/server/wm/ActivityRecord;

    if-eqz v8, :cond_3

    .line 890
    new-instance v8, Lcom/android/server/wm/AnimationDimmer;

    move-object v9, v1

    check-cast v9, Lcom/android/server/wm/ActivityRecord;

    .line 891
    invoke-virtual {v9}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v9

    invoke-direct {v8, v7, v6, v0, v9}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v5, v8

    .line 893
    :cond_3
    instance-of v8, v1, Lcom/android/server/wm/Task;

    if-eqz v8, :cond_4

    .line 894
    new-instance v8, Lcom/android/server/wm/AnimationDimmer;

    invoke-direct {v8, v7, v6, v0, v1}, Lcom/android/server/wm/AnimationDimmer;-><init>(FFLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)V

    move-object v5, v8

    .line 897
    :cond_4
    invoke-virtual {v4}, Landroid/view/animation/TranslateAnimation;->getZAdjustment()I

    move-result v6

    sub-int/2addr v6, v3

    invoke-virtual {v5, v6}, Lcom/android/server/wm/AnimationDimmer;->setZAdjustment(I)V

    .line 898
    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 899
    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 902
    :cond_5
    :goto_0
    new-instance v6, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;

    const v7, 0x3f4ccccd    # 0.8f

    const v8, 0x3f733333    # 0.95f

    invoke-direct {v6, v7, v8}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V

    sput-object v6, Lcom/android/server/wm/AppTransitionInjector;->sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

    .line 903
    invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 904
    const-wide/16 v6, 0x1f4

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 905
    invoke-virtual {v2, v3}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V

    .line 906
    return-object v2
.end method

.method static createBackActivityFromRoundedViewAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;F)Landroid/view/animation/Animation;
    .locals 12
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "targetPositionRect"    # Landroid/graphics/Rect;
    .param p3, "radius"    # F

    .line 294
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 296
    .local v0, "positionRect":Landroid/graphics/Rect;
    const-wide/16 v1, 0xc8

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz p0, :cond_0

    .line 297
    new-instance v4, Landroid/view/animation/AnimationSet;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 298
    .local v4, "set":Landroid/view/animation/AnimationSet;
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    const v6, 0x3f4ccccd    # 0.8f

    invoke-direct {v5, v6, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v3, v5

    .line 300
    .local v3, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v4, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 301
    sget-object v5, Lcom/android/server/wm/AppTransitionInjector;->SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 302
    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 303
    invoke-virtual {v3, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 304
    move-object v1, v4

    .line 305
    .end local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v4    # "set":Landroid/view/animation/AnimationSet;
    .local v1, "anim":Landroid/view/animation/Animation;
    goto :goto_0

    .line 306
    .end local v1    # "anim":Landroid/view/animation/Animation;
    :cond_0
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 308
    .local v4, "alphaAnimation":Landroid/view/animation/Animation;
    const-wide/16 v5, 0x64

    invoke-virtual {v4, v5, v6}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 309
    new-instance v5, Lcom/android/server/wm/RadiusAnimation;

    sget v6, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    int-to-float v6, v6

    invoke-direct {v5, v6, p3}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V

    .line 310
    .local v5, "radiusAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    .line 311
    .local v6, "scaleX":F
    new-instance v7, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;

    iget v8, v0, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    sub-float v9, v3, v6

    div-float/2addr v8, v9

    invoke-direct {v7, v3, v6, v8}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;-><init>(FFF)V

    .line 313
    .local v7, "scaleXAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    .line 314
    .local v8, "scaleY":F
    new-instance v9, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;

    iget v10, v0, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    sub-float v11, v3, v8

    div-float/2addr v10, v11

    invoke-direct {v9, v3, v8, v10}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;-><init>(FFF)V

    move-object v3, v9

    .line 316
    .local v3, "scaleYAnimation":Landroid/view/animation/Animation;
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 317
    .local v9, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v9, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 318
    invoke-virtual {v9, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 319
    invoke-virtual {v9, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 320
    invoke-virtual {v9, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 322
    sget-object v11, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 323
    sget-object v11, Lcom/android/server/wm/AppTransitionInjector;->SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v5, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 324
    invoke-virtual {v7, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 325
    invoke-virtual {v3, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 327
    invoke-virtual {v9, v10}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 329
    invoke-virtual {v4, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 330
    const-wide/16 v1, 0x1c2

    invoke-virtual {v5, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 331
    invoke-virtual {v7, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 332
    invoke-virtual {v3, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 334
    move-object v1, v9

    .line 336
    .end local v3    # "scaleYAnimation":Landroid/view/animation/Animation;
    .end local v4    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v5    # "radiusAnimation":Landroid/view/animation/Animation;
    .end local v6    # "scaleX":F
    .end local v7    # "scaleXAnimation":Landroid/view/animation/Animation;
    .end local v8    # "scaleY":F
    .end local v9    # "set":Landroid/view/animation/AnimationSet;
    .restart local v1    # "anim":Landroid/view/animation/Animation;
    :goto_0
    return-object v1
.end method

.method static createBackActivityScaledToScreenCenter(ZLandroid/graphics/Rect;)Landroid/view/animation/Animation;
    .locals 14
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;

    .line 341
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 342
    .local v0, "positionRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 343
    .local v1, "scaleWith":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    float-to-int v2, v3

    .line 344
    .local v2, "scaleHeight":I
    iget v3, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 345
    iget v3, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 346
    iget v3, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v4, v1

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/graphics/Rect;->right:I

    .line 347
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    .line 349
    const-wide/16 v3, 0x1c2

    const/4 v5, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    if-eqz p0, :cond_0

    .line 350
    new-instance v7, Landroid/view/animation/AnimationSet;

    invoke-direct {v7, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v5, v7

    .line 351
    .local v5, "set":Landroid/view/animation/AnimationSet;
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const v8, 0x3f4ccccd    # 0.8f

    invoke-direct {v7, v8, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v6, v7

    .line 353
    .local v6, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 354
    sget-object v7, Lcom/android/server/wm/AppTransitionInjector;->SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v5, v7}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 355
    const/4 v7, -0x1

    invoke-virtual {v5, v7}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 356
    invoke-virtual {v5, v3, v4}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 357
    move-object v3, v5

    .line 358
    .end local v5    # "set":Landroid/view/animation/AnimationSet;
    .end local v6    # "alphaAnimation":Landroid/view/animation/Animation;
    .local v3, "anim":Landroid/view/animation/Animation;
    goto :goto_0

    .line 359
    .end local v3    # "anim":Landroid/view/animation/Animation;
    :cond_0
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v8, 0x0

    invoke-direct {v7, v6, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 361
    .local v7, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    .line 362
    .local v8, "scaleX":F
    new-instance v9, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;

    iget v10, v0, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    sub-float v11, v6, v8

    div-float/2addr v10, v11

    invoke-direct {v9, v6, v8, v10}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;-><init>(FFF)V

    .line 364
    .local v9, "scaleXAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v10, v11

    .line 365
    .local v10, "scaleY":F
    new-instance v11, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;

    iget v12, v0, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    sub-float v13, v6, v10

    div-float/2addr v12, v13

    invoke-direct {v11, v6, v10, v12}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;-><init>(FFF)V

    move-object v6, v11

    .line 367
    .local v6, "scaleYAnimation":Landroid/view/animation/Animation;
    new-instance v11, Landroid/view/animation/AnimationSet;

    invoke-direct {v11, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 368
    .local v11, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v11, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 369
    invoke-virtual {v11, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 370
    invoke-virtual {v11, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 371
    sget-object v12, Lcom/android/server/wm/AppTransitionInjector;->SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v11, v12}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 372
    invoke-virtual {v11, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 373
    invoke-virtual {v11, v3, v4}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 374
    move-object v3, v11

    .line 376
    .end local v6    # "scaleYAnimation":Landroid/view/animation/Animation;
    .end local v7    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v8    # "scaleX":F
    .end local v9    # "scaleXAnimation":Landroid/view/animation/Animation;
    .end local v10    # "scaleY":F
    .end local v11    # "set":Landroid/view/animation/AnimationSet;
    .restart local v3    # "anim":Landroid/view/animation/Animation;
    :goto_0
    return-object v3
.end method

.method static createDummyAnimation(F)Landroid/view/animation/Animation;
    .locals 3
    .param p0, "alpha"    # F

    .line 718
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 719
    .local v0, "dummyAnimation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 720
    return-object v0
.end method

.method static createFloatWindowShowHideAnimation(ZLandroid/graphics/Rect;Lcom/android/server/wm/MiuiFreeFormActivityStack;)Landroid/view/animation/Animation;
    .locals 25
    .param p0, "enter"    # Z
    .param p1, "frame"    # Landroid/graphics/Rect;
    .param p2, "freemAs"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 527
    move/from16 v0, p0

    move-object/from16 v1, p2

    const/4 v2, 0x0

    .line 528
    .local v2, "set":Landroid/view/animation/AnimationSet;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createFloatWindowShowHideAnimation enter = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AppTransitionInjector"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    const/high16 v3, 0x3f800000    # 1.0f

    .line 530
    .local v3, "freeFormScale":F
    const/4 v4, 0x0

    .line 531
    .local v4, "needSetRoundedCorners":Z
    if-eqz v1, :cond_0

    .line 532
    const/4 v4, 0x1

    .line 533
    iget v3, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 535
    :cond_0
    new-instance v5, Landroid/view/animation/AnimationSet;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v2, v5

    .line 536
    const/4 v5, 0x1

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, 0x42a00000    # 80.0f

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    if-eqz v0, :cond_4

    .line 537
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v10, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 538
    .local v10, "alphaAnimation":Landroid/view/animation/Animation;
    const-wide/16 v11, 0x15e

    invoke-virtual {v10, v11, v12}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 539
    new-instance v13, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v13, v9}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v10, v13}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 540
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v13

    int-to-float v13, v13

    mul-float/2addr v13, v3

    .line 541
    .local v13, "windowHeight":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v3

    .line 542
    .local v14, "windowWidth":F
    cmpl-float v15, v13, v14

    if-lez v15, :cond_1

    move v15, v13

    goto :goto_0

    :cond_1
    move v15, v14

    .line 543
    .local v15, "maxSide":F
    :goto_0
    cmpl-float v16, v15, v7

    if-lez v16, :cond_2

    sub-float v7, v15, v7

    div-float v8, v7, v15

    :cond_2
    move/from16 v17, v8

    .line 544
    .local v17, "scaleEnd":F
    new-instance v7, Landroid/view/animation/ScaleAnimation;

    const/high16 v18, 0x3f800000    # 1.0f

    const/high16 v20, 0x3f800000    # 1.0f

    const/16 v21, 0x0

    div-float v22, v14, v6

    const/16 v23, 0x0

    div-float v24, v13, v6

    move-object/from16 v16, v7

    move/from16 v19, v17

    invoke-direct/range {v16 .. v24}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v6, v7

    .line 546
    .local v6, "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v6, v11, v12}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 547
    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7, v9}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v6, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 548
    invoke-virtual {v2, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 549
    invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 550
    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 551
    if-eqz v4, :cond_3

    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V

    .line 552
    .end local v6    # "scaleAnimation":Landroid/view/animation/Animation;
    .end local v10    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v13    # "windowHeight":F
    .end local v14    # "windowWidth":F
    .end local v15    # "maxSide":F
    .end local v17    # "scaleEnd":F
    :cond_3
    goto :goto_2

    .line 553
    :cond_4
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v10, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v9, v10

    .line 554
    .local v9, "alphaAnimation":Landroid/view/animation/Animation;
    const-wide/16 v10, 0xc8

    invoke-virtual {v9, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 555
    new-instance v12, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v13, 0x3fc00000    # 1.5f

    invoke-direct {v12, v13}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v9, v12}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 556
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v12

    int-to-float v12, v12

    mul-float/2addr v12, v3

    .line 557
    .local v12, "windowHeight":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v3

    .line 558
    .restart local v14    # "windowWidth":F
    cmpl-float v15, v12, v14

    if-lez v15, :cond_5

    move v15, v12

    goto :goto_1

    :cond_5
    move v15, v14

    .line 559
    .restart local v15    # "maxSide":F
    :goto_1
    cmpl-float v16, v15, v7

    if-lez v16, :cond_6

    sub-float v7, v15, v7

    div-float v8, v7, v15

    :cond_6
    move/from16 v18, v8

    .line 560
    .local v18, "scaleEnd":F
    new-instance v7, Landroid/view/animation/ScaleAnimation;

    const/high16 v17, 0x3f800000    # 1.0f

    const/high16 v19, 0x3f800000    # 1.0f

    const/16 v21, 0x0

    div-float v22, v14, v6

    const/16 v23, 0x0

    div-float v24, v12, v6

    move-object/from16 v16, v7

    move/from16 v20, v18

    invoke-direct/range {v16 .. v24}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v6, v7

    .line 562
    .restart local v6    # "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v6, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 563
    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7, v13}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v6, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 564
    invoke-virtual {v2, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 565
    invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 566
    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 567
    if-eqz v4, :cond_7

    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V

    .line 569
    .end local v6    # "scaleAnimation":Landroid/view/animation/Animation;
    .end local v9    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v12    # "windowHeight":F
    .end local v14    # "windowWidth":F
    .end local v15    # "maxSide":F
    .end local v18    # "scaleEnd":F
    :cond_7
    :goto_2
    return-object v2
.end method

.method static createFreeFormActivityOpenAndExitAnimation(Z)Landroid/view/animation/Animation;
    .locals 4
    .param p0, "enter"    # Z

    .line 470
    const/4 v0, 0x0

    .line 471
    .local v0, "translateAnimation":Landroid/view/animation/Animation;
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz p0, :cond_0

    .line 472
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v0, v3

    goto :goto_0

    .line 474
    :cond_0
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v0, v3

    .line 476
    :goto_0
    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 477
    return-object v0
.end method

.method static createFreeFormAppOpenAndExitAnimation(Lcom/android/server/wm/WindowManagerService;ZLandroid/graphics/Rect;Lcom/android/server/wm/MiuiFreeFormActivityStack;)Landroid/view/animation/Animation;
    .locals 21
    .param p0, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p1, "enter"    # Z
    .param p2, "frame"    # Landroid/graphics/Rect;
    .param p3, "freemAs"    # Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 481
    move-object/from16 v0, p3

    const/4 v1, 0x0

    .line 482
    .local v1, "set":Landroid/view/animation/AnimationSet;
    if-eqz v0, :cond_1

    .line 483
    new-instance v2, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v1, v2

    .line 484
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x40000000    # 2.0f

    const v6, 0x3f333333    # 0.7f

    const v7, 0x3f59999a    # 0.85f

    const-wide/16 v8, 0x1f4

    if-eqz p1, :cond_0

    .line 485
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v10, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v3, v10

    .line 486
    .local v3, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 487
    new-instance v4, Lcom/android/server/wm/PhysicBasedInterpolator;

    invoke-direct {v4, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 488
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    iget v10, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    mul-float/2addr v4, v10

    .line 489
    .local v4, "windowHeight":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    iget v11, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    mul-float/2addr v10, v11

    .line 490
    .local v10, "windowWidth":F
    new-instance v20, Landroid/view/animation/ScaleAnimation;

    const v12, 0x3f19999a    # 0.6f

    const/high16 v13, 0x3f800000    # 1.0f

    const v14, 0x3f19999a    # 0.6f

    const/high16 v15, 0x3f800000    # 1.0f

    const/16 v16, 0x0

    div-float v17, v10, v5

    const/16 v18, 0x0

    div-float v19, v4, v5

    move-object/from16 v11, v20

    invoke-direct/range {v11 .. v19}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v5, v20

    .line 492
    .local v5, "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v5, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 493
    new-instance v8, Lcom/android/server/wm/PhysicBasedInterpolator;

    invoke-direct {v8, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    invoke-virtual {v5, v8}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 494
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 495
    invoke-virtual {v1, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 496
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 497
    .end local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v4    # "windowHeight":F
    .end local v5    # "scaleAnimation":Landroid/view/animation/Animation;
    .end local v10    # "windowWidth":F
    goto :goto_0

    .line 498
    :cond_0
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v10, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v3, v10

    .line 499
    .restart local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 500
    new-instance v4, Lcom/android/server/wm/PhysicBasedInterpolator;

    invoke-direct {v4, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 501
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    iget v10, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    mul-float/2addr v4, v10

    .line 502
    .restart local v4    # "windowHeight":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    iget v11, v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    mul-float/2addr v10, v11

    .line 503
    .restart local v10    # "windowWidth":F
    new-instance v20, Landroid/view/animation/ScaleAnimation;

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, 0x3f19999a    # 0.6f

    const/high16 v14, 0x3f800000    # 1.0f

    const v15, 0x3f19999a    # 0.6f

    const/16 v16, 0x0

    div-float v17, v10, v5

    const/16 v18, 0x0

    div-float v19, v4, v5

    move-object/from16 v11, v20

    invoke-direct/range {v11 .. v19}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v5, v20

    .line 505
    .restart local v5    # "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v5, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 506
    new-instance v8, Lcom/android/server/wm/PhysicBasedInterpolator;

    invoke-direct {v8, v7, v6}, Lcom/android/server/wm/PhysicBasedInterpolator;-><init>(FF)V

    invoke-virtual {v5, v8}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 507
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 508
    invoke-virtual {v1, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 509
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 512
    .end local v3    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v4    # "windowHeight":F
    .end local v5    # "scaleAnimation":Landroid/view/animation/Animation;
    .end local v10    # "windowWidth":F
    :cond_1
    :goto_0
    return-object v1
.end method

.method static createLaunchActivityFromRoundedViewAnimation(IZLandroid/graphics/Rect;Landroid/graphics/Rect;F)Landroid/view/animation/Animation;
    .locals 23
    .param p0, "transit"    # I
    .param p1, "enter"    # Z
    .param p2, "appFrame"    # Landroid/graphics/Rect;
    .param p3, "positionRect"    # Landroid/graphics/Rect;
    .param p4, "radius"    # F

    .line 244
    move-object/from16 v0, p3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 245
    .local v1, "appWidth":I
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 246
    .local v2, "appHeight":I
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 247
    .local v3, "startX":I
    iget v4, v0, Landroid/graphics/Rect;->top:I

    .line 248
    .local v4, "startY":I
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 249
    .local v5, "startWidth":I
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v6

    .line 250
    .local v6, "startHeight":I
    const/4 v8, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    if-eqz p1, :cond_0

    .line 251
    new-instance v12, Landroid/view/animation/AlphaAnimation;

    const/4 v13, 0x0

    invoke-direct {v12, v13, v11}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 253
    .local v12, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v13, Lcom/android/server/wm/RadiusAnimation;

    sget v14, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    int-to-float v14, v14

    move/from16 v15, p4

    invoke-direct {v13, v15, v14}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V

    .line 254
    .local v13, "radiusAnimation":Landroid/view/animation/Animation;
    int-to-float v14, v5

    int-to-float v9, v1

    div-float/2addr v14, v9

    .line 255
    .local v14, "scaleX":F
    int-to-float v9, v6

    int-to-float v10, v2

    div-float/2addr v9, v10

    .line 256
    .local v9, "scaleY":F
    new-instance v10, Landroid/view/animation/ScaleAnimation;

    const/high16 v18, 0x3f800000    # 1.0f

    const/high16 v20, 0x3f800000    # 1.0f

    int-to-float v7, v3

    sub-float v16, v11, v14

    div-float v21, v7, v16

    int-to-float v7, v4

    sub-float/2addr v11, v9

    div-float v22, v7, v11

    move-object/from16 v16, v10

    move/from16 v17, v14

    move/from16 v19, v9

    invoke-direct/range {v16 .. v22}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    move-object v7, v10

    .line 260
    .local v7, "scaleAnimation":Landroid/view/animation/ScaleAnimation;
    invoke-virtual {v7, v1, v2, v1, v2}, Landroid/view/animation/ScaleAnimation;->initialize(IIII)V

    .line 261
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v8}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v8, v10

    .line 262
    .local v8, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v8, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 263
    invoke-virtual {v8, v13}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 264
    invoke-virtual {v8, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 266
    sget-object v10, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v12, v10}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 267
    sget-object v10, Lcom/android/server/wm/AppTransitionInjector;->SCALE_UP_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v13, v10}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 268
    invoke-virtual {v7, v10}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 270
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 272
    const-wide/16 v10, 0x64

    invoke-virtual {v12, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 273
    const-wide/16 v10, 0x1f4

    invoke-virtual {v13, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 274
    invoke-virtual {v7, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 277
    move-object v7, v8

    .line 278
    .end local v8    # "set":Landroid/view/animation/AnimationSet;
    .end local v9    # "scaleY":F
    .end local v12    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v13    # "radiusAnimation":Landroid/view/animation/Animation;
    .end local v14    # "scaleX":F
    .local v7, "anim":Landroid/view/animation/Animation;
    goto :goto_0

    .line 279
    .end local v7    # "anim":Landroid/view/animation/Animation;
    :cond_0
    move/from16 v15, p4

    new-instance v7, Landroid/view/animation/AnimationSet;

    invoke-direct {v7, v8}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 280
    .local v7, "set":Landroid/view/animation/AnimationSet;
    new-instance v8, Landroid/view/animation/AlphaAnimation;

    const v9, 0x3f4ccccd    # 0.8f

    invoke-direct {v8, v11, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 282
    .local v8, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v7, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 283
    sget-object v9, Lcom/android/server/wm/AppTransitionInjector;->SCALE_UP_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v8, v9}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 284
    const/4 v9, -0x1

    invoke-virtual {v7, v9}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 285
    const-wide/16 v9, 0x1f4

    invoke-virtual {v8, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 286
    move-object v9, v7

    .line 287
    .local v9, "anim":Landroid/view/animation/Animation;
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 289
    .end local v8    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v9    # "anim":Landroid/view/animation/Animation;
    .local v7, "anim":Landroid/view/animation/Animation;
    :goto_0
    return-object v7
.end method

.method static createScaleDownAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;F)Landroid/view/animation/Animation;
    .locals 8
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "targetPositionRect"    # Landroid/graphics/Rect;
    .param p3, "radius"    # F

    .line 206
    const/high16 v0, 0x3f800000    # 1.0f

    const-wide/16 v1, 0x1c2

    if-eqz p0, :cond_0

    .line 207
    new-instance v3, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 208
    .local v3, "set":Landroid/view/animation/AnimationSet;
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    const v5, 0x3f4ccccd    # 0.8f

    invoke-direct {v4, v5, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v0, v4

    .line 210
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v3, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 211
    sget-object v4, Lcom/android/server/wm/AppTransitionInjector;->SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 212
    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 213
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 214
    move-object v0, v3

    .line 215
    .end local v3    # "set":Landroid/view/animation/AnimationSet;
    .local v0, "anim":Landroid/view/animation/Animation;
    goto :goto_0

    .line 216
    .end local v0    # "anim":Landroid/view/animation/Animation;
    :cond_0
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v0, v3

    .line 218
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    const-wide/16 v3, 0x64

    invoke-virtual {v0, v3, v4}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 219
    new-instance v3, Lcom/android/server/wm/RadiusAnimation;

    sget v4, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    int-to-float v4, v4

    invoke-direct {v3, v4, p3}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V

    .line 220
    .local v3, "radiusAnimation":Landroid/view/animation/Animation;
    new-instance v4, Lcom/android/server/wm/ScaleWithClipAnimation;

    invoke-direct {v4, p1, p2}, Lcom/android/server/wm/ScaleWithClipAnimation;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 221
    .local v4, "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation;
    new-instance v5, Landroid/view/animation/AnimationSet;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 222
    .local v5, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v5, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 223
    invoke-virtual {v5, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 224
    invoke-virtual {v5, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 226
    sget-object v7, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 227
    sget-object v7, Lcom/android/server/wm/AppTransitionInjector;->SCALE_DOWN_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 228
    invoke-virtual {v4, v7}, Lcom/android/server/wm/ScaleWithClipAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 230
    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 232
    const-wide/16 v6, 0xc8

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 233
    invoke-virtual {v3, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 234
    invoke-virtual {v4, v1, v2}, Lcom/android/server/wm/ScaleWithClipAnimation;->setDuration(J)V

    .line 236
    move-object v1, v5

    move-object v0, v1

    .line 238
    .end local v3    # "radiusAnimation":Landroid/view/animation/Animation;
    .end local v4    # "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation;
    .end local v5    # "set":Landroid/view/animation/AnimationSet;
    .local v0, "anim":Landroid/view/animation/Animation;
    :goto_0
    return-object v0
.end method

.method static createScaleUpAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;F)Landroid/view/animation/Animation;
    .locals 9
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "positionRect"    # Landroid/graphics/Rect;
    .param p3, "radius"    # F

    .line 165
    const/4 v0, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const-wide/16 v3, 0x1f4

    if-eqz p0, :cond_0

    .line 166
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    const/4 v6, 0x0

    invoke-direct {v5, v6, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v1, v5

    .line 168
    .local v1, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v5, Lcom/android/server/wm/RadiusAnimation;

    sget v6, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    int-to-float v6, v6

    invoke-direct {v5, p3, v6}, Lcom/android/server/wm/RadiusAnimation;-><init>(FF)V

    .line 170
    .local v5, "radiusAnimation":Landroid/view/animation/Animation;
    new-instance v6, Lcom/android/server/wm/ScaleWithClipAnimation;

    invoke-direct {v6, p2, p1}, Lcom/android/server/wm/ScaleWithClipAnimation;-><init>(Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 172
    .local v6, "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation;
    new-instance v7, Landroid/view/animation/AnimationSet;

    invoke-direct {v7, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v2, v7

    .line 173
    .local v2, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 174
    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 175
    invoke-virtual {v2, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 177
    sget-object v7, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 178
    sget-object v7, Lcom/android/server/wm/AppTransitionInjector;->SCALE_UP_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v5, v7}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 179
    invoke-virtual {v6, v7}, Lcom/android/server/wm/ScaleWithClipAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 181
    invoke-virtual {v2, v0}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 183
    const-wide/16 v7, 0x64

    invoke-virtual {v1, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 184
    invoke-virtual {v5, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 185
    invoke-virtual {v6, v3, v4}, Lcom/android/server/wm/ScaleWithClipAnimation;->setDuration(J)V

    .line 188
    move-object v0, v2

    .line 189
    .end local v1    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v2    # "set":Landroid/view/animation/AnimationSet;
    .end local v5    # "radiusAnimation":Landroid/view/animation/Animation;
    .end local v6    # "scaleWithClipAnimation":Lcom/android/server/wm/ScaleWithClipAnimation;
    .local v0, "anim":Landroid/view/animation/Animation;
    goto :goto_0

    .line 190
    .end local v0    # "anim":Landroid/view/animation/Animation;
    :cond_0
    new-instance v5, Landroid/view/animation/AnimationSet;

    invoke-direct {v5, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v2, v5

    .line 191
    .restart local v2    # "set":Landroid/view/animation/AnimationSet;
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    const v6, 0x3f4ccccd    # 0.8f

    invoke-direct {v5, v1, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v1, v5

    .line 193
    .restart local v1    # "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 194
    sget-object v5, Lcom/android/server/wm/AppTransitionInjector;->SCALE_UP_PHYSIC_BASED_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 195
    const/4 v5, -0x1

    invoke-virtual {v2, v5}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 196
    invoke-virtual {v1, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 197
    move-object v3, v2

    .line 198
    .local v3, "anim":Landroid/view/animation/Animation;
    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    move-object v0, v3

    .line 200
    .end local v1    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v2    # "set":Landroid/view/animation/AnimationSet;
    .end local v3    # "anim":Landroid/view/animation/Animation;
    .restart local v0    # "anim":Landroid/view/animation/Animation;
    :goto_0
    return-object v0
.end method

.method static createTaskOpenCloseTransition(ZLandroid/graphics/Rect;ZLcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;
    .locals 19
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "isOpenOrClose"    # Z
    .param p3, "service"    # Lcom/android/server/wm/WindowManagerService;

    .line 724
    move-object/from16 v0, p3

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 725
    .local v1, "set":Landroid/view/animation/AnimationSet;
    const/4 v3, 0x0

    .line 726
    .local v3, "translateAnimation":Landroid/view/animation/Animation;
    const/4 v4, 0x0

    .line 727
    .local v4, "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    .line 728
    .local v5, "width":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v6

    .line 729
    .local v6, "heigth":I
    const-wide/16 v7, 0x96

    if-eqz p2, :cond_1

    .line 730
    if-eqz p0, :cond_0

    .line 731
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    const/4 v10, 0x1

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v9, v18

    invoke-direct/range {v9 .. v17}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object/from16 v3, v18

    .line 736
    new-instance v16, Landroid/view/animation/ScaleAnimation;

    const v10, 0x3f666666    # 0.9f

    const v12, 0x3f666666    # 0.9f

    const/high16 v13, 0x3f800000    # 1.0f

    div-int/lit8 v9, v5, 0x2

    int-to-float v14, v9

    div-int/lit8 v9, v6, 0x2

    int-to-float v15, v9

    move-object/from16 v9, v16

    invoke-direct/range {v9 .. v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    move-object/from16 v4, v16

    .line 737
    invoke-virtual {v4, v7, v8}, Landroid/view/animation/Animation;->setStartOffset(J)V

    goto/16 :goto_0

    .line 739
    :cond_0
    new-instance v7, Landroid/view/animation/ScaleAnimation;

    const/high16 v10, 0x3f800000    # 1.0f

    const v11, 0x3f666666    # 0.9f

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, 0x3f666666    # 0.9f

    div-int/lit8 v8, v5, 0x2

    int-to-float v14, v8

    div-int/lit8 v8, v6, 0x2

    int-to-float v15, v8

    move-object v9, v7

    invoke-direct/range {v9 .. v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    move-object v4, v7

    .line 740
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/high16 v11, -0x40800000    # -1.0f

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object/from16 v7, v16

    invoke-direct/range {v7 .. v15}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object/from16 v3, v16

    goto/16 :goto_0

    .line 746
    :cond_1
    if-eqz p0, :cond_2

    .line 747
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    const/4 v10, 0x1

    const/high16 v11, -0x40800000    # -1.0f

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v9, v18

    invoke-direct/range {v9 .. v17}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object/from16 v3, v18

    .line 751
    new-instance v16, Landroid/view/animation/ScaleAnimation;

    const v10, 0x3f666666    # 0.9f

    const/high16 v11, 0x3f800000    # 1.0f

    const v12, 0x3f666666    # 0.9f

    const/high16 v13, 0x3f800000    # 1.0f

    div-int/lit8 v9, v5, 0x2

    int-to-float v14, v9

    div-int/lit8 v9, v6, 0x2

    int-to-float v15, v9

    move-object/from16 v9, v16

    invoke-direct/range {v9 .. v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    move-object/from16 v4, v16

    .line 752
    invoke-virtual {v4, v7, v8}, Landroid/view/animation/Animation;->setStartOffset(J)V

    goto :goto_0

    .line 754
    :cond_2
    iget-object v7, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/server/wm/AppTransitionInjector;->getNavigationBarMode(Landroid/content/Context;)I

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, v0, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    .line 755
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    .line 756
    new-instance v7, Landroid/view/animation/ScaleAnimation;

    const/high16 v10, 0x3f800000    # 1.0f

    const v11, 0x3f666666    # 0.9f

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, 0x3f666666    # 0.9f

    div-int/lit8 v8, v5, 0x2

    int-to-float v14, v8

    div-int/lit8 v8, v6, 0x2

    int-to-float v15, v8

    move-object v9, v7

    invoke-direct/range {v9 .. v15}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    move-object v4, v7

    .line 757
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const v11, 0x3f8ccccd    # 1.1f

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object/from16 v7, v16

    invoke-direct/range {v7 .. v15}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object/from16 v3, v16

    goto :goto_0

    .line 762
    :cond_3
    new-instance v14, Landroid/view/animation/ScaleAnimation;

    const/high16 v8, 0x3f800000    # 1.0f

    const v9, 0x3f666666    # 0.9f

    const/high16 v10, 0x3f800000    # 1.0f

    const v11, 0x3f666666    # 0.9f

    div-int/lit8 v7, v5, 0x2

    int-to-float v12, v7

    div-int/lit8 v7, v6, 0x2

    int-to-float v13, v7

    move-object v7, v14

    invoke-direct/range {v7 .. v13}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    move-object v4, v14

    .line 763
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object/from16 v7, v16

    invoke-direct/range {v7 .. v15}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object/from16 v3, v16

    .line 770
    :goto_0
    new-instance v7, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;

    const v8, 0x3f19999a    # 0.6f

    const v9, 0x3f7d70a4    # 0.99f

    invoke-direct {v7, v8, v9}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V

    sput-object v7, Lcom/android/server/wm/AppTransitionInjector;->sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

    .line 771
    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 772
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 773
    sget-object v7, Lcom/android/server/wm/AppTransitionInjector;->sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 774
    const-wide/16 v7, 0x28a

    invoke-virtual {v1, v7, v8}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 775
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V

    .line 776
    return-object v1
.end method

.method static createTransitionAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ILandroid/graphics/Point;)Landroid/view/animation/Animation;
    .locals 40
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "targetPositionRect"    # Landroid/graphics/Rect;
    .param p3, "startRect"    # Landroid/graphics/Rect;
    .param p4, "orientation"    # I
    .param p5, "inertia"    # Landroid/graphics/Point;

    .line 594
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    new-instance v2, Landroid/graphics/Rect;

    move-object/from16 v3, p2

    invoke-direct {v2, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 595
    .local v2, "positionRect":Landroid/graphics/Rect;
    const/4 v5, 0x1

    if-eqz v1, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    move v6, v5

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    .line 596
    .local v6, "hasStartRect":Z
    :goto_0
    move/from16 v7, p4

    if-ne v7, v5, :cond_1

    move v8, v5

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    .line 597
    .local v8, "isPortrait":Z
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v15

    .line 598
    .local v15, "appWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v14

    .line 600
    .local v14, "appHeight":I
    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v9

    xor-int/2addr v9, v5

    move/from16 v23, v9

    .line 601
    .local v23, "canFindPosition":Z
    sget-object v9, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v13, v9, Landroid/graphics/Rect;->left:I

    .line 602
    .local v13, "insetLeft":I
    sget-object v9, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v12, v9, Landroid/graphics/Rect;->top:I

    .line 603
    .local v12, "insetTop":I
    sget-object v9, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v11, v9, Landroid/graphics/Rect;->right:I

    .line 604
    .local v11, "insetRight":I
    sget-object v9, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    iget v10, v9, Landroid/graphics/Rect;->bottom:I

    .line 606
    .local v10, "insetBottom":I
    if-eqz v6, :cond_2

    .line 607
    iget v9, v0, Landroid/graphics/Rect;->left:I

    neg-int v9, v9

    iget v4, v0, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    invoke-virtual {v1, v9, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 610
    :cond_2
    if-eqz v23, :cond_3

    .line 611
    iget v4, v0, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    iget v9, v0, Landroid/graphics/Rect;->top:I

    neg-int v9, v9

    invoke-virtual {v2, v4, v9}, Landroid/graphics/Rect;->offset(II)V

    .line 614
    :cond_3
    const v4, 0x3ecccccd    # 0.4f

    if-eqz v23, :cond_4

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int v17, v13, v11

    sub-int v9, v9, v17

    goto :goto_2

    .line 615
    :cond_4
    int-to-float v9, v15

    mul-float/2addr v9, v4

    float-to-int v9, v9

    :goto_2
    nop

    .line 616
    .local v9, "targetWidth":I
    if-eqz v23, :cond_5

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v17

    add-int v18, v12, v10

    sub-int v17, v17, v18

    goto :goto_3

    .line 617
    :cond_5
    int-to-float v5, v14

    mul-float/2addr v5, v4

    float-to-int v5, v5

    move/from16 v17, v5

    :goto_3
    move/from16 v5, v17

    .line 619
    .local v5, "targetHeight":I
    if-eqz v6, :cond_6

    iget v4, v1, Landroid/graphics/Rect;->left:I

    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    .line 620
    .local v4, "startX":I
    :goto_4
    if-eqz v6, :cond_7

    iget v3, v1, Landroid/graphics/Rect;->top:I

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    .line 621
    .local v3, "startY":I
    :goto_5
    if-eqz v6, :cond_8

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v18, v4, v18

    goto :goto_6

    .line 622
    :cond_8
    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v18, v1, v18

    :goto_6
    move/from16 v1, v18

    .line 623
    .local v1, "startCenterX":I
    if-eqz v6, :cond_9

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v18, v3, v18

    goto :goto_7

    .line 624
    :cond_9
    iget v7, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v18, v7, v18

    :goto_7
    move/from16 v7, v18

    .line 625
    .local v7, "startCenterY":I
    const v18, 0x3f19999a    # 0.6f

    const/high16 v19, 0x40000000    # 2.0f

    if-eqz v23, :cond_a

    move/from16 v20, v10

    .end local v10    # "insetBottom":I
    .local v20, "insetBottom":I
    iget v10, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v13

    goto :goto_8

    .line 626
    .end local v20    # "insetBottom":I
    .restart local v10    # "insetBottom":I
    :cond_a
    move/from16 v20, v10

    .end local v10    # "insetBottom":I
    .restart local v20    # "insetBottom":I
    int-to-float v10, v15

    mul-float v10, v10, v18

    div-float v10, v10, v19

    float-to-int v10, v10

    :goto_8
    nop

    .line 627
    .local v10, "targetX":I
    if-eqz v23, :cond_b

    move/from16 v21, v11

    .end local v11    # "insetRight":I
    .local v21, "insetRight":I
    iget v11, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v11, v12

    goto :goto_9

    .line 628
    .end local v21    # "insetRight":I
    .restart local v11    # "insetRight":I
    :cond_b
    move/from16 v21, v11

    .end local v11    # "insetRight":I
    .restart local v21    # "insetRight":I
    int-to-float v11, v14

    mul-float v11, v11, v18

    div-float v11, v11, v19

    float-to-int v11, v11

    :goto_9
    nop

    .line 630
    .local v11, "targetY":I
    move/from16 v24, v7

    .end local v7    # "startCenterY":I
    .local v24, "startCenterY":I
    if-eqz v6, :cond_c

    .line 631
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    move/from16 v25, v1

    .end local v1    # "startCenterX":I
    .local v25, "startCenterX":I
    int-to-float v1, v15

    div-float v1, v7, v1

    goto :goto_a

    .end local v25    # "startCenterX":I
    .restart local v1    # "startCenterX":I
    :cond_c
    move/from16 v25, v1

    .end local v1    # "startCenterX":I
    .restart local v25    # "startCenterX":I
    const/high16 v1, 0x3f800000    # 1.0f

    .line 632
    .local v1, "startScaleX":F
    :goto_a
    if-eqz v6, :cond_d

    .line 633
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    move/from16 v26, v6

    .end local v6    # "hasStartRect":Z
    .local v26, "hasStartRect":Z
    int-to-float v6, v14

    div-float v6, v7, v6

    goto :goto_b

    .end local v26    # "hasStartRect":Z
    .restart local v6    # "hasStartRect":Z
    :cond_d
    move/from16 v26, v6

    .end local v6    # "hasStartRect":Z
    .restart local v26    # "hasStartRect":Z
    const/high16 v6, 0x3f800000    # 1.0f

    .line 634
    .local v6, "startScaleY":F
    :goto_b
    int-to-float v7, v9

    move/from16 v22, v12

    .end local v12    # "insetTop":I
    .local v22, "insetTop":I
    int-to-float v12, v15

    div-float/2addr v7, v12

    .line 635
    .local v7, "scaleX":F
    int-to-float v12, v5

    move/from16 v27, v5

    .end local v5    # "targetHeight":I
    .local v27, "targetHeight":I
    int-to-float v5, v14

    div-float v5, v12, v5

    .line 638
    .local v5, "scaleY":F
    move/from16 v28, v13

    .end local v13    # "insetLeft":I
    .local v28, "insetLeft":I
    if-eqz p0, :cond_f

    .line 639
    new-instance v12, Landroid/view/animation/AnimationSet;

    const/4 v13, 0x1

    invoke-direct {v12, v13}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 640
    .local v12, "set":Landroid/view/animation/AnimationSet;
    new-instance v13, Landroid/view/animation/ScaleAnimation;

    const v32, 0x3f4ccccd    # 0.8f

    const/high16 v33, 0x3f800000    # 1.0f

    const v34, 0x3f4ccccd    # 0.8f

    const/high16 v35, 0x3f800000    # 1.0f

    move/from16 v38, v9

    .end local v9    # "targetWidth":I
    .local v38, "targetWidth":I
    int-to-float v9, v15

    div-float v36, v9, v19

    int-to-float v9, v14

    div-float v37, v9, v19

    move-object/from16 v31, v13

    invoke-direct/range {v31 .. v37}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    move-object v9, v13

    .line 644
    .local v9, "scaleAnimation":Landroid/view/animation/Animation;
    move/from16 v19, v14

    const-wide/16 v13, 0x12c

    .end local v14    # "appHeight":I
    .local v19, "appHeight":I
    invoke-virtual {v9, v13, v14}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 645
    sget-boolean v13, Lcom/android/server/wm/AppTransitionInjector;->IS_E10:Z

    if-nez v13, :cond_e

    .line 646
    invoke-virtual {v12, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 648
    :cond_e
    sget-object v13, Lcom/android/server/wm/AppTransitionInjector;->CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v12, v13}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 649
    const/4 v13, -0x1

    invoke-virtual {v12, v13}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 650
    move-object v9, v12

    .line 651
    .end local v12    # "set":Landroid/view/animation/AnimationSet;
    .local v9, "anim":Landroid/view/animation/Animation;
    move/from16 v17, v1

    move-object/from16 v37, v2

    move v12, v4

    move/from16 v39, v7

    move v7, v10

    move v0, v11

    move/from16 v33, v20

    move/from16 v34, v21

    move/from16 v35, v22

    move/from16 v10, v24

    move/from16 v36, v25

    move/from16 v25, v38

    move/from16 v38, v15

    move v15, v3

    move/from16 v3, v19

    goto/16 :goto_f

    .end local v19    # "appHeight":I
    .end local v38    # "targetWidth":I
    .local v9, "targetWidth":I
    .restart local v14    # "appHeight":I
    :cond_f
    move/from16 v38, v9

    move/from16 v19, v14

    .end local v9    # "targetWidth":I
    .end local v14    # "appHeight":I
    .restart local v19    # "appHeight":I
    .restart local v38    # "targetWidth":I
    const/4 v9, 0x0

    if-eqz v23, :cond_13

    .line 652
    new-instance v12, Landroid/view/animation/AnimationSet;

    const/4 v13, 0x0

    invoke-direct {v12, v13}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    move-object v14, v12

    .line 653
    .local v14, "set":Landroid/view/animation/AnimationSet;
    new-instance v12, Landroid/view/animation/AlphaAnimation;

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-direct {v12, v13, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v13, v12

    .line 654
    .local v13, "alphaAnimation":Landroid/view/animation/Animation;
    move-object/from16 v17, v14

    move/from16 v16, v15

    .end local v14    # "set":Landroid/view/animation/AnimationSet;
    .end local v15    # "appWidth":I
    .local v16, "appWidth":I
    .local v17, "set":Landroid/view/animation/AnimationSet;
    const-wide/16 v14, 0x28

    invoke-virtual {v13, v14, v15}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 655
    const-wide/16 v14, 0xd2

    invoke-virtual {v13, v14, v15}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 656
    sget-object v9, Lcom/android/server/wm/AppTransitionInjector;->CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v13, v9}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 660
    if-nez p5, :cond_11

    .line 661
    new-instance v12, Landroid/view/animation/TranslateXAnimation;

    int-to-float v14, v4

    iget v15, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v15, v10

    int-to-float v15, v15

    invoke-direct {v12, v14, v15}, Landroid/view/animation/TranslateXAnimation;-><init>(FF)V

    .line 662
    .local v12, "translateXAnimation":Landroid/view/animation/Animation;
    new-instance v14, Landroid/view/animation/TranslateYAnimation;

    int-to-float v15, v3

    move-object/from16 v18, v9

    iget v9, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v11

    int-to-float v9, v9

    invoke-direct {v14, v15, v9}, Landroid/view/animation/TranslateYAnimation;-><init>(FF)V

    move-object v9, v14

    .line 664
    .local v9, "translateYAnimation":Landroid/view/animation/Animation;
    new-instance v14, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;

    invoke-direct {v14, v1, v7}, Lcom/android/server/wm/AppTransitionInjector$ScaleXAnimation;-><init>(FF)V

    .line 665
    .local v14, "scaleXAnimation":Landroid/view/animation/Animation;
    if-eqz v8, :cond_10

    move-object/from16 v15, v18

    goto :goto_c

    .line 666
    :cond_10
    sget-object v15, Lcom/android/server/wm/AppTransitionInjector;->QUINT_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 665
    :goto_c
    invoke-virtual {v14, v15}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 667
    move v15, v10

    move/from16 v18, v11

    const-wide/16 v10, 0x12c

    .end local v10    # "targetX":I
    .end local v11    # "targetY":I
    .local v15, "targetX":I
    .local v18, "targetY":I
    invoke-virtual {v14, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 669
    new-instance v10, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;

    invoke-direct {v10, v6, v5}, Lcom/android/server/wm/AppTransitionInjector$ScaleYAnimation;-><init>(FF)V

    .line 670
    .local v10, "scaleYAnimation":Landroid/view/animation/Animation;
    sget-object v11, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v10, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 671
    move/from16 v32, v3

    move/from16 v31, v4

    const-wide/16 v3, 0x12c

    .end local v3    # "startY":I
    .end local v4    # "startX":I
    .local v31, "startX":I
    .local v32, "startY":I
    invoke-virtual {v10, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 672
    invoke-virtual {v12, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 673
    invoke-virtual {v9, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 674
    move-object/from16 v3, v17

    .end local v17    # "set":Landroid/view/animation/AnimationSet;
    .local v3, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v3, v14}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 675
    invoke-virtual {v3, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 676
    .end local v10    # "scaleYAnimation":Landroid/view/animation/Animation;
    .end local v14    # "scaleXAnimation":Landroid/view/animation/Animation;
    move-object/from16 v37, v2

    move-object v4, v3

    move/from16 v39, v7

    move-object v2, v13

    move v7, v15

    move/from16 v0, v18

    move/from16 v3, v19

    move/from16 v33, v20

    move/from16 v34, v21

    move/from16 v35, v22

    move/from16 v10, v24

    move/from16 v36, v25

    move/from16 v25, v38

    move/from16 v38, v16

    goto/16 :goto_d

    .line 677
    .end local v9    # "translateYAnimation":Landroid/view/animation/Animation;
    .end local v12    # "translateXAnimation":Landroid/view/animation/Animation;
    .end local v15    # "targetX":I
    .end local v18    # "targetY":I
    .end local v31    # "startX":I
    .end local v32    # "startY":I
    .local v3, "startY":I
    .restart local v4    # "startX":I
    .local v10, "targetX":I
    .restart local v11    # "targetY":I
    .restart local v17    # "set":Landroid/view/animation/AnimationSet;
    :cond_11
    move/from16 v32, v3

    move/from16 v31, v4

    move v15, v10

    move/from16 v18, v11

    move-object/from16 v3, v17

    .end local v4    # "startX":I
    .end local v10    # "targetX":I
    .end local v11    # "targetY":I
    .end local v17    # "set":Landroid/view/animation/AnimationSet;
    .local v3, "set":Landroid/view/animation/AnimationSet;
    .restart local v15    # "targetX":I
    .restart local v18    # "targetY":I
    .restart local v31    # "startX":I
    .restart local v32    # "startY":I
    iget v4, v2, Landroid/graphics/Rect;->left:I

    add-int v4, v4, v28

    div-int/lit8 v9, v38, 0x2

    add-int/2addr v4, v9

    int-to-float v4, v4

    .line 678
    .local v4, "endCenterX":F
    iget v9, v2, Landroid/graphics/Rect;->top:I

    add-int v9, v9, v22

    div-int/lit8 v10, v27, 0x2

    add-int/2addr v9, v10

    int-to-float v14, v9

    .line 679
    .local v14, "endCenterY":F
    new-instance v17, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;

    move/from16 v12, v25

    .end local v25    # "startCenterX":I
    .local v12, "startCenterX":I
    int-to-float v11, v12

    move/from16 v25, v38

    .end local v38    # "targetWidth":I
    .local v25, "targetWidth":I
    move-object/from16 v9, v17

    move/from16 v33, v20

    .end local v20    # "insetBottom":I
    .local v33, "insetBottom":I
    move v10, v1

    move/from16 v0, v18

    move/from16 v34, v21

    move/from16 v18, v11

    .end local v18    # "targetY":I
    .end local v21    # "insetRight":I
    .local v0, "targetY":I
    .local v34, "insetRight":I
    move v11, v7

    move/from16 v36, v12

    move/from16 v35, v22

    .end local v12    # "startCenterX":I
    .end local v22    # "insetTop":I
    .local v35, "insetTop":I
    .local v36, "startCenterX":I
    move/from16 v12, v18

    move-object/from16 v37, v2

    move-object v2, v13

    .end local v13    # "alphaAnimation":Landroid/view/animation/Animation;
    .local v2, "alphaAnimation":Landroid/view/animation/Animation;
    .local v37, "positionRect":Landroid/graphics/Rect;
    move v13, v4

    move/from16 v29, v4

    move/from16 v30, v14

    move-object v4, v3

    move/from16 v3, v19

    .end local v14    # "endCenterY":F
    .end local v19    # "appHeight":I
    .local v3, "appHeight":I
    .local v4, "set":Landroid/view/animation/AnimationSet;
    .local v29, "endCenterX":F
    .local v30, "endCenterY":F
    move-object/from16 v14, p5

    move/from16 v39, v7

    move v7, v15

    move/from16 v38, v16

    .end local v15    # "targetX":I
    .end local v16    # "appWidth":I
    .local v7, "targetX":I
    .local v38, "appWidth":I
    .local v39, "scaleX":F
    move/from16 v15, v38

    invoke-direct/range {v9 .. v15}, Lcom/android/server/wm/AppTransitionInjector$BezierXAnimation;-><init>(FFFFLandroid/graphics/Point;I)V

    move-object/from16 v12, v17

    .line 681
    .local v12, "translateXAnimation":Landroid/view/animation/Animation;
    new-instance v9, Lcom/android/server/wm/AppTransitionInjector$BezierYAnimation;

    move/from16 v10, v24

    .end local v24    # "startCenterY":I
    .local v10, "startCenterY":I
    int-to-float v11, v10

    move-object/from16 v16, v9

    move/from16 v17, v6

    move/from16 v18, v5

    move/from16 v19, v11

    move/from16 v20, v30

    move-object/from16 v21, p5

    move/from16 v22, v3

    invoke-direct/range {v16 .. v22}, Lcom/android/server/wm/AppTransitionInjector$BezierYAnimation;-><init>(FFFFLandroid/graphics/Point;I)V

    .line 683
    .restart local v9    # "translateYAnimation":Landroid/view/animation/Animation;
    const-wide/16 v13, 0x190

    invoke-virtual {v12, v13, v14}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 684
    invoke-virtual {v9, v13, v14}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 687
    .end local v29    # "endCenterX":F
    .end local v30    # "endCenterY":F
    :goto_d
    if-eqz v8, :cond_12

    sget-object v11, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    goto :goto_e

    .line 688
    :cond_12
    sget-object v11, Lcom/android/server/wm/AppTransitionInjector;->QUINT_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 687
    :goto_e
    invoke-virtual {v12, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 689
    sget-object v11, Lcom/android/server/wm/AppTransitionInjector;->QUART_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v9, v11}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 691
    invoke-virtual {v4, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 692
    invoke-virtual {v4, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 693
    invoke-virtual {v4, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 694
    const/4 v11, 0x1

    invoke-virtual {v4, v11}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 695
    move-object v9, v4

    .line 696
    .end local v2    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v4    # "set":Landroid/view/animation/AnimationSet;
    .end local v12    # "translateXAnimation":Landroid/view/animation/Animation;
    .local v9, "anim":Landroid/view/animation/Animation;
    move/from16 v17, v1

    move/from16 v12, v31

    move/from16 v15, v32

    goto :goto_f

    .line 697
    .end local v0    # "targetY":I
    .end local v9    # "anim":Landroid/view/animation/Animation;
    .end local v31    # "startX":I
    .end local v32    # "startY":I
    .end local v33    # "insetBottom":I
    .end local v34    # "insetRight":I
    .end local v35    # "insetTop":I
    .end local v36    # "startCenterX":I
    .end local v37    # "positionRect":Landroid/graphics/Rect;
    .end local v39    # "scaleX":F
    .local v2, "positionRect":Landroid/graphics/Rect;
    .local v3, "startY":I
    .local v4, "startX":I
    .local v7, "scaleX":F
    .local v10, "targetX":I
    .restart local v11    # "targetY":I
    .local v15, "appWidth":I
    .restart local v19    # "appHeight":I
    .restart local v20    # "insetBottom":I
    .restart local v21    # "insetRight":I
    .restart local v22    # "insetTop":I
    .restart local v24    # "startCenterY":I
    .local v25, "startCenterX":I
    .local v38, "targetWidth":I
    :cond_13
    move-object/from16 v37, v2

    move/from16 v32, v3

    move/from16 v31, v4

    move/from16 v39, v7

    move v7, v10

    move v0, v11

    move/from16 v3, v19

    move/from16 v33, v20

    move/from16 v34, v21

    move/from16 v35, v22

    move/from16 v10, v24

    move/from16 v36, v25

    move/from16 v25, v38

    const/4 v11, 0x1

    move/from16 v38, v15

    .end local v2    # "positionRect":Landroid/graphics/Rect;
    .end local v4    # "startX":I
    .end local v11    # "targetY":I
    .end local v15    # "appWidth":I
    .end local v19    # "appHeight":I
    .end local v20    # "insetBottom":I
    .end local v21    # "insetRight":I
    .end local v22    # "insetTop":I
    .end local v24    # "startCenterY":I
    .restart local v0    # "targetY":I
    .local v3, "appHeight":I
    .local v7, "targetX":I
    .local v10, "startCenterY":I
    .local v25, "targetWidth":I
    .restart local v31    # "startX":I
    .restart local v32    # "startY":I
    .restart local v33    # "insetBottom":I
    .restart local v34    # "insetRight":I
    .restart local v35    # "insetTop":I
    .restart local v36    # "startCenterX":I
    .restart local v37    # "positionRect":Landroid/graphics/Rect;
    .local v38, "appWidth":I
    .restart local v39    # "scaleX":F
    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v11}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 698
    .local v2, "set":Landroid/view/animation/AnimationSet;
    new-instance v4, Landroid/view/animation/ScaleAnimation;

    const v11, 0x3ecccccd    # 0.4f

    invoke-direct {v4, v1, v11, v6, v11}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 701
    .local v4, "scaleAnimation":Landroid/view/animation/Animation;
    new-instance v11, Landroid/view/animation/TranslateAnimation;

    move/from16 v12, v31

    .end local v31    # "startX":I
    .local v12, "startX":I
    int-to-float v13, v12

    int-to-float v14, v7

    move/from16 v15, v32

    .end local v32    # "startY":I
    .local v15, "startY":I
    int-to-float v9, v15

    move/from16 v17, v1

    .end local v1    # "startScaleX":F
    .local v17, "startScaleX":F
    int-to-float v1, v0

    invoke-direct {v11, v13, v14, v9, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    move-object v1, v11

    .line 702
    .local v1, "translateAnimation":Landroid/view/animation/Animation;
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/4 v11, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-direct {v9, v13, v11}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 704
    .local v9, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v2, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 705
    invoke-virtual {v2, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 706
    invoke-virtual {v2, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 707
    const-wide/16 v13, 0x12c

    invoke-virtual {v2, v13, v14}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 708
    sget-object v11, Lcom/android/server/wm/AppTransitionInjector;->CUBIC_EASE_OUT_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v11}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 709
    sget-boolean v11, Lcom/android/server/wm/AppTransitionInjector;->IS_E10:Z

    if-eqz v11, :cond_14

    .line 710
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/view/animation/AnimationSet;->setZAdjustment(I)V

    .line 712
    :cond_14
    move-object v11, v2

    move-object v9, v11

    .line 714
    .end local v1    # "translateAnimation":Landroid/view/animation/Animation;
    .end local v2    # "set":Landroid/view/animation/AnimationSet;
    .end local v4    # "scaleAnimation":Landroid/view/animation/Animation;
    .local v9, "anim":Landroid/view/animation/Animation;
    :goto_f
    return-object v9
.end method

.method static createWallPaperOpenAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "positionRect"    # Landroid/graphics/Rect;

    .line 573
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/server/wm/AppTransitionInjector;->createWallPaperOpenAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static createWallPaperOpenAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "positionRect"    # Landroid/graphics/Rect;
    .param p3, "orientation"    # I

    .line 584
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/android/server/wm/AppTransitionInjector;->createWallPaperOpenAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static createWallPaperOpenAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "positionRect"    # Landroid/graphics/Rect;
    .param p3, "startRect"    # Landroid/graphics/Rect;

    .line 578
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/android/server/wm/AppTransitionInjector;->createWallPaperOpenAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static createWallPaperOpenAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;I)Landroid/view/animation/Animation;
    .locals 6
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "positionRect"    # Landroid/graphics/Rect;
    .param p3, "startRect"    # Landroid/graphics/Rect;
    .param p4, "orientation"    # I

    .line 589
    const/4 v3, 0x0

    const/4 v5, 0x0

    move v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/server/wm/AppTransitionInjector;->createTransitionAnimation(ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;ILandroid/graphics/Point;)Landroid/view/animation/Animation;

    move-result-object v0

    return-object v0
.end method

.method static createWallerOpenCloseTransitionAnimation(ZLandroid/graphics/Rect;Z)Landroid/view/animation/Animation;
    .locals 18
    .param p0, "enter"    # Z
    .param p1, "appFrame"    # Landroid/graphics/Rect;
    .param p2, "isOpenOrClose"    # Z

    .line 913
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 916
    .local v0, "set":Landroid/view/animation/AnimationSet;
    const/4 v2, 0x0

    .line 917
    .local v2, "scaleAnimation":Landroid/view/animation/Animation;
    const/4 v3, 0x0

    .line 918
    .local v3, "alphaAnimation":Landroid/view/animation/Animation;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 919
    .local v4, "width":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    .line 920
    .local v5, "heigth":I
    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    if-eqz p0, :cond_0

    .line 938
    new-instance v17, Landroid/view/animation/ScaleAnimation;

    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x1

    const/high16 v14, 0x3f000000    # 0.5f

    const/4 v15, 0x1

    const/high16 v16, 0x3f000000    # 0.5f

    move-object/from16 v8, v17

    invoke-direct/range {v8 .. v16}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object/from16 v2, v17

    .line 939
    new-instance v8, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v8, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v3, v8

    .line 940
    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 941
    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 945
    :cond_0
    new-instance v8, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v8, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    move-object v3, v8

    .line 946
    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 949
    :goto_0
    new-instance v6, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;

    const v7, 0x3f266666    # 0.65f

    const v8, 0x3f733333    # 0.95f

    invoke-direct {v6, v7, v8}, Lcom/android/server/wm/AppTransitionInjector$ActivityTranstionInterpolator;-><init>(FF)V

    sput-object v6, Lcom/android/server/wm/AppTransitionInjector;->sActivityTransitionInterpolator:Landroid/view/animation/Interpolator;

    .line 951
    invoke-virtual {v0, v6}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 952
    invoke-static {}, Lcom/android/server/wm/AppTransitionInjector;->isCTS()Z

    move-result v6

    if-eqz v6, :cond_1

    const-wide/16 v6, 0x190

    goto :goto_1

    :cond_1
    const-wide/16 v6, 0x226

    :goto_1
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 954
    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setHasRoundedCorners(Z)V

    .line 955
    return-object v0
.end method

.method static disableSnapshot(Ljava/lang/String;)Z
    .locals 1
    .param p0, "pkg"    # Ljava/lang/String;

    .line 1525
    sget-object v0, Lcom/android/server/wm/AppTransitionInjector;->BLACK_LIST_NOT_ALLOWED_SNAPSHOT:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1526
    const/4 v0, 0x1

    return v0

    .line 1528
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static disableSnapshotByComponent(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 2
    .param p0, "mActivity"    # Lcom/android/server/wm/ActivityRecord;

    .line 1532
    sget-object v0, Lcom/android/server/wm/AppTransitionInjector;->BLACK_LIST_NOT_ALLOWED_SNAPSHOT_COMPONENT:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1533
    const/4 v0, 0x1

    return v0

    .line 1535
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static disableSnapshotForApplock(Ljava/lang/String;I)Z
    .locals 5
    .param p0, "pkg"    # Ljava/lang/String;
    .param p1, "uid"    # I

    .line 1553
    const-string/jumbo v0, "security"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 1554
    .local v0, "b":Landroid/os/IBinder;
    const/4 v1, 0x0

    .line 1555
    .local v1, "result":Z
    if-eqz v0, :cond_1

    .line 1557
    :try_start_0
    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    .line 1558
    .local v2, "userId":I
    invoke-static {v0}, Lmiui/security/ISecurityManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/security/ISecurityManager;

    move-result-object v3

    .line 1559
    .local v3, "service":Lmiui/security/ISecurityManager;
    invoke-interface {v3, v2}, Lmiui/security/ISecurityManager;->haveAccessControlPassword(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1560
    invoke-interface {v3, p0, v2}, Lmiui/security/ISecurityManager;->getApplicationAccessControlEnabledAsUser(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1561
    const/4 v4, 0x0

    invoke-interface {v3, p0, v4, v2}, Lmiui/security/ISecurityManager;->checkAccessControlPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v4, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    move v1, v4

    .line 1564
    .end local v2    # "userId":I
    .end local v3    # "service":Lmiui/security/ISecurityManager;
    goto :goto_1

    .line 1562
    :catch_0
    move-exception v2

    .line 1563
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "AppTransitionInjector"

    const-string/jumbo v4, "start window checkAccessControlPass error: "

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1566
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return v1
.end method

.method private static doAnimationCallback(Landroid/os/IRemoteCallback;)V
    .locals 1
    .param p0, "callback"    # Landroid/os/IRemoteCallback;

    .line 409
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p0, v0}, Landroid/os/IRemoteCallback;->sendResult(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    goto :goto_0

    .line 410
    :catch_0
    move-exception v0

    .line 412
    :goto_0
    return-void
.end method

.method public static getMiuiAnimSupportInset()Landroid/graphics/Rect;
    .locals 1

    .line 1521
    sget-object v0, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    return-object v0
.end method

.method public static getNavigationBarMode(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .line 1630
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1631
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    .line 1630
    const-string v2, "navigation_mode"

    const/4 v3, 0x2

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method private static getRoundedCornerRadius(Landroid/view/RoundedCorner;)F
    .locals 1
    .param p0, "roundedCorner"    # Landroid/view/RoundedCorner;

    .line 1608
    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 1609
    :cond_0
    invoke-virtual {p0}, Landroid/view/RoundedCorner;->getRadius()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public static getUnHierarchicalAnimationTargets(Landroid/util/ArraySet;Landroid/util/ArraySet;Z)Landroid/util/ArraySet;
    .locals 5
    .param p2, "visible"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/ArraySet<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;",
            "Landroid/util/ArraySet<",
            "Lcom/android/server/wm/ActivityRecord;",
            ">;Z)",
            "Landroid/util/ArraySet<",
            "Lcom/android/server/wm/WindowContainer;",
            ">;"
        }
    .end annotation

    .line 1618
    .local p0, "openingApps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;"
    .local p1, "closingApps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;"
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1619
    .local v0, "candidates":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/android/server/wm/WindowContainer;>;"
    if-eqz p2, :cond_0

    move-object v1, p0

    goto :goto_0

    :cond_0
    move-object v1, p1

    .line 1620
    .local v1, "apps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Lcom/android/server/wm/ActivityRecord;>;"
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Landroid/util/ArraySet;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1621
    invoke-virtual {v1, v2}, Landroid/util/ArraySet;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/ActivityRecord;

    .line 1622
    .local v3, "app":Lcom/android/server/wm/ActivityRecord;
    invoke-virtual {v3, p2}, Lcom/android/server/wm/ActivityRecord;->shouldApplyAnimation(Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1623
    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1620
    .end local v3    # "app":Lcom/android/server/wm/ActivityRecord;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1626
    .end local v2    # "i":I
    :cond_2
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2, v0}, Landroid/util/ArraySet;-><init>(Ljava/util/Collection;)V

    return-object v2
.end method

.method static ignoreLaunchedFromSystemSurface(Ljava/lang/String;)Z
    .locals 1
    .param p0, "pkg"    # Ljava/lang/String;

    .line 1539
    sget-object v0, Lcom/android/server/wm/AppTransitionInjector;->IGNORE_LAUNCHED_FROM_SYSTEM_SURFACE:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1540
    const/4 v0, 0x1

    return v0

    .line 1542
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method static initDisplayRoundCorner(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .line 1578
    const/16 v0, 0x3c

    .line 1579
    .local v0, "result":I
    if-eqz p0, :cond_0

    .line 1580
    invoke-virtual {p0}, Landroid/content/Context;->getDisplayNoVerify()Landroid/view/Display;

    move-result-object v1

    .line 1581
    .local v1, "display":Landroid/view/Display;
    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1582
    nop

    .line 1583
    invoke-virtual {v1, v2}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;

    move-result-object v2

    .line 1582
    invoke-static {v2}, Lcom/android/server/wm/AppTransitionInjector;->getRoundedCornerRadius(Landroid/view/RoundedCorner;)F

    move-result v2

    .line 1584
    .local v2, "topLeftRadius":F
    nop

    .line 1585
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;

    move-result-object v3

    .line 1584
    invoke-static {v3}, Lcom/android/server/wm/AppTransitionInjector;->getRoundedCornerRadius(Landroid/view/RoundedCorner;)F

    move-result v3

    .line 1586
    .local v3, "topRightRadius":F
    nop

    .line 1587
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;

    move-result-object v4

    .line 1586
    invoke-static {v4}, Lcom/android/server/wm/AppTransitionInjector;->getRoundedCornerRadius(Landroid/view/RoundedCorner;)F

    move-result v4

    .line 1588
    .local v4, "bottomRightRadius":F
    nop

    .line 1589
    const/4 v5, 0x3

    invoke-virtual {v1, v5}, Landroid/view/Display;->getRoundedCorner(I)Landroid/view/RoundedCorner;

    move-result-object v5

    .line 1588
    invoke-static {v5}, Lcom/android/server/wm/AppTransitionInjector;->getRoundedCornerRadius(Landroid/view/RoundedCorner;)F

    move-result v5

    .line 1592
    .local v5, "bottomLeftRadius":F
    invoke-static {v2, v3}, Lcom/android/server/wm/AppTransitionInjector;->minRadius(FF)F

    move-result v6

    .line 1593
    invoke-static {v5, v4}, Lcom/android/server/wm/AppTransitionInjector;->minRadius(FF)F

    move-result v7

    .line 1592
    invoke-static {v6, v7}, Lcom/android/server/wm/AppTransitionInjector;->minRadius(FF)F

    move-result v6

    float-to-int v0, v6

    .line 1594
    int-to-float v6, v0

    const/4 v7, 0x0

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_0

    .line 1595
    const/16 v0, 0x3c

    .line 1598
    .end local v1    # "display":Landroid/view/Display;
    .end local v2    # "topLeftRadius":F
    .end local v3    # "topRightRadius":F
    .end local v4    # "bottomRightRadius":F
    .end local v5    # "bottomLeftRadius":F
    :cond_0
    sput v0, Lcom/android/server/wm/AppTransitionInjector;->DISPLAY_ROUND_CORNER_RADIUS:I

    .line 1599
    return-void
.end method

.method public static isCTS()Z
    .locals 2

    .line 1613
    const-string v0, "ro.miui.cts"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "persist.sys.miui_optimization"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method static isUseFloatingAnimation(Landroid/view/animation/Animation;)Z
    .locals 3
    .param p0, "a"    # Landroid/view/animation/Animation;

    .line 516
    instance-of v0, p0, Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_1

    .line 517
    move-object v0, p0

    check-cast v0, Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/animation/Animation;

    .line 518
    .local v1, "animation":Landroid/view/animation/Animation;
    instance-of v2, v1, Landroid/view/animation/TranslateWithClipAnimation;

    if-eqz v2, :cond_0

    .line 519
    const/4 v0, 0x1

    return v0

    .line 521
    .end local v1    # "animation":Landroid/view/animation/Animation;
    :cond_0
    goto :goto_0

    .line 523
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method static isUseFreeFormAnimation(I)Z
    .locals 1
    .param p0, "transit"    # I

    .line 422
    const/16 v0, 0x18

    if-eq p0, v0, :cond_1

    const/4 v0, 0x6

    if-eq p0, v0, :cond_1

    const/16 v0, 0x19

    if-eq p0, v0, :cond_1

    const/4 v0, 0x7

    if-ne p0, v0, :cond_0

    goto :goto_0

    .line 428
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 426
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method static loadAnimationNotCheckForDimmer(IZLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;
    .locals 2
    .param p0, "transit"    # I
    .param p1, "enter"    # Z
    .param p2, "frame"    # Landroid/graphics/Rect;
    .param p3, "container"    # Lcom/android/server/wm/WindowContainer;
    .param p4, "service"    # Lcom/android/server/wm/WindowManagerService;

    .line 960
    const/4 v0, 0x0

    .line 961
    .local v0, "defaultAnimation":Landroid/view/animation/Animation;
    packed-switch p0, :pswitch_data_0

    .line 971
    const/4 v0, 0x0

    goto :goto_0

    .line 967
    :pswitch_0
    const/4 v1, 0x0

    invoke-static {p1, p2, v1, p3, p4}, Lcom/android/server/wm/AppTransitionInjector;->createActivityOpenCloseTransitionForDimmer(ZLandroid/graphics/Rect;ZLcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 969
    goto :goto_0

    .line 963
    :pswitch_1
    const/4 v1, 0x1

    invoke-static {p1, p2, v1, p3, p4}, Lcom/android/server/wm/AppTransitionInjector;->createActivityOpenCloseTransitionForDimmer(ZLandroid/graphics/Rect;ZLcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 965
    nop

    .line 973
    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static loadAnimationSafely(Landroid/content/Context;I)Landroid/view/animation/Animation;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .line 1102
    :try_start_0
    invoke-static {p0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1103
    :catch_0
    move-exception v0

    .line 1104
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AppTransitionInjector"

    const-string v2, "Unable to load animation resource"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1105
    const/4 v1, 0x0

    return-object v1
.end method

.method static loadDefaultAnimation(Landroid/view/WindowManager$LayoutParams;IZLandroid/graphics/Rect;)Landroid/view/animation/Animation;
    .locals 8
    .param p0, "lp"    # Landroid/view/WindowManager$LayoutParams;
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "frame"    # Landroid/graphics/Rect;

    .line 1120
    const/4 v0, 0x0

    .line 1121
    .local v0, "defaultAnimation":Landroid/view/animation/Animation;
    invoke-static {p0}, Lcom/android/server/wm/AppTransitionInjector;->useDefaultAnimationAttr(Landroid/view/WindowManager$LayoutParams;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1122
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-static/range {v2 .. v7}, Lcom/android/server/wm/AppTransitionInjector;->loadDefaultAnimationNotCheck(Landroid/view/WindowManager$LayoutParams;IZLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1124
    :cond_0
    return-object v0
.end method

.method static loadDefaultAnimationNotCheck(Landroid/view/WindowManager$LayoutParams;IZLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;Lcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;
    .locals 7
    .param p0, "lp"    # Landroid/view/WindowManager$LayoutParams;
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "frame"    # Landroid/graphics/Rect;
    .param p4, "container"    # Lcom/android/server/wm/WindowContainer;
    .param p5, "service"    # Lcom/android/server/wm/WindowManagerService;

    .line 1128
    const/4 v0, 0x0

    .line 1129
    .local v0, "defaultAnimation":Landroid/view/animation/Animation;
    const/4 v1, 0x0

    .line 1130
    .local v1, "mffasStub":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    const/4 v2, 0x0

    .line 1131
    .local v2, "freemAs":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1132
    .local v3, "freeformScale":F
    instance-of v4, p4, Lcom/android/server/wm/ActivityRecord;

    if-eqz v4, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    .line 1133
    move-object v4, p4

    check-cast v4, Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v4}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I

    move-result v4

    .line 1134
    .local v4, "rootTaskId":I
    iget-object v5, p5, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-interface {v5, v4}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v1

    .line 1135
    if-eqz v1, :cond_0

    .line 1136
    instance-of v5, v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    if-eqz v5, :cond_0

    .line 1137
    move-object v2, v1

    check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 1138
    iget v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;->mFreeFormScale:F

    .line 1141
    .end local v4    # "rootTaskId":I
    :cond_0
    const/4 v4, 0x0

    if-eqz p4, :cond_1

    .line 1142
    iget-boolean v5, p4, Lcom/android/server/wm/WindowContainer;->mNeedReplaceTaskAnimation:Z

    goto :goto_0

    :cond_1
    move v5, v4

    .line 1143
    .local v5, "isSwitchAnimationType":Z
    :goto_0
    if-eqz v5, :cond_5

    .line 1144
    const/16 v6, 0x8

    if-eq p1, v6, :cond_4

    const/16 v6, 0xa

    if-ne p1, v6, :cond_2

    goto :goto_1

    .line 1146
    :cond_2
    const/16 v6, 0x9

    if-eq p1, v6, :cond_3

    const/16 v6, 0xb

    if-ne p1, v6, :cond_5

    .line 1147
    :cond_3
    const/4 p1, 0x7

    goto :goto_2

    .line 1145
    :cond_4
    :goto_1
    const/4 p1, 0x6

    .line 1150
    :cond_5
    :goto_2
    const/4 v6, 0x1

    sparse-switch p1, :sswitch_data_0

    .line 1196
    const/4 v0, 0x0

    goto :goto_3

    .line 1165
    :sswitch_0
    if-nez v2, :cond_6

    .line 1166
    goto :goto_3

    .line 1168
    :cond_6
    if-eqz p5, :cond_8

    .line 1169
    iget-object v4, p5, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/server/wm/AppTransitionInjector;->updateToTranslucentAnimIfNeeded(I)I

    move-result v6

    invoke-static {v4, v6}, Lcom/android/server/wm/AppTransitionInjector;->loadAnimationSafely(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_3

    .line 1152
    :sswitch_1
    if-nez v2, :cond_7

    .line 1153
    goto :goto_3

    .line 1155
    :cond_7
    if-eqz p5, :cond_8

    .line 1156
    iget-object v4, p5, Lcom/android/server/wm/WindowManagerService;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/server/wm/AppTransitionInjector;->updateToTranslucentAnimIfNeeded(I)I

    move-result v6

    invoke-static {v4, v6}, Lcom/android/server/wm/AppTransitionInjector;->loadAnimationSafely(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_3

    .line 1191
    :sswitch_2
    if-eqz p2, :cond_8

    .line 1192
    invoke-static {p2, p3, v4}, Lcom/android/server/wm/AppTransitionInjector;->createWallerOpenCloseTransitionAnimation(ZLandroid/graphics/Rect;Z)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_3

    .line 1183
    :sswitch_3
    invoke-static {p2, p3, v4, p5}, Lcom/android/server/wm/AppTransitionInjector;->createTaskOpenCloseTransition(ZLandroid/graphics/Rect;ZLcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1184
    goto :goto_3

    .line 1179
    :sswitch_4
    invoke-static {p2, p3, v6, p5}, Lcom/android/server/wm/AppTransitionInjector;->createTaskOpenCloseTransition(ZLandroid/graphics/Rect;ZLcom/android/server/wm/WindowManagerService;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1180
    goto :goto_3

    .line 1174
    :sswitch_5
    invoke-static {p2, p3, v4, v3, p4}, Lcom/android/server/wm/AppTransitionInjector;->createActivityOpenCloseTransition(ZLandroid/graphics/Rect;ZFLcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1176
    goto :goto_3

    .line 1161
    :sswitch_6
    invoke-static {p2, p3, v6, v3, p4}, Lcom/android/server/wm/AppTransitionInjector;->createActivityOpenCloseTransition(ZLandroid/graphics/Rect;ZFLcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1163
    nop

    .line 1198
    :cond_8
    :goto_3
    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_6
        0x7 -> :sswitch_5
        0x8 -> :sswitch_4
        0x9 -> :sswitch_3
        0xa -> :sswitch_4
        0xb -> :sswitch_3
        0xc -> :sswitch_2
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method static loadFreeFormAnimation(Lcom/android/server/wm/WindowManagerService;IZLandroid/graphics/Rect;Lcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;
    .locals 5
    .param p0, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "frame"    # Landroid/graphics/Rect;
    .param p4, "container"    # Lcom/android/server/wm/WindowContainer;

    .line 433
    const/4 v0, 0x0

    .line 434
    .local v0, "a":Landroid/view/animation/Animation;
    instance-of v1, p4, Lcom/android/server/wm/WindowState;

    if-eqz v1, :cond_0

    .line 435
    move-object v1, p4

    check-cast v1, Lcom/android/server/wm/WindowState;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowState;->getTask()Lcom/android/server/wm/Task;

    move-result-object p4

    .line 437
    :cond_0
    instance-of v1, p4, Lcom/android/server/wm/ActivityRecord;

    if-eqz v1, :cond_1

    .line 438
    move-object v1, p4

    check-cast v1, Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object p4

    .line 440
    :cond_1
    if-eqz p4, :cond_5

    instance-of v1, p4, Lcom/android/server/wm/Task;

    if-eqz v1, :cond_5

    .line 441
    move-object v1, p4

    check-cast v1, Lcom/android/server/wm/Task;

    invoke-virtual {v1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    .line 442
    .local v1, "rootTaskId":I
    iget-object v2, p0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;

    invoke-interface {v2, v1}, Lcom/android/server/wm/MiuiFreeFormManagerServiceStub;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v2

    .line 443
    .local v2, "freemAsstub":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    if-eqz v2, :cond_5

    .line 444
    instance-of v3, v2, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    if-eqz v3, :cond_5

    .line 445
    move-object v3, v2

    check-cast v3, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 446
    .local v3, "freemAs":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    const/16 v4, 0x8

    if-eq p1, v4, :cond_3

    const/16 v4, 0xa

    if-eq p1, v4, :cond_3

    const/16 v4, 0x9

    if-eq p1, v4, :cond_3

    const/16 v4, 0xb

    if-eq p1, v4, :cond_3

    if-nez p1, :cond_2

    goto :goto_0

    .line 456
    :cond_2
    goto :goto_1

    .line 451
    :cond_3
    :goto_0
    invoke-virtual {v3}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isNeedAnimation()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 452
    invoke-static {p0, p2, p3, v3}, Lcom/android/server/wm/AppTransitionInjector;->createFreeFormAppOpenAndExitAnimation(Lcom/android/server/wm/WindowManagerService;ZLandroid/graphics/Rect;Lcom/android/server/wm/MiuiFreeFormActivityStack;)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 454
    :cond_4
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->setNeedAnimation(Z)V

    .line 466
    .end local v1    # "rootTaskId":I
    .end local v2    # "freemAsstub":Lcom/android/server/wm/MiuiFreeFormActivityStackStub;
    .end local v3    # "freemAs":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_5
    :goto_1
    return-object v0
.end method

.method private static minRadius(FF)F
    .locals 2
    .param p0, "aRadius"    # F
    .param p1, "bRadius"    # F

    .line 1602
    const/4 v0, 0x0

    cmpl-float v1, p0, v0

    if-nez v1, :cond_0

    return p1

    .line 1603
    :cond_0
    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    return p0

    .line 1604
    :cond_1
    invoke-static {p0, p1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method static recalculateClipRevealTranslateYDuration(J)J
    .locals 2
    .param p0, "duration"    # J

    .line 1245
    const-wide/16 v0, 0x32

    sub-long v0, p0, v0

    return-wide v0
.end method

.method static setMiuiAnimSupportInset(Landroid/graphics/Rect;)V
    .locals 1
    .param p0, "inset"    # Landroid/graphics/Rect;

    .line 1071
    if-nez p0, :cond_0

    .line 1072
    sget-object v0, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_0

    .line 1074
    :cond_0
    sget-object v0, Lcom/android/server/wm/AppTransitionInjector;->sMiuiAnimSupportInset:Landroid/graphics/Rect;

    invoke-virtual {v0, p0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1076
    :goto_0
    return-void
.end method

.method private static updateToTranslucentAnimIfNeeded(I)I
    .locals 1
    .param p0, "transit"    # I

    .line 1110
    const/16 v0, 0x18

    if-ne p0, v0, :cond_0

    .line 1111
    const v0, 0x10a0012

    return v0

    .line 1113
    :cond_0
    const/16 v0, 0x19

    if-ne p0, v0, :cond_1

    .line 1114
    const v0, 0x10a0011

    return v0

    .line 1116
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method static useDefaultAnimationAttr(Landroid/view/WindowManager$LayoutParams;)Z
    .locals 2
    .param p0, "lp"    # Landroid/view/WindowManager$LayoutParams;

    .line 1201
    if-nez p0, :cond_0

    .line 1202
    const/4 v0, 0x0

    return v0

    .line 1205
    :cond_0
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget v0, p0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    const/high16 v1, -0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 1208
    :cond_1
    const-string v0, "android"

    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 1206
    :cond_2
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method static useDefaultAnimationAttr(Landroid/view/WindowManager$LayoutParams;I)Z
    .locals 2
    .param p0, "lp"    # Landroid/view/WindowManager$LayoutParams;
    .param p1, "resId"    # I

    .line 1213
    if-nez p0, :cond_0

    .line 1214
    const/4 v0, 0x0

    return v0

    .line 1217
    :cond_0
    iget-object v0, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_2

    const/high16 v0, -0x1000000

    and-int/2addr v0, p1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 1220
    :cond_1
    const-string v0, "android"

    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 1218
    :cond_2
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method static useDefaultAnimationAttr(Landroid/view/WindowManager$LayoutParams;IIZZ)Z
    .locals 3
    .param p0, "lp"    # Landroid/view/WindowManager$LayoutParams;
    .param p1, "resId"    # I
    .param p2, "transit"    # I
    .param p3, "enter"    # Z
    .param p4, "isFreeForm"    # Z

    .line 1224
    const/4 v0, 0x1

    if-eqz p4, :cond_1

    const/4 v1, 0x6

    if-eq p2, v1, :cond_0

    const/16 v1, 0x18

    if-eq p2, v1, :cond_0

    const/4 v1, 0x7

    if-eq p2, v1, :cond_0

    const/16 v1, 0x19

    if-ne p2, v1, :cond_1

    .line 1231
    :cond_0
    return v0

    .line 1233
    :cond_1
    if-nez p0, :cond_2

    .line 1234
    const/4 v0, 0x0

    return v0

    .line 1237
    :cond_2
    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_4

    const/high16 v1, -0x1000000

    and-int/2addr v1, p1

    const/high16 v2, 0x1000000

    if-eq v1, v2, :cond_4

    if-eqz p3, :cond_3

    const/16 v1, 0xc

    if-ne p2, v1, :cond_3

    if-nez p1, :cond_3

    goto :goto_0

    .line 1241
    :cond_3
    const-string v0, "android"

    iget-object v1, p0, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 1239
    :cond_4
    :goto_0
    return v0
.end method
