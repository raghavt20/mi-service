.class Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;
.super Ljava/lang/Object;
.source "MiuiOrientationImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiOrientationImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SmartOrientationPolicy"
.end annotation


# static fields
.field private static final MIUI_SMART_ORIEANTATION_ACTIVITY_KEY:Ljava/lang/String; = "smart_orientation_activity_list"

.field private static final MIUI_SMART_ORIENTATION_KEY:Ljava/lang/String; = "smart_orientation_list"

.field private static final MOULD_NAME:Ljava/lang/String; = "miuiSmartOrientation"

.field private static final TAG:Ljava/lang/String; = "SmartOrientationPolicy"


# instance fields
.field private final mActivityList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

.field private final mContext:Landroid/content/Context;

.field private final mPackageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mResources:Landroid/content/res/Resources;

.field final synthetic this$0:Lcom/android/server/wm/MiuiOrientationImpl;


# direct methods
.method static bridge synthetic -$$Nest$mupdateListFromCloud(Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updateListFromCloud()V

    return-void
.end method

.method constructor <init>(Lcom/android/server/wm/MiuiOrientationImpl;Landroid/content/Context;Lcom/android/server/wm/ActivityTaskManagerServiceImpl;)V
    .locals 1
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiOrientationImpl;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "atmServiceImpl"    # Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 1312
    iput-object p1, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1313
    iput-object p3, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mAtmServiceImpl:Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    .line 1314
    iput-object p2, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mContext:Landroid/content/Context;

    .line 1315
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mResources:Landroid/content/res/Resources;

    .line 1317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    .line 1318
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    .line 1319
    return-void
.end method

.method private clearList()V
    .locals 1

    .line 1360
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1361
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1362
    return-void
.end method

.method private registerDataObserver()V
    .locals 4

    .line 1326
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->this$0:Lcom/android/server/wm/MiuiOrientationImpl;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiOrientationImpl;->isSmartOrientEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1327
    :cond_0
    invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updatePolicyListFromLocal()V

    .line 1328
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1329
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy$1;

    .line 1330
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy$1;-><init>(Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;Landroid/os/Handler;)V

    .line 1328
    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1337
    return-void
.end method

.method private updateListFromCloud()V
    .locals 3

    .line 1354
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "smart_orientation_list"

    const-string v2, "miuiSmartOrientation"

    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updateListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "smart_orientation_activity_list"

    invoke-direct {p0, v0, v2, v1}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->updateListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1357
    return-void
.end method

.method private updateListFromCloud(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "moduleName"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;

    .line 1366
    const-string v0, "SmartOrientationPolicy"

    .line 1367
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1366
    const/4 v2, 0x0

    invoke-static {v1, p2, p3, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1368
    .local v1, "data":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateListFromCloud: data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " moduleName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " key="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1370
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1371
    return-void

    .line 1373
    :cond_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1374
    .local v2, "apps":Lorg/json/JSONArray;
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string/jumbo v3, "smart_orientation_activity_list"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_1
    const-string/jumbo v3, "smart_orientation_list"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :goto_0
    const/4 v3, -0x1

    :goto_1
    const-string/jumbo v4, "updateListFromCloud: mCloudList: "

    packed-switch v3, :pswitch_data_0

    goto :goto_4

    .line 1383
    :pswitch_0
    :try_start_1
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1384
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 1385
    iget-object v5, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1384
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1387
    .end local v3    # "i":I
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1388
    goto :goto_4

    .line 1376
    :pswitch_1
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1377
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 1378
    iget-object v5, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1377
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1380
    .end local v3    # "i":I
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1381
    nop

    .line 1395
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "apps":Lorg/json/JSONArray;
    :goto_4
    goto :goto_5

    .line 1393
    :catch_0
    move-exception v1

    .line 1394
    .local v1, "e":Lorg/json/JSONException;
    const-string v2, "exception when updateListFromCloud: "

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1396
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_5
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x78bb729d -> :sswitch_1
        -0x47fc4757 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private updatePolicyListFromLocal()V
    .locals 5

    .line 1340
    invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->clearList()V

    .line 1341
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mResources:Landroid/content/res/Resources;

    .line 1342
    const v1, 0x110300c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 1343
    .local v0, "packages":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 1344
    iget-object v2, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    aget-object v3, v0, v1

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1343
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1346
    .end local v1    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mResources:Landroid/content/res/Resources;

    .line 1347
    const v2, 0x110300c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 1348
    .local v1, "activities":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v3, v1

    if-ge v2, v3, :cond_1

    .line 1349
    iget-object v3, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    aget-object v4, v1, v2

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1348
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1351
    .end local v2    # "i":I
    :cond_1
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 6
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .line 1413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1414
    .local v0, "innerPrefix":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const-string v2, "] "

    const-string v3, "["

    if-nez v1, :cond_0

    .line 1415
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "SmartOrientationPackageList(Package)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1416
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1417
    .local v4, "packages":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1418
    .end local v4    # "packages":Ljava/lang/String;
    goto :goto_0

    .line 1420
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1421
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1422
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "SmartOrientationActivityList(Activity)"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1423
    iget-object v1, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1424
    .local v4, "activities":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1425
    .end local v4    # "activities":Ljava/lang/String;
    goto :goto_1

    .line 1427
    :cond_1
    return-void
.end method

.method init()V
    .locals 0

    .line 1322
    invoke-direct {p0}, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->registerDataObserver()V

    .line 1323
    return-void
.end method

.method public isNeedAccSensor(Ljava/lang/String;)Z
    .locals 1
    .param p1, "activityName"    # Ljava/lang/String;

    .line 1406
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mActivityList:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1407
    const/4 v0, 0x1

    return v0

    .line 1409
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isSupportSmartOrientation(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1399
    iget-object v0, p0, Lcom/android/server/wm/MiuiOrientationImpl$SmartOrientationPolicy;->mPackageList:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1400
    const/4 v0, 0x1

    return v0

    .line 1402
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
