class com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList {
	 /* .source "MiuiWindowMonitorImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiWindowMonitorImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ProcessWindowList" */
} // .end annotation
/* # static fields */
private static final java.lang.String KEY_COUNT;
private static final java.lang.String KEY_CURRENT_COUNT;
private static final java.lang.String KEY_KILLED;
private static final java.lang.String KEY_LEAK_TYPE;
private static final java.lang.String KEY_MAXCOUNT;
private static final java.lang.String KEY_PACKAGE;
private static final java.lang.String KEY_PID;
private static final java.lang.String KEY_RESOURCE_TYPE;
private static final java.lang.String KEY_TYPE;
private static final java.lang.String KEY_TYPES;
private static final java.lang.String KEY_WARNED;
private static final java.lang.String KEY_WINDOWS;
private static final java.lang.String LEAK_TYPES;
private static final java.lang.String RESOURCE_TYPE;
/* # instance fields */
private Integer mCurCount;
private Integer mLastReportCount;
private Integer mLastWarnCount;
private Integer mLeakType;
private Integer mMaxCount;
private java.lang.String mPackageName;
private Integer mPid;
private Boolean mWarned;
private Boolean mWillKill;
private android.util.SparseArray mWindowsByType;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;", */
/* ">;>;>;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ( ) {
/* .locals 3 */
/* .line 721 */
final String v0 = "application"; // const-string v0, "application"
/* const-string/jumbo v1, "system" */
final String v2 = "none"; // const-string v2, "none"
/* filled-new-array {v2, v0, v1}, [Ljava/lang/String; */
return;
} // .end method
public com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ( ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 751 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 723 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mWindowsByType = v0;
/* .line 743 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I */
/* .line 746 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z */
/* .line 749 */
/* iput-boolean v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWillKill:Z */
/* .line 752 */
/* iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I */
/* .line 753 */
this.mPackageName = p2;
/* .line 754 */
return;
} // .end method
/* # virtual methods */
Integer getCurrentCount ( ) {
/* .locals 1 */
/* .line 844 */
/* iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I */
} // .end method
Integer getLastReportCount ( ) {
/* .locals 1 */
/* .line 856 */
/* iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastReportCount:I */
} // .end method
Integer getLastWarnCount ( ) {
/* .locals 1 */
/* .line 864 */
/* iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastWarnCount:I */
} // .end method
Integer getLeakType ( ) {
/* .locals 1 */
/* .line 876 */
/* iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I */
} // .end method
Integer getMaxCount ( ) {
/* .locals 1 */
/* .line 840 */
/* iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I */
} // .end method
java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 852 */
v0 = this.mPackageName;
} // .end method
Integer getPid ( ) {
/* .locals 1 */
/* .line 848 */
/* iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I */
} // .end method
java.lang.String getTopTypeWindows ( ) {
/* .locals 10 */
/* .line 896 */
int v0 = 0; // const/4 v0, 0x0
/* .line 897 */
/* .local v0, "top":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
int v1 = -1; // const/4 v1, -0x1
/* .line 898 */
/* .local v1, "topCount":I */
int v2 = -1; // const/4 v2, -0x1
/* .line 900 */
/* .local v2, "topType":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = this.mWindowsByType;
v4 = (( android.util.SparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->size()I
/* .local v4, "size":I */
} // :goto_0
/* if-ge v3, v4, :cond_4 */
/* .line 901 */
v5 = this.mWindowsByType;
/* .line 902 */
(( android.util.SparseArray ) v5 ).valueAt ( v3 ); // invoke-virtual {v5, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Ljava/util/HashMap; */
/* .line 903 */
/* .local v5, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
if ( v5 != null) { // if-eqz v5, :cond_3
v6 = (( java.util.HashMap ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->size()I
/* if-gtz v6, :cond_0 */
/* .line 904 */
/* .line 906 */
} // :cond_0
int v6 = 0; // const/4 v6, 0x0
/* .line 907 */
/* .local v6, "typeCount":I */
(( java.util.HashMap ) v5 ).values ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v8 = } // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_2
/* check-cast v8, Ljava/util/HashMap; */
/* .line 908 */
/* .local v8, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;" */
if ( v8 != null) { // if-eqz v8, :cond_1
v9 = (( java.util.HashMap ) v8 ).size ( ); // invoke-virtual {v8}, Ljava/util/HashMap;->size()I
/* if-lez v9, :cond_1 */
/* .line 909 */
v9 = (( java.util.HashMap ) v8 ).size ( ); // invoke-virtual {v8}, Ljava/util/HashMap;->size()I
/* add-int/2addr v6, v9 */
/* .line 911 */
} // .end local v8 # "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
} // :cond_1
/* .line 912 */
} // :cond_2
/* if-le v6, v1, :cond_3 */
/* .line 913 */
/* move v1, v6 */
/* .line 914 */
/* move-object v0, v5 */
/* .line 915 */
v7 = this.mWindowsByType;
v2 = (( android.util.SparseArray ) v7 ).keyAt ( v3 ); // invoke-virtual {v7, v3}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 900 */
} // .end local v5 # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
} // .end local v6 # "typeCount":I
} // :cond_3
} // :goto_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 919 */
} // .end local v3 # "i":I
} // .end local v4 # "size":I
} // :cond_4
if ( v0 != null) { // if-eqz v0, :cond_8
v3 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* if-gtz v3, :cond_5 */
/* .line 923 */
} // :cond_5
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 924 */
/* .local v3, "result":Ljava/lang/StringBuilder; */
(( java.util.HashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_7
/* check-cast v5, Ljava/lang/String; */
/* .line 925 */
/* .local v5, "name":Ljava/lang/String; */
v6 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v6, :cond_6 */
/* .line 926 */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v7, "|" */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 928 */
} // .end local v5 # "name":Ljava/lang/String;
} // :cond_6
/* .line 929 */
} // :cond_7
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 920 */
} // .end local v3 # "result":Ljava/lang/StringBuilder;
} // :cond_8
} // :goto_4
final String v3 = "no window"; // const-string v3, "no window"
} // .end method
android.util.SparseArray getWindows ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/HashMap<", */
/* "Landroid/os/IBinder;", */
/* "Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;", */
/* ">;>;>;" */
/* } */
} // .end annotation
/* .line 892 */
v0 = this.mWindowsByType;
} // .end method
void put ( android.os.IBinder p0, com.android.server.wm.MiuiWindowMonitorImpl$WindowInfo p1 ) {
/* .locals 4 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "info" # Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo; */
/* .line 933 */
/* iget v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I */
/* iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I */
/* if-ne v0, v1, :cond_4 */
v0 = this.mPackageName;
v1 = this.mPackageName;
v0 = android.text.TextUtils .equals ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 940 */
} // :cond_0
v0 = this.mWindowsByType;
/* iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I */
/* .line 941 */
(( android.util.SparseArray ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/HashMap; */
/* .line 942 */
/* .local v0, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
/* if-nez v0, :cond_1 */
/* .line 943 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
/* move-object v0, v1 */
/* .line 944 */
v1 = this.mWindowsByType;
/* iget v2, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I */
(( android.util.SparseArray ) v1 ).put ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 946 */
} // :cond_1
v1 = this.mWindowName;
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/HashMap; */
/* .line 947 */
/* .local v1, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;" */
/* if-nez v1, :cond_2 */
/* .line 948 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
/* move-object v1, v2 */
/* .line 949 */
v2 = this.mWindowName;
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 951 */
} // :cond_2
(( java.util.HashMap ) v1 ).put ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 952 */
/* iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I */
/* .line 955 */
v2 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p0 ).getCurrentCount ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
/* .line 956 */
/* .local v2, "count":I */
/* iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I */
/* if-le v2, v3, :cond_3 */
/* .line 957 */
/* iput v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I */
/* .line 959 */
} // :cond_3
return;
/* .line 934 */
} // .end local v0 # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
} // .end local v1 # "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
} // .end local v2 # "count":I
} // :cond_4
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "pid not match, mPid="; // const-string v1, "pid not match, mPid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", info.pid="; // const-string v1, ", info.pid="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mPackage="; // const-string v1, ", mPackage="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", info.package="; // const-string v1, ", info.package="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", info.window="; // const-string v1, ", info.window="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mWindowName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", info.type="; // const-string v1, ", info.type="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p2, Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;->mType:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiWindowMonitorImpl"; // const-string v1, "MiuiWindowMonitorImpl"
android.util.Slog .w ( v1,v0 );
/* .line 937 */
return;
} // .end method
void remove ( Integer p0, java.lang.String p1, android.os.IBinder p2 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "name" # Ljava/lang/String; */
/* .param p3, "token" # Landroid/os/IBinder; */
/* .line 962 */
v0 = this.mWindowsByType;
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/HashMap; */
/* .line 963 */
/* .local v0, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
/* if-nez v0, :cond_0 */
/* .line 964 */
return;
/* .line 966 */
} // :cond_0
(( java.util.HashMap ) v0 ).get ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/util/HashMap; */
/* .line 967 */
/* .local v1, "windows":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;" */
/* if-nez v1, :cond_1 */
/* .line 968 */
return;
/* .line 970 */
} // :cond_1
(( java.util.HashMap ) v1 ).remove ( p3 ); // invoke-virtual {v1, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 971 */
/* iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I */
/* add-int/lit8 v2, v2, -0x1 */
/* iput v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mCurCount:I */
/* .line 972 */
v2 = (( java.util.HashMap ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->size()I
/* if-nez v2, :cond_2 */
/* .line 973 */
(( java.util.HashMap ) v0 ).remove ( p2 ); // invoke-virtual {v0, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 975 */
} // :cond_2
v2 = (( java.util.HashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->size()I
/* if-nez v2, :cond_3 */
/* .line 976 */
v2 = this.mWindowsByType;
(( android.util.SparseArray ) v2 ).remove ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 978 */
} // :cond_3
return;
} // .end method
void setLastReportCount ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "lastReportCount" # I */
/* .line 860 */
/* iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastReportCount:I */
/* .line 861 */
return;
} // .end method
void setLastWarnCount ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "lastWarnCount" # I */
/* .line 868 */
/* iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLastWarnCount:I */
/* .line 869 */
return;
} // .end method
void setLeakType ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "leakType" # I */
/* .line 872 */
/* iput p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I */
/* .line 873 */
return;
} // .end method
void setWarned ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "warned" # Z */
/* .line 880 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z */
/* .line 881 */
return;
} // .end method
void setWillKill ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "willKill" # Z */
/* .line 888 */
/* iput-boolean p1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWillKill:Z */
/* .line 889 */
return;
} // .end method
org.json.JSONObject toJson ( ) {
/* .locals 15 */
/* .line 788 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 789 */
/* .local v0, "jobj":Lorg/json/JSONObject; */
/* new-instance v1, Lorg/json/JSONArray; */
/* invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V */
/* .line 791 */
/* .local v1, "jarray":Lorg/json/JSONArray; */
try { // :try_start_0
final String v2 = "resourceType"; // const-string v2, "resourceType"
/* const-string/jumbo v3, "window" */
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 792 */
final String v2 = "pid"; // const-string v2, "pid"
/* iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I */
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 793 */
final String v2 = "package"; // const-string v2, "package"
v3 = this.mPackageName;
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 794 */
final String v2 = "maxCount"; // const-string v2, "maxCount"
/* iget v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I */
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 795 */
final String v2 = "leakType"; // const-string v2, "leakType"
v3 = com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList.LEAK_TYPES;
/* iget v4, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mLeakType:I */
/* aget-object v3, v3, v4 */
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 796 */
/* const-string/jumbo v2, "warned" */
/* iget-boolean v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z */
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
/* .line 797 */
final String v2 = "killed"; // const-string v2, "killed"
/* iget-boolean v3, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWillKill:Z */
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
/* .line 799 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = this.mWindowsByType;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* .local v3, "size":I */
} // :goto_0
/* if-ge v2, v3, :cond_4 */
/* .line 800 */
v4 = this.mWindowsByType;
/* .line 801 */
(( android.util.SparseArray ) v4 ).valueAt ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Ljava/util/HashMap; */
/* .line 802 */
/* .local v4, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
v5 = this.mWindowsByType;
v5 = (( android.util.SparseArray ) v5 ).keyAt ( v2 ); // invoke-virtual {v5, v2}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 803 */
/* .local v5, "windowType":I */
if ( v4 != null) { // if-eqz v4, :cond_3
v6 = (( java.util.HashMap ) v4 ).size ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->size()I
/* if-lez v6, :cond_3 */
/* .line 804 */
int v6 = 0; // const/4 v6, 0x0
/* .line 805 */
/* .local v6, "typeCount":I */
/* new-instance v7, Lorg/json/JSONObject; */
/* invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V */
/* .line 806 */
/* .local v7, "typeObj":Lorg/json/JSONObject; */
/* new-instance v8, Lorg/json/JSONObject; */
/* invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V */
/* .line 808 */
/* .local v8, "nameObj":Lorg/json/JSONObject; */
(( java.util.HashMap ) v4 ).entrySet ( ); // invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v10 = } // :goto_1
if ( v10 != null) { // if-eqz v10, :cond_2
/* check-cast v10, Ljava/util/Map$Entry; */
/* .line 809 */
/* .local v10, "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
/* check-cast v11, Ljava/lang/String; */
/* .line 810 */
/* .local v11, "name":Ljava/lang/String; */
/* check-cast v12, Ljava/util/HashMap; */
/* .line 811 */
/* .local v12, "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;" */
int v13 = 0; // const/4 v13, 0x0
/* .line 812 */
/* .local v13, "nameCount":I */
if ( v12 != null) { // if-eqz v12, :cond_0
/* .line 813 */
v14 = (( java.util.HashMap ) v12 ).size ( ); // invoke-virtual {v12}, Ljava/util/HashMap;->size()I
/* move v13, v14 */
/* .line 815 */
} // :cond_0
/* if-lez v13, :cond_1 */
/* .line 816 */
/* add-int/2addr v6, v13 */
/* .line 817 */
(( org.json.JSONObject ) v8 ).put ( v11, v13 ); // invoke-virtual {v8, v11, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 819 */
} // .end local v10 # "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
} // .end local v11 # "name":Ljava/lang/String;
} // .end local v12 # "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
} // .end local v13 # "nameCount":I
} // :cond_1
/* .line 820 */
} // :cond_2
/* if-lez v6, :cond_3 */
/* .line 821 */
/* const-string/jumbo v9, "type" */
(( org.json.JSONObject ) v7 ).put ( v9, v5 ); // invoke-virtual {v7, v9, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 822 */
final String v9 = "count"; // const-string v9, "count"
(( org.json.JSONObject ) v7 ).put ( v9, v6 ); // invoke-virtual {v7, v9, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 823 */
/* const-string/jumbo v9, "windows" */
(( org.json.JSONObject ) v7 ).put ( v9, v8 ); // invoke-virtual {v7, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 824 */
(( org.json.JSONArray ) v1 ).put ( v7 ); // invoke-virtual {v1, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
/* .line 799 */
} // .end local v4 # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
} // .end local v5 # "windowType":I
} // .end local v6 # "typeCount":I
} // .end local v7 # "typeObj":Lorg/json/JSONObject;
} // .end local v8 # "nameObj":Lorg/json/JSONObject;
} // :cond_3
/* add-int/lit8 v2, v2, 0x1 */
/* .line 828 */
} // .end local v2 # "i":I
} // .end local v3 # "size":I
} // :cond_4
final String v2 = "curCount"; // const-string v2, "curCount"
v3 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p0 ).getCurrentCount ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
(( org.json.JSONObject ) v0 ).put ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
/* .line 829 */
v2 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p0 ).getCurrentCount ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
/* if-lez v2, :cond_5 */
/* .line 830 */
/* const-string/jumbo v2, "types" */
(( org.json.JSONObject ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 835 */
} // :cond_5
/* nop */
/* .line 836 */
/* .line 832 */
/* :catch_0 */
/* move-exception v2 */
/* .line 833 */
/* .local v2, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
/* .line 834 */
int v3 = 0; // const/4 v3, 0x0
} // .end method
public java.lang.String toString ( ) {
/* .locals 14 */
/* .line 758 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v1 = "pid="; // const-string v1, "pid="
/* invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 759 */
/* .local v0, "builder":Ljava/lang/StringBuilder; */
/* iget v1, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mPid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ","; // const-string v2, ","
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "package="; // const-string v3, "package="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mPackageName;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "\n"; // const-string v3, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 760 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v4 = this.mWindowsByType;
v4 = (( android.util.SparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->size()I
/* .local v4, "size":I */
} // :goto_0
/* if-ge v1, v4, :cond_3 */
/* .line 761 */
v5 = this.mWindowsByType;
/* .line 762 */
(( android.util.SparseArray ) v5 ).valueAt ( v1 ); // invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Ljava/util/HashMap; */
/* .line 763 */
/* .local v5, "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
v6 = this.mWindowsByType;
v6 = (( android.util.SparseArray ) v6 ).keyAt ( v1 ); // invoke-virtual {v6, v1}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 764 */
/* .local v6, "windowType":I */
if ( v5 != null) { // if-eqz v5, :cond_2
v7 = (( java.util.HashMap ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->size()I
/* if-lez v7, :cond_2 */
/* .line 765 */
final String v7 = " Type:"; // const-string v7, " Type:"
(( java.lang.StringBuilder ) v0 ).append ( v7 ); // invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = ",count="; // const-string v8, ",count="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 766 */
v9 = (( java.util.HashMap ) v5 ).size ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->size()I
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 768 */
(( java.util.HashMap ) v5 ).entrySet ( ); // invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_2
/* check-cast v9, Ljava/util/Map$Entry; */
/* .line 769 */
/* .local v9, "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;" */
/* check-cast v10, Ljava/lang/String; */
/* .line 770 */
/* .local v10, "name":Ljava/lang/String; */
/* check-cast v11, Ljava/util/HashMap; */
/* .line 771 */
/* .local v11, "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;" */
int v12 = 0; // const/4 v12, 0x0
/* .line 772 */
/* .local v12, "nameCount":I */
if ( v11 != null) { // if-eqz v11, :cond_0
/* .line 773 */
v12 = (( java.util.HashMap ) v11 ).size ( ); // invoke-virtual {v11}, Ljava/util/HashMap;->size()I
/* .line 775 */
} // :cond_0
/* if-lez v12, :cond_1 */
/* .line 776 */
final String v13 = " Windows:"; // const-string v13, " Windows:"
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v10 ); // invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v8 ); // invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 777 */
(( java.lang.StringBuilder ) v13 ).append ( v12 ); // invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).append ( v3 ); // invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 779 */
} // .end local v9 # "windows":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
} // .end local v10 # "name":Ljava/lang/String;
} // .end local v11 # "value":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;"
} // .end local v12 # "nameCount":I
} // :cond_1
/* .line 760 */
} // .end local v5 # "windowsByName":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashMap<Landroid/os/IBinder;Lcom/android/server/wm/MiuiWindowMonitorImpl$WindowInfo;>;>;"
} // .end local v6 # "windowType":I
} // :cond_2
/* add-int/lit8 v1, v1, 0x1 */
/* .line 782 */
} // .end local v1 # "i":I
} // .end local v4 # "size":I
} // :cond_3
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "currentCount="; // const-string v3, "currentCount="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.android.server.wm.MiuiWindowMonitorImpl$ProcessWindowList ) p0 ).getCurrentCount ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->getCurrentCount()I
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 783 */
final String v2 = "maxCount"; // const-string v2, "maxCount"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "="; // const-string v2, "="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mMaxCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 784 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
Boolean warned ( ) {
/* .locals 1 */
/* .line 884 */
/* iget-boolean v0, p0, Lcom/android/server/wm/MiuiWindowMonitorImpl$ProcessWindowList;->mWarned:Z */
} // .end method
