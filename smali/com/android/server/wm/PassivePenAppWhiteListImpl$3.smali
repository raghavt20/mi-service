.class Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;
.super Landroid/database/ContentObserver;
.source "PassivePenAppWhiteListImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/PassivePenAppWhiteListImpl;->registerCloudControlObserver(Landroid/content/ContentResolver;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

.field final synthetic val$contentResolver:Landroid/content/ContentResolver;

.field final synthetic val$moduleName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/wm/PassivePenAppWhiteListImpl;Landroid/os/Handler;Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/PassivePenAppWhiteListImpl;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 214
    iput-object p1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    iput-object p3, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;->val$contentResolver:Landroid/content/ContentResolver;

    iput-object p4, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;->val$moduleName:Ljava/lang/String;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .line 217
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 218
    const-string v0, "PassivePenAppWhiteListImpl"

    const-string v1, "cloud data has update"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v0, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;->this$0:Lcom/android/server/wm/PassivePenAppWhiteListImpl;

    iget-object v1, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;->val$contentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/android/server/wm/PassivePenAppWhiteListImpl$3;->val$moduleName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->-$$Nest$mreadLocalCloudControlData(Lcom/android/server/wm/PassivePenAppWhiteListImpl;Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 220
    return-void
.end method
