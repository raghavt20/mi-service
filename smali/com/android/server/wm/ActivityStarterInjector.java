class com.android.server.wm.ActivityStarterInjector {
	 /* .source "ActivityStarterInjector.java" */
	 /* # static fields */
	 public static final Integer FLAG_ASSOCIATED_SETTINGS_AV;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 com.android.server.wm.ActivityStarterInjector ( ) {
		 /* .locals 0 */
		 /* .line 37 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void checkFreeformSupport ( com.android.server.wm.ActivityTaskManagerService p0, android.app.ActivityOptions p1 ) {
		 /* .locals 2 */
		 /* .param p0, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
		 /* .param p1, "options" # Landroid/app/ActivityOptions; */
		 /* .line 44 */
		 /* iget-boolean v0, p0, Lcom/android/server/wm/ActivityTaskManagerService;->mSupportsFreeformWindowManagement:Z */
		 /* if-nez v0, :cond_2 */
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* .line 46 */
			 v0 = 			 (( android.app.ActivityOptions ) p1 ).getLaunchWindowingMode ( ); // invoke-virtual {p1}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
			 int v1 = 5; // const/4 v1, 0x5
			 /* if-eq v0, v1, :cond_1 */
		 } // :cond_0
		 v0 = this.mContext;
		 /* .line 48 */
		 v0 = 		 com.android.server.wm.MiuiDesktopModeUtils .isActive ( v0 );
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* .line 56 */
		 } // :cond_1
		 v0 = this.mMiuiFreeFormManagerService;
		 /* .line 58 */
	 } // :cond_2
	 return;
} // .end method
public static Boolean getLastFrame ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p0, "name" # Ljava/lang/String; */
	 /* .line 202 */
	 final String v0 = "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"; // const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.VideoInviteActivity"
	 v0 = 	 (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 /* if-nez v0, :cond_1 */
	 /* .line 203 */
	 final String v0 = "com.tencent.mm/.plugin.voip.ui.VideoActivity"; // const-string v0, "com.tencent.mm/.plugin.voip.ui.VideoActivity"
	 v0 = 	 (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 /* if-nez v0, :cond_1 */
	 /* .line 204 */
	 final String v0 = "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"; // const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVActivity"
	 v0 = 	 (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 /* if-nez v0, :cond_1 */
	 /* .line 205 */
	 final String v0 = "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"; // const-string v0, "com.tencent.mobileqq/com.tencent.av.ui.AVLoadingDialogActivity"
	 v0 = 	 (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 /* if-nez v0, :cond_1 */
	 /* .line 206 */
	 final String v0 = "com.android.incallui/.InCallActivity"; // const-string v0, "com.android.incallui/.InCallActivity"
	 v0 = 	 (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 /* if-nez v0, :cond_1 */
	 /* .line 207 */
	 final String v0 = "com.google.android.dialer/com.android.incallui.InCallActivity"; // const-string v0, "com.google.android.dialer/com.android.incallui.InCallActivity"
	 v0 = 	 (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 /* if-nez v0, :cond_1 */
	 /* .line 208 */
	 /* const-string/jumbo v0, "voipcalling.VoipActivityV2" */
	 v0 = 	 (( java.lang.String ) p0 ).contains ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 211 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 209 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private static com.android.server.wm.ActivityRecord getLastFreeFormActivityRecord ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/wm/ActivityRecord;", */
/* ">;)", */
/* "Lcom/android/server/wm/ActivityRecord;" */
/* } */
} // .end annotation
/* .line 241 */
/* .local p0, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;" */
v0 = if ( p0 != null) { // if-eqz p0, :cond_0
v0 = /* if-nez v0, :cond_0 */
/* add-int/lit8 v0, v0, -0x1 */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* .line 242 */
/* add-int/lit8 v0, v0, -0x1 */
/* check-cast v0, Lcom/android/server/wm/ActivityRecord; */
v0 = (( com.android.server.wm.ActivityRecord ) v0 ).getWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityRecord;->getWindowingMode()I
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_0 */
v0 = /* .line 243 */
/* add-int/lit8 v0, v0, -0x1 */
/* check-cast v0, Lcom/android/server/wm/ActivityRecord; */
/* .line 245 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Boolean isStartedInMiuiSetttingVirtualDispaly ( com.android.server.wm.RootWindowContainer p0, android.content.Intent p1, android.content.pm.ActivityInfo p2 ) {
/* .locals 4 */
/* .param p0, "root" # Lcom/android/server/wm/RootWindowContainer; */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .line 231 */
int v0 = 0; // const/4 v0, 0x0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 232 */
(( com.android.server.wm.RootWindowContainer ) p0 ).findActivity ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lcom/android/server/wm/RootWindowContainer;->findActivity(Landroid/content/Intent;Landroid/content/pm/ActivityInfo;Z)Lcom/android/server/wm/ActivityRecord;
/* .line 233 */
/* .local v1, "result":Lcom/android/server/wm/ActivityRecord; */
if ( v1 != null) { // if-eqz v1, :cond_0
	 v2 = this.intent;
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 v2 = this.intent;
		 v2 = 		 (( android.content.Intent ) v2 ).getMiuiFlags ( ); // invoke-virtual {v2}, Landroid/content/Intent;->getMiuiFlags()I
		 /* const/high16 v3, 0x8000000 */
		 /* and-int/2addr v2, v3 */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 234 */
			 int v0 = 1; // const/4 v0, 0x1
			 /* .line 237 */
		 } // .end local v1 # "result":Lcom/android/server/wm/ActivityRecord;
	 } // :cond_0
} // .end method
public static android.app.ActivityOptions modifyLaunchActivityOptionIfNeed ( com.android.server.wm.ActivityTaskManagerService p0, com.android.server.wm.RootWindowContainer p1, java.lang.String p2, android.app.ActivityOptions p3, com.android.server.wm.WindowProcessController p4, android.content.Intent p5, Integer p6, android.content.pm.ActivityInfo p7, com.android.server.wm.ActivityRecord p8 ) {
	 /* .locals 14 */
	 /* .param p0, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
	 /* .param p1, "root" # Lcom/android/server/wm/RootWindowContainer; */
	 /* .param p2, "callingPackgae" # Ljava/lang/String; */
	 /* .param p3, "options" # Landroid/app/ActivityOptions; */
	 /* .param p4, "callerApp" # Lcom/android/server/wm/WindowProcessController; */
	 /* .param p5, "intent" # Landroid/content/Intent; */
	 /* .param p6, "userId" # I */
	 /* .param p7, "aInfo" # Landroid/content/pm/ActivityInfo; */
	 /* .param p8, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
	 /* .line 167 */
	 /* move-object/from16 v8, p3 */
	 /* move-object/from16 v9, p5 */
	 /* move-object v10, p1 */
	 /* move-object/from16 v11, p7 */
	 v0 = 	 com.android.server.wm.ActivityStarterInjector .isStartedInMiuiSetttingVirtualDispaly ( p1,v9,v11 );
	 int v1 = 0; // const/4 v1, 0x0
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 if ( v8 != null) { // if-eqz v8, :cond_0
			 /* .line 168 */
			 v0 = 			 /* invoke-virtual/range {p3 ..p3}, Landroid/app/ActivityOptions;->getLaunchDisplayId()I */
			 int v2 = -1; // const/4 v2, -0x1
			 /* if-ne v0, v2, :cond_0 */
			 /* .line 169 */
			 (( android.app.ActivityOptions ) v8 ).setLaunchDisplayId ( v1 ); // invoke-virtual {v8, v1}, Landroid/app/ActivityOptions;->setLaunchDisplayId(I)Landroid/app/ActivityOptions;
			 /* .line 171 */
		 } // :cond_0
		 int v12 = 0; // const/4 v12, 0x0
		 /* .line 172 */
		 /* .local v12, "pcRun":Z */
		 /* if-nez v12, :cond_1 */
		 if ( v9 != null) { // if-eqz v9, :cond_1
			 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 v0 = android.util.MiuiMultiWindowAdapter.START_FROM_FREEFORM_BLACK_LIST_ACTIVITY;
				 /* .line 174 */
				 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
				 v0 = 				 (( android.content.ComponentName ) v2 ).flattenToShortString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
				 if ( v0 != null) { // if-eqz v0, :cond_1
					 /* .line 177 */
					 /* .line 179 */
				 } // :cond_1
				 int v0 = 0; // const/4 v0, 0x0
				 /* .line 180 */
				 /* .local v0, "ar":Lcom/android/server/wm/ActivityRecord; */
				 (( com.android.server.wm.RootWindowContainer ) p1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
				 if ( v2 != null) { // if-eqz v2, :cond_2
					 /* .line 181 */
					 (( com.android.server.wm.RootWindowContainer ) p1 ).getDefaultTaskDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/RootWindowContainer;->getDefaultTaskDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
					 int v3 = 1; // const/4 v3, 0x1
					 (( com.android.server.wm.TaskDisplayArea ) v2 ).getTopRootTaskInWindowingMode ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/wm/TaskDisplayArea;->getTopRootTaskInWindowingMode(I)Lcom/android/server/wm/Task;
					 /* .line 182 */
					 /* .local v2, "topTask":Lcom/android/server/wm/Task; */
					 if ( v2 != null) { // if-eqz v2, :cond_2
						 /* .line 183 */
						 (( com.android.server.wm.Task ) v2 ).getTopActivity ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
						 /* move-object v13, v0 */
						 /* .line 186 */
					 } // .end local v2 # "topTask":Lcom/android/server/wm/Task;
				 } // :cond_2
				 /* move-object v13, v0 */
			 } // .end local v0 # "ar":Lcom/android/server/wm/ActivityRecord;
			 /* .local v13, "ar":Lcom/android/server/wm/ActivityRecord; */
		 } // :goto_0
		 if ( v8 != null) { // if-eqz v8, :cond_3
			 v0 = 			 /* invoke-virtual/range {p3 ..p3}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I */
			 int v2 = 5; // const/4 v2, 0x5
			 /* if-ne v0, v2, :cond_3 */
			 if ( v13 != null) { // if-eqz v13, :cond_3
				 v0 = this.mActivityComponent;
				 if ( v0 != null) { // if-eqz v0, :cond_3
					 v0 = android.util.MiuiMultiWindowAdapter.LIST_ABOUT_LOCK_MODE_ACTIVITY;
					 v2 = this.mActivityComponent;
					 /* .line 188 */
					 v0 = 					 (( android.content.ComponentName ) v2 ).flattenToString ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
					 if ( v0 != null) { // if-eqz v0, :cond_3
						 /* .line 189 */
						 (( android.app.ActivityOptions ) v8 ).setLaunchWindowingMode ( v1 ); // invoke-virtual {v8, v1}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
						 /* .line 190 */
						 /* .line 192 */
					 } // :cond_3
					 if ( v9 != null) { // if-eqz v9, :cond_4
						 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
						 if ( v0 != null) { // if-eqz v0, :cond_4
							 com.android.server.wm.MiuiFreeformUtilStub .getInstance ( );
							 /* .line 193 */
							 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
							 v0 = 							 (( android.content.ComponentName ) v1 ).flattenToShortString ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
							 if ( v0 != null) { // if-eqz v0, :cond_4
								 /* .line 194 */
								 final String v0 = "ActivityStarter"; // const-string v0, "ActivityStarter"
								 final String v1 = "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed skip due to desktop Full-screen strategy"; // const-string v1, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed skip due to desktop Full-screen strategy"
								 android.util.Slog .d ( v0,v1 );
								 /* .line 195 */
								 /* .line 197 */
							 } // :cond_4
							 /* move-object v0, p1 */
							 /* move-object/from16 v1, p2 */
							 /* move-object/from16 v2, p3 */
							 /* move-object/from16 v3, p4 */
							 /* move/from16 v4, p6 */
							 /* move-object/from16 v5, p5 */
							 /* move v6, v12 */
							 /* move-object/from16 v7, p8 */
							 /* invoke-static/range {v0 ..v7}, Lcom/android/server/wm/ActivityStarterInjector;->modifyLaunchActivityOptionIfNeed(Lcom/android/server/wm/RootWindowContainer;Ljava/lang/String;Landroid/app/ActivityOptions;Lcom/android/server/wm/WindowProcessController;ILandroid/content/Intent;ZLcom/android/server/wm/ActivityRecord;)Landroid/app/ActivityOptions; */
						 } // .end method
						 private static android.app.ActivityOptions modifyLaunchActivityOptionIfNeed ( com.android.server.wm.RootWindowContainer p0, java.lang.String p1, android.app.ActivityOptions p2, com.android.server.wm.WindowProcessController p3, Integer p4, android.content.Intent p5, Boolean p6, com.android.server.wm.ActivityRecord p7 ) {
							 /* .locals 17 */
							 /* .param p0, "root" # Lcom/android/server/wm/RootWindowContainer; */
							 /* .param p1, "callingPackgae" # Ljava/lang/String; */
							 /* .param p2, "options" # Landroid/app/ActivityOptions; */
							 /* .param p3, "callerApp" # Lcom/android/server/wm/WindowProcessController; */
							 /* .param p4, "userId" # I */
							 /* .param p5, "intent" # Landroid/content/Intent; */
							 /* .param p6, "pcRun" # Z */
							 /* .param p7, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
							 /* .line 64 */
							 /* move-object/from16 v1, p5 */
							 int v0 = 0; // const/4 v0, 0x0
							 /* .line 65 */
							 /* .local v0, "startPackageName":Ljava/lang/String; */
							 int v2 = 0; // const/4 v2, 0x0
							 /* .line 66 */
							 /* .local v2, "sourceFfas":Lcom/android/server/wm/MiuiFreeFormActivityStack; */
							 /* move-object/from16 v3, p7 */
							 /* .line 67 */
							 /* .local v3, "sourceFreeFormActivity":Lcom/android/server/wm/ActivityRecord; */
							 if ( v1 != null) { // if-eqz v1, :cond_3
								 /* .line 68 */
								 final String v4 = "android.intent.extra.shortcut.NAME"; // const-string v4, "android.intent.extra.shortcut.NAME"
								 (( android.content.Intent ) v1 ).getStringExtra ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
								 if ( v5 != null) { // if-eqz v5, :cond_0
									 /* .line 69 */
									 (( android.content.Intent ) v1 ).getStringExtra ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
									 /* .line 70 */
								 } // :cond_0
								 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
								 if ( v4 != null) { // if-eqz v4, :cond_1
									 /* .line 71 */
									 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
									 (( android.content.ComponentName ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
									 /* .line 73 */
								 } // :cond_1
							 } // :goto_0
							 /* if-nez v0, :cond_2 */
							 /* .line 74 */
							 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getPackage()Ljava/lang/String; */
							 /* move-object v4, v0 */
							 /* .line 73 */
						 } // :cond_2
						 /* move-object v4, v0 */
						 /* .line 67 */
					 } // :cond_3
					 /* move-object v4, v0 */
					 /* .line 77 */
				 } // .end local v0 # "startPackageName":Ljava/lang/String;
				 /* .local v4, "startPackageName":Ljava/lang/String; */
			 } // :goto_1
			 /* if-nez v3, :cond_5 */
			 if ( p3 != null) { // if-eqz p3, :cond_5
				 /* .line 78 */
				 /* invoke-virtual/range {p3 ..p3}, Lcom/android/server/wm/WindowProcessController;->getActivities()Ljava/util/List; */
				 /* .line 79 */
				 /* .local v0, "callerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;" */
				 /* invoke-virtual/range {p3 ..p3}, Lcom/android/server/wm/WindowProcessController;->getInactiveActivities()Ljava/util/List; */
				 /* .line 80 */
				 /* .local v5, "inactiveActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;" */
				 com.android.server.wm.ActivityStarterInjector .getLastFreeFormActivityRecord ( v0 );
				 if ( v6 != null) { // if-eqz v6, :cond_4
					 /* .line 81 */
					 com.android.server.wm.ActivityStarterInjector .getLastFreeFormActivityRecord ( v0 );
				 } // :cond_4
				 com.android.server.wm.ActivityStarterInjector .getLastFreeFormActivityRecord ( v5 );
			 } // :goto_2
			 /* move-object v3, v6 */
			 /* .line 83 */
		 } // .end local v0 # "callerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
	 } // .end local v5 # "inactiveActivities":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/ActivityRecord;>;"
} // :cond_5
if ( v3 != null) { // if-eqz v3, :cond_6
	 /* .line 84 */
	 v0 = this.mWmService;
	 v0 = this.mAtmService;
	 v0 = this.mMiuiFreeFormManagerService;
	 /* .line 85 */
	 v5 = 	 (( com.android.server.wm.ActivityRecord ) v3 ).getRootTaskId ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getRootTaskId()I
	 /* move-object v2, v0 */
	 /* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
	 /* .line 87 */
} // :cond_6
if ( v1 != null) { // if-eqz v1, :cond_8
	 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
	 if ( v0 != null) { // if-eqz v0, :cond_8
		 /* .line 88 */
		 android.util.MiuiMultiWindowAdapter .getCanStartActivityFromFullscreenToFreefromList ( );
		 /* .line 89 */
		 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
		 v0 = 		 (( android.content.ComponentName ) v5 ).getClassName ( ); // invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
		 if ( v0 != null) { // if-eqz v0, :cond_7
			 /* .line 90 */
			 /* move-object/from16 v5, p0 */
			 v0 = this.mWmService;
			 v0 = this.mAtmService;
			 v0 = this.mMiuiFreeFormManagerService;
			 /* move-object v2, v0 */
			 /* check-cast v2, Lcom/android/server/wm/MiuiFreeFormActivityStack; */
			 /* .line 89 */
		 } // :cond_7
		 /* move-object/from16 v5, p0 */
		 /* .line 87 */
	 } // :cond_8
	 /* move-object/from16 v5, p0 */
	 /* .line 93 */
} // :goto_3
final String v6 = "ActivityStarter"; // const-string v6, "ActivityStarter"
int v7 = 1; // const/4 v7, 0x1
if ( v2 != null) { // if-eqz v2, :cond_d
	 if ( v4 != null) { // if-eqz v4, :cond_d
		 /* .line 94 */
		 (( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getStackPackageName ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getStackPackageName()Ljava/lang/String;
		 v0 = 		 (( java.lang.String ) v4 ).equals ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* if-nez v0, :cond_9 */
		 if ( v3 != null) { // if-eqz v3, :cond_d
			 v0 = this.mActivityComponent;
			 if ( v0 != null) { // if-eqz v0, :cond_d
				 v0 = this.mActivityComponent;
				 /* .line 96 */
				 (( android.content.ComponentName ) v0 ).flattenToString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
				 final String v8 = "com.miui.securitycore/com.miui.xspace.ui.activity.XSpaceResolveActivity"; // const-string v8, "com.miui.securitycore/com.miui.xspace.ui.activity.XSpaceResolveActivity"
				 v0 = 				 (( java.lang.String ) v8 ).equals ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_d
				 } // :cond_9
				 if ( v3 != null) { // if-eqz v3, :cond_d
					 v0 = this.mActivityComponent;
					 if ( v0 != null) { // if-eqz v0, :cond_d
						 v0 = this.mActivityComponent;
						 /* .line 98 */
						 (( android.content.ComponentName ) v0 ).flattenToString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;
						 final String v8 = "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"; // const-string v8, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"
						 v0 = 						 (( java.lang.String ) v8 ).equals ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 /* if-nez v0, :cond_d */
						 /* .line 99 */
						 v0 = this.mWmService;
						 v0 = this.mContext;
						 android.util.MiuiMultiWindowUtils .getActivityOptions ( v0,v4,v7 );
						 /* .line 101 */
					 } // .end local p2 # "options":Landroid/app/ActivityOptions;
					 /* .local v8, "options":Landroid/app/ActivityOptions; */
					 v9 = this.mTask;
					 /* .line 102 */
					 /* .local v9, "sourceTask":Lcom/android/server/wm/Task; */
					 if ( v9 != null) { // if-eqz v9, :cond_b
						 v0 = 						 com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
						 if ( v0 != null) { // if-eqz v0, :cond_b
							 v0 = this.mTaskSupervisor;
							 /* .line 103 */
							 (( com.android.server.wm.ActivityTaskSupervisor ) v0 ).getLaunchParamsController ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
							 v0 = 							 (( com.android.server.wm.LaunchParamsController ) v0 ).hasFreeformDesktopMemory ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/wm/LaunchParamsController;->hasFreeformDesktopMemory(Lcom/android/server/wm/Task;)Z
							 if ( v0 != null) { // if-eqz v0, :cond_b
								 /* .line 104 */
								 v0 = this.mTaskSupervisor;
								 (( com.android.server.wm.ActivityTaskSupervisor ) v0 ).getLaunchParamsController ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
								 (( com.android.server.wm.LaunchParamsController ) v0 ).getFreeformLastPosition ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastPosition(Lcom/android/server/wm/Task;)Landroid/graphics/Rect;
								 /* .line 105 */
								 /* .local v10, "launchBounds":Landroid/graphics/Rect; */
								 (( android.app.ActivityOptions ) v8 ).setLaunchBounds ( v10 ); // invoke-virtual {v8, v10}, Landroid/app/ActivityOptions;->setLaunchBounds(Landroid/graphics/Rect;)Landroid/app/ActivityOptions;
								 /* .line 106 */
								 v0 = this.mTaskSupervisor;
								 (( com.android.server.wm.ActivityTaskSupervisor ) v0 ).getLaunchParamsController ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskSupervisor;->getLaunchParamsController()Lcom/android/server/wm/LaunchParamsController;
								 v11 = 								 (( com.android.server.wm.LaunchParamsController ) v0 ).getFreeformLastScale ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/server/wm/LaunchParamsController;->getFreeformLastScale(Lcom/android/server/wm/Task;)F
								 /* .line 107 */
								 /* .local v11, "scale":F */
								 /* new-instance v0, Ljava/lang/StringBuilder; */
								 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
								 final String v12 = "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed:: modify bounds and scale in dkt launchBounds: "; // const-string v12, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed:: modify bounds and scale in dkt launchBounds: "
								 (( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
								 (( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
								 final String v12 = " scale: "; // const-string v12, " scale: "
								 (( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
								 (( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
								 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
								 android.util.Slog .d ( v6,v0 );
								 /* .line 111 */
								 try { // :try_start_0
									 final String v0 = "getActivityOptionsInjector"; // const-string v0, "getActivityOptionsInjector"
									 int v12 = 0; // const/4 v12, 0x0
									 /* new-array v13, v12, [Ljava/lang/Object; */
									 android.util.MiuiMultiWindowUtils .isMethodExist ( v8,v0,v13 );
									 /* .line 113 */
									 /* .local v0, "method":Ljava/lang/reflect/Method; */
									 if ( v0 != null) { // if-eqz v0, :cond_a
										 /* const/high16 v13, -0x40800000 # -1.0f */
										 /* cmpl-float v13, v11, v13 */
										 if ( v13 != null) { // if-eqz v13, :cond_a
											 /* .line 114 */
											 /* new-array v13, v12, [Ljava/lang/Object; */
											 (( java.lang.reflect.Method ) v0 ).invoke ( v8, v13 ); // invoke-virtual {v0, v8, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
											 /* const-string/jumbo v14, "setFreeformScale" */
											 /* new-array v15, v7, [Ljava/lang/Object; */
											 /* .line 115 */
											 java.lang.Float .valueOf ( v11 );
											 /* aput-object v16, v15, v12 */
											 /* .line 114 */
											 android.util.MiuiMultiWindowUtils .invoke ( v13,v14,v15 );
											 /* :try_end_0 */
											 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
											 /* .line 118 */
										 } // .end local v0 # "method":Ljava/lang/reflect/Method;
									 } // :cond_a
									 /* .line 117 */
									 /* :catch_0 */
									 /* move-exception v0 */
									 /* .line 120 */
								 } // .end local v10 # "launchBounds":Landroid/graphics/Rect;
							 } // .end local v11 # "scale":F
						 } // :cond_b
					 } // :goto_4
					 v0 = 					 (( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getFreeFormLaunchFromTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I
					 if ( v0 != null) { // if-eqz v0, :cond_c
						 /* .line 121 */
						 v0 = 						 (( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getFreeFormLaunchFromTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I
						 (( android.app.ActivityOptions ) v8 ).setLaunchFromTaskId ( v0 ); // invoke-virtual {v8, v0}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V
						 /* .line 123 */
					 } // :cond_c
					 /* new-instance v0, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v10 = "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed:: options = "; // const-string v10, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed:: options = "
					 (( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
					 final String v10 = " sourceFfas: "; // const-string v10, " sourceFfas: "
					 (( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
					 final String v10 = " sourceFreeFormActivity: "; // const-string v10, " sourceFreeFormActivity: "
					 (( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 android.util.Slog .d ( v6,v0 );
					 /* .line 128 */
				 } // .end local v8 # "options":Landroid/app/ActivityOptions;
			 } // .end local v9 # "sourceTask":Lcom/android/server/wm/Task;
			 /* .restart local p2 # "options":Landroid/app/ActivityOptions; */
		 } // :cond_d
		 /* move-object/from16 v8, p2 */
	 } // .end local p2 # "options":Landroid/app/ActivityOptions;
	 /* .restart local v8 # "options":Landroid/app/ActivityOptions; */
} // :goto_5
if ( v2 != null) { // if-eqz v2, :cond_14
	 if ( v4 != null) { // if-eqz v4, :cond_14
		 /* .line 129 */
		 v0 = this.mTask;
		 /* .line 130 */
		 /* .local v0, "task":Lcom/android/server/wm/Task; */
		 if ( v0 != null) { // if-eqz v0, :cond_14
			 v9 = this.realActivity;
			 if ( v9 != null) { // if-eqz v9, :cond_14
				 v9 = android.util.MiuiMultiWindowAdapter.NOT_AVOID_LAUNCH_OTHER_FREEFORM_LIST;
				 v10 = this.realActivity;
				 /* .line 131 */
				 v9 = 				 (( android.content.ComponentName ) v10 ).getClassName ( ); // invoke-virtual {v10}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
				 if ( v9 != null) { // if-eqz v9, :cond_14
					 /* .line 132 */
					 if ( v0 != null) { // if-eqz v0, :cond_e
						 (( com.android.server.wm.Task ) v0 ).getBounds ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getBounds()Landroid/graphics/Rect;
					 } // :cond_e
					 (( com.android.server.wm.ActivityRecord ) v3 ).getBounds ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityRecord;->getBounds()Landroid/graphics/Rect;
					 /* .line 133 */
					 /* .local v9, "currentRect":Landroid/graphics/Rect; */
				 } // :goto_6
				 v10 = this.mWmService;
				 v10 = this.mContext;
				 /* .line 134 */
				 int v11 = -1; // const/4 v11, -0x1
				 if ( v9 != null) { // if-eqz v9, :cond_f
					 /* iget v12, v9, Landroid/graphics/Rect;->left:I */
				 } // :cond_f
				 /* move v12, v11 */
			 } // :goto_7
			 if ( v9 != null) { // if-eqz v9, :cond_10
				 /* iget v11, v9, Landroid/graphics/Rect;->top:I */
				 /* .line 133 */
			 } // :cond_10
			 android.util.MiuiMultiWindowUtils .getActivityOptions ( v10,v4,v7,v12,v11 );
			 /* .line 135 */
			 /* .local v7, "targetOptions":Landroid/app/ActivityOptions; */
			 if ( v7 != null) { // if-eqz v7, :cond_14
				 /* .line 136 */
				 if ( v8 != null) { // if-eqz v8, :cond_11
					 v10 = 					 (( android.app.ActivityOptions ) v8 ).getForceLaunchNewTask ( ); // invoke-virtual {v8}, Landroid/app/ActivityOptions;->getForceLaunchNewTask()Z
					 if ( v10 != null) { // if-eqz v10, :cond_11
						 /* .line 137 */
						 (( android.app.ActivityOptions ) v7 ).setForceLaunchNewTask ( ); // invoke-virtual {v7}, Landroid/app/ActivityOptions;->setForceLaunchNewTask()V
						 /* .line 139 */
					 } // :cond_11
					 v10 = 					 (( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getFreeFormLaunchFromTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I
					 if ( v10 != null) { // if-eqz v10, :cond_12
						 /* .line 140 */
						 v10 = 						 (( com.android.server.wm.MiuiFreeFormActivityStack ) v2 ).getFreeFormLaunchFromTaskId ( ); // invoke-virtual {v2}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->getFreeFormLaunchFromTaskId()I
						 (( android.app.ActivityOptions ) v7 ).setLaunchFromTaskId ( v10 ); // invoke-virtual {v7, v10}, Landroid/app/ActivityOptions;->setLaunchFromTaskId(I)V
						 /* .line 142 */
					 } // :cond_12
					 if ( v8 != null) { // if-eqz v8, :cond_13
						 /* move-object v10, v7 */
					 } // :cond_13
					 int v10 = 0; // const/4 v10, 0x0
				 } // :goto_8
				 /* move-object v8, v10 */
				 /* .line 143 */
				 final String v10 = "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed::due to application confirm lock"; // const-string v10, "ActivityStarterInjector::modifyLaunchActivityOptionIfNeed::due to application confirm lock"
				 android.util.Slog .d ( v6,v10 );
				 /* .line 148 */
			 } // .end local v0 # "task":Lcom/android/server/wm/Task;
		 } // .end local v7 # "targetOptions":Landroid/app/ActivityOptions;
	 } // .end local v9 # "currentRect":Landroid/graphics/Rect;
} // :cond_14
v0 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
if ( v0 != null) { // if-eqz v0, :cond_16
	 /* .line 149 */
	 /* if-nez v8, :cond_15 */
	 /* .line 150 */
	 android.app.ActivityOptions .makeBasic ( );
	 /* .line 152 */
} // :cond_15
if ( v1 != null) { // if-eqz v1, :cond_16
	 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
	 if ( v0 != null) { // if-eqz v0, :cond_16
		 /* .line 153 */
		 /* invoke-virtual/range {p5 ..p5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName; */
		 (( android.content.ComponentName ) v0 ).flattenToShortString ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
		 final String v6 = "com.android.camera/.OneShotCamera"; // const-string v6, "com.android.camera/.OneShotCamera"
		 v0 = 		 (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_16
			 /* .line 154 */
			 (( android.app.ActivityOptions ) v8 ).setForceLaunchNewTask ( ); // invoke-virtual {v8}, Landroid/app/ActivityOptions;->setForceLaunchNewTask()V
			 /* .line 157 */
		 } // :cond_16
	 } // .end method
	 public static void startActivityUncheckedBefore ( com.android.server.wm.ActivityRecord p0, Boolean p1 ) {
		 /* .locals 0 */
		 /* .param p0, "r" # Lcom/android/server/wm/ActivityRecord; */
		 /* .param p1, "isFromHome" # Z */
		 /* .line 226 */
		 return;
	 } // .end method
