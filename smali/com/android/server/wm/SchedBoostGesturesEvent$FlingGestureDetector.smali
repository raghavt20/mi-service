.class final Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SchedBoostGesturesEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/SchedBoostGesturesEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FlingGestureDetector"
.end annotation


# instance fields
.field private mOverscroller:Landroid/widget/OverScroller;

.field final synthetic this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;


# direct methods
.method constructor <init>(Lcom/android/server/wm/SchedBoostGesturesEvent;)V
    .locals 1

    .line 102
    iput-object p1, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 103
    new-instance v0, Landroid/widget/OverScroller;

    invoke-static {p1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmContext(Lcom/android/server/wm/SchedBoostGesturesEvent;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->mOverscroller:Landroid/widget/OverScroller;

    .line 104
    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 16
    .param p1, "down"    # Landroid/view/MotionEvent;
    .param p2, "up"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .line 117
    move-object/from16 v0, p0

    move/from16 v1, p3

    move/from16 v2, p4

    iget-object v3, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->mOverscroller:Landroid/widget/OverScroller;

    invoke-virtual {v3}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    .line 118
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 120
    .local v3, "now":J
    iget-object v5, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v5}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmLastFlingTime(Lcom/android/server/wm/SchedBoostGesturesEvent;)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    const/4 v6, 0x1

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v5}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmLastFlingTime(Lcom/android/server/wm/SchedBoostGesturesEvent;)J

    move-result-wide v7

    const-wide/16 v9, 0x1388

    add-long/2addr v7, v9

    cmp-long v5, v3, v7

    if-lez v5, :cond_0

    .line 121
    iget-object v5, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->mOverscroller:Landroid/widget/OverScroller;

    invoke-virtual {v5, v6}, Landroid/widget/OverScroller;->forceFinished(Z)V

    .line 123
    :cond_0
    iget-object v7, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->mOverscroller:Landroid/widget/OverScroller;

    const/4 v8, 0x0

    const/4 v9, 0x0

    float-to-int v10, v1

    float-to-int v11, v2

    const/high16 v12, -0x80000000

    const v13, 0x7fffffff

    const/high16 v14, -0x80000000

    const v15, 0x7fffffff

    invoke-virtual/range {v7 .. v15}, Landroid/widget/OverScroller;->fling(IIIIIIII)V

    .line 125
    iget-object v5, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->mOverscroller:Landroid/widget/OverScroller;

    invoke-virtual {v5}, Landroid/widget/OverScroller;->getDuration()I

    move-result v5

    .line 126
    .local v5, "duration":I
    const/16 v7, 0x1388

    if-le v5, v7, :cond_1

    .line 127
    const/16 v5, 0x1388

    .line 129
    :cond_1
    iget-object v7, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v7, v3, v4}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fputmLastFlingTime(Lcom/android/server/wm/SchedBoostGesturesEvent;J)V

    .line 130
    iget-object v7, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v7}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent;)Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 131
    iget-object v7, v0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v7}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent;)Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    move-result-object v7

    invoke-interface {v7, v1, v2, v5}, Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;->onFling(FFI)V

    .line 133
    :cond_2
    return v6
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .line 139
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v0}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmScrollFired(Lcom/android/server/wm/SchedBoostGesturesEvent;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v0}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent;)Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v0}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fgetmGesturesEventListener(Lcom/android/server/wm/SchedBoostGesturesEvent;)Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent$GesturesEventListener;->onScroll(Z)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->this$0:Lcom/android/server/wm/SchedBoostGesturesEvent;

    invoke-static {v0, v1}, Lcom/android/server/wm/SchedBoostGesturesEvent;->-$$Nest$fputmScrollFired(Lcom/android/server/wm/SchedBoostGesturesEvent;Z)V

    .line 145
    :cond_1
    return v1
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .line 108
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->mOverscroller:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/android/server/wm/SchedBoostGesturesEvent$FlingGestureDetector;->mOverscroller:Landroid/widget/OverScroller;

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    .line 111
    :cond_0
    return v1
.end method
