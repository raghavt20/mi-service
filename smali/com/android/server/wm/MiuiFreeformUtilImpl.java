public class com.android.server.wm.MiuiFreeformUtilImpl implements com.android.server.wm.MiuiFreeformUtilStub {
	 /* .source "MiuiFreeformUtilImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 public static final java.lang.String UNKNOWN;
	 /* # direct methods */
	 public com.android.server.wm.MiuiFreeformUtilImpl ( ) {
		 /* .locals 0 */
		 /* .line 42 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static Boolean lambda$isSupportFreeFormInDesktopMode$0 ( com.android.server.wm.ActivityRecord p0 ) { //synthethic
		 /* .locals 1 */
		 /* .param p0, "activityRecord" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 638 */
		 v0 = 		 (( com.android.server.wm.ActivityRecord ) p0 ).isFullScreen ( ); // invoke-virtual {p0}, Lcom/android/server/wm/ActivityRecord;->isFullScreen()Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 639 */
			 int v0 = 1; // const/4 v0, 0x1
			 /* .line 641 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 /* # virtual methods */
	 public void adjuestFrameForChild ( com.android.server.wm.WindowState p0 ) {
		 /* .locals 0 */
		 /* .param p1, "win" # Lcom/android/server/wm/WindowState; */
		 /* .line 440 */
		 com.android.server.wm.WindowStateStubImpl .adjuestFrameForChild ( p1 );
		 /* .line 441 */
		 return;
	 } // .end method
	 public void adjuestFreeFormTouchRegion ( com.android.server.wm.WindowState p0, android.graphics.Region p1 ) {
		 /* .locals 0 */
		 /* .param p1, "win" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "outRegion" # Landroid/graphics/Region; */
		 /* .line 444 */
		 com.android.server.wm.WindowStateStubImpl .adjuestFreeFormTouchRegion ( p1,p2 );
		 /* .line 445 */
		 return;
	 } // .end method
	 public void adjuestScaleAndFrame ( com.android.server.wm.WindowState p0, com.android.server.wm.Task p1 ) {
		 /* .locals 0 */
		 /* .param p1, "win" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "task" # Lcom/android/server/wm/Task; */
		 /* .line 436 */
		 com.android.server.wm.WindowStateStubImpl .adjuestScaleAndFrame ( p1,p2 );
		 /* .line 437 */
		 return;
	 } // .end method
	 public com.android.server.wm.ActivityRecord adjustTopActivityIfNeed ( com.android.server.wm.ActivityRecord p0 ) {
		 /* .locals 3 */
		 /* .param p1, "oriTop" # Lcom/android/server/wm/ActivityRecord; */
		 /* .line 584 */
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 (( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 585 */
				 (( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
				 v0 = 				 (( com.android.server.wm.Task ) v0 ).inFreeformWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 586 */
					 (( com.android.server.wm.ActivityRecord ) p1 ).getRootTask ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getRootTask()Lcom/android/server/wm/Task;
					 v0 = 					 (( com.android.server.wm.Task ) v0 ).isAlwaysOnTop ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->isAlwaysOnTop()Z
					 if ( v0 != null) { // if-eqz v0, :cond_0
						 /* .line 587 */
						 (( com.android.server.wm.ActivityRecord ) p1 ).getDisplayArea ( ); // invoke-virtual {p1}, Lcom/android/server/wm/ActivityRecord;->getDisplayArea()Lcom/android/server/wm/TaskDisplayArea;
						 int v1 = 0; // const/4 v1, 0x0
						 int v2 = 1; // const/4 v2, 0x1
						 (( com.android.server.wm.TaskDisplayArea ) v0 ).topRunningActivity ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/TaskDisplayArea;->topRunningActivity(ZZ)Lcom/android/server/wm/ActivityRecord;
						 /* .line 588 */
						 /* .local v0, "topAr":Lcom/android/server/wm/ActivityRecord; */
						 /* new-instance v1, Ljava/lang/StringBuilder; */
						 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
						 final String v2 = " adjustTopActivityIfNeed topAr: "; // const-string v2, " adjustTopActivityIfNeed topAr: "
						 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
						 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
						 final String v2 = "MiuiFreeformUtilImpl"; // const-string v2, "MiuiFreeformUtilImpl"
						 android.util.Slog .d ( v2,v1 );
						 /* .line 589 */
						 /* .line 591 */
					 } // .end local v0 # "topAr":Lcom/android/server/wm/ActivityRecord;
				 } // :cond_0
			 } // .end method
			 public void checkFreeformSupport ( com.android.server.wm.ActivityTaskManagerService p0, android.app.ActivityOptions p1 ) {
				 /* .locals 0 */
				 /* .param p1, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
				 /* .param p2, "options" # Landroid/app/ActivityOptions; */
				 /* .line 391 */
				 com.android.server.wm.ActivityStarterInjector .checkFreeformSupport ( p1,p2 );
				 /* .line 392 */
				 return;
			 } // .end method
			 public java.util.List getAbnormalFreeformBlackList ( Boolean p0 ) {
				 /* .locals 1 */
				 /* .param p1, "fromSystem" # Z */
				 /* .annotation system Ldalvik/annotation/Signature; */
				 /* value = { */
				 /* "(Z)", */
				 /* "Ljava/util/List<", */
				 /* "Ljava/lang/String;", */
				 /* ">;" */
				 /* } */
			 } // .end annotation
			 /* .line 79 */
			 android.util.MiuiMultiWindowAdapter .getAbnormalFreeformBlackList ( p1 );
		 } // .end method
		 public java.util.List getAbnormalFreeformWhiteList ( Boolean p0 ) {
			 /* .locals 1 */
			 /* .param p1, "fromSystem" # Z */
			 /* .annotation system Ldalvik/annotation/Signature; */
			 /* value = { */
			 /* "(Z)", */
			 /* "Ljava/util/List<", */
			 /* "Ljava/lang/String;", */
			 /* ">;" */
			 /* } */
		 } // .end annotation
		 /* .line 87 */
		 android.util.MiuiMultiWindowAdapter .getAbnormalFreeformWhiteList ( p1 );
	 } // .end method
	 public java.util.List getAdditionalFreeformAspectRatio1Apps ( ) {
		 /* .locals 1 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "()", */
		 /* "Ljava/util/List<", */
		 /* "Ljava/lang/String;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 350 */
	 android.util.MiuiMultiWindowAdapter .getAdditionalFreeformAspectRatio1Apps ( );
} // .end method
public java.util.List getAdditionalFreeformAspectRatio2Apps ( ) {
	 /* .locals 1 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 358 */
android.util.MiuiMultiWindowAdapter .getAdditionalFreeformAspectRatio2Apps ( );
} // .end method
public java.util.List getApplicationLockActivityList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 226 */
android.util.MiuiMultiWindowAdapter .getApplicationLockActivityList ( );
} // .end method
public java.util.List getAudioForegroundPinAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 270 */
android.util.MiuiMultiWindowAdapter .getAudioForegroundPinAppList ( );
} // .end method
public java.util.List getCanStartActivityFromFullscreenToFreefromList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 58 */
android.util.MiuiMultiWindowAdapter .getCanStartActivityFromFullscreenToFreefromList ( );
} // .end method
public java.util.List getCvwUnsupportedFreeformWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 211 */
android.util.MiuiMultiWindowAdapter .getCvwUnsupportedFreeformWhiteList ( );
} // .end method
public java.util.List getDesktopFreeformWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 361 */
android.util.MiuiMultiWindowAdapter .getDesktopFreeformWhiteList ( );
} // .end method
public java.util.List getDesktopModeLaunchFreeformIgnoreTranslucentAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 675 */
android.util.MiuiMultiWindowAdapter .getDesktopModeLaunchFreeformIgnoreTranslucentAppList ( );
} // .end method
public java.util.List getDesktopModeLaunchFullscreenAppList ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 655 */
android.util.MiuiMultiWindowAdapter .getDesktopModeLaunchFullscreenAppList ( );
/* .line 656 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v1, v0 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
} // :goto_0
} // .end method
public java.util.List getDesktopModeLaunchFullscreenNotHideOtherAppList ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 663 */
android.util.MiuiMultiWindowAdapter .getDesktopModeLaunchFullscreenNotHideOtherAppList ( );
/* .line 664 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v1, v0 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
} // :goto_0
} // .end method
public Integer getEnableAbnormalFreeFormDebug ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "fromSystem" # Z */
/* .line 334 */
v0 = android.util.MiuiMultiWindowAdapter .getEnableAbnormalFreeFormDebug ( p1 );
} // .end method
public java.util.List getFixedRotationAppList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 294 */
android.util.MiuiMultiWindowAdapter .getFixedRotationAppList ( );
} // .end method
public java.util.List getForceLandscapeApplication ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 242 */
android.util.MiuiMultiWindowAdapter .getForceLandscapeApplication ( );
} // .end method
public java.util.List getForegroundPinAppBlackList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 286 */
android.util.MiuiMultiWindowAdapter .getForegroundPinAppBlackList ( );
} // .end method
public java.util.List getForegroundPinAppWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 278 */
android.util.MiuiMultiWindowAdapter .getForegroundPinAppWhiteList ( );
} // .end method
public android.graphics.Rect getFreeFormAccessibleArea ( android.content.Context p0, com.android.server.wm.DisplayContent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "displayContent" # Lcom/android/server/wm/DisplayContent; */
/* .line 547 */
/* if-nez p2, :cond_0 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
/* .line 548 */
} // :cond_0
(( com.android.server.wm.DisplayContent ) p2 ).getInsetsStateController ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;
/* .line 549 */
/* .local v0, "insetsStateController":Lcom/android/server/wm/InsetsStateController; */
v2 = (( com.android.server.wm.DisplayContent ) p2 ).getRotation ( ); // invoke-virtual {p2}, Lcom/android/server/wm/DisplayContent;->getRotation()I
/* .line 550 */
v3 = com.android.server.wm.MiuiFreeFormGestureController .getStatusBarHeight ( v0 );
/* .line 551 */
v4 = com.android.server.wm.MiuiFreeFormGestureController .getNavBarHeight ( v0 );
v1 = this.mDisplayFrames;
/* .line 552 */
v5 = com.android.server.wm.MiuiFreeFormGestureController .getDisplayCutoutHeight ( v1 );
/* .line 553 */
v6 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( p1 );
/* .line 549 */
/* move-object v1, p1 */
/* invoke-static/range {v1 ..v6}, Landroid/util/MiuiMultiWindowUtils;->getFreeFormAccessibleArea(Landroid/content/Context;IIIIZ)Landroid/graphics/Rect; */
} // .end method
public java.util.List getFreeformBlackList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 66 */
android.util.MiuiMultiWindowAdapter .getFreeformBlackList ( );
} // .end method
public java.util.List getFreeformBlackList ( android.content.Context p0, java.util.HashMap p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p3, "isNeedUpdateList" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/LinkedList<", */
/* "Ljava/lang/Long;", */
/* ">;>;Z)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 71 */
/* .local p2, "allTimestamps":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/LinkedList<Ljava/lang/Long;>;>;" */
android.util.MiuiMultiWindowUtils .getFreeformBlackList ( p1,p2,p3 );
} // .end method
public java.util.List getFreeformCaptionInsetsHeightToZeroList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 234 */
android.util.MiuiMultiWindowAdapter .getFreeformCaptionInsetsHeightToZeroList ( );
} // .end method
public java.util.List getFreeformDisableOverlayList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 159 */
android.util.MiuiMultiWindowAdapter .getFreeformDisableOverlayList ( );
} // .end method
public java.util.List getFreeformIgnoreRequestOrientationList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 167 */
android.util.MiuiMultiWindowAdapter .getFreeformIgnoreRequestOrientationList ( );
} // .end method
public java.util.List getFreeformNeedRelunchList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 175 */
android.util.MiuiMultiWindowAdapter .getFreeformNeedRelunchList ( );
} // .end method
public java.util.List getFreeformRecommendMapApplicationList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 103 */
android.util.MiuiMultiWindowAdapter .getFreeformRecommendMapApplicationList ( );
} // .end method
public java.util.List getFreeformRecommendWaitingApplicationList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 111 */
android.util.MiuiMultiWindowAdapter .getFreeformRecommendWaitingApplicationList ( );
} // .end method
public android.graphics.Rect getFreeformRect ( android.content.Context p0, Boolean p1, Boolean p2, Boolean p3, Boolean p4, android.graphics.Rect p5 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "needDisplayContentRotation" # Z */
/* .param p3, "isVertical" # Z */
/* .param p4, "isMiniFreeformMode" # Z */
/* .param p5, "isFreeformLandscape" # Z */
/* .param p6, "outBounds" # Landroid/graphics/Rect; */
/* .line 378 */
/* invoke-static/range {p1 ..p6}, Landroid/util/MiuiMultiWindowUtils;->getFreeformRect(Landroid/content/Context;ZZZZLandroid/graphics/Rect;)Landroid/graphics/Rect; */
} // .end method
public java.util.List getFreeformResizeableWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 207 */
android.util.MiuiMultiWindowAdapter .getFreeformResizeableWhiteList ( );
} // .end method
public Float getFreeformRoundCorner ( ) {
/* .locals 1 */
/* .line 432 */
} // .end method
public Float getFreeformScale ( android.app.ActivityOptions p0 ) {
/* .locals 4 */
/* .param p1, "options" # Landroid/app/ActivityOptions; */
/* .line 557 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v1, v0, [Ljava/lang/Object; */
final String v2 = "getActivityOptionsInjector"; // const-string v2, "getActivityOptionsInjector"
android.util.MiuiMultiWindowUtils .isMethodExist ( p1,v2,v1 );
/* .line 559 */
/* .local v1, "method":Ljava/lang/reflect/Method; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 561 */
try { // :try_start_0
/* new-array v2, v0, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
final String v3 = "getFreeformScale"; // const-string v3, "getFreeformScale"
/* new-array v0, v0, [Ljava/lang/Object; */
android.util.MiuiMultiWindowUtils .invoke ( v2,v3,v0 );
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 563 */
/* :catch_0 */
/* move-exception v0 */
/* .line 564 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 567 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
/* const/high16 v0, 0x3f800000 # 1.0f */
} // .end method
public java.util.List getFreeformVideoWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 95 */
android.util.MiuiMultiWindowAdapter .getFreeformVideoWhiteList ( );
} // .end method
public java.util.List getHideSelfIfNewFreeformTaskWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 191 */
android.util.MiuiMultiWindowAdapter .getHideSelfIfNewFreeformTaskWhiteList ( );
} // .end method
public Float getHotSpaceBottomOffsetPad ( ) {
/* .locals 1 */
/* .line 518 */
/* int-to-float v0, v0 */
} // .end method
public Float getHotSpaceOffset ( ) {
/* .locals 1 */
/* .line 514 */
/* int-to-float v0, v0 */
} // .end method
public Integer getHotSpaceResizeOffsetPad ( ) {
/* .locals 1 */
/* .line 534 */
/* const/16 v0, 0x21 */
} // .end method
public Integer getHotSpaceTopCaptionUpwardsOffset ( ) {
/* .locals 1 */
/* .line 538 */
/* const/16 v0, 0x16 */
} // .end method
public java.util.List getLaunchInTaskList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 326 */
android.util.MiuiMultiWindowAdapter .getLaunchInTaskList ( );
} // .end method
public java.util.List getLockModeActivityList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 151 */
android.util.MiuiMultiWindowAdapter .getAboutLockModeActivityList ( );
} // .end method
public Float getMiniFreeformPaddingStroke ( ) {
/* .locals 1 */
/* .line 522 */
/* const/high16 v0, 0x41a00000 # 20.0f */
} // .end method
public Float getMiuiMultiWindowUtilsScale ( ) {
/* .locals 1 */
/* .line 416 */
} // .end method
public java.util.List getRotationFromDisplayApp ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 302 */
android.util.MiuiMultiWindowAdapter .getRotationFromDisplayApp ( );
} // .end method
public java.util.List getSensorDisableList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 342 */
android.util.MiuiMultiWindowAdapter .getSensorDisableList ( );
} // .end method
public java.util.List getSensorDisableWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 318 */
android.util.MiuiMultiWindowAdapter .getSensorDisableWhiteList ( );
} // .end method
public Float getShadowRadius ( ) {
/* .locals 1 */
/* .line 473 */
/* const/high16 v0, 0x43c80000 # 400.0f */
} // .end method
public java.util.List getShowHiddenTaskIfFinishedWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 199 */
android.util.MiuiMultiWindowAdapter .getShowHiddenTaskIfFinishedWhiteList ( );
} // .end method
public java.util.List getStartFromFreeformBlackList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 183 */
android.util.MiuiMultiWindowAdapter .getStartFromFreeformBlackList ( );
} // .end method
public java.util.List getSupportCvwLevelFullList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 119 */
android.util.MiuiMultiWindowAdapter .getSupportCvwLevelFullList ( );
} // .end method
public java.util.List getSupportCvwLevelHorizontalList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 135 */
android.util.MiuiMultiWindowAdapter .getSupportCvwLevelHorizontalList ( );
} // .end method
public java.util.List getSupportCvwLevelVerticalList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 127 */
android.util.MiuiMultiWindowAdapter .getSupportCvwLevelVerticalList ( );
} // .end method
public Integer getTopDecorCaptionViewHeight ( ) {
/* .locals 1 */
/* .line 428 */
} // .end method
public java.util.List getTopGameList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 250 */
android.util.MiuiMultiWindowAdapter .getTopGameList ( );
} // .end method
public java.util.List getTopVideoList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 258 */
android.util.MiuiMultiWindowAdapter .getTopVideoList ( );
} // .end method
public java.util.List getUnsupportCvwLevelFullList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 143 */
android.util.MiuiMultiWindowAdapter .getUnsupportCvwLevelFullList ( );
} // .end method
public java.util.List getUseDefaultCameraPipelineApp ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 310 */
android.util.MiuiMultiWindowAdapter .getUseDefaultCameraPipelineApp ( );
} // .end method
public Integer handleFreeformModeRequst ( android.os.IBinder p0, Integer p1, android.content.Context p2 ) {
/* .locals 1 */
/* .param p1, "token" # Landroid/os/IBinder; */
/* .param p2, "cmd" # I */
/* .param p3, "mContext" # Landroid/content/Context; */
/* .line 407 */
v0 = com.android.server.wm.ActivityTaskManagerServiceImpl .handleFreeformModeRequst ( p1,p2,p3 );
} // .end method
public Boolean ignoreClearTaskFlagInFreeForm ( com.android.server.wm.ActivityRecord p0, com.android.server.wm.ActivityRecord p1 ) {
/* .locals 3 */
/* .param p1, "reusedActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "startActivity" # Lcom/android/server/wm/ActivityRecord; */
/* .line 489 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
if ( p2 != null) { // if-eqz p2, :cond_2
v1 = this.shortComponentName;
/* if-nez v1, :cond_0 */
v1 = this.shortComponentName;
/* if-nez v1, :cond_0 */
/* .line 493 */
} // :cond_0
final String v1 = "com.tencent.mobileqq/com.tencent.open.agent.AgentActivity"; // const-string v1, "com.tencent.mobileqq/com.tencent.open.agent.AgentActivity"
v2 = this.shortComponentName;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.shortComponentName;
/* .line 494 */
final String v2 = "com.tencent.mobileqq/.activity.LoginActivity"; // const-string v2, "com.tencent.mobileqq/.activity.LoginActivity"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
/* nop */
/* .line 493 */
} // :goto_0
/* .line 491 */
} // :cond_2
} // :goto_1
} // .end method
public Boolean inFreeformWhiteList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 219 */
v0 = android.util.MiuiMultiWindowAdapter .inFreeformWhiteList ( p1 );
} // .end method
public void initDesktop ( android.content.Context p0 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 618 */
com.android.server.wm.MiuiDesktopModeUtils .initDesktop ( p1 );
/* .line 619 */
return;
} // .end method
public Boolean isAppLockActivity ( android.content.ComponentName p0, android.content.ComponentName p1 ) {
/* .locals 1 */
/* .param p1, "componentName" # Landroid/content/ComponentName; */
/* .param p2, "cls" # Landroid/content/ComponentName; */
/* .line 762 */
v0 = android.util.MiuiMultiWindowAdapter .isAppLockActivity ( p1,p2 );
} // .end method
public Boolean isAppSetAutoUiInDesktopMode ( com.android.server.wm.ActivityRecord p0, Integer p1, android.content.Context p2 ) {
/* .locals 6 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "windowingMode" # I */
/* .param p3, "context" # Landroid/content/Context; */
/* .line 724 */
final String v0 = "android.window.PROPERTY_AUTOUI_ALLOW_SYSTEM_OVERRIDE"; // const-string v0, "android.window.PROPERTY_AUTOUI_ALLOW_SYSTEM_OVERRIDE"
v1 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isDeskTopModeActive ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_5
/* if-nez p2, :cond_5 */
/* if-nez p1, :cond_0 */
/* goto/16 :goto_5 */
/* .line 727 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
/* .line 728 */
/* .local v1, "autouiAllowApplication":I */
int v3 = -1; // const/4 v3, -0x1
/* .line 730 */
/* .local v3, "autouiAllowComponent":I */
try { // :try_start_0
(( android.content.Context ) p3 ).getPackageManager ( ); // invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v5 = this.packageName;
/* .line 731 */
(( android.content.pm.PackageManager ) v4 ).getProperty ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getProperty(Ljava/lang/String;Ljava/lang/String;)Landroid/content/pm/PackageManager$Property;
/* .line 732 */
/* .local v4, "propertyAutouiApplication":Landroid/content/pm/PackageManager$Property; */
(( android.content.pm.PackageManager$Property ) v4 ).getName ( ); // invoke-virtual {v4}, Landroid/content/pm/PackageManager$Property;->getName()Ljava/lang/String;
v5 = (( java.lang.String ) v5 ).isEmpty ( ); // invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 733 */
int v1 = -1; // const/4 v1, -0x1
/* .line 735 */
} // :cond_1
v5 = (( android.content.pm.PackageManager$Property ) v4 ).getBoolean ( ); // invoke-virtual {v4}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v1, v5 */
/* .line 739 */
} // .end local v4 # "propertyAutouiApplication":Landroid/content/pm/PackageManager$Property;
} // :goto_0
/* .line 737 */
/* :catch_0 */
/* move-exception v4 */
/* .line 741 */
} // :goto_1
try { // :try_start_1
(( android.content.Context ) p3 ).getPackageManager ( ); // invoke-virtual {p3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v5 = this.mActivityComponent;
/* .line 742 */
(( android.content.pm.PackageManager ) v4 ).getProperty ( v0, v5 ); // invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getProperty(Ljava/lang/String;Landroid/content/ComponentName;)Landroid/content/pm/PackageManager$Property;
/* .line 743 */
/* .local v0, "propertyAutouiComponent":Landroid/content/pm/PackageManager$Property; */
(( android.content.pm.PackageManager$Property ) v0 ).getName ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getName()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).isEmpty ( ); // invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 744 */
int v3 = -1; // const/4 v3, -0x1
/* .line 746 */
} // :cond_2
v4 = (( android.content.pm.PackageManager$Property ) v0 ).getBoolean ( ); // invoke-virtual {v0}, Landroid/content/pm/PackageManager$Property;->getBoolean()Z
/* :try_end_1 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move v3, v4 */
/* .line 750 */
} // .end local v0 # "propertyAutouiComponent":Landroid/content/pm/PackageManager$Property;
} // :goto_2
/* .line 748 */
/* :catch_1 */
/* move-exception v0 */
/* .line 751 */
} // :goto_3
int v0 = 1; // const/4 v0, 0x1
/* if-eq v3, v0, :cond_4 */
/* if-ne v1, v0, :cond_3 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 758 */
} // :cond_3
/* .line 753 */
} // :cond_4
} // :goto_4
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "autouiAllow:: r.packageName="; // const-string v4, "autouiAllow:: r.packageName="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.packageName;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ", r.mActivityComponent ="; // const-string v4, ", r.mActivityComponent ="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mActivityComponent;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v4 = ", autouiAllowApplication="; // const-string v4, ", autouiAllowApplication="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", autouiAllowComponent="; // const-string v4, ", autouiAllowComponent="
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiFreeformUtilImpl"; // const-string v4, "MiuiFreeformUtilImpl"
android.util.Slog .i ( v4,v2 );
/* .line 756 */
/* .line 725 */
} // .end local v1 # "autouiAllowApplication":I
} // .end local v3 # "autouiAllowComponent":I
} // :cond_5
} // :goto_5
} // .end method
public Boolean isDeskTopModeActive ( ) {
/* .locals 1 */
/* .line 621 */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
} // .end method
public Boolean isDisableSplashScreenInFreeFormTaskSwitch ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "componentName" # Ljava/lang/String; */
/* .line 485 */
final String v0 = "com.tencent.qqlive/com.tencent.tauth.AuthActivity"; // const-string v0, "com.tencent.qqlive/com.tencent.tauth.AuthActivity"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isEnableAbnormalFreeform ( java.lang.String p0, android.content.Context p1, java.util.List p2, java.util.List p3, Integer p4 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .param p5, "enableAbnormalFreeform" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Landroid/content/Context;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;I)Z" */
/* } */
} // .end annotation
/* .line 395 */
/* .local p3, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* .local p4, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = android.util.MiuiMultiWindowUtils .isEnableAbnormalFreeform ( p1,p2,p3,p4,p5 );
} // .end method
public Boolean isEnalbleAbnormalFreeform ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 576 */
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isLandscapeGameApp ( p2, p1 ); // invoke-virtual {p0, p2, p1}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isLandscapeGameApp(Ljava/lang/String;Landroid/content/Context;)Z
/* if-nez v0, :cond_0 */
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isPadScreen ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isPadScreen(Landroid/content/Context;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 578 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
(( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getAbnormalFreeformBlackList ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getAbnormalFreeformBlackList(Z)Ljava/util/List;
/* .line 579 */
(( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getAbnormalFreeformWhiteList ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getAbnormalFreeformWhiteList(Z)Ljava/util/List;
/* .line 580 */
v6 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getEnableAbnormalFreeFormDebug ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getEnableAbnormalFreeFormDebug(Z)I
/* .line 577 */
/* move-object v1, p0 */
/* move-object v2, p2 */
/* move-object v3, p1 */
v1 = /* invoke-virtual/range {v1 ..v6}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isEnableAbnormalFreeform(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;Ljava/util/List;I)Z */
/* if-nez v1, :cond_2 */
/* .line 580 */
v1 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isDeskTopModeActive ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z
if ( v1 != null) { // if-eqz v1, :cond_1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :cond_2
} // :goto_0
/* nop */
/* .line 576 */
} // :goto_1
} // .end method
public Boolean isForceResizeable ( ) {
/* .locals 1 */
/* .line 403 */
v0 = android.util.MiuiMultiWindowUtils .isForceResizeable ( );
} // .end method
public Boolean isFullScreenStrategyNeededInDesktopMode ( com.android.server.wm.ActivityRecord p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "r" # Lcom/android/server/wm/ActivityRecord; */
/* .param p2, "windowingMode" # I */
/* .line 682 */
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isDeskTopModeActive ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_3
/* if-nez p2, :cond_3 */
/* if-nez p1, :cond_0 */
/* .line 685 */
} // :cond_0
(( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getDesktopModeLaunchFullscreenAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;
v0 = v2 = this.packageName;
/* if-nez v0, :cond_2 */
/* .line 686 */
(( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getDesktopModeLaunchFullscreenAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;
v0 = v2 = this.shortComponentName;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 690 */
} // :cond_1
/* .line 687 */
} // :cond_2
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isFullScreenStrategyNeededInDesktopMode, launch fullscreen, r.shortComponentName= "; // const-string v1, " isFullScreenStrategyNeededInDesktopMode, launch fullscreen, r.shortComponentName= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.shortComponentName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeformUtilImpl"; // const-string v1, "MiuiFreeformUtilImpl"
android.util.Slog .i ( v1,v0 );
/* .line 688 */
int v0 = 1; // const/4 v0, 0x1
/* .line 683 */
} // :cond_3
} // :goto_1
} // .end method
public Boolean isFullScreenStrategyNeededInDesktopMode ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "shortComponentName" # Ljava/lang/String; */
/* .line 697 */
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isDeskTopModeActive ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 698 */
/* .line 700 */
} // :cond_0
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getDesktopModeLaunchFullscreenAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 701 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " isFullScreenStrategyNeededInDesktopMode, launch fullscreen, shortComponentName= "; // const-string v1, " isFullScreenStrategyNeededInDesktopMode, launch fullscreen, shortComponentName= "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiFreeformUtilImpl"; // const-string v1, "MiuiFreeformUtilImpl"
android.util.Slog .i ( v1,v0 );
/* .line 702 */
int v0 = 1; // const/4 v0, 0x1
/* .line 704 */
} // :cond_1
} // .end method
public Boolean isLandscapeGameApp ( java.lang.String p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 399 */
v0 = android.util.MiuiMultiWindowUtils .isLandscapeGameApp ( p1,p2 );
} // .end method
public Boolean isLaunchNotesInKeyGuard ( android.content.Intent p0 ) {
/* .locals 4 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 708 */
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isDeskTopModeActive ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
/* if-nez p1, :cond_0 */
/* .line 711 */
} // :cond_0
(( android.content.Intent ) p1 ).getExtras ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;
/* .line 712 */
/* .local v0, "bundle":Landroid/os/Bundle; */
if ( v0 != null) { // if-eqz v0, :cond_1
final String v2 = "scene"; // const-string v2, "scene"
(( android.os.Bundle ) v0 ).getString ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
} // :cond_1
final String v2 = ""; // const-string v2, ""
/* .line 713 */
/* .local v2, "scene":Ljava/lang/String; */
} // :goto_0
final String v3 = "keyguard"; // const-string v3, "keyguard"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_3 */
/* .line 714 */
final String v3 = "off_screen"; // const-string v3, "off_screen"
v3 = (( java.lang.String ) v3 ).equals ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 717 */
} // :cond_2
/* .line 715 */
} // :cond_3
} // :goto_1
int v1 = 1; // const/4 v1, 0x1
/* .line 709 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // .end local v2 # "scene":Ljava/lang/String;
} // :cond_4
} // :goto_2
} // .end method
public Boolean isMIUIProduct ( ) {
/* .locals 1 */
/* .line 609 */
/* sget-boolean v0, Landroid/os/Build;->IS_MIUI:Z */
} // .end method
public Boolean isMiuiBuild ( ) {
/* .locals 2 */
/* .line 459 */
/* nop */
/* .line 460 */
final String v0 = "ro.miui.cts"; // const-string v0, "ro.miui.cts"
android.os.SystemProperties .get ( v0 );
final String v1 = "1"; // const-string v1, "1"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 459 */
/* xor-int/lit8 v0, v0, 0x1 */
final String v1 = "persist.sys.miui_optimization"; // const-string v1, "persist.sys.miui_optimization"
v0 = android.os.SystemProperties .getBoolean ( v1,v0 );
} // .end method
public Boolean isMiuiCvwFeatureEnable ( ) {
/* .locals 1 */
/* .line 469 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isOrientationLandscape ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "orientation" # I */
/* .line 424 */
v0 = android.util.MiuiMultiWindowUtils .isOrientationLandscape ( p1 );
} // .end method
public Boolean isPadScreen ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 506 */
v0 = android.util.MiuiMultiWindowUtils .isPadScreen ( p1 );
} // .end method
public Boolean isSkipUpdateFreeFormShadow ( com.android.server.wm.Task p0 ) {
/* .locals 4 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 498 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 499 */
} // :cond_0
v1 = (( com.android.server.wm.Task ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getChildCount()I
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_1 */
(( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 501 */
(( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v1 = this.shortComponentName;
/* .line 500 */
final String v3 = "com.eg.android.AlipayGphone/com.alipay.mobile.quinox.SchemeLauncherActivity"; // const-string v3, "com.eg.android.AlipayGphone/com.alipay.mobile.quinox.SchemeLauncherActivity"
v1 = (( java.lang.String ) v3 ).equals ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v0, v2 */
} // :cond_1
/* nop */
/* .line 502 */
/* .local v0, "result":Z */
} // :goto_0
} // .end method
public Boolean isSupportFreeFormInDesktopMode ( com.android.server.wm.Task p0 ) {
/* .locals 5 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 624 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_7
/* .line 625 */
v1 = this.origActivity;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.origActivity;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 626 */
} // :cond_0
v1 = this.realActivity;
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.realActivity;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 627 */
} // :cond_1
(( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 628 */
(( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v1 = this.packageName;
} // :cond_2
/* const-string/jumbo v1, "unknown" */
} // :goto_0
/* nop */
/* .line 629 */
/* .local v1, "packageName":Ljava/lang/String; */
(( com.android.server.wm.Task ) p1 ).topRunningActivityLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->topRunningActivityLocked()Lcom/android/server/wm/ActivityRecord;
/* .line 630 */
/* .local v2, "r":Lcom/android/server/wm/ActivityRecord; */
v3 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getDesktopModeLaunchFullscreenAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;
/* if-nez v3, :cond_6 */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 631 */
(( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getDesktopModeLaunchFullscreenAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;
v3 = v4 = this.shortComponentName;
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 634 */
} // :cond_3
/* if-nez v2, :cond_4 */
(( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).getDesktopModeLaunchFullscreenAppList ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->getDesktopModeLaunchFullscreenAppList()Ljava/util/List;
v4 = this.realActivity;
v3 = (( android.content.ComponentName ) v4 ).flattenToShortString ( ); // invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 635 */
/* .line 637 */
} // :cond_4
/* new-instance v0, Lcom/android/server/wm/MiuiFreeformUtilImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/wm/MiuiFreeformUtilImpl$$ExternalSyntheticLambda0;-><init>()V */
(( com.android.server.wm.Task ) p1 ).getActivity ( v0 ); // invoke-virtual {p1, v0}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;
/* .line 643 */
/* .local v0, "fullScreenActivity":Lcom/android/server/wm/ActivityRecord; */
int v3 = 1; // const/4 v3, 0x1
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 644 */
/* .line 646 */
} // :cond_5
/* iget-boolean v4, p1, Lcom/android/server/wm/Task;->mLaunchFullScreenInDesktopMode:Z */
/* xor-int/2addr v3, v4 */
/* .line 632 */
} // .end local v0 # "fullScreenActivity":Lcom/android/server/wm/ActivityRecord;
} // :cond_6
} // :goto_1
/* .line 649 */
} // .end local v1 # "packageName":Ljava/lang/String;
} // .end local v2 # "r":Lcom/android/server/wm/ActivityRecord;
} // :cond_7
} // .end method
public Boolean isSupportFreeFormMultiTask ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 615 */
v0 = android.util.MiuiMultiWindowAdapter .isSupportFreeFormMultiTask ( p1 );
} // .end method
public Boolean isUseFreeFormAnimation ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "transit" # I */
/* .line 464 */
v0 = com.android.server.wm.AppTransitionInjector .isUseFreeFormAnimation ( p1 );
} // .end method
public android.view.animation.Animation loadFreeFormAnimation ( com.android.server.wm.WindowManagerService p0, Integer p1, Boolean p2, android.graphics.Rect p3, com.android.server.wm.WindowContainer p4 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/wm/WindowManagerService; */
/* .param p2, "transit" # I */
/* .param p3, "enter" # Z */
/* .param p4, "frame" # Landroid/graphics/Rect; */
/* .param p5, "container" # Lcom/android/server/wm/WindowContainer; */
/* .line 412 */
com.android.server.wm.AppTransitionInjector .loadFreeFormAnimation ( p1,p2,p3,p4,p5 );
} // .end method
public android.app.ActivityOptions modifyLaunchActivityOptionIfNeed ( com.android.server.wm.ActivityTaskManagerService p0, com.android.server.wm.RootWindowContainer p1, java.lang.String p2, android.app.ActivityOptions p3, com.android.server.wm.WindowProcessController p4, android.content.Intent p5, Integer p6, android.content.pm.ActivityInfo p7, com.android.server.wm.ActivityRecord p8 ) {
/* .locals 1 */
/* .param p1, "service" # Lcom/android/server/wm/ActivityTaskManagerService; */
/* .param p2, "root" # Lcom/android/server/wm/RootWindowContainer; */
/* .param p3, "callingPackgae" # Ljava/lang/String; */
/* .param p4, "options" # Landroid/app/ActivityOptions; */
/* .param p5, "callerApp" # Lcom/android/server/wm/WindowProcessController; */
/* .param p6, "intent" # Landroid/content/Intent; */
/* .param p7, "userId" # I */
/* .param p8, "aInfo" # Landroid/content/pm/ActivityInfo; */
/* .param p9, "sourceRecord" # Lcom/android/server/wm/ActivityRecord; */
/* .line 386 */
/* invoke-static/range {p1 ..p9}, Lcom/android/server/wm/ActivityStarterInjector;->modifyLaunchActivityOptionIfNeed(Lcom/android/server/wm/ActivityTaskManagerService;Lcom/android/server/wm/RootWindowContainer;Ljava/lang/String;Landroid/app/ActivityOptions;Lcom/android/server/wm/WindowProcessController;Landroid/content/Intent;ILandroid/content/pm/ActivityInfo;Lcom/android/server/wm/ActivityRecord;)Landroid/app/ActivityOptions; */
} // .end method
public Boolean multiFreeFormSupported ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 510 */
v0 = android.util.MiuiMultiWindowUtils .multiFreeFormSupported ( p1 );
} // .end method
public Boolean needRelunchFreeform ( java.lang.String p0, android.content.res.Configuration p1, android.content.res.Configuration p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "tempConfig" # Landroid/content/res/Configuration; */
/* .param p3, "fullConfig" # Landroid/content/res/Configuration; */
/* .line 526 */
v0 = android.util.MiuiMultiWindowAdapter .needRelunchFreeform ( p1,p2,p3 );
} // .end method
public Boolean notNeedRelunchFreeform ( java.lang.String p0, android.content.res.Configuration p1, android.content.res.Configuration p2 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "tempConfig" # Landroid/content/res/Configuration; */
/* .param p3, "fullConfig" # Landroid/content/res/Configuration; */
/* .line 373 */
v0 = android.util.MiuiMultiWindowAdapter .notNeedRelunchFreeform ( p1,p2,p3 );
} // .end method
public void onForegroundWindowChanged ( com.android.server.wm.WindowProcessController p0, android.content.pm.ActivityInfo p1, com.android.server.wm.ActivityRecord p2, com.android.server.wm.ActivityRecord$State p3 ) {
/* .locals 0 */
/* .param p1, "app" # Lcom/android/server/wm/WindowProcessController; */
/* .param p2, "info" # Landroid/content/pm/ActivityInfo; */
/* .param p3, "record" # Lcom/android/server/wm/ActivityRecord; */
/* .param p4, "state" # Lcom/android/server/wm/ActivityRecord$State; */
/* .line 449 */
com.android.server.wm.ActivityTaskManagerServiceImpl .onForegroundWindowChanged ( p1,p2,p3,p4 );
/* .line 450 */
return;
} // .end method
public Float reviewFreeFormBounds ( android.graphics.Rect p0, android.graphics.Rect p1, Float p2, android.graphics.Rect p3 ) {
/* .locals 1 */
/* .param p1, "currentBounds" # Landroid/graphics/Rect; */
/* .param p2, "newBounds" # Landroid/graphics/Rect; */
/* .param p3, "currentScale" # F */
/* .param p4, "accessibleArea" # Landroid/graphics/Rect; */
/* .line 543 */
v0 = android.util.MiuiMultiWindowUtils .reviewFreeFormBounds ( p1,p2,p3,p4 );
} // .end method
public void setAbnormalFreeformBlackList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 75 */
/* .local p1, "abnormalFreeformBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setAbnormalFreeformBlackList ( p1 );
/* .line 76 */
return;
} // .end method
public void setAbnormalFreeformWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 83 */
/* .local p1, "abnormalFreeformWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setAbnormalFreeformWhiteList ( p1 );
/* .line 84 */
return;
} // .end method
public void setAdditionalFreeformAspectRatio1Apps ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 346 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setAdditionalFreeformAspectRatio1Apps ( p1 );
/* .line 347 */
return;
} // .end method
public void setAdditionalFreeformAspectRatio2Apps ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 354 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setAdditionalFreeformAspectRatio2Apps ( p1 );
/* .line 355 */
return;
} // .end method
public void setApplicationLockActivityList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 222 */
/* .local p1, "applicationLockActivityList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setApplicationLockActivityList ( p1 );
/* .line 223 */
return;
} // .end method
public void setAudioForegroundPinAppList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 266 */
/* .local p1, "audioForegroundPinAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setAudioForegroundPinAppList ( p1 );
/* .line 267 */
return;
} // .end method
public void setCanStartActivityFromFullscreenToFreefromList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 54 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setCanStartActivityFromFullscreenToFreefromList ( p1 );
/* .line 55 */
return;
} // .end method
public void setCvwUnsupportedFreeformWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 215 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setCvwUnsupportedFreeformWhiteList ( p1 );
/* .line 216 */
return;
} // .end method
public void setDesktopFreeformWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 365 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setDesktopFreeformWhiteList ( p1 );
/* .line 366 */
return;
} // .end method
public void setDesktopModeLaunchFreeformIgnoreTranslucentAppList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 671 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setDesktopModeLaunchFreeformIgnoreTranslucentAppList ( p1 );
/* .line 672 */
return;
} // .end method
public void setDesktopModeLaunchFullscreenAppList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 659 */
/* .local p1, "needRelunchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setDesktopModeLaunchFullscreenAppList ( p1 );
/* .line 660 */
return;
} // .end method
public void setDesktopModeLaunchFullscreenNotHideOtherAppList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 668 */
/* .local p1, "needRelunchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setDesktopModeLaunchFullscreenNotHideOtherAppList ( p1 );
/* .line 669 */
return;
} // .end method
public void setEnableAbnormalFreeFormDebug ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "enableAbnormalFreeFormDebug" # I */
/* .line 330 */
android.util.MiuiMultiWindowAdapter .setEnableAbnormalFreeFormDebug ( p1 );
/* .line 331 */
return;
} // .end method
public void setEnableForegroundPin ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enableForegroundPin" # Z */
/* .line 262 */
android.util.MiuiMultiWindowAdapter .setEnableForegroundPin ( p1 );
/* .line 263 */
return;
} // .end method
public void setFixedRotationAppList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 290 */
/* .local p1, "fixedRotationAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFixedRotationAppList ( p1 );
/* .line 291 */
return;
} // .end method
public void setForceLandscapeApplication ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 238 */
/* .local p1, "forceLandscapeApplication":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setForceLandscapeApplication ( p1 );
/* .line 239 */
return;
} // .end method
public void setForegroundPinAppBlackList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 282 */
/* .local p1, "foregroundPinAppBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setForegroundPinAppBlackList ( p1 );
/* .line 283 */
return;
} // .end method
public void setForegroundPinAppWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 274 */
/* .local p1, "foregroundPinAppWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setForegroundPinAppWhiteList ( p1 );
/* .line 275 */
return;
} // .end method
public void setFreeformBlackList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 62 */
/* .local p1, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformBlackList ( p1 );
/* .line 63 */
return;
} // .end method
public void setFreeformCaptionInsetsHeightToZeroList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 230 */
/* .local p1, "freeformCaptionInsetsHeightToZeroList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformCaptionInsetsHeightToZeroList ( p1 );
/* .line 231 */
return;
} // .end method
public void setFreeformDisableOverlayList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 155 */
/* .local p1, "disableOverlayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformDisableOverlayList ( p1 );
/* .line 156 */
return;
} // .end method
public void setFreeformIgnoreRequestOrientationList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 163 */
/* .local p1, "ignoreRequestOrientationList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformIgnoreRequestOrientationList ( p1 );
/* .line 164 */
return;
} // .end method
public void setFreeformNeedRelunchList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 171 */
/* .local p1, "needRelunchList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformNeedRelunchList ( p1 );
/* .line 172 */
return;
} // .end method
public void setFreeformRecommendMapApplicationList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 99 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformRecommendMapApplicationList ( p1 );
/* .line 100 */
return;
} // .end method
public void setFreeformRecommendWaitingApplicationList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 107 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformRecommendWaitingApplicationList ( p1 );
/* .line 108 */
return;
} // .end method
public void setFreeformResizeableWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 203 */
/* .local p1, "resizeableWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformResizeableWhiteList ( p1 );
/* .line 204 */
return;
} // .end method
public Boolean setFreeformScale ( Float p0, android.app.ActivityOptions p1 ) {
/* .locals 7 */
/* .param p1, "scale" # F */
/* .param p2, "options" # Landroid/app/ActivityOptions; */
/* .line 595 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v1, v0, [Ljava/lang/Object; */
final String v2 = "getActivityOptionsInjector"; // const-string v2, "getActivityOptionsInjector"
android.util.MiuiMultiWindowUtils .isMethodExist ( p2,v2,v1 );
/* .line 597 */
/* .local v1, "method":Ljava/lang/reflect/Method; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 599 */
try { // :try_start_0
/* new-array v2, v0, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( p2, v2 ); // invoke-virtual {v1, p2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* const-string/jumbo v3, "setFreeformScale" */
int v4 = 1; // const/4 v4, 0x1
/* new-array v5, v4, [Ljava/lang/Object; */
/* .line 600 */
java.lang.Float .valueOf ( p1 );
/* aput-object v6, v5, v0 */
/* .line 599 */
android.util.MiuiMultiWindowUtils .invoke ( v2,v3,v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v0, v4 */
} // :cond_0
/* .line 601 */
/* :catch_0 */
/* move-exception v2 */
/* .line 602 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 605 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // .end method
public void setFreeformVideoWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 91 */
/* .local p1, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setFreeformVideoWhiteList ( p1 );
/* .line 92 */
return;
} // .end method
public void setHideSelfIfNewFreeformTaskWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 187 */
/* .local p1, "hideSelfIfNewFreeformTaskWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setHideSelfIfNewFreeformTaskWhiteList ( p1 );
/* .line 188 */
return;
} // .end method
public void setLaunchInTaskList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 322 */
/* .local p1, "launchInTaskList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setLaunchInTaskList ( p1 );
/* .line 323 */
return;
} // .end method
public void setLockModeActivityList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 147 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setAboutLockModeActivityList ( p1 );
/* .line 148 */
return;
} // .end method
public void setMiuiMultiWindowUtilsScale ( Float p0 ) {
/* .locals 0 */
/* .param p1, "scale" # F */
/* .line 420 */
/* .line 421 */
return;
} // .end method
public void setRotationFromDisplayApp ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 298 */
/* .local p1, "rotationFromDisplayApp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setRotationFromDisplayApp ( p1 );
/* .line 299 */
return;
} // .end method
public com.android.server.wm.SafeActivityOptions setSafeActivityOptions ( com.android.server.wm.Task p0 ) {
/* .locals 6 */
/* .param p1, "task" # Lcom/android/server/wm/Task; */
/* .line 766 */
if ( p1 != null) { // if-eqz p1, :cond_5
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isDeskTopModeActive ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = (( com.android.server.wm.Task ) p1 ).inFreeformWindowingMode ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->inFreeformWindowingMode()Z
/* if-nez v0, :cond_0 */
/* .line 769 */
} // :cond_0
v0 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isSupportFreeFormInDesktopMode ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isSupportFreeFormInDesktopMode(Lcom/android/server/wm/Task;)Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* .line 770 */
android.app.ActivityOptions .makeBasic ( );
/* .line 771 */
/* .local v0, "options":Landroid/app/ActivityOptions; */
(( android.app.ActivityOptions ) v0 ).setLaunchWindowingMode ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
/* .line 772 */
/* new-instance v1, Lcom/android/server/wm/SafeActivityOptions; */
/* invoke-direct {v1, v0}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V */
/* .line 774 */
} // .end local v0 # "options":Landroid/app/ActivityOptions;
} // :cond_1
v0 = this.origActivity;
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = this.origActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 775 */
} // :cond_2
v0 = this.realActivity;
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.realActivity;
(( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 776 */
} // :cond_3
(( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 777 */
(( com.android.server.wm.Task ) p1 ).getTopNonFinishingActivity ( ); // invoke-virtual {p1}, Lcom/android/server/wm/Task;->getTopNonFinishingActivity()Lcom/android/server/wm/ActivityRecord;
v0 = this.packageName;
} // :cond_4
/* const-string/jumbo v0, "unknown" */
} // :goto_0
/* nop */
/* .line 778 */
/* .local v0, "packageName":Ljava/lang/String; */
/* new-instance v2, Lcom/android/server/wm/SafeActivityOptions; */
v3 = this.mAtmService;
v3 = this.mContext;
/* .line 779 */
v4 = (( com.android.server.wm.MiuiFreeformUtilImpl ) p0 ).isDeskTopModeActive ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiFreeformUtilImpl;->isDeskTopModeActive()Z
/* .line 778 */
int v5 = 0; // const/4 v5, 0x0
android.util.MiuiMultiWindowUtils .getActivityOptions ( v3,v0,v1,v5,v4 );
/* invoke-direct {v2, v1}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V */
/* .line 767 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // :cond_5
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void setSensorDisableList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 338 */
/* .local p1, "sensorDisableList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setSensorDisableList ( p1 );
/* .line 339 */
return;
} // .end method
public void setSensorDisableWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 314 */
/* .local p1, "sensorDisableWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setSensorDisableWhiteList ( p1 );
/* .line 315 */
return;
} // .end method
public void setShowHiddenTaskIfFinishedWhiteList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 195 */
/* .local p1, "showHiddenTaskIfFinishedWhiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setShowHiddenTaskIfFinishedWhiteList ( p1 );
/* .line 196 */
return;
} // .end method
public void setStartFromFreeformBlackList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 179 */
/* .local p1, "startFromFreeformBlackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setStartFromFreeformBlackList ( p1 );
/* .line 180 */
return;
} // .end method
public void setSupportCvwLevelFullList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 115 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setSupportCvwLevelFullList ( p1 );
/* .line 116 */
return;
} // .end method
public void setSupportCvwLevelHorizontalList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 131 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setSupportCvwLevelHorizontalList ( p1 );
/* .line 132 */
return;
} // .end method
public void setSupportCvwLevelVerticalList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 123 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setSupportCvwLevelVerticalList ( p1 );
/* .line 124 */
return;
} // .end method
public void setTopGameList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 246 */
/* .local p1, "topGameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setTopGameList ( p1 );
/* .line 247 */
return;
} // .end method
public void setTopVideoList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 254 */
/* .local p1, "topVideoList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setTopVideoList ( p1 );
/* .line 255 */
return;
} // .end method
public void setUnsupportCvwLevelFullList ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 139 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setUnsupportCvwLevelFullList ( p1 );
/* .line 140 */
return;
} // .end method
public void setUseDefaultCameraPipelineApp ( java.util.List p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 306 */
/* .local p1, "useDefaultCameraPipelineApp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
android.util.MiuiMultiWindowAdapter .setUseDefaultCameraPipelineApp ( p1 );
/* .line 307 */
return;
} // .end method
public Boolean shouldSkipRelaunchForDkt ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "activityName" # Ljava/lang/String; */
/* .line 530 */
v0 = android.util.MiuiMultiWindowAdapter .shouldSkipRelaunchForDkt ( p1 );
} // .end method
public Boolean supportsFreeform ( ) {
/* .locals 1 */
/* .line 368 */
v0 = com.android.server.wm.ActivityTaskSupervisorImpl .supportsFreeform ( );
} // .end method
public void updateApplicationConfiguration ( com.android.server.wm.ActivityTaskSupervisor p0, android.content.res.Configuration p1, java.lang.String p2 ) {
/* .locals 0 */
/* .param p1, "stackSupervisor" # Lcom/android/server/wm/ActivityTaskSupervisor; */
/* .param p2, "globalConfiguration" # Landroid/content/res/Configuration; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 49 */
com.android.server.wm.ActivityTaskSupervisorImpl .updateApplicationConfiguration ( p1,p2,p3 );
/* .line 51 */
return;
} // .end method
