.class public Lcom/android/server/wm/OrientationSensorJudgeImpl;
.super Lcom/android/server/wm/OrientationSensorJudgeStub;
.source "OrientationSensorJudgeImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;,
        Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl;
    }
.end annotation


# static fields
.field private static final LOG:Z

.field private static final MSG_UPDATE_FOREGROUND_APP:I = 0x2

.field private static final MSG_UPDATE_FOREGROUND_APP_SYNC:I = 0x1

.field private static final TAG:Ljava/lang/String; = "OrientationSensorJudgeImpl"

.field private static final mSupportUIOrientationV2:Z


# instance fields
.field private mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mActivityTaskManagerInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

.field private mDesiredRotation:I

.field private mForegroundAppPackageName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mLastPackageName:Ljava/lang/String;

.field private mLastReportedRotation:I

.field private mOrientationSensorJudge:Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

.field private mPendingForegroundAppPackageName:Ljava/lang/String;

.field private mTaskStackListener:Landroid/app/TaskStackListener;


# direct methods
.method public static synthetic $r8$lambda$CBmdnJPE8xJjtqE4B66hgR76txI(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->lambda$initialize$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmActivityTaskManager(Lcom/android/server/wm/OrientationSensorJudgeImpl;)Landroid/app/IActivityTaskManager;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmForegroundAppPackageName(Lcom/android/server/wm/OrientationSensorJudgeImpl;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mForegroundAppPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/server/wm/OrientationSensorJudgeImpl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmPendingForegroundAppPackageName(Lcom/android/server/wm/OrientationSensorJudgeImpl;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateForegroundApp(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundApp()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateForegroundAppSync(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundAppSync()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetLOG()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->LOG:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 26
    const-string v0, "debug.orientation.log"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->LOG:Z

    .line 29
    nop

    .line 30
    const-string/jumbo v0, "support_ui_orientation_v2"

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mSupportUIOrientationV2:Z

    .line 29
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeStub;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastPackageName:Ljava/lang/String;

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I

    .line 35
    iput v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastReportedRotation:I

    return-void
.end method

.method private getTopAppPackageName()Ljava/lang/String;
    .locals 3

    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mActivityTaskManagerInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    if-eqz v1, :cond_0

    .line 161
    nop

    .line 162
    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerInternal;->getTopApp()Lcom/android/server/wm/WindowProcessController;

    move-result-object v1

    .line 163
    .local v1, "controller":Lcom/android/server/wm/WindowProcessController;
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 166
    iget-object v2, v1, Lcom/android/server/wm/WindowProcessController;->mInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 169
    .end local v1    # "controller":Lcom/android/server/wm/WindowProcessController;
    :cond_0
    return-object v0
.end method

.method private synthetic lambda$initialize$0()V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->registerForegroundAppUpdater()V

    .line 57
    return-void
.end method

.method private registerForegroundAppUpdater()V
    .locals 2

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    iget-object v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V

    .line 69
    invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->updateForegroundApp()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 73
    :goto_0
    return-void
.end method

.method private updateForegroundApp()V
    .locals 2

    .line 81
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;

    invoke-direct {v1, p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl$1;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 114
    return-void
.end method

.method private updateForegroundAppSync()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mForegroundAppPackageName:Ljava/lang/String;

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mPendingForegroundAppPackageName:Ljava/lang/String;

    .line 122
    sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->LOG:Z

    if-eqz v0, :cond_0

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateForegroundAppSync, mPendingPackage = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mForegroundAppPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrientationSensorJudgeImpl"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    iget v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastReportedRotation:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 126
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mOrientationSensorJudge:Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->callFinalizeRotation(I)V

    .line 128
    :cond_1
    return-void
.end method


# virtual methods
.method public finalizeRotation(I)I
    .locals 4
    .param p1, "reportedRotation"    # I

    .line 132
    invoke-direct {p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl;->getTopAppPackageName()Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "packageName":Ljava/lang/String;
    iput p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastReportedRotation:I

    .line 134
    if-ltz p1, :cond_0

    const/4 v1, 0x3

    if-gt p1, v1, :cond_0

    .line 135
    iput p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I

    .line 136
    iget-object v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mOrientationSensorJudge:Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

    invoke-virtual {v1, p1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->updateDesiredRotation(I)V

    .line 137
    iget-object v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mOrientationSensorJudge:Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->evaluateRotationChangeLocked()I

    move-result v1

    .local v1, "newRotation":I
    goto :goto_0

    .line 138
    .end local v1    # "newRotation":I
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 139
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 140
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I

    .line 141
    iget-object v2, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mOrientationSensorJudge:Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

    invoke-virtual {v2, v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->updateDesiredRotation(I)V

    .line 142
    iget-object v1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mOrientationSensorJudge:Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

    invoke-virtual {v1}, Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;->evaluateRotationChangeLocked()I

    move-result v1

    .restart local v1    # "newRotation":I
    goto :goto_0

    .line 144
    .end local v1    # "newRotation":I
    :cond_1
    const/4 v1, -0x1

    .restart local v1    # "newRotation":I
    goto :goto_0

    .line 147
    .end local v1    # "newRotation":I
    :cond_2
    const/4 v1, -0x1

    .line 149
    .restart local v1    # "newRotation":I
    :goto_0
    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastPackageName:Ljava/lang/String;

    .line 150
    sget-boolean v2, Lcom/android/server/wm/OrientationSensorJudgeImpl;->LOG:Z

    if-eqz v2, :cond_3

    .line 151
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finalizeRotation: reportedRotation = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mDesiredRotation = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " newRotation = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mLastPackageName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mLastPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "OrientationSensorJudgeImpl"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_3
    return v1
.end method

.method public initialize(Landroid/content/Context;Landroid/os/Looper;Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "judge"    # Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

    .line 47
    sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mSupportUIOrientationV2:Z

    if-nez v0, :cond_0

    .line 48
    return-void

    .line 50
    :cond_0
    new-instance v0, Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;

    invoke-direct {v0, p0, p2}, Lcom/android/server/wm/OrientationSensorJudgeImpl$OrientationSensorJudgeImplHandler;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mHandler:Landroid/os/Handler;

    .line 51
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 52
    const-class v0, Lcom/android/server/wm/ActivityTaskManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityTaskManagerInternal;

    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mActivityTaskManagerInternal:Lcom/android/server/wm/ActivityTaskManagerInternal;

    .line 53
    iput-object p3, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mOrientationSensorJudge:Lcom/android/server/wm/WindowOrientationListener$OrientationSensorJudge;

    .line 54
    new-instance v0, Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl;

    invoke-direct {v0, p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl$TaskStackListenerImpl;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V

    iput-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mTaskStackListener:Landroid/app/TaskStackListener;

    .line 55
    iget-object v0, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/OrientationSensorJudgeImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/server/wm/OrientationSensorJudgeImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/OrientationSensorJudgeImpl;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 58
    return-void
.end method

.method public isSupportUIOrientationV2()Z
    .locals 1

    .line 181
    sget-boolean v0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mSupportUIOrientationV2:Z

    return v0
.end method

.method public updateDesiredRotation(I)V
    .locals 0
    .param p1, "desiredRotation"    # I

    .line 173
    iput p1, p0, Lcom/android/server/wm/OrientationSensorJudgeImpl;->mDesiredRotation:I

    .line 174
    return-void
.end method
