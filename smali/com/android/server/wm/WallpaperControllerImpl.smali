.class public Lcom/android/server/wm/WallpaperControllerImpl;
.super Ljava/lang/Object;
.source "WallpaperControllerImpl.java"

# interfaces
.implements Lcom/android/server/wm/WallpaperControllerStub;


# static fields
.field private static final DEBUG:Z = false

.field private static final KEYGUARD_DESCRIPTOR:Ljava/lang/String; = "miui.systemui.keyguard.Wallpaper"

.field private static final SCROLL_DESKTOP_WALLPAPER:Ljava/lang/String; = "pref_key_wallpaper_screen_scrolled_span"

.field private static final TAG:Ljava/lang/String; = "WallpaperControllerImpl"

.field private static final WALLPAPER_OFFSET_CENTER:F = 0.5f

.field private static final WALLPAPER_OFFSET_DEFAULT:F = -1.0f


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public findWallpapers(Lcom/android/server/wm/WindowState;Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;)V
    .locals 3
    .param p1, "w"    # Lcom/android/server/wm/WindowState;
    .param p2, "findResult"    # Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;

    .line 59
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mAttrs:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v1, 0x7dd

    if-ne v0, v1, :cond_3

    .line 60
    iget-object v0, p1, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    invoke-virtual {v0}, Lcom/android/server/wm/WindowToken;->asWallpaperToken()Lcom/android/server/wm/WallpaperWindowToken;

    move-result-object v0

    .line 61
    .local v0, "token":Lcom/android/server/wm/WallpaperWindowToken;
    invoke-virtual {v0}, Lcom/android/server/wm/WallpaperWindowToken;->canShowWhenLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->hasTopShowWhenLockedWallpaper()Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    invoke-virtual {p2, p1}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->setTopShowWhenLockedWallpaper(Lcom/android/server/wm/WindowState;)V

    goto :goto_1

    .line 63
    :cond_0
    invoke-virtual {v0}, Lcom/android/server/wm/WallpaperWindowToken;->canShowWhenLocked()Z

    move-result v1

    if-nez v1, :cond_3

    .line 65
    :try_start_0
    const-string v1, "miui.systemui.keyguard.Wallpaper"

    iget-object v2, p1, Lcom/android/server/wm/WindowState;->mToken:Lcom/android/server/wm/WindowToken;

    iget-object v2, v2, Lcom/android/server/wm/WindowToken;->token:Landroid/os/IBinder;

    invoke-interface {v2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {p2, p1}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->setTopShowWhenLockedWallpaper(Lcom/android/server/wm/WindowState;)V

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p2}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->hasTopHideWhenLockedWallpaper()Z

    move-result v1

    if-nez v1, :cond_2

    .line 68
    invoke-virtual {p2, p1}, Lcom/android/server/wm/WallpaperController$FindWallpaperTargetResult;->setTopHideWhenLockedWallpaper(Lcom/android/server/wm/WindowState;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_2
    :goto_0
    goto :goto_1

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 75
    .end local v0    # "token":Lcom/android/server/wm/WallpaperWindowToken;
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_3
    :goto_1
    return-void
.end method

.method public getLastWallpaperX(Landroid/content/Context;)F
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    const/high16 v0, -0x40800000    # -1.0f

    if-eqz p1, :cond_1

    .line 40
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v1, :cond_2

    .line 42
    nop

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "pref_key_wallpaper_screen_scrolled_span"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    const/high16 v0, 0x3f000000    # 0.5f

    :goto_0
    nop

    .line 48
    .local v0, "wallpaperX":F
    return v0

    .line 51
    .end local v0    # "wallpaperX":F
    :cond_1
    const-string v1, "WallpaperControllerImpl"

    const-string v2, "getLastWallpaperX: fail, context null"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :cond_2
    return v0
.end method

.method public showWallpaperIfNeeded(Landroid/window/TransitionInfo;Lcom/android/server/wm/DisplayContent;Landroid/view/SurfaceControl$Transaction;)V
    .locals 10
    .param p1, "info"    # Landroid/window/TransitionInfo;
    .param p2, "displayContent"    # Lcom/android/server/wm/DisplayContent;
    .param p3, "t"    # Landroid/view/SurfaceControl$Transaction;

    .line 79
    invoke-virtual {p1}, Landroid/window/TransitionInfo;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a

    sget-boolean v0, Lmiui/os/DeviceFeature;->IS_FOLD_DEVICE:Z

    if-nez v0, :cond_a

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 82
    :cond_0
    const/4 v0, 0x0

    .line 83
    .local v0, "hasHomeOpen":Z
    const/4 v2, 0x0

    .line 84
    .local v2, "hasAppClose":Z
    const/4 v3, 0x0

    .line 85
    .local v3, "hasWallpaperOpen":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ge v4, v5, :cond_7

    .line 86
    invoke-virtual {p1}, Landroid/window/TransitionInfo;->getChanges()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/window/TransitionInfo$Change;

    .line 87
    .local v5, "change":Landroid/window/TransitionInfo$Change;
    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I

    move-result v7

    if-eq v7, v1, :cond_1

    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I

    move-result v7

    const/4 v8, 0x4

    if-ne v7, v8, :cond_2

    .line 88
    :cond_1
    invoke-static {v5, p1}, Landroid/window/TransitionInfo;->isIndependent(Landroid/window/TransitionInfo$Change;Landroid/window/TransitionInfo;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 89
    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 90
    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActivityManager$RunningTaskInfo;->getActivityType()I

    move-result v7

    if-ne v7, v6, :cond_2

    .line 91
    const/4 v2, 0x1

    .line 93
    :cond_2
    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I

    move-result v7

    const/4 v8, 0x3

    if-eq v7, v6, :cond_3

    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I

    move-result v7

    if-ne v7, v8, :cond_4

    .line 94
    :cond_3
    invoke-static {v5, p1}, Landroid/window/TransitionInfo;->isIndependent(Landroid/window/TransitionInfo$Change;Landroid/window/TransitionInfo;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 95
    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 96
    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getTaskInfo()Landroid/app/ActivityManager$RunningTaskInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ActivityManager$RunningTaskInfo;->getActivityType()I

    move-result v7

    if-ne v7, v1, :cond_4

    .line 97
    const/4 v0, 0x1

    .line 100
    :cond_4
    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I

    move-result v7

    if-eq v7, v6, :cond_5

    invoke-virtual {v5}, Landroid/window/TransitionInfo$Change;->getMode()I

    move-result v6

    if-ne v6, v8, :cond_6

    .line 101
    :cond_5
    invoke-virtual {v5, v1}, Landroid/window/TransitionInfo$Change;->hasFlags(I)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 102
    const/4 v3, 0x1

    .line 85
    .end local v5    # "change":Landroid/window/TransitionInfo$Change;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 105
    .end local v4    # "i":I
    :cond_7
    if-nez v2, :cond_9

    if-eqz v0, :cond_9

    if-eqz v3, :cond_9

    .line 106
    iget-object v1, p2, Lcom/android/server/wm/DisplayContent;->mWallpaperController:Lcom/android/server/wm/WallpaperController;

    invoke-virtual {v1}, Lcom/android/server/wm/WallpaperController;->getAllTopWallpapers()Ljava/util/List;

    move-result-object v1

    .line 107
    .local v1, "wallpapers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/WindowState;>;"
    if-eqz v1, :cond_9

    .line 108
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/server/wm/WindowState;

    .line 109
    .local v5, "wallpaper":Lcom/android/server/wm/WindowState;
    iget-object v7, v5, Lcom/android/server/wm/WindowState;->mWinAnimator:Lcom/android/server/wm/WindowStateAnimator;

    .line 110
    .local v7, "wsa":Lcom/android/server/wm/WindowStateAnimator;
    if-eqz v7, :cond_8

    iget v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mDrawState:I

    if-ne v8, v6, :cond_8

    iget v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mShownAlpha:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v8, v8, v9

    if-nez v8, :cond_8

    .line 111
    iget-object v8, v7, Lcom/android/server/wm/WindowStateAnimator;->mSurfaceController:Lcom/android/server/wm/WindowSurfaceController;

    invoke-virtual {v8, p3}, Lcom/android/server/wm/WindowSurfaceController;->showRobustly(Landroid/view/SurfaceControl$Transaction;)V

    .line 113
    .end local v5    # "wallpaper":Lcom/android/server/wm/WindowState;
    .end local v7    # "wsa":Lcom/android/server/wm/WindowStateAnimator;
    :cond_8
    goto :goto_1

    .line 116
    .end local v1    # "wallpapers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/wm/WindowState;>;"
    :cond_9
    return-void

    .line 80
    .end local v0    # "hasHomeOpen":Z
    .end local v2    # "hasAppClose":Z
    .end local v3    # "hasWallpaperOpen":Z
    :cond_a
    :goto_2
    return-void
.end method
