.class Lcom/android/server/wm/WindowManagerServiceImpl$2;
.super Landroid/app/IWallpaperManagerCallback$Stub;
.source "WindowManagerServiceImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/WindowManagerServiceImpl;->registerBootCompletedReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/WindowManagerServiceImpl;


# direct methods
.method constructor <init>(Lcom/android/server/wm/WindowManagerServiceImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/WindowManagerServiceImpl;

    .line 357
    iput-object p1, p0, Lcom/android/server/wm/WindowManagerServiceImpl$2;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    invoke-direct {p0}, Landroid/app/IWallpaperManagerCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onWallpaperChanged()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 360
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl$2;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(I)V

    .line 361
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl$2;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z

    .line 362
    return-void
.end method

.method public onWallpaperColorsChanged(Landroid/app/WallpaperColors;II)V
    .locals 2
    .param p1, "colors"    # Landroid/app/WallpaperColors;
    .param p2, "which"    # I
    .param p3, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 366
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl$2;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->removeMessages(I)V

    .line 367
    iget-object v0, p0, Lcom/android/server/wm/WindowManagerServiceImpl$2;->this$0:Lcom/android/server/wm/WindowManagerServiceImpl;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerServiceImpl;->mWmService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mH:Lcom/android/server/wm/WindowManagerService$H;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->sendEmptyMessage(I)Z

    .line 368
    return-void
.end method
