public class com.android.server.wm.GetDragEventProxy {
	 /* .source "GetDragEventProxy.java" */
	 /* # static fields */
	 public static com.xiaomi.reflect.RefInt DRAG_FLAGS_URI_ACCESS;
	 /* .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments; */
} // .end annotation
} // .end field
public static com.xiaomi.reflect.RefInt DRAG_FLAGS_URI_PERMISSIONS;
/* .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments; */
} // .end annotation
} // .end field
public static com.xiaomi.reflect.RefObject mData;
/* .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments; */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/xiaomi/reflect/RefObject<", */
/* "Landroid/content/ClipData;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static com.xiaomi.reflect.RefInt mFlags;
/* .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments; */
} // .end annotation
} // .end field
public static com.xiaomi.reflect.RefObject mService;
/* .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments; */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/xiaomi/reflect/RefObject<", */
/* "Lcom/android/server/wm/WindowManagerService;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static com.xiaomi.reflect.RefInt mSourceUserId;
/* .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments; */
} // .end annotation
} // .end field
public static com.xiaomi.reflect.RefInt mUid;
/* .annotation runtime Lcom/xiaomi/reflect/annotation/FieldArguments; */
} // .end annotation
} // .end field
public static com.xiaomi.reflect.RefMethod obtainDragEvent;
/* .annotation runtime Lcom/xiaomi/reflect/annotation/MethodQualifiedArguments; */
/* classNames = { */
/* "int", */
/* "float", */
/* "float", */
/* "android.content.ClipData", */
/* "boolean", */
/* "com.android.internal.view.IDragAndDropPermissions" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lcom/xiaomi/reflect/RefMethod<", */
/* "Landroid/view/DragEvent;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.wm.GetDragEventProxy ( ) {
/* .locals 2 */
/* .line 18 */
/* const-class v0, Lcom/android/server/wm/GetDragEventProxy; */
final String v1 = "com.android.server.wm.DragState"; // const-string v1, "com.android.server.wm.DragState"
com.xiaomi.reflect.RefClass .attach ( v0,v1 );
/* .line 19 */
return;
} // .end method
public com.android.server.wm.GetDragEventProxy ( ) {
/* .locals 0 */
/* .line 16 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
