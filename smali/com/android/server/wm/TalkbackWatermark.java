class com.android.server.wm.TalkbackWatermark implements com.android.server.wm.TalkbackWatermarkStub {
	 /* .source "TalkbackWatermark.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.graphics.BLASTBufferQueue mBlastBufferQueue;
	 private android.content.BroadcastReceiver mBroadcast;
	 private final Float mDetDp;
	 private Integer mDetPx;
	 private Boolean mDrawNeeded;
	 private Boolean mHasDrawn;
	 private Integer mLastDH;
	 private Integer mLastDW;
	 private final Float mPaddingDp;
	 private Integer mPaddingPx;
	 private final Float mShadowDx;
	 private final Float mShadowDy;
	 private final Float mShadowRadius;
	 private java.lang.String mString1;
	 private java.lang.String mString2;
	 private android.view.Surface mSurface;
	 private android.view.SurfaceControl mSurfaceControl;
	 private final Float mTextSizeDp;
	 private Integer mTextSizePx;
	 private final Float mTitleSizeDp;
	 private Float mTitleSizePx;
	 private android.view.SurfaceControl$Transaction mTransaction;
	 private com.android.server.wm.WindowManagerService mWms;
	 private final Float mXProportion;
	 private final Float mYProportionTop;
	 /* # direct methods */
	 public static void $r8$lambda$37iju6o-pbGYJtR2DbIm9LM8X74 ( com.android.server.wm.TalkbackWatermark p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->lambda$show$1()V */
		 return;
	 } // .end method
	 public static void $r8$lambda$EP-Zo72UAMbTgk_9DhzaaAQRevs ( com.android.server.wm.TalkbackWatermark p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->lambda$updateWaterMark$0()V */
		 return;
	 } // .end method
	 public static void $r8$lambda$eVfletGoOtHMriGPp_gr3ZUCg7U ( com.android.server.wm.TalkbackWatermark p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->dismissInternal()V */
		 return;
	 } // .end method
	 public static void $r8$lambda$lCnD30CJjjaPm4mWDDhqHPVtkdA ( com.android.server.wm.TalkbackWatermark p0 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->lambda$dismiss$2()V */
		 return;
	 } // .end method
	 static void -$$Nest$mrefresh ( com.android.server.wm.TalkbackWatermark p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->refresh()V */
		 return;
	 } // .end method
	 static void -$$Nest$mupdateWaterMark ( com.android.server.wm.TalkbackWatermark p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->updateWaterMark()V */
		 return;
	 } // .end method
	 com.android.server.wm.TalkbackWatermark ( ) {
		 /* .locals 1 */
		 /* .line 64 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 69 */
		 /* const v0, 0x41cb999a # 25.45f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mTitleSizeDp:F */
		 /* .line 70 */
		 /* const/high16 v0, 0x41a00000 # 20.0f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mTextSizeDp:F */
		 /* .line 71 */
		 /* const v0, 0x41a2f5c3 # 20.37f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mDetDp:F */
		 /* .line 72 */
		 /* const v0, 0x4145c28f # 12.36f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mPaddingDp:F */
		 /* .line 73 */
		 /* const/high16 v0, 0x3f800000 # 1.0f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mShadowRadius:F */
		 /* .line 74 */
		 /* const/high16 v0, 0x40000000 # 2.0f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mShadowDx:F */
		 /* .line 75 */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mShadowDy:F */
		 /* .line 80 */
		 /* const v0, 0x3ecccccd # 0.4f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mYProportionTop:F */
		 /* .line 81 */
		 /* const/high16 v0, 0x3f000000 # 0.5f */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mXProportion:F */
		 /* .line 85 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mSurfaceControl = v0;
		 /* .line 86 */
		 this.mTransaction = v0;
		 /* .line 87 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I */
		 /* .line 88 */
		 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I */
		 /* .line 89 */
		 /* iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z */
		 /* .line 90 */
		 /* iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mHasDrawn:Z */
		 return;
	 } // .end method
	 private synchronized void dismissInternal ( ) {
		 /* .locals 3 */
		 /* monitor-enter p0 */
		 /* .line 352 */
		 try { // :try_start_0
			 v0 = this.mSurfaceControl;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
			 /* if-nez v0, :cond_0 */
			 /* .line 353 */
			 /* monitor-exit p0 */
			 return;
			 /* .line 355 */
		 } // :cond_0
		 try { // :try_start_1
			 final String v0 = "TalkbackWatermark"; // const-string v0, "TalkbackWatermark"
			 /* const-string/jumbo v1, "talkback-test dismissInternal" */
			 android.util.Slog .d ( v0,v1 );
			 /* .line 356 */
			 /* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->hideInternal()V */
			 /* .line 357 */
			 v0 = this.mSurface;
			 (( android.view.Surface ) v0 ).destroy ( ); // invoke-virtual {v0}, Landroid/view/Surface;->destroy()V
			 /* .line 358 */
			 v0 = this.mBlastBufferQueue;
			 (( android.graphics.BLASTBufferQueue ) v0 ).destroy ( ); // invoke-virtual {v0}, Landroid/graphics/BLASTBufferQueue;->destroy()V
			 /* .line 359 */
			 v0 = this.mWms;
			 (( com.android.server.wm.WindowManagerService ) v0 ).openSurfaceTransaction ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
			 /* :try_end_1 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
			 /* .line 362 */
			 try { // :try_start_2
				 v0 = this.mSurfaceControl;
				 int v1 = 0; // const/4 v1, 0x0
				 (( android.view.SurfaceControl ) v0 ).reparent ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl;->reparent(Landroid/view/SurfaceControl;)V
				 /* .line 363 */
				 v0 = this.mSurfaceControl;
				 (( android.view.SurfaceControl ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->release()V
				 /* :try_end_2 */
				 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
				 /* .line 366 */
				 try { // :try_start_3
					 v0 = this.mWms;
					 /* const-string/jumbo v2, "updateTalkbackWatermark" */
					 (( com.android.server.wm.WindowManagerService ) v0 ).closeSurfaceTransaction ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
					 /* .line 367 */
					 /* nop */
					 /* .line 368 */
					 this.mBlastBufferQueue = v1;
					 /* .line 369 */
					 this.mSurfaceControl = v1;
					 /* .line 370 */
					 this.mSurface = v1;
					 /* .line 371 */
					 int v0 = 0; // const/4 v0, 0x0
					 /* iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mHasDrawn:Z */
					 /* .line 372 */
					 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I */
					 /* .line 373 */
					 /* iput v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I */
					 /* :try_end_3 */
					 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
					 /* .line 374 */
					 /* monitor-exit p0 */
					 return;
					 /* .line 366 */
				 } // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
				 /* :catchall_0 */
				 /* move-exception v0 */
				 try { // :try_start_4
					 v1 = this.mWms;
					 /* const-string/jumbo v2, "updateTalkbackWatermark" */
					 (( com.android.server.wm.WindowManagerService ) v1 ).closeSurfaceTransaction ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
					 /* .line 367 */
					 /* throw v0 */
					 /* :try_end_4 */
					 /* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
					 /* .line 351 */
					 /* :catchall_1 */
					 /* move-exception v0 */
					 /* monitor-exit p0 */
					 /* throw v0 */
				 } // .end method
				 private synchronized void doCreateSurface ( com.android.server.wm.WindowManagerService p0 ) {
					 /* .locals 10 */
					 /* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
					 /* monitor-enter p0 */
					 /* .line 154 */
					 try { // :try_start_0
						 (( com.android.server.wm.WindowManagerService ) p1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
						 /* .line 155 */
						 /* .local v0, "dc":Lcom/android/server/wm/DisplayContent; */
						 v1 = this.mRealDisplayMetrics;
						 /* iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I */
						 /* int-to-float v1, v1 */
						 /* const/high16 v2, 0x43200000 # 160.0f */
						 /* div-float/2addr v1, v2 */
						 /* .line 156 */
						 /* .local v1, "constNum":F */
						 /* const/high16 v2, 0x41a00000 # 20.0f */
						 /* mul-float/2addr v2, v1 */
						 /* float-to-int v2, v2 */
						 /* iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mTextSizePx:I */
						 /* .line 157 */
						 /* const v2, 0x41a2f5c3 # 20.37f */
						 /* mul-float/2addr v2, v1 */
						 /* float-to-int v2, v2 */
						 /* iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mDetPx:I */
						 /* .line 158 */
						 /* const v2, 0x4145c28f # 12.36f */
						 /* mul-float/2addr v2, v1 */
						 /* float-to-int v2, v2 */
						 /* iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mPaddingPx:I */
						 /* .line 159 */
						 /* const v2, 0x41cb999a # 25.45f */
						 /* mul-float/2addr v2, v1 */
						 /* float-to-int v2, v2 */
						 /* int-to-float v2, v2 */
						 /* iput v2, p0, Lcom/android/server/wm/TalkbackWatermark;->mTitleSizePx:F */
						 /* :try_end_0 */
						 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
						 /* .line 161 */
						 int v2 = 0; // const/4 v2, 0x0
						 /* .line 163 */
						 /* .local v2, "ctrl":Landroid/view/SurfaceControl; */
						 try { // :try_start_1
							 (( com.android.server.wm.DisplayContent ) v0 ).makeOverlay ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;
							 final String v4 = "TalkbackWatermarkSurface"; // const-string v4, "TalkbackWatermarkSurface"
							 /* .line 164 */
							 (( android.view.SurfaceControl$Builder ) v3 ).setName ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
							 /* .line 165 */
							 (( android.view.SurfaceControl$Builder ) v3 ).setBLASTLayer ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->setBLASTLayer()Landroid/view/SurfaceControl$Builder;
							 /* .line 166 */
							 int v4 = 1; // const/4 v4, 0x1
							 (( android.view.SurfaceControl$Builder ) v3 ).setBufferSize ( v4, v4 ); // invoke-virtual {v3, v4, v4}, Landroid/view/SurfaceControl$Builder;->setBufferSize(II)Landroid/view/SurfaceControl$Builder;
							 /* .line 167 */
							 int v4 = -3; // const/4 v4, -0x3
							 (( android.view.SurfaceControl$Builder ) v3 ).setFormat ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setFormat(I)Landroid/view/SurfaceControl$Builder;
							 final String v4 = "TalkbackWatermarkSurface"; // const-string v4, "TalkbackWatermarkSurface"
							 /* .line 168 */
							 (( android.view.SurfaceControl$Builder ) v3 ).setCallsite ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setCallsite(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
							 /* .line 169 */
							 (( android.view.SurfaceControl$Builder ) v3 ).build ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
							 /* move-object v2, v3 */
							 /* .line 170 */
							 v3 = this.mTransactionFactory;
							 /* check-cast v3, Landroid/view/SurfaceControl$Transaction; */
							 this.mTransaction = v3;
							 /* .line 173 */
							 /* const v4, 0xf4240 */
							 (( android.view.SurfaceControl$Transaction ) v3 ).setLayer ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
							 /* .line 174 */
							 v3 = this.mTransaction;
							 int v4 = 0; // const/4 v4, 0x0
							 (( android.view.SurfaceControl$Transaction ) v3 ).setPosition ( v2, v4, v4 ); // invoke-virtual {v3, v2, v4, v4}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
							 /* .line 175 */
							 v3 = this.mTransaction;
							 (( android.view.SurfaceControl$Transaction ) v3 ).show ( v2 ); // invoke-virtual {v3, v2}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
							 /* .line 176 */
							 v3 = this.mTransaction;
							 v4 = 							 (( com.android.server.wm.DisplayContent ) v0 ).getDisplayId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/DisplayContent;->getDisplayId()I
							 final String v5 = "TalkbackWatermarkSurface"; // const-string v5, "TalkbackWatermarkSurface"
							 com.android.server.wm.InputMonitor .setTrustedOverlayInputInfo ( v2,v3,v4,v5 );
							 /* .line 177 */
							 v3 = this.mTransaction;
							 (( android.view.SurfaceControl$Transaction ) v3 ).apply ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl$Transaction;->apply()V
							 /* .line 178 */
							 this.mSurfaceControl = v2;
							 /* .line 179 */
							 /* new-instance v3, Landroid/graphics/BLASTBufferQueue; */
							 final String v5 = "TalkbackWatermarkSurface"; // const-string v5, "TalkbackWatermarkSurface"
							 v6 = this.mSurfaceControl;
							 int v7 = 1; // const/4 v7, 0x1
							 int v8 = 1; // const/4 v8, 0x1
							 int v9 = 1; // const/4 v9, 0x1
							 /* move-object v4, v3 */
							 /* invoke-direct/range {v4 ..v9}, Landroid/graphics/BLASTBufferQueue;-><init>(Ljava/lang/String;Landroid/view/SurfaceControl;III)V */
							 this.mBlastBufferQueue = v3;
							 /* .line 181 */
							 (( android.graphics.BLASTBufferQueue ) v3 ).createSurface ( ); // invoke-virtual {v3}, Landroid/graphics/BLASTBufferQueue;->createSurface()Landroid/view/Surface;
							 this.mSurface = v3;
							 /* :try_end_1 */
							 /* .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_1 ..:try_end_1} :catch_0 */
							 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
							 /* .line 184 */
							 /* .line 182 */
						 } // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
						 /* :catch_0 */
						 /* move-exception v3 */
						 /* .line 183 */
						 /* .local v3, "e":Landroid/view/Surface$OutOfResourcesException; */
						 try { // :try_start_2
							 final String v4 = "TalkbackWatermark"; // const-string v4, "TalkbackWatermark"
							 /* new-instance v5, Ljava/lang/StringBuilder; */
							 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
							 final String v6 = "createrSurface e"; // const-string v6, "createrSurface e"
							 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
							 (( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
							 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
							 android.util.Slog .w ( v4,v5 );
							 /* :try_end_2 */
							 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
							 /* .line 185 */
						 } // .end local v3 # "e":Landroid/view/Surface$OutOfResourcesException;
					 } // :goto_0
					 /* monitor-exit p0 */
					 return;
					 /* .line 153 */
				 } // .end local v0 # "dc":Lcom/android/server/wm/DisplayContent;
			 } // .end local v1 # "constNum":F
		 } // .end local v2 # "ctrl":Landroid/view/SurfaceControl;
	 } // .end local p1 # "wms":Lcom/android/server/wm/WindowManagerService;
	 /* :catchall_0 */
	 /* move-exception p1 */
	 /* monitor-exit p0 */
	 /* throw p1 */
} // .end method
private void drawIfNeeded ( ) {
	 /* .locals 36 */
	 /* .line 204 */
	 /* move-object/from16 v1, p0 */
	 /* iget-boolean v0, v1, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z */
	 if ( v0 != null) { // if-eqz v0, :cond_4
		 /* .line 205 */
		 /* iget v2, v1, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I */
		 /* .line 206 */
		 /* .local v2, "dw":I */
		 /* iget v3, v1, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I */
		 /* .line 208 */
		 /* .local v3, "dh":I */
		 v0 = this.mBlastBufferQueue;
		 v4 = this.mSurfaceControl;
		 int v5 = 1; // const/4 v5, 0x1
		 (( android.graphics.BLASTBufferQueue ) v0 ).update ( v4, v2, v3, v5 ); // invoke-virtual {v0, v4, v2, v3, v5}, Landroid/graphics/BLASTBufferQueue;->update(Landroid/view/SurfaceControl;III)V
		 /* .line 210 */
		 /* new-instance v0, Landroid/graphics/Rect; */
		 int v4 = 0; // const/4 v4, 0x0
		 /* invoke-direct {v0, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V */
		 /* move-object v6, v0 */
		 /* .line 211 */
		 /* .local v6, "dirty":Landroid/graphics/Rect; */
		 int v7 = 0; // const/4 v7, 0x0
		 /* .line 213 */
		 /* .local v7, "c":Landroid/graphics/Canvas; */
		 try { // :try_start_0
			 v0 = this.mSurface;
			 (( android.view.Surface ) v0 ).lockCanvas ( v6 ); // invoke-virtual {v0, v6}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* move-object v7, v0 */
			 /* .line 216 */
			 /* .line 214 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 215 */
			 /* .local v0, "e":Ljava/lang/RuntimeException; */
			 final String v8 = "TalkbackWatermark"; // const-string v8, "TalkbackWatermark"
			 final String v9 = "Failed to lock canvas"; // const-string v9, "Failed to lock canvas"
			 android.util.Slog .w ( v8,v9,v0 );
			 /* .line 218 */
		 } // .end local v0 # "e":Ljava/lang/RuntimeException;
	 } // :goto_0
	 if ( v7 != null) { // if-eqz v7, :cond_3
		 v0 = 		 (( android.graphics.Canvas ) v7 ).getWidth ( ); // invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I
		 /* if-ne v0, v2, :cond_3 */
		 v0 = 		 (( android.graphics.Canvas ) v7 ).getHeight ( ); // invoke-virtual {v7}, Landroid/graphics/Canvas;->getHeight()I
		 /* if-eq v0, v3, :cond_0 */
		 /* goto/16 :goto_3 */
		 /* .line 222 */
	 } // :cond_0
	 /* iput-boolean v4, v1, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z */
	 /* .line 224 */
	 v0 = android.graphics.PorterDuff$Mode.CLEAR;
	 (( android.graphics.Canvas ) v7 ).drawColor ( v4, v0 ); // invoke-virtual {v7, v4, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V
	 /* .line 226 */
	 /* const/16 v0, 0x3c */
	 /* .line 227 */
	 /* .local v0, "offset":I */
	 /* const/high16 v8, 0x3f000000 # 0.5f */
	 /* int-to-float v9, v2 */
	 /* mul-float/2addr v9, v8 */
	 /* float-to-int v8, v9 */
	 /* .line 228 */
	 /* .local v8, "x":I */
	 /* const v9, 0x3ecccccd # 0.4f */
	 /* int-to-float v10, v3 */
	 /* mul-float/2addr v10, v9 */
	 /* float-to-int v9, v10 */
	 /* add-int/lit8 v9, v9, 0x3c */
	 /* .line 230 */
	 /* .local v9, "y":I */
	 /* new-instance v10, Landroid/graphics/Paint; */
	 /* invoke-direct {v10, v5}, Landroid/graphics/Paint;-><init>(I)V */
	 /* move-object v15, v10 */
	 /* .line 231 */
	 /* .local v15, "paint":Landroid/graphics/Paint; */
	 /* iget v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mTitleSizePx:F */
	 (( android.graphics.Paint ) v15 ).setTextSize ( v10 ); // invoke-virtual {v15, v10}, Landroid/graphics/Paint;->setTextSize(F)V
	 /* .line 232 */
	 v10 = android.graphics.Typeface.SANS_SERIF;
	 android.graphics.Typeface .create ( v10,v4 );
	 (( android.graphics.Paint ) v15 ).setTypeface ( v4 ); // invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
	 /* .line 233 */
	 /* const v4, -0x4c4c4d */
	 (( android.graphics.Paint ) v15 ).setColor ( v4 ); // invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setColor(I)V
	 /* .line 234 */
	 v4 = android.graphics.Paint$Align.CENTER;
	 (( android.graphics.Paint ) v15 ).setTextAlign ( v4 ); // invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V
	 /* .line 235 */
	 /* const/high16 v4, 0x3f800000 # 1.0f */
	 /* const/high16 v10, -0x1000000 */
	 /* const/high16 v11, 0x40000000 # 2.0f */
	 (( android.graphics.Paint ) v15 ).setShadowLayer ( v4, v11, v11, v10 ); // invoke-virtual {v15, v4, v11, v11, v10}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V
	 /* .line 236 */
	 v4 = this.mString1;
	 /* int-to-float v10, v8 */
	 /* int-to-float v11, v9 */
	 (( android.graphics.Canvas ) v7 ).drawText ( v4, v10, v11, v15 ); // invoke-virtual {v7, v4, v10, v11, v15}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
	 /* .line 238 */
	 /* iget v4, v1, Lcom/android/server/wm/TalkbackWatermark;->mTextSizePx:I */
	 /* int-to-float v4, v4 */
	 (( android.graphics.Paint ) v15 ).setTextSize ( v4 ); // invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setTextSize(F)V
	 /* .line 239 */
	 /* new-instance v4, Landroid/text/TextPaint; */
	 /* invoke-direct {v4, v15}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V */
	 /* move-object/from16 v20, v4 */
	 /* .line 241 */
	 /* .local v20, "textPaint":Landroid/text/TextPaint; */
	 java.util.Locale .getDefault ( );
	 v4 = 	 android.text.TextUtils .getLayoutDirectionFromLocale ( v4 );
	 /* .line 242 */
	 /* .local v4, "dir":I */
	 /* if-ne v4, v5, :cond_2 */
	 /* .line 243 */
	 int v10 = 1; // const/4 v10, 0x1
	 /* .line 244 */
	 /* .local v10, "line":I */
	 int v11 = 0; // const/4 v11, 0x0
	 /* move/from16 v27, v10 */
	 /* move v14, v11 */
} // .end local v10 # "line":I
/* .local v14, "i":I */
/* .local v27, "line":I */
} // :goto_1
v10 = this.mString2;
v10 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
/* if-ge v14, v10, :cond_1 */
/* .line 245 */
v10 = this.mString2;
v23 = (( java.lang.String ) v10 ).length ( ); // invoke-virtual {v10}, Ljava/lang/String;->length()I
/* const/16 v24, 0x1 */
v11 = (( android.graphics.Canvas ) v7 ).getWidth ( ); // invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I
/* iget v12, v1, Lcom/android/server/wm/TalkbackWatermark;->mPaddingPx:I */
/* sub-int/2addr v11, v12 */
/* int-to-float v11, v11 */
/* const/16 v26, 0x0 */
/* move-object/from16 v21, v10 */
/* move/from16 v22, v14 */
/* move/from16 v25, v11 */
v21 = /* invoke-virtual/range {v20 ..v26}, Landroid/text/TextPaint;->breakText(Ljava/lang/CharSequence;IIZF[F)I */
/* .line 246 */
/* .local v21, "len":I */
v10 = this.mString2;
/* add-int v11, v14, v21 */
(( java.lang.String ) v10 ).substring ( v14, v11 ); // invoke-virtual {v10, v14, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.String ) v10 ).toCharArray ( ); // invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C
int v12 = 0; // const/4 v12, 0x0
/* const/16 v16, 0x0 */
/* int-to-float v13, v8 */
/* iget v10, v1, Lcom/android/server/wm/TalkbackWatermark;->mDetPx:I */
/* mul-int v10, v10, v27 */
/* add-int/2addr v10, v9 */
/* int-to-float v10, v10 */
/* const/16 v18, 0x1 */
/* move/from16 v17, v10 */
/* move-object v10, v7 */
/* move/from16 v19, v13 */
/* move/from16 v13, v21 */
} // .end local v14 # "i":I
/* .local v22, "i":I */
/* move/from16 v14, v16 */
/* move-object/from16 v23, v15 */
} // .end local v15 # "paint":Landroid/graphics/Paint;
/* .local v23, "paint":Landroid/graphics/Paint; */
/* move/from16 v15, v21 */
/* move/from16 v16, v19 */
/* move-object/from16 v19, v20 */
/* invoke-virtual/range {v10 ..v19}, Landroid/graphics/Canvas;->drawTextRun([CIIIIFFZLandroid/graphics/Paint;)V */
/* .line 247 */
/* add-int v14, v22, v21 */
/* .line 248 */
} // .end local v22 # "i":I
/* .restart local v14 # "i":I */
/* nop */
} // .end local v21 # "len":I
/* add-int/lit8 v27, v27, 0x1 */
/* .line 249 */
/* move-object/from16 v15, v23 */
/* .line 244 */
} // .end local v23 # "paint":Landroid/graphics/Paint;
/* .restart local v15 # "paint":Landroid/graphics/Paint; */
} // :cond_1
/* move/from16 v22, v14 */
/* move-object/from16 v23, v15 */
/* .line 250 */
} // .end local v14 # "i":I
} // .end local v15 # "paint":Landroid/graphics/Paint;
} // .end local v27 # "line":I
/* .restart local v23 # "paint":Landroid/graphics/Paint; */
/* .line 251 */
} // .end local v23 # "paint":Landroid/graphics/Paint;
/* .restart local v15 # "paint":Landroid/graphics/Paint; */
} // :cond_2
/* move-object/from16 v23, v15 */
} // .end local v15 # "paint":Landroid/graphics/Paint;
/* .restart local v23 # "paint":Landroid/graphics/Paint; */
/* new-instance v10, Landroid/text/StaticLayout; */
v11 = this.mString2;
v12 = (( android.graphics.Canvas ) v7 ).getWidth ( ); // invoke-virtual {v7}, Landroid/graphics/Canvas;->getWidth()I
/* iget v13, v1, Lcom/android/server/wm/TalkbackWatermark;->mPaddingPx:I */
/* sub-int v31, v12, v13 */
v32 = android.text.Layout$Alignment.ALIGN_NORMAL;
/* const/high16 v33, 0x3f800000 # 1.0f */
/* const/16 v34, 0x0 */
/* const/16 v35, 0x0 */
/* move-object/from16 v28, v10 */
/* move-object/from16 v29, v11 */
/* move-object/from16 v30, v20 */
/* invoke-direct/range {v28 ..v35}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V */
/* .line 252 */
/* .local v10, "staticLayout":Landroid/text/StaticLayout; */
(( android.graphics.Canvas ) v7 ).save ( ); // invoke-virtual {v7}, Landroid/graphics/Canvas;->save()I
/* .line 253 */
/* int-to-float v11, v8 */
/* iget v12, v1, Lcom/android/server/wm/TalkbackWatermark;->mDetPx:I */
/* add-int/2addr v12, v9 */
/* int-to-float v12, v12 */
(( android.graphics.Canvas ) v7 ).translate ( v11, v12 ); // invoke-virtual {v7, v11, v12}, Landroid/graphics/Canvas;->translate(FF)V
/* .line 254 */
(( android.text.StaticLayout ) v10 ).draw ( v7 ); // invoke-virtual {v10, v7}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V
/* .line 255 */
(( android.graphics.Canvas ) v7 ).restore ( ); // invoke-virtual {v7}, Landroid/graphics/Canvas;->restore()V
/* .line 258 */
} // .end local v10 # "staticLayout":Landroid/text/StaticLayout;
} // :goto_2
v10 = this.mSurface;
(( android.view.Surface ) v10 ).unlockCanvasAndPost ( v7 ); // invoke-virtual {v10, v7}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
/* .line 259 */
/* iput-boolean v5, v1, Lcom/android/server/wm/TalkbackWatermark;->mHasDrawn:Z */
/* .line 219 */
} // .end local v0 # "offset":I
} // .end local v4 # "dir":I
} // .end local v8 # "x":I
} // .end local v9 # "y":I
} // .end local v20 # "textPaint":Landroid/text/TextPaint;
} // .end local v23 # "paint":Landroid/graphics/Paint;
} // :cond_3
} // :goto_3
return;
/* .line 261 */
} // .end local v2 # "dw":I
} // .end local v3 # "dh":I
} // .end local v6 # "dirty":Landroid/graphics/Rect;
} // .end local v7 # "c":Landroid/graphics/Canvas;
} // :cond_4
} // :goto_4
return;
} // .end method
private synchronized void hideInternal ( ) {
/* .locals 3 */
/* monitor-enter p0 */
/* .line 337 */
try { // :try_start_0
v0 = this.mSurfaceControl;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* if-nez v0, :cond_0 */
/* .line 338 */
/* monitor-exit p0 */
return;
/* .line 340 */
} // :cond_0
try { // :try_start_1
v0 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v0 ).openSurfaceTransaction ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 342 */
try { // :try_start_2
android.view.SurfaceControl .getGlobalTransaction ( );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 343 */
/* .local v0, "transaction":Landroid/view/SurfaceControl$Transaction; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 344 */
try { // :try_start_3
v1 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).hide ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 347 */
} // .end local v0 # "transaction":Landroid/view/SurfaceControl$Transaction;
/* :catchall_0 */
/* move-exception v0 */
} // :cond_1
} // :goto_0
try { // :try_start_4
v0 = this.mWms;
/* const-string/jumbo v1, "updateTalkbackWatermark" */
(( com.android.server.wm.WindowManagerService ) v0 ).closeSurfaceTransaction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* .line 348 */
/* nop */
/* .line 349 */
/* monitor-exit p0 */
return;
/* .line 347 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
/* :catchall_1 */
/* move-exception v0 */
} // :goto_1
try { // :try_start_5
v1 = this.mWms;
/* const-string/jumbo v2, "updateTalkbackWatermark" */
(( com.android.server.wm.WindowManagerService ) v1 ).closeSurfaceTransaction ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 348 */
/* throw v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 336 */
/* :catchall_2 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private void lambda$dismiss$2 ( ) { //synthethic
/* .locals 3 */
/* .line 284 */
try { // :try_start_0
v0 = this.mWms;
v0 = this.mContext;
v1 = this.mBroadcast;
(( android.content.Context ) v0 ).unregisterReceiver ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 287 */
/* .line 285 */
/* :catch_0 */
/* move-exception v0 */
/* .line 286 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
final String v1 = "TalkbackWatermark"; // const-string v1, "TalkbackWatermark"
final String v2 = "mBroadcast is not registered"; // const-string v2, "mBroadcast is not registered"
android.util.Slog .d ( v1,v2,v0 );
/* .line 288 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :goto_0
return;
} // .end method
private void lambda$show$1 ( ) { //synthethic
/* .locals 4 */
/* .line 267 */
v0 = this.mWms;
v0 = this.mContext;
v1 = this.mBroadcast;
/* new-instance v2, Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.LOCALE_CHANGED"; // const-string v3, "android.intent.action.LOCALE_CHANGED"
/* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v0 ).registerReceiver ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 268 */
return;
} // .end method
private void lambda$updateWaterMark$0 ( ) { //synthethic
/* .locals 5 */
/* .line 135 */
/* const-string/jumbo v0, "updateTalkbackWatermark" */
v1 = this.mWms;
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = -2; // const/4 v2, -0x2
/* const-string/jumbo v3, "talkback_watermark_enable" */
int v4 = 1; // const/4 v4, 0x1
v1 = android.provider.Settings$Secure .getIntForUser ( v1,v3,v4,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* move v1, v4 */
/* .line 137 */
/* .local v1, "enabled":Z */
v2 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v2 ).openSurfaceTransaction ( ); // invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
/* .line 138 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "talkback-test enabled =" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "TalkbackWatermark"; // const-string v4, "TalkbackWatermark"
android.util.Slog .d ( v4,v2 );
/* .line 140 */
try { // :try_start_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v2 );
/* .line 141 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 142 */
v2 = this.mWms;
/* invoke-direct {p0, v2}, Lcom/android/server/wm/TalkbackWatermark;->doCreateSurface(Lcom/android/server/wm/WindowManagerService;)V */
/* .line 143 */
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->showInternal()V */
/* .line 145 */
} // :cond_1
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->dismissInternal()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 148 */
} // :goto_1
v2 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v2 ).closeSurfaceTransaction ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 149 */
/* nop */
/* .line 150 */
return;
/* .line 148 */
/* :catchall_0 */
/* move-exception v2 */
v3 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v3 ).closeSurfaceTransaction ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 149 */
/* throw v2 */
} // .end method
private synchronized void refresh ( ) {
/* .locals 2 */
/* monitor-enter p0 */
/* .line 378 */
try { // :try_start_0
final String v0 = "TalkbackWatermark"; // const-string v0, "TalkbackWatermark"
/* const-string/jumbo v1, "talkback-test refresh" */
android.util.Slog .d ( v0,v1 );
/* .line 379 */
v0 = this.mWms;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 380 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z */
/* .line 381 */
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->updateWaterMark()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 382 */
/* monitor-exit p0 */
return;
/* .line 377 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private synchronized void setupBroadcast ( ) {
/* .locals 1 */
/* monitor-enter p0 */
/* .line 118 */
try { // :try_start_0
v0 = this.mBroadcast;
/* if-nez v0, :cond_0 */
/* .line 119 */
/* new-instance v0, Lcom/android/server/wm/TalkbackWatermark$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/TalkbackWatermark$2;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V */
this.mBroadcast = v0;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 131 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
} // :cond_0
/* monitor-exit p0 */
return;
/* .line 117 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private synchronized void showInternal ( ) {
/* .locals 6 */
/* monitor-enter p0 */
/* .line 309 */
try { // :try_start_0
v0 = this.mSurfaceControl;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* if-nez v0, :cond_0 */
/* .line 310 */
/* monitor-exit p0 */
return;
/* .line 312 */
} // :cond_0
try { // :try_start_1
v0 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v0 ).openSurfaceTransaction ( ); // invoke-virtual {v0}, Lcom/android/server/wm/WindowManagerService;->openSurfaceTransaction()V
/* .line 313 */
v0 = this.mWms;
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 314 */
/* const v1, 0x110f03b8 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
this.mString1 = v0;
/* .line 315 */
v0 = this.mWms;
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 316 */
/* const v1, 0x110f03b9 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
this.mString2 = v0;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 319 */
try { // :try_start_2
android.view.SurfaceControl .getGlobalTransaction ( );
/* .line 320 */
/* .local v0, "transaction":Landroid/view/SurfaceControl$Transaction; */
v1 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v1 ).getDefaultDisplayContentLocked ( ); // invoke-virtual {v1}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;
/* .line 321 */
/* .local v1, "defaultDc":Lcom/android/server/wm/DisplayContent; */
(( com.android.server.wm.DisplayContent ) v1 ).getDisplayInfo ( ); // invoke-virtual {v1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
/* .line 322 */
/* .local v2, "defaultInfo":Landroid/view/DisplayInfo; */
/* iget v3, v2, Landroid/view/DisplayInfo;->logicalWidth:I */
/* .line 323 */
/* .local v3, "defaultDw":I */
/* iget v4, v2, Landroid/view/DisplayInfo;->logicalHeight:I */
/* .line 324 */
/* .local v4, "defaultDh":I */
/* iget v5, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
if ( v5 != null) { // if-eqz v5, :cond_1
try { // :try_start_3
/* iget v5, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* if-nez v5, :cond_2 */
/* .line 332 */
} // .end local v0 # "transaction":Landroid/view/SurfaceControl$Transaction;
} // .end local v1 # "defaultDc":Lcom/android/server/wm/DisplayContent;
} // .end local v2 # "defaultInfo":Landroid/view/DisplayInfo;
} // .end local v3 # "defaultDw":I
} // .end local v4 # "defaultDh":I
/* :catchall_0 */
/* move-exception v0 */
/* .line 325 */
/* .restart local v0 # "transaction":Landroid/view/SurfaceControl$Transaction; */
/* .restart local v1 # "defaultDc":Lcom/android/server/wm/DisplayContent; */
/* .restart local v2 # "defaultInfo":Landroid/view/DisplayInfo; */
/* .restart local v3 # "defaultDw":I */
/* .restart local v4 # "defaultDh":I */
} // :cond_1
} // :goto_0
try { // :try_start_4
(( com.android.server.wm.TalkbackWatermark ) p0 ).positionSurface ( v3, v4 ); // invoke-virtual {p0, v3, v4}, Lcom/android/server/wm/TalkbackWatermark;->positionSurface(II)V
/* .line 327 */
} // :cond_2
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->drawIfNeeded()V */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 328 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 329 */
try { // :try_start_5
v5 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).show ( v5 ); // invoke-virtual {v0, v5}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
/* .line 332 */
} // .end local v0 # "transaction":Landroid/view/SurfaceControl$Transaction;
} // .end local v1 # "defaultDc":Lcom/android/server/wm/DisplayContent;
} // .end local v2 # "defaultInfo":Landroid/view/DisplayInfo;
} // .end local v3 # "defaultDw":I
} // .end local v4 # "defaultDh":I
} // :cond_3
try { // :try_start_6
v0 = this.mWms;
/* const-string/jumbo v1, "updateTalkbackWatermark" */
(( com.android.server.wm.WindowManagerService ) v0 ).closeSurfaceTransaction ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
/* .line 333 */
/* nop */
/* .line 334 */
/* monitor-exit p0 */
return;
/* .line 332 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
/* :catchall_1 */
/* move-exception v0 */
} // :goto_1
try { // :try_start_7
v1 = this.mWms;
/* const-string/jumbo v2, "updateTalkbackWatermark" */
(( com.android.server.wm.WindowManagerService ) v1 ).closeSurfaceTransaction ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/wm/WindowManagerService;->closeSurfaceTransaction(Ljava/lang/String;)V
/* .line 333 */
/* throw v0 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_2 */
/* .line 308 */
/* :catchall_2 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
private void updateWaterMark ( ) {
/* .locals 2 */
/* .line 134 */
v0 = this.mWms;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda3;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 151 */
return;
} // .end method
/* # virtual methods */
public synchronized void dismiss ( ) {
/* .locals 2 */
/* monitor-enter p0 */
/* .line 282 */
try { // :try_start_0
v0 = this.mWms;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* .line 289 */
v0 = this.mSurfaceControl;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v0, :cond_0 */
/* .line 290 */
/* monitor-exit p0 */
return;
/* .line 292 */
} // :cond_0
try { // :try_start_1
final String v0 = "TalkbackWatermark"; // const-string v0, "TalkbackWatermark"
/* const-string/jumbo v1, "talkback-test dismiss" */
android.util.Slog .d ( v0,v1 );
/* .line 293 */
v0 = this.mWms;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda2;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 294 */
/* monitor-exit p0 */
return;
/* .line 281 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public void init ( com.android.server.wm.WindowManagerService p0 ) {
/* .locals 5 */
/* .param p1, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .line 101 */
/* nop */
/* .line 102 */
/* const-string/jumbo v0, "talkback_watermark_enable" */
android.provider.Settings$Secure .getUriFor ( v0 );
/* .line 103 */
/* .local v0, "talkbackWatermarkEnableUri":Landroid/net/Uri; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v2, Lcom/android/server/wm/TalkbackWatermark$1; */
/* new-instance v3, Landroid/os/Handler; */
/* .line 105 */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* invoke-direct {v2, p0, v3, v0}, Lcom/android/server/wm/TalkbackWatermark$1;-><init>(Lcom/android/server/wm/TalkbackWatermark;Landroid/os/Handler;Landroid/net/Uri;)V */
/* .line 103 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v0, v3, v2, v4 ); // invoke-virtual {v1, v0, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 113 */
this.mWms = p1;
/* .line 114 */
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->setupBroadcast()V */
/* .line 115 */
return;
} // .end method
public synchronized void positionSurface ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "dw" # I */
/* .param p2, "dh" # I */
/* monitor-enter p0 */
/* .line 189 */
try { // :try_start_0
v0 = this.mSurfaceControl;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* if-nez v0, :cond_0 */
/* .line 190 */
/* monitor-exit p0 */
return;
/* .line 192 */
} // :cond_0
try { // :try_start_1
/* iget v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I */
/* if-ne v0, p1, :cond_1 */
/* iget v0, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I */
/* if-eq v0, p2, :cond_3 */
/* .line 193 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
} // :cond_1
/* iput p1, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDW:I */
/* .line 194 */
/* iput p2, p0, Lcom/android/server/wm/TalkbackWatermark;->mLastDH:I */
/* .line 195 */
android.view.SurfaceControl .getGlobalTransaction ( );
/* .line 196 */
/* .local v0, "transaction":Landroid/view/SurfaceControl$Transaction; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 197 */
v1 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).setBufferSize ( v1, p1, p2 ); // invoke-virtual {v0, v1, p1, p2}, Landroid/view/SurfaceControl$Transaction;->setBufferSize(Landroid/view/SurfaceControl;II)Landroid/view/SurfaceControl$Transaction;
/* .line 199 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/android/server/wm/TalkbackWatermark;->mDrawNeeded:Z */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 201 */
} // .end local v0 # "transaction":Landroid/view/SurfaceControl$Transaction;
} // :cond_3
/* monitor-exit p0 */
return;
/* .line 188 */
} // .end local p1 # "dw":I
} // .end local p2 # "dh":I
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void setVisible ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "visible" # Z */
/* monitor-enter p0 */
/* .line 273 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 274 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->showInternal()V */
/* .line 276 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
} // :cond_0
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->hideInternal()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 278 */
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 272 */
} // .end local p1 # "visible":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized void show ( ) {
/* .locals 2 */
/* monitor-enter p0 */
/* .line 265 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/wm/TalkbackWatermark;->updateWaterMark()V */
/* .line 266 */
v0 = this.mWms;
v0 = this.mH;
/* new-instance v1, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/wm/TalkbackWatermark$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/TalkbackWatermark;)V */
(( com.android.server.wm.WindowManagerService$H ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/wm/WindowManagerService$H;->post(Ljava/lang/Runnable;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 269 */
/* monitor-exit p0 */
return;
/* .line 264 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public synchronized void updateTalkbackMode ( Boolean p0, android.content.ComponentName p1 ) {
/* .locals 2 */
/* .param p1, "enable" # Z */
/* .param p2, "mComponentName" # Landroid/content/ComponentName; */
/* monitor-enter p0 */
/* .line 298 */
try { // :try_start_0
(( android.content.ComponentName ) p2 ).flattenToShortString ( ); // invoke-virtual {p2}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;
final String v1 = "TalkBackService"; // const-string v1, "TalkBackService"
v0 = (( java.lang.String ) v0 ).contains ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 299 */
(( android.content.ComponentName ) p2 ).getPackageName ( ); // invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
final String v1 = "com.google.android.marvin.talkback"; // const-string v1, "com.google.android.marvin.talkback"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 300 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 301 */
(( com.android.server.wm.TalkbackWatermark ) p0 ).show ( ); // invoke-virtual {p0}, Lcom/android/server/wm/TalkbackWatermark;->show()V
/* .line 303 */
} // .end local p0 # "this":Lcom/android/server/wm/TalkbackWatermark;
} // :cond_0
(( com.android.server.wm.TalkbackWatermark ) p0 ).dismiss ( ); // invoke-virtual {p0}, Lcom/android/server/wm/TalkbackWatermark;->dismiss()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 306 */
} // :cond_1
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 297 */
} // .end local p1 # "enable":Z
} // .end local p2 # "mComponentName":Landroid/content/ComponentName;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
