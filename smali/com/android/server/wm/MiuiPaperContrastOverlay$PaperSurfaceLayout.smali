.class final Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;
.super Ljava/lang/Object;
.source "MiuiPaperContrastOverlay.java"

# interfaces
.implements Landroid/hardware/display/DisplayManagerInternal$DisplayTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiPaperContrastOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PaperSurfaceLayout"
.end annotation


# instance fields
.field private final mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field private mSurfaceControl:Landroid/view/SurfaceControl;

.field final synthetic this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MiuiPaperContrastOverlay;Landroid/hardware/display/DisplayManagerInternal;Landroid/view/SurfaceControl;)V
    .locals 0
    .param p2, "displayManagerInternal"    # Landroid/hardware/display/DisplayManagerInternal;
    .param p3, "surfaceControl"    # Landroid/view/SurfaceControl;

    .line 908
    iput-object p1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->this$0:Lcom/android/server/wm/MiuiPaperContrastOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 909
    iput-object p2, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 910
    iput-object p3, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 911
    invoke-virtual {p2, p0}, Landroid/hardware/display/DisplayManagerInternal;->registerDisplayTransactionListener(Landroid/hardware/display/DisplayManagerInternal$DisplayTransactionListener;)V

    .line 912
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 915
    monitor-enter p0

    .line 916
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    .line 917
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 918
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    invoke-virtual {v0, p0}, Landroid/hardware/display/DisplayManagerInternal;->unregisterDisplayTransactionListener(Landroid/hardware/display/DisplayManagerInternal$DisplayTransactionListener;)V

    .line 919
    return-void

    .line 917
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onDisplayTransaction(Landroid/view/SurfaceControl$Transaction;)V
    .locals 10
    .param p1, "t"    # Landroid/view/SurfaceControl$Transaction;

    .line 923
    monitor-enter p0

    .line 924
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    if-nez v0, :cond_0

    .line 925
    monitor-exit p0

    return-void

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManagerInternal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v0

    .line 929
    .local v0, "displayInfo":Landroid/view/DisplayInfo;
    if-nez v0, :cond_1

    .line 932
    monitor-exit p0

    return-void

    .line 934
    :cond_1
    iget v1, v0, Landroid/view/DisplayInfo;->rotation:I

    const/4 v2, 0x0

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 949
    :pswitch_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget v3, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v3, v2}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 950
    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, -0x40800000    # -1.0f

    const/4 v9, 0x0

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    goto :goto_0

    .line 944
    :pswitch_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget v2, v0, Landroid/view/DisplayInfo;->logicalWidth:I

    int-to-float v2, v2

    iget v3, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 946
    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v9, -0x40800000    # -1.0f

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    .line 947
    goto :goto_0

    .line 940
    :pswitch_2
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    iget v3, v0, Landroid/view/DisplayInfo;->logicalHeight:I

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 941
    iget-object v5, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/4 v6, 0x0

    const/high16 v7, -0x40800000    # -1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    .line 942
    goto :goto_0

    .line 936
    :pswitch_3
    iget-object v1, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    invoke-virtual {p1, v1, v2, v2}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;

    .line 937
    iget-object v4, p0, Lcom/android/server/wm/MiuiPaperContrastOverlay$PaperSurfaceLayout;->mSurfaceControl:Landroid/view/SurfaceControl;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;FFFF)Landroid/view/SurfaceControl$Transaction;

    .line 938
    nop

    .line 953
    .end local v0    # "displayInfo":Landroid/view/DisplayInfo;
    :goto_0
    monitor-exit p0

    .line 954
    return-void

    .line 953
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
