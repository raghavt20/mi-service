.class Lcom/android/server/wm/MiuiFreeformTrackManager$11;
.super Ljava/lang/Object;
.source "MiuiFreeformTrackManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSmallWindowPinedEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

.field final synthetic val$applicationName:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$pinedWay:Ljava/lang/String;

.field final synthetic val$posX:I

.field final synthetic val$posY:I


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreeformTrackManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiFreeformTrackManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 455
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iput-object p2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$applicationName:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$pinedWay:Ljava/lang/String;

    iput p5, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$posX:I

    iput p6, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$posY:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 457
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeformTrackManager;->mFreeFormTrackLock:Ljava/lang/Object;

    monitor-enter v0

    .line 459
    :try_start_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 460
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 461
    .local v1, "jsonData":Lorg/json/JSONObject;
    const-string/jumbo v2, "tip"

    const-string v3, "621.1.2.1.21747"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 462
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2, v1}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$mputCommomParam(Lcom/android/server/wm/MiuiFreeformTrackManager;Lorg/json/JSONObject;)V

    .line 463
    const-string v2, "EVENT_NAME"

    const-string v3, "hide_window"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 464
    const-string v2, "app_package_name"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 465
    const-string v2, "app_display_name"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$applicationName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 466
    const-string/jumbo v2, "small_window_hide_way"

    iget-object v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$pinedWay:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 467
    const-string v2, "hide_window_x_coordinate"

    iget v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$posX:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 468
    const-string v2, "hide_window_y_coordinate"

    iget v3, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->val$posY:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 469
    iget-object v2, p0, Lcom/android/server/wm/MiuiFreeformTrackManager$11;->this$0:Lcom/android/server/wm/MiuiFreeformTrackManager;

    invoke-static {v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->-$$Nest$fgetmITrackBinder(Lcom/android/server/wm/MiuiFreeformTrackManager;)Lcom/miui/analytics/ITrackBinder;

    move-result-object v2

    const-string v3, "31000000538"

    const-string v4, "android"

    .line 470
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    .line 469
    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/miui/analytics/ITrackBinder;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    .end local v1    # "jsonData":Lorg/json/JSONObject;
    :cond_0
    goto :goto_0

    .line 475
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 472
    :catch_0
    move-exception v1

    .line 473
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 475
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    monitor-exit v0

    .line 476
    return-void

    .line 475
    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
