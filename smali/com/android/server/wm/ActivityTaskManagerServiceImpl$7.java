class com.android.server.wm.ActivityTaskManagerServiceImpl$7 implements android.animation.Animator$AnimatorListener {
	 /* .source "ActivityTaskManagerServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->animationOut()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.ActivityTaskManagerServiceImpl this$0; //synthetic
final android.view.SurfaceControl$Transaction val$t; //synthetic
/* # direct methods */
 com.android.server.wm.ActivityTaskManagerServiceImpl$7 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/ActivityTaskManagerServiceImpl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1436 */
this.this$0 = p1;
this.val$t = p2;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onAnimationCancel ( android.animation.Animator p0 ) {
/* .locals 2 */
/* .param p1, "animator" # Landroid/animation/Animator; */
/* .line 1451 */
v0 = this.this$0;
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fgetmScreenshotLayer ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1452 */
v0 = this.val$t;
v1 = this.this$0;
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fgetmScreenshotLayer ( v1 );
(( android.view.SurfaceControl$Transaction ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
(( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 1453 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fputmScreenshotLayer ( v0,v1 );
/* .line 1455 */
} // :cond_0
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fputmInAnimationOut ( v0,v1 );
/* .line 1456 */
return;
} // .end method
public void onAnimationEnd ( android.animation.Animator p0 ) {
/* .locals 2 */
/* .param p1, "animator" # Landroid/animation/Animator; */
/* .line 1442 */
v0 = this.this$0;
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fgetmScreenshotLayer ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1443 */
v0 = this.val$t;
v1 = this.this$0;
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fgetmScreenshotLayer ( v1 );
(( android.view.SurfaceControl$Transaction ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->remove(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
(( android.view.SurfaceControl$Transaction ) v0 ).apply ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl$Transaction;->apply()V
/* .line 1444 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fputmScreenshotLayer ( v0,v1 );
/* .line 1446 */
} // :cond_0
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.android.server.wm.ActivityTaskManagerServiceImpl .-$$Nest$fputmInAnimationOut ( v0,v1 );
/* .line 1447 */
return;
} // .end method
public void onAnimationRepeat ( android.animation.Animator p0 ) {
/* .locals 0 */
/* .param p1, "animator" # Landroid/animation/Animator; */
/* .line 1459 */
return;
} // .end method
public void onAnimationStart ( android.animation.Animator p0 ) {
/* .locals 0 */
/* .param p1, "animator" # Landroid/animation/Animator; */
/* .line 1438 */
return;
} // .end method
