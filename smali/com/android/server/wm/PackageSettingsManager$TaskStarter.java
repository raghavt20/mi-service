class com.android.server.wm.PackageSettingsManager$TaskStarter {
	 /* .source "PackageSettingsManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/PackageSettingsManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "TaskStarter" */
} // .end annotation
/* # instance fields */
final java.lang.String callingFeatureId;
final java.lang.String callingPackage;
final Integer callingUid;
final android.content.Intent intent;
final com.android.server.wm.SafeActivityOptions mOptions;
final java.lang.String mReason;
final Integer realCallingPid;
final Integer realCallingUid;
final com.android.server.wm.PackageSettingsManager this$0; //synthetic
final Integer userId;
/* # direct methods */
static void -$$Nest$mrestartTask ( com.android.server.wm.PackageSettingsManager$TaskStarter p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->restartTask()V */
return;
} // .end method
private com.android.server.wm.PackageSettingsManager$TaskStarter ( ) {
/* .locals 2 */
/* .param p2, "task" # Lcom/android/server/wm/Task; */
/* .param p3, "activityOptions" # Landroid/app/ActivityOptions; */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 312 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 313 */
/* iget v0, p2, Lcom/android/server/wm/Task;->mCallingUid:I */
/* iput v0, p0, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->callingUid:I */
/* .line 314 */
v0 = android.os.Binder .getCallingPid ( );
/* iput v0, p0, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->realCallingPid:I */
/* .line 315 */
v0 = android.os.Binder .getCallingUid ( );
/* iput v0, p0, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->realCallingUid:I */
/* .line 316 */
v0 = this.mCallingPackage;
this.callingPackage = v0;
/* .line 317 */
v0 = this.mCallingFeatureId;
this.callingFeatureId = v0;
/* .line 318 */
v0 = this.intent;
this.intent = v0;
/* .line 320 */
int v0 = 1; // const/4 v0, 0x1
if ( p3 != null) { // if-eqz p3, :cond_1
	 /* .line 321 */
	 p1 = 	 (( android.app.ActivityOptions ) p3 ).getLaunchWindowingMode ( ); // invoke-virtual {p3}, Landroid/app/ActivityOptions;->getLaunchWindowingMode()I
	 /* if-eq p1, v0, :cond_0 */
	 /* .line 322 */
	 (( android.app.ActivityOptions ) p3 ).setLaunchWindowingMode ( v0 ); // invoke-virtual {p3, v0}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
	 /* .line 324 */
} // :cond_0
/* new-instance p1, Lcom/android/server/wm/SafeActivityOptions; */
/* invoke-direct {p1, p3}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V */
this.mOptions = p1;
/* .line 326 */
} // :cond_1
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmFullScreenLaunchOption ( p1 );
/* if-nez v1, :cond_2 */
/* .line 327 */
android.app.ActivityOptions .makeBasic ( );
/* .line 328 */
/* .local v1, "option":Landroid/app/ActivityOptions; */
(( android.app.ActivityOptions ) v1 ).setLaunchWindowingMode ( v0 ); // invoke-virtual {v1, v0}, Landroid/app/ActivityOptions;->setLaunchWindowingMode(I)V
/* .line 329 */
/* new-instance v0, Lcom/android/server/wm/SafeActivityOptions; */
/* invoke-direct {v0, v1}, Lcom/android/server/wm/SafeActivityOptions;-><init>(Landroid/app/ActivityOptions;)V */
com.android.server.wm.PackageSettingsManager .-$$Nest$fputmFullScreenLaunchOption ( p1,v0 );
/* .line 331 */
} // .end local v1 # "option":Landroid/app/ActivityOptions;
} // :cond_2
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmFullScreenLaunchOption ( p1 );
this.mOptions = p1;
/* .line 333 */
} // :goto_0
/* iget p1, p2, Lcom/android/server/wm/Task;->mUserId:I */
/* iput p1, p0, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->userId:I */
/* .line 334 */
this.mReason = p4;
/* .line 335 */
return;
} // .end method
 com.android.server.wm.PackageSettingsManager$TaskStarter ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;-><init>(Lcom/android/server/wm/PackageSettingsManager;Lcom/android/server/wm/Task;Landroid/app/ActivityOptions;Ljava/lang/String;)V */
return;
} // .end method
private void restartTask ( ) {
/* .locals 22 */
/* .line 338 */
/* move-object/from16 v1, p0 */
v0 = this.this$0;
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmAtmService ( v0 );
v2 = this.mGlobalLock;
/* monitor-enter v2 */
/* .line 339 */
try { // :try_start_0
com.android.server.wm.WindowManagerService .boostPriorityForLockedSection ( );
/* .line 346 */
v0 = this.this$0;
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmAtmService ( v0 );
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getActivityStartController ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->getActivityStartController()Lcom/android/server/wm/ActivityStartController;
/* iget v4, v1, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->callingUid:I */
/* iget v5, v1, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->realCallingPid:I */
/* iget v6, v1, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->realCallingUid:I */
v7 = this.callingPackage;
v8 = this.callingFeatureId;
v9 = this.intent;
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
v15 = this.mOptions;
/* iget v0, v1, Lcom/android/server/wm/PackageSettingsManager$TaskStarter;->userId:I */
/* const/16 v17, 0x0 */
v14 = this.mReason;
/* const/16 v19, 0x0 */
/* const/16 v20, 0x0 */
v21 = android.app.BackgroundStartPrivileges.NONE;
/* move-object/from16 v18, v14 */
int v14 = 0; // const/4 v14, 0x0
/* move/from16 v16, v0 */
/* invoke-virtual/range {v3 ..v21}, Lcom/android/server/wm/ActivityStartController;->startActivityInPackage(IIILjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILcom/android/server/wm/SafeActivityOptions;ILcom/android/server/wm/Task;Ljava/lang/String;ZLcom/android/server/am/PendingIntentRecord;Landroid/app/BackgroundStartPrivileges;)I */
/* .line 349 */
com.android.server.wm.WindowManagerService .resetPriorityAfterLockedSection ( );
/* .line 350 */
/* monitor-exit v2 */
/* .line 351 */
return;
/* .line 350 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v0 */
} // .end method
