public class com.android.server.wm.MiuiDisplayReportImpl implements com.android.server.wm.MiuiDisplayReportStub {
	 /* .source "MiuiDisplayReportImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static final android.util.ArraySet WHITE_LIST_FOR_TRACING;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/util/ArraySet<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.app.ActivityManagerInternal mAmInternal;
private android.os.ShellCommand mShellCmd;
private com.android.server.wm.WindowManagerService mWmInternal;
/* # direct methods */
public static void $r8$lambda$iHxXkgqpVSSYc7hnUtkP-iwSsKY ( com.android.server.wm.MiuiDisplayReportImpl p0, android.view.IWindow p1, java.lang.String p2, java.lang.String p3, android.os.ParcelFileDescriptor p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiDisplayReportImpl;->lambda$dumpLocalWindowAsync$1(Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V */
return;
} // .end method
public static void $r8$lambda$m7i2XhbOeX96SqfxF5uHHAvjAdk ( com.android.server.wm.MiuiDisplayReportImpl p0, java.lang.String p1, java.lang.String p2, com.android.internal.os.ByteTransferPipe[] p3, com.android.server.wm.WindowState p4 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/server/wm/MiuiDisplayReportImpl;->lambda$dumpViewCaptures$0(Ljava/lang/String;Ljava/lang/String;[Lcom/android/internal/os/ByteTransferPipe;Lcom/android/server/wm/WindowState;)V */
return;
} // .end method
static com.android.server.wm.MiuiDisplayReportImpl ( ) {
/* .locals 2 */
/* .line 25 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 28 */
final String v1 = "com.miui.uireporter"; // const-string v1, "com.miui.uireporter"
(( android.util.ArraySet ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
/* .line 29 */
return;
} // .end method
public com.android.server.wm.MiuiDisplayReportImpl ( ) {
/* .locals 0 */
/* .line 22 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void dumpLocalWindowAsync ( android.view.IWindow p0, java.lang.String p1, java.lang.String p2, android.os.ParcelFileDescriptor p3 ) {
/* .locals 8 */
/* .param p1, "client" # Landroid/view/IWindow; */
/* .param p2, "cmd" # Ljava/lang/String; */
/* .param p3, "params" # Ljava/lang/String; */
/* .param p4, "pfd" # Landroid/os/ParcelFileDescriptor; */
/* .line 110 */
com.android.server.IoThread .getExecutor ( );
/* new-instance v7, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda0; */
/* move-object v1, v7 */
/* move-object v2, p0 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/MiuiDisplayReportImpl;Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V */
/* .line 121 */
return;
} // .end method
private void lambda$dumpLocalWindowAsync$1 ( android.view.IWindow p0, java.lang.String p1, java.lang.String p2, android.os.ParcelFileDescriptor p3 ) { //synthethic
/* .locals 2 */
/* .param p1, "client" # Landroid/view/IWindow; */
/* .param p2, "cmd" # Ljava/lang/String; */
/* .param p3, "params" # Ljava/lang/String; */
/* .param p4, "pfd" # Landroid/os/ParcelFileDescriptor; */
/* .line 111 */
v0 = this.mWmInternal;
v0 = this.mGlobalLock;
/* monitor-enter v0 */
/* .line 113 */
try { // :try_start_0
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 118 */
	 /* .line 119 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* .line 114 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 117 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 try { // :try_start_1
		 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
		 /* .line 119 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
/* monitor-exit v0 */
/* .line 120 */
return;
/* .line 119 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void lambda$dumpViewCaptures$0 ( java.lang.String p0, java.lang.String p1, com.android.internal.os.ByteTransferPipe[] p2, com.android.server.wm.WindowState p3 ) { //synthethic
/* .locals 4 */
/* .param p1, "windowName" # Ljava/lang/String; */
/* .param p2, "viewName" # Ljava/lang/String; */
/* .param p3, "pip" # [Lcom/android/internal/os/ByteTransferPipe; */
/* .param p4, "w" # Lcom/android/server/wm/WindowState; */
/* .line 78 */
v0 = (( com.android.server.wm.WindowState ) p4 ).isVisible ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowState;->isVisible()Z
if ( v0 != null) { // if-eqz v0, :cond_1
(( com.android.server.wm.WindowState ) p4 ).getName ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowState;->getName()Ljava/lang/String;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 79 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 81 */
	 /* .local v0, "pipe":Lcom/android/internal/os/ByteTransferPipe; */
	 try { // :try_start_0
		 /* new-instance v1, Lcom/android/internal/os/ByteTransferPipe; */
		 /* invoke-direct {v1}, Lcom/android/internal/os/ByteTransferPipe;-><init>()V */
		 /* move-object v0, v1 */
		 /* .line 82 */
		 (( com.android.internal.os.ByteTransferPipe ) v0 ).getWriteFd ( ); // invoke-virtual {v0}, Lcom/android/internal/os/ByteTransferPipe;->getWriteFd()Landroid/os/ParcelFileDescriptor;
		 /* .line 83 */
		 /* .local v1, "pfd":Landroid/os/ParcelFileDescriptor; */
		 v2 = 		 (( com.android.server.wm.WindowState ) p4 ).isClientLocal ( ); // invoke-virtual {p4}, Lcom/android/server/wm/WindowState;->isClientLocal()Z
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 final String v3 = "CAPTURE"; // const-string v3, "CAPTURE"
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* .line 84 */
			 try { // :try_start_1
				 v2 = this.mClient;
				 /* invoke-direct {p0, v2, v3, p2, v1}, Lcom/android/server/wm/MiuiDisplayReportImpl;->dumpLocalWindowAsync(Landroid/view/IWindow;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)V */
				 /* .line 86 */
			 } // :cond_0
			 v2 = this.mClient;
			 /* .line 88 */
		 } // :goto_0
		 int v2 = 0; // const/4 v2, 0x0
		 /* aput-object v0, p3, v2 */
		 /* :try_end_1 */
		 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
		 /* .line 93 */
	 } // .end local v1 # "pfd":Landroid/os/ParcelFileDescriptor;
	 /* .line 89 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 90 */
	 /* .local v1, "e":Ljava/lang/Exception; */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 91 */
		 (( com.android.internal.os.ByteTransferPipe ) v0 ).kill ( ); // invoke-virtual {v0}, Lcom/android/internal/os/ByteTransferPipe;->kill()V
		 /* .line 95 */
	 } // .end local v0 # "pipe":Lcom/android/internal/os/ByteTransferPipe;
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_1
return;
} // .end method
/* # virtual methods */
public Integer dumpViewCaptures ( ) {
/* .locals 8 */
/* .line 59 */
v0 = this.mWmInternal;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_4
v2 = this.mShellCmd;
/* if-nez v2, :cond_0 */
/* .line 62 */
} // :cond_0
final String v2 = "android.permission.DUMP"; // const-string v2, "android.permission.DUMP"
final String v3 = "runDumpViewCapture()"; // const-string v3, "runDumpViewCapture()"
v0 = (( com.android.server.wm.WindowManagerService ) v0 ).checkCallingPermission ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/WindowManagerService;->checkCallingPermission(Ljava/lang/String;Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 66 */
v0 = this.mShellCmd;
(( android.os.ShellCommand ) v0 ).getNextArgRequired ( ); // invoke-virtual {v0}, Landroid/os/ShellCommand;->getNextArgRequired()Ljava/lang/String;
/* .line 67 */
/* .local v0, "windowName":Ljava/lang/String; */
v2 = this.mShellCmd;
(( android.os.ShellCommand ) v2 ).getNextArgRequired ( ); // invoke-virtual {v2}, Landroid/os/ShellCommand;->getNextArgRequired()Ljava/lang/String;
/* .line 68 */
/* .local v2, "viewName":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v3, :cond_2 */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 72 */
} // :cond_1
int v3 = 1; // const/4 v3, 0x1
/* new-array v3, v3, [Lcom/android/internal/os/ByteTransferPipe; */
/* .line 74 */
/* .local v3, "pip":[Lcom/android/internal/os/ByteTransferPipe; */
v4 = this.mShellCmd;
(( android.os.ShellCommand ) v4 ).getRawOutputStream ( ); // invoke-virtual {v4}, Landroid/os/ShellCommand;->getRawOutputStream()Ljava/io/OutputStream;
/* check-cast v4, Ljava/io/FileOutputStream; */
/* .line 75 */
/* .local v4, "out":Ljava/io/FileOutputStream; */
v5 = this.mWmInternal;
v5 = this.mGlobalLock;
/* monitor-enter v5 */
/* .line 77 */
try { // :try_start_0
v6 = this.mWmInternal;
v6 = this.mRoot;
/* new-instance v7, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v7, p0, v0, v2, v3}, Lcom/android/server/wm/MiuiDisplayReportImpl$$ExternalSyntheticLambda1;-><init>(Lcom/android/server/wm/MiuiDisplayReportImpl;Ljava/lang/String;Ljava/lang/String;[Lcom/android/internal/os/ByteTransferPipe;)V */
(( com.android.server.wm.RootWindowContainer ) v6 ).forAllWindows ( v7, v1 ); // invoke-virtual {v6, v7, v1}, Lcom/android/server/wm/RootWindowContainer;->forAllWindows(Ljava/util/function/Consumer;Z)V
/* .line 96 */
/* monitor-exit v5 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 99 */
try { // :try_start_1
	 /* aget-object v5, v3, v1 */
	 (( com.android.internal.os.ByteTransferPipe ) v5 ).get ( ); // invoke-virtual {v5}, Lcom/android/internal/os/ByteTransferPipe;->get()[B
	 /* .line 100 */
	 /* .local v5, "data":[B */
	 (( java.io.FileOutputStream ) v4 ).write ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V
	 /* :try_end_1 */
	 /* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
	 /* .line 103 */
} // .end local v5 # "data":[B
/* .line 101 */
/* :catch_0 */
/* move-exception v5 */
/* .line 102 */
/* .local v5, "e":Ljava/io/IOException; */
(( java.io.IOException ) v5 ).printStackTrace ( ); // invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
/* .line 104 */
} // .end local v5 # "e":Ljava/io/IOException;
} // :goto_0
/* .line 96 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v5 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
/* .line 69 */
} // .end local v3 # "pip":[Lcom/android/internal/os/ByteTransferPipe;
} // .end local v4 # "out":Ljava/io/FileOutputStream;
} // :cond_2
} // :goto_1
/* .line 64 */
} // .end local v0 # "windowName":Ljava/lang/String;
} // .end local v2 # "viewName":Ljava/lang/String;
} // :cond_3
/* new-instance v0, Ljava/lang/SecurityException; */
final String v1 = "Requires DUMP permission"; // const-string v1, "Requires DUMP permission"
/* invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 60 */
} // :cond_4
} // :goto_2
} // .end method
public void init ( android.os.ShellCommand p0, com.android.server.wm.WindowManagerService p1 ) {
/* .locals 0 */
/* .param p1, "shellcmd" # Landroid/os/ShellCommand; */
/* .param p2, "wms" # Lcom/android/server/wm/WindowManagerService; */
/* .line 37 */
this.mShellCmd = p1;
/* .line 38 */
this.mWmInternal = p2;
/* .line 39 */
return;
} // .end method
public Boolean traceEnableForCaller ( ) {
/* .locals 4 */
/* .line 43 */
v0 = this.mAmInternal;
/* if-nez v0, :cond_0 */
/* .line 44 */
/* const-class v0, Landroid/app/ActivityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/app/ActivityManagerInternal; */
this.mAmInternal = v0;
/* .line 46 */
} // :cond_0
v0 = this.mAmInternal;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 47 */
v0 = android.os.Binder .getCallingPid ( );
/* .line 48 */
/* .local v0, "callingPid":I */
v2 = this.mAmInternal;
(( android.app.ActivityManagerInternal ) v2 ).getPackageNameByPid ( v0 ); // invoke-virtual {v2, v0}, Landroid/app/ActivityManagerInternal;->getPackageNameByPid(I)Ljava/lang/String;
/* .line 49 */
/* .local v2, "packageName":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 50 */
/* .line 52 */
} // :cond_1
v1 = com.android.server.wm.MiuiDisplayReportImpl.WHITE_LIST_FOR_TRACING;
v1 = (( android.util.ArraySet ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z
/* .line 54 */
} // .end local v0 # "callingPid":I
} // .end local v2 # "packageName":Ljava/lang/String;
} // :cond_2
} // .end method
