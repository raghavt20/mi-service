.class public Lcom/android/server/wm/WindowContainerImpl;
.super Ljava/lang/Object;
.source "WindowContainerImpl.java"

# interfaces
.implements Lcom/android/server/wm/WindowContainerStub;


# instance fields
.field private mSplitDimmer:Landroid/view/animation/Animation;

.field private wc:Lcom/android/server/wm/WindowContainer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clearTransitionDimmer()V
    .locals 3

    .line 24
    iget-object v0, p0, Lcom/android/server/wm/WindowContainerImpl;->mSplitDimmer:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    .line 25
    return-void

    .line 27
    :cond_0
    invoke-static {}, Lcom/android/server/wm/AppTransitionStub;->get()Lcom/android/server/wm/AppTransitionStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/WindowContainerImpl;->mSplitDimmer:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/android/server/wm/WindowContainerImpl;->wc:Lcom/android/server/wm/WindowContainer;

    invoke-interface {v0, v1, v2}, Lcom/android/server/wm/AppTransitionStub;->stopSplitDimmer(Landroid/view/animation/Animation;Lcom/android/server/wm/WindowContainer;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/WindowContainerImpl;->mSplitDimmer:Landroid/view/animation/Animation;

    .line 29
    return-void
.end method

.method public getSplitDimmer()Landroid/view/animation/Animation;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/android/server/wm/WindowContainerImpl;->mSplitDimmer:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public init(Lcom/android/server/wm/WindowContainer;)V
    .locals 1
    .param p1, "container"    # Lcom/android/server/wm/WindowContainer;

    .line 18
    iput-object p1, p0, Lcom/android/server/wm/WindowContainerImpl;->wc:Lcom/android/server/wm/WindowContainer;

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/wm/WindowContainerImpl;->mSplitDimmer:Landroid/view/animation/Animation;

    .line 20
    return-void
.end method

.method public setSplitDimmer(Landroid/view/animation/Animation;)V
    .locals 5
    .param p1, "a"    # Landroid/view/animation/Animation;

    .line 33
    invoke-virtual {p0}, Lcom/android/server/wm/WindowContainerImpl;->clearTransitionDimmer()V

    .line 34
    instance-of v0, p1, Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_2

    .line 35
    move-object v0, p1

    check-cast v0, Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v0

    .line 36
    .local v0, "animations":Ljava/util/List;, "Ljava/util/List<Landroid/view/animation/Animation;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 37
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 38
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/animation/Animation;

    .line 39
    .local v2, "animation":Landroid/view/animation/Animation;
    invoke-static {}, Lcom/android/server/wm/AppTransitionStub;->get()Lcom/android/server/wm/AppTransitionStub;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/wm/WindowContainerImpl;->wc:Lcom/android/server/wm/WindowContainer;

    .line 40
    invoke-interface {v3, v2, v4}, Lcom/android/server/wm/AppTransitionStub;->dimSplitDimmerAboveIfNeeded(Landroid/view/animation/Animation;Lcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;

    move-result-object v3

    .line 41
    .local v3, "splitDimmer":Landroid/view/animation/Animation;
    iput-object v3, p0, Lcom/android/server/wm/WindowContainerImpl;->mSplitDimmer:Landroid/view/animation/Animation;

    .line 42
    if-eqz v3, :cond_0

    .line 43
    goto :goto_1

    .line 37
    .end local v2    # "animation":Landroid/view/animation/Animation;
    .end local v3    # "splitDimmer":Landroid/view/animation/Animation;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    .end local v0    # "animations":Ljava/util/List;, "Ljava/util/List<Landroid/view/animation/Animation;>;"
    .end local v1    # "i":I
    :cond_1
    :goto_1
    goto :goto_2

    .line 48
    :cond_2
    invoke-static {}, Lcom/android/server/wm/AppTransitionStub;->get()Lcom/android/server/wm/AppTransitionStub;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/WindowContainerImpl;->wc:Lcom/android/server/wm/WindowContainer;

    invoke-interface {v0, p1, v1}, Lcom/android/server/wm/AppTransitionStub;->dimSplitDimmerAboveIfNeeded(Landroid/view/animation/Animation;Lcom/android/server/wm/WindowContainer;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 49
    .local v0, "splitDimmer":Landroid/view/animation/Animation;
    iput-object v0, p0, Lcom/android/server/wm/WindowContainerImpl;->mSplitDimmer:Landroid/view/animation/Animation;

    .line 51
    .end local v0    # "splitDimmer":Landroid/view/animation/Animation;
    :goto_2
    return-void
.end method
