.class Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/wm/MiuiMultiWindowRecommendController;->addSplitScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

.field final synthetic val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MiuiMultiWindowRecommendController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 191
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iput-object p2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 195
    const-string v0, "_"

    const-string v1, "MiuiMultiWindowRecommendController"

    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v2, v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->hasNotification()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v2, v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-virtual {v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->isSplitScreenRecommendValid(Lcom/android/server/wm/RecommendDataEntry;)Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_1

    .line 201
    :cond_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-static {v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V

    .line 202
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x110c003b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/SplitScreenRecommendLayout;

    invoke-static {v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/SplitScreenRecommendLayout;)V

    .line 204
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v2, v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 205
    invoke-virtual {v2}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/wm/DisplayContent;->getInsetsStateController()Lcom/android/server/wm/InsetsStateController;

    move-result-object v2

    .line 204
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getStatusBarHeight(Lcom/android/server/wm/InsetsStateController;Z)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 206
    invoke-virtual {v4}, Lcom/android/server/wm/WindowManagerService;->getDefaultDisplayContentLocked()Lcom/android/server/wm/DisplayContent;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/wm/DisplayContent;->mDisplayFrames:Lcom/android/server/wm/DisplayFrames;

    invoke-static {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getDisplayCutoutHeight(Lcom/android/server/wm/DisplayFrames;)I

    move-result v4

    .line 204
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 207
    .local v2, "statusBarHeight":I
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/android/server/wm/SplitScreenRecommendLayout;->createLayoutParams(I)Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/view/WindowManager$LayoutParams;)V

    .line 208
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/wm/SplitScreenRecommendLayout;->getSplitScreenIconContainer()Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fputmSplitScreenRecommendIconContainer(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/widget/RelativeLayout;)V

    .line 209
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 210
    invoke-virtual {v5}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-virtual {v7}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryUserId()I

    move-result v7

    .line 209
    invoke-static {v5, v6, v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->loadDrawableByPackageName(Ljava/lang/String;Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 211
    invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->val$recommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 212
    invoke-virtual {v8}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryUserId()I

    move-result v8

    .line 211
    invoke-static {v6, v7, v8}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->loadDrawableByPackageName(Ljava/lang/String;Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 209
    invoke-virtual {v4, v5, v6}, Lcom/android/server/wm/SplitScreenRecommendLayout;->setSplitScreenIcon(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 213
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v5, v5, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->splitScreenClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Lcom/android/server/wm/SplitScreenRecommendLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    const/4 v4, 0x1

    new-array v5, v4, [Landroid/view/View;

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v5}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v5

    invoke-interface {v5}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v6

    new-array v3, v3, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v5, v6, v3}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    .line 215
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v5, 0x2

    if-ge v3, v5, :cond_3

    .line 216
    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v5, v5, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mWindowManager:Landroid/view/WindowManager;

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    invoke-static {}, Landroid/view/inspector/WindowInspector;->getGlobalWindowViews()Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 218
    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v5, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->setSplitScreenRecommendState(Z)V

    .line 219
    iget-object v5, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v5}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->startMultiWindowRecommendAnimation(Landroid/view/View;Z)V

    .line 220
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendViewForTimer()V

    .line 221
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v4, v4, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    if-eqz v4, :cond_1

    .line 222
    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v4, v4, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v6

    .line 223
    invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v6

    .line 224
    invoke-virtual {v6}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v8

    .line 225
    invoke-virtual {v8}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-static {v6}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->-$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/RecommendDataEntry;

    move-result-object v7

    .line 226
    invoke-virtual {v7}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 223
    invoke-virtual {v4, v5, v0}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSplitScreenRecommendExposeEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :cond_1
    return-void

    .line 215
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 231
    .end local v3    # "i":I
    :cond_3
    const-string v0, " addSplitScreenRecommendView twice fail "

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    nop

    .end local v2    # "statusBarHeight":I
    goto :goto_2

    .line 196
    :cond_4
    :goto_1
    const-string v0, "addSplitScreenRecommendView: hasNotification or invalid, return"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->setLastSplitScreenRecommendTime(J)V

    .line 198
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->clearRecentAppList()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;->this$0:Lcom/android/server/wm/MiuiMultiWindowRecommendController;

    invoke-virtual {v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendViewForTimer()V

    .line 234
    const-string v2, " addSplitScreenRecommendView fail"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 236
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void
.end method
