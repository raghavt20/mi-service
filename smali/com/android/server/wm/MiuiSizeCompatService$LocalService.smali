.class final Lcom/android/server/wm/MiuiSizeCompatService$LocalService;
.super Lcom/android/server/wm/MiuiSizeCompatInternal;
.source "MiuiSizeCompatService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MiuiSizeCompatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MiuiSizeCompatService;


# direct methods
.method public static synthetic $r8$lambda$2ra2ZNN48_wua7nyYvLMoci82QU(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$mshowWarningNotification(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityRecord;)V

    return-void
.end method

.method private constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;)V
    .locals 0

    .line 1193
    iput-object p1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/MiuiSizeCompatService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;-><init>(Lcom/android/server/wm/MiuiSizeCompatService;)V

    return-void
.end method


# virtual methods
.method public executeShellCommand(Ljava/lang/String;[Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 1
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 1288
    const-string v0, "-setFixedAspectRatio"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1289
    const-string v0, "-setSizeCompatDebug"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1293
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 1290
    :cond_1
    :goto_0
    const-string v0, "Command is deprecated, use adb shell cmd MiuiSizeCompat instead"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1291
    const/4 v0, 0x1

    return v0
.end method

.method public getAspectGravityByPackage(Ljava/lang/String;)I
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1269
    const/4 v0, 0x0

    .line 1271
    .local v0, "info":Landroid/sizecompat/AspectRatioInfo;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1272
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    goto :goto_0

    .line 1273
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1274
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    goto :goto_0

    .line 1276
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 1278
    :goto_0
    if-eqz v0, :cond_2

    iget v1, v0, Landroid/sizecompat/AspectRatioInfo;->mGravity:I

    goto :goto_1

    :cond_2
    const/16 v1, 0x11

    :goto_1
    return v1
.end method

.method public getAspectRatioByPackage(Ljava/lang/String;)F
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1245
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$mgetAspectRatioByPackage(Lcom/android/server/wm/MiuiSizeCompatService;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getMiuiGameSizeCompatEnabledApps()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation

    .line 1264
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->getMiuiGameSizeCompatEnabledApps()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getMiuiSizeCompatEnabledApps()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/sizecompat/AspectRatioInfo;",
            ">;"
        }
    .end annotation

    .line 1206
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->getMiuiSizeCompatEnabledApps()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getScaleModeByPackage(Ljava/lang/String;)I
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1249
    const/4 v0, 0x0

    .line 1251
    .local v0, "info":Landroid/sizecompat/AspectRatioInfo;
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1253
    const/4 v1, 0x3

    return v1

    .line 1254
    :cond_0
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1255
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    goto :goto_0

    .line 1257
    :cond_1
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmStaticSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/sizecompat/AspectRatioInfo;

    .line 1259
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/sizecompat/AspectRatioInfo;->getScaleMode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public inMiuiGameSizeCompat(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1283
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmGameSettingConfig(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAppSizeCompatRestarting(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .line 1239
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    .line 1240
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1239
    :goto_0
    return v0
.end method

.method public onSystemReady(Lcom/android/server/wm/ActivityTaskManagerService;)V
    .locals 1
    .param p1, "atms"    # Lcom/android/server/wm/ActivityTaskManagerService;

    .line 1198
    :try_start_0
    iget-object v0, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0, p1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$monSystemReady(Lcom/android/server/wm/MiuiSizeCompatService;Lcom/android/server/wm/ActivityTaskManagerService;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1201
    goto :goto_0

    .line 1199
    :catch_0
    move-exception v0

    .line 1200
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1202
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public showWarningNotification(Lcom/android/server/wm/ActivityRecord;)V
    .locals 5
    .param p1, "ar"    # Lcom/android/server/wm/ActivityRecord;

    .line 1211
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmNotificationShowing(Lcom/android/server/wm/MiuiSizeCompatService;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v2}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmServiceReady(Lcom/android/server/wm/MiuiSizeCompatService;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v0

    goto :goto_1

    :cond_1
    :goto_0
    move v2, v1

    .line 1212
    .local v2, "skip":Z
    :goto_1
    if-nez v2, :cond_3

    .line 1213
    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v4}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v4

    iget-object v4, v4, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetPERMISSION_ACTIVITY(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    :cond_2
    move v2, v0

    .line 1215
    :cond_3
    const-string v0, "MiuiSizeCompatService"

    if-eqz v2, :cond_5

    .line 1216
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz p1, :cond_4

    sget-boolean v1, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z

    if-eqz v1, :cond_4

    .line 1217
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not show notification for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,mNotificationShowing= "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmNotificationShowing(Lcom/android/server/wm/MiuiSizeCompatService;)Z

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,mCurFullAct= "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    :cond_4
    return-void

    .line 1223
    :cond_5
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    if-eq p1, v1, :cond_6

    .line 1224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ar="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\nmCurFullAct="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmCurFullAct(Lcom/android/server/wm/MiuiSizeCompatService;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v3

    iget-object v3, v3, Lcom/android/server/wm/ActivityRecord;->shortComponentName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    :cond_6
    iget-object v1, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v1}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmSettingConfigs(Lcom/android/server/wm/MiuiSizeCompatService;)Ljava/util/Map;

    move-result-object v1

    iget-object v3, p1, Lcom/android/server/wm/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/sizecompat/AspectRatioInfo;

    .line 1227
    .local v1, "info":Landroid/sizecompat/AspectRatioInfo;
    if-eqz v1, :cond_7

    iget v3, v1, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_7

    iget v3, v1, Landroid/sizecompat/AspectRatioInfo;->mLastAspectRatio:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_7

    .line 1229
    new-instance v0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/server/wm/MiuiSizeCompatService$LocalService$$ExternalSyntheticLambda0;-><init>()V

    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v0, v3, p1}, Lcom/android/internal/util/function/pooled/PooledLambda;->obtainMessage(Ljava/util/function/BiConsumer;Ljava/lang/Object;Ljava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1231
    .local v0, "message":Landroid/os/Message;
    iget-object v3, p0, Lcom/android/server/wm/MiuiSizeCompatService$LocalService;->this$0:Lcom/android/server/wm/MiuiSizeCompatService;

    invoke-static {v3}, Lcom/android/server/wm/MiuiSizeCompatService;->-$$Nest$fgetmFgHandler(Lcom/android/server/wm/MiuiSizeCompatService;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .end local v0    # "message":Landroid/os/Message;
    goto :goto_2

    .line 1232
    :cond_7
    if-eqz v1, :cond_8

    .line 1233
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Can not show notification, mAspectRatio="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/sizecompat/AspectRatioInfo;->mAspectRatio:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mLastAspectRatio="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/sizecompat/AspectRatioInfo;->mLastAspectRatio:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1232
    :cond_8
    :goto_2
    nop

    .line 1235
    :goto_3
    return-void
.end method
