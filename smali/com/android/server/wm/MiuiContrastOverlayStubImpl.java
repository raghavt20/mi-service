public class com.android.server.wm.MiuiContrastOverlayStubImpl implements com.android.server.wm.MiuiContrastOverlayStub {
	 /* .source "MiuiContrastOverlayStubImpl.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer TYPE_LAYER_MULTIPLIER;
	 private static android.view.SurfaceControlFeatureImpl surfaceControlFeature;
	 /* # instance fields */
	 private java.lang.String TAG;
	 private Integer mDh;
	 private android.view.Display mDisplay;
	 private Integer mDw;
	 private android.view.SurfaceControl mSurfaceControl;
	 private android.view.SurfaceControl$Transaction mTransaction;
	 /* # direct methods */
	 static com.android.server.wm.MiuiContrastOverlayStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 25 */
		 /* new-instance v0, Landroid/view/SurfaceControlFeatureImpl; */
		 /* invoke-direct {v0}, Landroid/view/SurfaceControlFeatureImpl;-><init>()V */
		 return;
	 } // .end method
	 public com.android.server.wm.MiuiContrastOverlayStubImpl ( ) {
		 /* .locals 1 */
		 /* .line 17 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 20 */
		 final String v0 = "MiuiContrastOverlayStubImpl"; // const-string v0, "MiuiContrastOverlayStubImpl"
		 this.TAG = v0;
		 /* .line 21 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I */
		 /* .line 22 */
		 /* iput v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void hideContrastOverlay ( ) {
		 /* .locals 2 */
		 /* .line 73 */
		 v0 = this.mSurfaceControl;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 74 */
			 v1 = this.mTransaction;
			 (( android.view.SurfaceControl$Transaction ) v1 ).hide ( v0 ); // invoke-virtual {v1, v0}, Landroid/view/SurfaceControl$Transaction;->hide(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
			 /* .line 75 */
			 v0 = this.mSurfaceControl;
			 (( android.view.SurfaceControl ) v0 ).release ( ); // invoke-virtual {v0}, Landroid/view/SurfaceControl;->release()V
			 /* .line 76 */
			 int v0 = 0; // const/4 v0, 0x0
			 this.mSurfaceControl = v0;
			 /* .line 78 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void init ( com.android.server.wm.DisplayContent p0, android.util.DisplayMetrics p1, android.content.Context p2 ) {
		 /* .locals 7 */
		 /* .param p1, "dc" # Lcom/android/server/wm/DisplayContent; */
		 /* .param p2, "dm" # Landroid/util/DisplayMetrics; */
		 /* .param p3, "mcontext" # Landroid/content/Context; */
		 /* .line 29 */
		 (( com.android.server.wm.DisplayContent ) p1 ).getDisplay ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplay()Landroid/view/Display;
		 this.mDisplay = v0;
		 /* .line 30 */
		 (( com.android.server.wm.DisplayContent ) p1 ).getDisplayInfo ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->getDisplayInfo()Landroid/view/DisplayInfo;
		 /* .line 31 */
		 /* .local v0, "defaultInfo":Landroid/view/DisplayInfo; */
		 /* iget v1, v0, Landroid/view/DisplayInfo;->logicalWidth:I */
		 /* iput v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I */
		 /* .line 32 */
		 /* iget v1, v0, Landroid/view/DisplayInfo;->logicalHeight:I */
		 /* iput v1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I */
		 /* .line 33 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 34 */
		 /* .local v1, "control":Landroid/view/SurfaceControl; */
		 /* iget v2, p2, Landroid/util/DisplayMetrics;->densityDpi:I */
		 /* int-to-float v2, v2 */
		 /* const/high16 v3, 0x43200000 # 160.0f */
		 /* div-float/2addr v2, v3 */
		 /* .line 37 */
		 /* .local v2, "constNum":F */
		 try { // :try_start_0
			 (( com.android.server.wm.DisplayContent ) p1 ).makeOverlay ( ); // invoke-virtual {p1}, Lcom/android/server/wm/DisplayContent;->makeOverlay()Landroid/view/SurfaceControl$Builder;
			 final String v4 = "MiuiContrastOverlay"; // const-string v4, "MiuiContrastOverlay"
			 /* .line 38 */
			 (( android.view.SurfaceControl$Builder ) v3 ).setName ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setName(Ljava/lang/String;)Landroid/view/SurfaceControl$Builder;
			 /* .line 39 */
			 int v4 = 1; // const/4 v4, 0x1
			 (( android.view.SurfaceControl$Builder ) v3 ).setOpaque ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/SurfaceControl$Builder;->setOpaque(Z)Landroid/view/SurfaceControl$Builder;
			 /* .line 40 */
			 (( android.view.SurfaceControl$Builder ) v3 ).setColorLayer ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->setColorLayer()Landroid/view/SurfaceControl$Builder;
			 /* .line 41 */
			 (( android.view.SurfaceControl$Builder ) v3 ).build ( ); // invoke-virtual {v3}, Landroid/view/SurfaceControl$Builder;->build()Landroid/view/SurfaceControl;
			 /* move-object v1, v3 */
			 /* .line 42 */
			 android.view.SurfaceControl .getGlobalTransaction ( );
			 this.mTransaction = v3;
			 /* .line 43 */
			 v3 = com.android.server.wm.MiuiContrastOverlayStubImpl.surfaceControlFeature;
			 v4 = this.mDisplay;
			 v4 = 			 (( android.view.Display ) v4 ).getLayerStack ( ); // invoke-virtual {v4}, Landroid/view/Display;->getLayerStack()I
			 (( android.view.SurfaceControlFeatureImpl ) v3 ).setLayerStack ( v1, v4 ); // invoke-virtual {v3, v1, v4}, Landroid/view/SurfaceControlFeatureImpl;->setLayerStack(Landroid/view/SurfaceControl;I)V
			 /* .line 44 */
			 v3 = this.mTransaction;
			 /* const v4, 0xf423f */
			 (( android.view.SurfaceControl$Transaction ) v3 ).setLayer ( v1, v4 ); // invoke-virtual {v3, v1, v4}, Landroid/view/SurfaceControl$Transaction;->setLayer(Landroid/view/SurfaceControl;I)Landroid/view/SurfaceControl$Transaction;
			 /* .line 45 */
			 v3 = this.mTransaction;
			 int v4 = 0; // const/4 v4, 0x0
			 (( android.view.SurfaceControl$Transaction ) v3 ).setPosition ( v1, v4, v4 ); // invoke-virtual {v3, v1, v4, v4}, Landroid/view/SurfaceControl$Transaction;->setPosition(Landroid/view/SurfaceControl;FF)Landroid/view/SurfaceControl$Transaction;
			 /* .line 46 */
			 v3 = this.mTransaction;
			 (( android.view.SurfaceControl$Transaction ) v3 ).show ( v1 ); // invoke-virtual {v3, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
			 /* :try_end_0 */
			 /* .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 49 */
			 /* .line 47 */
			 /* :catch_0 */
			 /* move-exception v3 */
			 /* .line 48 */
			 /* .local v3, "e":Landroid/view/Surface$OutOfResourcesException; */
			 v4 = this.TAG;
			 /* new-instance v5, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v6 = "createSurface e "; // const-string v6, "createSurface e "
			 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Log .d ( v4,v5 );
			 /* .line 50 */
		 } // .end local v3 # "e":Landroid/view/Surface$OutOfResourcesException;
	 } // :goto_0
	 this.mSurfaceControl = v1;
	 /* .line 51 */
	 return;
} // .end method
public void positionSurface ( Integer p0, Integer p1 ) {
	 /* .locals 6 */
	 /* .param p1, "dw" # I */
	 /* .param p2, "dh" # I */
	 /* .line 54 */
	 /* iget v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I */
	 /* if-ne p1, v0, :cond_0 */
	 /* iget v0, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I */
	 /* if-eq p2, v0, :cond_1 */
	 /* .line 55 */
} // :cond_0
/* iput p1, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I */
/* .line 56 */
/* iput p2, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I */
/* .line 57 */
v0 = this.mTransaction;
v1 = this.mSurfaceControl;
/* new-instance v2, Landroid/graphics/Rect; */
/* iget v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I */
/* iget v4, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V */
(( android.view.SurfaceControl$Transaction ) v0 ).setWindowCrop ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
/* .line 59 */
} // :cond_1
return;
} // .end method
public void setAlpha ( Float p0 ) {
/* .locals 2 */
/* .param p1, "alpha" # F */
/* .line 69 */
v0 = this.mTransaction;
v1 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).setAlpha ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 70 */
return;
} // .end method
public void showContrastOverlay ( Float p0 ) {
/* .locals 6 */
/* .param p1, "alpha" # F */
/* .line 62 */
v0 = com.android.server.wm.MiuiContrastOverlayStubImpl.surfaceControlFeature;
v1 = this.mSurfaceControl;
int v2 = 3; // const/4 v2, 0x3
/* new-array v2, v2, [F */
/* fill-array-data v2, :array_0 */
(( android.view.SurfaceControlFeatureImpl ) v0 ).setColor ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/SurfaceControlFeatureImpl;->setColor(Landroid/view/SurfaceControl;[F)V
/* .line 63 */
(( com.android.server.wm.MiuiContrastOverlayStubImpl ) p0 ).setAlpha ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->setAlpha(F)V
/* .line 64 */
v0 = this.mTransaction;
v1 = this.mSurfaceControl;
/* new-instance v2, Landroid/graphics/Rect; */
/* iget v3, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDw:I */
/* iget v4, p0, Lcom/android/server/wm/MiuiContrastOverlayStubImpl;->mDh:I */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V */
(( android.view.SurfaceControl$Transaction ) v0 ).setWindowCrop ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
/* .line 65 */
v0 = this.mTransaction;
v1 = this.mSurfaceControl;
(( android.view.SurfaceControl$Transaction ) v0 ).show ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/SurfaceControl$Transaction;->show(Landroid/view/SurfaceControl;)Landroid/view/SurfaceControl$Transaction;
/* .line 66 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
} // .end array-data
} // .end method
