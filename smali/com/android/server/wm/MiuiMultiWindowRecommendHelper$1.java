class com.android.server.wm.MiuiMultiWindowRecommendHelper$1 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "MiuiMultiWindowRecommendHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiMultiWindowRecommendHelper this$0; //synthetic
/* # direct methods */
 com.android.server.wm.MiuiMultiWindowRecommendHelper$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper; */
/* .line 92 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 10 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 95 */
v0 = this.this$0;
v0 = com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$misDeviceSupportSplitScreenRecommend ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
	 v0 = 	 com.android.server.wm.MiuiDesktopModeUtils .isDesktopActive ( );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 98 */
	 } // :cond_0
	 v0 = this.this$0;
	 v0 = this.mMiuiMultiWindowRecommendController;
	 (( com.android.server.wm.MiuiMultiWindowRecommendController ) v0 ).removeFreeFormRecommendView ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V
	 /* .line 99 */
	 v0 = this.this$0;
	 (( com.android.server.wm.MiuiMultiWindowRecommendHelper ) v0 ).getFocusedTask ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->getFocusedTask()Lcom/android/server/wm/Task;
	 /* .line 100 */
	 /* .local v0, "focusedTask":Lcom/android/server/wm/Task; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 101 */
	 /* .local v1, "focusedTaskPackageName":Ljava/lang/String; */
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 v2 = this.realActivity;
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* .line 102 */
			 v2 = this.realActivity;
			 (( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
			 /* move-object v7, v1 */
			 /* .line 104 */
		 } // :cond_1
		 /* move-object v7, v1 */
	 } // .end local v1 # "focusedTaskPackageName":Ljava/lang/String;
	 /* .local v7, "focusedTaskPackageName":Ljava/lang/String; */
} // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_2
	 v1 = this.mForegroundPackageName;
	 v1 = 	 (( java.lang.String ) v7 ).equals ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 106 */
		 v1 = this.this$0;
		 v1 = 		 com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$mcheckPreConditionsForSplitScreen ( v1,v0 );
		 if ( v1 != null) { // if-eqz v1, :cond_2
			 /* .line 107 */
			 v8 = this.this$0;
			 /* new-instance v9, Lcom/android/server/wm/SplitScreenRecommendTaskInfo; */
			 /* iget v2, v0, Lcom/android/server/wm/Task;->mTaskId:I */
			 /* .line 108 */
			 java.lang.System .currentTimeMillis ( );
			 /* move-result-wide v4 */
			 /* move-object v1, v9 */
			 /* move-object v3, v7 */
			 /* move-object v6, v0 */
			 /* invoke-direct/range {v1 ..v6}, Lcom/android/server/wm/SplitScreenRecommendTaskInfo;-><init>(ILjava/lang/String;JLcom/android/server/wm/Task;)V */
			 /* .line 107 */
			 com.android.server.wm.MiuiMultiWindowRecommendHelper .-$$Nest$mpredictSplitScreen ( v8,v9 );
			 /* .line 109 */
			 /* new-instance v1, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v2 = " focused task id = "; // const-string v2, " focused task id = "
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 v2 = 			 (( com.android.server.wm.Task ) v0 ).getRootTaskId ( ); // invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 final String v2 = " packageName = "; // const-string v2, " packageName = "
			 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 final String v2 = "MiuiMultiWindowRecommendHelper"; // const-string v2, "MiuiMultiWindowRecommendHelper"
			 android.util.Slog .d ( v2,v1 );
			 /* .line 113 */
		 } // :cond_2
		 return;
		 /* .line 96 */
	 } // .end local v0 # "focusedTask":Lcom/android/server/wm/Task;
} // .end local v7 # "focusedTaskPackageName":Ljava/lang/String;
} // :cond_3
} // :goto_1
return;
} // .end method
