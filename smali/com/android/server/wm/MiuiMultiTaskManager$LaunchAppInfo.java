class com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo {
	 /* .source "MiuiMultiTaskManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiMultiTaskManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "LaunchAppInfo" */
} // .end annotation
/* # instance fields */
private android.content.ComponentName returnTarget;
private java.util.ArrayList supports;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static android.content.ComponentName -$$Nest$fgetreturnTarget ( com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.returnTarget;
} // .end method
static java.util.ArrayList -$$Nest$fgetsupports ( com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.supports;
} // .end method
public com.android.server.wm.MiuiMultiTaskManager$LaunchAppInfo ( ) {
/* .locals 0 */
/* .param p2, "returnTarget" # Landroid/content/ComponentName; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Landroid/content/ComponentName;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 28 */
/* .local p1, "supports":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 29 */
this.supports = p1;
/* .line 30 */
this.returnTarget = p2;
/* .line 31 */
return;
} // .end method
