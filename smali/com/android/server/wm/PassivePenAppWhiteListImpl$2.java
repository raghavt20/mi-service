class com.android.server.wm.PassivePenAppWhiteListImpl$2 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "PassivePenAppWhiteListImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/PassivePenAppWhiteListImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.wm.PassivePenAppWhiteListImpl this$0; //synthetic
/* # direct methods */
 com.android.server.wm.PassivePenAppWhiteListImpl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/wm/PassivePenAppWhiteListImpl; */
/* .line 165 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 5 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 168 */
v0 = this.mForegroundPackageName;
/* .line 169 */
/* .local v0, "forePkg":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " mForegroundPackageName= "; // const-string v2, " mForegroundPackageName= "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "PassivePenAppWhiteListImpl"; // const-string v2, "PassivePenAppWhiteListImpl"
android.util.Slog .d ( v2,v1 );
/* .line 170 */
v1 = this.this$0;
v1 = (( com.android.server.wm.PassivePenAppWhiteListImpl ) v1 ).whiteListApp ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->whiteListApp(Ljava/lang/String;)Z
/* .line 171 */
/* .local v1, "whiteListApp":Z */
v3 = this.this$0;
v3 = (( com.android.server.wm.PassivePenAppWhiteListImpl ) v3 ).blackAppAndWhiteAppChanged ( v0 ); // invoke-virtual {v3, v0}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->blackAppAndWhiteAppChanged(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 172 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " whiteListApp= "; // const-string v4, " whiteListApp= "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 173 */
v2 = this.this$0;
v3 = this.mAtmService;
v3 = (( com.android.server.wm.ActivityTaskManagerService ) v3 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v3}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
/* iput-boolean v3, v2, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z */
/* .line 174 */
v2 = this.this$0;
/* iget-boolean v2, v2, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->splitMode:Z */
/* if-nez v2, :cond_0 */
/* .line 175 */
v2 = this.this$0;
(( com.android.server.wm.PassivePenAppWhiteListImpl ) v2 ).dispatchTopAppWhiteState ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/PassivePenAppWhiteListImpl;->dispatchTopAppWhiteState(I)Z
/* .line 178 */
} // :cond_0
return;
} // .end method
