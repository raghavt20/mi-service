.class final Lcom/android/server/wm/MultiSplitBlockListController$H;
.super Landroid/os/Handler;
.source "MultiSplitBlockListController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/wm/MultiSplitBlockListController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "H"
.end annotation


# static fields
.field private static final MSG_MULTI_SPLIT_BLOCK_LIST_CHANGED:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/android/server/wm/MultiSplitBlockListController;


# direct methods
.method public constructor <init>(Lcom/android/server/wm/MultiSplitBlockListController;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/wm/MultiSplitBlockListController;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 180
    iput-object p1, p0, Lcom/android/server/wm/MultiSplitBlockListController$H;->this$0:Lcom/android/server/wm/MultiSplitBlockListController;

    .line 181
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 182
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 185
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 186
    iget-object v0, p0, Lcom/android/server/wm/MultiSplitBlockListController$H;->this$0:Lcom/android/server/wm/MultiSplitBlockListController;

    invoke-static {v0}, Lcom/android/server/wm/MultiSplitBlockListController;->-$$Nest$fgetmGlobalLock(Lcom/android/server/wm/MultiSplitBlockListController;)Lcom/android/server/wm/WindowManagerGlobalLock;

    move-result-object v0

    monitor-enter v0

    .line 187
    :try_start_0
    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->boostPriorityForLockedSection()V

    .line 188
    iget-object v1, p0, Lcom/android/server/wm/MultiSplitBlockListController$H;->this$0:Lcom/android/server/wm/MultiSplitBlockListController;

    invoke-static {v1}, Lcom/android/server/wm/MultiSplitBlockListController;->-$$Nest$mupdateDeferredBlockListLocked(Lcom/android/server/wm/MultiSplitBlockListController;)V

    .line 189
    invoke-static {}, Lcom/android/server/wm/WindowManagerService;->resetPriorityAfterLockedSection()V

    .line 190
    monitor-exit v0

    return-void

    .line 191
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 192
    :cond_0
    return-void
.end method
