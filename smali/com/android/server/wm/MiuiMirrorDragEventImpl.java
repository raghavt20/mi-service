public class com.android.server.wm.MiuiMirrorDragEventImpl implements com.android.server.wm.MiuiMirrorDragEventStub {
	 /* .source "MiuiMirrorDragEventImpl.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public com.android.server.wm.MiuiMirrorDragEventImpl ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public android.view.DragEvent getDragEvent ( com.android.server.wm.WindowState p0, Float p1, Float p2, android.content.ClipData p3, com.android.server.wm.DragState p4, android.view.DragEvent p5 ) {
		 /* .locals 12 */
		 /* .param p1, "newWin" # Lcom/android/server/wm/WindowState; */
		 /* .param p2, "touchX" # F */
		 /* .param p3, "touchY" # F */
		 /* .param p4, "data" # Landroid/content/ClipData; */
		 /* .param p5, "dragState" # Lcom/android/server/wm/DragState; */
		 /* .param p6, "event" # Landroid/view/DragEvent; */
		 /* .line 17 */
		 /* move-object/from16 v0, p5 */
		 final String v1 = "com.xiaomi.mirror"; // const-string v1, "com.xiaomi.mirror"
		 (( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 18 */
			 v1 = 			 (( com.android.server.wm.WindowState ) p1 ).getOwningUid ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningUid()I
			 v1 = 			 android.os.UserHandle .getUserId ( v1 );
			 /* .line 20 */
			 /* .local v1, "targetUserId":I */
			 v2 = com.android.server.wm.GetDragEventProxy.mFlags;
			 v10 = 			 (( com.xiaomi.reflect.RefInt ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I
			 /* .line 21 */
			 /* .local v10, "flags":I */
			 /* and-int/lit16 v2, v10, 0x100 */
			 if ( v2 != null) { // if-eqz v2, :cond_0
				 v2 = com.android.server.wm.GetDragEventProxy.DRAG_FLAGS_URI_ACCESS;
				 int v3 = 0; // const/4 v3, 0x0
				 v2 = 				 (( com.xiaomi.reflect.RefInt ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I
				 /* and-int/2addr v2, v10 */
				 if ( v2 != null) { // if-eqz v2, :cond_0
					 v2 = com.android.server.wm.GetDragEventProxy.mData;
					 /* .line 22 */
					 (( com.xiaomi.reflect.RefObject ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;
					 if ( v2 != null) { // if-eqz v2, :cond_0
						 /* .line 23 */
						 /* new-instance v11, Lcom/android/server/wm/DragAndDropPermissionsHandler; */
						 v2 = com.android.server.wm.GetDragEventProxy.mService;
						 /* .line 24 */
						 (( com.xiaomi.reflect.RefObject ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;
						 /* check-cast v2, Lcom/android/server/wm/WindowManagerService; */
						 v4 = this.mGlobalLock;
						 v2 = com.android.server.wm.GetDragEventProxy.mData;
						 /* .line 25 */
						 (( com.xiaomi.reflect.RefObject ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;
						 /* move-object v5, v2 */
						 /* check-cast v5, Landroid/content/ClipData; */
						 v2 = com.android.server.wm.GetDragEventProxy.mUid;
						 /* .line 26 */
						 v6 = 						 (( com.xiaomi.reflect.RefInt ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I
						 /* .line 27 */
						 (( com.android.server.wm.WindowState ) p1 ).getOwningPackage ( ); // invoke-virtual {p1}, Lcom/android/server/wm/WindowState;->getOwningPackage()Ljava/lang/String;
						 v2 = com.android.server.wm.GetDragEventProxy.DRAG_FLAGS_URI_PERMISSIONS;
						 /* .line 28 */
						 v2 = 						 (( com.xiaomi.reflect.RefInt ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I
						 /* and-int v8, v10, v2 */
						 v2 = com.android.server.wm.GetDragEventProxy.mSourceUserId;
						 /* .line 29 */
						 v9 = 						 (( com.xiaomi.reflect.RefInt ) v2 ).get ( v0 ); // invoke-virtual {v2, v0}, Lcom/xiaomi/reflect/RefInt;->get(Ljava/lang/Object;)I
						 /* move-object v2, v11 */
						 /* move-object v3, v4 */
						 /* move-object v4, v5 */
						 /* move v5, v6 */
						 /* move-object v6, v7 */
						 /* move v7, v8 */
						 /* move v8, v9 */
						 /* move v9, v1 */
						 /* invoke-direct/range {v2 ..v9}, Lcom/android/server/wm/DragAndDropPermissionsHandler;-><init>(Lcom/android/server/wm/WindowManagerGlobalLock;Landroid/content/ClipData;ILjava/lang/String;III)V */
						 /* .local v2, "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler; */
						 /* .line 32 */
					 } // .end local v2 # "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler;
				 } // :cond_0
				 int v2 = 0; // const/4 v2, 0x0
				 /* .line 34 */
				 /* .restart local v2 # "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler; */
			 } // :goto_0
			 v9 = com.android.server.wm.GetDragEventProxy.obtainDragEvent;
			 int v3 = 1; // const/4 v3, 0x1
			 java.lang.Integer .valueOf ( v3 );
			 java.lang.Float .valueOf ( p2 );
			 java.lang.Float .valueOf ( p3 );
			 v6 = com.android.server.wm.GetDragEventProxy.mData;
			 /* .line 35 */
			 (( com.xiaomi.reflect.RefObject ) v6 ).get ( v0 ); // invoke-virtual {v6, v0}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;
			 int v7 = 0; // const/4 v7, 0x0
			 java.lang.Boolean .valueOf ( v7 );
			 /* move-object v8, v2 */
			 /* filled-new-array/range {v3 ..v8}, [Ljava/lang/Object; */
			 /* .line 34 */
			 (( com.xiaomi.reflect.RefMethod ) v9 ).invoke ( v0, v3 ); // invoke-virtual {v9, v0, v3}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
			 /* check-cast v3, Landroid/view/DragEvent; */
		 } // .end local p6 # "event":Landroid/view/DragEvent;
		 /* .local v3, "event":Landroid/view/DragEvent; */
		 /* .line 17 */
	 } // .end local v1 # "targetUserId":I
} // .end local v2 # "dragAndDropPermissions":Lcom/android/server/wm/DragAndDropPermissionsHandler;
} // .end local v3 # "event":Landroid/view/DragEvent;
} // .end local v10 # "flags":I
/* .restart local p6 # "event":Landroid/view/DragEvent; */
} // :cond_1
/* move-object/from16 v3, p6 */
/* .line 37 */
} // .end local p6 # "event":Landroid/view/DragEvent;
/* .restart local v3 # "event":Landroid/view/DragEvent; */
} // :goto_1
} // .end method
