.class public Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;
.super Ljava/lang/Object;
.source "MiuiFreeFormKeyCombinationHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiFreeFormKeyCombinationHelper"


# instance fields
.field private mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

.field private mIsHandleKeyCombination:Z


# direct methods
.method constructor <init>(Lcom/android/server/wm/MiuiFreeFormGestureController;)V
    .locals 1
    .param p1, "gestureController"    # Lcom/android/server/wm/MiuiFreeFormGestureController;

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z

    .line 21
    iput-object p1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    .line 22
    return-void
.end method

.method private getActivityName(Landroid/content/Context;Lcom/android/server/wm/ActivityRecord;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "activityRecord"    # Lcom/android/server/wm/ActivityRecord;

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 92
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_0

    .line 93
    iget-object v1, p2, Lcom/android/server/wm/ActivityRecord;->info:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v1, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 95
    :cond_0
    const-string v1, ""

    return-object v1
.end method

.method static synthetic lambda$freeFormAndFullScreenToggleByKeyCombination$0(Lcom/android/server/wm/ActivityRecord;)Z
    .locals 1
    .param p0, "app"    # Lcom/android/server/wm/ActivityRecord;

    .line 40
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/server/wm/ActivityRecord;->isAnimating(I)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$showNotSupportToast$1(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "activityName"    # Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f029e

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "tips":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 103
    return-void
.end method

.method private showNotSupportToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "activityName"    # Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1, p2}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 104
    return-void
.end method


# virtual methods
.method public exitFreeFormByKeyCombination(Lcom/android/server/wm/Task;)V
    .locals 5
    .param p1, "task"    # Lcom/android/server/wm/Task;

    .line 107
    iget-object v0, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 108
    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 109
    .local v0, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInMiniFreeFormMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    return-void

    .line 112
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v1}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    .line 113
    .local v2, "ar":Lcom/android/server/wm/ActivityRecord;
    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 114
    invoke-virtual {p1, v3}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V

    .line 115
    invoke-static {}, Landroid/app/ActivityClient;->getInstance()Landroid/app/ActivityClient;

    move-result-object v3

    iget-object v4, v2, Lcom/android/server/wm/ActivityRecord;->token:Landroid/os/IBinder;

    invoke-virtual {v3, v4, v1}, Landroid/app/ActivityClient;->moveActivityTaskToBack(Landroid/os/IBinder;Z)Z

    goto :goto_0

    .line 117
    :cond_1
    invoke-virtual {p1, v3}, Lcom/android/server/wm/Task;->setAlwaysOnTop(Z)V

    .line 118
    invoke-virtual {p1, p1}, Lcom/android/server/wm/Task;->moveTaskToBack(Lcom/android/server/wm/Task;)Z

    .line 120
    :goto_0
    invoke-static {}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isDesktopActive()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {p1}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/android/server/wm/MiuiFreeFormManagerService;->autoLayoutOthersIfNeed(I)V

    .line 123
    :cond_2
    return-void
.end method

.method public freeFormAndFullScreenToggleByKeyCombination(Z)V
    .locals 6
    .param p1, "isStartFreeForm"    # Z

    .line 25
    const/4 v0, 0x0

    .line 26
    .local v0, "focusedTask":Lcom/android/server/wm/Task;
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    iget-object v1, v1, Lcom/android/server/wm/DisplayContent;->mFocusedApp:Lcom/android/server/wm/ActivityRecord;

    if-eqz v1, :cond_0

    .line 27
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mDisplayContent:Lcom/android/server/wm/DisplayContent;

    iget-object v1, v1, Lcom/android/server/wm/DisplayContent;->mFocusedApp:Lcom/android/server/wm/ActivityRecord;

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityRecord;->getTask()Lcom/android/server/wm/Task;

    move-result-object v0

    .line 30
    :cond_0
    const-string v1, "MiuiFreeFormKeyCombinationHelper"

    if-nez v0, :cond_1

    .line 31
    const-string v2, "no task focused, do noting"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    return-void

    .line 35
    :cond_1
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->isActivityTypeHomeOrRecents()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 36
    const-string/jumbo v2, "task isActivityTypeHomeOrRecents"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    return-void

    .line 40
    :cond_2
    new-instance v2, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda0;

    invoke-direct {v2}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper$$ExternalSyntheticLambda0;-><init>()V

    invoke-virtual {v0, v2}, Lcom/android/server/wm/Task;->getActivity(Ljava/util/function/Predicate;)Lcom/android/server/wm/ActivityRecord;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 41
    const-string/jumbo v2, "topFocusedStack is in animating"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    return-void

    .line 45
    :cond_3
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getWindowingMode()I

    move-result v2

    .line 46
    .local v2, "windowingMode":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "topFocusedStack.getRootTaskId()= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " islaunchFreeform= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " windowingMode= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mIsHandleKeyCombination= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z

    if-eqz v1, :cond_4

    .line 50
    return-void

    .line 52
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z

    .line 54
    const/4 v3, 0x0

    if-eqz p1, :cond_7

    if-ne v2, v1, :cond_7

    .line 55
    :try_start_0
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskResizeableForFreeform(I)Z

    move-result v4

    if-nez v4, :cond_6

    .line 56
    invoke-virtual {v0, v3, v1}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 57
    .local v1, "ar":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_5

    .line 58
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v5, v5, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 59
    invoke-direct {p0, v5, v1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->getActivityName(Landroid/content/Context;Lcom/android/server/wm/ActivityRecord;)Ljava/lang/String;

    move-result-object v5

    .line 58
    invoke-direct {p0, v4, v5}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->showNotSupportToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 61
    .end local v1    # "ar":Lcom/android/server/wm/ActivityRecord;
    :cond_5
    goto/16 :goto_4

    .line 62
    :cond_6
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->freeformFullscreenTask(I)V

    goto/16 :goto_4

    .line 85
    :catchall_0
    move-exception v1

    goto/16 :goto_2

    .line 82
    :catch_0
    move-exception v1

    goto/16 :goto_1

    .line 64
    :cond_7
    if-nez p1, :cond_8

    const/4 v4, 0x5

    if-ne v2, v4, :cond_8

    .line 65
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    .line 66
    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->getMiuiFreeFormActivityStack(I)Lcom/android/server/wm/MiuiFreeFormActivityStackStub;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/MiuiFreeFormActivityStack;

    .line 67
    .local v1, "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/android/server/wm/MiuiFreeFormActivityStack;->isInFreeFormMode()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 68
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fullscreenFreeformTask(I)V

    goto :goto_3

    .line 70
    .end local v1    # "mffas":Lcom/android/server/wm/MiuiFreeFormActivityStack;
    :cond_8
    if-eqz p1, :cond_b

    const/4 v4, 0x6

    if-ne v2, v4, :cond_b

    .line 71
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/Task;->getRootTaskId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/server/wm/ActivityTaskManagerService;->getTaskResizeableForFreeform(I)Z

    move-result v4

    if-nez v4, :cond_9

    .line 72
    invoke-virtual {v0, v3, v1}, Lcom/android/server/wm/Task;->getTopActivity(ZZ)Lcom/android/server/wm/ActivityRecord;

    move-result-object v1

    .line 73
    .local v1, "ar":Lcom/android/server/wm/ActivityRecord;
    if-eqz v1, :cond_a

    .line 74
    iget-object v4, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v4, v4, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v4, v4, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v5, v5, Lcom/android/server/wm/MiuiFreeFormGestureController;->mService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v5, v5, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    .line 75
    invoke-direct {p0, v5, v1}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->getActivityName(Landroid/content/Context;Lcom/android/server/wm/ActivityRecord;)Ljava/lang/String;

    move-result-object v5

    .line 74
    invoke-direct {p0, v4, v5}, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->showNotSupportToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    .end local v1    # "ar":Lcom/android/server/wm/ActivityRecord;
    :cond_9
    iget-object v1, v0, Lcom/android/server/wm/Task;->mTransitionController:Lcom/android/server/wm/TransitionController;

    invoke-virtual {v1}, Lcom/android/server/wm/TransitionController;->isCollecting()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, v0, Lcom/android/server/wm/Task;->mTransitionController:Lcom/android/server/wm/TransitionController;

    .line 78
    invoke-virtual {v1}, Lcom/android/server/wm/TransitionController;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_c

    .line 79
    iget-object v1, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v1, v1, Lcom/android/server/wm/MiuiFreeFormGestureController;->mMiuiFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget v4, v0, Lcom/android/server/wm/Task;->mTaskId:I

    invoke-virtual {v1, v4}, Lcom/android/server/wm/MiuiFreeFormManagerService;->fromSoscToFreeform(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 77
    :cond_a
    :goto_0
    goto :goto_4

    .line 83
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v1    # "e":Ljava/lang/Exception;
    goto :goto_4

    .line 85
    :goto_2
    iput-boolean v3, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z

    .line 86
    throw v1

    .line 70
    :cond_b
    :goto_3
    nop

    .line 85
    :cond_c
    :goto_4
    iput-boolean v3, p0, Lcom/android/server/wm/MiuiFreeFormKeyCombinationHelper;->mIsHandleKeyCombination:Z

    .line 86
    nop

    .line 87
    return-void
.end method
