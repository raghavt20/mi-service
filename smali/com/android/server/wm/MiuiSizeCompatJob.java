public class com.android.server.wm.MiuiSizeCompatJob extends android.app.job.JobService {
	 /* .source "MiuiSizeCompatJob.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/wm/MiuiSizeCompatJob$H; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final java.lang.String APP_PKG;
private static final java.lang.String EVENT_NAME;
private static final java.lang.String EVENT_STATUS_KEY;
private static final java.lang.String EVENT_STATUS_NAME_KEY;
private static final java.lang.String EVENT_STATUS_PACKAGE_KEY;
private static final java.lang.String EVENT_STATUS_STYLE_KEY;
private static final java.lang.String EVENT_STYLE_EMBEDED_VALUE;
private static final java.lang.String SERVER_CLASS_NAME;
private static final java.lang.String SERVER_PKG_NAME;
private static final Integer SIZE_COMPAT_JOB_ID;
private static final java.lang.String TAG;
private static final android.content.ComponentName mCName;
/* # instance fields */
private android.os.IBinder$DeathRecipient mDeathRecipient;
private android.os.Handler mH;
private com.miui.analytics.ITrackBinder mITrackBinder;
private android.content.ServiceConnection mServiceConnection;
private java.lang.Object mTrackLock;
/* # direct methods */
static android.os.IBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( com.android.server.wm.MiuiSizeCompatJob p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDeathRecipient;
} // .end method
static android.os.Handler -$$Nest$fgetmH ( com.android.server.wm.MiuiSizeCompatJob p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mH;
} // .end method
static com.miui.analytics.ITrackBinder -$$Nest$fgetmITrackBinder ( com.android.server.wm.MiuiSizeCompatJob p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mITrackBinder;
} // .end method
static java.lang.Object -$$Nest$fgetmTrackLock ( com.android.server.wm.MiuiSizeCompatJob p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mTrackLock;
} // .end method
static void -$$Nest$fputmITrackBinder ( com.android.server.wm.MiuiSizeCompatJob p0, com.miui.analytics.ITrackBinder p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mITrackBinder = p1;
	 return;
} // .end method
static void -$$Nest$mbindOneTrackService ( com.android.server.wm.MiuiSizeCompatJob p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->bindOneTrackService()V */
	 return;
} // .end method
static void -$$Nest$mtrackEvent ( com.android.server.wm.MiuiSizeCompatJob p0, android.app.job.JobParameters p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiSizeCompatJob;->trackEvent(Landroid/app/job/JobParameters;)V */
	 return;
} // .end method
static com.android.server.wm.MiuiSizeCompatJob ( ) {
	 /* .locals 3 */
	 /* .line 41 */
	 /* new-instance v0, Landroid/content/ComponentName; */
	 /* const-class v1, Lcom/android/server/wm/MiuiSizeCompatJob; */
	 (( java.lang.Class ) v1 ).getName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;
	 final String v2 = "android"; // const-string v2, "android"
	 /* invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
	 return;
} // .end method
public com.android.server.wm.MiuiSizeCompatJob ( ) {
	 /* .locals 1 */
	 /* .line 37 */
	 /* invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V */
	 /* .line 53 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mTrackLock = v0;
	 /* .line 101 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatJob$1; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiSizeCompatJob$1;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;)V */
	 this.mServiceConnection = v0;
	 /* .line 126 */
	 /* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatJob$2; */
	 /* invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiSizeCompatJob$2;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;)V */
	 this.mDeathRecipient = v0;
	 return;
} // .end method
private void bindOneTrackService ( ) {
	 /* .locals 5 */
	 /* .line 144 */
	 v0 = this.mTrackLock;
	 /* monitor-enter v0 */
	 /* .line 145 */
	 try { // :try_start_0
		 v1 = this.mITrackBinder;
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 146 */
			 final String v1 = "MiuiSizeCompatJob"; // const-string v1, "MiuiSizeCompatJob"
			 final String v2 = "Already bound."; // const-string v2, "Already bound."
			 android.util.Slog .d ( v1,v2 );
			 /* .line 147 */
			 /* monitor-exit v0 */
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 return;
			 /* .line 150 */
		 } // :cond_0
		 try { // :try_start_1
			 /* new-instance v1, Landroid/content/Intent; */
			 /* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
			 /* .line 151 */
			 /* .local v1, "intent":Landroid/content/Intent; */
			 final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
			 final String v3 = "com.miui.analytics.onetrack.TrackService"; // const-string v3, "com.miui.analytics.onetrack.TrackService"
			 (( android.content.Intent ) v1 ).setClassName ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
			 /* .line 152 */
			 v2 = this.mServiceConnection;
			 v3 = android.os.UserHandle.CURRENT;
			 int v4 = 1; // const/4 v4, 0x1
			 (( com.android.server.wm.MiuiSizeCompatJob ) p0 ).bindServiceAsUser ( v1, v2, v4, v3 ); // invoke-virtual {p0, v1, v2, v4, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
			 /* :try_end_1 */
			 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
			 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
			 /* .line 155 */
		 } // .end local v1 # "intent":Landroid/content/Intent;
		 /* .line 153 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 154 */
		 /* .local v1, "e":Ljava/lang/Exception; */
		 try { // :try_start_2
			 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
			 /* .line 156 */
		 } // .end local v1 # "e":Ljava/lang/Exception;
	 } // :goto_0
	 /* monitor-exit v0 */
	 /* .line 157 */
	 return;
	 /* .line 156 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_2 */
	 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
	 /* throw v1 */
} // .end method
private java.util.Map getEmbeddedApps ( ) {
	 /* .locals 1 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/String;", */
	 /* "Ljava/lang/Boolean;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 246 */
int v0 = 0; // const/4 v0, 0x0
/* .line 254 */
/* .local v0, "embeddedApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;" */
} // .end method
public static void sheduleJob ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 65 */
final String v0 = "jobscheduler"; // const-string v0, "jobscheduler"
(( android.content.Context ) p0 ).getSystemService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/job/JobScheduler; */
/* .line 66 */
/* .local v0, "js":Landroid/app/job/JobScheduler; */
/* new-instance v1, Landroid/app/job/JobInfo$Builder; */
/* const v2, 0x82f5 */
v3 = com.android.server.wm.MiuiSizeCompatJob.mCName;
/* invoke-direct {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V */
/* .line 67 */
int v2 = 2; // const/4 v2, 0x2
(( android.app.job.JobInfo$Builder ) v1 ).setRequiredNetworkType ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;
/* .line 68 */
/* const-wide/32 v2, 0x5265c00 */
(( android.app.job.JobInfo$Builder ) v1 ).setPeriodic ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/app/job/JobInfo$Builder;->setPeriodic(J)Landroid/app/job/JobInfo$Builder;
/* .line 69 */
(( android.app.job.JobInfo$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;
/* .line 70 */
/* .local v1, "jobInfo":Landroid/app/job/JobInfo; */
(( android.app.job.JobScheduler ) v0 ).schedule ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
/* .line 71 */
return;
} // .end method
private void trackEvent ( android.app.job.JobParameters p0 ) {
/* .locals 13 */
/* .param p1, "parameters" # Landroid/app/job/JobParameters; */
/* .line 196 */
v0 = this.mTrackLock;
/* monitor-enter v0 */
/* .line 197 */
try { // :try_start_0
	 android.os.SystemClock .uptimeMillis ( );
	 /* move-result-wide v1 */
	 /* .line 198 */
	 /* .local v1, "start":J */
	 /* sget-boolean v3, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
		 return;
		 /* .line 200 */
	 } // :cond_0
	 int v3 = 0; // const/4 v3, 0x0
	 try { // :try_start_1
		 android.sizecompat.MiuiSizeCompatManager .getMiuiSizeCompatInstalledApps ( );
		 /* .line 201 */
		 /* .local v4, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;" */
		 /* invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->getEmbeddedApps()Ljava/util/Map; */
		 /* .line 202 */
		 /* .local v5, "embeddedApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;" */
		 v6 = this.mITrackBinder;
		 if ( v6 != null) { // if-eqz v6, :cond_2
			 if ( v4 != null) { // if-eqz v4, :cond_2
				 /* .line 203 */
				 /* new-instance v6, Lorg/json/JSONObject; */
				 /* invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V */
				 /* .line 204 */
				 /* .local v6, "object":Lorg/json/JSONObject; */
				 /* new-instance v7, Lorg/json/JSONArray; */
				 /* invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V */
				 /* .line 205 */
				 /* .local v7, "array":Lorg/json/JSONArray; */
				 /* new-instance v8, Lcom/android/server/wm/MiuiSizeCompatJob$3; */
				 /* invoke-direct {v8, p0, v5, v7}, Lcom/android/server/wm/MiuiSizeCompatJob$3;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;Ljava/util/Map;Lorg/json/JSONArray;)V */
				 /* .line 224 */
				 final String v8 = "EVENT_NAME"; // const-string v8, "EVENT_NAME"
				 /* const-string/jumbo v9, "status" */
				 (( org.json.JSONObject ) v6 ).put ( v8, v9 ); // invoke-virtual {v6, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
				 /* .line 225 */
				 final String v8 = "app_display_status"; // const-string v8, "app_display_status"
				 (( org.json.JSONObject ) v6 ).put ( v8, v7 ); // invoke-virtual {v6, v8, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
				 /* .line 226 */
				 v8 = this.mITrackBinder;
				 final String v9 = "31000000779"; // const-string v9, "31000000779"
				 final String v10 = "android"; // const-string v10, "android"
				 (( org.json.JSONObject ) v6 ).toString ( ); // invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
				 /* .line 227 */
				 android.os.SystemClock .uptimeMillis ( );
				 /* move-result-wide v8 */
				 /* sub-long/2addr v8, v1 */
				 /* .line 228 */
				 /* .local v8, "took":J */
				 /* sget-boolean v10, Lcom/android/server/wm/MiuiSizeCompatService;->DEBUG:Z */
				 if ( v10 != null) { // if-eqz v10, :cond_1
					 /* .line 229 */
					 final String v10 = "MiuiSizeCompatJob"; // const-string v10, "MiuiSizeCompatJob"
					 (( org.json.JSONArray ) v7 ).toString ( ); // invoke-virtual {v7}, Lorg/json/JSONArray;->toString()Ljava/lang/String;
					 android.util.Log .d ( v10,v11 );
					 /* .line 231 */
				 } // :cond_1
				 final String v10 = "MiuiSizeCompatJob"; // const-string v10, "MiuiSizeCompatJob"
				 /* new-instance v11, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v12 = "Track "; // const-string v12, "Track "
				 (( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v12 = 				 (( org.json.JSONArray ) v7 ).length ( ); // invoke-virtual {v7}, Lorg/json/JSONArray;->length()I
				 (( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 final String v12 = " events took "; // const-string v12, " events took "
				 (( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v11 ).append ( v8, v9 ); // invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
				 final String v12 = " ms."; // const-string v12, " ms."
				 (( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Log .d ( v10,v11 );
				 /* :try_end_1 */
				 /* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
				 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
				 /* .line 236 */
			 } // .end local v4 # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/sizecompat/AspectRatioInfo;>;"
		 } // .end local v5 # "embeddedApps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
	 } // .end local v6 # "object":Lorg/json/JSONObject;
} // .end local v7 # "array":Lorg/json/JSONArray;
} // .end local v8 # "took":J
} // :cond_2
try { // :try_start_2
(( com.android.server.wm.MiuiSizeCompatJob ) p0 ).jobFinished ( p1, v3 ); // invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->jobFinished(Landroid/app/job/JobParameters;Z)V
/* .line 237 */
final String v3 = "MiuiSizeCompatJob"; // const-string v3, "MiuiSizeCompatJob"
final String v4 = "Job finished"; // const-string v4, "Job finished"
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 236 */
/* :catchall_0 */
/* move-exception v4 */
/* .line 233 */
/* :catch_0 */
/* move-exception v4 */
/* .line 234 */
/* .local v4, "e":Ljava/lang/Exception; */
try { // :try_start_3
(( java.lang.Exception ) v4 ).printStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 236 */
} // .end local v4 # "e":Ljava/lang/Exception;
try { // :try_start_4
(( com.android.server.wm.MiuiSizeCompatJob ) p0 ).jobFinished ( p1, v3 ); // invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->jobFinished(Landroid/app/job/JobParameters;Z)V
/* .line 237 */
final String v3 = "MiuiSizeCompatJob"; // const-string v3, "MiuiSizeCompatJob"
final String v4 = "Job finished"; // const-string v4, "Job finished"
} // :goto_0
android.util.Log .d ( v3,v4 );
/* .line 238 */
/* nop */
/* .line 239 */
} // .end local v1 # "start":J
/* monitor-exit v0 */
/* .line 240 */
return;
/* .line 236 */
/* .restart local v1 # "start":J */
} // :goto_1
(( com.android.server.wm.MiuiSizeCompatJob ) p0 ).jobFinished ( p1, v3 ); // invoke-virtual {p0, p1, v3}, Lcom/android/server/wm/MiuiSizeCompatJob;->jobFinished(Landroid/app/job/JobParameters;Z)V
/* .line 237 */
final String v3 = "MiuiSizeCompatJob"; // const-string v3, "MiuiSizeCompatJob"
final String v5 = "Job finished"; // const-string v5, "Job finished"
android.util.Log .d ( v3,v5 );
/* .line 238 */
/* nop */
} // .end local p0 # "this":Lcom/android/server/wm/MiuiSizeCompatJob;
} // .end local p1 # "parameters":Landroid/app/job/JobParameters;
/* throw v4 */
/* .line 239 */
} // .end local v1 # "start":J
/* .restart local p0 # "this":Lcom/android/server/wm/MiuiSizeCompatJob; */
/* .restart local p1 # "parameters":Landroid/app/job/JobParameters; */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v1 */
} // .end method
/* # virtual methods */
public void onCreate ( ) {
/* .locals 2 */
/* .line 58 */
final String v0 = "MiuiSizeCompatJob"; // const-string v0, "MiuiSizeCompatJob"
final String v1 = "onCreate"; // const-string v1, "onCreate"
android.util.Log .d ( v0,v1 );
/* .line 59 */
/* new-instance v0, Lcom/android/server/wm/MiuiSizeCompatJob$H; */
com.android.server.MiuiBgThread .get ( );
(( com.android.server.MiuiBgThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/android/server/wm/MiuiSizeCompatJob$H;-><init>(Lcom/android/server/wm/MiuiSizeCompatJob;Landroid/os/Looper;)V */
this.mH = v0;
/* .line 60 */
/* invoke-direct {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->bindOneTrackService()V */
/* .line 61 */
return;
} // .end method
public void onDestroy ( ) {
/* .locals 2 */
/* .line 259 */
/* invoke-super {p0}, Landroid/app/job/JobService;->onDestroy()V */
/* .line 260 */
final String v0 = "MiuiSizeCompatJob"; // const-string v0, "MiuiSizeCompatJob"
final String v1 = "onDestroy"; // const-string v1, "onDestroy"
android.util.Log .d ( v0,v1 );
/* .line 261 */
(( com.android.server.wm.MiuiSizeCompatJob ) p0 ).unBindOneTrackService ( ); // invoke-virtual {p0}, Lcom/android/server/wm/MiuiSizeCompatJob;->unBindOneTrackService()V
/* .line 262 */
return;
} // .end method
public Boolean onStartJob ( android.app.job.JobParameters p0 ) {
/* .locals 5 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 177 */
v0 = (( android.app.job.JobParameters ) p1 ).getJobId ( ); // invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I
/* const v1, 0x82f5 */
/* if-ne v0, v1, :cond_0 */
/* .line 178 */
final String v0 = "MiuiSizeCompatJob"; // const-string v0, "MiuiSizeCompatJob"
final String v1 = "Start job!"; // const-string v1, "Start job!"
android.util.Log .d ( v0,v1 );
/* .line 179 */
v0 = this.mH;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 180 */
v0 = this.mH;
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* const-wide/16 v3, 0x3e8 */
(( android.os.Handler ) v0 ).sendMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 181 */
/* .line 183 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean onStopJob ( android.app.job.JobParameters p0 ) {
/* .locals 2 */
/* .param p1, "params" # Landroid/app/job/JobParameters; */
/* .line 188 */
v0 = (( android.app.job.JobParameters ) p1 ).getJobId ( ); // invoke-virtual {p1}, Landroid/app/job/JobParameters;->getJobId()I
/* const v1, 0x82f5 */
/* if-ne v0, v1, :cond_0 */
/* .line 189 */
final String v0 = "MiuiSizeCompatJob"; // const-string v0, "MiuiSizeCompatJob"
final String v1 = "Stop job!"; // const-string v1, "Stop job!"
android.util.Log .d ( v0,v1 );
/* .line 191 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void unBindOneTrackService ( ) {
/* .locals 5 */
/* .line 160 */
v0 = this.mTrackLock;
/* monitor-enter v0 */
/* .line 161 */
try { // :try_start_0
v1 = this.mITrackBinder;
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 162 */
v3 = this.mDeathRecipient;
int v4 = 0; // const/4 v4, 0x0
/* .line 163 */
this.mITrackBinder = v2;
/* .line 164 */
final String v1 = "MiuiSizeCompatJob"; // const-string v1, "MiuiSizeCompatJob"
/* const-string/jumbo v3, "unbind OneTrack Service success." */
android.util.Slog .d ( v1,v3 );
/* .line 166 */
} // :cond_0
final String v1 = "MiuiSizeCompatJob"; // const-string v1, "MiuiSizeCompatJob"
/* const-string/jumbo v3, "unbind OneTrack Service fail." */
android.util.Slog .d ( v1,v3 );
/* .line 168 */
} // :goto_0
v1 = this.mServiceConnection;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 169 */
(( com.android.server.wm.MiuiSizeCompatJob ) p0 ).unbindService ( v1 ); // invoke-virtual {p0, v1}, Lcom/android/server/wm/MiuiSizeCompatJob;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 170 */
this.mServiceConnection = v2;
/* .line 172 */
} // :cond_1
/* monitor-exit v0 */
/* .line 173 */
return;
/* .line 172 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
