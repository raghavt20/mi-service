class com.android.server.wm.MiuiFreezeImpl$ShowDialogRunnable implements java.lang.Runnable {
	 /* .source "MiuiFreezeImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/MiuiFreezeImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "ShowDialogRunnable" */
} // .end annotation
/* # instance fields */
final com.android.server.wm.MiuiFreezeImpl this$0; //synthetic
/* # direct methods */
private com.android.server.wm.MiuiFreezeImpl$ShowDialogRunnable ( ) {
/* .locals 0 */
/* .line 482 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.android.server.wm.MiuiFreezeImpl$ShowDialogRunnable ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/MiuiFreezeImpl$ShowDialogRunnable;-><init>(Lcom/android/server/wm/MiuiFreezeImpl;)V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 486 */
v0 = this.this$0;
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fgetmLoadingDialog ( v0 );
final String v1 = "MiuiFreezeImpl"; // const-string v1, "MiuiFreezeImpl"
/* if-nez v0, :cond_1 */
v0 = this.this$0;
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fgetmReceiver ( v0 );
/* if-nez v0, :cond_0 */
/* .line 490 */
} // :cond_0
v0 = this.this$0;
/* new-instance v2, Lcom/android/server/wm/MiuiLoadingDialog; */
v3 = this.this$0;
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fgetmUiContext ( v3 );
/* invoke-direct {v2, v3}, Lcom/android/server/wm/MiuiLoadingDialog;-><init>(Landroid/content/Context;)V */
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fputmLoadingDialog ( v0,v2 );
/* .line 491 */
v0 = this.this$0;
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fgetmLoadingDialog ( v0 );
v2 = this.this$0;
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fgetdialogShowListener ( v2 );
(( com.android.server.wm.MiuiLoadingDialog ) v0 ).setOnShowAnimListener ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/MiuiLoadingDialog;->setOnShowAnimListener(Lmiuix/appcompat/app/AlertDialog$OnDialogShowAnimListener;)V
/* .line 492 */
v0 = this.this$0;
com.android.server.wm.MiuiFreezeImpl .-$$Nest$fgetmLoadingDialog ( v0 );
(( com.android.server.wm.MiuiLoadingDialog ) v0 ).show ( ); // invoke-virtual {v0}, Lcom/android/server/wm/MiuiLoadingDialog;->show()V
/* .line 493 */
/* const-string/jumbo v0, "start show dialog" */
android.util.Log .d ( v1,v0 );
/* .line 494 */
return;
/* .line 487 */
} // :cond_1
} // :goto_0
final String v0 = "dialog is showing or mReceiver is null "; // const-string v0, "dialog is showing or mReceiver is null "
android.util.Log .i ( v1,v0 );
/* .line 488 */
return;
} // .end method
