.class public Lcom/android/server/wm/MiuiMultiWindowRecommendController;
.super Ljava/lang/Object;
.source "MiuiMultiWindowRecommendController.java"


# static fields
.field private static final MULTI_WINDOW_RECOMMEND_RADIUS:I

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_COLOR:[F

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_OFFSETY:F = 25.0f

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_OUTSET:F = 15.0f

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_RADIUS:F = 350.0f

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_RESET_COLOR:[F

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_V2_COLOR:I = 0x73000000

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_V2_DISPERSION:F = 1.0f

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_V2_OFFSET_X:F = 0.0f

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_V2_OFFSET_Y:F = 20.0f

.field public static final MULTI_WINDOW_RECOMMEND_SHADOW_V2_RADIUS:F = 350.0f

.field private static final TAG:Ljava/lang/String; = "MiuiMultiWindowRecommendController"


# instance fields
.field private dismissFreeFormRecommendViewRunnable:Ljava/lang/Runnable;

.field private dismissSplitScreenRecommendViewRunnable:Ljava/lang/Runnable;

.field freeFormClickListener:Landroid/view/View$OnClickListener;

.field private mAnimationDelay:J

.field private mContext:Landroid/content/Context;

.field private mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

.field private mFreeFormRecommendIconContainer:Landroid/widget/RelativeLayout;

.field private mFreeFormRecommendLayout:Lcom/android/server/wm/FreeFormRecommendLayout;

.field private mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mRecommendAutoDisapperTime:J

.field mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

.field mService:Lcom/android/server/wm/WindowManagerService;

.field private mShowHideAnimConfig:Lmiuix/animation/base/AnimConfig;

.field private mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

.field private mSplitScreenRecommendIconContainer:Landroid/widget/RelativeLayout;

.field private mSplitScreenRecommendLayout:Lcom/android/server/wm/SplitScreenRecommendLayout;

.field mWindowManager:Landroid/view/WindowManager;

.field removeFreeFormRecomendRunnable:Ljava/lang/Runnable;

.field removeSplitScreenRecommendRunnable:Ljava/lang/Runnable;

.field splitScreenClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/FreeFormRecommendLayout;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendLayout:Lcom/android/server/wm/FreeFormRecommendLayout;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Landroid/view/WindowManager$LayoutParams;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/RecommendDataEntry;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)Lcom/android/server/wm/SplitScreenRecommendLayout;
    .locals 0

    iget-object p0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSplitScreenRecommendLayout:Lcom/android/server/wm/SplitScreenRecommendLayout;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmFreeFormRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFreeFormRecommendIconContainer(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/widget/RelativeLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendIconContainer:Landroid/widget/RelativeLayout;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFreeFormRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/FreeFormRecommendLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendLayout:Lcom/android/server/wm/FreeFormRecommendLayout;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLayoutParams(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/view/WindowManager$LayoutParams;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSpiltScreenRecommendDataEntry(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSplitScreenRecommendIconContainer(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Landroid/widget/RelativeLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSplitScreenRecommendIconContainer:Landroid/widget/RelativeLayout;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSplitScreenRecommendLayout(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/SplitScreenRecommendLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSplitScreenRecommendLayout:Lcom/android/server/wm/SplitScreenRecommendLayout;

    return-void
.end method

.method static bridge synthetic -$$Nest$menterSmallFreeForm(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->enterSmallFreeForm()V

    return-void
.end method

.method static bridge synthetic -$$Nest$menterSplitScreen(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->enterSplitScreen()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 58
    nop

    .line 60
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 59
    const/4 v1, 0x1

    const/high16 v2, 0x41900000    # 18.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->MULTI_WINDOW_RECOMMEND_RADIUS:I

    .line 65
    const/4 v0, 0x4

    new-array v1, v0, [F

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->MULTI_WINDOW_RECOMMEND_SHADOW_RESET_COLOR:[F

    .line 66
    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->MULTI_WINDOW_RECOMMEND_SHADOW_COLOR:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x3ecccccd    # 0.4f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/android/server/wm/WindowManagerService;
    .param p3, "miuiMultiWindowRecommendHelper"    # Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 51
    new-instance v0, Lcom/android/server/wm/RecommendDataEntry;

    invoke-direct {v0}, Lcom/android/server/wm/RecommendDataEntry;-><init>()V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 55
    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendAutoDisapperTime:J

    .line 56
    const-wide/16 v0, 0x47e

    iput-wide v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mAnimationDelay:J

    .line 84
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$1;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$1;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->splitScreenClickListener:Landroid/view/View$OnClickListener;

    .line 93
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$2;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$2;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendRunnable:Ljava/lang/Runnable;

    .line 105
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$3;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$3;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecomendRunnable:Ljava/lang/Runnable;

    .line 148
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$4;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$4;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->dismissSplitScreenRecommendViewRunnable:Ljava/lang/Runnable;

    .line 163
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$5;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$5;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->dismissFreeFormRecommendViewRunnable:Ljava/lang/Runnable;

    .line 282
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$8;

    invoke-direct {v0, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$8;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->freeFormClickListener:Landroid/view/View$OnClickListener;

    .line 75
    if-eqz p1, :cond_0

    .line 76
    iput-object p1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mContext:Landroid/content/Context;

    .line 77
    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mWindowManager:Landroid/view/WindowManager;

    .line 78
    iput-object p2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    .line 79
    iput-object p3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    .line 80
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->initFolmeConfig()V

    .line 82
    :cond_0
    return-void
.end method

.method private enterSmallFreeForm()V
    .locals 5

    .line 134
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, "enterSmallFreeForm "

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 138
    invoke-virtual {v1}, Lcom/android/server/wm/RecommendDataEntry;->getFreeFormTaskId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    const-string v4, "enterSmallFreeFormByFreeFormRecommend"

    filled-new-array {v1, v2, v4, v3}, [Ljava/lang/Object;

    move-result-object v1

    .line 136
    const-string v2, "launchMiniFreeFormWindowVersion2"

    invoke-static {v0, v2, v1}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 142
    invoke-virtual {v1}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 143
    invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getFreeformPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 142
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackFreeFormRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    return-void
.end method

.method private enterSplitScreen()V
    .locals 5

    .line 118
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, "enterSplitScreen "

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mTaskOrganizerController:Lcom/android/server/wm/TaskOrganizerController;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 122
    invoke-virtual {v1}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryTaskId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryTaskId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v1, v2}, [Ljava/lang/Object;

    move-result-object v1

    .line 120
    const-string/jumbo v2, "startTasksForSystem"

    invoke-static {v0, v2, v1}, Landroid/util/MiuiMultiWindowUtils;->invoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendHelper:Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;

    iget-object v0, v0, Lcom/android/server/wm/MiuiMultiWindowRecommendHelper;->mFreeFormManagerService:Lcom/android/server/wm/MiuiFreeFormManagerService;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormManagerService;->mFreeFormGestureController:Lcom/android/server/wm/MiuiFreeFormGestureController;

    iget-object v0, v0, Lcom/android/server/wm/MiuiFreeFormGestureController;->mTrackManager:Lcom/android/server/wm/MiuiFreeformTrackManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 125
    invoke-virtual {v2}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 126
    invoke-virtual {v3}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 127
    invoke-virtual {v4}, Lcom/android/server/wm/RecommendDataEntry;->getPrimaryPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    .line 128
    invoke-virtual {v3}, Lcom/android/server/wm/RecommendDataEntry;->getSecondaryPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->getApplicationName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-virtual {v0, v1, v2}, Lcom/android/server/wm/MiuiFreeformTrackManager;->trackSplitScreenRecommendClickEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    return-void
.end method

.method public static getDisplayCutoutHeight(Lcom/android/server/wm/DisplayFrames;)I
    .locals 4
    .param p0, "displayFrames"    # Lcom/android/server/wm/DisplayFrames;

    .line 517
    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 518
    :cond_0
    const/4 v0, 0x0

    .line 519
    .local v0, "displayCutoutHeight":I
    iget-object v1, p0, Lcom/android/server/wm/DisplayFrames;->mInsetsState:Landroid/view/InsetsState;

    invoke-virtual {v1}, Landroid/view/InsetsState;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v1

    .line 520
    .local v1, "cutout":Landroid/view/DisplayCutout;
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    if-nez v2, :cond_1

    .line 521
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetTop()I

    move-result v0

    goto :goto_0

    .line 522
    :cond_1
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 523
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetBottom()I

    move-result v0

    goto :goto_0

    .line 524
    :cond_2
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 525
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetLeft()I

    move-result v0

    goto :goto_0

    .line 526
    :cond_3
    iget v2, p0, Lcom/android/server/wm/DisplayFrames;->mRotation:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 527
    invoke-virtual {v1}, Landroid/view/DisplayCutout;->getSafeInsetRight()I

    move-result v0

    .line 529
    :cond_4
    :goto_0
    return v0
.end method

.method public static getStatusBarHeight(Lcom/android/server/wm/InsetsStateController;Z)I
    .locals 7
    .param p0, "insetsStateController"    # Lcom/android/server/wm/InsetsStateController;
    .param p1, "ignoreVisibility"    # Z

    .line 533
    if-nez p0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 534
    :cond_0
    const/4 v0, 0x0

    .line 535
    .local v0, "statusBarHeight":I
    invoke-virtual {p0}, Lcom/android/server/wm/InsetsStateController;->getSourceProviders()Landroid/util/SparseArray;

    move-result-object v1

    .line 536
    .local v1, "providers":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/android/server/wm/InsetsSourceProvider;>;"
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_4

    .line 537
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/wm/InsetsSourceProvider;

    .line 538
    .local v3, "provider":Lcom/android/server/wm/InsetsSourceProvider;
    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->getType()I

    move-result v4

    invoke-static {}, Landroid/view/WindowInsets$Type;->statusBars()I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 539
    goto :goto_1

    .line 541
    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 542
    :cond_2
    invoke-virtual {v3}, Lcom/android/server/wm/InsetsSourceProvider;->getSource()Landroid/view/InsetsSource;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/InsetsSource;->getFrame()Landroid/graphics/Rect;

    move-result-object v4

    .line 543
    .local v4, "frame":Landroid/graphics/Rect;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 544
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 536
    .end local v3    # "provider":Lcom/android/server/wm/InsetsSourceProvider;
    .end local v4    # "frame":Landroid/graphics/Rect;
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 549
    .end local v2    # "i":I
    :cond_4
    return v0
.end method

.method public static loadDrawableByPackageName(Ljava/lang/String;Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userId"    # I

    .line 179
    const/4 v0, 0x0

    .line 181
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 182
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, p0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {p2}, Lmiui/os/UserHandleEx;->getUserHandle(I)Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    .line 185
    .end local v1    # "packageManager":Landroid/content/pm/PackageManager;
    goto :goto_0

    .line 183
    :catch_0
    move-exception v1

    .line 184
    .local v1, "unused":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not found application by pkgName "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiMultiWindowRecommendController"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    .end local v1    # "unused":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    return-object v0
.end method

.method private setRadius(Landroid/view/View;F)V
    .locals 1
    .param p1, "content"    # Landroid/view/View;
    .param p2, "radius"    # F

    .line 458
    new-instance v0, Lcom/android/server/wm/MiuiMultiWindowRecommendController$12;

    invoke-direct {v0, p0, p2}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$12;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 464
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setClipToOutline(Z)V

    .line 465
    return-void
.end method


# virtual methods
.method public addFreeFormRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 2
    .param p1, "recommendDataEntry"    # Lcom/android/server/wm/RecommendDataEntry;

    .line 241
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, " addFreeFormRecommendView start"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$7;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z

    .line 279
    return-void
.end method

.method public addSplitScreenRecommendView(Lcom/android/server/wm/RecommendDataEntry;)V
    .locals 4
    .param p1, "recommendDataEntry"    # Lcom/android/server/wm/RecommendDataEntry;

    .line 190
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, " addSplitScreenRecommendView start"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;

    invoke-direct {v1, p0, p1}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$6;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;Lcom/android/server/wm/RecommendDataEntry;)V

    iget-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mAnimationDelay:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 238
    return-void
.end method

.method public getApplicationName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 501
    const/4 v0, 0x0

    .line 502
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 504
    .local v1, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v2, v2, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v2, v2, Lcom/android/server/wm/ActivityTaskManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    move-object v0, v2

    .line 505
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 508
    goto :goto_0

    .line 506
    :catch_0
    move-exception v2

    .line 507
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 509
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    const-string v2, ""

    .line 510
    .local v2, "applicationName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 511
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    move-object v2, v3

    check-cast v2, Ljava/lang/String;

    .line 513
    :cond_0
    return-object v2
.end method

.method public getSplitScreenRecommendLayout()Lcom/android/server/wm/SplitScreenRecommendLayout;
    .locals 1

    .line 454
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSplitScreenRecommendLayout:Lcom/android/server/wm/SplitScreenRecommendLayout;

    return-object v0
.end method

.method public inFreeFormRecommendState()Z
    .locals 1

    .line 483
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    if-eqz v0, :cond_0

    .line 484
    invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getFreeFormRecommendState()Z

    move-result v0

    return v0

    .line 486
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public inSplitScreenRecommendState()Z
    .locals 1

    .line 468
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {v0}, Lcom/android/server/wm/RecommendDataEntry;->getSplitScreenRecommendState()Z

    move-result v0

    return v0

    .line 471
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public initFolmeConfig()V
    .locals 4

    .line 304
    new-instance v0, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiuix/animation/base/AnimConfig;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/4 v2, -0x2

    invoke-virtual {v0, v2, v1}, Lmiuix/animation/base/AnimConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lmiuix/animation/listener/TransitionListener;

    new-instance v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;

    invoke-direct {v2, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$9;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 305
    invoke-virtual {v0, v1}, Lmiuix/animation/base/AnimConfig;->addListeners([Lmiuix/animation/listener/TransitionListener;)Lmiuix/animation/base/AnimConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mShowHideAnimConfig:Lmiuix/animation/base/AnimConfig;

    .line 334
    return-void

    :array_0
    .array-data 4
        0x3f400000    # 0.75f
        0x3eb33333    # 0.35f
    .end array-data
.end method

.method public removeFreeFormRecommendView()V
    .locals 2

    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " removeFreeFormRecommendView mFreeFormRecommendLayout= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendLayout:Lcom/android/server/wm/FreeFormRecommendLayout;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$11;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$11;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z

    .line 451
    return-void
.end method

.method public removeFreeFormRecommendViewForTimer()V
    .locals 4

    .line 298
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, "removeFreeFormRecommendViewForTimer"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->dismissFreeFormRecommendViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 300
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->dismissFreeFormRecommendViewRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendAutoDisapperTime:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 301
    return-void
.end method

.method public removeRecommendView()V
    .locals 0

    .line 496
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeFreeFormRecommendView()V

    .line 497
    invoke-virtual {p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->removeSplitScreenRecommendView()V

    .line 498
    return-void
.end method

.method public removeSplitScreenRecommendView()V
    .locals 2

    .line 410
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " removeSplitScreenRecommendView mSplitScreenRecommendLayout= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSplitScreenRecommendLayout:Lcom/android/server/wm/SplitScreenRecommendLayout;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    new-instance v1, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;

    invoke-direct {v1, p0}, Lcom/android/server/wm/MiuiMultiWindowRecommendController$10;-><init>(Lcom/android/server/wm/MiuiMultiWindowRecommendController;)V

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->post(Ljava/lang/Runnable;)Z

    .line 429
    return-void
.end method

.method public removeSplitScreenRecommendViewForTimer()V
    .locals 4

    .line 292
    const-string v0, "MiuiMultiWindowRecommendController"

    const-string v1, "removeSplitScreenRecommendViewForTimer"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->dismissSplitScreenRecommendViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 294
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mService:Lcom/android/server/wm/WindowManagerService;

    iget-object v0, v0, Lcom/android/server/wm/WindowManagerService;->mAtmService:Lcom/android/server/wm/ActivityTaskManagerService;

    iget-object v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mUiHandler:Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->dismissSplitScreenRecommendViewRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mRecommendAutoDisapperTime:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/wm/ActivityTaskManagerService$UiHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 295
    return-void
.end method

.method public resetShadow(Landroid/view/View;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;

    .line 358
    if-nez p1, :cond_0

    .line 359
    return-void

    .line 361
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v0

    .line 362
    .local v0, "viewRootImpl":Landroid/view/ViewRootImpl;
    if-eqz v0, :cond_2

    .line 363
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " resetShadow: view= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMultiWindowRecommendController"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    invoke-static {}, Lcom/android/server/wm/RecommendUtils;->isSupportMiuiShadowV2()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 365
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/android/server/wm/RecommendUtils;->resetMiShadowV2(Landroid/view/SurfaceControl;Landroid/content/Context;)V

    goto :goto_0

    .line 367
    :cond_1
    nop

    .line 368
    invoke-virtual {v0}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->MULTI_WINDOW_RECOMMEND_SHADOW_RESET_COLOR:[F

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 367
    invoke-static/range {v3 .. v10}, Lcom/android/server/wm/RecommendUtils;->resetShadowSettingsInTransactionForSurfaceControl(Landroid/view/SurfaceControl;IF[FFFFI)V

    .line 372
    :cond_2
    :goto_0
    return-void
.end method

.method public setCornerRadiusAndShadow(Landroid/view/View;)V
    .locals 20
    .param p1, "view"    # Landroid/view/View;

    .line 337
    move-object/from16 v0, p1

    if-nez v0, :cond_0

    .line 338
    return-void

    .line 340
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v1

    .line 341
    .local v1, "viewRootImpl":Landroid/view/ViewRootImpl;
    if-eqz v1, :cond_2

    .line 342
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " setCornerRadiusAndShadow: view= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiMultiWindowRecommendController"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-static {}, Lcom/android/server/wm/RecommendUtils;->isSupportMiuiShadowV2()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 344
    invoke-virtual {v1}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v3

    const/high16 v4, 0x73000000

    const/4 v5, 0x0

    const/high16 v6, 0x41a00000    # 20.0f

    const/high16 v7, 0x43af0000    # 350.0f

    const/high16 v8, 0x3f800000    # 1.0f

    sget v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->MULTI_WINDOW_RECOMMEND_RADIUS:I

    int-to-float v9, v2

    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v2, p0

    iget-object v11, v2, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mContext:Landroid/content/Context;

    invoke-static/range {v3 .. v11}, Lcom/android/server/wm/RecommendUtils;->setMiShadowV2(Landroid/view/SurfaceControl;IFFFFFLandroid/graphics/RectF;Landroid/content/Context;)V

    goto :goto_0

    .line 349
    :cond_1
    move-object/from16 v2, p0

    .line 350
    invoke-virtual {v1}, Landroid/view/ViewRootImpl;->getSurfaceControl()Landroid/view/SurfaceControl;

    move-result-object v12

    const/4 v13, 0x1

    const/high16 v14, 0x43af0000    # 350.0f

    sget-object v15, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->MULTI_WINDOW_RECOMMEND_SHADOW_COLOR:[F

    const/16 v16, 0x0

    const/high16 v17, 0x41c80000    # 25.0f

    const/high16 v18, 0x41700000    # 15.0f

    const/16 v19, 0x1

    .line 349
    invoke-static/range {v12 .. v19}, Lcom/android/server/wm/RecommendUtils;->setShadowSettingsInTransactionForSurfaceControl(Landroid/view/SurfaceControl;IF[FFFFI)V

    goto :goto_0

    .line 341
    :cond_2
    move-object/from16 v2, p0

    .line 355
    :goto_0
    return-void
.end method

.method public setFreeFormRecommendState(Z)V
    .locals 1
    .param p1, "inRecommend"    # Z

    .line 490
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mFreeFormRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    if-eqz v0, :cond_0

    .line 491
    invoke-virtual {v0, p1}, Lcom/android/server/wm/RecommendDataEntry;->setFreeFormRecommendState(Z)V

    .line 493
    :cond_0
    return-void
.end method

.method public setSplitScreenRecommendState(Z)V
    .locals 2
    .param p1, "inRecommend"    # Z

    .line 475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setSplitScreenRecommendState:  mSpiltScreenRecommendDataEntry= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " inRecommend= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v0, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mSpiltScreenRecommendDataEntry:Lcom/android/server/wm/RecommendDataEntry;

    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {v0, p1}, Lcom/android/server/wm/RecommendDataEntry;->setSplitScreenRecommendState(Z)V

    .line 480
    :cond_0
    return-void
.end method

.method public startMultiWindowRecommendAnimation(Landroid/view/View;Z)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "appear"    # Z

    .line 375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " startMultiWindowRecommendAnimation view= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " appear= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiWindowRecommendController"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    if-nez p1, :cond_0

    .line 377
    return-void

    .line 379
    :cond_0
    filled-new-array {p1}, [Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/IStateStyle;->cancel()V

    .line 380
    const/4 v0, 0x0

    .line 381
    .local v0, "hiddenState":Lmiuix/animation/controller/AnimState;
    const/4 v1, 0x0

    .line 382
    .local v1, "shownState":Lmiuix/animation/controller/AnimState;
    instance-of v2, p1, Lcom/android/server/wm/FreeFormRecommendLayout;

    const-wide/16 v3, 0x0

    const-wide v5, 0x3fee666660000000L    # 0.949999988079071

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    if-eqz v2, :cond_1

    .line 383
    new-instance v2, Lmiuix/animation/controller/AnimState;

    const-string v9, "freeFormRecommendHide"

    invoke-direct {v2, v9}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    sget-object v9, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    .line 384
    invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v9, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    .line 385
    invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v5, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    .line 386
    invoke-virtual {v2, v5, v3, v4}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v0

    .line 387
    new-instance v2, Lmiuix/animation/controller/AnimState;

    const-string v3, "freeFormRecommendShow"

    invoke-direct {v2, v3}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    .line 388
    invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v3, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    .line 389
    invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v3, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    .line 390
    invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v1

    goto :goto_0

    .line 392
    :cond_1
    instance-of v2, p1, Lcom/android/server/wm/SplitScreenRecommendLayout;

    if-eqz v2, :cond_2

    .line 393
    new-instance v2, Lmiuix/animation/controller/AnimState;

    const-string/jumbo v9, "splitScreenRecommendHide"

    invoke-direct {v2, v9}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    sget-object v9, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    .line 394
    invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v9, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    .line 395
    invoke-virtual {v2, v9, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v5, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    .line 396
    invoke-virtual {v2, v5, v3, v4}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v0

    .line 397
    new-instance v2, Lmiuix/animation/controller/AnimState;

    const-string/jumbo v3, "splitScreenRecommendShow"

    invoke-direct {v2, v3}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    .line 398
    invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v3, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    .line 399
    invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v2

    sget-object v3, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    .line 400
    invoke-virtual {v2, v3, v7, v8}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v1

    .line 402
    :cond_2
    :goto_0
    if-eqz p2, :cond_3

    .line 403
    filled-new-array {p1}, [Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mShowHideAnimConfig:Lmiuix/animation/base/AnimConfig;

    filled-new-array {v3}, [Lmiuix/animation/base/AnimConfig;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lmiuix/animation/IStateStyle;->fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/base/AnimConfig;)Lmiuix/animation/IStateStyle;

    goto :goto_1

    .line 405
    :cond_3
    filled-new-array {p1}, [Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/wm/MiuiMultiWindowRecommendController;->mShowHideAnimConfig:Lmiuix/animation/base/AnimConfig;

    filled-new-array {v3}, [Lmiuix/animation/base/AnimConfig;

    move-result-object v3

    invoke-interface {v2, v1, v0, v3}, Lmiuix/animation/IStateStyle;->fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/base/AnimConfig;)Lmiuix/animation/IStateStyle;

    .line 407
    :goto_1
    return-void
.end method
