class com.android.server.wm.ScreenRotationAnimationImpl$ScreenRotationSpec implements com.android.server.wm.LocalAnimationAdapter$AnimationSpec {
	 /* .source "ScreenRotationAnimationImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/ScreenRotationAnimationImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ScreenRotationSpec" */
} // .end annotation
/* # instance fields */
private android.view.animation.Animation mAnimation;
private Integer mHeight;
private final java.lang.ThreadLocal mThreadLocalTmps;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ThreadLocal<", */
/* "Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mWidth;
/* # direct methods */
public com.android.server.wm.ScreenRotationAnimationImpl$ScreenRotationSpec ( ) {
/* .locals 1 */
/* .param p1, "animation" # Landroid/view/animation/Animation; */
/* .param p2, "originalWidth" # I */
/* .param p3, "originalHeight" # I */
/* .line 369 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 364 */
/* new-instance v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec$$ExternalSyntheticLambda0; */
/* invoke-direct {v0}, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec$$ExternalSyntheticLambda0;-><init>()V */
/* .line 365 */
java.lang.ThreadLocal .withInitial ( v0 );
this.mThreadLocalTmps = v0;
/* .line 370 */
this.mAnimation = p1;
/* .line 371 */
/* iput p2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mWidth:I */
/* .line 372 */
/* iput p3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mHeight:I */
/* .line 373 */
return;
} // .end method
/* # virtual methods */
public void apply ( android.view.SurfaceControl$Transaction p0, android.view.SurfaceControl p1, Long p2 ) {
/* .locals 6 */
/* .param p1, "t" # Landroid/view/SurfaceControl$Transaction; */
/* .param p2, "leash" # Landroid/view/SurfaceControl; */
/* .param p3, "currentPlayTime" # J */
/* .line 383 */
v0 = this.mThreadLocalTmps;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues; */
/* .line 384 */
/* .local v0, "tmp":Lcom/android/server/wm/ScreenRotationAnimationImpl$TmpValues; */
v1 = this.transformation;
(( android.view.animation.Transformation ) v1 ).clear ( ); // invoke-virtual {v1}, Landroid/view/animation/Transformation;->clear()V
/* .line 385 */
v1 = this.mAnimation;
v2 = this.transformation;
(( android.view.animation.Animation ) v1 ).getTransformation ( p3, p4, v2 ); // invoke-virtual {v1, p3, p4, v2}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z
/* .line 386 */
v1 = this.transformation;
(( android.view.animation.Transformation ) v1 ).getMatrix ( ); // invoke-virtual {v1}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;
v2 = this.floats;
(( android.view.SurfaceControl$Transaction ) p1 ).setMatrix ( p2, v1, v2 ); // invoke-virtual {p1, p2, v1, v2}, Landroid/view/SurfaceControl$Transaction;->setMatrix(Landroid/view/SurfaceControl;Landroid/graphics/Matrix;[F)Landroid/view/SurfaceControl$Transaction;
/* .line 387 */
v1 = this.transformation;
v1 = (( android.view.animation.Transformation ) v1 ).getAlpha ( ); // invoke-virtual {v1}, Landroid/view/animation/Transformation;->getAlpha()F
(( android.view.SurfaceControl$Transaction ) p1 ).setAlpha ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setAlpha(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 388 */
v1 = this.transformation;
v1 = (( android.view.animation.Transformation ) v1 ).hasClipRect ( ); // invoke-virtual {v1}, Landroid/view/animation/Transformation;->hasClipRect()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 389 */
/* int-to-float v1, v1 */
(( android.view.SurfaceControl$Transaction ) p1 ).setCornerRadius ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 390 */
/* new-instance v1, Landroid/graphics/Rect; */
v2 = this.transformation;
(( android.view.animation.Transformation ) v2 ).getClipRect ( ); // invoke-virtual {v2}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;
/* iget v2, v2, Landroid/graphics/Rect;->left:I */
v3 = this.transformation;
/* .line 391 */
(( android.view.animation.Transformation ) v3 ).getClipRect ( ); // invoke-virtual {v3}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;
/* iget v3, v3, Landroid/graphics/Rect;->top:I */
v4 = this.transformation;
/* .line 392 */
(( android.view.animation.Transformation ) v4 ).getClipRect ( ); // invoke-virtual {v4}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;
/* iget v4, v4, Landroid/graphics/Rect;->right:I */
v5 = this.transformation;
/* .line 393 */
(( android.view.animation.Transformation ) v5 ).getClipRect ( ); // invoke-virtual {v5}, Landroid/view/animation/Transformation;->getClipRect()Landroid/graphics/Rect;
/* iget v5, v5, Landroid/graphics/Rect;->bottom:I */
/* invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V */
/* .line 390 */
(( android.view.SurfaceControl$Transaction ) p1 ).setWindowCrop ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
/* .line 395 */
} // :cond_0
/* int-to-float v1, v1 */
(( android.view.SurfaceControl$Transaction ) p1 ).setCornerRadius ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setCornerRadius(Landroid/view/SurfaceControl;F)Landroid/view/SurfaceControl$Transaction;
/* .line 396 */
/* new-instance v1, Landroid/graphics/Rect; */
/* iget v2, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mWidth:I */
/* iget v3, p0, Lcom/android/server/wm/ScreenRotationAnimationImpl$ScreenRotationSpec;->mHeight:I */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V */
(( android.view.SurfaceControl$Transaction ) p1 ).setWindowCrop ( p2, v1 ); // invoke-virtual {p1, p2, v1}, Landroid/view/SurfaceControl$Transaction;->setWindowCrop(Landroid/view/SurfaceControl;Landroid/graphics/Rect;)Landroid/view/SurfaceControl$Transaction;
/* .line 398 */
} // :goto_0
return;
} // .end method
public void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 402 */
(( java.io.PrintWriter ) p1 ).print ( p2 ); // invoke-virtual {p1, p2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 403 */
v0 = this.mAnimation;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 404 */
return;
} // .end method
public void dumpDebugInner ( android.util.proto.ProtoOutputStream p0 ) {
/* .locals 5 */
/* .param p1, "proto" # Landroid/util/proto/ProtoOutputStream; */
/* .line 408 */
/* const-wide v0, 0x10b00000001L */
(( android.util.proto.ProtoOutputStream ) p1 ).start ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->start(J)J
/* move-result-wide v0 */
/* .line 409 */
/* .local v0, "token":J */
v2 = this.mAnimation;
(( java.lang.Object ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
/* const-wide v3, 0x10900000001L */
(( android.util.proto.ProtoOutputStream ) p1 ).write ( v3, v4, v2 ); // invoke-virtual {p1, v3, v4, v2}, Landroid/util/proto/ProtoOutputStream;->write(JLjava/lang/String;)V
/* .line 410 */
(( android.util.proto.ProtoOutputStream ) p1 ).end ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/util/proto/ProtoOutputStream;->end(J)V
/* .line 411 */
return;
} // .end method
public Long getDuration ( ) {
/* .locals 2 */
/* .line 377 */
v0 = this.mAnimation;
(( android.view.animation.Animation ) v0 ).computeDurationHint ( ); // invoke-virtual {v0}, Landroid/view/animation/Animation;->computeDurationHint()J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
