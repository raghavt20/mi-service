class com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager {
	 /* .source "PackageSettingsManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/wm/PackageSettingsManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "DisplayCompatPackageManager" */
} // .end annotation
/* # instance fields */
final java.util.function.Consumer mCallback;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/function/Consumer<", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mPackagesMapBySystem;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mPackagesMapByUserSettings;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.android.server.wm.PackageSettingsManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$uQDd_O5hx4wGzk3LKvsZQKDw_js ( com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager p0, java.util.concurrent.ConcurrentHashMap p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->lambda$new$0(Ljava/util/concurrent/ConcurrentHashMap;)V */
return;
} // .end method
static java.util.Map -$$Nest$fgetmPackagesMapBySystem ( com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackagesMapBySystem;
} // .end method
static void -$$Nest$mdump ( com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager p0, java.io.PrintWriter p1, java.lang.String p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V */
return;
} // .end method
 com.android.server.wm.PackageSettingsManager$DisplayCompatPackageManager ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/android/server/wm/PackageSettingsManager; */
/* .line 162 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 163 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mPackagesMapBySystem = v0;
/* .line 165 */
/* new-instance v0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)V */
this.mCallback = v0;
return;
} // .end method
private void dump ( java.io.PrintWriter p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .param p2, "prefix" # Ljava/lang/String; */
/* .line 200 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 201 */
/* .local v0, "innerPrefix":Ljava/lang/String; */
v1 = v1 = this.mPackagesMapBySystem;
final String v2 = "] "; // const-string v2, "] "
final String v3 = "["; // const-string v3, "["
/* if-nez v1, :cond_0 */
/* .line 202 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "DisplayCompatPackages(System)"; // const-string v4, "DisplayCompatPackages(System)"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 203 */
v1 = this.mPackagesMapBySystem;
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Ljava/util/Map$Entry; */
/* .line 204 */
/* .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v6, Ljava/lang/String; */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 205 */
/* check-cast v6, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v6 ).intValue ( ); // invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
com.android.server.wm.PackageSettingsManager .displayCompatPolicyToString ( v6 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 204 */
(( java.io.PrintWriter ) p1 ).println ( v5 ); // invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 206 */
} // .end local v4 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 209 */
} // :cond_0
v1 = this.mPackagesMapByUserSettings;
/* .line 210 */
/* .local v1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;" */
v4 = this.mPackagesMapByUserSettings;
v4 = if ( v4 != null) { // if-eqz v4, :cond_1
/* if-nez v4, :cond_1 */
/* .line 211 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( p2 ); // invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "DisplayCompatPackages(UserSetting)"; // const-string v5, "DisplayCompatPackages(UserSetting)"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v4 ); // invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 212 */
v4 = this.mPackagesMapByUserSettings;
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_1
/* check-cast v5, Ljava/util/Map$Entry; */
/* .line 213 */
/* .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;" */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v7, Ljava/lang/String; */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 214 */
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
com.android.server.wm.PackageSettingsManager .displayCompatPolicyToString ( v7 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 213 */
(( java.io.PrintWriter ) p1 ).println ( v6 ); // invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 215 */
} // .end local v5 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
/* .line 217 */
} // :cond_1
return;
} // .end method
private void lambda$new$0 ( java.util.concurrent.ConcurrentHashMap p0 ) { //synthethic
/* .locals 1 */
/* .param p1, "map" # Ljava/util/concurrent/ConcurrentHashMap; */
/* .line 166 */
v0 = this.mPackagesMapBySystem;
/* .line 167 */
/* new-instance v0, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager$1;-><init>(Lcom/android/server/wm/PackageSettingsManager$DisplayCompatPackageManager;)V */
(( java.util.concurrent.ConcurrentHashMap ) p1 ).forEach ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->forEach(Ljava/util/function/BiConsumer;)V
/* .line 197 */
return;
} // .end method
/* # virtual methods */
public void clearUserSettings ( ) {
/* .locals 1 */
/* .line 220 */
v0 = this.mPackagesMapByUserSettings;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 221 */
/* .line 223 */
} // :cond_0
return;
} // .end method
public Integer getPolicy ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 227 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "DisplayCompatPackageManager getPolicy packageName = "; // const-string v1, "DisplayCompatPackageManager getPolicy packageName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "PackageSettingsManager"; // const-string v1, "PackageSettingsManager"
android.util.Slog .w ( v1,v0 );
/* .line 228 */
v0 = this.this$0;
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmAtmService ( v0 );
/* iget-boolean v0, v0, Lcom/android/server/wm/ActivityTaskManagerService;->mForceResizableActivities:Z */
int v1 = 3; // const/4 v1, 0x3
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 229 */
/* .line 232 */
} // :cond_0
v0 = this.mPackagesMapByUserSettings;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 233 */
/* .line 234 */
/* .local v0, "value":Ljava/lang/Object; */
/* instance-of v2, v0, Ljava/lang/Integer; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 235 */
/* move-object v2, v0 */
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 236 */
/* .local v2, "policy":I */
int v3 = 1; // const/4 v3, 0x1
/* if-eq v2, v3, :cond_1 */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 238 */
} // :cond_1
/* .line 242 */
} // .end local v0 # "value":Ljava/lang/Object;
} // .end local v2 # "policy":I
} // :cond_2
v0 = this.mPackagesMapBySystem;
/* .line 243 */
/* .restart local v0 # "value":Ljava/lang/Object; */
/* instance-of v2, v0, Ljava/lang/Integer; */
final String v3 = "android.supports_size_changes"; // const-string v3, "android.supports_size_changes"
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 244 */
/* move-object v2, v0 */
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 245 */
/* .restart local v2 # "policy":I */
/* if-eq v2, v1, :cond_4 */
int v1 = 4; // const/4 v1, 0x4
/* if-eq v2, v1, :cond_4 */
int v1 = 7; // const/4 v1, 0x7
/* if-eq v2, v1, :cond_4 */
/* const/16 v1, 0x8 */
/* if-eq v2, v1, :cond_4 */
/* const/16 v1, 0x9 */
/* if-eq v2, v1, :cond_4 */
/* const/16 v1, 0xa */
/* if-eq v2, v1, :cond_4 */
/* const/16 v1, 0xb */
/* if-eq v2, v1, :cond_4 */
int v1 = 6; // const/4 v1, 0x6
/* if-ne v2, v1, :cond_3 */
/* .line 255 */
} // .end local v2 # "policy":I
} // :cond_3
/* .line 253 */
/* .restart local v2 # "policy":I */
} // :cond_4
} // :goto_0
/* .line 256 */
} // .end local v2 # "policy":I
} // :cond_5
v2 = this.this$0;
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmAtmServiceImpl ( v2 );
v4 = android.appcompat.ApplicationCompatUtilsStub.MIUI_SUPPORT_APP_CONTINUITY;
v2 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).getMetaDataBoolean ( p1, v4 ); // invoke-virtual {v2, p1, v4}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v2, :cond_8 */
v2 = this.this$0;
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmAtmServiceImpl ( v2 );
/* .line 257 */
v2 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).getMetaDataBoolean ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 262 */
} // :cond_6
} // :goto_1
v1 = this.this$0;
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmAtmServiceImpl ( v1 );
v2 = android.appcompat.ApplicationCompatUtilsStub.MIUI_SUPPORT_APP_CONTINUITY;
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v1 ).getMetaDataBoolean ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v1, :cond_7 */
v1 = this.this$0;
com.android.server.wm.PackageSettingsManager .-$$Nest$fgetmAtmServiceImpl ( v1 );
/* .line 263 */
v1 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v1 ).getMetaDataBoolean ( p1, v3 ); // invoke-virtual {v1, p1, v3}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getMetaDataBoolean(Ljava/lang/String;Ljava/lang/String;)Z
/* if-nez v1, :cond_7 */
/* .line 264 */
int v1 = 0; // const/4 v1, 0x0
/* .line 266 */
} // :cond_7
int v1 = 5; // const/4 v1, 0x5
/* .line 258 */
} // :cond_8
} // :goto_2
} // .end method
public void setPolicy ( java.util.Map p0, Boolean p1 ) {
/* .locals 1 */
/* .param p2, "replaceAll" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Map<", */
/* "+", */
/* "Ljava/lang/String;", */
/* "+", */
/* "Ljava/lang/Integer;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 270 */
/* .local p1, "requestedPackages":Ljava/util/Map;, "Ljava/util/Map<+Ljava/lang/String;+Ljava/lang/Integer;>;" */
v0 = this.mPackagesMapByUserSettings;
/* if-nez v0, :cond_0 */
/* .line 271 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mPackagesMapByUserSettings = v0;
/* .line 272 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 273 */
/* .line 275 */
} // :cond_1
} // :goto_0
v0 = this.mPackagesMapByUserSettings;
/* .line 276 */
return;
} // .end method
