.class Lcom/android/server/wm/ImmersiveModeConfirmationImpl;
.super Ljava/lang/Object;
.source "ImmersiveModeConfirmationImpl.java"

# interfaces
.implements Lcom/android/server/wm/ImmersiveModeConfirmationStub;


# static fields
.field private static final FACTORY_BUILD_SIGN:Ljava/lang/String; = "1"

.field private static final IS_FACTORY_BUILD:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ImmersiveModeConfirmation"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 33
    const-string v0, "ro.boot.factorybuild"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/wm/ImmersiveModeConfirmationImpl;->IS_FACTORY_BUILD:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public disableImmersiveConfirmation(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkg"    # Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/android/server/wm/PolicyControl;->disableImmersiveConfirmation(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 47
    invoke-static {p1, p2}, Lcom/android/server/wm/PolicyControl;->dump(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 48
    return-void
.end method

.method public handleMessageShow(Lcom/android/server/wm/ImmersiveModeConfirmation;IZ)V
    .locals 3
    .param p1, "immersiveModeConfirmation"    # Lcom/android/server/wm/ImmersiveModeConfirmation;
    .param p2, "what"    # I
    .param p3, "isDebug"    # Z

    .line 54
    sget-object v0, Lcom/android/server/wm/ImmersiveModeConfirmationImpl;->IS_FACTORY_BUILD:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "ImmersiveModeConfirmation"

    if-nez v1, :cond_1

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 60
    :cond_0
    if-eqz p3, :cond_3

    .line 61
    const-string v0, "factory build not show immersive mode confirmation"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 55
    :cond_1
    :goto_0
    if-eqz p3, :cond_2

    .line 56
    const-string v0, "Not factory build"

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_2
    sget-object v0, Lcom/android/server/wm/ImmersiveModeConfirmationProxy;->handleShow:Lcom/xiaomi/reflect/RefMethod;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :cond_3
    :goto_1
    return-void
.end method

.method public reloadFromSetting(Landroid/content/Context;)Z
    .locals 1
    .param p1, "cxt"    # Landroid/content/Context;

    .line 37
    invoke-static {p1}, Lcom/android/server/wm/PolicyControl;->reloadFromSetting(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
