class com.android.server.MiuiBatteryServiceImpl$11 implements android.bluetooth.BluetoothProfile$ServiceListener {
	 /* .source "MiuiBatteryServiceImpl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryServiceImpl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryServiceImpl this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryServiceImpl$11 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryServiceImpl; */
/* .line 444 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( Integer p0, android.bluetooth.BluetoothProfile p1 ) {
/* .locals 2 */
/* .param p1, "profile" # I */
/* .param p2, "proxy" # Landroid/bluetooth/BluetoothProfile; */
/* .line 448 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 450 */
/* :pswitch_0 */
v0 = this.this$0;
/* move-object v1, p2 */
/* check-cast v1, Landroid/bluetooth/BluetoothA2dp; */
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmA2dp ( v0,v1 );
/* .line 451 */
/* .line 453 */
/* :pswitch_1 */
v0 = this.this$0;
/* move-object v1, p2 */
/* check-cast v1, Landroid/bluetooth/BluetoothHeadset; */
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmHeadset ( v0,v1 );
/* .line 454 */
/* nop */
/* .line 458 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onServiceDisconnected ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "profile" # I */
/* .line 463 */
int v0 = 0; // const/4 v0, 0x0
/* packed-switch p1, :pswitch_data_0 */
/* .line 465 */
/* :pswitch_0 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmA2dp ( v1,v0 );
/* .line 466 */
/* .line 468 */
/* :pswitch_1 */
v1 = this.this$0;
com.android.server.MiuiBatteryServiceImpl .-$$Nest$fputmHeadset ( v1,v0 );
/* .line 469 */
/* nop */
/* .line 473 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
