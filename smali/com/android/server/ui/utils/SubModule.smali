.class public Lcom/android/server/ui/utils/SubModule;
.super Ljava/lang/Object;
.source "SubModule.java"


# static fields
.field public static ID_BASE:I = 0x0

.field public static final ID_OD:I

.field public static final MODULE_VERSION:Ljava/lang/String; = "1.0"

.field public static final SUB_MODULE_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 7
    const/4 v0, -0x1

    sput v0, Lcom/android/server/ui/utils/SubModule;->ID_BASE:I

    .line 8
    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/android/server/ui/utils/SubModule;->ID_BASE:I

    sput v0, Lcom/android/server/ui/utils/SubModule;->ID_OD:I

    .line 9
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/server/ui/utils/SubModule;->SUB_MODULE_LIST:Ljava/util/ArrayList;

    .line 11
    const-string v2, "od"

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
