public class com.android.server.ui.utils.LogUtil {
	 /* .source "LogUtil.java" */
	 /* # static fields */
	 private static Boolean DEBUG_ALL;
	 /* # direct methods */
	 static com.android.server.ui.utils.LogUtil ( ) {
		 /* .locals 1 */
		 /* .line 6 */
		 int v0 = 1; // const/4 v0, 0x1
		 com.android.server.ui.utils.LogUtil.DEBUG_ALL = (v0!= 0);
		 return;
	 } // .end method
	 public com.android.server.ui.utils.LogUtil ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static void logD ( java.lang.String p0, java.lang.String p1 ) {
		 /* .locals 1 */
		 /* .param p0, "tag" # Ljava/lang/String; */
		 /* .param p1, "msg" # Ljava/lang/String; */
		 /* .line 9 */
		 /* sget-boolean v0, Lcom/android/server/ui/utils/LogUtil;->DEBUG_ALL:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 10 */
			 android.util.Log .d ( p0,p1 );
			 /* .line 12 */
		 } // :cond_0
		 return;
	 } // .end method
	 public static void logE ( java.lang.String p0, java.lang.String p1 ) {
		 /* .locals 0 */
		 /* .param p0, "tag" # Ljava/lang/String; */
		 /* .param p1, "msg" # Ljava/lang/String; */
		 /* .line 20 */
		 android.util.Log .e ( p0,p1 );
		 /* .line 21 */
		 return;
	 } // .end method
	 public static void logI ( java.lang.String p0, java.lang.String p1 ) {
		 /* .locals 0 */
		 /* .param p0, "tag" # Ljava/lang/String; */
		 /* .param p1, "msg" # Ljava/lang/String; */
		 /* .line 14 */
		 android.util.Log .i ( p0,p1 );
		 /* .line 15 */
		 return;
	 } // .end method
	 public static void logW ( java.lang.String p0, java.lang.String p1 ) {
		 /* .locals 0 */
		 /* .param p0, "tag" # Ljava/lang/String; */
		 /* .param p1, "msg" # Ljava/lang/String; */
		 /* .line 17 */
		 android.util.Log .w ( p0,p1 );
		 /* .line 18 */
		 return;
	 } // .end method
