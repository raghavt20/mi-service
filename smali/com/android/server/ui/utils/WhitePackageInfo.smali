.class public Lcom/android/server/ui/utils/WhitePackageInfo;
.super Ljava/lang/Object;
.source "WhitePackageInfo.java"


# instance fields
.field private mVersion:Ljava/lang/String;

.field private mWhitePackageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getVersion()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/android/server/ui/utils/WhitePackageInfo;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getWhitePackageList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/android/server/ui/utils/WhitePackageInfo;->mWhitePackageList:Ljava/util/ArrayList;

    return-object v0
.end method
