public class com.android.server.ui.UIService {
	 /* .source "UIService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ui/UIService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static android.os.HandlerThread mThread;
/* # instance fields */
private android.content.Context mContext;
private com.android.server.ui.display.DisplayCloudController mDisplayCloudController;
/* # direct methods */
private com.android.server.ui.UIService ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 33 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 34 */
	 final String v0 = "UIService"; // const-string v0, "UIService"
	 /* const-string/jumbo v1, "start uiservice" */
	 com.android.server.ui.utils.LogUtil .logD ( v0,v1 );
	 /* .line 35 */
	 this.mContext = p1;
	 /* .line 36 */
	 com.android.server.ui.display.DisplayCloudController .getInstance ( p1 );
	 this.mDisplayCloudController = v0;
	 /* .line 37 */
	 return;
} // .end method
 com.android.server.ui.UIService ( ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/android/server/ui/UIService;-><init>(Landroid/content/Context;)V */
	 return;
} // .end method
private static void ensureThreadLocked ( ) {
	 /* .locals 3 */
	 /* .line 22 */
	 v0 = com.android.server.ui.UIService.mThread;
	 /* if-nez v0, :cond_0 */
	 /* .line 23 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "UIService"; // const-string v1, "UIService"
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
	 /* .line 24 */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 26 */
} // :cond_0
return;
} // .end method
public static synchronized android.os.HandlerThread getThread ( ) {
/* .locals 2 */
/* const-class v0, Lcom/android/server/ui/UIService; */
/* monitor-enter v0 */
/* .line 29 */
try { // :try_start_0
	 com.android.server.ui.UIService .ensureThreadLocked ( );
	 /* .line 30 */
	 v1 = com.android.server.ui.UIService.mThread;
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* monitor-exit v0 */
	 /* .line 28 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* throw v1 */
} // .end method
