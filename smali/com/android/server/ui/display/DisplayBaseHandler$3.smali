.class Lcom/android/server/ui/display/DisplayBaseHandler$3;
.super Ljava/lang/Object;
.source "DisplayBaseHandler.java"

# interfaces
.implements Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ui/display/DisplayBaseHandler;->registerMultiScreenEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ui/display/DisplayBaseHandler;


# direct methods
.method constructor <init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ui/display/DisplayBaseHandler;

    .line 124
    iput-object p1, p0, Lcom/android/server/ui/display/DisplayBaseHandler$3;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "MultiScreenStatus"    # Z

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onChange: MultiScreenStatus = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UIService-DisplayBaseHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler$3;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    const/4 v1, 0x3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/ui/display/DisplayBaseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/ui/display/DisplayBaseHandler;->sendMessage(Landroid/os/Message;)Z

    .line 129
    return-void
.end method
