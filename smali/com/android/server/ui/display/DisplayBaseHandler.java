public class com.android.server.ui.display.DisplayBaseHandler extends android.os.Handler {
	 /* .source "DisplayBaseHandler.java" */
	 /* # static fields */
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private final Integer MSG_ID_CLOUD_CHANGE;
	 private final Integer MSG_ID_FORGROUND_CHANGE;
	 private final Integer MSG_ID_MULTISCREEN_CHANGE;
	 private final Integer SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE;
	 private java.lang.String UI_SERVICE_CLOUD_FILE;
	 private android.os.IBinder mFlinger;
	 private com.android.server.ui.event_status.ForegroundStatusHandler mForegroundStatusHandler;
	 private Integer mKey;
	 private com.android.server.MiuiCommonCloudHelperStub mMiuiCommonCloudHelper;
	 private com.android.server.ui.event_status.MultiScreenHandler mMultiScreenHandler;
	 private java.util.ArrayList mWhitePackageList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static Integer -$$Nest$fgetmKey ( com.android.server.ui.display.DisplayBaseHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I */
} // .end method
static com.android.server.MiuiCommonCloudHelperStub -$$Nest$fgetmMiuiCommonCloudHelper ( com.android.server.ui.display.DisplayBaseHandler p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiCommonCloudHelper;
} // .end method
static java.util.ArrayList -$$Nest$fgetmWhitePackageList ( com.android.server.ui.display.DisplayBaseHandler p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWhitePackageList;
} // .end method
static void -$$Nest$fputmWhitePackageList ( com.android.server.ui.display.DisplayBaseHandler p0, java.util.ArrayList p1 ) { //bridge//synthethic
/* .locals 0 */
this.mWhitePackageList = p1;
return;
} // .end method
public com.android.server.ui.display.DisplayBaseHandler ( ) {
/* .locals 1 */
/* .line 42 */
com.android.server.ui.UIService .getThread ( );
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 33 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I */
/* .line 34 */
final String v0 = ""; // const-string v0, ""
this.UI_SERVICE_CLOUD_FILE = v0;
/* .line 36 */
/* const/16 v0, 0x7983 */
/* iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE:I */
/* .line 37 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->MSG_ID_CLOUD_CHANGE:I */
/* .line 38 */
int v0 = 2; // const/4 v0, 0x2
/* iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->MSG_ID_FORGROUND_CHANGE:I */
/* .line 39 */
int v0 = 3; // const/4 v0, 0x3
/* iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->MSG_ID_MULTISCREEN_CHANGE:I */
/* .line 43 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 59 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "UIService-DisplayBaseHandler"; // const-string v1, "UIService-DisplayBaseHandler"
/* packed-switch v0, :pswitch_data_0 */
/* .line 71 */
/* :pswitch_0 */
final String v0 = "handleMessage MSG_ID_MultiScreen_CHANGE"; // const-string v0, "handleMessage MSG_ID_MultiScreen_CHANGE"
com.android.server.ui.utils.LogUtil .logI ( v1,v0 );
/* .line 72 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
(( com.android.server.ui.display.DisplayBaseHandler ) p0 ).onMultiScreenChange ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/ui/display/DisplayBaseHandler;->onMultiScreenChange(Z)V
/* .line 73 */
/* .line 66 */
/* :pswitch_1 */
final String v0 = "handleMessage MSG_ID_FORGROUND_CHANGE"; // const-string v0, "handleMessage MSG_ID_FORGROUND_CHANGE"
com.android.server.ui.utils.LogUtil .logI ( v1,v0 );
/* .line 67 */
v0 = this.obj;
/* check-cast v0, Lmiui/process/ForegroundInfo; */
(( com.android.server.ui.display.DisplayBaseHandler ) p0 ).onForegroundChange ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/ui/display/DisplayBaseHandler;->onForegroundChange(Lmiui/process/ForegroundInfo;)V
/* .line 68 */
/* .line 61 */
/* :pswitch_2 */
final String v0 = "handleMessage MSG_ID_CLOUD_CHANGE"; // const-string v0, "handleMessage MSG_ID_CLOUD_CHANGE"
com.android.server.ui.utils.LogUtil .logI ( v1,v0 );
/* .line 62 */
v0 = this.obj;
/* check-cast v0, Ljava/util/ArrayList; */
(( com.android.server.ui.display.DisplayBaseHandler ) p0 ).onCloudUpdate ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/server/ui/display/DisplayBaseHandler;->onCloudUpdate(Ljava/util/ArrayList;)V
/* .line 63 */
/* nop */
/* .line 78 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void init ( android.content.Context p0, Integer p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "key" # I */
/* .param p3, "cloud_file" # Ljava/lang/String; */
/* .line 46 */
/* iput p2, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I */
/* .line 47 */
this.UI_SERVICE_CLOUD_FILE = p3;
/* .line 48 */
com.android.server.MiuiCommonCloudHelperStub .getInstance ( );
this.mMiuiCommonCloudHelper = v0;
/* .line 49 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
android.os.Environment .getDownloadCacheDirectory ( );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.android.server.MiuiCommonCloudHelperStub ) v0 ).init ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lcom/android/server/MiuiCommonCloudHelperStub;->init(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V
/* .line 50 */
com.android.server.ui.event_status.ForegroundStatusHandler .getInstance ( p1 );
this.mForegroundStatusHandler = v0;
/* .line 51 */
com.android.server.ui.event_status.MultiScreenHandler .getInstance ( p1 );
this.mMultiScreenHandler = v0;
/* .line 52 */
(( com.android.server.ui.display.DisplayBaseHandler ) p0 ).registerCloudObserver ( ); // invoke-virtual {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;->registerCloudObserver()V
/* .line 53 */
(( com.android.server.ui.display.DisplayBaseHandler ) p0 ).registerFgEvent ( ); // invoke-virtual {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;->registerFgEvent()V
/* .line 54 */
(( com.android.server.ui.display.DisplayBaseHandler ) p0 ).registerMultiScreenEvent ( ); // invoke-virtual {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;->registerMultiScreenEvent()V
/* .line 55 */
return;
} // .end method
public void notifySFMode ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "cookie" # I */
/* .param p2, "status" # I */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .line 134 */
final String v0 = "notifySFMode"; // const-string v0, "notifySFMode"
final String v1 = "UIService-DisplayBaseHandler"; // const-string v1, "UIService-DisplayBaseHandler"
com.android.server.ui.utils.LogUtil .logD ( v1,v0 );
/* .line 135 */
v0 = this.mFlinger;
/* if-nez v0, :cond_0 */
/* .line 136 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
this.mFlinger = v0;
/* .line 137 */
} // :cond_0
v0 = this.mFlinger;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 138 */
final String v0 = "getService succesd"; // const-string v0, "getService succesd"
com.android.server.ui.utils.LogUtil .logD ( v1,v0 );
/* .line 139 */
android.os.Parcel .obtain ( );
/* .line 140 */
/* .local v0, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 141 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 142 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 143 */
(( android.os.Parcel ) v0 ).writeString ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 145 */
try { // :try_start_0
final String v2 = "notify SurfaceFlinger"; // const-string v2, "notify SurfaceFlinger"
com.android.server.ui.utils.LogUtil .logD ( v1,v2 );
/* .line 146 */
v2 = this.mFlinger;
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
/* const/16 v5, 0x7983 */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 150 */
} // :goto_0
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 151 */
/* .line 150 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 147 */
/* :catch_0 */
/* move-exception v2 */
/* .line 148 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to notify SurfaceFlinger"; // const-string v4, "Failed to notify SurfaceFlinger"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logE ( v1,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v2 # "ex":Ljava/lang/Exception;
/* .line 150 */
} // :goto_1
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 151 */
/* throw v1 */
/* .line 153 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // :cond_1
} // :goto_2
return;
} // .end method
public void onCloudUpdate ( java.util.ArrayList p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 80 */
/* .local p1, "WhitePackageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public void onForegroundChange ( miui.process.ForegroundInfo p0 ) {
/* .locals 0 */
/* .param p1, "appInfo" # Lmiui/process/ForegroundInfo; */
/* .line 82 */
return;
} // .end method
public void onMultiScreenChange ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "IsMultiScreen" # Z */
/* .line 84 */
return;
} // .end method
public void registerCloudObserver ( ) {
/* .locals 2 */
/* .line 87 */
final String v0 = "UIService-DisplayBaseHandler"; // const-string v0, "UIService-DisplayBaseHandler"
final String v1 = "registerCloudObserver"; // const-string v1, "registerCloudObserver"
com.android.server.ui.utils.LogUtil .logD ( v0,v1 );
/* .line 88 */
v0 = this.mMiuiCommonCloudHelper;
/* new-instance v1, Lcom/android/server/ui/display/DisplayBaseHandler$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/ui/display/DisplayBaseHandler$1;-><init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V */
(( com.android.server.MiuiCommonCloudHelperStub ) v0 ).registerObserver ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->registerObserver(Lcom/android/server/MiuiCommonCloudHelperStub$Observer;)V
/* .line 105 */
v0 = this.mMiuiCommonCloudHelper;
v1 = this.UI_SERVICE_CLOUD_FILE;
(( com.android.server.MiuiCommonCloudHelperStub ) v0 ).initFromAssets ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->initFromAssets(Ljava/lang/String;)V
/* .line 106 */
v0 = this.mMiuiCommonCloudHelper;
(( com.android.server.MiuiCommonCloudHelperStub ) v0 ).startCloudObserve ( ); // invoke-virtual {v0}, Lcom/android/server/MiuiCommonCloudHelperStub;->startCloudObserve()V
/* .line 107 */
return;
} // .end method
public void registerFgEvent ( ) {
/* .locals 3 */
/* .line 110 */
final String v0 = "UIService-DisplayBaseHandler"; // const-string v0, "UIService-DisplayBaseHandler"
final String v1 = "registerFgEvent"; // const-string v1, "registerFgEvent"
com.android.server.ui.utils.LogUtil .logD ( v0,v1 );
/* .line 111 */
v0 = this.mForegroundStatusHandler;
/* iget v1, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I */
/* new-instance v2, Lcom/android/server/ui/display/DisplayBaseHandler$2; */
/* invoke-direct {v2, p0}, Lcom/android/server/ui/display/DisplayBaseHandler$2;-><init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V */
(( com.android.server.ui.event_status.ForegroundStatusHandler ) v0 ).addFgStatusChangeCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->addFgStatusChangeCallback(ILcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;)V
/* .line 120 */
return;
} // .end method
public void registerMultiScreenEvent ( ) {
/* .locals 3 */
/* .line 123 */
final String v0 = "UIService-DisplayBaseHandler"; // const-string v0, "UIService-DisplayBaseHandler"
final String v1 = "registerMultiScreenEvent"; // const-string v1, "registerMultiScreenEvent"
com.android.server.ui.utils.LogUtil .logD ( v0,v1 );
/* .line 124 */
v0 = this.mMultiScreenHandler;
/* iget v1, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I */
/* new-instance v2, Lcom/android/server/ui/display/DisplayBaseHandler$3; */
/* invoke-direct {v2, p0}, Lcom/android/server/ui/display/DisplayBaseHandler$3;-><init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V */
(( com.android.server.ui.event_status.MultiScreenHandler ) v0 ).addMultiScreenStatusChangeCallback ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/ui/event_status/MultiScreenHandler;->addMultiScreenStatusChangeCallback(ILcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;)V
/* .line 131 */
return;
} // .end method
