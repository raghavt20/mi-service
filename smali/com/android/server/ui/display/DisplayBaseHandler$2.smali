.class Lcom/android/server/ui/display/DisplayBaseHandler$2;
.super Ljava/lang/Object;
.source "DisplayBaseHandler.java"

# interfaces
.implements Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ui/display/DisplayBaseHandler;->registerFgEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ui/display/DisplayBaseHandler;


# direct methods
.method constructor <init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ui/display/DisplayBaseHandler;

    .line 111
    iput-object p1, p0, Lcom/android/server/ui/display/DisplayBaseHandler$2;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange(Lmiui/process/ForegroundInfo;)V
    .locals 2
    .param p1, "appInfo"    # Lmiui/process/ForegroundInfo;

    .line 114
    if-eqz p1, :cond_0

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onChange: fg app = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UIService-DisplayBaseHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler$2;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/android/server/ui/display/DisplayBaseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/ui/display/DisplayBaseHandler;->sendMessage(Landroid/os/Message;)Z

    .line 118
    :cond_0
    return-void
.end method
