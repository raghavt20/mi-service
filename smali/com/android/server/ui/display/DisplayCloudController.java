public class com.android.server.ui.display.DisplayCloudController {
	 /* .source "DisplayCloudController.java" */
	 /* # static fields */
	 public static final java.lang.String TAG;
	 private static com.android.server.ui.display.DisplayCloudController mInstance;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private com.android.server.ui.event_status.ForegroundStatusHandler mForegroundStatusHandler;
	 private com.android.server.ui.event_status.MultiScreenHandler mMultiScreenHandler;
	 private com.android.server.ui.display.DisplayODHandler mOD;
	 /* # direct methods */
	 static com.android.server.ui.display.DisplayCloudController ( ) {
		 /* .locals 1 */
		 /* .line 14 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 private com.android.server.ui.display.DisplayCloudController ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 21 */
		 this.mContext = p1;
		 /* .line 22 */
		 final String v0 = "ro.vendor.display.uiservice.enable"; // const-string v0, "ro.vendor.display.uiservice.enable"
		 int v1 = 1; // const/4 v1, 0x1
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 23 */
			 com.android.server.ui.display.DisplayODHandler .getInstance ( p1 );
			 this.mOD = v0;
			 /* .line 25 */
		 } // :cond_0
		 com.android.server.ui.event_status.ForegroundStatusHandler .getInstance ( p1 );
		 this.mForegroundStatusHandler = v0;
		 /* .line 26 */
		 com.android.server.ui.event_status.MultiScreenHandler .getInstance ( p1 );
		 this.mMultiScreenHandler = v0;
		 /* .line 27 */
		 return;
	 } // .end method
	 public static synchronized com.android.server.ui.display.DisplayCloudController getInstance ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* const-class v0, Lcom/android/server/ui/display/DisplayCloudController; */
		 /* monitor-enter v0 */
		 /* .line 30 */
		 try { // :try_start_0
			 final String v1 = "UIService-DisplayCloudController"; // const-string v1, "UIService-DisplayCloudController"
			 final String v2 = "getInstance"; // const-string v2, "getInstance"
			 com.android.server.ui.utils.LogUtil .logD ( v1,v2 );
			 /* .line 31 */
			 v1 = com.android.server.ui.display.DisplayCloudController.mInstance;
			 /* if-nez v1, :cond_0 */
			 if ( p0 != null) { // if-eqz p0, :cond_0
				 /* .line 32 */
				 /* new-instance v1, Lcom/android/server/ui/display/DisplayCloudController; */
				 /* invoke-direct {v1, p0}, Lcom/android/server/ui/display/DisplayCloudController;-><init>(Landroid/content/Context;)V */
				 /* .line 34 */
			 } // :cond_0
			 v1 = com.android.server.ui.display.DisplayCloudController.mInstance;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* monitor-exit v0 */
			 /* .line 29 */
		 } // .end local p0 # "context":Landroid/content/Context;
		 /* :catchall_0 */
		 /* move-exception p0 */
		 /* monitor-exit v0 */
		 /* throw p0 */
	 } // .end method
