public class com.android.server.ui.display.DisplayODHandler extends com.android.server.ui.display.DisplayBaseHandler {
	 /* .source "DisplayODHandler.java" */
	 /* # static fields */
	 public static final java.lang.String TAG;
	 private static com.android.server.ui.display.DisplayODHandler mInstance;
	 /* # instance fields */
	 private final Integer COOKIE_OD_MODE;
	 private final java.lang.String UI_SERVICE_CLOUD_FILE;
	 private miui.process.ForegroundInfo mAppInfo;
	 private final android.content.Context mContext;
	 private Boolean mIsMultiScreen;
	 private Integer mIsODSwitch;
	 private final Integer mKey;
	 private final java.lang.Object mOdLocked;
	 private java.util.ArrayList mOdPackages;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.android.server.ui.display.DisplayODHandler ( ) {
/* .locals 1 */
/* .line 17 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.android.server.ui.display.DisplayODHandler ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 28 */
/* invoke-direct {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;-><init>()V */
/* .line 19 */
/* const-string/jumbo v0, "ui_service_od_config.xml" */
this.UI_SERVICE_CLOUD_FILE = v0;
/* .line 20 */
/* iput v1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mKey:I */
/* .line 21 */
/* const/16 v2, 0x3e7 */
/* iput v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->COOKIE_OD_MODE:I */
/* .line 22 */
int v2 = 0; // const/4 v2, 0x0
this.mAppInfo = v2;
/* .line 23 */
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z */
/* .line 24 */
/* iput v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsODSwitch:I */
/* .line 25 */
/* new-instance v2, Ljava/util/ArrayList; */
/* const/16 v3, 0xa */
/* invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V */
this.mOdPackages = v2;
/* .line 26 */
/* new-instance v2, Ljava/lang/Object; */
/* invoke-direct {v2}, Ljava/lang/Object;-><init>()V */
this.mOdLocked = v2;
/* .line 29 */
this.mContext = p1;
/* .line 30 */
(( com.android.server.ui.display.DisplayODHandler ) p0 ).init ( p1, v1, v0 ); // invoke-virtual {p0, p1, v1, v0}, Lcom/android/server/ui/display/DisplayODHandler;->init(Landroid/content/Context;ILjava/lang/String;)V
/* .line 31 */
return;
} // .end method
private void ODSwitch ( ) {
/* .locals 6 */
/* .line 63 */
v0 = this.mAppInfo;
if ( v0 != null) { // if-eqz v0, :cond_3
	 v0 = this.mOdPackages;
	 /* if-nez v0, :cond_0 */
	 /* .line 67 */
} // :cond_0
v0 = this.mOdLocked;
/* monitor-enter v0 */
/* .line 68 */
try { // :try_start_0
	 v1 = this.mOdPackages;
	 v2 = this.mAppInfo;
	 v2 = this.mForegroundPackageName;
	 v1 = 	 (( java.util.ArrayList ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
	 /* .line 69 */
	 /* .local v1, "tempSwitch":Z */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* iget-boolean v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z */
		 /* if-nez v2, :cond_1 */
		 /* .line 70 */
		 int v2 = 1; // const/4 v2, 0x1
		 /* .local v2, "status":I */
		 /* .line 72 */
	 } // .end local v2 # "status":I
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 74 */
/* .restart local v2 # "status":I */
} // :goto_0
/* iget v3, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsODSwitch:I */
/* if-eq v2, v3, :cond_2 */
/* .line 75 */
/* iput v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsODSwitch:I */
/* .line 76 */
v3 = this.mAppInfo;
v3 = this.mForegroundPackageName;
/* const/16 v4, 0x3e7 */
(( com.android.server.ui.display.DisplayODHandler ) p0 ).notifySFMode ( v4, v2, v3 ); // invoke-virtual {p0, v4, v2, v3}, Lcom/android/server/ui/display/DisplayODHandler;->notifySFMode(IILjava/lang/String;)V
/* .line 77 */
final String v3 = "UIService-DisplayODHandler"; // const-string v3, "UIService-DisplayODHandler"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "onForegroundChanged for OD value="; // const-string v5, "onForegroundChanged for OD value="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " mForegroundPackageName="; // const-string v5, " mForegroundPackageName="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mAppInfo;
v5 = this.mForegroundPackageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " tempSwitch="; // const-string v5, " tempSwitch="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logI ( v3,v4 );
/* .line 80 */
} // .end local v1 # "tempSwitch":Z
} // :cond_2
/* monitor-exit v0 */
/* .line 81 */
return;
/* .line 80 */
} // .end local v2 # "status":I
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 64 */
} // :cond_3
} // :goto_1
return;
} // .end method
public static synchronized com.android.server.ui.display.DisplayODHandler getInstance ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/android/server/ui/display/DisplayODHandler; */
/* monitor-enter v0 */
/* .line 34 */
try { // :try_start_0
final String v1 = "UIService-DisplayODHandler"; // const-string v1, "UIService-DisplayODHandler"
final String v2 = "getInstance"; // const-string v2, "getInstance"
com.android.server.ui.utils.LogUtil .logD ( v1,v2 );
/* .line 35 */
v1 = com.android.server.ui.display.DisplayODHandler.mInstance;
/* if-nez v1, :cond_0 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 36 */
/* new-instance v1, Lcom/android/server/ui/display/DisplayODHandler; */
/* invoke-direct {v1, p0}, Lcom/android/server/ui/display/DisplayODHandler;-><init>(Landroid/content/Context;)V */
/* .line 38 */
} // :cond_0
v1 = com.android.server.ui.display.DisplayODHandler.mInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 33 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
/* # virtual methods */
public void onCloudUpdate ( java.util.ArrayList p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 43 */
/* .local p1, "WhitePackageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
final String v0 = "UIService-DisplayODHandler"; // const-string v0, "UIService-DisplayODHandler"
final String v1 = "onCloudUpdate"; // const-string v1, "onCloudUpdate"
com.android.server.ui.utils.LogUtil .logD ( v0,v1 );
/* .line 44 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 45 */
this.mOdPackages = p1;
/* .line 46 */
} // :cond_0
return;
} // .end method
public void onDestroy ( ) {
/* .locals 0 */
/* .line 85 */
return;
} // .end method
public void onForegroundChange ( miui.process.ForegroundInfo p0 ) {
/* .locals 2 */
/* .param p1, "appInfo" # Lmiui/process/ForegroundInfo; */
/* .line 50 */
this.mAppInfo = p1;
/* .line 51 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mForegroundPackageName = "; // const-string v1, "mForegroundPackageName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAppInfo;
v1 = this.mForegroundPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "UIService-DisplayODHandler"; // const-string v1, "UIService-DisplayODHandler"
com.android.server.ui.utils.LogUtil .logD ( v1,v0 );
/* .line 52 */
/* invoke-direct {p0}, Lcom/android/server/ui/display/DisplayODHandler;->ODSwitch()V */
/* .line 53 */
return;
} // .end method
public void onMultiScreenChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "IsMultiScreen" # Z */
/* .line 57 */
/* iput-boolean p1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z */
/* .line 58 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mIsMultiScreen is "; // const-string v1, "mIsMultiScreen is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "UIService-DisplayODHandler"; // const-string v1, "UIService-DisplayODHandler"
com.android.server.ui.utils.LogUtil .logD ( v1,v0 );
/* .line 59 */
/* invoke-direct {p0}, Lcom/android/server/ui/display/DisplayODHandler;->ODSwitch()V */
/* .line 60 */
return;
} // .end method
