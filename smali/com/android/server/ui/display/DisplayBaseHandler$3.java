class com.android.server.ui.display.DisplayBaseHandler$3 implements com.android.server.ui.event_status.MultiScreenHandler$IMultiScreenStatusChangeCallback {
	 /* .source "DisplayBaseHandler.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ui/display/DisplayBaseHandler;->registerMultiScreenEvent()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ui.display.DisplayBaseHandler this$0; //synthetic
/* # direct methods */
 com.android.server.ui.display.DisplayBaseHandler$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ui/display/DisplayBaseHandler; */
/* .line 124 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "MultiScreenStatus" # Z */
/* .line 127 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onChange: MultiScreenStatus = "; // const-string v1, "onChange: MultiScreenStatus = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "UIService-DisplayBaseHandler"; // const-string v1, "UIService-DisplayBaseHandler"
com.android.server.ui.utils.LogUtil .logI ( v1,v0 );
/* .line 128 */
v0 = this.this$0;
int v1 = 3; // const/4 v1, 0x3
java.lang.Boolean .valueOf ( p1 );
(( com.android.server.ui.display.DisplayBaseHandler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/android/server/ui/display/DisplayBaseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( com.android.server.ui.display.DisplayBaseHandler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ui/display/DisplayBaseHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 129 */
return;
} // .end method
