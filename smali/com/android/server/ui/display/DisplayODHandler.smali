.class public Lcom/android/server/ui/display/DisplayODHandler;
.super Lcom/android/server/ui/display/DisplayBaseHandler;
.source "DisplayODHandler.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "UIService-DisplayODHandler"

.field private static mInstance:Lcom/android/server/ui/display/DisplayODHandler;


# instance fields
.field private final COOKIE_OD_MODE:I

.field private final UI_SERVICE_CLOUD_FILE:Ljava/lang/String;

.field private mAppInfo:Lmiui/process/ForegroundInfo;

.field private final mContext:Landroid/content/Context;

.field private mIsMultiScreen:Z

.field private mIsODSwitch:I

.field private final mKey:I

.field private final mOdLocked:Ljava/lang/Object;

.field private mOdPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ui/display/DisplayODHandler;->mInstance:Lcom/android/server/ui/display/DisplayODHandler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 28
    invoke-direct {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;-><init>()V

    .line 19
    const-string/jumbo v0, "ui_service_od_config.xml"

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayODHandler;->UI_SERVICE_CLOUD_FILE:Ljava/lang/String;

    .line 20
    sget v1, Lcom/android/server/ui/utils/SubModule;->ID_OD:I

    iput v1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mKey:I

    .line 21
    const/16 v2, 0x3e7

    iput v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->COOKIE_OD_MODE:I

    .line 22
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mAppInfo:Lmiui/process/ForegroundInfo;

    .line 23
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z

    .line 24
    iput v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsODSwitch:I

    .line 25
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mOdPackages:Ljava/util/ArrayList;

    .line 26
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mOdLocked:Ljava/lang/Object;

    .line 29
    iput-object p1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mContext:Landroid/content/Context;

    .line 30
    invoke-virtual {p0, p1, v1, v0}, Lcom/android/server/ui/display/DisplayODHandler;->init(Landroid/content/Context;ILjava/lang/String;)V

    .line 31
    return-void
.end method

.method private ODSwitch()V
    .locals 6

    .line 63
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayODHandler;->mAppInfo:Lmiui/process/ForegroundInfo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/ui/display/DisplayODHandler;->mOdPackages:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    goto :goto_1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayODHandler;->mOdLocked:Ljava/lang/Object;

    monitor-enter v0

    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mOdPackages:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mAppInfo:Lmiui/process/ForegroundInfo;

    iget-object v2, v2, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 69
    .local v1, "tempSwitch":Z
    if-eqz v1, :cond_1

    iget-boolean v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z

    if-nez v2, :cond_1

    .line 70
    const/4 v2, 0x1

    .local v2, "status":I
    goto :goto_0

    .line 72
    .end local v2    # "status":I
    :cond_1
    const/4 v2, 0x0

    .line 74
    .restart local v2    # "status":I
    :goto_0
    iget v3, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsODSwitch:I

    if-eq v2, v3, :cond_2

    .line 75
    iput v2, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsODSwitch:I

    .line 76
    iget-object v3, p0, Lcom/android/server/ui/display/DisplayODHandler;->mAppInfo:Lmiui/process/ForegroundInfo;

    iget-object v3, v3, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    const/16 v4, 0x3e7

    invoke-virtual {p0, v4, v2, v3}, Lcom/android/server/ui/display/DisplayODHandler;->notifySFMode(IILjava/lang/String;)V

    .line 77
    const-string v3, "UIService-DisplayODHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onForegroundChanged for OD value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mForegroundPackageName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/ui/display/DisplayODHandler;->mAppInfo:Lmiui/process/ForegroundInfo;

    iget-object v5, v5, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " tempSwitch="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .end local v1    # "tempSwitch":Z
    :cond_2
    monitor-exit v0

    .line 81
    return-void

    .line 80
    .end local v2    # "status":I
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 64
    :cond_3
    :goto_1
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/server/ui/display/DisplayODHandler;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/ui/display/DisplayODHandler;

    monitor-enter v0

    .line 34
    :try_start_0
    const-string v1, "UIService-DisplayODHandler"

    const-string v2, "getInstance"

    invoke-static {v1, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    sget-object v1, Lcom/android/server/ui/display/DisplayODHandler;->mInstance:Lcom/android/server/ui/display/DisplayODHandler;

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    .line 36
    new-instance v1, Lcom/android/server/ui/display/DisplayODHandler;

    invoke-direct {v1, p0}, Lcom/android/server/ui/display/DisplayODHandler;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/ui/display/DisplayODHandler;->mInstance:Lcom/android/server/ui/display/DisplayODHandler;

    .line 38
    :cond_0
    sget-object v1, Lcom/android/server/ui/display/DisplayODHandler;->mInstance:Lcom/android/server/ui/display/DisplayODHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 33
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public onCloudUpdate(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 43
    .local p1, "WhitePackageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "UIService-DisplayODHandler"

    const-string v1, "onCloudUpdate"

    invoke-static {v0, v1}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    if-eqz p1, :cond_0

    .line 45
    iput-object p1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mOdPackages:Ljava/util/ArrayList;

    .line 46
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 85
    return-void
.end method

.method public onForegroundChange(Lmiui/process/ForegroundInfo;)V
    .locals 2
    .param p1, "appInfo"    # Lmiui/process/ForegroundInfo;

    .line 50
    iput-object p1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mAppInfo:Lmiui/process/ForegroundInfo;

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mForegroundPackageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mAppInfo:Lmiui/process/ForegroundInfo;

    iget-object v1, v1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UIService-DisplayODHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lcom/android/server/ui/display/DisplayODHandler;->ODSwitch()V

    .line 53
    return-void
.end method

.method public onMultiScreenChange(Z)V
    .locals 2
    .param p1, "IsMultiScreen"    # Z

    .line 57
    iput-boolean p1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mIsMultiScreen is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/ui/display/DisplayODHandler;->mIsMultiScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UIService-DisplayODHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0}, Lcom/android/server/ui/display/DisplayODHandler;->ODSwitch()V

    .line 60
    return-void
.end method
