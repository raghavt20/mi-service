.class public Lcom/android/server/ui/display/DisplayBaseHandler;
.super Landroid/os/Handler;
.source "DisplayBaseHandler.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "UIService-DisplayBaseHandler"


# instance fields
.field private final MSG_ID_CLOUD_CHANGE:I

.field private final MSG_ID_FORGROUND_CHANGE:I

.field private final MSG_ID_MULTISCREEN_CHANGE:I

.field private final SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE:I

.field private UI_SERVICE_CLOUD_FILE:Ljava/lang/String;

.field private mFlinger:Landroid/os/IBinder;

.field private mForegroundStatusHandler:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

.field private mKey:I

.field private mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

.field private mMultiScreenHandler:Lcom/android/server/ui/event_status/MultiScreenHandler;

.field private mWhitePackageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmKey(Lcom/android/server/ui/display/DisplayBaseHandler;)I
    .locals 0

    iget p0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiCommonCloudHelper(Lcom/android/server/ui/display/DisplayBaseHandler;)Lcom/android/server/MiuiCommonCloudHelperStub;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWhitePackageList(Lcom/android/server/ui/display/DisplayBaseHandler;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mWhitePackageList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmWhitePackageList(Lcom/android/server/ui/display/DisplayBaseHandler;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mWhitePackageList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 42
    invoke-static {}, Lcom/android/server/ui/UIService;->getThread()Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->UI_SERVICE_CLOUD_FILE:Ljava/lang/String;

    .line 36
    const/16 v0, 0x7983

    iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->SURFACE_FLINGER_TRANSACTION_DISPLAY_FEATURE:I

    .line 37
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->MSG_ID_CLOUD_CHANGE:I

    .line 38
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->MSG_ID_FORGROUND_CHANGE:I

    .line 39
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->MSG_ID_MULTISCREEN_CHANGE:I

    .line 43
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 59
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "UIService-DisplayBaseHandler"

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 71
    :pswitch_0
    const-string v0, "handleMessage MSG_ID_MultiScreen_CHANGE"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/ui/display/DisplayBaseHandler;->onMultiScreenChange(Z)V

    .line 73
    goto :goto_0

    .line 66
    :pswitch_1
    const-string v0, "handleMessage MSG_ID_FORGROUND_CHANGE"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/process/ForegroundInfo;

    invoke-virtual {p0, v0}, Lcom/android/server/ui/display/DisplayBaseHandler;->onForegroundChange(Lmiui/process/ForegroundInfo;)V

    .line 68
    goto :goto_0

    .line 61
    :pswitch_2
    const-string v0, "handleMessage MSG_ID_CLOUD_CHANGE"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/android/server/ui/display/DisplayBaseHandler;->onCloudUpdate(Ljava/util/ArrayList;)V

    .line 63
    nop

    .line 78
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # I
    .param p3, "cloud_file"    # Ljava/lang/String;

    .line 46
    iput p2, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I

    .line 47
    iput-object p3, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->UI_SERVICE_CLOUD_FILE:Ljava/lang/String;

    .line 48
    invoke-static {}, Lcom/android/server/MiuiCommonCloudHelperStub;->getInstance()Lcom/android/server/MiuiCommonCloudHelperStub;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    .line 49
    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/android/server/MiuiCommonCloudHelperStub;->init(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    .line 50
    invoke-static {p1}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mForegroundStatusHandler:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    .line 51
    invoke-static {p1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/ui/event_status/MultiScreenHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mMultiScreenHandler:Lcom/android/server/ui/event_status/MultiScreenHandler;

    .line 52
    invoke-virtual {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;->registerCloudObserver()V

    .line 53
    invoke-virtual {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;->registerFgEvent()V

    .line 54
    invoke-virtual {p0}, Lcom/android/server/ui/display/DisplayBaseHandler;->registerMultiScreenEvent()V

    .line 55
    return-void
.end method

.method public notifySFMode(IILjava/lang/String;)V
    .locals 6
    .param p1, "cookie"    # I
    .param p2, "status"    # I
    .param p3, "pkg"    # Ljava/lang/String;

    .line 134
    const-string v0, "notifySFMode"

    const-string v1, "UIService-DisplayBaseHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mFlinger:Landroid/os/IBinder;

    if-nez v0, :cond_0

    .line 136
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mFlinger:Landroid/os/IBinder;

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mFlinger:Landroid/os/IBinder;

    if-eqz v0, :cond_1

    .line 138
    const-string v0, "getService succesd"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 140
    .local v0, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 142
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    :try_start_0
    const-string v2, "notify SurfaceFlinger"

    invoke-static {v1, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v2, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mFlinger:Landroid/os/IBinder;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x7983

    invoke-interface {v2, v5, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    :goto_0
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 151
    goto :goto_2

    .line 150
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 147
    :catch_0
    move-exception v2

    .line 148
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to notify SurfaceFlinger"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/server/ui/utils/LogUtil;->logE(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    .line 150
    :goto_1
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 151
    throw v1

    .line 153
    .end local v0    # "data":Landroid/os/Parcel;
    :cond_1
    :goto_2
    return-void
.end method

.method public onCloudUpdate(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 80
    .local p1, "WhitePackageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    return-void
.end method

.method public onForegroundChange(Lmiui/process/ForegroundInfo;)V
    .locals 0
    .param p1, "appInfo"    # Lmiui/process/ForegroundInfo;

    .line 82
    return-void
.end method

.method public onMultiScreenChange(Z)V
    .locals 0
    .param p1, "IsMultiScreen"    # Z

    .line 84
    return-void
.end method

.method public registerCloudObserver()V
    .locals 2

    .line 87
    const-string v0, "UIService-DisplayBaseHandler"

    const-string v1, "registerCloudObserver"

    invoke-static {v0, v1}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    new-instance v1, Lcom/android/server/ui/display/DisplayBaseHandler$1;

    invoke-direct {v1, p0}, Lcom/android/server/ui/display/DisplayBaseHandler$1;-><init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V

    invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->registerObserver(Lcom/android/server/MiuiCommonCloudHelperStub$Observer;)V

    .line 105
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    iget-object v1, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->UI_SERVICE_CLOUD_FILE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/MiuiCommonCloudHelperStub;->initFromAssets(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mMiuiCommonCloudHelper:Lcom/android/server/MiuiCommonCloudHelperStub;

    invoke-virtual {v0}, Lcom/android/server/MiuiCommonCloudHelperStub;->startCloudObserve()V

    .line 107
    return-void
.end method

.method public registerFgEvent()V
    .locals 3

    .line 110
    const-string v0, "UIService-DisplayBaseHandler"

    const-string v1, "registerFgEvent"

    invoke-static {v0, v1}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mForegroundStatusHandler:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    iget v1, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I

    new-instance v2, Lcom/android/server/ui/display/DisplayBaseHandler$2;

    invoke-direct {v2, p0}, Lcom/android/server/ui/display/DisplayBaseHandler$2;-><init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V

    invoke-virtual {v0, v1, v2}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->addFgStatusChangeCallback(ILcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;)V

    .line 120
    return-void
.end method

.method public registerMultiScreenEvent()V
    .locals 3

    .line 123
    const-string v0, "UIService-DisplayBaseHandler"

    const-string v1, "registerMultiScreenEvent"

    invoke-static {v0, v1}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mMultiScreenHandler:Lcom/android/server/ui/event_status/MultiScreenHandler;

    iget v1, p0, Lcom/android/server/ui/display/DisplayBaseHandler;->mKey:I

    new-instance v2, Lcom/android/server/ui/display/DisplayBaseHandler$3;

    invoke-direct {v2, p0}, Lcom/android/server/ui/display/DisplayBaseHandler$3;-><init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V

    invoke-virtual {v0, v1, v2}, Lcom/android/server/ui/event_status/MultiScreenHandler;->addMultiScreenStatusChangeCallback(ILcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;)V

    .line 131
    return-void
.end method
