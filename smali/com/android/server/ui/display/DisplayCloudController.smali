.class public Lcom/android/server/ui/display/DisplayCloudController;
.super Ljava/lang/Object;
.source "DisplayCloudController.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "UIService-DisplayCloudController"

.field private static mInstance:Lcom/android/server/ui/display/DisplayCloudController;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mForegroundStatusHandler:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

.field private mMultiScreenHandler:Lcom/android/server/ui/event_status/MultiScreenHandler;

.field private mOD:Lcom/android/server/ui/display/DisplayODHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ui/display/DisplayCloudController;->mInstance:Lcom/android/server/ui/display/DisplayCloudController;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/android/server/ui/display/DisplayCloudController;->mContext:Landroid/content/Context;

    .line 22
    const-string v0, "ro.vendor.display.uiservice.enable"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-static {p1}, Lcom/android/server/ui/display/DisplayODHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/ui/display/DisplayODHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayCloudController;->mOD:Lcom/android/server/ui/display/DisplayODHandler;

    .line 25
    :cond_0
    invoke-static {p1}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayCloudController;->mForegroundStatusHandler:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    .line 26
    invoke-static {p1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->getInstance(Landroid/content/Context;)Lcom/android/server/ui/event_status/MultiScreenHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/display/DisplayCloudController;->mMultiScreenHandler:Lcom/android/server/ui/event_status/MultiScreenHandler;

    .line 27
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/server/ui/display/DisplayCloudController;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/ui/display/DisplayCloudController;

    monitor-enter v0

    .line 30
    :try_start_0
    const-string v1, "UIService-DisplayCloudController"

    const-string v2, "getInstance"

    invoke-static {v1, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    sget-object v1, Lcom/android/server/ui/display/DisplayCloudController;->mInstance:Lcom/android/server/ui/display/DisplayCloudController;

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    .line 32
    new-instance v1, Lcom/android/server/ui/display/DisplayCloudController;

    invoke-direct {v1, p0}, Lcom/android/server/ui/display/DisplayCloudController;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/ui/display/DisplayCloudController;->mInstance:Lcom/android/server/ui/display/DisplayCloudController;

    .line 34
    :cond_0
    sget-object v1, Lcom/android/server/ui/display/DisplayCloudController;->mInstance:Lcom/android/server/ui/display/DisplayCloudController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 29
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method
