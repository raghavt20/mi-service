class com.android.server.ui.display.DisplayBaseHandler$1 implements com.android.server.MiuiCommonCloudHelperStub$Observer {
	 /* .source "DisplayBaseHandler.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ui/display/DisplayBaseHandler;->registerCloudObserver()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ui.display.DisplayBaseHandler this$0; //synthetic
/* # direct methods */
 com.android.server.ui.display.DisplayBaseHandler$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ui/display/DisplayBaseHandler; */
/* .line 88 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void update ( ) {
/* .locals 4 */
/* .line 91 */
final String v0 = "cloud data update:"; // const-string v0, "cloud data update:"
final String v1 = "UIService-DisplayBaseHandler"; // const-string v1, "UIService-DisplayBaseHandler"
com.android.server.ui.utils.LogUtil .logI ( v1,v0 );
/* .line 92 */
v0 = this.this$0;
com.android.server.ui.display.DisplayBaseHandler .-$$Nest$fgetmMiuiCommonCloudHelper ( v0 );
v2 = com.android.server.ui.utils.SubModule.SUB_MODULE_LIST;
v3 = this.this$0;
v3 = com.android.server.ui.display.DisplayBaseHandler .-$$Nest$fgetmKey ( v3 );
(( java.util.ArrayList ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
(( com.android.server.MiuiCommonCloudHelperStub ) v0 ).getDataByModuleName ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/MiuiCommonCloudHelperStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/ui/utils/WhitePackageInfo; */
/* .line 93 */
/* .local v0, "whitePackageInfo":Lcom/android/server/ui/utils/WhitePackageInfo; */
v2 = this.this$0;
(( com.android.server.ui.utils.WhitePackageInfo ) v0 ).getWhitePackageList ( ); // invoke-virtual {v0}, Lcom/android/server/ui/utils/WhitePackageInfo;->getWhitePackageList()Ljava/util/ArrayList;
com.android.server.ui.display.DisplayBaseHandler .-$$Nest$fputmWhitePackageList ( v2,v3 );
/* .line 94 */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 95 */
	 (( com.android.server.ui.utils.WhitePackageInfo ) v0 ).getWhitePackageList ( ); // invoke-virtual {v0}, Lcom/android/server/ui/utils/WhitePackageInfo;->getWhitePackageList()Ljava/util/ArrayList;
	 (( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
	 /* check-cast v3, Ljava/lang/String; */
	 /* .line 96 */
	 /* .local v3, "data":Ljava/lang/String; */
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* .line 97 */
		 com.android.server.ui.utils.LogUtil .logI ( v1,v3 );
		 /* .line 99 */
	 } // .end local v3 # "data":Ljava/lang/String;
} // :cond_0
/* .line 101 */
} // :cond_1
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.android.server.ui.display.DisplayBaseHandler .-$$Nest$fgetmWhitePackageList ( v1 );
(( com.android.server.ui.display.DisplayBaseHandler ) v1 ).obtainMessage ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lcom/android/server/ui/display/DisplayBaseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( com.android.server.ui.display.DisplayBaseHandler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/ui/display/DisplayBaseHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 102 */
return;
} // .end method
