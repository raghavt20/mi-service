.class Lcom/android/server/ui/display/DisplayBaseHandler$1;
.super Ljava/lang/Object;
.source "DisplayBaseHandler.java"

# interfaces
.implements Lcom/android/server/MiuiCommonCloudHelperStub$Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/ui/display/DisplayBaseHandler;->registerCloudObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ui/display/DisplayBaseHandler;


# direct methods
.method constructor <init>(Lcom/android/server/ui/display/DisplayBaseHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ui/display/DisplayBaseHandler;

    .line 88
    iput-object p1, p0, Lcom/android/server/ui/display/DisplayBaseHandler$1;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update()V
    .locals 4

    .line 91
    const-string v0, "cloud data update:"

    const-string v1, "UIService-DisplayBaseHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/android/server/ui/display/DisplayBaseHandler$1;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    invoke-static {v0}, Lcom/android/server/ui/display/DisplayBaseHandler;->-$$Nest$fgetmMiuiCommonCloudHelper(Lcom/android/server/ui/display/DisplayBaseHandler;)Lcom/android/server/MiuiCommonCloudHelperStub;

    move-result-object v0

    sget-object v2, Lcom/android/server/ui/utils/SubModule;->SUB_MODULE_LIST:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/server/ui/display/DisplayBaseHandler$1;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    invoke-static {v3}, Lcom/android/server/ui/display/DisplayBaseHandler;->-$$Nest$fgetmKey(Lcom/android/server/ui/display/DisplayBaseHandler;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/server/MiuiCommonCloudHelperStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ui/utils/WhitePackageInfo;

    .line 93
    .local v0, "whitePackageInfo":Lcom/android/server/ui/utils/WhitePackageInfo;
    iget-object v2, p0, Lcom/android/server/ui/display/DisplayBaseHandler$1;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    invoke-virtual {v0}, Lcom/android/server/ui/utils/WhitePackageInfo;->getWhitePackageList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ui/display/DisplayBaseHandler;->-$$Nest$fputmWhitePackageList(Lcom/android/server/ui/display/DisplayBaseHandler;Ljava/util/ArrayList;)V

    .line 94
    if-eqz v0, :cond_1

    .line 95
    invoke-virtual {v0}, Lcom/android/server/ui/utils/WhitePackageInfo;->getWhitePackageList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 96
    .local v3, "data":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 97
    invoke-static {v1, v3}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .end local v3    # "data":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/android/server/ui/display/DisplayBaseHandler$1;->this$0:Lcom/android/server/ui/display/DisplayBaseHandler;

    const/4 v2, 0x1

    invoke-static {v1}, Lcom/android/server/ui/display/DisplayBaseHandler;->-$$Nest$fgetmWhitePackageList(Lcom/android/server/ui/display/DisplayBaseHandler;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/ui/display/DisplayBaseHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/ui/display/DisplayBaseHandler;->sendMessage(Landroid/os/Message;)Z

    .line 102
    return-void
.end method
