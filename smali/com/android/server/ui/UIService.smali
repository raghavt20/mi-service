.class public Lcom/android/server/ui/UIService;
.super Ljava/lang/Object;
.source "UIService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ui/UIService$Lifecycle;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UIService"

.field private static mThread:Landroid/os/HandlerThread;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDisplayCloudController:Lcom/android/server/ui/display/DisplayCloudController;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "UIService"

    const-string/jumbo v1, "start uiservice"

    invoke-static {v0, v1}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/android/server/ui/UIService;->mContext:Landroid/content/Context;

    .line 36
    invoke-static {p1}, Lcom/android/server/ui/display/DisplayCloudController;->getInstance(Landroid/content/Context;)Lcom/android/server/ui/display/DisplayCloudController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/UIService;->mDisplayCloudController:Lcom/android/server/ui/display/DisplayCloudController;

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/server/ui/UIService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/server/ui/UIService;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private static ensureThreadLocked()V
    .locals 3

    .line 22
    sget-object v0, Lcom/android/server/ui/UIService;->mThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "UIService"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ui/UIService;->mThread:Landroid/os/HandlerThread;

    .line 24
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 26
    :cond_0
    return-void
.end method

.method public static declared-synchronized getThread()Landroid/os/HandlerThread;
    .locals 2

    const-class v0, Lcom/android/server/ui/UIService;

    monitor-enter v0

    .line 29
    :try_start_0
    invoke-static {}, Lcom/android/server/ui/UIService;->ensureThreadLocked()V

    .line 30
    sget-object v1, Lcom/android/server/ui/UIService;->mThread:Landroid/os/HandlerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 28
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
