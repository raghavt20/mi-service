class com.android.server.ui.event_status.ForegroundStatusHandler$1 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "ForegroundStatusHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ui/event_status/ForegroundStatusHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ui.event_status.ForegroundStatusHandler this$0; //synthetic
/* # direct methods */
 com.android.server.ui.event_status.ForegroundStatusHandler$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ui/event_status/ForegroundStatusHandler; */
/* .line 33 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 2 */
/* .param p1, "appInfo" # Lmiui/process/ForegroundInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 36 */
final String v0 = "onForegroundInfoChanged"; // const-string v0, "onForegroundInfoChanged"
final String v1 = "UIService-ForegroundStatusHandler"; // const-string v1, "UIService-ForegroundStatusHandler"
com.android.server.ui.utils.LogUtil .logD ( v1,v0 );
/* .line 37 */
/* if-nez p1, :cond_0 */
/* .line 38 */
final String v0 = "appInfo is null"; // const-string v0, "appInfo is null"
com.android.server.ui.utils.LogUtil .logI ( v1,v0 );
/* .line 39 */
return;
/* .line 41 */
} // :cond_0
v0 = this.this$0;
com.android.server.ui.event_status.ForegroundStatusHandler .-$$Nest$fputmCurrentFgAppInfo ( v0,p1 );
/* .line 42 */
v0 = this.this$0;
(( com.android.server.ui.event_status.ForegroundStatusHandler ) v0 ).updataAppInfo ( ); // invoke-virtual {v0}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->updataAppInfo()V
/* .line 43 */
return;
} // .end method
