.class public Lcom/android/server/ui/event_status/ForegroundStatusHandler;
.super Ljava/lang/Object;
.source "ForegroundStatusHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "UIService-ForegroundStatusHandler"

.field private static mInstance:Lcom/android/server/ui/event_status/ForegroundStatusHandler;


# instance fields
.field private mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

.field private final mCallbacks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mCurrentFgAppInfo:Lmiui/process/ForegroundInfo;


# direct methods
.method static bridge synthetic -$$Nest$fputmCurrentFgAppInfo(Lcom/android/server/ui/event_status/ForegroundStatusHandler;Lmiui/process/ForegroundInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCurrentFgAppInfo:Lmiui/process/ForegroundInfo;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 14
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mInstance:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCallbacks:Landroid/util/SparseArray;

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCurrentFgAppInfo:Lmiui/process/ForegroundInfo;

    .line 33
    new-instance v0, Lcom/android/server/ui/event_status/ForegroundStatusHandler$1;

    invoke-direct {v0, p0}, Lcom/android/server/ui/event_status/ForegroundStatusHandler$1;-><init>(Lcom/android/server/ui/event_status/ForegroundStatusHandler;)V

    iput-object v0, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mAppObserver:Lmiui/process/IForegroundInfoListener$Stub;

    .line 28
    iput-object p1, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mContext:Landroid/content/Context;

    .line 29
    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 30
    invoke-static {}, Lmiui/process/ProcessManager;->getForegroundInfo()Lmiui/process/ForegroundInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCurrentFgAppInfo:Lmiui/process/ForegroundInfo;

    .line 31
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/server/ui/event_status/ForegroundStatusHandler;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    monitor-enter v0

    .line 20
    :try_start_0
    const-string v1, "UIService-ForegroundStatusHandler"

    const-string v2, "getInstance"

    invoke-static {v1, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    sget-object v1, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mInstance:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    .line 22
    new-instance v1, Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    invoke-direct {v1, p0}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mInstance:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    .line 24
    :cond_0
    sget-object v1, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mInstance:Lcom/android/server/ui/event_status/ForegroundStatusHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 19
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public addFgStatusChangeCallback(ILcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;)V
    .locals 1
    .param p1, "key"    # I
    .param p2, "callback"    # Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;

    .line 47
    if-nez p2, :cond_0

    .line 48
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 50
    return-void
.end method

.method public updataAppInfo()V
    .locals 4

    .line 53
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 55
    .local v1, "key":I
    iget-object v2, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;

    iget-object v3, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->mCurrentFgAppInfo:Lmiui/process/ForegroundInfo;

    invoke-interface {v2, v3}, Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;->onChange(Lmiui/process/ForegroundInfo;)V

    .line 53
    .end local v1    # "key":I
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    .end local v0    # "i":I
    :cond_0
    return-void
.end method
