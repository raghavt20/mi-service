.class Lcom/android/server/ui/event_status/ForegroundStatusHandler$1;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "ForegroundStatusHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ui/event_status/ForegroundStatusHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ui/event_status/ForegroundStatusHandler;


# direct methods
.method constructor <init>(Lcom/android/server/ui/event_status/ForegroundStatusHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    .line 33
    iput-object p1, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler$1;->this$0:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 2
    .param p1, "appInfo"    # Lmiui/process/ForegroundInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 36
    const-string v0, "onForegroundInfoChanged"

    const-string v1, "UIService-ForegroundStatusHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    if-nez p1, :cond_0

    .line 38
    const-string v0, "appInfo is null"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler$1;->this$0:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    invoke-static {v0, p1}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->-$$Nest$fputmCurrentFgAppInfo(Lcom/android/server/ui/event_status/ForegroundStatusHandler;Lmiui/process/ForegroundInfo;)V

    .line 42
    iget-object v0, p0, Lcom/android/server/ui/event_status/ForegroundStatusHandler$1;->this$0:Lcom/android/server/ui/event_status/ForegroundStatusHandler;

    invoke-virtual {v0}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;->updataAppInfo()V

    .line 43
    return-void
.end method
