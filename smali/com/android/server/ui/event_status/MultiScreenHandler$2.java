class com.android.server.ui.event_status.MultiScreenHandler$2 extends android.app.TaskStackListener {
	 /* .source "MultiScreenHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ui/event_status/MultiScreenHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ui.event_status.MultiScreenHandler this$0; //synthetic
/* # direct methods */
 com.android.server.ui.event_status.MultiScreenHandler$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ui/event_status/MultiScreenHandler; */
/* .line 105 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/TaskStackListener;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onTaskStackChanged ( ) {
/* .locals 2 */
/* .line 108 */
v0 = this.this$0;
v1 = com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$misInSplitScreenMode ( v0 );
com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fputmIsSplitScreen ( v0,v1 );
/* .line 110 */
v0 = this.this$0;
v0 = com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fgetmIsSplitScreen ( v0 );
/* if-nez v0, :cond_1 */
v0 = this.this$0;
v0 = com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fgetmIsSmallScreen ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 113 */
} // :cond_0
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.ui.event_status.MultiScreenHandler ) v0 ).onMultiScreenChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V
/* .line 111 */
} // :cond_1
} // :goto_0
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
(( com.android.server.ui.event_status.MultiScreenHandler ) v0 ).onMultiScreenChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V
/* .line 115 */
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onTaskStackChanged, splitScreenMode: "; // const-string v1, "onTaskStackChanged, splitScreenMode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fgetmIsSplitScreen ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " isSmallScreen: "; // const-string v1, " isSmallScreen: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fgetmIsSmallScreen ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "UIService-MultiScreenHandler"; // const-string v1, "UIService-MultiScreenHandler"
com.android.server.ui.utils.LogUtil .logI ( v1,v0 );
/* .line 116 */
return;
} // .end method
