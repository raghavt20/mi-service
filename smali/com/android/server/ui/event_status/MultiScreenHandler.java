public class com.android.server.ui.event_status.MultiScreenHandler {
	 /* .source "MultiScreenHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ACTIVITYTASKMANAGER;
private static final java.lang.String METHOD_GETSERVICE;
private static final java.lang.String TAG;
private static com.android.server.ui.event_status.MultiScreenHandler mInstance;
/* # instance fields */
private final android.util.SparseArray mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
private miui.app.IFreeformCallback mFreeformCallback;
private Boolean mIsMultiScreen;
private Boolean mIsSmallScreen;
private Boolean mIsSplitScreen;
private android.app.TaskStackListener mTaskStackListener;
/* # direct methods */
static Boolean -$$Nest$fgetmIsSmallScreen ( com.android.server.ui.event_status.MultiScreenHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z */
} // .end method
static Boolean -$$Nest$fgetmIsSplitScreen ( com.android.server.ui.event_status.MultiScreenHandler p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z */
} // .end method
static void -$$Nest$fputmIsSmallScreen ( com.android.server.ui.event_status.MultiScreenHandler p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z */
return;
} // .end method
static void -$$Nest$fputmIsSplitScreen ( com.android.server.ui.event_status.MultiScreenHandler p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z */
return;
} // .end method
static Boolean -$$Nest$misInSplitScreenMode ( com.android.server.ui.event_status.MultiScreenHandler p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->isInSplitScreenMode()Z */
} // .end method
static com.android.server.ui.event_status.MultiScreenHandler ( ) {
/* .locals 1 */
/* .line 16 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.android.server.ui.event_status.MultiScreenHandler ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 33 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 20 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z */
/* .line 21 */
/* iput-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z */
/* .line 22 */
/* iput-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z */
/* .line 23 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mCallbacks = v0;
/* .line 45 */
/* new-instance v0, Lcom/android/server/ui/event_status/MultiScreenHandler$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/ui/event_status/MultiScreenHandler$1;-><init>(Lcom/android/server/ui/event_status/MultiScreenHandler;)V */
this.mFreeformCallback = v0;
/* .line 105 */
/* new-instance v0, Lcom/android/server/ui/event_status/MultiScreenHandler$2; */
/* invoke-direct {v0, p0}, Lcom/android/server/ui/event_status/MultiScreenHandler$2;-><init>(Lcom/android/server/ui/event_status/MultiScreenHandler;)V */
this.mTaskStackListener = v0;
/* .line 34 */
this.mContext = p1;
/* .line 35 */
v0 = this.mFreeformCallback;
miui.app.MiuiFreeFormManager .registerFreeformCallback ( v0 );
/* .line 36 */
(( com.android.server.ui.event_status.MultiScreenHandler ) p0 ).registerTaskStackListener ( ); // invoke-virtual {p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->registerTaskStackListener()V
/* .line 37 */
return;
} // .end method
private android.app.IActivityTaskManager getActivityTaskManager ( ) {
/* .locals 1 */
/* .line 119 */
android.app.ActivityTaskManager .getService ( );
} // .end method
public static synchronized com.android.server.ui.event_status.MultiScreenHandler getInstance ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/android/server/ui/event_status/MultiScreenHandler; */
/* monitor-enter v0 */
/* .line 26 */
try { // :try_start_0
final String v1 = "UIService-MultiScreenHandler"; // const-string v1, "UIService-MultiScreenHandler"
final String v2 = "getInstance"; // const-string v2, "getInstance"
com.android.server.ui.utils.LogUtil .logD ( v1,v2 );
/* .line 27 */
v1 = com.android.server.ui.event_status.MultiScreenHandler.mInstance;
/* if-nez v1, :cond_0 */
if ( p0 != null) { // if-eqz p0, :cond_0
	 /* .line 28 */
	 /* new-instance v1, Lcom/android/server/ui/event_status/MultiScreenHandler; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;-><init>(Landroid/content/Context;)V */
	 /* .line 30 */
} // :cond_0
v1 = com.android.server.ui.event_status.MultiScreenHandler.mInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 25 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
private Boolean isInSplitScreenMode ( ) {
/* .locals 6 */
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
final String v1 = "android.app.ActivityTaskManager"; // const-string v1, "android.app.ActivityTaskManager"
java.lang.Class .forName ( v1 );
/* .line 96 */
/* .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v2 = "getService"; // const-string v2, "getService"
/* new-array v3, v0, [Ljava/lang/Class; */
(( java.lang.Class ) v1 ).getMethod ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* new-array v3, v0, [Ljava/lang/Object; */
int v4 = 0; // const/4 v4, 0x0
(( java.lang.reflect.Method ) v2 ).invoke ( v4, v3 ); // invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 97 */
/* .local v2, "obj":Ljava/lang/Object; */
(( java.lang.Object ) v2 ).getClass ( ); // invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
final String v4 = "isInSplitScreenWindowingMode"; // const-string v4, "isInSplitScreenWindowingMode"
/* new-array v5, v0, [Ljava/lang/Class; */
(( java.lang.Class ) v3 ).getMethod ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 98 */
/* .local v3, "method":Ljava/lang/reflect/Method; */
/* new-array v4, v0, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v3 ).invoke ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v4 ).booleanValue ( ); // invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 99 */
} // .end local v1 # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v2 # "obj":Ljava/lang/Object;
} // .end local v3 # "method":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v1 */
/* .line 100 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getStackInfo exception : "; // const-string v3, "getStackInfo exception : "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "UIService-MultiScreenHandler"; // const-string v3, "UIService-MultiScreenHandler"
com.android.server.ui.utils.LogUtil .logE ( v3,v2 );
/* .line 102 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
/* # virtual methods */
public void addMultiScreenStatusChangeCallback ( Integer p0, com.android.server.ui.event_status.MultiScreenHandler$IMultiScreenStatusChangeCallback p1 ) {
/* .locals 1 */
/* .param p1, "key" # I */
/* .param p2, "callback" # Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback; */
/* .line 40 */
/* if-nez p2, :cond_0 */
/* .line 41 */
return;
/* .line 42 */
} // :cond_0
v0 = this.mCallbacks;
(( android.util.SparseArray ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 43 */
return;
} // .end method
public void onMultiScreenChanged ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "isMultiScreen" # Z */
/* .line 130 */
/* iget-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z */
/* if-eq v0, p1, :cond_0 */
/* .line 131 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "onMultiScreenChanged, splitScreenMode: "; // const-string v1, "onMultiScreenChanged, splitScreenMode: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " isSmallScreen: "; // const-string v1, " isSmallScreen: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "UIService-MultiScreenHandler"; // const-string v1, "UIService-MultiScreenHandler"
com.android.server.ui.utils.LogUtil .logD ( v1,v0 );
/* .line 132 */
/* iput-boolean p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z */
/* .line 133 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mCallbacks;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_0 */
/* .line 134 */
v1 = this.mCallbacks;
v1 = (( android.util.SparseArray ) v1 ).keyAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 135 */
/* .local v1, "key":I */
v2 = this.mCallbacks;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback; */
/* iget-boolean v3, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z */
/* .line 133 */
} // .end local v1 # "key":I
/* add-int/lit8 v0, v0, 0x1 */
/* .line 138 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public void registerTaskStackListener ( ) {
/* .locals 3 */
/* .line 123 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->getActivityTaskManager()Landroid/app/IActivityTaskManager; */
v1 = this.mTaskStackListener;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 126 */
/* .line 124 */
/* :catch_0 */
/* move-exception v0 */
/* .line 125 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "UIService-MultiScreenHandler"; // const-string v1, "UIService-MultiScreenHandler"
final String v2 = "Faild to call registerTaskStackListener"; // const-string v2, "Faild to call registerTaskStackListener"
com.android.server.ui.utils.LogUtil .logW ( v1,v2 );
/* .line 127 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
