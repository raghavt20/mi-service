.class Lcom/android/server/ui/event_status/MultiScreenHandler$2;
.super Landroid/app/TaskStackListener;
.source "MultiScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ui/event_status/MultiScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;


# direct methods
.method constructor <init>(Lcom/android/server/ui/event_status/MultiScreenHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ui/event_status/MultiScreenHandler;

    .line 105
    iput-object p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-direct {p0}, Landroid/app/TaskStackListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTaskStackChanged()V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$misInSplitScreenMode(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fputmIsSplitScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V

    .line 110
    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fgetmIsSplitScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fgetmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V

    goto :goto_1

    .line 111
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V

    .line 115
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onTaskStackChanged, splitScreenMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fgetmIsSplitScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSmallScreen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fgetmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UIService-MultiScreenHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logI(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void
.end method
