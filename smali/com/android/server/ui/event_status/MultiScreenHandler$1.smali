.class Lcom/android/server/ui/event_status/MultiScreenHandler$1;
.super Lmiui/app/IFreeformCallback$Stub;
.source "MultiScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/ui/event_status/MultiScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;


# direct methods
.method constructor <init>(Lcom/android/server/ui/event_status/MultiScreenHandler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/android/server/ui/event_status/MultiScreenHandler;

    .line 45
    iput-object p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-direct {p0}, Lmiui/app/IFreeformCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchFreeFormStackModeChanged(ILmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 6
    .param p1, "action"    # I
    .param p2, "stackInfo"    # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    .line 48
    const/4 v0, 0x0

    const/4 v1, 0x1

    const-string v2, ", state:"

    const-string v3, "UIService-MultiScreenHandler"

    packed-switch p1, :pswitch_data_0

    .line 83
    const-string/jumbo v2, "warning for access here"

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logW(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 79
    :pswitch_0
    iget-object v4, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v4, v0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fputmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V

    .line 80
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MINIFREEFORM_TO_FULLSCREEN:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    goto/16 :goto_0

    .line 74
    :pswitch_1
    iget-object v4, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v4, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fputmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V

    .line 75
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MINIFREEFORM_TO_FREEFORM:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    goto/16 :goto_0

    .line 69
    :pswitch_2
    iget-object v4, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v4, v0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fputmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V

    .line 70
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FREEFORM_TO_FULLSCREEN:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    goto :goto_0

    .line 64
    :pswitch_3
    iget-object v4, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v4, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fputmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V

    .line 65
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FREEFORM_TO_MINIFREEFORM:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    goto :goto_0

    .line 59
    :pswitch_4
    iget-object v4, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v4, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fputmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V

    .line 60
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FULLSCREEN_TO_MINIFREEFORM:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    goto :goto_0

    .line 54
    :pswitch_5
    iget-object v4, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v4, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fputmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V

    .line 55
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FULLSCREEN_TO_FREEFORM:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    nop

    .line 85
    :goto_0
    iget-object v2, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v2}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fgetmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-static {v2}, Lcom/android/server/ui/event_status/MultiScreenHandler;->-$$Nest$fgetmIsSplitScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V

    goto :goto_2

    .line 86
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;->this$0:Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-virtual {v0, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V

    .line 90
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
