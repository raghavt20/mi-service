class com.android.server.ui.event_status.MultiScreenHandler$1 extends miui.app.IFreeformCallback$Stub {
	 /* .source "MultiScreenHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/ui/event_status/MultiScreenHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ui.event_status.MultiScreenHandler this$0; //synthetic
/* # direct methods */
 com.android.server.ui.event_status.MultiScreenHandler$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ui/event_status/MultiScreenHandler; */
/* .line 45 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/app/IFreeformCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void dispatchFreeFormStackModeChanged ( Integer p0, miui.app.MiuiFreeFormManager$MiuiFreeFormStackInfo p1 ) {
/* .locals 6 */
/* .param p1, "action" # I */
/* .param p2, "stackInfo" # Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo; */
/* .line 48 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 1; // const/4 v1, 0x1
final String v2 = ", state:"; // const-string v2, ", state:"
final String v3 = "UIService-MultiScreenHandler"; // const-string v3, "UIService-MultiScreenHandler"
/* packed-switch p1, :pswitch_data_0 */
/* .line 83 */
/* const-string/jumbo v2, "warning for access here" */
com.android.server.ui.utils.LogUtil .logW ( v3,v2 );
/* goto/16 :goto_0 */
/* .line 79 */
/* :pswitch_0 */
v4 = this.this$0;
com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fputmIsSmallScreen ( v4,v0 );
/* .line 80 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MINIFREEFORM_TO_FULLSCREEN:"; // const-string v5, "MINIFREEFORM_TO_FULLSCREEN:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.packageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logD ( v3,v2 );
/* .line 81 */
/* goto/16 :goto_0 */
/* .line 74 */
/* :pswitch_1 */
v4 = this.this$0;
com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fputmIsSmallScreen ( v4,v1 );
/* .line 75 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "MINIFREEFORM_TO_FREEFORM:"; // const-string v5, "MINIFREEFORM_TO_FREEFORM:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.packageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logD ( v3,v2 );
/* .line 76 */
/* goto/16 :goto_0 */
/* .line 69 */
/* :pswitch_2 */
v4 = this.this$0;
com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fputmIsSmallScreen ( v4,v0 );
/* .line 70 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "FREEFORM_TO_FULLSCREEN:"; // const-string v5, "FREEFORM_TO_FULLSCREEN:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.packageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logD ( v3,v2 );
/* .line 71 */
/* .line 64 */
/* :pswitch_3 */
v4 = this.this$0;
com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fputmIsSmallScreen ( v4,v1 );
/* .line 65 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "FREEFORM_TO_MINIFREEFORM:"; // const-string v5, "FREEFORM_TO_MINIFREEFORM:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.packageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logD ( v3,v2 );
/* .line 66 */
/* .line 59 */
/* :pswitch_4 */
v4 = this.this$0;
com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fputmIsSmallScreen ( v4,v1 );
/* .line 60 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "FULLSCREEN_TO_MINIFREEFORM:"; // const-string v5, "FULLSCREEN_TO_MINIFREEFORM:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.packageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logD ( v3,v2 );
/* .line 61 */
/* .line 54 */
/* :pswitch_5 */
v4 = this.this$0;
com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fputmIsSmallScreen ( v4,v1 );
/* .line 55 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "FULLSCREEN_TO_FREEFORM:"; // const-string v5, "FULLSCREEN_TO_FREEFORM:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.packageName;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p2, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->windowState:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.ui.utils.LogUtil .logD ( v3,v2 );
/* .line 56 */
/* nop */
/* .line 85 */
} // :goto_0
v2 = this.this$0;
v2 = com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fgetmIsSmallScreen ( v2 );
/* if-nez v2, :cond_1 */
v2 = this.this$0;
v2 = com.android.server.ui.event_status.MultiScreenHandler .-$$Nest$fgetmIsSplitScreen ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 88 */
} // :cond_0
v1 = this.this$0;
(( com.android.server.ui.event_status.MultiScreenHandler ) v1 ).onMultiScreenChanged ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V
/* .line 86 */
} // :cond_1
} // :goto_1
v0 = this.this$0;
(( com.android.server.ui.event_status.MultiScreenHandler ) v0 ).onMultiScreenChanged ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ui/event_status/MultiScreenHandler;->onMultiScreenChanged(Z)V
/* .line 90 */
} // :goto_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
