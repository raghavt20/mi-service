.class public Lcom/android/server/ui/event_status/MultiScreenHandler;
.super Ljava/lang/Object;
.source "MultiScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;
    }
.end annotation


# static fields
.field private static final ACTIVITYTASKMANAGER:Ljava/lang/String; = "android.app.ActivityTaskManager"

.field private static final METHOD_GETSERVICE:Ljava/lang/String; = "getService"

.field private static final TAG:Ljava/lang/String; = "UIService-MultiScreenHandler"

.field private static mInstance:Lcom/android/server/ui/event_status/MultiScreenHandler;


# instance fields
.field private final mCallbacks:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mFreeformCallback:Lmiui/app/IFreeformCallback;

.field private mIsMultiScreen:Z

.field private mIsSmallScreen:Z

.field private mIsSplitScreen:Z

.field private mTaskStackListener:Landroid/app/TaskStackListener;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSplitScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmIsSmallScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsSplitScreen(Lcom/android/server/ui/event_status/MultiScreenHandler;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$misInSplitScreenMode(Lcom/android/server/ui/event_status/MultiScreenHandler;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->isInSplitScreenMode()Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mInstance:Lcom/android/server/ui/event_status/MultiScreenHandler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z

    .line 21
    iput-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z

    .line 22
    iput-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z

    .line 23
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mCallbacks:Landroid/util/SparseArray;

    .line 45
    new-instance v0, Lcom/android/server/ui/event_status/MultiScreenHandler$1;

    invoke-direct {v0, p0}, Lcom/android/server/ui/event_status/MultiScreenHandler$1;-><init>(Lcom/android/server/ui/event_status/MultiScreenHandler;)V

    iput-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mFreeformCallback:Lmiui/app/IFreeformCallback;

    .line 105
    new-instance v0, Lcom/android/server/ui/event_status/MultiScreenHandler$2;

    invoke-direct {v0, p0}, Lcom/android/server/ui/event_status/MultiScreenHandler$2;-><init>(Lcom/android/server/ui/event_status/MultiScreenHandler;)V

    iput-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mTaskStackListener:Landroid/app/TaskStackListener;

    .line 34
    iput-object p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mContext:Landroid/content/Context;

    .line 35
    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mFreeformCallback:Lmiui/app/IFreeformCallback;

    invoke-static {v0}, Lmiui/app/MiuiFreeFormManager;->registerFreeformCallback(Lmiui/app/IFreeformCallback;)V

    .line 36
    invoke-virtual {p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->registerTaskStackListener()V

    .line 37
    return-void
.end method

.method private getActivityTaskManager()Landroid/app/IActivityTaskManager;
    .locals 1

    .line 119
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/android/server/ui/event_status/MultiScreenHandler;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/android/server/ui/event_status/MultiScreenHandler;

    monitor-enter v0

    .line 26
    :try_start_0
    const-string v1, "UIService-MultiScreenHandler"

    const-string v2, "getInstance"

    invoke-static {v1, v2}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    sget-object v1, Lcom/android/server/ui/event_status/MultiScreenHandler;->mInstance:Lcom/android/server/ui/event_status/MultiScreenHandler;

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    .line 28
    new-instance v1, Lcom/android/server/ui/event_status/MultiScreenHandler;

    invoke-direct {v1, p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/server/ui/event_status/MultiScreenHandler;->mInstance:Lcom/android/server/ui/event_status/MultiScreenHandler;

    .line 30
    :cond_0
    sget-object v1, Lcom/android/server/ui/event_status/MultiScreenHandler;->mInstance:Lcom/android/server/ui/event_status/MultiScreenHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 25
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private isInSplitScreenMode()Z
    .locals 6

    .line 95
    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.app.ActivityTaskManager"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 96
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "getService"

    new-array v3, v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 97
    .local v2, "obj":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "isInSplitScreenWindowingMode"

    new-array v5, v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 98
    .local v3, "method":Ljava/lang/reflect/Method;
    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 99
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "obj":Ljava/lang/Object;
    .end local v3    # "method":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 100
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStackInfo exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UIService-MultiScreenHandler"

    invoke-static {v3, v2}, Lcom/android/server/ui/utils/LogUtil;->logE(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method


# virtual methods
.method public addMultiScreenStatusChangeCallback(ILcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;)V
    .locals 1
    .param p1, "key"    # I
    .param p2, "callback"    # Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;

    .line 40
    if-nez p2, :cond_0

    .line 41
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 43
    return-void
.end method

.method public onMultiScreenChanged(Z)V
    .locals 4
    .param p1, "isMultiScreen"    # Z

    .line 130
    iget-boolean v0, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z

    if-eq v0, p1, :cond_0

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onMultiScreenChanged, splitScreenMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSplitScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isSmallScreen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsSmallScreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UIService-MultiScreenHandler"

    invoke-static {v1, v0}, Lcom/android/server/ui/utils/LogUtil;->logD(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iput-boolean p1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z

    .line 133
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 135
    .local v1, "key":I
    iget-object v2, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mCallbacks:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;

    iget-boolean v3, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mIsMultiScreen:Z

    invoke-interface {v2, v3}, Lcom/android/server/ui/event_status/MultiScreenHandler$IMultiScreenStatusChangeCallback;->onChange(Z)V

    .line 133
    .end local v1    # "key":I
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public registerTaskStackListener()V
    .locals 3

    .line 123
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/ui/event_status/MultiScreenHandler;->getActivityTaskManager()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ui/event_status/MultiScreenHandler;->mTaskStackListener:Landroid/app/TaskStackListener;

    invoke-interface {v0, v1}, Landroid/app/IActivityTaskManager;->registerTaskStackListener(Landroid/app/ITaskStackListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "UIService-MultiScreenHandler"

    const-string v2, "Faild to call registerTaskStackListener"

    invoke-static {v1, v2}, Lcom/android/server/ui/utils/LogUtil;->logW(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
