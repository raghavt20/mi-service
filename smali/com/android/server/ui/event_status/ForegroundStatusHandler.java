public class com.android.server.ui.event_status.ForegroundStatusHandler {
	 /* .source "ForegroundStatusHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.android.server.ui.event_status.ForegroundStatusHandler mInstance;
/* # instance fields */
private miui.process.IForegroundInfoListener$Stub mAppObserver;
private final android.util.SparseArray mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
private miui.process.ForegroundInfo mCurrentFgAppInfo;
/* # direct methods */
static void -$$Nest$fputmCurrentFgAppInfo ( com.android.server.ui.event_status.ForegroundStatusHandler p0, miui.process.ForegroundInfo p1 ) { //bridge//synthethic
/* .locals 0 */
this.mCurrentFgAppInfo = p1;
return;
} // .end method
static com.android.server.ui.event_status.ForegroundStatusHandler ( ) {
/* .locals 1 */
/* .line 14 */
int v0 = 0; // const/4 v0, 0x0
return;
} // .end method
private com.android.server.ui.event_status.ForegroundStatusHandler ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 27 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 16 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mCallbacks = v0;
/* .line 17 */
int v0 = 0; // const/4 v0, 0x0
this.mCurrentFgAppInfo = v0;
/* .line 33 */
/* new-instance v0, Lcom/android/server/ui/event_status/ForegroundStatusHandler$1; */
/* invoke-direct {v0, p0}, Lcom/android/server/ui/event_status/ForegroundStatusHandler$1;-><init>(Lcom/android/server/ui/event_status/ForegroundStatusHandler;)V */
this.mAppObserver = v0;
/* .line 28 */
this.mContext = p1;
/* .line 29 */
miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
/* .line 30 */
miui.process.ProcessManager .getForegroundInfo ( );
this.mCurrentFgAppInfo = v0;
/* .line 31 */
return;
} // .end method
public static synchronized com.android.server.ui.event_status.ForegroundStatusHandler getInstance ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* const-class v0, Lcom/android/server/ui/event_status/ForegroundStatusHandler; */
/* monitor-enter v0 */
/* .line 20 */
try { // :try_start_0
final String v1 = "UIService-ForegroundStatusHandler"; // const-string v1, "UIService-ForegroundStatusHandler"
final String v2 = "getInstance"; // const-string v2, "getInstance"
com.android.server.ui.utils.LogUtil .logD ( v1,v2 );
/* .line 21 */
v1 = com.android.server.ui.event_status.ForegroundStatusHandler.mInstance;
/* if-nez v1, :cond_0 */
if ( p0 != null) { // if-eqz p0, :cond_0
	 /* .line 22 */
	 /* new-instance v1, Lcom/android/server/ui/event_status/ForegroundStatusHandler; */
	 /* invoke-direct {v1, p0}, Lcom/android/server/ui/event_status/ForegroundStatusHandler;-><init>(Landroid/content/Context;)V */
	 /* .line 24 */
} // :cond_0
v1 = com.android.server.ui.event_status.ForegroundStatusHandler.mInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 19 */
} // .end local p0 # "context":Landroid/content/Context;
/* :catchall_0 */
/* move-exception p0 */
/* monitor-exit v0 */
/* throw p0 */
} // .end method
/* # virtual methods */
public void addFgStatusChangeCallback ( Integer p0, com.android.server.ui.event_status.ForegroundStatusHandler$IForegroundInfoChangeCallback p1 ) {
/* .locals 1 */
/* .param p1, "key" # I */
/* .param p2, "callback" # Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback; */
/* .line 47 */
/* if-nez p2, :cond_0 */
/* .line 48 */
return;
/* .line 49 */
} // :cond_0
v0 = this.mCallbacks;
(( android.util.SparseArray ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 50 */
return;
} // .end method
public void updataAppInfo ( ) {
/* .locals 4 */
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mCallbacks;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-ge v0, v1, :cond_0 */
/* .line 54 */
v1 = this.mCallbacks;
v1 = (( android.util.SparseArray ) v1 ).keyAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 55 */
/* .local v1, "key":I */
v2 = this.mCallbacks;
(( android.util.SparseArray ) v2 ).get ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/android/server/ui/event_status/ForegroundStatusHandler$IForegroundInfoChangeCallback; */
v3 = this.mCurrentFgAppInfo;
/* .line 53 */
} // .end local v1 # "key":I
/* add-int/lit8 v0, v0, 0x1 */
/* .line 57 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
