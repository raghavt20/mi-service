public class com.android.server.ForceDarkAppListManager {
	 /* .source "ForceDarkAppListManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;, */
	 /* Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver; */
	 /* } */
} // .end annotation
/* # instance fields */
private java.lang.ref.SoftReference mAppListCacheRef;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/SoftReference<", */
/* "Ljava/util/ArrayList<", */
/* "Landroid/content/pm/PackageInfo;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private com.miui.darkmode.DarkModeAppData mDarkModeAppData;
private com.android.server.ForceDarkAppListProvider mForceDarkAppListProvider;
private java.util.HashMap mForceDarkAppSettings;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/android/server/DarkModeAppSettingsInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.ForceDarkUiModeModeManager mForceDarkUiModeModeManager;
private android.content.pm.PackageManager mPackageManager;
private miui.security.SecurityManagerInternal mSecurityManagerService;
/* # direct methods */
static com.android.server.ForceDarkAppListProvider -$$Nest$fgetmForceDarkAppListProvider ( com.android.server.ForceDarkAppListManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForceDarkAppListProvider;
} // .end method
static com.android.server.ForceDarkUiModeModeManager -$$Nest$fgetmForceDarkUiModeModeManager ( com.android.server.ForceDarkAppListManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mForceDarkUiModeModeManager;
} // .end method
static void -$$Nest$fputmForceDarkAppSettings ( com.android.server.ForceDarkAppListManager p0, java.util.HashMap p1 ) { //bridge//synthethic
/* .locals 0 */
this.mForceDarkAppSettings = p1;
return;
} // .end method
static void -$$Nest$mclearCache ( com.android.server.ForceDarkAppListManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/android/server/ForceDarkAppListManager;->clearCache()V */
return;
} // .end method
public com.android.server.ForceDarkAppListManager ( ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "forceDarkUiModeModeManager" # Lcom/android/server/ForceDarkUiModeModeManager; */
/* .line 54 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 47 */
/* new-instance v0, Ljava/lang/ref/SoftReference; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V */
this.mAppListCacheRef = v0;
/* .line 51 */
/* new-instance v0, Lcom/miui/darkmode/DarkModeAppData; */
/* invoke-direct {v0}, Lcom/miui/darkmode/DarkModeAppData;-><init>()V */
this.mDarkModeAppData = v0;
/* .line 55 */
com.android.server.ForceDarkAppListProvider .getInstance ( );
this.mForceDarkAppListProvider = v0;
/* .line 56 */
(( com.android.server.ForceDarkAppListProvider ) v0 ).getForceDarkAppSettings ( ); // invoke-virtual {v0}, Lcom/android/server/ForceDarkAppListProvider;->getForceDarkAppSettings()Ljava/util/HashMap;
this.mForceDarkAppSettings = v0;
/* .line 57 */
v0 = this.mForceDarkAppListProvider;
/* new-instance v1, Lcom/android/server/ForceDarkAppListManager$1; */
/* invoke-direct {v1, p0}, Lcom/android/server/ForceDarkAppListManager$1;-><init>(Lcom/android/server/ForceDarkAppListManager;)V */
(( com.android.server.ForceDarkAppListProvider ) v0 ).setAppListChangeListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/ForceDarkAppListProvider;->setAppListChangeListener(Lcom/android/server/ForceDarkAppListProvider$ForceDarkAppListChangeListener;)V
/* .line 65 */
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPackageManager = v0;
/* .line 66 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 67 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_REMOVED"; // const-string v1, "android.intent.action.PACKAGE_REMOVED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 68 */
final String v1 = "android.intent.action.PACKAGE_REPLACED"; // const-string v1, "android.intent.action.PACKAGE_REPLACED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 69 */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 70 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 71 */
/* new-instance v1, Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver;-><init>(Lcom/android/server/ForceDarkAppListManager;Lcom/android/server/ForceDarkAppListManager$AppChangedReceiver-IA;)V */
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) p1 ).registerReceiver ( v1, v0, v3 ); // invoke-virtual {p1, v1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 73 */
/* new-instance v1, Landroid/content/IntentFilter; */
final String v4 = "android.intent.action.LOCALE_CHANGED"; // const-string v4, "android.intent.action.LOCALE_CHANGED"
/* invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 74 */
/* new-instance v1, Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver; */
/* invoke-direct {v1, p0, v2}, Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver;-><init>(Lcom/android/server/ForceDarkAppListManager;Lcom/android/server/ForceDarkAppListManager$LocaleChangedReceiver-IA;)V */
(( android.content.Context ) p1 ).registerReceiver ( v1, v0, v3 ); // invoke-virtual {p1, v1, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 75 */
this.mContext = p1;
/* .line 76 */
this.mForceDarkUiModeModeManager = p2;
/* .line 77 */
return;
} // .end method
private void clearCache ( ) {
/* .locals 3 */
/* .line 199 */
v0 = this.mDarkModeAppData;
/* const-wide/16 v1, 0x0 */
(( com.miui.darkmode.DarkModeAppData ) v0 ).setCreateTime ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/darkmode/DarkModeAppData;->setCreateTime(J)V
/* .line 200 */
v0 = this.mAppListCacheRef;
(( java.lang.ref.SoftReference ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->clear()V
/* .line 201 */
return;
} // .end method
private com.miui.darkmode.DarkModeAppDetailInfo getAppInfo ( com.android.server.DarkModeAppSettingsInfo p0, android.content.pm.ApplicationInfo p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "darkModeAppSettingsInfo" # Lcom/android/server/DarkModeAppSettingsInfo; */
/* .param p2, "applicationInfo" # Landroid/content/pm/ApplicationInfo; */
/* .param p3, "userId" # I */
/* .line 189 */
v0 = this.mPackageManager;
(( android.content.pm.ApplicationInfo ) p2 ).loadLabel ( v0 ); // invoke-virtual {p2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
/* .line 190 */
/* .local v0, "label":Ljava/lang/String; */
/* new-instance v1, Lcom/miui/darkmode/DarkModeAppDetailInfo; */
/* invoke-direct {v1}, Lcom/miui/darkmode/DarkModeAppDetailInfo;-><init>()V */
/* .line 191 */
/* .local v1, "appInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo; */
(( com.miui.darkmode.DarkModeAppDetailInfo ) v1 ).setLabel ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setLabel(Ljava/lang/String;)V
/* .line 192 */
v2 = this.packageName;
(( com.miui.darkmode.DarkModeAppDetailInfo ) v1 ).setPkgName ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setPkgName(Ljava/lang/String;)V
/* .line 193 */
v2 = this.mSecurityManagerService;
v3 = this.packageName;
v2 = (( miui.security.SecurityManagerInternal ) v2 ).getAppDarkModeForUser ( v3, p3 ); // invoke-virtual {v2, v3, p3}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z
(( com.miui.darkmode.DarkModeAppDetailInfo ) v1 ).setEnabled ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setEnabled(Z)V
/* .line 194 */
v2 = (( com.android.server.DarkModeAppSettingsInfo ) p1 ).getAdaptStat ( ); // invoke-virtual {p1}, Lcom/android/server/DarkModeAppSettingsInfo;->getAdaptStat()I
(( com.miui.darkmode.DarkModeAppDetailInfo ) v1 ).setAdaptStatus ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setAdaptStatus(I)V
/* .line 195 */
} // .end method
private java.util.List getInstalledPkgList ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/content/pm/PackageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 210 */
v0 = this.mAppListCacheRef;
(( java.lang.ref.SoftReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;
/* check-cast v0, Ljava/util/ArrayList; */
/* .line 211 */
/* .local v0, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/PackageInfo;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.util.ArrayList ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
/* if-nez v1, :cond_0 */
/* .line 212 */
/* .line 214 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v0, v1 */
/* .line 215 */
v1 = this.mPackageManager;
/* const/16 v2, 0x40 */
(( android.content.pm.PackageManager ) v1 ).getInstalledPackages ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;
(( java.util.ArrayList ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 216 */
/* new-instance v1, Ljava/lang/ref/SoftReference; */
/* invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V */
this.mAppListCacheRef = v1;
/* .line 217 */
} // .end method
private Boolean shouldShowInSettings ( android.content.pm.ApplicationInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .line 268 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = (( android.content.pm.ApplicationInfo ) p1 ).isSystemApp ( ); // invoke-virtual {p1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z
/* if-nez v0, :cond_0 */
/* iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
/* const/16 v1, 0x2710 */
/* if-le v0, v1, :cond_0 */
v0 = this.packageName;
/* .line 269 */
final String v1 = "com.miui."; // const-string v1, "com.miui."
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
v0 = this.packageName;
final String v1 = "com.xiaomi."; // const-string v1, "com.xiaomi."
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
v0 = this.packageName;
/* .line 270 */
final String v1 = "com.mi."; // const-string v1, "com.mi."
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
v0 = this.packageName;
final String v1 = "com.google."; // const-string v1, "com.google."
v0 = (( java.lang.String ) v0 ).startsWith ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 268 */
} // :goto_0
} // .end method
/* # virtual methods */
public java.util.HashMap getAllForceDarkMapForSplashScreen ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 226 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 228 */
/* .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;" */
v1 = this.mForceDarkAppSettings;
(( java.util.HashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 229 */
/* .local v2, "settingsInfo":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;" */
/* check-cast v3, Lcom/android/server/DarkModeAppSettingsInfo; */
v3 = (( com.android.server.DarkModeAppSettingsInfo ) v3 ).getForceDarkSplashScreen ( ); // invoke-virtual {v3}, Lcom/android/server/DarkModeAppSettingsInfo;->getForceDarkSplashScreen()I
/* .line 230 */
/* .local v3, "forceDarkSplashScreenValue":I */
int v4 = 1; // const/4 v4, 0x1
/* if-eq v3, v4, :cond_0 */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 232 */
/* check-cast v4, Ljava/lang/String; */
java.lang.Integer .valueOf ( v3 );
(( java.util.HashMap ) v0 ).put ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 234 */
} // .end local v2 # "settingsInfo":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/android/server/DarkModeAppSettingsInfo;>;"
} // .end local v3 # "forceDarkSplashScreenValue":I
} // :cond_0
/* .line 235 */
} // :cond_1
} // .end method
public Boolean getAppDarkModeEnable ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 106 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
/* .line 109 */
} // :cond_0
v0 = this.mForceDarkAppSettings;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo; */
/* .line 110 */
/* .local v0, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 111 */
v2 = (( com.android.server.DarkModeAppSettingsInfo ) v0 ).isShowInSettings ( ); // invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isShowInSettings()Z
/* if-nez v2, :cond_2 */
/* .line 112 */
v2 = (( com.android.server.DarkModeAppSettingsInfo ) v0 ).getOverrideEnableValue ( ); // invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->getOverrideEnableValue()I
/* .line 113 */
/* .local v2, "overrideEnableValue":I */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_1 */
/* .line 114 */
/* .line 115 */
} // :cond_1
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 116 */
/* .line 120 */
} // .end local v2 # "overrideEnableValue":I
} // :cond_2
v1 = this.mSecurityManagerService;
v1 = (( miui.security.SecurityManagerInternal ) v1 ).getAppDarkModeForUser ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z
} // .end method
public Boolean getAppForceDarkOrigin ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 124 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 125 */
/* .line 128 */
} // :cond_0
v0 = this.mForceDarkAppSettings;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo; */
/* .line 129 */
/* .local v0, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 130 */
v1 = (( com.android.server.DarkModeAppSettingsInfo ) v0 ).isForceDarkOrigin ( ); // invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isForceDarkOrigin()Z
/* .line 133 */
} // :cond_1
v2 = this.mDarkModeAppData;
(( com.miui.darkmode.DarkModeAppData ) v2 ).getInfoWithPackageName ( p1 ); // invoke-virtual {v2, p1}, Lcom/miui/darkmode/DarkModeAppData;->getInfoWithPackageName(Ljava/lang/String;)Lcom/miui/darkmode/DarkModeAppDetailInfo;
/* if-nez v2, :cond_2 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public com.miui.darkmode.DarkModeAppData getDarkModeAppList ( Long p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "appLastUpdateTime" # J */
/* .param p3, "userId" # I */
/* .line 160 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p1, v0 */
/* if-lez v0, :cond_0 */
/* .line 161 */
v0 = this.mDarkModeAppData;
(( com.miui.darkmode.DarkModeAppData ) v0 ).getCreateTime ( ); // invoke-virtual {v0}, Lcom/miui/darkmode/DarkModeAppData;->getCreateTime()J
/* move-result-wide v0 */
/* cmp-long v0, p1, v0 */
/* if-nez v0, :cond_0 */
/* .line 162 */
int v0 = 0; // const/4 v0, 0x0
/* .line 165 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 167 */
/* .local v0, "darkModeAppDetailInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;" */
/* invoke-direct {p0}, Lcom/android/server/ForceDarkAppListManager;->getInstalledPkgList()Ljava/util/List; */
/* .line 168 */
/* .local v1, "installedPkgList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Landroid/content/pm/PackageInfo; */
/* .line 170 */
/* .local v3, "packageInfo":Landroid/content/pm/PackageInfo; */
v4 = this.mForceDarkAppSettings;
v5 = this.applicationInfo;
v5 = this.packageName;
(( java.util.HashMap ) v4 ).get ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lcom/android/server/DarkModeAppSettingsInfo; */
/* .line 171 */
/* .local v4, "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 172 */
v5 = (( com.android.server.DarkModeAppSettingsInfo ) v4 ).isShowInSettings ( ); // invoke-virtual {v4}, Lcom/android/server/DarkModeAppSettingsInfo;->isShowInSettings()Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 173 */
v5 = this.applicationInfo;
/* .line 174 */
/* invoke-direct {p0, v4, v5, p3}, Lcom/android/server/ForceDarkAppListManager;->getAppInfo(Lcom/android/server/DarkModeAppSettingsInfo;Landroid/content/pm/ApplicationInfo;I)Lcom/miui/darkmode/DarkModeAppDetailInfo; */
/* .line 175 */
/* .local v5, "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo; */
/* .line 176 */
} // .end local v5 # "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo;
/* .line 177 */
} // :cond_1
/* sget-boolean v5, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
v5 = this.applicationInfo;
v5 = /* invoke-direct {p0, v5}, Lcom/android/server/ForceDarkAppListManager;->shouldShowInSettings(Landroid/content/pm/ApplicationInfo;)Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 178 */
/* new-instance v5, Lcom/android/server/DarkModeAppSettingsInfo; */
/* invoke-direct {v5}, Lcom/android/server/DarkModeAppSettingsInfo;-><init>()V */
v6 = this.applicationInfo;
/* .line 179 */
/* invoke-direct {p0, v5, v6, p3}, Lcom/android/server/ForceDarkAppListManager;->getAppInfo(Lcom/android/server/DarkModeAppSettingsInfo;Landroid/content/pm/ApplicationInfo;I)Lcom/miui/darkmode/DarkModeAppDetailInfo; */
/* .line 180 */
/* .restart local v5 # "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo; */
/* .line 182 */
} // .end local v3 # "packageInfo":Landroid/content/pm/PackageInfo;
} // .end local v4 # "darkModeAppSettingsInfo":Lcom/android/server/DarkModeAppSettingsInfo;
} // .end local v5 # "darkModeAppDetailInfo":Lcom/miui/darkmode/DarkModeAppDetailInfo;
} // :cond_2
} // :goto_1
/* .line 183 */
} // :cond_3
v2 = this.mDarkModeAppData;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( com.miui.darkmode.DarkModeAppData ) v2 ).setCreateTime ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/miui/darkmode/DarkModeAppData;->setCreateTime(J)V
/* .line 184 */
v2 = this.mDarkModeAppData;
(( com.miui.darkmode.DarkModeAppData ) v2 ).setDarkModeAppDetailInfoList ( v0 ); // invoke-virtual {v2, v0}, Lcom/miui/darkmode/DarkModeAppData;->setDarkModeAppDetailInfoList(Ljava/util/List;)V
/* .line 185 */
v2 = this.mDarkModeAppData;
} // .end method
public Integer getInterceptRelaunchType ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .line 137 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 2; // const/4 v1, 0x2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 138 */
/* .line 141 */
} // :cond_0
v0 = this.mForceDarkAppSettings;
(( java.util.HashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lcom/android/server/DarkModeAppSettingsInfo; */
/* .line 142 */
/* .local v0, "settingsInfo":Lcom/android/server/DarkModeAppSettingsInfo; */
/* if-nez v0, :cond_1 */
/* .line 143 */
/* .line 146 */
} // :cond_1
v2 = (( com.android.server.DarkModeAppSettingsInfo ) v0 ).getInterceptRelaunch ( ); // invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->getInterceptRelaunch()I
/* .line 147 */
/* .local v2, "interceptRelaunchType":I */
v3 = (( com.android.server.DarkModeAppSettingsInfo ) v0 ).getInterceptRelaunch ( ); // invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->getInterceptRelaunch()I
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 148 */
/* .line 152 */
} // :cond_2
v3 = (( com.android.server.DarkModeAppSettingsInfo ) v0 ).isShowInSettings ( ); // invoke-virtual {v0}, Lcom/android/server/DarkModeAppSettingsInfo;->isShowInSettings()Z
if ( v3 != null) { // if-eqz v3, :cond_3
v3 = this.mSecurityManagerService;
v3 = (( miui.security.SecurityManagerInternal ) v3 ).getAppDarkModeForUser ( p1, p2 ); // invoke-virtual {v3, p1, p2}, Lmiui/security/SecurityManagerInternal;->getAppDarkModeForUser(Ljava/lang/String;I)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 153 */
int v1 = 1; // const/4 v1, 0x1
/* .line 156 */
} // :cond_3
} // .end method
public void onBootPhase ( Integer p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "phase" # I */
/* .param p2, "context" # Landroid/content/Context; */
/* .line 81 */
/* const-class v0, Lmiui/security/SecurityManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lmiui/security/SecurityManagerInternal; */
this.mSecurityManagerService = v0;
/* .line 82 */
v0 = this.mForceDarkAppListProvider;
(( com.android.server.ForceDarkAppListProvider ) v0 ).onBootPhase ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/ForceDarkAppListProvider;->onBootPhase(ILandroid/content/Context;)V
/* .line 83 */
return;
} // .end method
public void setAppDarkModeEnable ( java.lang.String p0, Boolean p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "enable" # Z */
/* .param p3, "userId" # I */
/* .line 86 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 87 */
return;
/* .line 90 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ":"; // const-string v2, ":"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "last_app_dark_mode_pkg"; // const-string v2, "last_app_dark_mode_pkg"
android.provider.Settings$System .putString ( v0,v2,v1 );
/* .line 92 */
v0 = this.mSecurityManagerService;
(( miui.security.SecurityManagerInternal ) v0 ).setAppDarkModeForUser ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lmiui/security/SecurityManagerInternal;->setAppDarkModeForUser(Ljava/lang/String;ZI)V
/* .line 93 */
v0 = this.mDarkModeAppData;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 94 */
(( com.miui.darkmode.DarkModeAppData ) v0 ).getDarkModeAppDetailInfoList ( ); // invoke-virtual {v0}, Lcom/miui/darkmode/DarkModeAppData;->getDarkModeAppDetailInfoList()Ljava/util/List;
/* .line 95 */
/* .local v0, "darkModeAppDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_2 */
/* .line 96 */
/* check-cast v2, Lcom/miui/darkmode/DarkModeAppDetailInfo; */
(( com.miui.darkmode.DarkModeAppDetailInfo ) v2 ).getPkgName ( ); // invoke-virtual {v2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->getPkgName()Ljava/lang/String;
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 97 */
/* check-cast v2, Lcom/miui/darkmode/DarkModeAppDetailInfo; */
(( com.miui.darkmode.DarkModeAppDetailInfo ) v2 ).setEnabled ( p2 ); // invoke-virtual {v2, p2}, Lcom/miui/darkmode/DarkModeAppDetailInfo;->setEnabled(Z)V
/* .line 98 */
v2 = this.mDarkModeAppData;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( com.miui.darkmode.DarkModeAppData ) v2 ).setCreateTime ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/miui/darkmode/DarkModeAppData;->setCreateTime(J)V
/* .line 99 */
/* .line 95 */
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 103 */
} // .end local v0 # "darkModeAppDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/darkmode/DarkModeAppDetailInfo;>;"
} // .end local v1 # "i":I
} // :cond_2
} // :goto_1
return;
} // .end method
