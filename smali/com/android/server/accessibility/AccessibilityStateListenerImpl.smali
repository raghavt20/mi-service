.class public Lcom/android/server/accessibility/AccessibilityStateListenerImpl;
.super Lcom/android/server/accessibility/AccessibilityStateListenerStub;
.source "AccessibilityStateListenerImpl.java"


# annotations
.annotation runtime Lcom/miui/base/annotations/MiuiStubHead;
    manifestName = "com.android.server.accessibility.AccessibilityStateListenerStub$$"
.end annotation


# static fields
.field private static final DIANMING_SERVICE:Ljava/lang/String; = "com.dianming.phoneapp/com.dianming.phoneapp.MyAccessibilityService"

.field private static final DIANMING_SERVICE_OMIT:Ljava/lang/String; = "com.dianming.phoneapp/.MyAccessibilityService"

.field private static final MIUI_ENHANCE_TALKBACK:Ljava/lang/String; = "com.miui.accessibility/com.miui.accessibility.enhance.tb.MiuiEnhanceTBService"

.field private static final NIRENR_SERVICE:Ljava/lang/String; = "com.nirenr.talkman/com.nirenr.talkman.TalkManAccessibilityService"

.field private static final NIRENR_SERVICE_OMIT:Ljava/lang/String; = "com.nirenr.talkman/.TalkManAccessibilityService"

.field private static final TAG:Ljava/lang/String; = "AccessibilityStateListenerImpl"

.field private static final TALKBACK_SERVICE:Ljava/lang/String; = "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

.field private static final TALKBACK_SERVICE_OMIT:Ljava/lang/String; = "com.google.android.marvin.talkback/.TalkBackService"

.field private static final TBACK_SERVICE:Ljava/lang/String; = "com.android.tback/net.tatans.soundback.SoundBackService"

.field private static final VOICEBACK_SERVICE:Ljava/lang/String; = "com.bjbyhd.voiceback/com.bjbyhd.voiceback.BoyhoodVoiceBackService"

.field private static final VOICEBACK_SERVICE_OMIT:Ljava/lang/String; = "com.bjbyhd.voiceback/.BoyhoodVoiceBackService"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityStateListenerStub;-><init>()V

    return-void
.end method

.method private static isMiuiEnhanceTBServiceInstalled(Landroid/view/accessibility/AccessibilityManager;)Z
    .locals 3
    .param p0, "accessibilityManager"    # Landroid/view/accessibility/AccessibilityManager;

    .line 85
    const-string v0, "com.miui.accessibility/com.miui.accessibility.enhance.tb.MiuiEnhanceTBService"

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 86
    return v2

    .line 89
    :cond_0
    nop

    .line 90
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 89
    invoke-virtual {p0, v0}, Landroid/view/accessibility/AccessibilityManager;->getInstalledServiceInfoWithComponentName(Landroid/content/ComponentName;)Landroid/accessibilityservice/AccessibilityServiceInfo;

    move-result-object v0

    .line 92
    .local v0, "serviceInfo":Landroid/accessibilityservice/AccessibilityServiceInfo;
    if-nez v0, :cond_1

    .line 93
    return v2

    .line 96
    :cond_1
    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isNecessaryEnable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "enabledServices"    # Ljava/lang/String;

    .line 74
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 76
    :cond_0
    const-string v0, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.google.android.marvin.talkback/.TalkBackService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 77
    const-string v0, "com.bjbyhd.voiceback/com.bjbyhd.voiceback.BoyhoodVoiceBackService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.bjbyhd.voiceback/.BoyhoodVoiceBackService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    const-string v0, "com.dianming.phoneapp/com.dianming.phoneapp.MyAccessibilityService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.dianming.phoneapp/.MyAccessibilityService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    const-string v0, "com.nirenr.talkman/com.nirenr.talkman.TalkManAccessibilityService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.nirenr.talkman/.TalkManAccessibilityService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    const-string v0, "com.android.tback/net.tatans.soundback.SoundBackService"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    .line 76
    :cond_2
    return v1
.end method


# virtual methods
.method public sendStateToClients(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stateFlags"    # I

    .line 46
    const-string v0, "com.miui.accessibility/com.miui.accessibility.enhance.tb.MiuiEnhanceTBService"

    const-string v1, "AccessibilityStateListenerImpl"

    if-nez p1, :cond_0

    return-void

    .line 47
    :cond_0
    :try_start_0
    const-string v2, "accessibility"

    .line 48
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    .line 49
    .local v2, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    if-nez v2, :cond_1

    return-void

    .line 51
    :cond_1
    invoke-static {v2}, Lcom/android/server/accessibility/AccessibilityStateListenerImpl;->isMiuiEnhanceTBServiceInstalled(Landroid/view/accessibility/AccessibilityManager;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 52
    const-string v0, "MiuiEnhanceTBService is not installed"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    return-void

    .line 56
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "enabled_accessibility_services"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, "enabledServices":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/android/server/accessibility/AccessibilityStateListenerImpl;->isNecessaryEnable(Ljava/lang/String;)Z

    move-result v4

    .line 61
    .local v4, "enabled":Z
    invoke-static {p1, v0}, Lcom/android/internal/accessibility/util/AccessibilityUtils;->isAccessibilityServiceEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eq v4, v5, :cond_3

    .line 62
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onAccessibilityStateChanged: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    nop

    .line 64
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 63
    invoke-static {p1, v0, v4}, Lcom/android/internal/accessibility/util/AccessibilityUtils;->setAccessibilityServiceState(Landroid/content/Context;Landroid/content/ComponentName;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .end local v2    # "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    .end local v3    # "enabledServices":Ljava/lang/String;
    .end local v4    # "enabled":Z
    :cond_3
    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown error found - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
