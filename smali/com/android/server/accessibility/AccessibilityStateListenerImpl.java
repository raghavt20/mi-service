public class com.android.server.accessibility.AccessibilityStateListenerImpl extends com.android.server.accessibility.AccessibilityStateListenerStub {
	 /* .source "AccessibilityStateListenerImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.accessibility.AccessibilityStateListenerStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String DIANMING_SERVICE;
private static final java.lang.String DIANMING_SERVICE_OMIT;
private static final java.lang.String MIUI_ENHANCE_TALKBACK;
private static final java.lang.String NIRENR_SERVICE;
private static final java.lang.String NIRENR_SERVICE_OMIT;
private static final java.lang.String TAG;
private static final java.lang.String TALKBACK_SERVICE;
private static final java.lang.String TALKBACK_SERVICE_OMIT;
private static final java.lang.String TBACK_SERVICE;
private static final java.lang.String VOICEBACK_SERVICE;
private static final java.lang.String VOICEBACK_SERVICE_OMIT;
/* # direct methods */
public com.android.server.accessibility.AccessibilityStateListenerImpl ( ) {
	 /* .locals 0 */
	 /* .line 23 */
	 /* invoke-direct {p0}, Lcom/android/server/accessibility/AccessibilityStateListenerStub;-><init>()V */
	 return;
} // .end method
private static Boolean isMiuiEnhanceTBServiceInstalled ( android.view.accessibility.AccessibilityManager p0 ) {
	 /* .locals 3 */
	 /* .param p0, "accessibilityManager" # Landroid/view/accessibility/AccessibilityManager; */
	 /* .line 85 */
	 final String v0 = "com.miui.accessibility/com.miui.accessibility.enhance.tb.MiuiEnhanceTBService"; // const-string v0, "com.miui.accessibility/com.miui.accessibility.enhance.tb.MiuiEnhanceTBService"
	 v1 = 	 android.text.TextUtils .isEmpty ( v0 );
	 int v2 = 0; // const/4 v2, 0x0
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 86 */
		 /* .line 89 */
	 } // :cond_0
	 /* nop */
	 /* .line 90 */
	 android.content.ComponentName .unflattenFromString ( v0 );
	 /* .line 89 */
	 (( android.view.accessibility.AccessibilityManager ) p0 ).getInstalledServiceInfoWithComponentName ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/accessibility/AccessibilityManager;->getInstalledServiceInfoWithComponentName(Landroid/content/ComponentName;)Landroid/accessibilityservice/AccessibilityServiceInfo;
	 /* .line 92 */
	 /* .local v0, "serviceInfo":Landroid/accessibilityservice/AccessibilityServiceInfo; */
	 /* if-nez v0, :cond_1 */
	 /* .line 93 */
	 /* .line 96 */
} // :cond_1
v1 = (( android.view.accessibility.AccessibilityManager ) p0 ).getInstalledAccessibilityServiceList ( ); // invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityManager;->getInstalledAccessibilityServiceList()Ljava/util/List;
} // .end method
private Boolean isNecessaryEnable ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "enabledServices" # Ljava/lang/String; */
/* .line 74 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 76 */
} // :cond_0
final String v0 = "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"; // const-string v0, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
final String v0 = "com.google.android.marvin.talkback/.TalkBackService"; // const-string v0, "com.google.android.marvin.talkback/.TalkBackService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 77 */
final String v0 = "com.bjbyhd.voiceback/com.bjbyhd.voiceback.BoyhoodVoiceBackService"; // const-string v0, "com.bjbyhd.voiceback/com.bjbyhd.voiceback.BoyhoodVoiceBackService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
final String v0 = "com.bjbyhd.voiceback/.BoyhoodVoiceBackService"; // const-string v0, "com.bjbyhd.voiceback/.BoyhoodVoiceBackService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 78 */
final String v0 = "com.dianming.phoneapp/com.dianming.phoneapp.MyAccessibilityService"; // const-string v0, "com.dianming.phoneapp/com.dianming.phoneapp.MyAccessibilityService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
final String v0 = "com.dianming.phoneapp/.MyAccessibilityService"; // const-string v0, "com.dianming.phoneapp/.MyAccessibilityService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 79 */
final String v0 = "com.nirenr.talkman/com.nirenr.talkman.TalkManAccessibilityService"; // const-string v0, "com.nirenr.talkman/com.nirenr.talkman.TalkManAccessibilityService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
final String v0 = "com.nirenr.talkman/.TalkManAccessibilityService"; // const-string v0, "com.nirenr.talkman/.TalkManAccessibilityService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_1 */
/* .line 80 */
final String v0 = "com.android.tback/net.tatans.soundback.SoundBackService"; // const-string v0, "com.android.tback/net.tatans.soundback.SoundBackService"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
/* .line 76 */
} // :cond_2
} // .end method
/* # virtual methods */
public void sendStateToClients ( android.content.Context p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "stateFlags" # I */
/* .line 46 */
final String v0 = "com.miui.accessibility/com.miui.accessibility.enhance.tb.MiuiEnhanceTBService"; // const-string v0, "com.miui.accessibility/com.miui.accessibility.enhance.tb.MiuiEnhanceTBService"
final String v1 = "AccessibilityStateListenerImpl"; // const-string v1, "AccessibilityStateListenerImpl"
/* if-nez p1, :cond_0 */
return;
/* .line 47 */
} // :cond_0
try { // :try_start_0
final String v2 = "accessibility"; // const-string v2, "accessibility"
/* .line 48 */
(( android.content.Context ) p1 ).getSystemService ( v2 ); // invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/view/accessibility/AccessibilityManager; */
/* .line 49 */
/* .local v2, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager; */
/* if-nez v2, :cond_1 */
return;
/* .line 51 */
} // :cond_1
v3 = com.android.server.accessibility.AccessibilityStateListenerImpl .isMiuiEnhanceTBServiceInstalled ( v2 );
/* if-nez v3, :cond_2 */
/* .line 52 */
final String v0 = "MiuiEnhanceTBService is not installed"; // const-string v0, "MiuiEnhanceTBService is not installed"
android.util.Log .e ( v1,v0 );
/* .line 53 */
return;
/* .line 56 */
} // :cond_2
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "enabled_accessibility_services"; // const-string v4, "enabled_accessibility_services"
android.provider.Settings$Secure .getString ( v3,v4 );
/* .line 59 */
/* .local v3, "enabledServices":Ljava/lang/String; */
v4 = /* invoke-direct {p0, v3}, Lcom/android/server/accessibility/AccessibilityStateListenerImpl;->isNecessaryEnable(Ljava/lang/String;)Z */
/* .line 61 */
/* .local v4, "enabled":Z */
v5 = com.android.internal.accessibility.util.AccessibilityUtils .isAccessibilityServiceEnabled ( p1,v0 );
/* if-eq v4, v5, :cond_3 */
/* .line 62 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "onAccessibilityStateChanged: "; // const-string v6, "onAccessibilityStateChanged: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v5 );
/* .line 63 */
/* nop */
/* .line 64 */
android.content.ComponentName .unflattenFromString ( v0 );
/* .line 63 */
com.android.internal.accessibility.util.AccessibilityUtils .setAccessibilityServiceState ( p1,v0,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 68 */
} // .end local v2 # "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
} // .end local v3 # "enabledServices":Ljava/lang/String;
} // .end local v4 # "enabled":Z
} // :cond_3
/* .line 66 */
/* :catch_0 */
/* move-exception v0 */
/* .line 67 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Unknown error found - "; // const-string v3, "Unknown error found - "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2,v0 );
/* .line 71 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
