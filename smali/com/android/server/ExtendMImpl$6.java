class com.android.server.ExtendMImpl$6 extends android.os.IVoldTaskListener$Stub {
	 /* .source "ExtendMImpl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/android/server/ExtendMImpl;->runFlush(I)Z */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.ExtendMImpl this$0; //synthetic
/* # direct methods */
 com.android.server.ExtendMImpl$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/ExtendMImpl; */
/* .line 1202 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/IVoldTaskListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onFinished ( Integer p0, android.os.PersistableBundle p1 ) {
/* .locals 8 */
/* .param p1, "status" # I */
/* .param p2, "extras" # Landroid/os/PersistableBundle; */
/* .line 1210 */
final String v0 = ""; // const-string v0, ""
final String v1 = "MFZ"; // const-string v1, "MFZ"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Flush finished with status: "; // const-string v3, "Flush finished with status: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "ExtM"; // const-string v3, "ExtM"
android.util.Slog .w ( v3,v2 );
/* .line 1212 */
/* const/16 v2, 0x80 */
try { // :try_start_0
	 /* new-instance v4, Ljava/io/File; */
	 final String v5 = "/sys/block/zram0/idle_stat"; // const-string v5, "/sys/block/zram0/idle_stat"
	 /* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
	 /* .line 1213 */
	 android.os.FileUtils .readTextFile ( v4,v2,v0 );
	 /* .line 1214 */
	 /* .local v4, "idle_stats":Ljava/lang/String; */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "Flush finished! After Flush idle_stats: "; // const-string v6, "Flush finished! After Flush idle_stats: "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v1,v5 );
	 /* :try_end_0 */
	 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 1217 */
	 /* nop */
} // .end local v4 # "idle_stats":Ljava/lang/String;
/* .line 1215 */
/* :catch_0 */
/* move-exception v4 */
/* .line 1216 */
/* .local v4, "e":Ljava/io/IOException; */
final String v5 = "After Flush idle_stats: error"; // const-string v5, "After Flush idle_stats: error"
android.util.Slog .d ( v1,v5 );
/* .line 1219 */
} // .end local v4 # "e":Ljava/io/IOException;
} // :goto_0
try { // :try_start_1
/* new-instance v4, Ljava/io/File; */
final String v5 = "/sys/block/zram0/bd_stat"; // const-string v5, "/sys/block/zram0/bd_stat"
/* invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1220 */
android.os.FileUtils .readTextFile ( v4,v2,v0 );
/* .line 1221 */
/* .local v0, "bd_stats":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Flush finished! After Flush bd_stats: "; // const-string v4, "Flush finished! After Flush bd_stats: "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 1224 */
/* nop */
} // .end local v0 # "bd_stats":Ljava/lang/String;
/* .line 1222 */
/* :catch_1 */
/* move-exception v0 */
/* .line 1223 */
/* .local v0, "e":Ljava/io/IOException; */
final String v2 = "After Flush bd_stats: error"; // const-string v2, "After Flush bd_stats: error"
android.util.Slog .d ( v1,v2 );
/* .line 1225 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_1
/* const-wide/16 v4, 0x0 */
/* .line 1226 */
/* .local v4, "zramFree":J */
v0 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$mreadMemInfo ( v0 );
/* .line 1227 */
v0 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fgetmMemInfo ( v0 );
final String v2 = "SwapFree"; // const-string v2, "SwapFree"
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v4 */
/* .line 1228 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Flush finished! After Flush zramFreeKb: "; // const-string v2, "Flush finished! After Flush zramFreeKb: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "KB"; // const-string v2, "KB"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1229 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.android.server.ExtendMImpl .-$$Nest$fputisFlushFinished ( v0,v1 );
/* .line 1230 */
/* if-nez p1, :cond_1 */
/* .line 1231 */
v0 = this.this$0;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
com.android.server.ExtendMImpl .-$$Nest$fputlastFlushTime ( v0,v1,v2 );
/* .line 1232 */
v0 = this.this$0;
v1 = com.android.server.ExtendMImpl .-$$Nest$fgetprev_bd_write ( v0 );
v2 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$fgetlastFlushTime ( v2 );
/* move-result-wide v6 */
com.android.server.ExtendMImpl .-$$Nest$mshowAndSaveFlushedStat ( v0,v1,v6,v7 );
/* .line 1233 */
v0 = com.android.server.ExtendMImpl .-$$Nest$sfgetLOG_VERBOSE ( );
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "Exit flush"; // const-string v0, "Exit flush"
android.util.Slog .d ( v3,v0 );
/* .line 1234 */
} // :cond_0
v0 = this.this$0;
com.android.server.ExtendMImpl .-$$Nest$mshowTotalStat ( v0 );
/* .line 1235 */
} // :cond_1
int v0 = -1; // const/4 v0, -0x1
/* if-ne p1, v0, :cond_2 */
/* .line 1236 */
final String v0 = "Flush process fork error "; // const-string v0, "Flush process fork error "
android.util.Slog .w ( v3,v0 );
/* .line 1238 */
} // :cond_2
} // :goto_2
return;
} // .end method
public void onStatus ( Integer p0, android.os.PersistableBundle p1 ) {
/* .locals 0 */
/* .param p1, "status" # I */
/* .param p2, "extras" # Landroid/os/PersistableBundle; */
/* .line 1206 */
return;
} // .end method
