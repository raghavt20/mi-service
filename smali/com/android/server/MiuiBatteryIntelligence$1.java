class com.android.server.MiuiBatteryIntelligence$1 extends android.content.BroadcastReceiver {
	 /* .source "MiuiBatteryIntelligence.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiBatteryIntelligence; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.android.server.MiuiBatteryIntelligence this$0; //synthetic
/* # direct methods */
 com.android.server.MiuiBatteryIntelligence$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/android/server/MiuiBatteryIntelligence; */
/* .line 87 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 90 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 91 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* const-wide/16 v4, 0x0 */
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 92 */
	 final String v1 = "plugged"; // const-string v1, "plugged"
	 int v6 = -1; // const/4 v6, -0x1
	 v1 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v6 ); // invoke-virtual {p2, v1, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 93 */
	 /* .local v1, "plugType":I */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* move v2, v3 */
		 /* .line 94 */
		 /* .local v2, "plugged":Z */
	 } // :cond_0
	 v6 = this.this$0;
	 v6 = 	 com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmPlugged ( v6 );
	 /* if-eq v2, v6, :cond_1 */
	 /* .line 95 */
	 v6 = this.this$0;
	 com.android.server.MiuiBatteryIntelligence .-$$Nest$fputmPlugged ( v6,v2 );
	 /* .line 96 */
	 v6 = this.this$0;
	 com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmHandler ( v6 );
	 (( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) v6 ).sendMessageDelayed ( v3, v4, v5 ); // invoke-virtual {v6, v3, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V
	 /* .line 98 */
} // .end local v1 # "plugType":I
} // .end local v2 # "plugged":Z
} // :cond_1
} // :cond_2
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 99 */
v1 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmHandler ( v1 );
int v2 = 2; // const/4 v2, 0x2
(( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) v1 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v1, v2, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V
/* .line 100 */
} // :cond_3
final String v1 = "android.net.wifi.WIFI_STATE_CHANGED"; // const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 101 */
/* const-string/jumbo v1, "wifi_state" */
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 102 */
/* .local v1, "wifiState":I */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmWifiState ( v2 );
/* if-eq v1, v2, :cond_5 */
/* .line 103 */
v2 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fputmWifiState ( v2,v1 );
/* .line 104 */
/* if-ne v1, v3, :cond_5 */
v2 = this.this$0;
v2 = com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmPlugged ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 105 */
v2 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmHandler ( v2 );
int v3 = 4; // const/4 v3, 0x4
(( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) v2 ).sendMessageDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V
/* .line 108 */
} // .end local v1 # "wifiState":I
} // :cond_4
final String v1 = "miui.intent.action.NEED_SCAN_WIFI"; // const-string v1, "miui.intent.action.NEED_SCAN_WIFI"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 109 */
v1 = this.this$0;
com.android.server.MiuiBatteryIntelligence .-$$Nest$fgetmHandler ( v1 );
int v2 = 3; // const/4 v2, 0x3
(( com.android.server.MiuiBatteryIntelligence$BatteryInelligenceHandler ) v1 ).sendMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v1, v2, v4, v5}, Lcom/android/server/MiuiBatteryIntelligence$BatteryInelligenceHandler;->sendMessageDelayed(IJ)V
/* .line 108 */
} // :cond_5
} // :goto_0
/* nop */
/* .line 111 */
} // :goto_1
return;
} // .end method
