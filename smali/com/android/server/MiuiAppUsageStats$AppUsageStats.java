class com.android.server.MiuiAppUsageStats$AppUsageStats implements java.lang.Comparable {
	 /* .source "MiuiAppUsageStats.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/server/MiuiAppUsageStats; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "AppUsageStats" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Comparable<", */
/* "Lcom/android/server/MiuiAppUsageStats$AppUsageStats;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
public Long firstForeGroundTime;
private Integer foregroundCount;
public Long lastBackGroundTime;
private Long lastUsageTime;
private java.lang.String pkgName;
final com.android.server.MiuiAppUsageStats this$0; //synthetic
private Long totalForegroundTime;
/* # direct methods */
public com.android.server.MiuiAppUsageStats$AppUsageStats ( ) {
/* .locals 3 */
/* .param p1, "this$0" # Lcom/android/server/MiuiAppUsageStats; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 400 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 401 */
this.pkgName = p2;
/* .line 402 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* .line 403 */
/* iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J */
/* .line 404 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I */
/* .line 405 */
/* iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->firstForeGroundTime:J */
/* .line 406 */
/* iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastBackGroundTime:J */
/* .line 407 */
return;
} // .end method
/* # virtual methods */
public void addForegroundTime ( Long p0 ) {
/* .locals 2 */
/* .param p1, "foregroundTime" # J */
/* .line 434 */
/* iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* add-long/2addr v0, p1 */
/* iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* .line 435 */
return;
} // .end method
public Integer compareTo ( com.android.server.MiuiAppUsageStats$AppUsageStats p0 ) {
/* .locals 4 */
/* .param p1, "data" # Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
/* .line 465 */
/* iget-wide v0, p1, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* iget-wide v2, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
v0 = java.lang.Long .compare ( v0,v1,v2,v3 );
} // .end method
public Integer compareTo ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 391 */
/* check-cast p1, Lcom/android/server/MiuiAppUsageStats$AppUsageStats; */
p1 = (( com.android.server.MiuiAppUsageStats$AppUsageStats ) p0 ).compareTo ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->compareTo(Lcom/android/server/MiuiAppUsageStats$AppUsageStats;)I
} // .end method
public Integer getForegroundCount ( ) {
/* .locals 1 */
/* .line 418 */
/* iget v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I */
} // .end method
public Long getLastUsageTime ( ) {
/* .locals 2 */
/* .line 414 */
/* iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J */
/* return-wide v0 */
} // .end method
public java.lang.String getPkgName ( ) {
/* .locals 1 */
/* .line 446 */
v0 = this.pkgName;
} // .end method
public Long getTotalForegroundTime ( ) {
/* .locals 2 */
/* .line 410 */
/* iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* return-wide v0 */
} // .end method
public void increaseForegroundCount ( ) {
/* .locals 1 */
/* .line 442 */
/* iget v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I */
/* add-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I */
/* .line 443 */
return;
} // .end method
public Boolean isValid ( ) {
/* .locals 4 */
/* .line 460 */
/* iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void minusForegroundTime ( Long p0 ) {
/* .locals 2 */
/* .param p1, "foregroundTime" # J */
/* .line 438 */
/* iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* sub-long/2addr v0, p1 */
/* iput-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* .line 439 */
return;
} // .end method
public void setForegroundCount ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "foregroundCount" # I */
/* .line 430 */
/* iput p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->foregroundCount:I */
/* .line 431 */
return;
} // .end method
public void setLastUsageTime ( Long p0 ) {
/* .locals 0 */
/* .param p1, "lastUsageTime" # J */
/* .line 426 */
/* iput-wide p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J */
/* .line 427 */
return;
} // .end method
public void setPkgName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .line 450 */
this.pkgName = p1;
/* .line 451 */
return;
} // .end method
public void setTotalForegroundTime ( Long p0 ) {
/* .locals 0 */
/* .param p1, "totalForegroundTime" # J */
/* .line 422 */
/* iput-wide p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->totalForegroundTime:J */
/* .line 423 */
return;
} // .end method
public void updateLastUsageTime ( Long p0 ) {
/* .locals 2 */
/* .param p1, "time" # J */
/* .line 454 */
/* iget-wide v0, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J */
/* cmp-long v0, p1, v0 */
/* if-lez v0, :cond_0 */
/* .line 455 */
/* iput-wide p1, p0, Lcom/android/server/MiuiAppUsageStats$AppUsageStats;->lastUsageTime:J */
/* .line 457 */
} // :cond_0
return;
} // .end method
