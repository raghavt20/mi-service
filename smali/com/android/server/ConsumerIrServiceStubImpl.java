public class com.android.server.ConsumerIrServiceStubImpl extends com.android.server.ConsumerIrServiceStub {
	 /* .source "ConsumerIrServiceStubImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.ConsumerIrServiceStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String DEVICE_REGION;
private static final java.lang.String EVENT_APP_ID;
private static final java.lang.String EVENT_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final java.lang.String KEY_EVENT_TYPE;
private static final java.lang.String ONE_TRACK_ACTION;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
/* # direct methods */
public static void $r8$lambda$rIS-hcid3WK0naZPYhQszbhFe_U ( com.android.server.ConsumerIrServiceStubImpl p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/android/server/ConsumerIrServiceStubImpl;->lambda$reportIrToOneTrack$0()V */
	 return;
} // .end method
static com.android.server.ConsumerIrServiceStubImpl ( ) {
	 /* .locals 2 */
	 /* .line 29 */
	 final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
	 final String v1 = "CN"; // const-string v1, "CN"
	 android.os.SystemProperties .get ( v0,v1 );
	 return;
} // .end method
public com.android.server.ConsumerIrServiceStubImpl ( ) {
	 /* .locals 0 */
	 /* .line 21 */
	 /* invoke-direct {p0}, Lcom/android/server/ConsumerIrServiceStub;-><init>()V */
	 return;
} // .end method
private void lambda$reportIrToOneTrack$0 ( ) { //synthethic
	 /* .locals 5 */
	 /* .line 39 */
	 final String v0 = "ConsumerIrService"; // const-string v0, "ConsumerIrService"
	 try { // :try_start_0
		 /* new-instance v1, Landroid/content/Intent; */
		 final String v2 = "onetrack.action.TRACK_EVENT"; // const-string v2, "onetrack.action.TRACK_EVENT"
		 /* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 40 */
		 /* .local v1, "intent":Landroid/content/Intent; */
		 final String v2 = "com.miui.analytics"; // const-string v2, "com.miui.analytics"
		 (( android.content.Intent ) v1 ).setPackage ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 41 */
		 final String v2 = "APP_ID"; // const-string v2, "APP_ID"
		 final String v3 = "31000000091"; // const-string v3, "31000000091"
		 (( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 42 */
		 final String v2 = "EVENT_NAME"; // const-string v2, "EVENT_NAME"
		 final String v3 = "ir"; // const-string v3, "ir"
		 (( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 43 */
		 final String v2 = "PACKAGE"; // const-string v2, "PACKAGE"
		 v3 = this.mContext;
		 (( android.content.Context ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
		 (( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 44 */
		 /* new-instance v2, Landroid/os/Bundle; */
		 /* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
		 /* .line 45 */
		 /* .local v2, "params":Landroid/os/Bundle; */
		 v3 = com.android.server.ConsumerIrServiceStubImpl.DEVICE_REGION;
		 final String v4 = "CN"; // const-string v4, "CN"
		 v3 = 		 (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 /* .line 46 */
			 int v3 = 2; // const/4 v3, 0x2
			 (( android.content.Intent ) v1 ).setFlags ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
			 /* .line 48 */
		 } // :cond_0
		 final String v3 = "ir_status"; // const-string v3, "ir_status"
		 int v4 = 1; // const/4 v4, 0x1
		 (( android.content.Intent ) v1 ).putExtra ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
		 /* .line 49 */
		 (( android.content.Intent ) v1 ).putExtras ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
		 /* .line 50 */
		 v3 = this.mContext;
		 (( android.content.Context ) v3 ).startService ( v1 ); // invoke-virtual {v3, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 53 */
		 /* nop */
	 } // .end local v1 # "intent":Landroid/content/Intent;
} // .end local v2 # "params":Landroid/os/Bundle;
/* .line 51 */
/* :catch_0 */
/* move-exception v1 */
/* .line 52 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "error reportIrToOneTrack:"; // const-string v3, "error reportIrToOneTrack:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 54 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
final String v1 = "reportIrToOneTrack"; // const-string v1, "reportIrToOneTrack"
android.util.Slog .d ( v0,v1 );
/* .line 55 */
return;
} // .end method
/* # virtual methods */
public void reportIrToOneTrack ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 33 */
v0 = com.android.server.ConsumerIrServiceStubImpl.DEVICE_REGION;
final String v1 = "IN"; // const-string v1, "IN"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 34 */
return;
/* .line 36 */
} // :cond_0
this.mContext = p1;
/* .line 37 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* new-instance v1, Lcom/android/server/ConsumerIrServiceStubImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/android/server/ConsumerIrServiceStubImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/server/ConsumerIrServiceStubImpl;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 56 */
return;
} // .end method
