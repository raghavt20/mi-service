public class com.android.server.WatchdogImpl extends com.android.server.WatchdogStub {
	 /* .source "WatchdogImpl.java" */
	 /* # annotations */
	 /* .annotation runtime Lcom/miui/base/annotations/MiuiStubHead; */
	 /* manifestName = "com.android.server.WatchdogStub$$" */
} // .end annotation
/* # static fields */
private static final java.lang.String APP_ID;
private static final Long CHECK_LAYER_TIMEOUT;
public static final Boolean DEBUG;
private static final java.lang.String EMPTY_BINDER;
private static final java.lang.String EMPTY_MESSAGE;
private static final java.lang.String EVENT_NAME;
private static final java.lang.String EXTRA_APP_ID;
private static final java.lang.String EXTRA_EVENT_NAME;
private static final java.lang.String EXTRA_PACKAGE_NAME;
private static final Integer FLAG_NON_ANONYMOUS;
private static final Integer FLAG_NOT_LIMITED_BY_USER_EXPERIENCE_PLAN;
private static final Long GB;
private static final java.lang.String HEAP_MONITOR_TAG;
private static final Integer HEAP_MONITOR_THRESHOLD;
private static final java.lang.String INTENT_ACTION_ONETRACK;
private static final java.lang.String INTENT_PACKAGE_ONETRACK;
private static final java.lang.String INVAILD_FORMAT_BINDER;
private static final Long KB;
private static final Integer MAX_TRACES;
private static final Long MB;
private static final java.lang.String NATIVE_HANG_ENABLE;
private static final java.lang.String NATIVE_HANG_PROCESS_NAME;
private static final Integer NATIVE_HANG_THRESHOLD;
private static final Boolean OOM_CRASH_ON_WATCHDOG;
private static final java.lang.String PACKAGE;
private static final java.lang.String PID;
private static final java.lang.String PROC_NAME;
private static final java.lang.String PROP_PRESERVE_LAYER_LEAK_CRIME_SCENE;
private static final java.lang.String REBOOT_MIUITEST;
private static final java.lang.String REGEX_PATTERN;
private static final java.lang.String SYSTEM_NATIVE_HANG_COUNT;
private static final java.lang.String SYSTEM_SERVER;
private static final java.lang.String SYSTEM_SERVER_START_COUNT;
private static final java.lang.String TAG;
private static final java.lang.String WATCHDOG_DIR;
private static final Integer WATCHDOG_THRESHOLD;
private static final java.util.List diableThreadList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static Boolean isHalfOom;
public static final java.util.Set nativeHangProcList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.content.Context mContext;
Integer mPendingTime;
Integer mToPid;
java.lang.String mToProcess;
/* # direct methods */
static com.android.server.WatchdogImpl ( ) {
/* .locals 3 */
/* .line 68 */
/* nop */
/* .line 69 */
final String v0 = "persist.sys.oom_crash_on_watchdog"; // const-string v0, "persist.sys.oom_crash_on_watchdog"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.android.server.WatchdogImpl.OOM_CRASH_ON_WATCHDOG = (v0!= 0);
/* .line 70 */
/* nop */
/* .line 71 */
final String v0 = "persist.sys.oom_crash_on_watchdog_size"; // const-string v0, "persist.sys.oom_crash_on_watchdog_size"
/* const/16 v2, 0x1f4 */
v0 = android.os.SystemProperties .getInt ( v0,v2 );
/* .line 78 */
com.android.server.WatchdogImpl.isHalfOom = (v1!= 0);
/* .line 80 */
final String v0 = "PackageManager"; // const-string v0, "PackageManager"
/* filled-new-array {v0}, [Ljava/lang/String; */
java.util.Arrays .asList ( v0 );
/* .line 108 */
/* new-instance v0, Ljava/util/HashSet; */
final String v1 = "/system/bin/surfaceflinger"; // const-string v1, "/system/bin/surfaceflinger"
final String v2 = "/system/bin/keystore2/data/misc/keystore"; // const-string v2, "/system/bin/keystore2/data/misc/keystore"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
/* .line 109 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V */
/* .line 108 */
return;
} // .end method
 com.android.server.WatchdogImpl ( ) {
/* .locals 1 */
/* .line 114 */
/* invoke-direct {p0}, Lcom/android/server/WatchdogStub;-><init>()V */
/* .line 449 */
int v0 = 0; // const/4 v0, 0x0
this.mToProcess = v0;
/* .line 450 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I */
/* .line 451 */
/* iput v0, p0, Lcom/android/server/WatchdogImpl;->mToPid:I */
/* .line 115 */
v0 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 116 */
com.android.server.WatchdogImpl .scheduleLayerLeakCheck ( );
/* .line 117 */
android.view.SurfaceControlImpl .enableRenovationForLibraryTests ( );
/* .line 119 */
} // :cond_0
return;
} // .end method
private Boolean checkIsMiuiMtbf ( ) {
/* .locals 2 */
/* .line 433 */
final String v0 = "persist.reboot.miuitest"; // const-string v0, "persist.reboot.miuitest"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
private Boolean checkLabTestStatus ( ) {
/* .locals 1 */
/* .line 437 */
v0 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0}, Lcom/android/server/WatchdogImpl;->checkIsMiuiMtbf()Z */
/* if-nez v0, :cond_0 */
/* .line 438 */
int v0 = 0; // const/4 v0, 0x0
/* .line 440 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean checkNativeHang ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p1, "binderTransInfo" # Ljava/lang/String; */
/* .line 458 */
v0 = /* invoke-direct {p0}, Lcom/android/server/WatchdogImpl;->checkNativeHangEnable()Z */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MIUIScout Watchdog"; // const-string v2, "MIUIScout Watchdog"
/* if-nez v0, :cond_0 */
/* .line 459 */
final String v0 = "naitve hang feature disable."; // const-string v0, "naitve hang feature disable."
android.util.Slog .w ( v2,v0 );
/* .line 460 */
/* .line 463 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_5 */
final String v0 = "Here are no Binder-related exception messages available."; // const-string v0, "Here are no Binder-related exception messages available."
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_5 */
/* .line 464 */
final String v0 = "regex match failed"; // const-string v0, "regex match failed"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* goto/16 :goto_1 */
/* .line 469 */
} // :cond_1
try { // :try_start_0
final String v0 = "\\bfrom((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+to((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+elapsed:((?:\\s+)?\\d*\\.?\\d*).\\b"; // const-string v0, "\\bfrom((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+to((?:\\s+)?\\d+)\\(((?:\\s+)?\\S+)\\):.+elapsed:((?:\\s+)?\\d*\\.?\\d*).\\b"
java.util.regex.Pattern .compile ( v0 );
/* .line 470 */
/* .local v0, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v0 ).matcher ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 471 */
/* .local v3, "matcher":Ljava/util/regex/Matcher; */
v4 = (( java.util.regex.Matcher ) v3 ).find ( ); // invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z
/* if-nez v4, :cond_2 */
/* .line 472 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "nativeHang ("; // const-string v5, "nativeHang ("
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ") regex match failed"; // const-string v5, ") regex match failed"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v4 );
/* .line 473 */
/* .line 475 */
} // :cond_2
int v4 = 1; // const/4 v4, 0x1
(( java.util.regex.Matcher ) v3 ).group ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v5 ).trim ( ); // invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
/* .line 476 */
/* .local v5, "fromPid":I */
int v6 = 2; // const/4 v6, 0x2
(( java.util.regex.Matcher ) v3 ).group ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v6 ).trim ( ); // invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 477 */
/* .local v6, "fromProcess":Ljava/lang/String; */
int v7 = 3; // const/4 v7, 0x3
(( java.util.regex.Matcher ) v3 ).group ( v7 ); // invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
v7 = java.lang.Integer .parseInt ( v7 );
/* iput v7, p0, Lcom/android/server/WatchdogImpl;->mToPid:I */
/* .line 478 */
int v7 = 4; // const/4 v7, 0x4
(( java.util.regex.Matcher ) v3 ).group ( v7 ); // invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
this.mToProcess = v7;
/* .line 479 */
int v7 = 5; // const/4 v7, 0x5
(( java.util.regex.Matcher ) v3 ).group ( v7 ); // invoke-virtual {v3, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
(( java.lang.String ) v7 ).trim ( ); // invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;
java.lang.Double .valueOf ( v7 );
(( java.lang.Double ) v7 ).doubleValue ( ); // invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v7 */
/* .line 480 */
/* .local v7, "timeout":D */
java.lang.Math .round ( v7,v8 );
/* move-result-wide v9 */
/* long-to-int v9, v9 */
/* iput v9, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I */
/* .line 482 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "fromPid="; // const-string v10, "fromPid="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v5 ); // invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", fromProcess="; // const-string v10, ", fromProcess="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6 ); // invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ", mToPid="; // const-string v10, ", mToPid="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v10, p0, Lcom/android/server/WatchdogImpl;->mToPid:I */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = ", mToProcess="; // const-string v10, ", mToProcess="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v10 = this.mToProcess;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v10 = ", mPendingTime="; // const-string v10, ", mPendingTime="
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v10, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v9 );
/* .line 486 */
v9 = android.os.Process .myPid ( );
/* if-ne v9, v5, :cond_4 */
v9 = com.android.server.WatchdogImpl.nativeHangProcList;
v9 = v10 = this.mToProcess;
if ( v9 != null) { // if-eqz v9, :cond_4
/* iget v9, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* const/16 v10, 0x3c */
/* if-ge v9, v10, :cond_3 */
/* .line 494 */
} // .end local v0 # "pattern":Ljava/util/regex/Pattern;
} // .end local v3 # "matcher":Ljava/util/regex/Matcher;
} // .end local v5 # "fromPid":I
} // .end local v6 # "fromProcess":Ljava/lang/String;
} // .end local v7 # "timeout":D
} // :cond_3
/* nop */
/* .line 495 */
final String v0 = "Occur native hang"; // const-string v0, "Occur native hang"
android.util.Slog .e ( v2,v0 );
/* .line 496 */
/* .line 488 */
/* .restart local v0 # "pattern":Ljava/util/regex/Pattern; */
/* .restart local v3 # "matcher":Ljava/util/regex/Matcher; */
/* .restart local v5 # "fromPid":I */
/* .restart local v6 # "fromProcess":Ljava/lang/String; */
/* .restart local v7 # "timeout":D */
} // :cond_4
} // :goto_0
/* .line 491 */
} // .end local v0 # "pattern":Ljava/util/regex/Pattern;
} // .end local v3 # "matcher":Ljava/util/regex/Matcher;
} // .end local v5 # "fromPid":I
} // .end local v6 # "fromProcess":Ljava/lang/String;
} // .end local v7 # "timeout":D
/* :catch_0 */
/* move-exception v0 */
/* .line 492 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v3 = "checkNativeHang process Error: "; // const-string v3, "checkNativeHang process Error: "
android.util.Slog .w ( v2,v3,v0 );
/* .line 493 */
/* .line 465 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_5
} // :goto_1
} // .end method
private Boolean checkNativeHangEnable ( ) {
/* .locals 2 */
/* .line 421 */
final String v0 = "persist.sys.stability.nativehang.enable"; // const-string v0, "persist.sys.stability.nativehang.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
private static java.io.File getWatchdogDir ( ) {
/* .locals 3 */
/* .line 291 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miuilog/stability/scout/watchdog"; // const-string v1, "/data/miuilog/stability/scout/watchdog"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 292 */
/* .local v0, "tracesDir":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
v1 = (( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
/* if-nez v1, :cond_0 */
/* .line 293 */
int v1 = 0; // const/4 v1, 0x0
/* .line 295 */
} // :cond_0
/* const/16 v1, 0x1ed */
int v2 = -1; // const/4 v2, -0x1
android.os.FileUtils .setPermissions ( v0,v1,v2,v2 );
/* .line 296 */
} // .end method
static void lambda$reportEvent$1 ( miui.mqsas.sdk.event.WatchdogEvent p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "event" # Lmiui/mqsas/sdk/event/WatchdogEvent; */
/* .line 359 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v0 ).reportWatchdogEvent ( p0 ); // invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportWatchdogEvent(Lmiui/mqsas/sdk/event/WatchdogEvent;)Z
return;
} // .end method
static Boolean lambda$saveWatchdogTrace$0 ( java.lang.String p0, java.util.TreeSet p1, java.io.File p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "prefix" # Ljava/lang/String; */
/* .param p1, "existingTraces" # Ljava/util/TreeSet; */
/* .param p2, "pathname" # Ljava/io/File; */
/* .line 238 */
(( java.io.File ) p2 ).getName ( ); // invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;
v0 = (( java.lang.String ) v0 ).startsWith ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 239 */
(( java.util.TreeSet ) p1 ).add ( p2 ); // invoke-virtual {p1, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
/* .line 241 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static java.lang.String prettySize ( Long p0 ) {
/* .locals 8 */
/* .param p0, "byte_count" # J */
/* .line 300 */
int v0 = 4; // const/4 v0, 0x4
/* new-array v1, v0, [J */
/* fill-array-data v1, :array_0 */
/* .line 307 */
/* .local v1, "kUnitThresholds":[J */
/* new-array v0, v0, [J */
/* fill-array-data v0, :array_1 */
/* .line 308 */
/* .local v0, "kBytesPerUnit":[J */
final String v2 = "MB"; // const-string v2, "MB"
final String v3 = "GB"; // const-string v3, "GB"
final String v4 = "B"; // const-string v4, "B"
final String v5 = "KB"; // const-string v5, "KB"
/* filled-new-array {v4, v5, v2, v3}, [Ljava/lang/String; */
/* .line 309 */
/* .local v2, "kUnitStrings":[Ljava/lang/String; */
final String v3 = ""; // const-string v3, ""
/* .line 310 */
/* .local v3, "negative_str":Ljava/lang/String; */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, p0, v4 */
/* if-gez v4, :cond_0 */
/* .line 311 */
final String v3 = "-"; // const-string v3, "-"
/* .line 312 */
/* neg-long p0, p0 */
/* .line 314 */
} // :cond_0
/* array-length v4, v1 */
/* .line 315 */
/* .local v4, "i":I */
} // :cond_1
/* add-int/lit8 v4, v4, -0x1 */
/* if-lez v4, :cond_2 */
/* .line 316 */
/* aget-wide v5, v1, v4 */
/* cmp-long v5, p0, v5 */
/* if-ltz v5, :cond_1 */
/* .line 317 */
/* nop */
/* .line 320 */
} // :cond_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget-wide v6, v0, v4 */
/* div-long v6, p0, v6 */
(( java.lang.StringBuilder ) v5 ).append ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* aget-object v6, v2, v4 */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :array_0 */
/* .array-data 8 */
/* 0x0 */
/* 0x2800 */
/* 0xa00000 */
/* 0x280000000L */
} // .end array-data
/* :array_1 */
/* .array-data 8 */
/* 0x1 */
/* 0x400 */
/* 0x100000 */
/* 0x40000000 */
} // .end array-data
} // .end method
private static void reportEvent ( Integer p0, java.lang.String p1, java.io.File p2, java.util.List p3, java.lang.String p4, java.lang.String p5 ) {
/* .locals 10 */
/* .param p0, "type" # I */
/* .param p1, "subject" # Ljava/lang/String; */
/* .param p2, "trace" # Ljava/io/File; */
/* .param p4, "mBinderInfo" # Ljava/lang/String; */
/* .param p5, "mUuid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/lang/String;", */
/* "Ljava/io/File;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/Watchdog$HandlerChecker;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 326 */
/* .local p3, "handlerCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/Watchdog$HandlerChecker;>;" */
/* new-instance v0, Lmiui/mqsas/sdk/event/WatchdogEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/WatchdogEvent;-><init>()V */
/* .line 327 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/WatchdogEvent; */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setType ( p0 ); // invoke-virtual {v0, p0}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setType(I)V
/* .line 328 */
v1 = android.os.Process .myPid ( );
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setPid ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setPid(I)V
/* .line 329 */
/* const-string/jumbo v1, "system_server" */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setProcessName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setProcessName(Ljava/lang/String;)V
/* .line 330 */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setPackageName ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setPackageName(Ljava/lang/String;)V
/* .line 331 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setTimeStamp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setTimeStamp(J)V
/* .line 332 */
int v1 = 1; // const/4 v1, 0x1
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setSystem ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setSystem(Z)V
/* .line 333 */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setSummary ( p1 ); // invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setSummary(Ljava/lang/String;)V
/* .line 334 */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setDetails ( p1 ); // invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setDetails(Ljava/lang/String;)V
/* .line 335 */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setBinderTransactionInfo ( p4 ); // invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setBinderTransactionInfo(Ljava/lang/String;)V
/* .line 336 */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setUuid ( p5 ); // invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setUuid(Ljava/lang/String;)V
/* .line 337 */
final String v2 = "persist.sys.zygote.start_pid"; // const-string v2, "persist.sys.zygote.start_pid"
android.os.SystemProperties .get ( v2 );
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setZygotePid ( v2 ); // invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setZygotePid(Ljava/lang/String;)V
/* .line 338 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 339 */
(( java.io.File ) p2 ).getAbsolutePath ( ); // invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setLogName ( v2 ); // invoke-virtual {v0, v2}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setLogName(Ljava/lang/String;)V
/* .line 341 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 342 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 343 */
/* .local v2, "details":Ljava/lang/StringBuilder; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = } // :goto_0
/* if-ge v3, v4, :cond_2 */
/* .line 344 */
/* check-cast v4, Lcom/android/server/Watchdog$HandlerChecker; */
(( com.android.server.Watchdog$HandlerChecker ) v4 ).getThread ( ); // invoke-virtual {v4}, Lcom/android/server/Watchdog$HandlerChecker;->getThread()Ljava/lang/Thread;
(( java.lang.Thread ) v4 ).getStackTrace ( ); // invoke-virtual {v4}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;
/* .line 345 */
/* .local v4, "st":[Ljava/lang/StackTraceElement; */
/* array-length v5, v4 */
int v6 = 0; // const/4 v6, 0x0
} // :goto_1
/* if-ge v6, v5, :cond_1 */
/* aget-object v7, v4, v6 */
/* .line 346 */
/* .local v7, "element":Ljava/lang/StackTraceElement; */
final String v8 = " at "; // const-string v8, " at "
(( java.lang.StringBuilder ) v2 ).append ( v8 ); // invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v7 ); // invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v9 = "\n"; // const-string v9, "\n"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 345 */
} // .end local v7 # "element":Ljava/lang/StackTraceElement;
/* add-int/lit8 v6, v6, 0x1 */
/* .line 348 */
} // :cond_1
final String v5 = "\n\n"; // const-string v5, "\n\n"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 343 */
} // .end local v4 # "st":[Ljava/lang/StackTraceElement;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 350 */
} // .end local v3 # "i":I
} // :cond_2
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setDetails ( v3 ); // invoke-virtual {v0, v3}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setDetails(Ljava/lang/String;)V
/* .line 354 */
} // .end local v2 # "details":Ljava/lang/StringBuilder;
} // :cond_3
int v2 = 2; // const/4 v2, 0x2
/* if-ne v2, p0, :cond_4 */
/* .line 355 */
(( miui.mqsas.sdk.event.WatchdogEvent ) v0 ).setEnsureReport ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/WatchdogEvent;->setEnsureReport(Z)V
/* .line 357 */
} // :cond_4
v1 = miui.mqsas.scout.ScoutUtils .isLibraryTest ( );
/* if-nez v1, :cond_5 */
/* .line 358 */
/* new-instance v1, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, v0}, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda1;-><init>(Lmiui/mqsas/sdk/event/WatchdogEvent;)V */
miui.mqsas.sdk.MQSEventManagerDelegate .runtimeWithTimeout ( v1 );
/* .line 361 */
} // :cond_5
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v1 ).reportWatchdogEvent ( v0 ); // invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportWatchdogEvent(Lmiui/mqsas/sdk/event/WatchdogEvent;)Z
/* .line 363 */
} // :goto_2
return;
} // .end method
private void reportNativeHang ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 397 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
/* .line 398 */
return;
/* .line 401 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "onetrack.action.TRACK_EVENT"; // const-string v1, "onetrack.action.TRACK_EVENT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 402 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.analytics"; // const-string v1, "com.miui.analytics"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 403 */
final String v1 = "APP_ID"; // const-string v1, "APP_ID"
final String v2 = "31000401706"; // const-string v2, "31000401706"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 404 */
final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
final String v2 = "native_hang"; // const-string v2, "native_hang"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 405 */
final String v1 = "PACKAGE"; // const-string v1, "PACKAGE"
final String v2 = "android"; // const-string v2, "android"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 406 */
final String v1 = "native_hang_pid"; // const-string v1, "native_hang_pid"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 407 */
final String v1 = "naitve_hang_packageName"; // const-string v1, "naitve_hang_packageName"
(( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 408 */
int v1 = 3; // const/4 v1, 0x3
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 409 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 417 */
/* nop */
} // .end local v0 # "intent":Landroid/content/Intent;
/* .line 414 */
/* :catch_0 */
/* move-exception v0 */
/* .line 415 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
/* .line 416 */
final String v1 = "MIUIScout Watchdog"; // const-string v1, "MIUIScout Watchdog"
final String v2 = "Upload onetrack exception!"; // const-string v2, "Upload onetrack exception!"
android.util.Slog .e ( v1,v2,v0 );
/* .line 418 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static java.io.File saveWatchdogTrace ( Boolean p0, java.lang.String p1, java.lang.String p2, java.io.File p3 ) {
/* .locals 16 */
/* .param p0, "halfWatchdog" # Z */
/* .param p1, "subject" # Ljava/lang/String; */
/* .param p2, "binderTransInfo" # Ljava/lang/String; */
/* .param p3, "file" # Ljava/io/File; */
/* .line 229 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v2, p3 */
com.android.server.WatchdogImpl .getWatchdogDir ( );
/* .line 230 */
/* .local v3, "tracesDir":Ljava/io/File; */
final String v4 = "Watchdog"; // const-string v4, "Watchdog"
/* if-nez v3, :cond_0 */
/* .line 231 */
final String v0 = "Failed to get watchdog dir"; // const-string v0, "Failed to get watchdog dir"
android.util.Slog .w ( v4,v0 );
/* .line 232 */
/* .line 234 */
} // :cond_0
if ( p0 != null) { // if-eqz p0, :cond_1
final String v0 = "pre_watchdog_pid_"; // const-string v0, "pre_watchdog_pid_"
} // :cond_1
/* const-string/jumbo v0, "watchdog_pid_" */
} // :goto_0
/* move-object v5, v0 */
/* .line 236 */
/* .local v5, "prefix":Ljava/lang/String; */
/* new-instance v0, Ljava/util/TreeSet; */
/* invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V */
/* move-object v6, v0 */
/* .line 237 */
/* .local v6, "existingTraces":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;" */
/* new-instance v0, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, v5, v6}, Lcom/android/server/WatchdogImpl$$ExternalSyntheticLambda0;-><init>(Ljava/lang/String;Ljava/util/TreeSet;)V */
(( java.io.File ) v3 ).listFiles ( v0 ); // invoke-virtual {v3, v0}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;
/* .line 243 */
v0 = (( java.util.TreeSet ) v6 ).size ( ); // invoke-virtual {v6}, Ljava/util/TreeSet;->size()I
int v7 = 3; // const/4 v7, 0x3
/* if-lt v0, v7, :cond_3 */
/* .line 244 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
int v7 = 2; // const/4 v7, 0x2
/* if-ge v0, v7, :cond_2 */
/* .line 245 */
(( java.util.TreeSet ) v6 ).pollLast ( ); // invoke-virtual {v6}, Ljava/util/TreeSet;->pollLast()Ljava/lang/Object;
/* .line 244 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 247 */
} // .end local v0 # "i":I
} // :cond_2
(( java.util.TreeSet ) v6 ).iterator ( ); // invoke-virtual {v6}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;
v7 = } // :goto_2
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Ljava/io/File; */
/* .line 248 */
/* .local v7, "trace":Ljava/io/File; */
(( java.io.File ) v7 ).delete ( ); // invoke-virtual {v7}, Ljava/io/File;->delete()Z
/* .line 249 */
} // .end local v7 # "trace":Ljava/io/File;
/* .line 251 */
} // :cond_3
/* new-instance v0, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v7, "yyyy_MM_dd_HH_mm_ss" */
v8 = java.util.Locale.US;
/* invoke-direct {v0, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V */
/* move-object v7, v0 */
/* .line 252 */
/* .local v7, "dateFormat":Ljava/text/SimpleDateFormat; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = android.os.Process .myPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = "_"; // const-string v8, "_"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v8, Ljava/util/Date; */
/* invoke-direct {v8}, Ljava/util/Date;-><init>()V */
(( java.text.SimpleDateFormat ) v7 ).format ( v8 ); // invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 253 */
/* .local v8, "fileName":Ljava/lang/String; */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, v3, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* move-object v9, v0 */
/* .line 254 */
/* .local v9, "watchdogFile":Ljava/io/File; */
int v10 = 0; // const/4 v10, 0x0
/* .line 255 */
/* .local v10, "fout":Ljava/io/FileOutputStream; */
int v11 = 0; // const/4 v11, 0x0
/* .line 257 */
/* .local v11, "fin":Ljava/io/FileInputStream; */
if ( v2 != null) { // if-eqz v2, :cond_4
try { // :try_start_0
/* invoke-virtual/range {p3 ..p3}, Ljava/io/File;->length()J */
/* move-result-wide v12 */
/* const-wide/16 v14, 0x0 */
/* cmp-long v0, v12, v14 */
/* if-lez v0, :cond_4 */
/* .line 258 */
/* new-instance v0, Ljava/io/FileInputStream; */
/* invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v11, v0 */
/* .line 260 */
} // :cond_4
/* new-instance v0, Ljava/io/FileOutputStream; */
/* invoke-direct {v0, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* move-object v10, v0 */
/* .line 263 */
/* const/16 v0, 0xa */
/* if-nez v11, :cond_5 */
/* .line 264 */
final String v12 = "Subject"; // const-string v12, "Subject"
v13 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v12 ).getBytes ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.io.FileOutputStream ) v10 ).write ( v12 ); // invoke-virtual {v10, v12}, Ljava/io/FileOutputStream;->write([B)V
/* .line 265 */
(( java.io.FileOutputStream ) v10 ).write ( v0 ); // invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V
/* .line 266 */
(( java.io.FileOutputStream ) v10 ).write ( v0 ); // invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V
/* .line 267 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 268 */
v12 = java.nio.charset.StandardCharsets.UTF_8;
(( java.lang.String ) v1 ).getBytes ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B
(( java.io.FileOutputStream ) v10 ).write ( v12 ); // invoke-virtual {v10, v12}, Ljava/io/FileOutputStream;->write([B)V
/* .line 269 */
(( java.io.FileOutputStream ) v10 ).write ( v0 ); // invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V
/* .line 270 */
(( java.io.FileOutputStream ) v10 ).write ( v0 ); // invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V
/* .line 273 */
} // :cond_5
int v12 = 0; // const/4 v12, 0x0
android.os.FileUtils .copyInternalUserspace ( v11,v10,v12,v12,v12 );
/* .line 275 */
} // :cond_6
} // :goto_3
/* if-nez p0, :cond_7 */
/* .line 276 */
(( java.io.FileOutputStream ) v10 ).write ( v0 ); // invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write(I)V
/* .line 277 */
final String v0 = "------ ps info ------"; // const-string v0, "------ ps info ------"
(( java.lang.String ) v0 ).getBytes ( ); // invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v10 ).write ( v0 ); // invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write([B)V
/* .line 278 */
int v0 = 0; // const/4 v0, 0x0
com.android.server.ScoutHelper .getPsInfo ( v0 );
(( java.lang.String ) v0 ).getBytes ( ); // invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B
(( java.io.FileOutputStream ) v10 ).write ( v0 ); // invoke-virtual {v10, v0}, Ljava/io/FileOutputStream;->write([B)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 283 */
} // :cond_7
/* nop */
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v11 );
/* .line 284 */
libcore.io.IoUtils .closeQuietly ( v10 );
/* .line 285 */
/* .line 283 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 280 */
/* :catch_0 */
/* move-exception v0 */
/* .line 281 */
/* .local v0, "e":Ljava/lang/Exception; */
try { // :try_start_1
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "Failed to save watchdog trace: "; // const-string v13, "Failed to save watchdog trace: "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).getMessage ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v12 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 283 */
/* nop */
} // .end local v0 # "e":Ljava/lang/Exception;
/* .line 286 */
} // :goto_5
/* const/16 v0, 0x1a4 */
int v4 = -1; // const/4 v4, -0x1
android.os.FileUtils .setPermissions ( v9,v0,v4,v4 );
/* .line 287 */
/* .line 283 */
} // :goto_6
libcore.io.IoUtils .closeQuietly ( v11 );
/* .line 284 */
libcore.io.IoUtils .closeQuietly ( v10 );
/* .line 285 */
/* throw v0 */
} // .end method
private static void scheduleLayerLeakCheck ( ) {
/* .locals 4 */
/* .line 122 */
com.android.internal.os.BackgroundThread .getHandler ( );
/* .line 123 */
/* .local v0, "h":Landroid/os/Handler; */
/* new-instance v1, Lcom/android/server/WatchdogImpl$1; */
/* invoke-direct {v1, v0}, Lcom/android/server/WatchdogImpl$1;-><init>(Landroid/os/Handler;)V */
/* const-wide/32 v2, 0x249f0 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 136 */
return;
} // .end method
private void setWatchdogCount ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "count" # I */
/* .line 429 */
/* const-string/jumbo v0, "sys.stability.nativehang.count" */
java.lang.String .valueOf ( p1 );
android.os.SystemProperties .set ( v0,v1 );
/* .line 430 */
return;
} // .end method
/* # virtual methods */
public Boolean checkAndSaveStatus ( Boolean p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "waitHalf" # Z */
/* .param p2, "binderTransInfo" # Ljava/lang/String; */
/* .line 501 */
v0 = /* invoke-direct {p0}, Lcom/android/server/WatchdogImpl;->checkLabTestStatus()Z */
int v1 = 0; // const/4 v1, 0x0
final String v2 = "MIUIScout Watchdog"; // const-string v2, "MIUIScout Watchdog"
/* if-nez v0, :cond_0 */
/* .line 502 */
final String v0 = "only checkNativeHang when MIUI MTBF& non-laboratory test scenarios"; // const-string v0, "only checkNativeHang when MIUI MTBF& non-laboratory test scenarios"
android.util.Slog .w ( v2,v0 );
/* .line 504 */
/* .line 507 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 508 */
final String v0 = "only checkNativeHang when watchdog"; // const-string v0, "only checkNativeHang when watchdog"
android.util.Slog .w ( v2,v0 );
/* .line 509 */
/* .line 512 */
} // :cond_1
v0 = /* invoke-direct {p0, p2}, Lcom/android/server/WatchdogImpl;->checkNativeHang(Ljava/lang/String;)Z */
/* if-nez v0, :cond_2 */
/* .line 513 */
final String v0 = "not native hang process"; // const-string v0, "not native hang process"
android.util.Slog .w ( v2,v0 );
/* .line 514 */
/* .line 517 */
} // :cond_2
v0 = (( com.android.server.WatchdogImpl ) p0 ).getWatchdogCount ( ); // invoke-virtual {p0}, Lcom/android/server/WatchdogImpl;->getWatchdogCount()I
/* .line 518 */
/* .local v0, "oldCount":I */
/* const-string/jumbo v3, "sys.stability.nativehang.processname" */
int v4 = 1; // const/4 v4, 0x1
/* if-nez v0, :cond_3 */
/* .line 519 */
/* add-int/lit8 v1, v0, 0x1 */
/* invoke-direct {p0, v1}, Lcom/android/server/WatchdogImpl;->setWatchdogCount(I)V */
/* .line 520 */
v1 = this.mToProcess;
android.os.SystemProperties .set ( v3,v1 );
/* .line 521 */
/* .line 524 */
} // :cond_3
/* if-ne v0, v4, :cond_4 */
/* .line 525 */
android.os.SystemProperties .get ( v3 );
v6 = this.mToProcess;
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 526 */
/* add-int/lit8 v1, v0, 0x1 */
/* invoke-direct {p0, v1}, Lcom/android/server/WatchdogImpl;->setWatchdogCount(I)V */
/* .line 527 */
/* iget v1, p0, Lcom/android/server/WatchdogImpl;->mToPid:I */
v3 = this.mToProcess;
/* invoke-direct {p0, v1, v3}, Lcom/android/server/WatchdogImpl;->reportNativeHang(ILjava/lang/String;)V */
/* .line 528 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Occur native hang, processName="; // const-string v3, "Occur native hang, processName="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mToProcess;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", mPendTime="; // const-string v3, ", mPendTime="
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/server/WatchdogImpl;->mPendingTime:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = "s."; // const-string v3, "s."
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v1 );
/* .line 530 */
final String v1 = "Native process hang reboot time larger than 2 time, reboot device!"; // const-string v1, "Native process hang reboot time larger than 2 time, reboot device!"
android.util.Slog .e ( v2,v1 );
/* .line 531 */
/* .line 534 */
} // :cond_4
/* invoke-direct {p0, v1}, Lcom/android/server/WatchdogImpl;->setWatchdogCount(I)V */
/* .line 535 */
final String v2 = "null"; // const-string v2, "null"
android.os.SystemProperties .set ( v3,v2 );
/* .line 536 */
} // .end method
public void checkOOMState ( ) {
/* .locals 16 */
/* .line 190 */
final String v1 = "HeapUsage Monitor"; // const-string v1, "HeapUsage Monitor"
/* sget-boolean v0, Lcom/android/server/WatchdogImpl;->OOM_CRASH_ON_WATCHDOG:Z */
/* if-nez v0, :cond_0 */
/* .line 191 */
return;
/* .line 195 */
} // :cond_0
try { // :try_start_0
java.lang.Runtime .getRuntime ( );
/* .line 196 */
/* .local v0, "runtime":Ljava/lang/Runtime; */
(( java.lang.Runtime ) v0 ).totalMemory ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J
/* move-result-wide v2 */
/* .line 197 */
/* .local v2, "dalvikMax":J */
(( java.lang.Runtime ) v0 ).freeMemory ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J
/* move-result-wide v4 */
/* .line 198 */
/* .local v4, "dalvikFree":J */
/* sub-long v6, v2, v4 */
/* .line 199 */
/* .local v6, "allocHeapSpace":J */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "HeapAllocSize : "; // const-string v9, "HeapAllocSize : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.WatchdogImpl .prettySize ( v6,v7 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = "; monitorSize : "; // const-string v9, "; monitorSize : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = "MB"; // const-string v10, "MB"
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v8 );
/* .line 201 */
/* int-to-long v10, v9 */
/* const-wide/32 v12, 0x100000 */
/* mul-long/2addr v10, v12 */
/* cmp-long v8, v6, v10 */
int v10 = 0; // const/4 v10, 0x0
/* if-ltz v8, :cond_3 */
/* .line 202 */
(( java.lang.Runtime ) v0 ).gc ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V
/* .line 203 */
(( java.lang.Runtime ) v0 ).totalMemory ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J
/* move-result-wide v14 */
/* move-wide v2, v14 */
/* .line 204 */
(( java.lang.Runtime ) v0 ).freeMemory ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J
/* move-result-wide v14 */
/* move-wide v4, v14 */
/* .line 205 */
/* sub-long v6, v2, v4 */
/* .line 206 */
/* int-to-long v8, v9 */
/* mul-long/2addr v8, v12 */
/* cmp-long v8, v6, v8 */
/* if-gez v8, :cond_1 */
/* .line 207 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "After performing Gc, HeapAllocSize : "; // const-string v9, "After performing Gc, HeapAllocSize : "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.WatchdogImpl .prettySize ( v6,v7 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v8 );
/* .line 208 */
com.android.server.WatchdogImpl.isHalfOom = (v10!= 0);
/* .line 209 */
return;
/* .line 211 */
} // :cond_1
/* sget-boolean v8, Lcom/android/server/WatchdogImpl;->isHalfOom:Z */
/* if-nez v8, :cond_2 */
/* .line 216 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Half Oom, Heap of System_Server has allocated "; // const-string v9, "Half Oom, Heap of System_Server has allocated "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.WatchdogImpl .prettySize ( v6,v7 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v8 );
/* .line 217 */
int v8 = 1; // const/4 v8, 0x1
com.android.server.WatchdogImpl.isHalfOom = (v8!= 0);
/* .line 212 */
} // :cond_2
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "Heap of System_Server has allocated "; // const-string v9, "Heap of System_Server has allocated "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.server.WatchdogImpl .prettySize ( v6,v7 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " , So trigger OutOfMemoryError crash"; // const-string v9, " , So trigger OutOfMemoryError crash"
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 214 */
/* .local v8, "msg":Ljava/lang/String; */
/* new-instance v9, Ljava/lang/OutOfMemoryError; */
/* invoke-direct {v9, v8}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/android/server/WatchdogImpl;
/* throw v9 */
/* .line 220 */
} // .end local v8 # "msg":Ljava/lang/String;
/* .restart local p0 # "this":Lcom/android/server/WatchdogImpl; */
} // :cond_3
com.android.server.WatchdogImpl.isHalfOom = (v10!= 0);
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 224 */
} // .end local v0 # "runtime":Ljava/lang/Runtime;
} // .end local v2 # "dalvikMax":J
} // .end local v4 # "dalvikFree":J
} // .end local v6 # "allocHeapSpace":J
} // :goto_0
/* .line 222 */
/* :catch_0 */
/* move-exception v0 */
/* .line 223 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "checkOOMState:"; // const-string v3, "checkOOMState:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 225 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
public Integer getWatchdogCount ( ) {
/* .locals 2 */
/* .line 425 */
/* const-string/jumbo v0, "sys.stability.nativehang.count" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
} // .end method
public void init ( android.content.Context p0, com.android.server.am.ActivityManagerService p1 ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "activity" # Lcom/android/server/am/ActivityManagerService; */
/* .line 393 */
this.mContext = p1;
/* .line 394 */
return;
} // .end method
public Boolean isNativeHang ( ) {
/* .locals 2 */
/* .line 541 */
v0 = (( com.android.server.WatchdogImpl ) p0 ).getWatchdogCount ( ); // invoke-virtual {p0}, Lcom/android/server/WatchdogImpl;->getWatchdogCount()I
int v1 = 2; // const/4 v1, 0x2
/* if-lt v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean needRebootSystem ( ) {
/* .locals 1 */
/* .line 546 */
v0 = (( com.android.server.WatchdogImpl ) p0 ).isNativeHang ( ); // invoke-virtual {p0}, Lcom/android/server/WatchdogImpl;->isNativeHang()Z
/* if-nez v0, :cond_0 */
/* .line 547 */
int v0 = 0; // const/4 v0, 0x0
/* .line 550 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
void onHalfWatchdog ( java.lang.String p0, java.io.File p1, java.util.List p2, java.lang.String p3, java.lang.String p4 ) {
/* .locals 10 */
/* .param p1, "subject" # Ljava/lang/String; */
/* .param p2, "trace" # Ljava/io/File; */
/* .param p4, "binderTransInfo" # Ljava/lang/String; */
/* .param p5, "mUuid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/io/File;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/Watchdog$HandlerChecker;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 141 */
/* .local p3, "blockedCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/Watchdog$HandlerChecker;>;" */
v0 = android.os.Debug .isDebuggerConnected ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 142 */
return;
/* .line 147 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
v1 = if ( p3 != null) { // if-eqz p3, :cond_1
/* if-ne v1, v0, :cond_1 */
/* .line 148 */
int v1 = 0; // const/4 v1, 0x0
/* check-cast v1, Lcom/android/server/Watchdog$HandlerChecker; */
(( com.android.server.Watchdog$HandlerChecker ) v1 ).getName ( ); // invoke-virtual {v1}, Lcom/android/server/Watchdog$HandlerChecker;->getName()Ljava/lang/String;
/* .line 149 */
/* .local v1, "name":Ljava/lang/String; */
v2 = v2 = com.android.server.WatchdogImpl.diableThreadList;
if ( v2 != null) { // if-eqz v2, :cond_1
return;
/* .line 151 */
} // .end local v1 # "name":Ljava/lang/String;
} // :cond_1
com.android.server.ResourcePressureUtil .currentPsiState ( );
/* .line 152 */
/* .local v1, "psi":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Enter HALF_WATCHDOG \n"; // const-string v3, "Enter HALF_WATCHDOG \n"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MIUIScout Watchdog"; // const-string v3, "MIUIScout Watchdog"
android.util.Slog .w ( v3,v2 );
/* .line 153 */
com.android.server.ScoutSystemMonitor .getInstance ( );
int v3 = 2; // const/4 v3, 0x2
(( com.android.server.ScoutSystemMonitor ) v2 ).setWorkMessage ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/server/ScoutSystemMonitor;->setWorkMessage(I)V
/* .line 154 */
com.android.server.WatchdogImpl .saveWatchdogTrace ( v0,p1,p4,p2 );
/* .line 155 */
/* const/16 v4, 0x180 */
/* move-object v5, p1 */
/* move-object v6, p2 */
/* move-object v7, p3 */
/* move-object v8, p4 */
/* move-object v9, p5 */
/* invoke-static/range {v4 ..v9}, Lcom/android/server/WatchdogImpl;->reportEvent(ILjava/lang/String;Ljava/io/File;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 156 */
return;
} // .end method
void onWatchdog ( java.lang.String p0, java.io.File p1, java.util.List p2, java.lang.String p3, java.lang.String p4 ) {
/* .locals 8 */
/* .param p1, "subject" # Ljava/lang/String; */
/* .param p2, "trace" # Ljava/io/File; */
/* .param p4, "binderTransInfo" # Ljava/lang/String; */
/* .param p5, "mUuid" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/io/File;", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/Watchdog$HandlerChecker;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 161 */
/* .local p3, "blockedCheckers":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/Watchdog$HandlerChecker;>;" */
v0 = android.os.Debug .isDebuggerConnected ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 162 */
return;
/* .line 164 */
} // :cond_0
com.android.server.ResourcePressureUtil .currentPsiState ( );
/* .line 165 */
/* .local v0, "psi":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Enter WATCHDOG \n"; // const-string v2, "Enter WATCHDOG \n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MIUIScout Watchdog"; // const-string v2, "MIUIScout Watchdog"
android.util.Slog .w ( v2,v1 );
/* .line 166 */
com.android.server.am.AppProfilerStub .getInstance ( );
int v2 = 1; // const/4 v2, 0x1
/* .line 167 */
if ( p2 != null) { // if-eqz p2, :cond_1
int v1 = 2; // const/4 v1, 0x2
} // :cond_1
/* const/16 v1, 0x181 */
} // :goto_0
/* move v2, v1 */
/* .line 168 */
/* .local v2, "eventType":I */
int v1 = 0; // const/4 v1, 0x0
com.android.server.WatchdogImpl .saveWatchdogTrace ( v1,p1,p4,p2 );
/* .line 169 */
/* move-object v3, p1 */
/* move-object v4, p2 */
/* move-object v5, p3 */
/* move-object v6, p4 */
/* move-object v7, p5 */
/* invoke-static/range {v2 ..v7}, Lcom/android/server/WatchdogImpl;->reportEvent(ILjava/lang/String;Ljava/io/File;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 170 */
return;
} // .end method
public Boolean retryDumpMyStacktrace ( java.lang.String p0, Long p1, Long p2 ) {
/* .locals 15 */
/* .param p1, "fileName" # Ljava/lang/String; */
/* .param p2, "timeStartMs" # J */
/* .param p4, "totalTimeoutMs" # J */
/* .line 367 */
/* const/16 v0, 0x7d0 */
/* .line 368 */
/* .local v0, "QUICK_FAILURE_MILLS":I */
/* const/16 v1, 0x1388 */
/* .line 369 */
/* .local v1, "MIN_TIMEOUT_MILLIS":I */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* sub-long v2, v2, p2 */
/* .line 370 */
/* .local v2, "duration":J */
/* move-wide/from16 v4, p4 */
/* .line 371 */
/* .local v4, "timeoutMs":J */
/* const/16 v6, 0x3e8 */
/* .line 372 */
/* .local v6, "SLEEP_INTERVAL_MILLS":I */
int v7 = 3; // const/4 v7, 0x3
/* move v9, v7 */
/* move-wide v7, v4 */
/* move-wide/from16 v4, p2 */
/* .line 373 */
} // .end local p2 # "timeStartMs":J
/* .local v4, "timeStartMs":J */
/* .local v7, "timeoutMs":J */
/* .local v9, "retry":I */
} // :goto_0
/* const-wide/16 v10, 0x7d0 */
/* cmp-long v10, v2, v10 */
final String v11 = ", prevDumpDuration="; // const-string v11, ", prevDumpDuration="
final String v12 = "Watchdog"; // const-string v12, "Watchdog"
/* if-gez v10, :cond_2 */
/* add-int/lit8 v10, v9, -0x1 */
} // .end local v9 # "retry":I
/* .local v10, "retry":I */
/* if-lez v9, :cond_1 */
/* const-wide/16 v13, 0x1388 */
/* cmp-long v9, v7, v13 */
/* if-ltz v9, :cond_1 */
/* .line 374 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "retryDumpMyStacktrace: timeoutMs="; // const-string v13, "retryDumpMyStacktrace: timeoutMs="
(( java.lang.StringBuilder ) v9 ).append ( v13 ); // invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v7, v8 ); // invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v11 ); // invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v2, v3 ); // invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v12,v9 );
/* .line 375 */
/* const-wide/16 v13, 0x3e8 */
android.os.SystemClock .sleep ( v13,v14 );
/* .line 376 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
/* .line 377 */
/* sub-long/2addr v7, v13 */
/* .line 378 */
v9 = android.os.Process .myPid ( );
/* div-long v13, v7, v13 */
/* long-to-int v11, v13 */
/* move-object/from16 v13, p1 */
v9 = android.os.Debug .dumpJavaBacktraceToFileTimeout ( v9,v13,v11 );
if ( v9 != null) { // if-eqz v9, :cond_0
/* .line 380 */
final String v9 = "retryDumpMyStacktrace succeeded"; // const-string v9, "retryDumpMyStacktrace succeeded"
android.util.Slog .i ( v12,v9 );
/* .line 381 */
int v9 = 1; // const/4 v9, 0x1
/* .line 383 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v11 */
/* sub-long/2addr v11, v4 */
/* .line 384 */
/* .local v11, "dumpDuration":J */
/* sub-long/2addr v7, v11 */
/* .line 385 */
} // .end local v11 # "dumpDuration":J
/* move v9, v10 */
/* .line 373 */
} // :cond_1
/* move-object/from16 v13, p1 */
/* .line 386 */
/* move v9, v10 */
/* .line 373 */
} // .end local v10 # "retry":I
/* .restart local v9 # "retry":I */
} // :cond_2
/* move-object/from16 v13, p1 */
/* .line 386 */
} // :goto_1
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v14 = "retryDumpMyStacktrace failed: timeoutMs="; // const-string v14, "retryDumpMyStacktrace failed: timeoutMs="
(( java.lang.StringBuilder ) v10 ).append ( v14 ); // invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v7, v8 ); // invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).append ( v2, v3 ); // invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v12,v10 );
/* .line 388 */
int v10 = 0; // const/4 v10, 0x0
} // .end method
public void setWatchdogPropTimestamp ( Long p0 ) {
/* .locals 3 */
/* .param p1, "anrtime" # J */
/* .line 176 */
try { // :try_start_0
/* const-string/jumbo v0, "sys.service.watchdog.timestamp" */
java.lang.Long .toString ( p1,p2 );
android.os.SystemProperties .set ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 179 */
/* .line 177 */
/* :catch_0 */
/* move-exception v0 */
/* .line 178 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
final String v1 = "Watchdog"; // const-string v1, "Watchdog"
final String v2 = "Failed to set watchdog.timestamp property"; // const-string v2, "Failed to set watchdog.timestamp property"
android.util.Slog .e ( v1,v2,v0 );
/* .line 180 */
} // .end local v0 # "e":Ljava/lang/IllegalArgumentException;
} // :goto_0
return;
} // .end method
