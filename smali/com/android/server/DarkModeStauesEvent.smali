.class public Lcom/android/server/DarkModeStauesEvent;
.super Ljava/lang/Object;
.source "DarkModeStauesEvent.java"

# interfaces
.implements Lcom/android/server/DarkModeEvent;


# instance fields
.field private mAppEnable:Ljava/lang/String;

.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppName:Ljava/lang/String;

.field private mAppPkg:Ljava/lang/String;

.field private mAutoSwitch:Ljava/lang/String;

.field private mBeginTime:Ljava/lang/String;

.field private mContrastStatus:Ljava/lang/String;

.field private mDarkModeStatus:Ljava/lang/String;

.field private mEndTime:Ljava/lang/String;

.field private mEventName:Ljava/lang/String;

.field private mSettingChannel:Ljava/lang/String;

.field private mStatusAfterClick:Ljava/lang/String;

.field private mSuggest:I

.field private mSuggestClick:I

.field private mSuggestEnable:I

.field private mSuggestOpenInSetting:I

.field private mTimeModePattern:Ljava/lang/String;

.field private mTimeModeStatus:Ljava/lang/String;

.field private mTip:Ljava/lang/String;

.field private mWallPaperStatus:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mAppList:Ljava/util/List;

    .line 53
    return-void
.end method


# virtual methods
.method public clone()Lcom/android/server/DarkModeEvent;
    .locals 3

    .line 236
    new-instance v0, Lcom/android/server/DarkModeStauesEvent;

    invoke-direct {v0}, Lcom/android/server/DarkModeStauesEvent;-><init>()V

    .line 237
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 238
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getTip()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 239
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppEnable()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppEnable(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    .line 240
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppList(Ljava/util/List;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 241
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 242
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAppPkg()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAppPkg(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 243
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getAutoSwitch()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setAutoSwitch(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 244
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getBeginTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setBeginTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 245
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getContrastStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setContrastStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 246
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getDarkModeStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setDarkModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 247
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getEndTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setEndTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 248
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggest()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggest(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 249
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggestClick()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestClick(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 250
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSettingChannel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSettingChannel(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 251
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getStatusAfterClick()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setStatusAfterClick(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 252
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggestEnable()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestEnable(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 253
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getSuggestOpenInSetting()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setSuggestOpenInSetting(I)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 254
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getTimeModePattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTimeModePattern(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 255
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getTimeModeStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setTimeModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 256
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->getWallPaperStatus()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/DarkModeStauesEvent;->setWallPaperStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;

    move-result-object v0

    .line 257
    .local v0, "event":Lcom/android/server/DarkModeEvent;
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 10
    invoke-virtual {p0}, Lcom/android/server/DarkModeStauesEvent;->clone()Lcom/android/server/DarkModeEvent;

    move-result-object v0

    return-object v0
.end method

.method public getAppEnable()Ljava/lang/String;
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mAppEnable:Ljava/lang/String;

    return-object v0
.end method

.method public getAppList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mAppList:Ljava/util/List;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getAppPkg()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mAppPkg:Ljava/lang/String;

    return-object v0
.end method

.method public getAutoSwitch()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mAutoSwitch:Ljava/lang/String;

    return-object v0
.end method

.method public getBeginTime()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mBeginTime:Ljava/lang/String;

    return-object v0
.end method

.method public getContrastStatus()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mContrastStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getDarkModeStatus()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mDarkModeStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTime()Ljava/lang/String;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mEndTime:Ljava/lang/String;

    return-object v0
.end method

.method public getEventName()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mEventName:Ljava/lang/String;

    return-object v0
.end method

.method public getSettingChannel()Ljava/lang/String;
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSettingChannel:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusAfterClick()Ljava/lang/String;
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mStatusAfterClick:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggest()I
    .locals 1

    .line 200
    iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggest:I

    return v0
.end method

.method public getSuggestClick()I
    .locals 1

    .line 209
    iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestClick:I

    return v0
.end method

.method public getSuggestEnable()I
    .locals 1

    .line 218
    iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestEnable:I

    return v0
.end method

.method public getSuggestOpenInSetting()I
    .locals 1

    .line 227
    iget v0, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestOpenInSetting:I

    return v0
.end method

.method public getTimeModePattern()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mTimeModePattern:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeModeStatus()Ljava/lang/String;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mTimeModeStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getTip()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mTip:Ljava/lang/String;

    return-object v0
.end method

.method public getWallPaperStatus()Ljava/lang/String;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/android/server/DarkModeStauesEvent;->mWallPaperStatus:Ljava/lang/String;

    return-object v0
.end method

.method public setAppEnable(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "enable"    # Ljava/lang/String;

    .line 159
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mAppEnable:Ljava/lang/String;

    .line 160
    return-object p0
.end method

.method public setAppList(Ljava/util/List;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/android/server/DarkModeStauesEvent;"
        }
    .end annotation

    .line 132
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mAppList:Ljava/util/List;

    .line 133
    return-object p0
.end method

.method public setAppName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "appName"    # Ljava/lang/String;

    .line 150
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mAppName:Ljava/lang/String;

    .line 151
    return-object p0
.end method

.method public setAppPkg(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "appPkg"    # Ljava/lang/String;

    .line 141
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mAppPkg:Ljava/lang/String;

    .line 142
    return-object p0
.end method

.method public setAutoSwitch(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "autoSwitch"    # Ljava/lang/String;

    .line 168
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mAutoSwitch:Ljava/lang/String;

    .line 169
    return-object p0
.end method

.method public setBeginTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "beginTime"    # Ljava/lang/String;

    .line 96
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mBeginTime:Ljava/lang/String;

    .line 97
    return-object p0
.end method

.method public setContrastStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "montrastStatus"    # Ljava/lang/String;

    .line 123
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mContrastStatus:Ljava/lang/String;

    .line 124
    return-object p0
.end method

.method public setDarkModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "darkModeStatus"    # Ljava/lang/String;

    .line 69
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mDarkModeStatus:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public setEndTime(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "endTime"    # Ljava/lang/String;

    .line 105
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mEndTime:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method public setEventName(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "eventName"    # Ljava/lang/String;

    .line 60
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mEventName:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public setSettingChannel(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "settingChannel"    # Ljava/lang/String;

    .line 186
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSettingChannel:Ljava/lang/String;

    .line 187
    return-object p0
.end method

.method public setStatusAfterClick(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "statusAfterClick"    # Ljava/lang/String;

    .line 177
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mStatusAfterClick:Ljava/lang/String;

    .line 178
    return-object p0
.end method

.method public setSuggest(I)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "suggest"    # I

    .line 204
    iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggest:I

    .line 205
    return-object p0
.end method

.method public setSuggestClick(I)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "suggestClick"    # I

    .line 213
    iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestClick:I

    .line 214
    return-object p0
.end method

.method public setSuggestEnable(I)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "suggestEnable"    # I

    .line 222
    iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestEnable:I

    .line 223
    return-object p0
.end method

.method public setSuggestOpenInSetting(I)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "suggestOpenInSetting"    # I

    .line 231
    iput p1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestOpenInSetting:I

    .line 232
    return-object p0
.end method

.method public setTimeModePattern(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "timeModePattern"    # Ljava/lang/String;

    .line 87
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mTimeModePattern:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method public setTimeModeStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "timeModeStatus"    # Ljava/lang/String;

    .line 78
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mTimeModeStatus:Ljava/lang/String;

    .line 79
    return-object p0
.end method

.method public setTip(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "tip"    # Ljava/lang/String;

    .line 195
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mTip:Ljava/lang/String;

    .line 196
    return-object p0
.end method

.method public setWallPaperStatus(Ljava/lang/String;)Lcom/android/server/DarkModeStauesEvent;
    .locals 0
    .param p1, "wallPaperStatus"    # Ljava/lang/String;

    .line 114
    iput-object p1, p0, Lcom/android/server/DarkModeStauesEvent;->mWallPaperStatus:Ljava/lang/String;

    .line 115
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DarkModeStauesEvent{mEventName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/DarkModeStauesEvent;->mEventName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mTip=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mTip:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mDarkModeStatus=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mDarkModeStatus:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mTimeModeStatus=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mTimeModeStatus:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mTimeModePattern=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mTimeModePattern:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mBeginTime=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mBeginTime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mEndTime=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mEndTime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mWallPaperStatus=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mWallPaperStatus:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mContrastStatus=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mContrastStatus:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mAppList="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mAppList:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mAppPkg=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mAppPkg:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mAppName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mAppName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mAppEnable=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mAppEnable:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mAutoSwitch=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mAutoSwitch:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mStatusAfterClick=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mStatusAfterClick:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mSettingChannel=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/DarkModeStauesEvent;->mSettingChannel:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSuggest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggest:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSuggestClick="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestClick:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSuggestEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestEnable:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSuggestOpenInSetting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/server/DarkModeStauesEvent;->mSuggestOpenInSetting:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
