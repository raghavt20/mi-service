.class public Lcom/android/net/module/util/netlink/StructNdOptRdnss;
.super Lcom/android/net/module/util/netlink/NdOption;
.source "StructNdOptRdnss.java"


# static fields
.field public static final MIN_OPTION_LEN:B = 0x3t

.field private static final TAG:Ljava/lang/String;

.field public static final TYPE:I = 0x19


# instance fields
.field public final header:Lcom/android/net/module/util/structs/RdnssOption;

.field public final servers:[Ljava/net/Inet6Address;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    const-class v0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>([Ljava/net/Inet6Address;J)V
    .locals 7
    .param p1, "servers"    # [Ljava/net/Inet6Address;
    .param p2, "lifetime"    # J

    .line 62
    array-length v0, p1

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x19

    invoke-direct {p0, v1, v0}, Lcom/android/net/module/util/netlink/NdOption;-><init>(BI)V

    .line 64
    const-string v0, "Recursive DNS Servers address array must not be null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 65
    array-length v0, p1

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcom/android/net/module/util/structs/RdnssOption;

    const/16 v2, 0x19

    array-length v1, p1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-byte v3, v1

    const/4 v4, 0x0

    move-object v1, v0

    move-wide v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/android/net/module/util/structs/RdnssOption;-><init>(BBSJ)V

    iput-object v0, p0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->header:Lcom/android/net/module/util/structs/RdnssOption;

    .line 71
    invoke-virtual {p1}, [Ljava/net/Inet6Address;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/net/Inet6Address;

    iput-object v0, p0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->servers:[Ljava/net/Inet6Address;

    .line 72
    return-void

    .line 66
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DNS server address array must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNdOptRdnss;
    .locals 7
    .param p0, "buf"    # Ljava/nio/ByteBuffer;

    .line 82
    const/4 v0, 0x0

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/16 v2, 0x18

    if-ge v1, v2, :cond_0

    goto/16 :goto_1

    .line 84
    :cond_0
    :try_start_0
    const-class v1, Lcom/android/net/module/util/structs/RdnssOption;

    invoke-static {v1, p0}, Lcom/android/net/module/util/Struct;->parse(Ljava/lang/Class;Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/net/module/util/structs/RdnssOption;

    .line 85
    .local v1, "header":Lcom/android/net/module/util/structs/RdnssOption;
    iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->type:B

    const/16 v3, 0x19

    if-ne v2, v3, :cond_3

    .line 88
    iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B

    const/4 v3, 0x3

    if-lt v2, v3, :cond_2

    iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2

    .line 92
    iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B

    add-int/lit8 v2, v2, -0x1

    div-int/lit8 v2, v2, 0x2

    .line 93
    .local v2, "numOfDnses":I
    new-array v3, v2, [Ljava/net/Inet6Address;

    .line 94
    .local v3, "servers":[Ljava/net/Inet6Address;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_1

    .line 95
    const/16 v5, 0x10

    new-array v5, v5, [B

    .line 96
    .local v5, "rawAddress":[B
    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 97
    invoke-static {v5}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v6

    check-cast v6, Ljava/net/Inet6Address;

    aput-object v6, v3, v4

    .line 94
    .end local v5    # "rawAddress":[B
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 99
    .end local v4    # "i":I
    :cond_1
    new-instance v4, Lcom/android/net/module/util/netlink/StructNdOptRdnss;

    iget-wide v5, v1, Lcom/android/net/module/util/structs/RdnssOption;->lifetime:J

    invoke-direct {v4, v3, v5, v6}, Lcom/android/net/module/util/netlink/StructNdOptRdnss;-><init>([Ljava/net/Inet6Address;J)V

    return-object v4

    .line 89
    .end local v2    # "numOfDnses":I
    .end local v3    # "servers":[Ljava/net/Inet6Address;
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-byte v4, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .end local p0    # "buf":Ljava/nio/ByteBuffer;
    throw v2

    .line 86
    .restart local p0    # "buf":Ljava/nio/ByteBuffer;
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-byte v4, v1, Lcom/android/net/module/util/structs/RdnssOption;->type:B

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .end local p0    # "buf":Ljava/nio/ByteBuffer;
    throw v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v1    # "header":Lcom/android/net/module/util/structs/RdnssOption;
    .restart local p0    # "buf":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid RDNSS option: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    return-object v0

    .line 82
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_1
    return-object v0
.end method


# virtual methods
.method public toByteBuffer()Ljava/nio/ByteBuffer;
    .locals 2

    .line 118
    const-class v0, Lcom/android/net/module/util/structs/RdnssOption;

    invoke-static {v0}, Lcom/android/net/module/util/Struct;->getSize(Ljava/lang/Class;)I

    move-result v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->servers:[Ljava/net/Inet6Address;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 120
    .local v0, "buf":Ljava/nio/ByteBuffer;
    invoke-virtual {p0, v0}, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 121
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 122
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/util/StringJoiner;

    const-string v1, "["

    const-string v2, "]"

    const-string v3, ","

    invoke-direct {v0, v3, v1, v2}, Ljava/util/StringJoiner;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 129
    .local v0, "sj":Ljava/util/StringJoiner;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->servers:[Ljava/net/Inet6Address;

    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 130
    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/net/Inet6Address;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/StringJoiner;->add(Ljava/lang/CharSequence;)Ljava/util/StringJoiner;

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    .end local v1    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->header:Lcom/android/net/module/util/structs/RdnssOption;

    invoke-virtual {v1}, Lcom/android/net/module/util/structs/RdnssOption;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/StringJoiner;->toString()Ljava/lang/String;

    move-result-object v2

    filled-new-array {v1, v2}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "NdOptRdnss(%s,servers:%s)"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected writeToByteBuffer(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .line 110
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->header:Lcom/android/net/module/util/structs/RdnssOption;

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/structs/RdnssOption;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 111
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->servers:[Ljava/net/Inet6Address;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 112
    aget-object v1, v1, v0

    invoke-virtual {v1}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    .end local v0    # "i":I
    :cond_0
    return-void
.end method
