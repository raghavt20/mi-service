.class public Lcom/android/net/module/util/netlink/StructRtMsg;
.super Lcom/android/net/module/util/Struct;
.source "StructRtMsg.java"


# static fields
.field public static final STRUCT_SIZE:I = 0xc


# instance fields
.field public final dstLen:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x1
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final family:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x0
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final flags:J
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x8
        type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final protocol:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x5
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final scope:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x6
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final srcLen:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x2
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final table:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x4
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final tos:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x3
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final type:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x7
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field


# direct methods
.method constructor <init>(SSSSSSSSJ)V
    .locals 0
    .param p1, "family"    # S
    .param p2, "dstLen"    # S
    .param p3, "srcLen"    # S
    .param p4, "tos"    # S
    .param p5, "table"    # S
    .param p6, "protocol"    # S
    .param p7, "scope"    # S
    .param p8, "type"    # S
    .param p9, "flags"    # J

    .line 61
    invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V

    .line 62
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->family:S

    .line 63
    iput-short p2, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->dstLen:S

    .line 64
    iput-short p3, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->srcLen:S

    .line 65
    iput-short p4, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->tos:S

    .line 66
    iput-short p5, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->table:S

    .line 67
    iput-short p6, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->protocol:S

    .line 68
    iput-short p7, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->scope:S

    .line 69
    iput-short p8, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->type:S

    .line 70
    iput-wide p9, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->flags:J

    .line 71
    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructRtMsg;
    .locals 2
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 82
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 85
    :cond_0
    const-class v0, Lcom/android/net/module/util/netlink/StructRtMsg;

    invoke-static {v0, p0}, Lcom/android/net/module/util/Struct;->parse(Ljava/lang/Class;Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/net/module/util/netlink/StructRtMsg;

    return-object v0
.end method


# virtual methods
.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 93
    invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructRtMsg;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 94
    return-void
.end method
