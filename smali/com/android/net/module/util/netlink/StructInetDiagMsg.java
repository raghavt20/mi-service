public class com.android.net.module.util.netlink.StructInetDiagMsg {
	 /* .source "StructInetDiagMsg.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public com.android.net.module.util.netlink.StructInetDiagSockId id;
	 public Long idiag_expires;
	 public Object idiag_family;
	 public Long idiag_inode;
	 public Object idiag_retrans;
	 public Long idiag_rqueue;
	 public Object idiag_state;
	 public Object idiag_timer;
	 public Integer idiag_uid;
	 public Long idiag_wqueue;
	 /* # direct methods */
	 public com.android.net.module.util.netlink.StructInetDiagMsg ( ) {
		 /* .locals 0 */
		 /* .line 44 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static com.android.net.module.util.netlink.StructInetDiagMsg parse ( java.nio.ByteBuffer p0 ) {
		 /* .locals 3 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 68 */
		 v0 = 		 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
		 /* const/16 v1, 0x48 */
		 int v2 = 0; // const/4 v2, 0x0
		 /* if-ge v0, v1, :cond_0 */
		 /* .line 69 */
		 /* .line 71 */
	 } // :cond_0
	 /* new-instance v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg; */
	 /* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;-><init>()V */
	 /* .line 72 */
	 /* .local v0, "struct":Lcom/android/net/module/util/netlink/StructInetDiagMsg; */
	 v1 = 	 (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
	 v1 = 	 com.android.net.module.util.netlink.StructInetDiagMsg .unsignedByte ( v1 );
	 /* iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S */
	 /* .line 73 */
	 v1 = 	 (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
	 v1 = 	 com.android.net.module.util.netlink.StructInetDiagMsg .unsignedByte ( v1 );
	 /* iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_state:S */
	 /* .line 74 */
	 v1 = 	 (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
	 v1 = 	 com.android.net.module.util.netlink.StructInetDiagMsg .unsignedByte ( v1 );
	 /* iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_timer:S */
	 /* .line 75 */
	 v1 = 	 (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
	 v1 = 	 com.android.net.module.util.netlink.StructInetDiagMsg .unsignedByte ( v1 );
	 /* iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_retrans:S */
	 /* .line 76 */
	 /* iget-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S */
	 com.android.net.module.util.netlink.StructInetDiagSockId .parse ( p0,v1 );
	 this.id = v1;
	 /* .line 77 */
	 /* if-nez v1, :cond_1 */
	 /* .line 78 */
	 /* .line 80 */
} // :cond_1
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
java.lang.Integer .toUnsignedLong ( v1 );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_expires:J */
/* .line 81 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
java.lang.Integer .toUnsignedLong ( v1 );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_rqueue:J */
/* .line 82 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
java.lang.Integer .toUnsignedLong ( v1 );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_wqueue:J */
/* .line 83 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I */
/* .line 84 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
java.lang.Integer .toUnsignedLong ( v1 );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_inode:J */
/* .line 85 */
} // .end method
private static Object unsignedByte ( Object p0 ) {
/* .locals 1 */
/* .param p0, "b" # B */
/* .line 60 */
/* and-int/lit16 v0, p0, 0xff */
/* int-to-short v0, v0 */
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 90 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StructInetDiagMsg{ idiag_family{"; // const-string v1, "StructInetDiagMsg{ idiag_family{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_state{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_state:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_timer{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_timer:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_retrans{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_retrans:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, id{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.id;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_expires{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_expires:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_rqueue{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_rqueue:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_wqueue{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_wqueue:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_uid{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_inode{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_inode:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
