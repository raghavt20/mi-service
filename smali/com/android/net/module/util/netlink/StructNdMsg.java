public class com.android.net.module.util.netlink.StructNdMsg {
	 /* .source "StructNdMsg.java" */
	 /* # static fields */
	 public static Object NTF_MASTER;
	 public static Object NTF_PROXY;
	 public static Object NTF_ROUTER;
	 public static Object NTF_SELF;
	 public static Object NTF_USE;
	 public static final Object NUD_DELAY;
	 public static final Object NUD_FAILED;
	 public static final Object NUD_INCOMPLETE;
	 public static final Object NUD_NOARP;
	 public static final Object NUD_NONE;
	 public static final Object NUD_PERMANENT;
	 public static final Object NUD_PROBE;
	 public static final Object NUD_REACHABLE;
	 public static final Object NUD_STALE;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Object ndm_family;
	 public Object ndm_flags;
	 public Integer ndm_ifindex;
	 public Object ndm_state;
	 public Object ndm_type;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.StructNdMsg ( ) {
		 /* .locals 1 */
		 /* .line 80 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* sput-byte v0, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_USE:B */
		 /* .line 81 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* sput-byte v0, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_SELF:B */
		 /* .line 82 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* sput-byte v0, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_MASTER:B */
		 /* .line 83 */
		 /* const/16 v0, 0x8 */
		 /* sput-byte v0, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_PROXY:B */
		 /* .line 84 */
		 /* const/16 v0, -0x80 */
		 /* sput-byte v0, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_ROUTER:B */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNdMsg ( ) {
		 /* .locals 1 */
		 /* .line 152 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 153 */
		 /* int-to-byte v0, v0 */
		 /* iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_family:B */
		 /* .line 154 */
		 return;
	 } // .end method
	 private static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 119 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v1, 0xc */
			 /* if-lt v0, v1, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public static Boolean isNudStateConnected ( Object p0 ) {
	 /* .locals 1 */
	 /* .param p0, "nudState" # S */
	 /* .line 68 */
	 /* and-int/lit16 v0, p0, 0xc2 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean isNudStateValid ( Object p0 ) {
/* .locals 1 */
/* .param p0, "nudState" # S */
/* .line 75 */
v0 = com.android.net.module.util.netlink.StructNdMsg .isNudStateConnected ( p0 );
/* if-nez v0, :cond_1 */
/* and-int/lit8 v0, p0, 0x1c */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public static com.android.net.module.util.netlink.StructNdMsg parse ( java.nio.ByteBuffer p0 ) {
/* .locals 4 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 130 */
v0 = com.android.net.module.util.netlink.StructNdMsg .hasAvailableSpace ( p0 );
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 135 */
} // :cond_0
/* new-instance v0, Lcom/android/net/module/util/netlink/StructNdMsg; */
/* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNdMsg;-><init>()V */
/* .line 136 */
/* .local v0, "struct":Lcom/android/net/module/util/netlink/StructNdMsg; */
v1 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* iput-byte v1, v0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_family:B */
/* .line 137 */
v1 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* .line 138 */
/* .local v1, "pad1":B */
v2 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* .line 139 */
/* .local v2, "pad2":S */
v3 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v3, v0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_ifindex:I */
/* .line 140 */
v3 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v3, v0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S */
/* .line 141 */
v3 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* iput-byte v3, v0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_flags:B */
/* .line 142 */
v3 = (( java.nio.ByteBuffer ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B
/* iput-byte v3, v0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_type:B */
/* .line 143 */
} // .end method
private static java.lang.String stringForNudFlags ( Object p0 ) {
/* .locals 3 */
/* .param p0, "flags" # B */
/* .line 87 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 88 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* sget-byte v1, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_USE:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 89 */
final String v1 = "NTF_USE"; // const-string v1, "NTF_USE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 91 */
} // :cond_0
/* sget-byte v1, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_SELF:B */
/* and-int/2addr v1, p0 */
/* const-string/jumbo v2, "|" */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 92 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1 */
/* .line 93 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 95 */
} // :cond_1
final String v1 = "NTF_SELF"; // const-string v1, "NTF_SELF"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 97 */
} // :cond_2
/* sget-byte v1, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_MASTER:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 98 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_3 */
/* .line 99 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 101 */
} // :cond_3
final String v1 = "NTF_MASTER"; // const-string v1, "NTF_MASTER"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 103 */
} // :cond_4
/* sget-byte v1, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_PROXY:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 104 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_5 */
/* .line 105 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 107 */
} // :cond_5
final String v1 = "NTF_PROXY"; // const-string v1, "NTF_PROXY"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 109 */
} // :cond_6
/* sget-byte v1, Lcom/android/net/module/util/netlink/StructNdMsg;->NTF_ROUTER:B */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 110 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_7 */
/* .line 111 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 113 */
} // :cond_7
final String v1 = "NTF_ROUTER"; // const-string v1, "NTF_ROUTER"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 115 */
} // :cond_8
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String stringForNudState ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nudState" # S */
/* .line 49 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 60 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown NUD state: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 58 */
/* :sswitch_0 */
final String v0 = "NUD_PERMANENT"; // const-string v0, "NUD_PERMANENT"
/* .line 57 */
/* :sswitch_1 */
final String v0 = "NUD_NOARP"; // const-string v0, "NUD_NOARP"
/* .line 56 */
/* :sswitch_2 */
final String v0 = "NUD_FAILED"; // const-string v0, "NUD_FAILED"
/* .line 55 */
/* :sswitch_3 */
final String v0 = "NUD_PROBE"; // const-string v0, "NUD_PROBE"
/* .line 54 */
/* :sswitch_4 */
final String v0 = "NUD_DELAY"; // const-string v0, "NUD_DELAY"
/* .line 53 */
/* :sswitch_5 */
final String v0 = "NUD_STALE"; // const-string v0, "NUD_STALE"
/* .line 52 */
/* :sswitch_6 */
final String v0 = "NUD_REACHABLE"; // const-string v0, "NUD_REACHABLE"
/* .line 51 */
/* :sswitch_7 */
final String v0 = "NUD_INCOMPLETE"; // const-string v0, "NUD_INCOMPLETE"
/* .line 50 */
/* :sswitch_8 */
final String v0 = "NUD_NONE"; // const-string v0, "NUD_NONE"
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_8 */
/* 0x1 -> :sswitch_7 */
/* 0x2 -> :sswitch_6 */
/* 0x4 -> :sswitch_5 */
/* 0x8 -> :sswitch_4 */
/* 0x10 -> :sswitch_3 */
/* 0x20 -> :sswitch_2 */
/* 0x40 -> :sswitch_1 */
/* 0x80 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public Boolean nudConnected ( ) {
/* .locals 1 */
/* .line 176 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S */
v0 = com.android.net.module.util.netlink.StructNdMsg .isNudStateConnected ( v0 );
} // .end method
public Boolean nudValid ( ) {
/* .locals 1 */
/* .line 183 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S */
v0 = com.android.net.module.util.netlink.StructNdMsg .isNudStateValid ( v0 );
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 163 */
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_family:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 164 */
int v0 = 0; // const/4 v0, 0x0
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 165 */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 166 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_ifindex:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 167 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 168 */
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_flags:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 169 */
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_type:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 170 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 188 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = ""; // const-string v1, ""
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v2, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " ("; // const-string v2, " ("
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v3, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S */
com.android.net.module.util.netlink.StructNdMsg .stringForNudState ( v3 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 189 */
/* .local v0, "stateStr":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v4, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_flags:B */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v2, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_flags:B */
com.android.net.module.util.netlink.StructNdMsg .stringForNudFlags ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 190 */
/* .local v1, "flagsStr":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "StructNdMsg{ family{"; // const-string v3, "StructNdMsg{ family{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v3, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_family:B */
/* .line 191 */
com.android.net.module.util.netlink.NetlinkConstants .stringForAddressFamily ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, ifindex{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_ifindex:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, state{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, flags{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, type{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v3, p0, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_type:B */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} }" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 190 */
} // .end method
