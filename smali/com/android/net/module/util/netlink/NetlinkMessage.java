public class com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "NetlinkMessage.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 protected final com.android.net.module.util.netlink.StructNlMsgHdr mHeader;
	 /* # direct methods */
	 public com.android.net.module.util.netlink.NetlinkMessage ( ) {
		 /* .locals 0 */
		 /* .param p1, "nlmsghdr" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .line 92 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 93 */
		 this.mHeader = p1;
		 /* .line 94 */
		 return;
	 } // .end method
	 public static com.android.net.module.util.netlink.NetlinkMessage parse ( java.nio.ByteBuffer p0, Integer p1 ) {
		 /* .locals 6 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .param p1, "nlFamily" # I */
		 /* .line 48 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
		 } // :cond_0
		 int v0 = -1; // const/4 v0, -0x1
		 /* .line 49 */
		 /* .local v0, "startPosition":I */
	 } // :goto_0
	 com.android.net.module.util.netlink.StructNlMsgHdr .parse ( p0 );
	 /* .line 50 */
	 /* .local v1, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
	 int v2 = 0; // const/4 v2, 0x0
	 /* if-nez v1, :cond_1 */
	 /* .line 51 */
	 /* .line 54 */
} // :cond_1
/* iget v3, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
v3 = com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v3 );
/* .line 55 */
/* .local v3, "messageLength":I */
/* add-int/lit8 v4, v3, -0x10 */
/* .line 56 */
/* .local v4, "payloadLength":I */
/* if-ltz v4, :cond_7 */
v5 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* if-le v4, v5, :cond_2 */
/* .line 64 */
} // :cond_2
/* iget-short v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* const/16 v5, 0xf */
/* if-gt v2, v5, :cond_3 */
/* .line 65 */
com.android.net.module.util.netlink.NetlinkMessage .parseCtlMessage ( v1,p0,v4 );
/* .line 72 */
} // :cond_3
/* if-ne p1, v2, :cond_4 */
/* .line 73 */
com.android.net.module.util.netlink.NetlinkMessage .parseRtMessage ( v1,p0 );
/* .local v2, "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage; */
/* .line 74 */
} // .end local v2 # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
} // :cond_4
/* if-ne p1, v2, :cond_5 */
/* .line 75 */
com.android.net.module.util.netlink.NetlinkMessage .parseInetDiagMessage ( v1,p0 );
/* .restart local v2 # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage; */
/* .line 76 */
} // .end local v2 # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
} // :cond_5
/* if-ne p1, v2, :cond_6 */
/* .line 77 */
com.android.net.module.util.netlink.NetlinkMessage .parseNfMessage ( v1,p0 );
/* .restart local v2 # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage; */
/* .line 79 */
} // .end local v2 # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
} // :cond_6
int v2 = 0; // const/4 v2, 0x0
/* .line 84 */
/* .restart local v2 # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage; */
} // :goto_1
/* add-int v5, v0, v3 */
(( java.nio.ByteBuffer ) p0 ).position ( v5 ); // invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 86 */
/* .line 58 */
} // .end local v2 # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
} // :cond_7
} // :goto_2
v5 = (( java.nio.ByteBuffer ) p0 ).limit ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I
(( java.nio.ByteBuffer ) p0 ).position ( v5 ); // invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 59 */
} // .end method
private static com.android.net.module.util.netlink.NetlinkMessage parseCtlMessage ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1, Integer p2 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .param p2, "payloadLength" # I */
/* .line 114 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* packed-switch v0, :pswitch_data_0 */
/* .line 120 */
v0 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* add-int/2addr v0, p2 */
(( java.nio.ByteBuffer ) p1 ).position ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 121 */
/* new-instance v0, Lcom/android/net/module/util/netlink/NetlinkMessage; */
/* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
/* .line 116 */
/* :pswitch_0 */
com.android.net.module.util.netlink.NetlinkErrorMessage .parse ( p0,p1 );
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static com.android.net.module.util.netlink.NetlinkMessage parseInetDiagMessage ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 152 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* packed-switch v0, :pswitch_data_0 */
/* .line 155 */
int v0 = 0; // const/4 v0, 0x0
/* .line 154 */
/* :pswitch_0 */
com.android.net.module.util.netlink.InetDiagMessage .parse ( p0,p1 );
/* :pswitch_data_0 */
/* .packed-switch 0x14 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static com.android.net.module.util.netlink.NetlinkMessage parseNfMessage ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 162 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* packed-switch v0, :pswitch_data_0 */
/* .line 168 */
/* :pswitch_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 167 */
/* :pswitch_1 */
com.android.net.module.util.netlink.ConntrackMessage .parse ( p0,p1 );
/* :pswitch_data_0 */
/* .packed-switch 0x100 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private static com.android.net.module.util.netlink.NetlinkMessage parseRtMessage ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 1 */
/* .param p0, "nlmsghdr" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 129 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* sparse-switch v0, :sswitch_data_0 */
/* .line 145 */
int v0 = 0; // const/4 v0, 0x0
/* .line 144 */
/* :sswitch_0 */
com.android.net.module.util.netlink.NduseroptMessage .parse ( p0,p1 );
/* .line 142 */
/* :sswitch_1 */
com.android.net.module.util.netlink.RtNetlinkNeighborMessage .parse ( p0,p1 );
/* .line 138 */
/* :sswitch_2 */
com.android.net.module.util.netlink.RtNetlinkRouteMessage .parse ( p0,p1 );
/* .line 135 */
/* :sswitch_3 */
com.android.net.module.util.netlink.RtNetlinkAddressMessage .parse ( p0,p1 );
/* .line 132 */
/* :sswitch_4 */
com.android.net.module.util.netlink.RtNetlinkLinkMessage .parse ( p0,p1 );
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x10 -> :sswitch_4 */
/* 0x11 -> :sswitch_4 */
/* 0x14 -> :sswitch_3 */
/* 0x15 -> :sswitch_3 */
/* 0x18 -> :sswitch_2 */
/* 0x19 -> :sswitch_2 */
/* 0x1c -> :sswitch_1 */
/* 0x1d -> :sswitch_1 */
/* 0x1e -> :sswitch_1 */
/* 0x44 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public com.android.net.module.util.netlink.StructNlMsgHdr getHeader ( ) {
/* .locals 1 */
/* .line 98 */
v0 = this.mHeader;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 108 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "NetlinkMessage{"; // const-string v1, "NetlinkMessage{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHeader;
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
