public class com.android.net.module.util.netlink.ConntrackMessage$TupleIpv4 {
	 /* .source "ConntrackMessage.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/net/module/util/netlink/ConntrackMessage; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "TupleIpv4" */
} // .end annotation
/* # instance fields */
public final java.net.Inet4Address dst;
public final java.net.Inet4Address src;
/* # direct methods */
public com.android.net.module.util.netlink.ConntrackMessage$TupleIpv4 ( ) {
/* .locals 0 */
/* .param p1, "src" # Ljava/net/Inet4Address; */
/* .param p2, "dst" # Ljava/net/Inet4Address; */
/* .line 161 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 162 */
this.src = p1;
/* .line 163 */
this.dst = p2;
/* .line 164 */
return;
} // .end method
