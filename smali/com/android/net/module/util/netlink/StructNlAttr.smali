.class public Lcom/android/net/module/util/netlink/StructNlAttr;
.super Ljava/lang/Object;
.source "StructNlAttr.java"


# static fields
.field public static final NLA_F_NESTED:I = 0x8000

.field public static final NLA_HEADERLEN:I = 0x4


# instance fields
.field public nla_len:S

.field public nla_type:S

.field public nla_value:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 139
    return-void
.end method

.method public constructor <init>(SB)V
    .locals 2
    .param p1, "type"    # S
    .param p2, "value"    # B

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 142
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 143
    const/4 v0, 0x1

    new-array v0, v0, [B

    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 144
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    const/4 v1, 0x0

    aput-byte p2, v0, v1

    .line 145
    return-void
.end method

.method public constructor <init>(SI)V
    .locals 1
    .param p1, "type"    # S
    .param p2, "value"    # I

    .line 165
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SILjava/nio/ByteOrder;)V

    .line 166
    return-void
.end method

.method public constructor <init>(SILjava/nio/ByteOrder;)V
    .locals 3
    .param p1, "type"    # S
    .param p2, "value"    # I
    .param p3, "order"    # Ljava/nio/ByteOrder;

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 169
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 170
    new-array v0, v0, [B

    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 171
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 172
    .local v0, "buf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 174
    .local v1, "originalOrder":Ljava/nio/ByteOrder;
    :try_start_0
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 175
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 178
    nop

    .line 179
    return-void

    .line 177
    :catchall_0
    move-exception v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 178
    throw v2
.end method

.method public constructor <init>(SLandroid/net/MacAddress;)V
    .locals 1
    .param p1, "type"    # S
    .param p2, "mac"    # Landroid/net/MacAddress;

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 192
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 193
    invoke-virtual {p2}, Landroid/net/MacAddress;->toByteArray()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 194
    return-void
.end method

.method public constructor <init>(SLjava/lang/String;)V
    .locals 3
    .param p1, "type"    # S
    .param p2, "string"    # Ljava/lang/String;

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 197
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 198
    const/4 v0, 0x0

    .line 200
    .local v0, "value":[B
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 202
    .local v1, "stringBytes":[B
    array-length v2, v1

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v2

    .end local v1    # "stringBytes":[B
    goto :goto_0

    .line 206
    :catchall_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 207
    throw v1

    .line 203
    :catch_0
    move-exception v1

    .line 206
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 207
    nop

    .line 208
    return-void
.end method

.method public constructor <init>(SLjava/net/InetAddress;)V
    .locals 1
    .param p1, "type"    # S
    .param p2, "ip"    # Ljava/net/InetAddress;

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 187
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 188
    invoke-virtual {p2}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 189
    return-void
.end method

.method public constructor <init>(SS)V
    .locals 1
    .param p1, "type"    # S
    .param p2, "value"    # S

    .line 148
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V

    .line 149
    return-void
.end method

.method public constructor <init>(SSLjava/nio/ByteOrder;)V
    .locals 3
    .param p1, "type"    # S
    .param p2, "value"    # S
    .param p3, "order"    # Ljava/nio/ByteOrder;

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 152
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 153
    const/4 v0, 0x2

    new-array v0, v0, [B

    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 154
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 155
    .local v0, "buf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 157
    .local v1, "originalOrder":Ljava/nio/ByteOrder;
    :try_start_0
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 158
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 161
    nop

    .line 162
    return-void

    .line 160
    :catchall_0
    move-exception v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 161
    throw v2
.end method

.method public constructor <init>(S[B)V
    .locals 1
    .param p1, "type"    # S
    .param p2, "value"    # [B

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x4

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 182
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 183
    invoke-direct {p0, p2}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 184
    return-void
.end method

.method public varargs constructor <init>(S[Lcom/android/net/module/util/netlink/StructNlAttr;)V
    .locals 6
    .param p1, "type"    # S
    .param p2, "nested"    # [Lcom/android/net/module/util/netlink/StructNlAttr;

    .line 211
    invoke-direct {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>()V

    .line 212
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->makeNestedType(S)S

    move-result v0

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 214
    const/4 v0, 0x0

    .line 215
    .local v0, "payloadLength":I
    array-length v1, p2

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p2, v3

    .local v4, "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v5

    add-int/2addr v0, v5

    .end local v4    # "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 216
    :cond_0
    new-array v1, v0, [B

    invoke-direct {p0, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V

    .line 218
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 219
    .local v1, "buf":Ljava/nio/ByteBuffer;
    array-length v3, p2

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, p2, v2

    .line 220
    .restart local v4    # "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v4, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 219
    .end local v4    # "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 222
    :cond_1
    return-void
.end method

.method public static findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;
    .locals 3
    .param p0, "attrType"    # S
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 119
    nop

    :goto_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_3

    .line 120
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->peek(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v0

    .line 121
    .local v0, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-nez v0, :cond_0

    .line 122
    goto :goto_1

    .line 124
    :cond_0
    iget-short v1, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    if-ne v1, p0, :cond_1

    .line 125
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v1

    return-object v1

    .line 127
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 128
    goto :goto_1

    .line 130
    :cond_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 131
    .end local v0    # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    goto :goto_0

    .line 132
    :cond_3
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public static makeNestedType(S)S
    .locals 1
    .param p0, "type"    # S

    .line 47
    const v0, 0x8000

    or-int/2addr v0, p0

    int-to-short v0, v0

    return v0
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;
    .locals 5
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 90
    invoke-static {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->peek(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v0

    .line 91
    .local v0, "struct":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v2

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 96
    .local v1, "baseOffset":I
    add-int/lit8 v2, v1, 0x4

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 98
    iget-short v2, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    const v3, 0xffff

    and-int/2addr v2, v3

    .line 99
    .local v2, "valueLen":I
    add-int/lit8 v2, v2, -0x4

    .line 100
    if-lez v2, :cond_1

    .line 101
    new-array v3, v2, [B

    iput-object v3, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    .line 102
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 103
    invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 105
    :cond_1
    return-object v0

    .line 92
    .end local v1    # "baseOffset":I
    .end local v2    # "valueLen":I
    :cond_2
    :goto_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public static peek(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;
    .locals 6
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 59
    const/4 v0, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 64
    .local v1, "baseOffset":I
    new-instance v3, Lcom/android/net/module/util/netlink/StructNlAttr;

    invoke-direct {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>()V

    .line 65
    .local v3, "struct":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v4

    .line 66
    .local v4, "originalOrder":Ljava/nio/ByteOrder;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 68
    :try_start_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    iput-short v5, v3, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 69
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    iput-short v5, v3, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 72
    nop

    .line 74
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 75
    iget-short v5, v3, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    if-ge v5, v2, :cond_1

    .line 77
    return-object v0

    .line 79
    :cond_1
    return-object v3

    .line 71
    :catchall_0
    move-exception v0

    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 72
    throw v0

    .line 60
    .end local v1    # "baseOffset":I
    .end local v3    # "struct":Lcom/android/net/module/util/netlink/StructNlAttr;
    .end local v4    # "originalOrder":Ljava/nio/ByteOrder;
    :cond_2
    :goto_0
    return-object v0
.end method

.method private setValue([B)V
    .locals 1
    .param p1, "value"    # [B

    .line 382
    iput-object p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    .line 383
    if-eqz p1, :cond_0

    array-length v0, p1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x4

    int-to-short v0, v0

    iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 384
    return-void
.end method


# virtual methods
.method public getAlignedLength()I
    .locals 1

    .line 228
    iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    invoke-static {v0}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(S)I

    move-result v0

    return v0
.end method

.method public getValueAsBe16(S)S
    .locals 3
    .param p1, "defaultValue"    # S

    .line 235
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 236
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 239
    :cond_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 241
    .local v1, "originalOrder":Ljava/nio/ByteOrder;
    :try_start_0
    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 242
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 242
    return v2

    .line 244
    :catchall_0
    move-exception v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 245
    throw v2

    .line 237
    .end local v1    # "originalOrder":Ljava/nio/ByteOrder;
    :cond_1
    :goto_0
    return p1
.end method

.method public getValueAsBe32(I)I
    .locals 3
    .param p1, "defaultValue"    # I

    .line 252
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 253
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 256
    :cond_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 258
    .local v1, "originalOrder":Ljava/nio/ByteOrder;
    :try_start_0
    sget-object v2, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 259
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 259
    return v2

    .line 261
    :catchall_0
    move-exception v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 262
    throw v2

    .line 254
    .end local v1    # "originalOrder":Ljava/nio/ByteOrder;
    :cond_1
    :goto_0
    return p1
.end method

.method public getValueAsByte(B)B
    .locals 3
    .param p1, "defaultValue"    # B

    .line 282
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 283
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    return v1

    .line 284
    :cond_1
    :goto_0
    return p1
.end method

.method public getValueAsByteBuffer()Ljava/nio/ByteBuffer;
    .locals 2

    .line 269
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 270
    :cond_0
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 274
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 275
    return-object v0
.end method

.method public getValueAsInetAddress()Ljava/net/InetAddress;
    .locals 2

    .line 316
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 319
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "ignored":Ljava/net/UnknownHostException;
    return-object v1
.end method

.method public getValueAsInt(I)I
    .locals 2
    .param p1, "defaultValue"    # I

    .line 304
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInteger()Ljava/lang/Integer;

    move-result-object v0

    .line 305
    .local v0, "value":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, p1

    :goto_0
    return v1
.end method

.method public getValueAsInteger()Ljava/lang/Integer;
    .locals 3

    .line 293
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 294
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 297
    :cond_0
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    .line 295
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getValueAsMacAddress()Landroid/net/MacAddress;
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 337
    :cond_0
    :try_start_0
    invoke-static {v0}, Landroid/net/MacAddress;->fromBytes([B)Landroid/net/MacAddress;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "ignored":Ljava/lang/IllegalArgumentException;
    return-object v1
.end method

.method public getValueAsString()Ljava/lang/String;
    .locals 5

    .line 350
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 353
    :cond_0
    array-length v2, v0

    iget-short v3, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    add-int/lit8 v4, v3, -0x4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_1

    return-object v1

    .line 356
    :cond_1
    add-int/lit8 v3, v3, -0x4

    add-int/lit8 v3, v3, -0x1

    :try_start_0
    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 357
    .local v0, "array":[B
    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 358
    .end local v0    # "array":[B
    :catch_0
    move-exception v0

    .line 359
    .local v0, "ignored":Ljava/lang/Exception;
    return-object v1
.end method

.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 367
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v0

    .line 368
    .local v0, "originalOrder":Ljava/nio/ByteOrder;
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 370
    .local v1, "originalPosition":I
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 372
    :try_start_0
    iget-short v2, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 373
    iget-short v2, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 374
    iget-object v2, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    if-eqz v2, :cond_0

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    :cond_0
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 377
    nop

    .line 378
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 379
    return-void

    .line 376
    :catchall_0
    move-exception v2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 377
    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StructNlAttr{ nla_len{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, nla_type{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, nla_value{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    .line 391
    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkConstants;->hexify([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 388
    return-object v0
.end method
