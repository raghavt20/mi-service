.class public Lcom/android/net/module/util/netlink/NdOption;
.super Ljava/lang/Object;
.source "NdOption.java"


# static fields
.field public static final STRUCT_SIZE:I = 0x2

.field public static final UNKNOWN:Lcom/android/net/module/util/netlink/NdOption;


# instance fields
.field public final length:I

.field public final type:B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 87
    new-instance v0, Lcom/android/net/module/util/netlink/NdOption;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/android/net/module/util/netlink/NdOption;-><init>(BI)V

    sput-object v0, Lcom/android/net/module/util/netlink/NdOption;->UNKNOWN:Lcom/android/net/module/util/netlink/NdOption;

    return-void
.end method

.method constructor <init>(BI)V
    .locals 0
    .param p1, "type"    # B
    .param p2, "length"    # I

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-byte p1, p0, Lcom/android/net/module/util/netlink/NdOption;->type:B

    .line 37
    iput p2, p0, Lcom/android/net/module/util/netlink/NdOption;->length:I

    .line 38
    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NdOption;
    .locals 5
    .param p0, "buf"    # Ljava/nio/ByteBuffer;

    .line 56
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    return-object v2

    .line 59
    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    .line 60
    .local v0, "type":B
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    invoke-static {v1}, Ljava/lang/Byte;->toUnsignedInt(B)I

    move-result v1

    .line 61
    .local v1, "length":I
    if-nez v1, :cond_1

    return-object v2

    .line 63
    :cond_1
    sparse-switch v0, :sswitch_data_0

    .line 71
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    mul-int/lit8 v4, v1, 0x8

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 72
    .local v2, "newPosition":I
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 73
    sget-object v3, Lcom/android/net/module/util/netlink/NdOption;->UNKNOWN:Lcom/android/net/module/util/netlink/NdOption;

    return-object v3

    .line 65
    .end local v2    # "newPosition":I
    :sswitch_0
    invoke-static {p0}, Lcom/android/net/module/util/netlink/StructNdOptPref64;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNdOptPref64;

    move-result-object v2

    return-object v2

    .line 68
    :sswitch_1
    invoke-static {p0}, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNdOptRdnss;

    move-result-object v2

    return-object v2

    :sswitch_data_0
    .sparse-switch
        0x19 -> :sswitch_1
        0x26 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 84
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/NdOption;->type:B

    invoke-static {v0}, Ljava/lang/Byte;->toUnsignedInt(B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/NdOption;->length:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "NdOption(%d, %d)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeToByteBuffer(Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .line 78
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/NdOption;->type:B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 79
    iget v0, p0, Lcom/android/net/module/util/netlink/NdOption;->length:I

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 80
    return-void
.end method
