.class public Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "RtNetlinkLinkMessage.java"


# static fields
.field public static final IFLA_ADDRESS:S = 0x1s

.field public static final IFLA_IFNAME:S = 0x3s

.field public static final IFLA_MTU:S = 0x4s


# instance fields
.field private mHardwareAddress:Landroid/net/MacAddress;

.field private mIfinfomsg:Lcom/android/net/module/util/netlink/StructIfinfoMsg;

.field private mInterfaceName:Ljava/lang/String;

.field private mMtu:I


# direct methods
.method private constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V
    .locals 2
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 54
    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mIfinfomsg:Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    .line 56
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I

    .line 57
    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mHardwareAddress:Landroid/net/MacAddress;

    .line 58
    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mInterfaceName:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;
    .locals 4
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 90
    new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;

    invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 92
    .local v0, "linkMsg":Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    move-result-object v1

    iput-object v1, v0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mIfinfomsg:Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    .line 93
    if-nez v1, :cond_0

    const/4 v1, 0x0

    return-object v1

    .line 96
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 97
    .local v1, "baseOffset":I
    const/4 v2, 0x4

    invoke-static {v2, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 98
    .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-eqz v2, :cond_1

    .line 99
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInt(I)I

    move-result v3

    iput v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I

    .line 103
    :cond_1
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 104
    const/4 v3, 0x1

    invoke-static {v3, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 105
    if-eqz v2, :cond_2

    .line 106
    invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsMacAddress()Landroid/net/MacAddress;

    move-result-object v3

    iput-object v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mHardwareAddress:Landroid/net/MacAddress;

    .line 110
    :cond_2
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 111
    const/4 v3, 0x3

    invoke-static {v3, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 112
    if-eqz v2, :cond_3

    .line 113
    invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mInterfaceName:Ljava/lang/String;

    .line 116
    :cond_3
    return-object v0
.end method


# virtual methods
.method public getHardwareAddress()Landroid/net/MacAddress;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mHardwareAddress:Landroid/net/MacAddress;

    return-object v0
.end method

.method public getIfinfoHeader()Lcom/android/net/module/util/netlink/StructIfinfoMsg;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mIfinfomsg:Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    return-object v0
.end method

.method public getInterfaceName()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mInterfaceName:Ljava/lang/String;

    return-object v0
.end method

.method public getMtu()I
    .locals 1

    .line 62
    iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I

    return v0
.end method

.method protected pack(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 124
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 125
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mIfinfomsg:Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 127
    iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I

    if-eqz v0, :cond_0

    .line 128
    new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v2, 0x4

    invoke-direct {v1, v2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SI)V

    move-object v0, v1

    .line 129
    .local v0, "mtu":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 131
    .end local v0    # "mtu":Lcom/android/net/module/util/netlink/StructNlAttr;
    :cond_0
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mHardwareAddress:Landroid/net/MacAddress;

    if-eqz v0, :cond_1

    .line 132
    new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v2, 0x1

    invoke-direct {v1, v2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLandroid/net/MacAddress;)V

    move-object v0, v1

    .line 133
    .local v0, "hardwareAddress":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 135
    .end local v0    # "hardwareAddress":Lcom/android/net/module/util/netlink/StructNlAttr;
    :cond_1
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mInterfaceName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 136
    new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v2, 0x3

    invoke-direct {v1, v2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/lang/String;)V

    move-object v0, v1

    .line 137
    .local v0, "ifname":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 139
    .end local v0    # "ifname":Lcom/android/net/module/util/netlink/StructNlAttr;
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtNetlinkLinkMessage{ nlmsghdr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    sget v2, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    .line 144
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, Ifinfomsg{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mIfinfomsg:Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    .line 145
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, Hardware Address{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mHardwareAddress:Landroid/net/MacAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, MTU{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, Ifname{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mInterfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "} }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 143
    return-object v0
.end method
