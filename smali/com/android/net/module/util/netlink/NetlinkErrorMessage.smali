.class public Lcom/android/net/module/util/netlink/NetlinkErrorMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "NetlinkErrorMessage.java"


# instance fields
.field private mNlMsgErr:Lcom/android/net/module/util/netlink/StructNlMsgErr;


# direct methods
.method constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V
    .locals 1
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 54
    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->mNlMsgErr:Lcom/android/net/module/util/netlink/StructNlMsgErr;

    .line 56
    return-void
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkErrorMessage;
    .locals 2
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 41
    new-instance v0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;

    invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 43
    .local v0, "errorMsg":Lcom/android/net/module/util/netlink/NetlinkErrorMessage;
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructNlMsgErr;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlMsgErr;

    move-result-object v1

    iput-object v1, v0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->mNlMsgErr:Lcom/android/net/module/util/netlink/StructNlMsgErr;

    .line 44
    if-nez v1, :cond_0

    .line 45
    const/4 v1, 0x0

    return-object v1

    .line 48
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getNlMsgError()Lcom/android/net/module/util/netlink/StructNlMsgErr;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->mNlMsgErr:Lcom/android/net/module/util/netlink/StructNlMsgErr;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NetlinkErrorMessage{ nlmsghdr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    const-string v2, ""

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, nlmsgerr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->mNlMsgErr:Lcom/android/net/module/util/netlink/StructNlMsgErr;

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlMsgErr;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "} }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    return-object v0
.end method
