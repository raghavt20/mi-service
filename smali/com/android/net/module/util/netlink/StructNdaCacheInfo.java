public class com.android.net.module.util.netlink.StructNdaCacheInfo {
	 /* .source "StructNdaCacheInfo.java" */
	 /* # static fields */
	 private static final Long CLOCK_TICKS_PER_SECOND;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Integer ndm_confirmed;
	 public Integer ndm_refcnt;
	 public Integer ndm_updated;
	 public Integer ndm_used;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.StructNdaCacheInfo ( ) {
		 /* .locals 2 */
		 /* .line 62 */
		 android.system.Os .sysconf ( v0 );
		 /* move-result-wide v0 */
		 /* sput-wide v0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->CLOCK_TICKS_PER_SECOND:J */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNdaCacheInfo ( ) {
		 /* .locals 0 */
		 /* .line 100 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 36 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v1, 0x10 */
			 /* if-lt v0, v1, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public static com.android.net.module.util.netlink.StructNdaCacheInfo parse ( java.nio.ByteBuffer p0 ) {
	 /* .locals 2 */
	 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 47 */
	 v0 = 	 com.android.net.module.util.netlink.StructNdaCacheInfo .hasAvailableSpace ( p0 );
	 /* if-nez v0, :cond_0 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 52 */
} // :cond_0
/* new-instance v0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo; */
/* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;-><init>()V */
/* .line 53 */
/* .local v0, "struct":Lcom/android/net/module/util/netlink/StructNdaCacheInfo; */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_used:I */
/* .line 54 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_confirmed:I */
/* .line 55 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_updated:I */
/* .line 56 */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_refcnt:I */
/* .line 57 */
} // .end method
private static Long ticksToMilliSeconds ( Integer p0 ) {
/* .locals 6 */
/* .param p0, "intClockTicks" # I */
/* .line 65 */
/* int-to-long v0, p0 */
/* const-wide/16 v2, -0x1 */
/* and-long/2addr v0, v2 */
/* .line 66 */
/* .local v0, "longClockTicks":J */
/* const-wide/16 v2, 0x3e8 */
/* mul-long/2addr v2, v0 */
/* sget-wide v4, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->CLOCK_TICKS_PER_SECOND:J */
/* div-long/2addr v2, v4 */
/* return-wide v2 */
} // .end method
/* # virtual methods */
public Long lastConfirmed ( ) {
/* .locals 2 */
/* .line 114 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_confirmed:I */
com.android.net.module.util.netlink.StructNdaCacheInfo .ticksToMilliSeconds ( v0 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long lastUpdated ( ) {
/* .locals 2 */
/* .line 121 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_updated:I */
com.android.net.module.util.netlink.StructNdaCacheInfo .ticksToMilliSeconds ( v0 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long lastUsed ( ) {
/* .locals 2 */
/* .line 106 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_used:I */
com.android.net.module.util.netlink.StructNdaCacheInfo .ticksToMilliSeconds ( v0 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 126 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "NdaCacheInfo{ ndm_used{"; // const-string v1, "NdaCacheInfo{ ndm_used{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 127 */
(( com.android.net.module.util.netlink.StructNdaCacheInfo ) p0 ).lastUsed ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->lastUsed()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, ndm_confirmed{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 128 */
(( com.android.net.module.util.netlink.StructNdaCacheInfo ) p0 ).lastConfirmed ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->lastConfirmed()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, ndm_updated{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 129 */
(( com.android.net.module.util.netlink.StructNdaCacheInfo ) p0 ).lastUpdated ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->lastUpdated()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, ndm_refcnt{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->ndm_refcnt:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 126 */
} // .end method
