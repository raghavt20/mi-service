public class com.android.net.module.util.netlink.RtNetlinkRouteMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "RtNetlinkRouteMessage.java" */
	 /* # static fields */
	 public static final Object RTA_DST;
	 public static final Object RTA_GATEWAY;
	 public static final Object RTA_OIF;
	 /* # instance fields */
	 private android.net.IpPrefix mDestination;
	 private java.net.InetAddress mGateway;
	 private Integer mIfindex;
	 private com.android.net.module.util.netlink.StructRtMsg mRtmsg;
	 /* # direct methods */
	 private com.android.net.module.util.netlink.RtNetlinkRouteMessage ( ) {
		 /* .locals 1 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .line 64 */
		 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 65 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mRtmsg = v0;
		 /* .line 66 */
		 this.mDestination = v0;
		 /* .line 67 */
		 this.mGateway = v0;
		 /* .line 68 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I */
		 /* .line 69 */
		 return;
	 } // .end method
	 private static Boolean matchRouteAddressFamily ( java.net.InetAddress p0, Integer p1 ) {
		 /* .locals 1 */
		 /* .param p0, "address" # Ljava/net/InetAddress; */
		 /* .param p1, "family" # I */
		 /* .line 100 */
		 /* instance-of v0, p0, Ljava/net/Inet4Address; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* if-eq p1, v0, :cond_1 */
		 } // :cond_0
		 /* instance-of v0, p0, Ljava/net/Inet6Address; */
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* if-ne p1, v0, :cond_2 */
		 } // :cond_1
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_2
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static com.android.net.module.util.netlink.RtNetlinkRouteMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 9 */
/* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 115 */
/* new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage; */
/* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
/* .line 117 */
/* .local v0, "routeMsg":Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage; */
com.android.net.module.util.netlink.StructRtMsg .parse ( p1 );
this.mRtmsg = v1;
/* .line 118 */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 119 */
} // :cond_0
/* iget-short v1, v1, Lcom/android/net/module/util/netlink/StructRtMsg;->family:S */
/* .line 122 */
/* .local v1, "rtmFamily":I */
v3 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* .line 123 */
/* .local v3, "baseOffset":I */
int v4 = 1; // const/4 v4, 0x1
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v4,p1 );
/* .line 124 */
/* .local v4, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
int v5 = 0; // const/4 v5, 0x0
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 125 */
(( com.android.net.module.util.netlink.StructNlAttr ) v4 ).getValueAsInetAddress ( ); // invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
/* .line 127 */
/* .local v6, "destination":Ljava/net/InetAddress; */
/* if-nez v6, :cond_1 */
/* .line 129 */
} // :cond_1
v7 = com.android.net.module.util.netlink.RtNetlinkRouteMessage .matchRouteAddressFamily ( v6,v1 );
/* if-nez v7, :cond_2 */
/* .line 130 */
} // :cond_2
/* new-instance v7, Landroid/net/IpPrefix; */
v8 = this.mRtmsg;
/* iget-short v8, v8, Lcom/android/net/module/util/netlink/StructRtMsg;->dstLen:S */
/* invoke-direct {v7, v6, v8}, Landroid/net/IpPrefix;-><init>(Ljava/net/InetAddress;I)V */
this.mDestination = v7;
/* .line 131 */
} // .end local v6 # "destination":Ljava/net/InetAddress;
} // :cond_3
/* if-ne v1, v6, :cond_4 */
/* .line 132 */
/* new-instance v6, Landroid/net/IpPrefix; */
v7 = com.android.net.module.util.NetworkStackConstants.IPV4_ADDR_ANY;
/* invoke-direct {v6, v7, v5}, Landroid/net/IpPrefix;-><init>(Ljava/net/InetAddress;I)V */
this.mDestination = v6;
/* .line 133 */
} // :cond_4
/* if-ne v1, v6, :cond_8 */
/* .line 134 */
/* new-instance v6, Landroid/net/IpPrefix; */
v7 = com.android.net.module.util.NetworkStackConstants.IPV6_ADDR_ANY;
/* invoke-direct {v6, v7, v5}, Landroid/net/IpPrefix;-><init>(Ljava/net/InetAddress;I)V */
this.mDestination = v6;
/* .line 140 */
} // :goto_0
(( java.nio.ByteBuffer ) p1 ).position ( v3 ); // invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 141 */
int v6 = 5; // const/4 v6, 0x5
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v6,p1 );
/* .line 142 */
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 143 */
(( com.android.net.module.util.netlink.StructNlAttr ) v4 ).getValueAsInetAddress ( ); // invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
this.mGateway = v6;
/* .line 145 */
/* if-nez v6, :cond_5 */
/* .line 147 */
} // :cond_5
v6 = com.android.net.module.util.netlink.RtNetlinkRouteMessage .matchRouteAddressFamily ( v6,v1 );
/* if-nez v6, :cond_6 */
/* .line 151 */
} // :cond_6
(( java.nio.ByteBuffer ) p1 ).position ( v3 ); // invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 152 */
int v2 = 4; // const/4 v2, 0x4
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v2,p1 );
/* .line 153 */
} // .end local v4 # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
/* .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 158 */
v4 = (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsInt ( v5 ); // invoke-virtual {v2, v5}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInt(I)I
/* iput v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I */
/* .line 161 */
} // :cond_7
/* .line 136 */
} // .end local v2 # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
/* .restart local v4 # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
} // :cond_8
} // .end method
/* # virtual methods */
public android.net.IpPrefix getDestination ( ) {
/* .locals 1 */
/* .line 82 */
v0 = this.mDestination;
} // .end method
public java.net.InetAddress getGateway ( ) {
/* .locals 1 */
/* .line 87 */
v0 = this.mGateway;
} // .end method
public Integer getInterfaceIndex ( ) {
/* .locals 1 */
/* .line 72 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I */
} // .end method
public com.android.net.module.util.netlink.StructRtMsg getRtMsgHeader ( ) {
/* .locals 1 */
/* .line 77 */
v0 = this.mRtmsg;
} // .end method
protected void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 4 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 169 */
(( com.android.net.module.util.netlink.RtNetlinkRouteMessage ) p0 ).getHeader ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 170 */
v0 = this.mRtmsg;
(( com.android.net.module.util.netlink.StructRtMsg ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructRtMsg;->pack(Ljava/nio/ByteBuffer;)V
/* .line 172 */
/* new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr; */
v1 = this.mDestination;
(( android.net.IpPrefix ) v1 ).getAddress ( ); // invoke-virtual {v1}, Landroid/net/IpPrefix;->getAddress()Ljava/net/InetAddress;
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v0, v2, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V */
/* .line 173 */
/* .local v0, "destination":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 175 */
v1 = this.mGateway;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 176 */
/* new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr; */
int v3 = 5; // const/4 v3, 0x5
(( java.net.InetAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B
/* invoke-direct {v2, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[B)V */
/* move-object v1, v2 */
/* .line 177 */
/* .local v1, "gateway":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v1 ).pack ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 179 */
} // .end local v1 # "gateway":Lcom/android/net/module/util/netlink/StructNlAttr;
} // :cond_0
/* iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 180 */
/* new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr; */
int v3 = 4; // const/4 v3, 0x4
/* invoke-direct {v2, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SI)V */
/* move-object v1, v2 */
/* .line 181 */
/* .local v1, "ifindex":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v1 ).pack ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 183 */
} // .end local v1 # "ifindex":Lcom/android/net/module/util/netlink/StructNlAttr;
} // :cond_1
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 187 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "RtNetlinkRouteMessage{ nlmsghdr{"; // const-string v1, "RtNetlinkRouteMessage{ nlmsghdr{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHeader;
/* .line 188 */
java.lang.Integer .valueOf ( v2 );
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, Rtmsg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mRtmsg;
/* .line 189 */
(( com.android.net.module.util.netlink.StructRtMsg ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructRtMsg;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, destination{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mDestination;
/* .line 190 */
(( android.net.IpPrefix ) v1 ).getAddress ( ); // invoke-virtual {v1}, Landroid/net/IpPrefix;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v1 ).getHostAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, gateway{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 191 */
v1 = this.mGateway;
/* if-nez v1, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
(( java.net.InetAddress ) v1 ).getHostAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, ifindex{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 187 */
} // .end method
