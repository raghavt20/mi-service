public class com.android.net.module.util.netlink.NduseroptMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "NduseroptMessage.java" */
	 /* # static fields */
	 static final Integer NDUSEROPT_SRCADDR;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public final Object family;
	 public final Object icmp_code;
	 public final Object icmp_type;
	 public final Integer ifindex;
	 public final com.android.net.module.util.netlink.NdOption option;
	 public final Integer opts_len;
	 public final java.net.InetAddress srcaddr;
	 /* # direct methods */
	 com.android.net.module.util.netlink.NduseroptMessage ( ) {
		 /* .locals 7 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .param p2, "buf" # Ljava/nio/ByteBuffer; */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/net/UnknownHostException; */
		 /* } */
	 } // .end annotation
	 /* .line 68 */
	 final String v0 = "ND option extends past end of buffer"; // const-string v0, "ND option extends past end of buffer"
	 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
	 /* .line 71 */
	 java.nio.ByteOrder .nativeOrder ( );
	 (( java.nio.ByteBuffer ) p2 ).order ( v1 ); // invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
	 /* .line 72 */
	 v1 = 	 (( java.nio.ByteBuffer ) p2 ).position ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I
	 /* .line 73 */
	 /* .local v1, "start":I */
	 v2 = 	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* iput-byte v2, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->family:B */
	 /* .line 74 */
	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* .line 75 */
	 v3 = 	 (( java.nio.ByteBuffer ) p2 ).getShort ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getShort()S
	 v3 = 	 java.lang.Short .toUnsignedInt ( v3 );
	 /* iput v3, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->opts_len:I */
	 /* .line 76 */
	 v4 = 	 (( java.nio.ByteBuffer ) p2 ).getInt ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getInt()I
	 /* iput v4, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->ifindex:I */
	 /* .line 77 */
	 v5 = 	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* iput-byte v5, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_type:B */
	 /* .line 78 */
	 v5 = 	 (( java.nio.ByteBuffer ) p2 ).get ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B
	 /* iput-byte v5, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_code:B */
	 /* .line 79 */
	 v5 = 	 (( java.nio.ByteBuffer ) p2 ).position ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I
	 /* add-int/lit8 v5, v5, 0x6 */
	 (( java.nio.ByteBuffer ) p2 ).position ( v5 ); // invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
	 /* .line 89 */
	 v5 = java.nio.ByteOrder.BIG_ENDIAN;
	 (( java.nio.ByteBuffer ) p2 ).order ( v5 ); // invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
	 /* .line 91 */
	 try { // :try_start_0
		 (( java.nio.ByteBuffer ) p2 ).slice ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;
		 /* .line 92 */
		 /* .local v5, "slice":Ljava/nio/ByteBuffer; */
		 (( java.nio.ByteBuffer ) v5 ).limit ( v3 ); // invoke-virtual {v5, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;
		 /* .line 93 */
		 com.android.net.module.util.netlink.NdOption .parse ( v5 );
		 this.option = v6;
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* .line 97 */
	 } // .end local v5 # "slice":Ljava/nio/ByteBuffer;
	 /* add-int/lit8 v5, v1, 0x10 */
	 /* add-int/2addr v5, v3 */
	 /* .line 98 */
	 /* .local v5, "newPosition":I */
	 v3 = 	 (( java.nio.ByteBuffer ) p2 ).limit ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I
	 /* if-ge v5, v3, :cond_2 */
	 /* .line 101 */
	 (( java.nio.ByteBuffer ) p2 ).position ( v5 ); // invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
	 /* .line 102 */
} // .end local v5 # "newPosition":I
/* nop */
/* .line 105 */
com.android.net.module.util.netlink.StructNlAttr .parse ( p2 );
/* .line 106 */
/* .local v0, "nla":Lcom/android/net/module/util/netlink/StructNlAttr; */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* iget-short v3, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
	 int v5 = 1; // const/4 v5, 0x1
	 /* if-ne v3, v5, :cond_1 */
	 v3 = this.nla_value;
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 109 */
		 /* if-ne v2, v3, :cond_0 */
		 /* .line 111 */
		 int v2 = 0; // const/4 v2, 0x0
		 v3 = this.nla_value;
		 java.net.Inet6Address .getByAddress ( v2,v3,v4 );
		 this.srcaddr = v2;
		 /* .line 113 */
	 } // :cond_0
	 v2 = this.nla_value;
	 java.net.InetAddress .getByAddress ( v2 );
	 this.srcaddr = v2;
	 /* .line 115 */
} // :goto_0
return;
/* .line 107 */
} // :cond_1
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
final String v3 = "Invalid source address in ND useropt"; // const-string v3, "Invalid source address in ND useropt"
/* invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 99 */
} // .end local v0 # "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
/* .restart local v5 # "newPosition":I */
} // :cond_2
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
/* invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 97 */
} // .end local v5 # "newPosition":I
/* :catchall_0 */
/* move-exception v2 */
/* add-int/lit8 v3, v1, 0x10 */
/* iget v4, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->opts_len:I */
/* add-int/2addr v3, v4 */
/* .line 98 */
/* .local v3, "newPosition":I */
v4 = (( java.nio.ByteBuffer ) p2 ).limit ( ); // invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I
/* if-lt v3, v4, :cond_3 */
/* .line 99 */
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
/* invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 101 */
} // :cond_3
(( java.nio.ByteBuffer ) p2 ).position ( v3 ); // invoke-virtual {p2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 102 */
} // .end local v3 # "newPosition":I
/* throw v2 */
} // .end method
public static com.android.net.module.util.netlink.NduseroptMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 3 */
/* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "buf" # Ljava/nio/ByteBuffer; */
/* .line 128 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
v1 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v2, 0x10 */
/* if-ge v1, v2, :cond_0 */
/* .line 129 */
} // :cond_0
(( java.nio.ByteBuffer ) p1 ).order ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 131 */
/* .local v1, "oldOrder":Ljava/nio/ByteOrder; */
try { // :try_start_0
/* new-instance v2, Lcom/android/net/module/util/netlink/NduseroptMessage; */
/* invoke-direct {v2, p0, p1}, Lcom/android/net/module/util/netlink/NduseroptMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)V */
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 138 */
(( java.nio.ByteBuffer ) p1 ).order ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 131 */
/* .line 138 */
/* :catchall_0 */
/* move-exception v0 */
(( java.nio.ByteBuffer ) p1 ).order ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 139 */
/* throw v0 */
/* .line 132 */
/* :catch_0 */
/* move-exception v2 */
/* .line 136 */
/* .local v2, "e":Ljava/lang/Exception; */
/* nop */
/* .line 138 */
(( java.nio.ByteBuffer ) p1 ).order ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 136 */
/* .line 128 */
} // .end local v1 # "oldOrder":Ljava/nio/ByteOrder;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_1
} // :goto_0
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 7 */
/* .line 144 */
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->family:B */
/* .line 145 */
java.lang.Byte .valueOf ( v0 );
/* iget v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->opts_len:I */
java.lang.Integer .valueOf ( v0 );
/* iget v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->ifindex:I */
java.lang.Integer .valueOf ( v0 );
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_type:B */
v0 = java.lang.Byte .toUnsignedInt ( v0 );
java.lang.Integer .valueOf ( v0 );
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_code:B */
/* .line 146 */
v0 = java.lang.Byte .toUnsignedInt ( v0 );
java.lang.Integer .valueOf ( v0 );
v0 = this.srcaddr;
(( java.net.InetAddress ) v0 ).getHostAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* filled-new-array/range {v1 ..v6}, [Ljava/lang/Object; */
/* .line 144 */
final String v1 = "Nduseroptmsg(%d, %d, %d, %d, %d, %s)"; // const-string v1, "Nduseroptmsg(%d, %d, %d, %d, %d, %s)"
java.lang.String .format ( v1,v0 );
} // .end method
