public class com.android.net.module.util.netlink.NdOption {
	 /* .source "NdOption.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 public static final com.android.net.module.util.netlink.NdOption UNKNOWN;
	 /* # instance fields */
	 public final Integer length;
	 public final Object type;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.NdOption ( ) {
		 /* .locals 2 */
		 /* .line 87 */
		 /* new-instance v0, Lcom/android/net/module/util/netlink/NdOption; */
		 int v1 = 0; // const/4 v1, 0x0
		 /* invoke-direct {v0, v1, v1}, Lcom/android/net/module/util/netlink/NdOption;-><init>(BI)V */
		 return;
	 } // .end method
	 com.android.net.module.util.netlink.NdOption ( ) {
		 /* .locals 0 */
		 /* .param p1, "type" # B */
		 /* .param p2, "length" # I */
		 /* .line 35 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 36 */
		 /* iput-byte p1, p0, Lcom/android/net/module/util/netlink/NdOption;->type:B */
		 /* .line 37 */
		 /* iput p2, p0, Lcom/android/net/module/util/netlink/NdOption;->length:I */
		 /* .line 38 */
		 return;
	 } // .end method
	 public static com.android.net.module.util.netlink.NdOption parse ( java.nio.ByteBuffer p0 ) {
		 /* .locals 5 */
		 /* .param p0, "buf" # Ljava/nio/ByteBuffer; */
		 /* .line 56 */
		 v0 = 		 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
		 int v1 = 2; // const/4 v1, 0x2
		 int v2 = 0; // const/4 v2, 0x0
		 /* if-ge v0, v1, :cond_0 */
		 /* .line 59 */
	 } // :cond_0
	 v0 = 	 (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
	 v0 = 	 (( java.nio.ByteBuffer ) p0 ).get ( v0 ); // invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get(I)B
	 /* .line 60 */
	 /* .local v0, "type":B */
	 v1 = 	 (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
	 /* add-int/lit8 v1, v1, 0x1 */
	 v1 = 	 (( java.nio.ByteBuffer ) p0 ).get ( v1 ); // invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->get(I)B
	 v1 = 	 java.lang.Byte .toUnsignedInt ( v1 );
	 /* .line 61 */
	 /* .local v1, "length":I */
	 /* if-nez v1, :cond_1 */
	 /* .line 63 */
} // :cond_1
/* sparse-switch v0, :sswitch_data_0 */
/* .line 71 */
v2 = (( java.nio.ByteBuffer ) p0 ).limit ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I
v3 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* mul-int/lit8 v4, v1, 0x8 */
/* add-int/2addr v3, v4 */
v2 = java.lang.Math .min ( v2,v3 );
/* .line 72 */
/* .local v2, "newPosition":I */
(( java.nio.ByteBuffer ) p0 ).position ( v2 ); // invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 73 */
v3 = com.android.net.module.util.netlink.NdOption.UNKNOWN;
/* .line 65 */
} // .end local v2 # "newPosition":I
/* :sswitch_0 */
com.android.net.module.util.netlink.StructNdOptPref64 .parse ( p0 );
/* .line 68 */
/* :sswitch_1 */
com.android.net.module.util.netlink.StructNdOptRdnss .parse ( p0 );
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x19 -> :sswitch_1 */
/* 0x26 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 84 */
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/NdOption;->type:B */
v0 = java.lang.Byte .toUnsignedInt ( v0 );
java.lang.Integer .valueOf ( v0 );
/* iget v1, p0, Lcom/android/net/module/util/netlink/NdOption;->length:I */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
final String v1 = "NdOption(%d, %d)"; // const-string v1, "NdOption(%d, %d)"
java.lang.String .format ( v1,v0 );
} // .end method
void writeToByteBuffer ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "buf" # Ljava/nio/ByteBuffer; */
/* .line 78 */
/* iget-byte v0, p0, Lcom/android/net/module/util/netlink/NdOption;->type:B */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 79 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/NdOption;->length:I */
/* int-to-byte v0, v0 */
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
/* .line 80 */
return;
} // .end method
