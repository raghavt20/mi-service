.class public Lcom/android/net/module/util/netlink/StructIfinfoMsg;
.super Lcom/android/net/module/util/Struct;
.source "StructIfinfoMsg.java"


# static fields
.field public static final STRUCT_SIZE:I = 0x10


# instance fields
.field public final change:J
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x4
        type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final family:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x0
        padding = 0x1
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final flags:J
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x3
        type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final index:I
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x2
        type = .enum Lcom/android/net/module/util/Struct$Type;->S32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final type:I
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x1
        type = .enum Lcom/android/net/module/util/Struct$Type;->U16:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field


# direct methods
.method constructor <init>(SIIJJ)V
    .locals 0
    .param p1, "family"    # S
    .param p2, "type"    # I
    .param p3, "index"    # I
    .param p4, "flags"    # J
    .param p6, "change"    # J

    .line 52
    invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V

    .line 53
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->family:S

    .line 54
    iput p2, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->type:I

    .line 55
    iput p3, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->index:I

    .line 56
    iput-wide p4, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->flags:J

    .line 57
    iput-wide p6, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->change:J

    .line 58
    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructIfinfoMsg;
    .locals 2
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 69
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 72
    :cond_0
    const-class v0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    invoke-static {v0, p0}, Lcom/android/net/module/util/Struct;->parse(Ljava/lang/Class;Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;

    return-object v0
.end method


# virtual methods
.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 80
    invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 81
    return-void
.end method
