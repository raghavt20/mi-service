public class com.android.net.module.util.netlink.ConntrackMessage$TupleProto {
	 /* .source "ConntrackMessage.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/net/module/util/netlink/ConntrackMessage; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "TupleProto" */
} // .end annotation
/* # instance fields */
public final Object dstPort;
public final Object protoNum;
public final Object srcPort;
/* # direct methods */
public com.android.net.module.util.netlink.ConntrackMessage$TupleProto ( ) {
/* .locals 0 */
/* .param p1, "protoNum" # B */
/* .param p2, "srcPort" # S */
/* .param p3, "dstPort" # S */
/* .line 177 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 178 */
/* iput-byte p1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;->protoNum:B */
/* .line 179 */
/* iput-short p2, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;->srcPort:S */
/* .line 180 */
/* iput-short p3, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;->dstPort:S */
/* .line 181 */
return;
} // .end method
