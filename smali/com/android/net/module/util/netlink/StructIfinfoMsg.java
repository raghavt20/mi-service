public class com.android.net.module.util.netlink.StructIfinfoMsg extends com.android.net.module.util.Struct {
	 /* .source "StructIfinfoMsg.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public final Long change;
	 /* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
	 /* order = 0x4 */
	 /* type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object family;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x0 */
/* padding = 0x1 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Long flags;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x3 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Integer index;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x2 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->S32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Integer type;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x1 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U16:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
/* # direct methods */
 com.android.net.module.util.netlink.StructIfinfoMsg ( ) {
/* .locals 0 */
/* .param p1, "family" # S */
/* .param p2, "type" # I */
/* .param p3, "index" # I */
/* .param p4, "flags" # J */
/* .param p6, "change" # J */
/* .line 52 */
/* invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V */
/* .line 53 */
/* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->family:S */
/* .line 54 */
/* iput p2, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->type:I */
/* .line 55 */
/* iput p3, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->index:I */
/* .line 56 */
/* iput-wide p4, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->flags:J */
/* .line 57 */
/* iput-wide p6, p0, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->change:J */
/* .line 58 */
return;
} // .end method
public static com.android.net.module.util.netlink.StructIfinfoMsg parse ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 69 */
v0 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v1, 0x10 */
/* if-ge v0, v1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 72 */
} // :cond_0
/* const-class v0, Lcom/android/net/module/util/netlink/StructIfinfoMsg; */
com.android.net.module.util.Struct .parse ( v0,p0 );
/* check-cast v0, Lcom/android/net/module/util/netlink/StructIfinfoMsg; */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 0 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 80 */
(( com.android.net.module.util.netlink.StructIfinfoMsg ) p0 ).writeToByteBuffer ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V
/* .line 81 */
return;
} // .end method
