public class com.android.net.module.util.netlink.NetlinkErrorMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "NetlinkErrorMessage.java" */
	 /* # instance fields */
	 private com.android.net.module.util.netlink.StructNlMsgErr mNlMsgErr;
	 /* # direct methods */
	 com.android.net.module.util.netlink.NetlinkErrorMessage ( ) {
		 /* .locals 1 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .line 54 */
		 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 55 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mNlMsgErr = v0;
		 /* .line 56 */
		 return;
	 } // .end method
	 public static com.android.net.module.util.netlink.NetlinkErrorMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
		 /* .locals 2 */
		 /* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 41 */
		 /* new-instance v0, Lcom/android/net/module/util/netlink/NetlinkErrorMessage; */
		 /* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 43 */
		 /* .local v0, "errorMsg":Lcom/android/net/module/util/netlink/NetlinkErrorMessage; */
		 com.android.net.module.util.netlink.StructNlMsgErr .parse ( p1 );
		 this.mNlMsgErr = v1;
		 /* .line 44 */
		 /* if-nez v1, :cond_0 */
		 /* .line 45 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 48 */
	 } // :cond_0
} // .end method
/* # virtual methods */
public com.android.net.module.util.netlink.StructNlMsgErr getNlMsgError ( ) {
	 /* .locals 1 */
	 /* .line 59 */
	 v0 = this.mNlMsgErr;
} // .end method
public java.lang.String toString ( ) {
	 /* .locals 3 */
	 /* .line 64 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "NetlinkErrorMessage{ nlmsghdr{"; // const-string v1, "NetlinkErrorMessage{ nlmsghdr{"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 65 */
	 v1 = this.mHeader;
	 final String v2 = ""; // const-string v2, ""
	 /* if-nez v1, :cond_0 */
	 /* move-object v1, v2 */
} // :cond_0
v1 = this.mHeader;
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nlmsgerr{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 66 */
v1 = this.mNlMsgErr;
/* if-nez v1, :cond_1 */
} // :cond_1
(( com.android.net.module.util.netlink.StructNlMsgErr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlMsgErr;->toString()Ljava/lang/String;
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 64 */
} // .end method
