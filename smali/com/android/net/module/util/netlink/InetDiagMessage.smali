.class public Lcom/android/net/module/util/netlink/InetDiagMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "InetDiagMessage.java"


# static fields
.field private static final FAMILY:[I

.field public static final TAG:Ljava/lang/String; = "InetDiagMessage"

.field private static final TIMEOUT_MS:I = 0x1f4


# instance fields
.field public inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 207
    sget v0, Landroid/system/OsConstants;->AF_INET6:I

    sget v1, Landroid/system/OsConstants;->AF_INET:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sput-object v0, Lcom/android/net/module/util/netlink/InetDiagMessage;->FAMILY:[I

    return-void
.end method

.method public constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V
    .locals 1
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 160
    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 161
    new-instance v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;-><init>()V

    iput-object v0, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    .line 162
    return-void
.end method

.method public static buildInetDiagReqForAliveTcpSockets(I)[B
    .locals 8
    .param p0, "family"    # I

    .line 284
    sget v0, Landroid/system/OsConstants;->IPPROTO_TCP:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v4, 0x301

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/16 v7, 0xe

    move v3, p0

    invoke-static/range {v0 .. v7}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISIII)[B

    move-result-object v0

    return-object v0
.end method

.method private static closeSocketQuietly(Ljava/io/FileDescriptor;)V
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .line 180
    :try_start_0
    invoke-static {p0}, Landroid/net/util/SocketUtils;->closeSocket(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 183
    :goto_0
    return-void
.end method

.method public static containsUid(Lcom/android/net/module/util/netlink/InetDiagMessage;Ljava/util/Set;)Z
    .locals 3
    .param p0, "msg"    # Lcom/android/net/module/util/netlink/InetDiagMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/net/module/util/netlink/InetDiagMessage;",
            "Ljava/util/Set<",
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;>;)Z"
        }
    .end annotation

    .line 386
    .local p1, "ranges":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Range<Ljava/lang/Integer;>;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Range;

    .line 387
    .local v1, "range":Landroid/util/Range;, "Landroid/util/Range<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget v2, v2, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/Range;->contains(Ljava/lang/Comparable;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 388
    const/4 v0, 0x1

    return v0

    .line 390
    .end local v1    # "range":Landroid/util/Range;, "Landroid/util/Range<Ljava/lang/Integer;>;"
    :cond_0
    goto :goto_0

    .line 391
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public static destroyLiveTcpSockets(Ljava/util/Set;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Landroid/util/Range<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;,
            Ljava/io/InterruptedIOException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 465
    .local p0, "ranges":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Range<Ljava/lang/Integer;>;>;"
    .local p1, "exemptUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 466
    .local v0, "startTimeMs":J
    sget v2, Landroid/system/OsConstants;->IPPROTO_TCP:I

    new-instance v3, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda0;

    invoke-direct {v3, p1, p0}, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda0;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    const/16 v4, 0xe

    invoke-static {v2, v4, v3}, Lcom/android/net/module/util/netlink/InetDiagMessage;->destroySockets(IILjava/util/function/Predicate;)V

    .line 471
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 472
    .local v2, "durationMs":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Destroyed live tcp sockets for uids="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " exemptUids="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "InetDiagMessage"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    return-void
.end method

.method public static destroyLiveTcpSocketsByOwnerUids(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;,
            Ljava/io/InterruptedIOException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 487
    .local p0, "ownerUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 488
    .local v0, "startTimeMs":J
    sget v2, Landroid/system/OsConstants;->IPPROTO_TCP:I

    new-instance v3, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda1;

    invoke-direct {v3, p0}, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda1;-><init>(Ljava/util/Set;)V

    const/16 v4, 0xe

    invoke-static {v2, v4, v3}, Lcom/android/net/module/util/netlink/InetDiagMessage;->destroySockets(IILjava/util/function/Predicate;)V

    .line 492
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 493
    .local v2, "durationMs":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Destroyed live tcp sockets for uids="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "InetDiagMessage"

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    return-void
.end method

.method private static destroySockets(IILjava/util/function/Predicate;)V
    .locals 8
    .param p0, "proto"    # I
    .param p1, "states"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/function/Predicate<",
            "Lcom/android/net/module/util/netlink/InetDiagMessage;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/net/SocketException;,
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .line 423
    .local p2, "filter":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/net/module/util/netlink/InetDiagMessage;>;"
    const-string v0, "InetDiagMessage"

    const/4 v1, 0x0

    .line 424
    .local v1, "dumpFd":Ljava/io/FileDescriptor;
    const/4 v2, 0x0

    .line 427
    .local v2, "destroyFd":Ljava/io/FileDescriptor;
    :try_start_0
    invoke-static {}, Lcom/android/net/module/util/netlink/NetlinkUtils;->createNetLinkInetDiagSocket()Ljava/io/FileDescriptor;

    move-result-object v3

    move-object v1, v3

    .line 428
    invoke-static {}, Lcom/android/net/module/util/netlink/NetlinkUtils;->createNetLinkInetDiagSocket()Ljava/io/FileDescriptor;

    move-result-object v3

    move-object v2, v3

    .line 429
    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkUtils;->connectSocketToNetlink(Ljava/io/FileDescriptor;)V

    .line 430
    invoke-static {v2}, Lcom/android/net/module/util/netlink/NetlinkUtils;->connectSocketToNetlink(Ljava/io/FileDescriptor;)V

    .line 432
    sget v3, Landroid/system/OsConstants;->AF_INET:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget v4, Landroid/system/OsConstants;->AF_INET6:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/List;->of(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 434
    .local v4, "family":I
    :try_start_1
    invoke-static {v1, p0, p1, v4}, Lcom/android/net/module/util/netlink/InetDiagMessage;->sendNetlinkDumpRequest(Ljava/io/FileDescriptor;III)V
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/system/ErrnoException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 438
    nop

    .line 439
    :try_start_2
    invoke-static {v1, v2, p0, p2}, Lcom/android/net/module/util/netlink/InetDiagMessage;->processNetlinkDumpAndDestroySockets(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;ILjava/util/function/Predicate;)I

    move-result v5

    .line 441
    .local v5, "destroyedSockets":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Destroyed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " sockets, proto="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 442
    invoke-static {p0}, Lcom/android/net/module/util/netlink/NetlinkConstants;->stringForProtocol(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", family="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 443
    invoke-static {v4}, Lcom/android/net/module/util/netlink/NetlinkConstants;->stringForAddressFamily(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", states="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 441
    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    nop

    .end local v4    # "family":I
    .end local v5    # "destroyedSockets":I
    goto :goto_0

    .line 435
    .restart local v4    # "family":I
    :catch_0
    move-exception v5

    .line 436
    .local v5, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to send netlink dump request: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 437
    goto :goto_0

    .line 447
    .end local v4    # "family":I
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-static {v1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 448
    invoke-static {v2}, Lcom/android/net/module/util/netlink/InetDiagMessage;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 449
    nop

    .line 450
    return-void

    .line 447
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 448
    invoke-static {v2}, Lcom/android/net/module/util/netlink/InetDiagMessage;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 449
    throw v0
.end method

.method public static getConnectionOwnerUid(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;)I
    .locals 5
    .param p0, "protocol"    # I
    .param p1, "local"    # Ljava/net/InetSocketAddress;
    .param p2, "remote"    # Ljava/net/InetSocketAddress;

    .line 265
    const/4 v0, -0x1

    .line 266
    .local v0, "uid":I
    const/4 v1, 0x0

    .line 268
    .local v1, "fd":Ljava/io/FileDescriptor;
    :try_start_0
    sget v2, Landroid/system/OsConstants;->NETLINK_INET_DIAG:I

    invoke-static {v2}, Lcom/android/net/module/util/netlink/NetlinkUtils;->netlinkSocketForProto(I)Ljava/io/FileDescriptor;

    move-result-object v2

    move-object v1, v2

    .line 269
    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkUtils;->connectSocketToNetlink(Ljava/io/FileDescriptor;)V

    .line 270
    invoke-static {p0, p1, p2, v1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUid(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;Ljava/io/FileDescriptor;)I

    move-result v2
    :try_end_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v2

    .line 275
    nop

    :goto_0
    invoke-static {v1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 276
    goto :goto_1

    .line 275
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 271
    :catch_0
    move-exception v2

    .line 273
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "InetDiagMessage"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 277
    :goto_1
    return v0

    .line 275
    :goto_2
    invoke-static {v1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->closeSocketQuietly(Ljava/io/FileDescriptor;)V

    .line 276
    throw v2
.end method

.method public static inetDiagReqV2(ILcom/android/net/module/util/netlink/StructInetDiagSockId;ISSIII)[B
    .locals 13
    .param p0, "protocol"    # I
    .param p1, "id"    # Lcom/android/net/module/util/netlink/StructInetDiagSockId;
    .param p2, "family"    # I
    .param p3, "type"    # S
    .param p4, "flags"    # S
    .param p5, "pad"    # I
    .param p6, "idiagExt"    # I
    .param p7, "state"    # I

    .line 140
    const/16 v0, 0x48

    new-array v0, v0, [B

    .line 141
    .local v0, "bytes":[B
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 142
    .local v1, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 144
    new-instance v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-direct {v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V

    .line 145
    .local v2, "nlMsgHdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    array-length v3, v0

    iput v3, v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    .line 146
    move/from16 v3, p3

    iput-short v3, v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    .line 147
    move/from16 v4, p4

    iput-short v4, v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S

    .line 148
    invoke-virtual {v2, v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 149
    new-instance v12, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;

    move-object v5, v12

    move v6, p0

    move-object v7, p1

    move v8, p2

    move/from16 v9, p5

    move/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v5 .. v11}, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;-><init>(ILcom/android/net/module/util/netlink/StructInetDiagSockId;IIII)V

    .line 152
    .local v5, "inetDiagReqV2":Lcom/android/net/module/util/netlink/StructInetDiagReqV2;
    invoke-virtual {v5, v1}, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->pack(Ljava/nio/ByteBuffer;)V

    .line 153
    return-object v0
.end method

.method public static inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IS)[B
    .locals 8
    .param p0, "protocol"    # I
    .param p1, "local"    # Ljava/net/InetSocketAddress;
    .param p2, "remote"    # Ljava/net/InetSocketAddress;
    .param p3, "family"    # I
    .param p4, "flags"    # S

    .line 83
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, -0x1

    move v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v7}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISIII)[B

    move-result-object v0

    return-object v0
.end method

.method public static inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISIII)[B
    .locals 11
    .param p0, "protocol"    # I
    .param p1, "local"    # Ljava/net/InetSocketAddress;
    .param p2, "remote"    # Ljava/net/InetSocketAddress;
    .param p3, "family"    # I
    .param p4, "flags"    # S
    .param p5, "pad"    # I
    .param p6, "idiagExt"    # I
    .param p7, "state"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 113
    move-object v0, p1

    move-object v1, p2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v3

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    if-ne v4, v2, :cond_3

    .line 117
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 118
    new-instance v2, Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    invoke-direct {v2, p1, p2}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;-><init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;)V

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    move-object v4, v2

    .line 119
    .local v4, "id":Lcom/android/net/module/util/netlink/StructInetDiagSockId;
    const/16 v6, 0x14

    move v3, p0

    move v5, p3

    move v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    invoke-static/range {v3 .. v10}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILcom/android/net/module/util/netlink/StructInetDiagSockId;ISSIII)[B

    move-result-object v2

    return-object v2

    .line 114
    .end local v4    # "id":Lcom/android/net/module/util/netlink/StructInetDiagSockId;
    :cond_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Local and remote must be both null or both non-null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static isAdbSocket(Lcom/android/net/module/util/netlink/InetDiagMessage;)Z
    .locals 2
    .param p0, "msg"    # Lcom/android/net/module/util/netlink/InetDiagMessage;

    .line 378
    iget-object v0, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I

    const/16 v1, 0x7d0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isLoopback(Lcom/android/net/module/util/netlink/InetDiagMessage;)Z
    .locals 3
    .param p0, "msg"    # Lcom/android/net/module/util/netlink/InetDiagMessage;

    .line 414
    iget-object v0, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget-object v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->id:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    iget-object v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->locSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 415
    .local v0, "srcAddr":Ljava/net/InetAddress;
    iget-object v1, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget-object v1, v1, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->id:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    iget-object v1, v1, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->remSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 416
    .local v1, "dstAddr":Ljava/net/InetAddress;
    invoke-static {v0}, Lcom/android/net/module/util/netlink/InetDiagMessage;->isLoopbackAddress(Ljava/net/InetAddress;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 417
    invoke-static {v1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->isLoopbackAddress(Ljava/net/InetAddress;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 418
    invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 416
    :goto_1
    return v2
.end method

.method private static isLoopbackAddress(Ljava/net/InetAddress;)Z
    .locals 5
    .param p0, "addr"    # Ljava/net/InetAddress;

    .line 395
    invoke-virtual {p0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 396
    :cond_0
    instance-of v0, p0, Ljava/net/Inet6Address;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 400
    :cond_1
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 401
    .local v0, "addrBytes":[B
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/16 v4, 0xa

    if-ge v3, v4, :cond_3

    .line 402
    aget-byte v4, v0, v3

    if-eqz v4, :cond_2

    return v2

    .line 401
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 404
    .end local v3    # "i":I
    :cond_3
    aget-byte v3, v0, v4

    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    const/16 v3, 0xb

    aget-byte v3, v0, v3

    if-ne v3, v4, :cond_4

    const/16 v3, 0xc

    aget-byte v3, v0, v3

    const/16 v4, 0x7f

    if-ne v3, v4, :cond_4

    goto :goto_1

    :cond_4
    move v1, v2

    :goto_1
    return v1
.end method

.method static synthetic lambda$destroyLiveTcpSockets$0(Ljava/util/Set;Ljava/util/Set;Lcom/android/net/module/util/netlink/InetDiagMessage;)Z
    .locals 1
    .param p0, "exemptUids"    # Ljava/util/Set;
    .param p1, "ranges"    # Ljava/util/Set;
    .param p2, "diagMsg"    # Lcom/android/net/module/util/netlink/InetDiagMessage;

    .line 467
    iget-object v0, p2, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    invoke-static {p2, p1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->containsUid(Lcom/android/net/module/util/netlink/InetDiagMessage;Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-static {p2}, Lcom/android/net/module/util/netlink/InetDiagMessage;->isLoopback(Lcom/android/net/module/util/netlink/InetDiagMessage;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 470
    invoke-static {p2}, Lcom/android/net/module/util/netlink/InetDiagMessage;->isAdbSocket(Lcom/android/net/module/util/netlink/InetDiagMessage;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 467
    :goto_0
    return v0
.end method

.method static synthetic lambda$destroyLiveTcpSocketsByOwnerUids$1(Ljava/util/Set;Lcom/android/net/module/util/netlink/InetDiagMessage;)Z
    .locals 1
    .param p0, "ownerUids"    # Ljava/util/Set;
    .param p1, "diagMsg"    # Lcom/android/net/module/util/netlink/InetDiagMessage;

    .line 489
    iget-object v0, p1, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    invoke-static {p1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->isLoopback(Lcom/android/net/module/util/netlink/InetDiagMessage;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 491
    invoke-static {p1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->isAdbSocket(Lcom/android/net/module/util/netlink/InetDiagMessage;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 489
    :goto_0
    return v0
.end method

.method private static lookupUid(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;Ljava/io/FileDescriptor;)I
    .locals 13
    .param p0, "protocol"    # I
    .param p1, "local"    # Ljava/net/InetSocketAddress;
    .param p2, "remote"    # Ljava/net/InetSocketAddress;
    .param p3, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .line 214
    move v7, p0

    sget-object v0, Lcom/android/net/module/util/netlink/InetDiagMessage;->FAMILY:[I

    array-length v8, v0

    const/4 v9, 0x0

    move v10, v9

    :goto_0
    const/4 v11, -0x1

    if-ge v10, v8, :cond_2

    aget v12, v0, v10

    .line 220
    .local v12, "family":I
    sget v1, Landroid/system/OsConstants;->IPPROTO_UDP:I

    if-ne v7, v1, :cond_0

    .line 221
    const/4 v5, 0x1

    move v1, p0

    move-object v2, p2

    move-object v3, p1

    move v4, v12

    move-object/from16 v6, p3

    invoke-static/range {v1 .. v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I

    move-result v1

    .local v1, "uid":I
    goto :goto_1

    .line 223
    .end local v1    # "uid":I
    :cond_0
    const/4 v5, 0x1

    move v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, v12

    move-object/from16 v6, p3

    invoke-static/range {v1 .. v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I

    move-result v1

    .line 225
    .restart local v1    # "uid":I
    :goto_1
    if-eq v1, v11, :cond_1

    .line 226
    return v1

    .line 214
    .end local v12    # "family":I
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 237
    .end local v1    # "uid":I
    :cond_2
    sget v0, Landroid/system/OsConstants;->IPPROTO_UDP:I

    if-ne v7, v0, :cond_5

    .line 239
    :try_start_0
    new-instance v3, Ljava/net/InetSocketAddress;

    const-string v0, "::"

    .line 240
    invoke-static {v0}, Ljava/net/Inet6Address;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-direct {v3, v0, v9}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 241
    .local v3, "wildcard":Ljava/net/InetSocketAddress;
    sget v4, Landroid/system/OsConstants;->AF_INET6:I

    const/16 v5, 0x301

    move v1, p0

    move-object v2, p1

    move-object/from16 v6, p3

    invoke-static/range {v1 .. v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I

    move-result v0

    .line 243
    .local v0, "uid":I
    if-eq v0, v11, :cond_3

    .line 244
    return v0

    .line 246
    :cond_3
    new-instance v1, Ljava/net/InetSocketAddress;

    const-string v2, "0.0.0.0"

    invoke-static {v2}, Ljava/net/Inet4Address;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-direct {v1, v2, v9}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    move-object v3, v1

    .line 247
    sget v4, Landroid/system/OsConstants;->AF_INET:I

    const/16 v5, 0x301

    move v1, p0

    move-object v2, p1

    move-object/from16 v6, p3

    invoke-static/range {v1 .. v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I

    move-result v1
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 249
    if-eq v0, v11, :cond_4

    .line 250
    return v0

    .line 254
    .end local v3    # "wildcard":Ljava/net/InetSocketAddress;
    :cond_4
    goto :goto_2

    .line 252
    .end local v0    # "uid":I
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/net/UnknownHostException;
    const-string v1, "InetDiagMessage"

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :cond_5
    :goto_2
    return v11
.end method

.method private static lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I
    .locals 7
    .param p0, "protocol"    # I
    .param p1, "local"    # Ljava/net/InetSocketAddress;
    .param p2, "remote"    # Ljava/net/InetSocketAddress;
    .param p3, "family"    # I
    .param p4, "flags"    # S
    .param p5, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .line 189
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IS)[B

    move-result-object v6

    .line 190
    .local v6, "msg":[B
    const/4 v2, 0x0

    array-length v3, v6

    const-wide/16 v4, 0x1f4

    move-object v0, p5

    move-object v1, v6

    invoke-static/range {v0 .. v5}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I

    .line 191
    const/16 v0, 0x2000

    const-wide/16 v1, 0x1f4

    invoke-static {p5, v0, v1, v2}, Lcom/android/net/module/util/netlink/NetlinkUtils;->recvMessage(Ljava/io/FileDescriptor;IJ)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 193
    .local v0, "response":Ljava/nio/ByteBuffer;
    sget v1, Landroid/system/OsConstants;->NETLINK_INET_DIAG:I

    invoke-static {v0, v1}, Lcom/android/net/module/util/netlink/NetlinkMessage;->parse(Ljava/nio/ByteBuffer;I)Lcom/android/net/module/util/netlink/NetlinkMessage;

    move-result-object v1

    .line 194
    .local v1, "nlMsg":Lcom/android/net/module/util/netlink/NetlinkMessage;
    const/4 v2, -0x1

    if-nez v1, :cond_0

    .line 195
    return v2

    .line 197
    :cond_0
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v3

    .line 198
    .local v3, "hdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    iget-short v4, v3, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 199
    return v2

    .line 201
    :cond_1
    instance-of v4, v1, Lcom/android/net/module/util/netlink/InetDiagMessage;

    if-eqz v4, :cond_2

    .line 202
    move-object v2, v1

    check-cast v2, Lcom/android/net/module/util/netlink/InetDiagMessage;

    iget-object v2, v2, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget v2, v2, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I

    return v2

    .line 204
    :cond_2
    return v2
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/InetDiagMessage;
    .locals 2
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 170
    new-instance v0, Lcom/android/net/module/util/netlink/InetDiagMessage;

    invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/InetDiagMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 171
    .local v0, "msg":Lcom/android/net/module/util/netlink/InetDiagMessage;
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    move-result-object v1

    iput-object v1, v0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    .line 172
    if-nez v1, :cond_0

    .line 173
    const/4 v1, 0x0

    return-object v1

    .line 175
    :cond_0
    return-object v0
.end method

.method private static processNetlinkDumpAndDestroySockets(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;ILjava/util/function/Predicate;)I
    .locals 9
    .param p0, "dumpFd"    # Ljava/io/FileDescriptor;
    .param p1, "destroyFd"    # Ljava/io/FileDescriptor;
    .param p2, "proto"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/FileDescriptor;",
            "Ljava/io/FileDescriptor;",
            "I",
            "Ljava/util/function/Predicate<",
            "Lcom/android/net/module/util/netlink/InetDiagMessage;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 327
    .local p3, "filter":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/net/module/util/netlink/InetDiagMessage;>;"
    const/4 v0, 0x0

    .line 330
    .local v0, "destroyedSockets":I
    :goto_0
    const/16 v1, 0x2000

    const-wide/16 v2, 0x12c

    invoke-static {p0, v1, v2, v3}, Lcom/android/net/module/util/netlink/NetlinkUtils;->recvMessage(Ljava/io/FileDescriptor;IJ)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 333
    .local v1, "buf":Ljava/nio/ByteBuffer;
    :goto_1
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-lez v2, :cond_5

    .line 334
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    .line 335
    .local v2, "position":I
    sget v3, Landroid/system/OsConstants;->NETLINK_INET_DIAG:I

    invoke-static {v1, v3}, Lcom/android/net/module/util/netlink/NetlinkMessage;->parse(Ljava/nio/ByteBuffer;I)Lcom/android/net/module/util/netlink/NetlinkMessage;

    move-result-object v3

    .line 336
    .local v3, "nlMsg":Lcom/android/net/module/util/netlink/NetlinkMessage;
    const-string v4, "InetDiagMessage"

    if-nez v3, :cond_0

    .line 338
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 339
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to parse netlink message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkConstants;->hexify(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    goto :goto_3

    .line 343
    :cond_0
    invoke-virtual {v3}, Lcom/android/net/module/util/netlink/NetlinkMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v5

    iget-short v5, v5, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    .line 344
    return v0

    .line 347
    :cond_1
    instance-of v5, v3, Lcom/android/net/module/util/netlink/InetDiagMessage;

    if-nez v5, :cond_2

    .line 348
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received unexpected netlink message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    goto :goto_1

    .line 352
    :cond_2
    move-object v5, v3

    check-cast v5, Lcom/android/net/module/util/netlink/InetDiagMessage;

    .line 353
    .local v5, "diagMsg":Lcom/android/net/module/util/netlink/InetDiagMessage;
    invoke-interface {p3, v5}, Ljava/util/function/Predicate;->test(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 355
    :try_start_0
    invoke-static {p1, p2, v5}, Lcom/android/net/module/util/netlink/InetDiagMessage;->sendNetlinkDestroyRequest(Ljava/io/FileDescriptor;ILcom/android/net/module/util/netlink/InetDiagMessage;)V
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/system/ErrnoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    add-int/lit8 v0, v0, 0x1

    .line 362
    goto :goto_2

    .line 357
    :catch_0
    move-exception v6

    .line 358
    .local v6, "e":Ljava/lang/Exception;
    instance-of v7, v6, Landroid/system/ErrnoException;

    if-eqz v7, :cond_3

    move-object v7, v6

    check-cast v7, Landroid/system/ErrnoException;

    iget v7, v7, Landroid/system/ErrnoException;->errno:I

    sget v8, Landroid/system/OsConstants;->ENOENT:I

    if-eq v7, v8, :cond_4

    .line 360
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to destroy socket: diagMsg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    .end local v2    # "position":I
    .end local v3    # "nlMsg":Lcom/android/net/module/util/netlink/NetlinkMessage;
    .end local v5    # "diagMsg":Lcom/android/net/module/util/netlink/InetDiagMessage;
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_2
    goto/16 :goto_1

    .line 365
    .end local v1    # "buf":Ljava/nio/ByteBuffer;
    :cond_5
    :goto_3
    goto/16 :goto_0
.end method

.method private static sendNetlinkDestroyRequest(Ljava/io/FileDescriptor;ILcom/android/net/module/util/netlink/InetDiagMessage;)V
    .locals 16
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "proto"    # I
    .param p2, "diagMsg"    # Lcom/android/net/module/util/netlink/InetDiagMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 296
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget-object v3, v1, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->id:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    iget-object v1, v0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget-short v4, v1, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S

    const/16 v5, 0x15

    const/4 v6, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v1, v0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    iget-short v1, v1, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_state:S

    const/4 v2, 0x1

    shl-int v9, v2, v1

    move/from16 v2, p1

    invoke-static/range {v2 .. v9}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILcom/android/net/module/util/netlink/StructInetDiagSockId;ISSIII)[B

    move-result-object v1

    .line 306
    .local v1, "destroyMsg":[B
    const/4 v12, 0x0

    array-length v13, v1

    const-wide/16 v14, 0x12c

    move-object/from16 v10, p0

    move-object v11, v1

    invoke-static/range {v10 .. v15}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I

    .line 307
    invoke-static/range {p0 .. p0}, Lcom/android/net/module/util/netlink/NetlinkUtils;->receiveNetlinkAck(Ljava/io/FileDescriptor;)V

    .line 308
    return-void
.end method

.method private static sendNetlinkDumpRequest(Ljava/io/FileDescriptor;III)V
    .locals 13
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "proto"    # I
    .param p2, "states"    # I
    .param p3, "family"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 312
    const/4 v1, 0x0

    const/16 v3, 0x14

    const/16 v4, 0x301

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v0, p1

    move/from16 v2, p3

    move v7, p2

    invoke-static/range {v0 .. v7}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILcom/android/net/module/util/netlink/StructInetDiagSockId;ISSIII)[B

    move-result-object v0

    .line 321
    .local v0, "dumpMsg":[B
    const/4 v9, 0x0

    array-length v10, v0

    const-wide/16 v11, 0x12c

    move-object v7, p0

    move-object v8, v0

    invoke-static/range {v7 .. v12}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I

    .line 322
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .line 498
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InetDiagMessage{ nlmsghdr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 500
    iget-object v1, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    const-string v2, ""

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    sget v3, Landroid/system/OsConstants;->NETLINK_INET_DIAG:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, inet_diag_msg{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 502
    iget-object v1, p0, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagMsg:Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "} }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 498
    return-object v0
.end method
