public class com.android.net.module.util.netlink.NetlinkConstants {
	 /* .source "NetlinkConstants.java" */
	 /* # static fields */
	 private static final HEX_DIGITS;
	 public static final Integer IFF_LOWER_UP;
	 public static final Integer IFF_UP;
	 public static final Integer INET_DIAG_MEMINFO;
	 public static final Object IPCTNL_MSG_CT_DELETE;
	 public static final Object IPCTNL_MSG_CT_GET;
	 public static final Object IPCTNL_MSG_CT_GET_CTRZERO;
	 public static final Object IPCTNL_MSG_CT_GET_DYING;
	 public static final Object IPCTNL_MSG_CT_GET_STATS;
	 public static final Object IPCTNL_MSG_CT_GET_STATS_CPU;
	 public static final Object IPCTNL_MSG_CT_GET_UNCONFIRMED;
	 public static final Object IPCTNL_MSG_CT_NEW;
	 public static final Object NFNL_SUBSYS_CTNETLINK;
	 public static final Integer NLA_ALIGNTO;
	 public static final Object NLMSG_DONE;
	 public static final Object NLMSG_ERROR;
	 public static final Object NLMSG_MAX_RESERVED;
	 public static final Object NLMSG_NOOP;
	 public static final Object NLMSG_OVERRUN;
	 public static final Integer RTMGRP_IPV4_IFADDR;
	 public static final Integer RTMGRP_IPV6_IFADDR;
	 public static final Integer RTMGRP_IPV6_ROUTE;
	 public static final Integer RTMGRP_LINK;
	 public static final Integer RTMGRP_ND_USEROPT;
	 public static final Object RTM_DELADDR;
	 public static final Object RTM_DELLINK;
	 public static final Object RTM_DELNEIGH;
	 public static final Object RTM_DELROUTE;
	 public static final Object RTM_DELRULE;
	 public static final Integer RTM_F_CLONED;
	 public static final Object RTM_GETADDR;
	 public static final Object RTM_GETLINK;
	 public static final Object RTM_GETNEIGH;
	 public static final Object RTM_GETROUTE;
	 public static final Object RTM_GETRULE;
	 public static final Object RTM_NEWADDR;
	 public static final Object RTM_NEWLINK;
	 public static final Object RTM_NEWNDUSEROPT;
	 public static final Object RTM_NEWNEIGH;
	 public static final Object RTM_NEWROUTE;
	 public static final Object RTM_NEWRULE;
	 public static final Object RTM_SETLINK;
	 public static final Integer RTNLGRP_ND_USEROPT;
	 public static final Object RTN_UNICAST;
	 public static final Object RTPROT_KERNEL;
	 public static final Object RTPROT_RA;
	 public static final Object RT_SCOPE_UNIVERSE;
	 public static final Integer SOCKDIAG_MSG_HEADER_SIZE;
	 public static final Object SOCK_DESTROY;
	 public static final Object SOCK_DIAG_BY_FAMILY;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.NetlinkConstants ( ) {
		 /* .locals 1 */
		 /* .line 266 */
		 /* const/16 v0, 0x10 */
		 /* new-array v0, v0, [C */
		 /* fill-array-data v0, :array_0 */
		 return;
		 /* :array_0 */
		 /* .array-data 2 */
		 /* 0x30s */
		 /* 0x31s */
		 /* 0x32s */
		 /* 0x33s */
		 /* 0x34s */
		 /* 0x35s */
		 /* 0x36s */
		 /* 0x37s */
		 /* 0x38s */
		 /* 0x39s */
		 /* 0x41s */
		 /* 0x42s */
		 /* 0x43s */
		 /* 0x44s */
		 /* 0x45s */
		 /* 0x46s */
	 } // .end array-data
} // .end method
private com.android.net.module.util.netlink.NetlinkConstants ( ) {
	 /* .locals 0 */
	 /* .line 38 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static final Integer alignedLengthOf ( Integer p0 ) {
	 /* .locals 1 */
	 /* .param p0, "length" # I */
	 /* .line 62 */
	 /* if-gtz p0, :cond_0 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 63 */
} // :cond_0
/* add-int/lit8 v0, p0, 0x4 */
/* add-int/lit8 v0, v0, -0x1 */
/* div-int/lit8 v0, v0, 0x4 */
/* mul-int/lit8 v0, v0, 0x4 */
} // .end method
public static final Integer alignedLengthOf ( Object p0 ) {
/* .locals 2 */
/* .param p0, "length" # S */
/* .line 54 */
/* const v0, 0xffff */
/* and-int/2addr v0, p0 */
/* .line 55 */
/* .local v0, "intLength":I */
v1 = com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v0 );
} // .end method
public static java.lang.String hexify ( java.nio.ByteBuffer p0 ) {
/* .locals 3 */
/* .param p0, "buffer" # Ljava/nio/ByteBuffer; */
/* .line 98 */
/* if-nez p0, :cond_0 */
final String v0 = "(null)"; // const-string v0, "(null)"
/* .line 99 */
} // :cond_0
/* nop */
/* .line 100 */
(( java.nio.ByteBuffer ) p0 ).array ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B
v1 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
v2 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* .line 99 */
com.android.net.module.util.netlink.NetlinkConstants .toHexString ( v0,v1,v2 );
} // .end method
public static java.lang.String hexify ( Object[] p0 ) {
/* .locals 2 */
/* .param p0, "bytes" # [B */
/* .line 90 */
/* if-nez p0, :cond_0 */
final String v0 = "(null)"; // const-string v0, "(null)"
/* .line 91 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* array-length v1, p0 */
com.android.net.module.util.netlink.NetlinkConstants .toHexString ( p0,v0,v1 );
} // .end method
public static java.lang.String stringForAddressFamily ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "family" # I */
/* .line 70 */
/* if-ne p0, v0, :cond_0 */
final String v0 = "AF_INET"; // const-string v0, "AF_INET"
/* .line 71 */
} // :cond_0
/* if-ne p0, v0, :cond_1 */
final String v0 = "AF_INET6"; // const-string v0, "AF_INET6"
/* .line 72 */
} // :cond_1
/* if-ne p0, v0, :cond_2 */
final String v0 = "AF_NETLINK"; // const-string v0, "AF_NETLINK"
/* .line 73 */
} // :cond_2
/* if-ne p0, v0, :cond_3 */
final String v0 = "AF_UNSPEC"; // const-string v0, "AF_UNSPEC"
/* .line 74 */
} // :cond_3
java.lang.String .valueOf ( p0 );
} // .end method
private static java.lang.String stringForCtlMsgType ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .line 176 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 181 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown control message type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 180 */
/* :pswitch_0 */
final String v0 = "NLMSG_OVERRUN"; // const-string v0, "NLMSG_OVERRUN"
/* .line 179 */
/* :pswitch_1 */
final String v0 = "NLMSG_DONE"; // const-string v0, "NLMSG_DONE"
/* .line 178 */
/* :pswitch_2 */
final String v0 = "NLMSG_ERROR"; // const-string v0, "NLMSG_ERROR"
/* .line 177 */
/* :pswitch_3 */
final String v0 = "NLMSG_NOOP"; // const-string v0, "NLMSG_NOOP"
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static java.lang.String stringForInetDiagMsgType ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .line 217 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 219 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown SOCK_DIAG type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 218 */
/* :pswitch_0 */
final String v0 = "SOCK_DIAG_BY_FAMILY"; // const-string v0, "SOCK_DIAG_BY_FAMILY"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x14 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private static java.lang.String stringForNfMsgType ( Object p0 ) {
/* .locals 4 */
/* .param p0, "nlmType" # S */
/* .line 228 */
/* shr-int/lit8 v0, p0, 0x8 */
/* int-to-byte v0, v0 */
/* .line 229 */
/* .local v0, "subsysId":B */
/* int-to-byte v1, p0 */
/* .line 230 */
/* .local v1, "msgType":B */
/* packed-switch v0, :pswitch_data_0 */
/* .line 232 */
/* :pswitch_0 */
/* packed-switch v1, :pswitch_data_1 */
/* .line 240 */
/* :pswitch_1 */
final String v2 = "IPCTNL_MSG_CT_GET_UNCONFIRMED"; // const-string v2, "IPCTNL_MSG_CT_GET_UNCONFIRMED"
/* .line 239 */
/* :pswitch_2 */
final String v2 = "IPCTNL_MSG_CT_GET_DYING"; // const-string v2, "IPCTNL_MSG_CT_GET_DYING"
/* .line 238 */
/* :pswitch_3 */
final String v2 = "IPCTNL_MSG_CT_GET_STATS"; // const-string v2, "IPCTNL_MSG_CT_GET_STATS"
/* .line 237 */
/* :pswitch_4 */
final String v2 = "IPCTNL_MSG_CT_GET_STATS_CPU"; // const-string v2, "IPCTNL_MSG_CT_GET_STATS_CPU"
/* .line 236 */
/* :pswitch_5 */
final String v2 = "IPCTNL_MSG_CT_GET_CTRZERO"; // const-string v2, "IPCTNL_MSG_CT_GET_CTRZERO"
/* .line 235 */
/* :pswitch_6 */
final String v2 = "IPCTNL_MSG_CT_DELETE"; // const-string v2, "IPCTNL_MSG_CT_DELETE"
/* .line 234 */
/* :pswitch_7 */
final String v2 = "IPCTNL_MSG_CT_GET"; // const-string v2, "IPCTNL_MSG_CT_GET"
/* .line 233 */
/* :pswitch_8 */
final String v2 = "IPCTNL_MSG_CT_NEW"; // const-string v2, "IPCTNL_MSG_CT_NEW"
/* .line 244 */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "unknown NETFILTER type: " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public static java.lang.String stringForNlMsgType ( Object p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .param p1, "nlFamily" # I */
/* .line 254 */
/* const/16 v0, 0xf */
/* if-gt p0, v0, :cond_0 */
com.android.net.module.util.netlink.NetlinkConstants .stringForCtlMsgType ( p0 );
/* .line 259 */
} // :cond_0
/* if-ne p1, v0, :cond_1 */
com.android.net.module.util.netlink.NetlinkConstants .stringForRtMsgType ( p0 );
/* .line 260 */
} // :cond_1
/* if-ne p1, v0, :cond_2 */
com.android.net.module.util.netlink.NetlinkConstants .stringForInetDiagMsgType ( p0 );
/* .line 261 */
} // :cond_2
/* if-ne p1, v0, :cond_3 */
com.android.net.module.util.netlink.NetlinkConstants .stringForNfMsgType ( p0 );
/* .line 263 */
} // :cond_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.String stringForProtocol ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "protocol" # I */
/* .line 81 */
/* if-ne p0, v0, :cond_0 */
final String v0 = "IPPROTO_TCP"; // const-string v0, "IPPROTO_TCP"
/* .line 82 */
} // :cond_0
/* if-ne p0, v0, :cond_1 */
final String v0 = "IPPROTO_UDP"; // const-string v0, "IPPROTO_UDP"
/* .line 83 */
} // :cond_1
java.lang.String .valueOf ( p0 );
} // .end method
private static java.lang.String stringForRtMsgType ( Object p0 ) {
/* .locals 2 */
/* .param p0, "nlmType" # S */
/* .line 190 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 208 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "unknown RTM type: " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 207 */
/* :sswitch_0 */
final String v0 = "RTM_NEWNDUSEROPT"; // const-string v0, "RTM_NEWNDUSEROPT"
/* .line 206 */
/* :sswitch_1 */
final String v0 = "RTM_GETRULE"; // const-string v0, "RTM_GETRULE"
/* .line 205 */
/* :sswitch_2 */
final String v0 = "RTM_DELRULE"; // const-string v0, "RTM_DELRULE"
/* .line 204 */
/* :sswitch_3 */
final String v0 = "RTM_NEWRULE"; // const-string v0, "RTM_NEWRULE"
/* .line 203 */
/* :sswitch_4 */
final String v0 = "RTM_GETNEIGH"; // const-string v0, "RTM_GETNEIGH"
/* .line 202 */
/* :sswitch_5 */
final String v0 = "RTM_DELNEIGH"; // const-string v0, "RTM_DELNEIGH"
/* .line 201 */
/* :sswitch_6 */
final String v0 = "RTM_NEWNEIGH"; // const-string v0, "RTM_NEWNEIGH"
/* .line 200 */
/* :sswitch_7 */
final String v0 = "RTM_GETROUTE"; // const-string v0, "RTM_GETROUTE"
/* .line 199 */
/* :sswitch_8 */
final String v0 = "RTM_DELROUTE"; // const-string v0, "RTM_DELROUTE"
/* .line 198 */
/* :sswitch_9 */
final String v0 = "RTM_NEWROUTE"; // const-string v0, "RTM_NEWROUTE"
/* .line 197 */
/* :sswitch_a */
final String v0 = "RTM_GETADDR"; // const-string v0, "RTM_GETADDR"
/* .line 196 */
/* :sswitch_b */
final String v0 = "RTM_DELADDR"; // const-string v0, "RTM_DELADDR"
/* .line 195 */
/* :sswitch_c */
final String v0 = "RTM_NEWADDR"; // const-string v0, "RTM_NEWADDR"
/* .line 194 */
/* :sswitch_d */
final String v0 = "RTM_SETLINK"; // const-string v0, "RTM_SETLINK"
/* .line 193 */
/* :sswitch_e */
final String v0 = "RTM_GETLINK"; // const-string v0, "RTM_GETLINK"
/* .line 192 */
/* :sswitch_f */
final String v0 = "RTM_DELLINK"; // const-string v0, "RTM_DELLINK"
/* .line 191 */
/* :sswitch_10 */
final String v0 = "RTM_NEWLINK"; // const-string v0, "RTM_NEWLINK"
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x10 -> :sswitch_10 */
/* 0x11 -> :sswitch_f */
/* 0x12 -> :sswitch_e */
/* 0x13 -> :sswitch_d */
/* 0x14 -> :sswitch_c */
/* 0x15 -> :sswitch_b */
/* 0x16 -> :sswitch_a */
/* 0x18 -> :sswitch_9 */
/* 0x19 -> :sswitch_8 */
/* 0x1a -> :sswitch_7 */
/* 0x1c -> :sswitch_6 */
/* 0x1d -> :sswitch_5 */
/* 0x1e -> :sswitch_4 */
/* 0x20 -> :sswitch_3 */
/* 0x21 -> :sswitch_2 */
/* 0x22 -> :sswitch_1 */
/* 0x44 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public static java.lang.String toHexString ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 7 */
/* .param p0, "array" # [B */
/* .param p1, "offset" # I */
/* .param p2, "length" # I */
/* .line 272 */
/* mul-int/lit8 v0, p2, 0x2 */
/* new-array v0, v0, [C */
/* .line 274 */
/* .local v0, "buf":[C */
int v1 = 0; // const/4 v1, 0x0
/* .line 275 */
/* .local v1, "bufIndex":I */
/* move v2, p1 */
/* .local v2, "i":I */
} // :goto_0
/* add-int v3, p1, p2 */
/* if-ge v2, v3, :cond_0 */
/* .line 276 */
/* aget-byte v3, p0, v2 */
/* .line 277 */
/* .local v3, "b":B */
/* add-int/lit8 v4, v1, 0x1 */
} // .end local v1 # "bufIndex":I
/* .local v4, "bufIndex":I */
v5 = com.android.net.module.util.netlink.NetlinkConstants.HEX_DIGITS;
/* ushr-int/lit8 v6, v3, 0x4 */
/* and-int/lit8 v6, v6, 0xf */
/* aget-char v6, v5, v6 */
/* aput-char v6, v0, v1 */
/* .line 278 */
/* add-int/lit8 v1, v4, 0x1 */
} // .end local v4 # "bufIndex":I
/* .restart local v1 # "bufIndex":I */
/* and-int/lit8 v6, v3, 0xf */
/* aget-char v5, v5, v6 */
/* aput-char v5, v0, v4 */
/* .line 275 */
} // .end local v3 # "b":B
/* add-int/lit8 v2, v2, 0x1 */
/* .line 281 */
} // .end local v2 # "i":I
} // :cond_0
/* new-instance v2, Ljava/lang/String; */
/* invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V */
} // .end method
