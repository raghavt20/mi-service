.class public Lcom/android/net/module/util/netlink/StructInetDiagReqV2;
.super Ljava/lang/Object;
.source "StructInetDiagReqV2.java"


# static fields
.field public static final INET_DIAG_REQ_V2_ALL_STATES:I = -0x1

.field public static final STRUCT_SIZE:I = 0x38


# instance fields
.field private final mId:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

.field private final mIdiagExt:B

.field private final mPad:B

.field private final mSdiagFamily:B

.field private final mSdiagProtocol:B

.field private final mState:I


# direct methods
.method public constructor <init>(ILcom/android/net/module/util/netlink/StructInetDiagSockId;IIII)V
    .locals 1
    .param p1, "protocol"    # I
    .param p2, "id"    # Lcom/android/net/module/util/netlink/StructInetDiagSockId;
    .param p3, "family"    # I
    .param p4, "pad"    # I
    .param p5, "extension"    # I
    .param p6, "state"    # I

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagFamily:B

    .line 53
    int-to-byte v0, p1

    iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagProtocol:B

    .line 54
    iput-object p2, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mId:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    .line 55
    int-to-byte v0, p4

    iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mPad:B

    .line 56
    int-to-byte v0, p5

    iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mIdiagExt:B

    .line 57
    iput p6, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mState:I

    .line 58
    return-void
.end method


# virtual methods
.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 65
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagFamily:B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 66
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagProtocol:B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 67
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mIdiagExt:B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 68
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mPad:B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 69
    iget v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mState:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 70
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mId:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->pack(Ljava/nio/ByteBuffer;)V

    .line 71
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 75
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagFamily:B

    invoke-static {v0}, Lcom/android/net/module/util/netlink/NetlinkConstants;->stringForAddressFamily(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "familyStr":Ljava/lang/String;
    iget-byte v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagProtocol:B

    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkConstants;->stringForAddressFamily(I)Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "protocolStr":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "StructInetDiagReqV2{ sdiag_family{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, sdiag_protocol{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, idiag_ext{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-byte v3, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mIdiagExt:B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")}, pad{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-byte v3, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mPad:B

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, idiag_states{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mState:I

    .line 83
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 84
    iget-object v3, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mId:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    const-string v3, "inet_diag_sockid=null"

    :goto_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 78
    return-object v2
.end method
