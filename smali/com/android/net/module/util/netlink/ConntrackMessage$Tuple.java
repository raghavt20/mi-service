public class com.android.net.module.util.netlink.ConntrackMessage$Tuple {
	 /* .source "ConntrackMessage.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/android/net/module/util/netlink/ConntrackMessage; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Tuple" */
} // .end annotation
/* # instance fields */
public final java.net.Inet4Address dstIp;
public final Object dstPort;
public final Object protoNum;
public final java.net.Inet4Address srcIp;
public final Object srcPort;
/* # direct methods */
public com.android.net.module.util.netlink.ConntrackMessage$Tuple ( ) {
/* .locals 1 */
/* .param p1, "ip" # Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4; */
/* .param p2, "proto" # Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto; */
/* .line 113 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 114 */
v0 = this.src;
this.srcIp = v0;
/* .line 115 */
v0 = this.dst;
this.dstIp = v0;
/* .line 116 */
/* iget-short v0, p2, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;->srcPort:S */
/* iput-short v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->srcPort:S */
/* .line 117 */
/* iget-short v0, p2, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;->dstPort:S */
/* iput-short v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->dstPort:S */
/* .line 118 */
/* iget-byte v0, p2, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;->protoNum:B */
/* iput-byte v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->protoNum:B */
/* .line 119 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 124 */
/* instance-of v0, p1, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 125 */
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
/* .line 126 */
/* .local v0, "that":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
v2 = this.srcIp;
v3 = this.srcIp;
v2 = java.util.Objects .equals ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.dstIp;
v3 = this.dstIp;
/* .line 127 */
v2 = java.util.Objects .equals ( v2,v3 );
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* iget-short v2, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->srcPort:S */
	 /* iget-short v3, v0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->srcPort:S */
	 /* if-ne v2, v3, :cond_1 */
	 /* iget-short v2, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->dstPort:S */
	 /* iget-short v3, v0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->dstPort:S */
	 /* if-ne v2, v3, :cond_1 */
	 /* iget-byte v2, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->protoNum:B */
	 /* iget-byte v3, v0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->protoNum:B */
	 /* if-ne v2, v3, :cond_1 */
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_1
/* nop */
/* .line 126 */
} // :goto_0
} // .end method
public Integer hashCode ( ) {
/* .locals 5 */
/* .line 135 */
v0 = this.srcIp;
v1 = this.dstIp;
/* iget-short v2, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->srcPort:S */
java.lang.Short .valueOf ( v2 );
/* iget-short v3, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->dstPort:S */
java.lang.Short .valueOf ( v3 );
/* iget-byte v4, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->protoNum:B */
java.lang.Byte .valueOf ( v4 );
/* filled-new-array {v0, v1, v2, v3, v4}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
} // .end method
public java.lang.String toString ( ) {
/* .locals 6 */
/* .line 140 */
v0 = this.srcIp;
final String v1 = "null"; // const-string v1, "null"
/* if-nez v0, :cond_0 */
/* move-object v0, v1 */
} // :cond_0
(( java.net.Inet4Address ) v0 ).getHostAddress ( ); // invoke-virtual {v0}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
/* .line 141 */
/* .local v0, "srcIpStr":Ljava/lang/String; */
} // :goto_0
v2 = this.dstIp;
/* if-nez v2, :cond_1 */
} // :cond_1
(( java.net.Inet4Address ) v2 ).getHostAddress ( ); // invoke-virtual {v2}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;
/* .line 142 */
/* .local v1, "dstIpStr":Ljava/lang/String; */
} // :goto_1
/* iget-byte v2, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->protoNum:B */
com.android.net.module.util.netlink.NetlinkConstants .stringForProtocol ( v2 );
/* .line 144 */
/* .local v2, "protoStr":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Tuple{"; // const-string v4, "Tuple{"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ": "; // const-string v4, ": "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = ":"; // const-string v4, ":"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v5, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->srcPort:S */
/* .line 146 */
v5 = java.lang.Short .toUnsignedInt ( v5 );
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " -> "; // const-string v5, " -> "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v4, p0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;->dstPort:S */
/* .line 147 */
v4 = java.lang.Short .toUnsignedInt ( v4 );
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v4, "}" */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 144 */
} // .end method
