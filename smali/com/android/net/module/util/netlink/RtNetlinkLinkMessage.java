public class com.android.net.module.util.netlink.RtNetlinkLinkMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "RtNetlinkLinkMessage.java" */
	 /* # static fields */
	 public static final Object IFLA_ADDRESS;
	 public static final Object IFLA_IFNAME;
	 public static final Object IFLA_MTU;
	 /* # instance fields */
	 private android.net.MacAddress mHardwareAddress;
	 private com.android.net.module.util.netlink.StructIfinfoMsg mIfinfomsg;
	 private java.lang.String mInterfaceName;
	 private Integer mMtu;
	 /* # direct methods */
	 private com.android.net.module.util.netlink.RtNetlinkLinkMessage ( ) {
		 /* .locals 2 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .line 54 */
		 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 55 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mIfinfomsg = v0;
		 /* .line 56 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* iput v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I */
		 /* .line 57 */
		 this.mHardwareAddress = v0;
		 /* .line 58 */
		 this.mInterfaceName = v0;
		 /* .line 59 */
		 return;
	 } // .end method
	 public static com.android.net.module.util.netlink.RtNetlinkLinkMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
		 /* .locals 4 */
		 /* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 90 */
		 /* new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage; */
		 /* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 92 */
		 /* .local v0, "linkMsg":Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage; */
		 com.android.net.module.util.netlink.StructIfinfoMsg .parse ( p1 );
		 this.mIfinfomsg = v1;
		 /* .line 93 */
		 /* if-nez v1, :cond_0 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 96 */
	 } // :cond_0
	 v1 = 	 (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
	 /* .line 97 */
	 /* .local v1, "baseOffset":I */
	 int v2 = 4; // const/4 v2, 0x4
	 com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v2,p1 );
	 /* .line 98 */
	 /* .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
	 if ( v2 != null) { // if-eqz v2, :cond_1
		 /* .line 99 */
		 int v3 = 0; // const/4 v3, 0x0
		 v3 = 		 (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsInt ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInt(I)I
		 /* iput v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I */
		 /* .line 103 */
	 } // :cond_1
	 (( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
	 /* .line 104 */
	 int v3 = 1; // const/4 v3, 0x1
	 com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,p1 );
	 /* .line 105 */
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 /* .line 106 */
		 (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsMacAddress ( ); // invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsMacAddress()Landroid/net/MacAddress;
		 this.mHardwareAddress = v3;
		 /* .line 110 */
	 } // :cond_2
	 (( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
	 /* .line 111 */
	 int v3 = 3; // const/4 v3, 0x3
	 com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,p1 );
	 /* .line 112 */
	 if ( v2 != null) { // if-eqz v2, :cond_3
		 /* .line 113 */
		 (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsString ( ); // invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsString()Ljava/lang/String;
		 this.mInterfaceName = v3;
		 /* .line 116 */
	 } // :cond_3
} // .end method
/* # virtual methods */
public android.net.MacAddress getHardwareAddress ( ) {
	 /* .locals 1 */
	 /* .line 72 */
	 v0 = this.mHardwareAddress;
} // .end method
public com.android.net.module.util.netlink.StructIfinfoMsg getIfinfoHeader ( ) {
	 /* .locals 1 */
	 /* .line 67 */
	 v0 = this.mIfinfomsg;
} // .end method
public java.lang.String getInterfaceName ( ) {
	 /* .locals 1 */
	 /* .line 77 */
	 v0 = this.mInterfaceName;
} // .end method
public Integer getMtu ( ) {
	 /* .locals 1 */
	 /* .line 62 */
	 /* iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I */
} // .end method
protected void pack ( java.nio.ByteBuffer p0 ) {
	 /* .locals 3 */
	 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 124 */
	 (( com.android.net.module.util.netlink.RtNetlinkLinkMessage ) p0 ).getHeader ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
	 (( com.android.net.module.util.netlink.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 125 */
	 v0 = this.mIfinfomsg;
	 (( com.android.net.module.util.netlink.StructIfinfoMsg ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 127 */
	 /* iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 128 */
		 /* new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr; */
		 int v2 = 4; // const/4 v2, 0x4
		 /* invoke-direct {v1, v2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SI)V */
		 /* move-object v0, v1 */
		 /* .line 129 */
		 /* .local v0, "mtu":Lcom/android/net/module/util/netlink/StructNlAttr; */
		 (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
		 /* .line 131 */
	 } // .end local v0 # "mtu":Lcom/android/net/module/util/netlink/StructNlAttr;
} // :cond_0
v0 = this.mHardwareAddress;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 132 */
	 /* new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr; */
	 int v2 = 1; // const/4 v2, 0x1
	 /* invoke-direct {v1, v2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLandroid/net/MacAddress;)V */
	 /* move-object v0, v1 */
	 /* .line 133 */
	 /* .local v0, "hardwareAddress":Lcom/android/net/module/util/netlink/StructNlAttr; */
	 (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 135 */
} // .end local v0 # "hardwareAddress":Lcom/android/net/module/util/netlink/StructNlAttr;
} // :cond_1
v0 = this.mInterfaceName;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 136 */
/* new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr; */
int v2 = 3; // const/4 v2, 0x3
/* invoke-direct {v1, v2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/lang/String;)V */
/* move-object v0, v1 */
/* .line 137 */
/* .local v0, "ifname":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 139 */
} // .end local v0 # "ifname":Lcom/android/net/module/util/netlink/StructNlAttr;
} // :cond_2
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 143 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "RtNetlinkLinkMessage{ nlmsghdr{"; // const-string v1, "RtNetlinkLinkMessage{ nlmsghdr{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHeader;
/* .line 144 */
java.lang.Integer .valueOf ( v2 );
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, Ifinfomsg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIfinfomsg;
/* .line 145 */
(( com.android.net.module.util.netlink.StructIfinfoMsg ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfinfoMsg;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, Hardware Address{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHardwareAddress;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, MTU{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->mMtu:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, Ifname{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mInterfaceName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 143 */
} // .end method
