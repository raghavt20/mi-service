.class public Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "RtNetlinkAddressMessage.java"


# static fields
.field public static final IFA_ADDRESS:S = 0x1s

.field public static final IFA_CACHEINFO:S = 0x6s

.field public static final IFA_FLAGS:S = 0x8s


# instance fields
.field private mFlags:I

.field private mIfacacheInfo:Lcom/android/net/module/util/netlink/StructIfacacheInfo;

.field private mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

.field private mIpAddress:Ljava/net/InetAddress;


# direct methods
.method private constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V
    .locals 6
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 75
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Lcom/android/net/module/util/netlink/StructIfaddrMsg;Ljava/net/InetAddress;Lcom/android/net/module/util/netlink/StructIfacacheInfo;I)V

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Lcom/android/net/module/util/netlink/StructIfaddrMsg;Ljava/net/InetAddress;Lcom/android/net/module/util/netlink/StructIfacacheInfo;I)V
    .locals 0
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p2, "ifaddrMsg"    # Lcom/android/net/module/util/netlink/StructIfaddrMsg;
    .param p3, "ipAddress"    # Ljava/net/InetAddress;
    .param p4, "structIfacacheInfo"    # Lcom/android/net/module/util/netlink/StructIfacacheInfo;
    .param p5, "flags"    # I

    .line 68
    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 69
    iput-object p2, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    .line 70
    iput-object p3, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIpAddress:Ljava/net/InetAddress;

    .line 71
    iput-object p4, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfacacheInfo:Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    .line 72
    iput p5, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I

    .line 73
    return-void
.end method

.method private getRequiredSpace()I
    .locals 2

    .line 196
    const/16 v0, 0x18

    .line 198
    .local v0, "spaceRequired":I
    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIpAddress:Ljava/net/InetAddress;

    .line 199
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    .line 198
    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 201
    const/16 v1, 0x14

    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    add-int/lit8 v0, v0, 0x8

    .line 205
    return v0
.end method

.method public static newRtmNewAddressMessage(ILjava/net/InetAddress;SIBIJJ)[B
    .locals 22
    .param p0, "seqNo"    # I
    .param p1, "ip"    # Ljava/net/InetAddress;
    .param p2, "prefixlen"    # S
    .param p3, "flags"    # I
    .param p4, "scope"    # B
    .param p5, "ifIndex"    # I
    .param p6, "preferred"    # J
    .param p8, "valid"    # J

    .line 168
    move-object/from16 v0, p1

    const-string v1, "IP address to be set via netlink message cannot be null"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 170
    new-instance v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-direct {v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V

    .line 171
    .local v1, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    const/16 v2, 0x14

    iput-short v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    .line 172
    const/16 v2, 0x105

    iput-short v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S

    .line 173
    move/from16 v2, p0

    iput v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I

    .line 175
    new-instance v3, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;

    invoke-direct {v3, v1}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 177
    .local v3, "msg":Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;
    instance-of v4, v0, Ljava/net/Inet6Address;

    if-eqz v4, :cond_0

    sget v4, Landroid/system/OsConstants;->AF_INET6:I

    goto :goto_0

    :cond_0
    sget v4, Landroid/system/OsConstants;->AF_INET:I

    :goto_0
    int-to-byte v4, v4

    .line 180
    .local v4, "family":B
    new-instance v11, Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    int-to-short v6, v4

    const/4 v8, 0x0

    move/from16 v12, p4

    int-to-short v9, v12

    move-object v5, v11

    move/from16 v7, p2

    move/from16 v10, p5

    invoke-direct/range {v5 .. v10}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;-><init>(SSSSI)V

    iput-object v11, v3, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    .line 182
    iput-object v0, v3, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIpAddress:Ljava/net/InetAddress;

    .line 183
    new-instance v5, Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-object v13, v5

    move-wide/from16 v14, p6

    move-wide/from16 v16, p8

    invoke-direct/range {v13 .. v21}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;-><init>(JJJJ)V

    iput-object v5, v3, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfacacheInfo:Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    .line 185
    move/from16 v5, p3

    iput v5, v3, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I

    .line 187
    invoke-direct {v3}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->getRequiredSpace()I

    move-result v6

    new-array v6, v6, [B

    .line 188
    .local v6, "bytes":[B
    array-length v7, v6

    iput v7, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    .line 189
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 190
    .local v7, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 191
    invoke-virtual {v3, v7}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->pack(Ljava/nio/ByteBuffer;)V

    .line 192
    return-object v6
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;
    .locals 5
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 107
    new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;

    invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 109
    .local v0, "addrMsg":Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    move-result-object v1

    iput-object v1, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    .line 110
    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 113
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 114
    .local v1, "baseOffset":I
    const/4 v3, 0x1

    invoke-static {v3, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v3

    .line 115
    .local v3, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-nez v3, :cond_1

    return-object v2

    .line 116
    :cond_1
    invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;

    move-result-object v4

    iput-object v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIpAddress:Ljava/net/InetAddress;

    .line 117
    if-nez v4, :cond_2

    return-object v2

    .line 120
    :cond_2
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 121
    const/4 v4, 0x6

    invoke-static {v4, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v3

    .line 122
    if-eqz v3, :cond_3

    .line 123
    invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-static {v4}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    move-result-object v4

    iput-object v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfacacheInfo:Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    .line 127
    :cond_3
    iget-object v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    iget-short v4, v4, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->flags:S

    iput v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I

    .line 130
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 131
    const/16 v4, 0x8

    invoke-static {v4, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v3

    .line 132
    if-nez v3, :cond_4

    return-object v2

    .line 133
    :cond_4
    invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInteger()Ljava/lang/Integer;

    move-result-object v4

    .line 134
    .local v4, "value":Ljava/lang/Integer;
    if-nez v4, :cond_5

    return-object v2

    .line 135
    :cond_5
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I

    .line 137
    return-object v0
.end method


# virtual methods
.method public getFlags()I
    .locals 1

    .line 79
    iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I

    return v0
.end method

.method public getIfacacheInfo()Lcom/android/net/module/util/netlink/StructIfacacheInfo;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfacacheInfo:Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    return-object v0
.end method

.method public getIfaddrHeader()Lcom/android/net/module/util/netlink/StructIfaddrMsg;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    return-object v0
.end method

.method public getIpAddress()Ljava/net/InetAddress;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIpAddress:Ljava/net/InetAddress;

    return-object v0
.end method

.method protected pack(Ljava/nio/ByteBuffer;)V
    .locals 4
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 145
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 146
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 148
    new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIpAddress:Ljava/net/InetAddress;

    invoke-direct {v0, v1, v2}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V

    .line 149
    .local v0, "address":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 151
    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfacacheInfo:Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    if-eqz v1, :cond_0

    .line 152
    new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr;

    .line 153
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->writeToBytes()[B

    move-result-object v1

    const/4 v3, 0x6

    invoke-direct {v2, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[B)V

    move-object v1, v2

    .line 154
    .local v1, "cacheInfo":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 159
    .end local v1    # "cacheInfo":Lcom/android/net/module/util/netlink/StructNlAttr;
    :cond_0
    new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/16 v2, 0x8

    iget v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I

    invoke-direct {v1, v2, v3}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SI)V

    .line 160
    .local v1, "flags":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 161
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtNetlinkAddressMessage{ nlmsghdr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    sget v2, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    .line 211
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, Ifaddrmsg{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfaddrmsg:Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    .line 212
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, IP Address{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIpAddress:Ljava/net/InetAddress;

    .line 213
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, IfacacheInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 214
    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mIfacacheInfo:Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    if-nez v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, Address Flags{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I

    .line 215
    invoke-static {v1}, Lcom/android/net/module/util/HexDump;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "} }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    return-object v0
.end method
