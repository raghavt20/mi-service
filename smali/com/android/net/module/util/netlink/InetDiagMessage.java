public class com.android.net.module.util.netlink.InetDiagMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "InetDiagMessage.java" */
	 /* # static fields */
	 private static final FAMILY;
	 public static final java.lang.String TAG;
	 private static final Integer TIMEOUT_MS;
	 /* # instance fields */
	 public com.android.net.module.util.netlink.StructInetDiagMsg inetDiagMsg;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.InetDiagMessage ( ) {
		 /* .locals 2 */
		 /* .line 207 */
		 /* filled-new-array {v0, v1}, [I */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.InetDiagMessage ( ) {
		 /* .locals 1 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .line 160 */
		 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 161 */
		 /* new-instance v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg; */
		 /* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;-><init>()V */
		 this.inetDiagMsg = v0;
		 /* .line 162 */
		 return;
	 } // .end method
	 public static buildInetDiagReqForAliveTcpSockets ( Integer p0 ) {
		 /* .locals 8 */
		 /* .param p0, "family" # I */
		 /* .line 284 */
		 int v1 = 0; // const/4 v1, 0x0
		 int v2 = 0; // const/4 v2, 0x0
		 /* const/16 v4, 0x301 */
		 int v5 = 0; // const/4 v5, 0x0
		 int v6 = 2; // const/4 v6, 0x2
		 /* const/16 v7, 0xe */
		 /* move v3, p0 */
		 /* invoke-static/range {v0 ..v7}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISIII)[B */
	 } // .end method
	 private static void closeSocketQuietly ( java.io.FileDescriptor p0 ) {
		 /* .locals 1 */
		 /* .param p0, "fd" # Ljava/io/FileDescriptor; */
		 /* .line 180 */
		 try { // :try_start_0
			 android.net.util.SocketUtils .closeSocket ( p0 );
			 /* :try_end_0 */
			 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 182 */
			 /* .line 181 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 183 */
		 } // :goto_0
		 return;
	 } // .end method
	 public static Boolean containsUid ( com.android.net.module.util.netlink.InetDiagMessage p0, java.util.Set p1 ) {
		 /* .locals 3 */
		 /* .param p0, "msg" # Lcom/android/net/module/util/netlink/InetDiagMessage; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Lcom/android/net/module/util/netlink/InetDiagMessage;", */
		 /* "Ljava/util/Set<", */
		 /* "Landroid/util/Range<", */
		 /* "Ljava/lang/Integer;", */
		 /* ">;>;)Z" */
		 /* } */
	 } // .end annotation
	 /* .line 386 */
	 /* .local p1, "ranges":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Range<Ljava/lang/Integer;>;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* check-cast v1, Landroid/util/Range; */
	 /* .line 387 */
	 /* .local v1, "range":Landroid/util/Range;, "Landroid/util/Range<Ljava/lang/Integer;>;" */
	 v2 = this.inetDiagMsg;
	 /* iget v2, v2, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I */
	 java.lang.Integer .valueOf ( v2 );
	 v2 = 	 (( android.util.Range ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/Range;->contains(Ljava/lang/Comparable;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 388 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 390 */
	 } // .end local v1 # "range":Landroid/util/Range;, "Landroid/util/Range<Ljava/lang/Integer;>;"
} // :cond_0
/* .line 391 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static void destroyLiveTcpSockets ( java.util.Set p0, java.util.Set p1 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Landroid/util/Range<", */
/* "Ljava/lang/Integer;", */
/* ">;>;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/net/SocketException;, */
/* Ljava/io/InterruptedIOException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 465 */
/* .local p0, "ranges":Ljava/util/Set;, "Ljava/util/Set<Landroid/util/Range<Ljava/lang/Integer;>;>;" */
/* .local p1, "exemptUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 466 */
/* .local v0, "startTimeMs":J */
/* new-instance v3, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p1, p0}, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda0;-><init>(Ljava/util/Set;Ljava/util/Set;)V */
/* const/16 v4, 0xe */
com.android.net.module.util.netlink.InetDiagMessage .destroySockets ( v2,v4,v3 );
/* .line 471 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* sub-long/2addr v2, v0 */
/* .line 472 */
/* .local v2, "durationMs":J */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Destroyed live tcp sockets for uids="; // const-string v5, "Destroyed live tcp sockets for uids="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p0 ); // invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " exemptUids="; // const-string v5, " exemptUids="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " in "; // const-string v5, " in "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "ms"; // const-string v5, "ms"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "InetDiagMessage"; // const-string v5, "InetDiagMessage"
android.util.Log .d ( v5,v4 );
/* .line 474 */
return;
} // .end method
public static void destroyLiveTcpSocketsByOwnerUids ( java.util.Set p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/net/SocketException;, */
/* Ljava/io/InterruptedIOException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 487 */
/* .local p0, "ownerUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 488 */
/* .local v0, "startTimeMs":J */
/* new-instance v3, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda1; */
/* invoke-direct {v3, p0}, Lcom/android/net/module/util/netlink/InetDiagMessage$$ExternalSyntheticLambda1;-><init>(Ljava/util/Set;)V */
/* const/16 v4, 0xe */
com.android.net.module.util.netlink.InetDiagMessage .destroySockets ( v2,v4,v3 );
/* .line 492 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* sub-long/2addr v2, v0 */
/* .line 493 */
/* .local v2, "durationMs":J */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Destroyed live tcp sockets for uids="; // const-string v5, "Destroyed live tcp sockets for uids="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p0 ); // invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v5 = " in "; // const-string v5, " in "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2, v3 ); // invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = "ms"; // const-string v5, "ms"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "InetDiagMessage"; // const-string v5, "InetDiagMessage"
android.util.Log .d ( v5,v4 );
/* .line 494 */
return;
} // .end method
private static void destroySockets ( Integer p0, Integer p1, java.util.function.Predicate p2 ) {
/* .locals 8 */
/* .param p0, "proto" # I */
/* .param p1, "states" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(II", */
/* "Ljava/util/function/Predicate<", */
/* "Lcom/android/net/module/util/netlink/InetDiagMessage;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/net/SocketException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 423 */
/* .local p2, "filter":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/net/module/util/netlink/InetDiagMessage;>;" */
final String v0 = "InetDiagMessage"; // const-string v0, "InetDiagMessage"
int v1 = 0; // const/4 v1, 0x0
/* .line 424 */
/* .local v1, "dumpFd":Ljava/io/FileDescriptor; */
int v2 = 0; // const/4 v2, 0x0
/* .line 427 */
/* .local v2, "destroyFd":Ljava/io/FileDescriptor; */
try { // :try_start_0
com.android.net.module.util.netlink.NetlinkUtils .createNetLinkInetDiagSocket ( );
/* move-object v1, v3 */
/* .line 428 */
com.android.net.module.util.netlink.NetlinkUtils .createNetLinkInetDiagSocket ( );
/* move-object v2, v3 */
/* .line 429 */
com.android.net.module.util.netlink.NetlinkUtils .connectSocketToNetlink ( v1 );
/* .line 430 */
com.android.net.module.util.netlink.NetlinkUtils .connectSocketToNetlink ( v2 );
/* .line 432 */
java.lang.Integer .valueOf ( v3 );
java.lang.Integer .valueOf ( v4 );
java.util.List .of ( v3,v4 );
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 434 */
/* .local v4, "family":I */
try { // :try_start_1
com.android.net.module.util.netlink.InetDiagMessage .sendNetlinkDumpRequest ( v1,p0,p1,v4 );
/* :try_end_1 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 438 */
/* nop */
/* .line 439 */
try { // :try_start_2
v5 = com.android.net.module.util.netlink.InetDiagMessage .processNetlinkDumpAndDestroySockets ( v1,v2,p0,p2 );
/* .line 441 */
/* .local v5, "destroyedSockets":I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Destroyed "; // const-string v7, "Destroyed "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " sockets, proto="; // const-string v7, " sockets, proto="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 442 */
com.android.net.module.util.netlink.NetlinkConstants .stringForProtocol ( p0 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", family="; // const-string v7, ", family="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 443 */
com.android.net.module.util.netlink.NetlinkConstants .stringForAddressFamily ( v4 );
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ", states="; // const-string v7, ", states="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 441 */
android.util.Log .d ( v0,v6 );
/* .line 445 */
/* nop */
} // .end local v4 # "family":I
} // .end local v5 # "destroyedSockets":I
/* .line 435 */
/* .restart local v4 # "family":I */
/* :catch_0 */
/* move-exception v5 */
/* .line 436 */
/* .local v5, "e":Ljava/lang/Exception; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Failed to send netlink dump request: "; // const-string v7, "Failed to send netlink dump request: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v6 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 437 */
/* .line 447 */
} // .end local v4 # "family":I
} // .end local v5 # "e":Ljava/lang/Exception;
} // :cond_0
com.android.net.module.util.netlink.InetDiagMessage .closeSocketQuietly ( v1 );
/* .line 448 */
com.android.net.module.util.netlink.InetDiagMessage .closeSocketQuietly ( v2 );
/* .line 449 */
/* nop */
/* .line 450 */
return;
/* .line 447 */
/* :catchall_0 */
/* move-exception v0 */
com.android.net.module.util.netlink.InetDiagMessage .closeSocketQuietly ( v1 );
/* .line 448 */
com.android.net.module.util.netlink.InetDiagMessage .closeSocketQuietly ( v2 );
/* .line 449 */
/* throw v0 */
} // .end method
public static Integer getConnectionOwnerUid ( Integer p0, java.net.InetSocketAddress p1, java.net.InetSocketAddress p2 ) {
/* .locals 5 */
/* .param p0, "protocol" # I */
/* .param p1, "local" # Ljava/net/InetSocketAddress; */
/* .param p2, "remote" # Ljava/net/InetSocketAddress; */
/* .line 265 */
int v0 = -1; // const/4 v0, -0x1
/* .line 266 */
/* .local v0, "uid":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 268 */
/* .local v1, "fd":Ljava/io/FileDescriptor; */
try { // :try_start_0
com.android.net.module.util.netlink.NetlinkUtils .netlinkSocketForProto ( v2 );
/* move-object v1, v2 */
/* .line 269 */
com.android.net.module.util.netlink.NetlinkUtils .connectSocketToNetlink ( v1 );
/* .line 270 */
v2 = com.android.net.module.util.netlink.InetDiagMessage .lookupUid ( p0,p1,p2,v1 );
/* :try_end_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v0, v2 */
/* .line 275 */
/* nop */
} // :goto_0
com.android.net.module.util.netlink.InetDiagMessage .closeSocketQuietly ( v1 );
/* .line 276 */
/* .line 275 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 271 */
/* :catch_0 */
/* move-exception v2 */
/* .line 273 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v3 = "InetDiagMessage"; // const-string v3, "InetDiagMessage"
(( java.lang.Exception ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 275 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
/* .line 277 */
} // :goto_1
/* .line 275 */
} // :goto_2
com.android.net.module.util.netlink.InetDiagMessage .closeSocketQuietly ( v1 );
/* .line 276 */
/* throw v2 */
} // .end method
public static inetDiagReqV2 ( Integer p0, com.android.net.module.util.netlink.StructInetDiagSockId p1, Integer p2, Object p3, Object p4, Integer p5, Integer p6, Integer p7 ) {
/* .locals 13 */
/* .param p0, "protocol" # I */
/* .param p1, "id" # Lcom/android/net/module/util/netlink/StructInetDiagSockId; */
/* .param p2, "family" # I */
/* .param p3, "type" # S */
/* .param p4, "flags" # S */
/* .param p5, "pad" # I */
/* .param p6, "idiagExt" # I */
/* .param p7, "state" # I */
/* .line 140 */
/* const/16 v0, 0x48 */
/* new-array v0, v0, [B */
/* .line 141 */
/* .local v0, "bytes":[B */
java.nio.ByteBuffer .wrap ( v0 );
/* .line 142 */
/* .local v1, "byteBuffer":Ljava/nio/ByteBuffer; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v1 ).order ( v2 ); // invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 144 */
/* new-instance v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* invoke-direct {v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V */
/* .line 145 */
/* .local v2, "nlMsgHdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* array-length v3, v0 */
/* iput v3, v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
/* .line 146 */
/* move/from16 v3, p3 */
/* iput-short v3, v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* .line 147 */
/* move/from16 v4, p4 */
/* iput-short v4, v2, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S */
/* .line 148 */
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v2 ).pack ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 149 */
/* new-instance v12, Lcom/android/net/module/util/netlink/StructInetDiagReqV2; */
/* move-object v5, v12 */
/* move v6, p0 */
/* move-object v7, p1 */
/* move v8, p2 */
/* move/from16 v9, p5 */
/* move/from16 v10, p6 */
/* move/from16 v11, p7 */
/* invoke-direct/range {v5 ..v11}, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;-><init>(ILcom/android/net/module/util/netlink/StructInetDiagSockId;IIII)V */
/* .line 152 */
/* .local v5, "inetDiagReqV2":Lcom/android/net/module/util/netlink/StructInetDiagReqV2; */
(( com.android.net.module.util.netlink.StructInetDiagReqV2 ) v5 ).pack ( v1 ); // invoke-virtual {v5, v1}, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->pack(Ljava/nio/ByteBuffer;)V
/* .line 153 */
} // .end method
public static inetDiagReqV2 ( Integer p0, java.net.InetSocketAddress p1, java.net.InetSocketAddress p2, Integer p3, Object p4 ) {
/* .locals 8 */
/* .param p0, "protocol" # I */
/* .param p1, "local" # Ljava/net/InetSocketAddress; */
/* .param p2, "remote" # Ljava/net/InetSocketAddress; */
/* .param p3, "family" # I */
/* .param p4, "flags" # S */
/* .line 83 */
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = -1; // const/4 v7, -0x1
/* move v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move v3, p3 */
/* move v4, p4 */
/* invoke-static/range {v0 ..v7}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISIII)[B */
} // .end method
public static inetDiagReqV2 ( Integer p0, java.net.InetSocketAddress p1, java.net.InetSocketAddress p2, Integer p3, Object p4, Integer p5, Integer p6, Integer p7 ) {
/* .locals 11 */
/* .param p0, "protocol" # I */
/* .param p1, "local" # Ljava/net/InetSocketAddress; */
/* .param p2, "remote" # Ljava/net/InetSocketAddress; */
/* .param p3, "family" # I */
/* .param p4, "flags" # S */
/* .param p5, "pad" # I */
/* .param p6, "idiagExt" # I */
/* .param p7, "state" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/lang/IllegalArgumentException; */
/* } */
} // .end annotation
/* .line 113 */
/* move-object v0, p1 */
/* move-object v1, p2 */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
/* if-nez v0, :cond_0 */
/* move v4, v2 */
} // :cond_0
/* move v4, v3 */
} // :goto_0
/* if-nez v1, :cond_1 */
} // :cond_1
/* move v2, v3 */
} // :goto_1
/* if-ne v4, v2, :cond_3 */
/* .line 117 */
if ( v0 != null) { // if-eqz v0, :cond_2
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 118 */
/* new-instance v2, Lcom/android/net/module/util/netlink/StructInetDiagSockId; */
/* invoke-direct {v2, p1, p2}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;-><init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;)V */
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
} // :goto_2
/* move-object v4, v2 */
/* .line 119 */
/* .local v4, "id":Lcom/android/net/module/util/netlink/StructInetDiagSockId; */
/* const/16 v6, 0x14 */
/* move v3, p0 */
/* move v5, p3 */
/* move v7, p4 */
/* move/from16 v8, p5 */
/* move/from16 v9, p6 */
/* move/from16 v10, p7 */
/* invoke-static/range {v3 ..v10}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILcom/android/net/module/util/netlink/StructInetDiagSockId;ISSIII)[B */
/* .line 114 */
} // .end local v4 # "id":Lcom/android/net/module/util/netlink/StructInetDiagSockId;
} // :cond_3
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
final String v3 = "Local and remote must be both null or both non-null"; // const-string v3, "Local and remote must be both null or both non-null"
/* invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
} // .end method
public static Boolean isAdbSocket ( com.android.net.module.util.netlink.InetDiagMessage p0 ) {
/* .locals 2 */
/* .param p0, "msg" # Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* .line 378 */
v0 = this.inetDiagMsg;
/* iget v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I */
/* const/16 v1, 0x7d0 */
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean isLoopback ( com.android.net.module.util.netlink.InetDiagMessage p0 ) {
/* .locals 3 */
/* .param p0, "msg" # Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* .line 414 */
v0 = this.inetDiagMsg;
v0 = this.id;
v0 = this.locSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
/* .line 415 */
/* .local v0, "srcAddr":Ljava/net/InetAddress; */
v1 = this.inetDiagMsg;
v1 = this.id;
v1 = this.remSocketAddress;
(( java.net.InetSocketAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
/* .line 416 */
/* .local v1, "dstAddr":Ljava/net/InetAddress; */
v2 = com.android.net.module.util.netlink.InetDiagMessage .isLoopbackAddress ( v0 );
/* if-nez v2, :cond_1 */
/* .line 417 */
v2 = com.android.net.module.util.netlink.InetDiagMessage .isLoopbackAddress ( v1 );
/* if-nez v2, :cond_1 */
/* .line 418 */
v2 = (( java.net.InetAddress ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :cond_1
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
/* .line 416 */
} // :goto_1
} // .end method
private static Boolean isLoopbackAddress ( java.net.InetAddress p0 ) {
/* .locals 5 */
/* .param p0, "addr" # Ljava/net/InetAddress; */
/* .line 395 */
v0 = (( java.net.InetAddress ) p0 ).isLoopbackAddress ( ); // invoke-virtual {p0}, Ljava/net/InetAddress;->isLoopbackAddress()Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 396 */
} // :cond_0
/* instance-of v0, p0, Ljava/net/Inet6Address; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_1 */
/* .line 400 */
} // :cond_1
(( java.net.InetAddress ) p0 ).getAddress ( ); // invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B
/* .line 401 */
/* .local v0, "addrBytes":[B */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* const/16 v4, 0xa */
/* if-ge v3, v4, :cond_3 */
/* .line 402 */
/* aget-byte v4, v0, v3 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 401 */
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 404 */
} // .end local v3 # "i":I
} // :cond_3
/* aget-byte v3, v0, v4 */
int v4 = -1; // const/4 v4, -0x1
/* if-ne v3, v4, :cond_4 */
/* const/16 v3, 0xb */
/* aget-byte v3, v0, v3 */
/* if-ne v3, v4, :cond_4 */
/* const/16 v3, 0xc */
/* aget-byte v3, v0, v3 */
/* const/16 v4, 0x7f */
/* if-ne v3, v4, :cond_4 */
} // :cond_4
/* move v1, v2 */
} // :goto_1
} // .end method
static Boolean lambda$destroyLiveTcpSockets$0 ( java.util.Set p0, java.util.Set p1, com.android.net.module.util.netlink.InetDiagMessage p2 ) { //synthethic
/* .locals 1 */
/* .param p0, "exemptUids" # Ljava/util/Set; */
/* .param p1, "ranges" # Ljava/util/Set; */
/* .param p2, "diagMsg" # Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* .line 467 */
v0 = this.inetDiagMsg;
/* iget v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I */
v0 = java.lang.Integer .valueOf ( v0 );
/* if-nez v0, :cond_0 */
/* .line 468 */
v0 = com.android.net.module.util.netlink.InetDiagMessage .containsUid ( p2,p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 469 */
v0 = com.android.net.module.util.netlink.InetDiagMessage .isLoopback ( p2 );
/* if-nez v0, :cond_0 */
/* .line 470 */
v0 = com.android.net.module.util.netlink.InetDiagMessage .isAdbSocket ( p2 );
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 467 */
} // :goto_0
} // .end method
static Boolean lambda$destroyLiveTcpSocketsByOwnerUids$1 ( java.util.Set p0, com.android.net.module.util.netlink.InetDiagMessage p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "ownerUids" # Ljava/util/Set; */
/* .param p1, "diagMsg" # Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* .line 489 */
v0 = this.inetDiagMsg;
/* iget v0, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I */
v0 = java.lang.Integer .valueOf ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 490 */
v0 = com.android.net.module.util.netlink.InetDiagMessage .isLoopback ( p1 );
/* if-nez v0, :cond_0 */
/* .line 491 */
v0 = com.android.net.module.util.netlink.InetDiagMessage .isAdbSocket ( p1 );
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 489 */
} // :goto_0
} // .end method
private static Integer lookupUid ( Integer p0, java.net.InetSocketAddress p1, java.net.InetSocketAddress p2, java.io.FileDescriptor p3 ) {
/* .locals 13 */
/* .param p0, "protocol" # I */
/* .param p1, "local" # Ljava/net/InetSocketAddress; */
/* .param p2, "remote" # Ljava/net/InetSocketAddress; */
/* .param p3, "fd" # Ljava/io/FileDescriptor; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 214 */
/* move v7, p0 */
v0 = com.android.net.module.util.netlink.InetDiagMessage.FAMILY;
/* array-length v8, v0 */
int v9 = 0; // const/4 v9, 0x0
/* move v10, v9 */
} // :goto_0
int v11 = -1; // const/4 v11, -0x1
/* if-ge v10, v8, :cond_2 */
/* aget v12, v0, v10 */
/* .line 220 */
/* .local v12, "family":I */
/* if-ne v7, v1, :cond_0 */
/* .line 221 */
int v5 = 1; // const/4 v5, 0x1
/* move v1, p0 */
/* move-object v2, p2 */
/* move-object v3, p1 */
/* move v4, v12 */
/* move-object/from16 v6, p3 */
v1 = /* invoke-static/range {v1 ..v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I */
/* .local v1, "uid":I */
/* .line 223 */
} // .end local v1 # "uid":I
} // :cond_0
int v5 = 1; // const/4 v5, 0x1
/* move v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move v4, v12 */
/* move-object/from16 v6, p3 */
v1 = /* invoke-static/range {v1 ..v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I */
/* .line 225 */
/* .restart local v1 # "uid":I */
} // :goto_1
/* if-eq v1, v11, :cond_1 */
/* .line 226 */
/* .line 214 */
} // .end local v12 # "family":I
} // :cond_1
/* add-int/lit8 v10, v10, 0x1 */
/* .line 237 */
} // .end local v1 # "uid":I
} // :cond_2
/* if-ne v7, v0, :cond_5 */
/* .line 239 */
try { // :try_start_0
/* new-instance v3, Ljava/net/InetSocketAddress; */
final String v0 = "::"; // const-string v0, "::"
/* .line 240 */
java.net.Inet6Address .getByName ( v0 );
/* invoke-direct {v3, v0, v9}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V */
/* .line 241 */
/* .local v3, "wildcard":Ljava/net/InetSocketAddress; */
/* const/16 v5, 0x301 */
/* move v1, p0 */
/* move-object v2, p1 */
/* move-object/from16 v6, p3 */
v0 = /* invoke-static/range {v1 ..v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I */
/* .line 243 */
/* .local v0, "uid":I */
/* if-eq v0, v11, :cond_3 */
/* .line 244 */
/* .line 246 */
} // :cond_3
/* new-instance v1, Ljava/net/InetSocketAddress; */
final String v2 = "0.0.0.0"; // const-string v2, "0.0.0.0"
java.net.Inet4Address .getByName ( v2 );
/* invoke-direct {v1, v2, v9}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V */
/* move-object v3, v1 */
/* .line 247 */
/* const/16 v5, 0x301 */
/* move v1, p0 */
/* move-object v2, p1 */
/* move-object/from16 v6, p3 */
v1 = /* invoke-static/range {v1 ..v6}, Lcom/android/net/module/util/netlink/InetDiagMessage;->lookupUidByFamily(ILjava/net/InetSocketAddress;Ljava/net/InetSocketAddress;ISLjava/io/FileDescriptor;)I */
/* :try_end_0 */
/* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v1 */
/* .line 249 */
/* if-eq v0, v11, :cond_4 */
/* .line 250 */
/* .line 254 */
} // .end local v3 # "wildcard":Ljava/net/InetSocketAddress;
} // :cond_4
/* .line 252 */
} // .end local v0 # "uid":I
/* :catch_0 */
/* move-exception v0 */
/* .line 253 */
/* .local v0, "e":Ljava/net/UnknownHostException; */
final String v1 = "InetDiagMessage"; // const-string v1, "InetDiagMessage"
(( java.net.UnknownHostException ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/net/UnknownHostException;->toString()Ljava/lang/String;
android.util.Log .e ( v1,v2 );
/* .line 256 */
} // .end local v0 # "e":Ljava/net/UnknownHostException;
} // :cond_5
} // :goto_2
} // .end method
private static Integer lookupUidByFamily ( Integer p0, java.net.InetSocketAddress p1, java.net.InetSocketAddress p2, Integer p3, Object p4, java.io.FileDescriptor p5 ) {
/* .locals 7 */
/* .param p0, "protocol" # I */
/* .param p1, "local" # Ljava/net/InetSocketAddress; */
/* .param p2, "remote" # Ljava/net/InetSocketAddress; */
/* .param p3, "family" # I */
/* .param p4, "flags" # S */
/* .param p5, "fd" # Ljava/io/FileDescriptor; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 189 */
com.android.net.module.util.netlink.InetDiagMessage .inetDiagReqV2 ( p0,p1,p2,p3,p4 );
/* .line 190 */
/* .local v6, "msg":[B */
int v2 = 0; // const/4 v2, 0x0
/* array-length v3, v6 */
/* const-wide/16 v4, 0x1f4 */
/* move-object v0, p5 */
/* move-object v1, v6 */
/* invoke-static/range {v0 ..v5}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I */
/* .line 191 */
/* const/16 v0, 0x2000 */
/* const-wide/16 v1, 0x1f4 */
com.android.net.module.util.netlink.NetlinkUtils .recvMessage ( p5,v0,v1,v2 );
/* .line 193 */
/* .local v0, "response":Ljava/nio/ByteBuffer; */
com.android.net.module.util.netlink.NetlinkMessage .parse ( v0,v1 );
/* .line 194 */
/* .local v1, "nlMsg":Lcom/android/net/module/util/netlink/NetlinkMessage; */
int v2 = -1; // const/4 v2, -0x1
/* if-nez v1, :cond_0 */
/* .line 195 */
/* .line 197 */
} // :cond_0
(( com.android.net.module.util.netlink.NetlinkMessage ) v1 ).getHeader ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
/* .line 198 */
/* .local v3, "hdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* iget-short v4, v3, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
int v5 = 3; // const/4 v5, 0x3
/* if-ne v4, v5, :cond_1 */
/* .line 199 */
/* .line 201 */
} // :cond_1
/* instance-of v4, v1, Lcom/android/net/module/util/netlink/InetDiagMessage; */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 202 */
/* move-object v2, v1 */
/* check-cast v2, Lcom/android/net/module/util/netlink/InetDiagMessage; */
v2 = this.inetDiagMsg;
/* iget v2, v2, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I */
/* .line 204 */
} // :cond_2
} // .end method
public static com.android.net.module.util.netlink.InetDiagMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 2 */
/* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 170 */
/* new-instance v0, Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/InetDiagMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
/* .line 171 */
/* .local v0, "msg":Lcom/android/net/module/util/netlink/InetDiagMessage; */
com.android.net.module.util.netlink.StructInetDiagMsg .parse ( p1 );
this.inetDiagMsg = v1;
/* .line 172 */
/* if-nez v1, :cond_0 */
/* .line 173 */
int v1 = 0; // const/4 v1, 0x0
/* .line 175 */
} // :cond_0
} // .end method
private static Integer processNetlinkDumpAndDestroySockets ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, Integer p2, java.util.function.Predicate p3 ) {
/* .locals 9 */
/* .param p0, "dumpFd" # Ljava/io/FileDescriptor; */
/* .param p1, "destroyFd" # Ljava/io/FileDescriptor; */
/* .param p2, "proto" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/io/FileDescriptor;", */
/* "Ljava/io/FileDescriptor;", */
/* "I", */
/* "Ljava/util/function/Predicate<", */
/* "Lcom/android/net/module/util/netlink/InetDiagMessage;", */
/* ">;)I" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/InterruptedIOException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 327 */
/* .local p3, "filter":Ljava/util/function/Predicate;, "Ljava/util/function/Predicate<Lcom/android/net/module/util/netlink/InetDiagMessage;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 330 */
/* .local v0, "destroyedSockets":I */
} // :goto_0
/* const/16 v1, 0x2000 */
/* const-wide/16 v2, 0x12c */
com.android.net.module.util.netlink.NetlinkUtils .recvMessage ( p0,v1,v2,v3 );
/* .line 333 */
/* .local v1, "buf":Ljava/nio/ByteBuffer; */
} // :goto_1
v2 = (( java.nio.ByteBuffer ) v1 ).remaining ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I
/* if-lez v2, :cond_5 */
/* .line 334 */
v2 = (( java.nio.ByteBuffer ) v1 ).position ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I
/* .line 335 */
/* .local v2, "position":I */
com.android.net.module.util.netlink.NetlinkMessage .parse ( v1,v3 );
/* .line 336 */
/* .local v3, "nlMsg":Lcom/android/net/module/util/netlink/NetlinkMessage; */
final String v4 = "InetDiagMessage"; // const-string v4, "InetDiagMessage"
/* if-nez v3, :cond_0 */
/* .line 338 */
(( java.nio.ByteBuffer ) v1 ).position ( v2 ); // invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 339 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Failed to parse netlink message: "; // const-string v6, "Failed to parse netlink message: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.net.module.util.netlink.NetlinkConstants .hexify ( v1 );
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v5 );
/* .line 340 */
/* .line 343 */
} // :cond_0
(( com.android.net.module.util.netlink.NetlinkMessage ) v3 ).getHeader ( ); // invoke-virtual {v3}, Lcom/android/net/module/util/netlink/NetlinkMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
/* iget-short v5, v5, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
int v6 = 3; // const/4 v6, 0x3
/* if-ne v5, v6, :cond_1 */
/* .line 344 */
/* .line 347 */
} // :cond_1
/* instance-of v5, v3, Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* if-nez v5, :cond_2 */
/* .line 348 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Received unexpected netlink message: "; // const-string v6, "Received unexpected netlink message: "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .wtf ( v4,v5 );
/* .line 349 */
/* .line 352 */
} // :cond_2
/* move-object v5, v3 */
/* check-cast v5, Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* .line 353 */
v6 = /* .local v5, "diagMsg":Lcom/android/net/module/util/netlink/InetDiagMessage; */
if ( v6 != null) { // if-eqz v6, :cond_4
/* .line 355 */
try { // :try_start_0
com.android.net.module.util.netlink.InetDiagMessage .sendNetlinkDestroyRequest ( p1,p2,v5 );
/* :try_end_0 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/system/ErrnoException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 356 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 362 */
/* .line 357 */
/* :catch_0 */
/* move-exception v6 */
/* .line 358 */
/* .local v6, "e":Ljava/lang/Exception; */
/* instance-of v7, v6, Landroid/system/ErrnoException; */
if ( v7 != null) { // if-eqz v7, :cond_3
/* move-object v7, v6 */
/* check-cast v7, Landroid/system/ErrnoException; */
/* iget v7, v7, Landroid/system/ErrnoException;->errno:I */
/* if-eq v7, v8, :cond_4 */
/* .line 360 */
} // :cond_3
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Failed to destroy socket: diagMsg="; // const-string v8, "Failed to destroy socket: diagMsg="
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v8 = ", "; // const-string v8, ", "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v4,v7 );
/* .line 364 */
} // .end local v2 # "position":I
} // .end local v3 # "nlMsg":Lcom/android/net/module/util/netlink/NetlinkMessage;
} // .end local v5 # "diagMsg":Lcom/android/net/module/util/netlink/InetDiagMessage;
} // .end local v6 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_2
/* goto/16 :goto_1 */
/* .line 365 */
} // .end local v1 # "buf":Ljava/nio/ByteBuffer;
} // :cond_5
} // :goto_3
/* goto/16 :goto_0 */
} // .end method
private static void sendNetlinkDestroyRequest ( java.io.FileDescriptor p0, Integer p1, com.android.net.module.util.netlink.InetDiagMessage p2 ) {
/* .locals 16 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .param p1, "proto" # I */
/* .param p2, "diagMsg" # Lcom/android/net/module/util/netlink/InetDiagMessage; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/InterruptedIOException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 296 */
/* move-object/from16 v0, p2 */
v1 = this.inetDiagMsg;
v3 = this.id;
v1 = this.inetDiagMsg;
/* iget-short v4, v1, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S */
/* const/16 v5, 0x15 */
int v6 = 5; // const/4 v6, 0x5
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
v1 = this.inetDiagMsg;
/* iget-short v1, v1, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_state:S */
int v2 = 1; // const/4 v2, 0x1
/* shl-int v9, v2, v1 */
/* move/from16 v2, p1 */
/* invoke-static/range {v2 ..v9}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILcom/android/net/module/util/netlink/StructInetDiagSockId;ISSIII)[B */
/* .line 306 */
/* .local v1, "destroyMsg":[B */
int v12 = 0; // const/4 v12, 0x0
/* array-length v13, v1 */
/* const-wide/16 v14, 0x12c */
/* move-object/from16 v10, p0 */
/* move-object v11, v1 */
/* invoke-static/range {v10 ..v15}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I */
/* .line 307 */
/* invoke-static/range {p0 ..p0}, Lcom/android/net/module/util/netlink/NetlinkUtils;->receiveNetlinkAck(Ljava/io/FileDescriptor;)V */
/* .line 308 */
return;
} // .end method
private static void sendNetlinkDumpRequest ( java.io.FileDescriptor p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 13 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .param p1, "proto" # I */
/* .param p2, "states" # I */
/* .param p3, "family" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/InterruptedIOException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 312 */
int v1 = 0; // const/4 v1, 0x0
/* const/16 v3, 0x14 */
/* const/16 v4, 0x301 */
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move v0, p1 */
/* move/from16 v2, p3 */
/* move v7, p2 */
/* invoke-static/range {v0 ..v7}, Lcom/android/net/module/util/netlink/InetDiagMessage;->inetDiagReqV2(ILcom/android/net/module/util/netlink/StructInetDiagSockId;ISSIII)[B */
/* .line 321 */
/* .local v0, "dumpMsg":[B */
int v9 = 0; // const/4 v9, 0x0
/* array-length v10, v0 */
/* const-wide/16 v11, 0x12c */
/* move-object v7, p0 */
/* move-object v8, v0 */
/* invoke-static/range {v7 ..v12}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I */
/* .line 322 */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 498 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "InetDiagMessage{ nlmsghdr{"; // const-string v1, "InetDiagMessage{ nlmsghdr{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 500 */
v1 = this.mHeader;
final String v2 = ""; // const-string v2, ""
/* if-nez v1, :cond_0 */
/* move-object v1, v2 */
} // :cond_0
v1 = this.mHeader;
java.lang.Integer .valueOf ( v3 );
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, inet_diag_msg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 502 */
v1 = this.inetDiagMsg;
/* if-nez v1, :cond_1 */
} // :cond_1
(( com.android.net.module.util.netlink.StructInetDiagMsg ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->toString()Ljava/lang/String;
} // :goto_1
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 498 */
} // .end method
