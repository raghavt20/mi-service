public class com.android.net.module.util.netlink.NetlinkUtils {
	 /* .source "NetlinkUtils.java" */
	 /* # static fields */
	 public static final Integer DEFAULT_RECV_BUFSIZE;
	 public static final Integer INET_DIAG_INFO;
	 public static final Integer INET_DIAG_MARK;
	 public static final Integer INIT_MARK_VALUE;
	 public static final Long IO_TIMEOUT_MS;
	 public static final Integer NULL_MASK;
	 public static final Integer SOCKET_RECV_BUFSIZE;
	 private static final java.lang.String TAG;
	 public static final Integer TCP_ALIVE_STATE_FILTER;
	 private static final Integer TCP_ESTABLISHED;
	 private static final Integer TCP_SYN_RECV;
	 private static final Integer TCP_SYN_SENT;
	 public static final Integer UNKNOWN_MARK;
	 /* # direct methods */
	 private com.android.net.module.util.netlink.NetlinkUtils ( ) {
		 /* .locals 0 */
		 /* .line 236 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static void checkTimeout ( Long p0 ) {
		 /* .locals 2 */
		 /* .param p0, "timeoutMs" # J */
		 /* .line 194 */
		 /* const-wide/16 v0, 0x0 */
		 /* cmp-long v0, p0, v0 */
		 /* if-ltz v0, :cond_0 */
		 /* .line 197 */
		 return;
		 /* .line 195 */
	 } // :cond_0
	 /* new-instance v0, Ljava/lang/IllegalArgumentException; */
	 final String v1 = "Negative timeouts not permitted"; // const-string v1, "Negative timeouts not permitted"
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
public static void connectSocketToNetlink ( java.io.FileDescriptor p0 ) {
	 /* .locals 1 */
	 /* .param p0, "fd" # Ljava/io/FileDescriptor; */
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/system/ErrnoException;, */
	 /* Ljava/net/SocketException; */
	 /* } */
} // .end annotation
/* .line 190 */
int v0 = 0; // const/4 v0, 0x0
android.net.util.SocketUtils .makeNetlinkSocketAddress ( v0,v0 );
android.system.Os .connect ( p0,v0 );
/* .line 191 */
return;
} // .end method
public static java.io.FileDescriptor createNetLinkInetDiagSocket ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 177 */
/* or-int/2addr v1, v2 */
android.system.Os .socket ( v0,v1,v2 );
} // .end method
public static Boolean enoughBytesRemainForValidNlMsg ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p0, "bytes" # Ljava/nio/ByteBuffer; */
/* .line 81 */
v0 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v1, 0x10 */
/* if-lt v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static java.io.FileDescriptor netlinkSocketForProto ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "nlProto" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 168 */
android.system.Os .socket ( v0,v1,p0 );
/* .line 169 */
/* .local v0, "fd":Ljava/io/FileDescriptor; */
/* const/high16 v3, 0x10000 */
android.system.Os .setsockoptInt ( v0,v1,v2,v3 );
/* .line 170 */
} // .end method
private static com.android.net.module.util.netlink.NetlinkErrorMessage parseNetlinkErrorMessage ( java.nio.ByteBuffer p0 ) {
/* .locals 3 */
/* .param p0, "bytes" # Ljava/nio/ByteBuffer; */
/* .line 92 */
com.android.net.module.util.netlink.StructNlMsgHdr .parse ( p0 );
/* .line 93 */
/* .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
int v2 = 2; // const/4 v2, 0x2
/* if-eq v1, v2, :cond_0 */
/* .line 96 */
} // :cond_0
com.android.net.module.util.netlink.NetlinkErrorMessage .parse ( v0,p0 );
/* .line 94 */
} // :cond_1
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public static void receiveNetlinkAck ( java.io.FileDescriptor p0 ) {
/* .locals 6 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/InterruptedIOException;, */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 106 */
/* const/16 v0, 0x2000 */
/* const-wide/16 v1, 0x12c */
com.android.net.module.util.netlink.NetlinkUtils .recvMessage ( p0,v0,v1,v2 );
/* .line 108 */
/* .local v0, "bytes":Ljava/nio/ByteBuffer; */
com.android.net.module.util.netlink.NetlinkUtils .parseNetlinkErrorMessage ( v0 );
/* .line 109 */
/* .local v1, "response":Lcom/android/net/module/util/netlink/NetlinkErrorMessage; */
final String v2 = "receiveNetlinkAck, errmsg="; // const-string v2, "receiveNetlinkAck, errmsg="
final String v3 = "NetlinkUtils"; // const-string v3, "NetlinkUtils"
if ( v1 != null) { // if-eqz v1, :cond_1
(( com.android.net.module.util.netlink.NetlinkErrorMessage ) v1 ).getNlMsgError ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->getNlMsgError()Lcom/android/net/module/util/netlink/StructNlMsgErr;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 110 */
(( com.android.net.module.util.netlink.NetlinkErrorMessage ) v1 ).getNlMsgError ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->getNlMsgError()Lcom/android/net/module/util/netlink/StructNlMsgErr;
/* iget v4, v4, Lcom/android/net/module/util/netlink/StructNlMsgErr;->error:I */
/* .line 111 */
/* .local v4, "errno":I */
/* if-nez v4, :cond_0 */
/* .line 119 */
} // .end local v4 # "errno":I
/* nop */
/* .line 130 */
return;
/* .line 115 */
/* .restart local v4 # "errno":I */
} // :cond_0
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.android.net.module.util.netlink.NetlinkErrorMessage ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v2 );
/* .line 117 */
/* new-instance v2, Landroid/system/ErrnoException; */
(( com.android.net.module.util.netlink.NetlinkErrorMessage ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->toString()Ljava/lang/String;
v5 = java.lang.Math .abs ( v4 );
/* invoke-direct {v2, v3, v5}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;I)V */
/* throw v2 */
/* .line 121 */
} // .end local v4 # "errno":I
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 122 */
int v4 = 0; // const/4 v4, 0x0
(( java.nio.ByteBuffer ) v0 ).position ( v4 ); // invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 123 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "raw bytes: "; // const-string v5, "raw bytes: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.android.net.module.util.netlink.NetlinkConstants .hexify ( v0 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .local v4, "errmsg":Ljava/lang/String; */
/* .line 125 */
} // .end local v4 # "errmsg":Ljava/lang/String;
} // :cond_2
(( com.android.net.module.util.netlink.NetlinkErrorMessage ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->toString()Ljava/lang/String;
/* .line 127 */
/* .restart local v4 # "errmsg":Ljava/lang/String; */
} // :goto_0
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v3,v2 );
/* .line 128 */
/* new-instance v2, Landroid/system/ErrnoException; */
/* invoke-direct {v2, v4, v3}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;I)V */
/* throw v2 */
} // .end method
public static java.nio.ByteBuffer recvMessage ( java.io.FileDescriptor p0, Integer p1, Long p2 ) {
/* .locals 4 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .param p1, "bufsize" # I */
/* .param p2, "timeoutMs" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 207 */
com.android.net.module.util.netlink.NetlinkUtils .checkTimeout ( p2,p3 );
/* .line 209 */
android.system.StructTimeval .fromMillis ( p2,p3 );
android.system.Os .setsockoptTimeval ( p0,v0,v1,v2 );
/* .line 211 */
java.nio.ByteBuffer .allocate ( p1 );
/* .line 212 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
v1 = android.system.Os .read ( p0,v0 );
/* .line 213 */
/* .local v1, "length":I */
/* if-ne v1, p1, :cond_0 */
/* .line 214 */
final String v2 = "NetlinkUtils"; // const-string v2, "NetlinkUtils"
final String v3 = "maximum read"; // const-string v3, "maximum read"
android.util.Log .w ( v2,v3 );
/* .line 216 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
(( java.nio.ByteBuffer ) v0 ).position ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 217 */
(( java.nio.ByteBuffer ) v0 ).limit ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;
/* .line 218 */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v0 ).order ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 219 */
} // .end method
public static Integer sendMessage ( java.io.FileDescriptor p0, Object[] p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 3 */
/* .param p0, "fd" # Ljava/io/FileDescriptor; */
/* .param p1, "bytes" # [B */
/* .param p2, "offset" # I */
/* .param p3, "count" # I */
/* .param p4, "timeoutMs" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException;, */
/* Ljava/lang/IllegalArgumentException;, */
/* Ljava/io/InterruptedIOException; */
/* } */
} // .end annotation
/* .line 231 */
com.android.net.module.util.netlink.NetlinkUtils .checkTimeout ( p4,p5 );
/* .line 232 */
android.system.StructTimeval .fromMillis ( p4,p5 );
android.system.Os .setsockoptTimeval ( p0,v0,v1,v2 );
/* .line 233 */
v0 = android.system.Os .write ( p0,p1,p2,p3 );
} // .end method
public static void sendOneShotKernelMessage ( Integer p0, Object[] p1 ) {
/* .locals 10 */
/* .param p0, "nlProto" # I */
/* .param p1, "msg" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/system/ErrnoException; */
/* } */
} // .end annotation
/* .line 139 */
final String v0 = "NetlinkUtils"; // const-string v0, "NetlinkUtils"
final String v1 = "Error in NetlinkSocket.sendOneShotKernelMessage"; // const-string v1, "Error in NetlinkSocket.sendOneShotKernelMessage"
final String v2 = "Error in NetlinkSocket.sendOneShotKernelMessage"; // const-string v2, "Error in NetlinkSocket.sendOneShotKernelMessage"
/* .line 140 */
/* .local v2, "errPrefix":Ljava/lang/String; */
com.android.net.module.util.netlink.NetlinkUtils .netlinkSocketForProto ( p0 );
/* .line 143 */
/* .local v9, "fd":Ljava/io/FileDescriptor; */
try { // :try_start_0
com.android.net.module.util.netlink.NetlinkUtils .connectSocketToNetlink ( v9 );
/* .line 144 */
int v5 = 0; // const/4 v5, 0x0
/* array-length v6, p1 */
/* const-wide/16 v7, 0x12c */
/* move-object v3, v9 */
/* move-object v4, p1 */
/* invoke-static/range {v3 ..v8}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I */
/* .line 145 */
com.android.net.module.util.netlink.NetlinkUtils .receiveNetlinkAck ( v9 );
/* :try_end_0 */
/* .catch Ljava/io/InterruptedIOException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/net/SocketException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 154 */
try { // :try_start_1
android.net.util.SocketUtils .closeSocket ( v9 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 157 */
/* .line 155 */
/* :catch_0 */
/* move-exception v0 */
/* .line 158 */
/* nop */
/* .line 159 */
} // :goto_0
return;
/* .line 153 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 149 */
/* :catch_1 */
/* move-exception v3 */
/* .line 150 */
/* .local v3, "e":Ljava/net/SocketException; */
try { // :try_start_2
android.util.Log .e ( v0,v1,v3 );
/* .line 151 */
/* new-instance v0, Landroid/system/ErrnoException; */
/* invoke-direct {v0, v1, v4, v3}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V */
} // .end local v2 # "errPrefix":Ljava/lang/String;
} // .end local v9 # "fd":Ljava/io/FileDescriptor;
} // .end local p0 # "nlProto":I
} // .end local p1 # "msg":[B
/* throw v0 */
/* .line 146 */
} // .end local v3 # "e":Ljava/net/SocketException;
/* .restart local v2 # "errPrefix":Ljava/lang/String; */
/* .restart local v9 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p0 # "nlProto":I */
/* .restart local p1 # "msg":[B */
/* :catch_2 */
/* move-exception v3 */
/* .line 147 */
/* .local v3, "e":Ljava/io/InterruptedIOException; */
android.util.Log .e ( v0,v1,v3 );
/* .line 148 */
/* new-instance v0, Landroid/system/ErrnoException; */
/* invoke-direct {v0, v1, v4, v3}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V */
} // .end local v2 # "errPrefix":Ljava/lang/String;
} // .end local v9 # "fd":Ljava/io/FileDescriptor;
} // .end local p0 # "nlProto":I
} // .end local p1 # "msg":[B
/* throw v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 154 */
} // .end local v3 # "e":Ljava/io/InterruptedIOException;
/* .restart local v2 # "errPrefix":Ljava/lang/String; */
/* .restart local v9 # "fd":Ljava/io/FileDescriptor; */
/* .restart local p0 # "nlProto":I */
/* .restart local p1 # "msg":[B */
} // :goto_1
try { // :try_start_3
android.net.util.SocketUtils .closeSocket ( v9 );
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_3 */
/* .line 157 */
/* .line 155 */
/* :catch_3 */
/* move-exception v1 */
/* .line 158 */
} // :goto_2
/* throw v0 */
} // .end method
