.class public Lcom/android/net/module/util/netlink/StructIfacacheInfo;
.super Lcom/android/net/module/util/Struct;
.source "StructIfacacheInfo.java"


# static fields
.field public static final STRUCT_SIZE:I = 0x10


# instance fields
.field public final cstamp:J
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x2
        type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final preferred:J
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x0
        type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final tstamp:J
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x3
        type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final valid:J
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x1
        type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field


# direct methods
.method constructor <init>(JJJJ)V
    .locals 0
    .param p1, "preferred"    # J
    .param p3, "valid"    # J
    .param p5, "cstamp"    # J
    .param p7, "tstamp"    # J

    .line 50
    invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V

    .line 51
    iput-wide p1, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->preferred:J

    .line 52
    iput-wide p3, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->valid:J

    .line 53
    iput-wide p5, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->cstamp:J

    .line 54
    iput-wide p7, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->tstamp:J

    .line 55
    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructIfacacheInfo;
    .locals 2
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 66
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 69
    :cond_0
    const-class v0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    invoke-static {v0, p0}, Lcom/android/net/module/util/Struct;->parse(Ljava/lang/Class;Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;

    return-object v0
.end method


# virtual methods
.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 77
    invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 78
    return-void
.end method
