.class public Lcom/android/net/module/util/netlink/NetlinkUtils;
.super Ljava/lang/Object;
.source "NetlinkUtils.java"


# static fields
.field public static final DEFAULT_RECV_BUFSIZE:I = 0x2000

.field public static final INET_DIAG_INFO:I = 0x2

.field public static final INET_DIAG_MARK:I = 0xf

.field public static final INIT_MARK_VALUE:I = 0x0

.field public static final IO_TIMEOUT_MS:J = 0x12cL

.field public static final NULL_MASK:I = 0x0

.field public static final SOCKET_RECV_BUFSIZE:I = 0x10000

.field private static final TAG:Ljava/lang/String; = "NetlinkUtils"

.field public static final TCP_ALIVE_STATE_FILTER:I = 0xe

.field private static final TCP_ESTABLISHED:I = 0x1

.field private static final TCP_SYN_RECV:I = 0x3

.field private static final TCP_SYN_SENT:I = 0x2

.field public static final UNKNOWN_MARK:I = -0x1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkTimeout(J)V
    .locals 2
    .param p0, "timeoutMs"    # J

    .line 194
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    .line 197
    return-void

    .line 195
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Negative timeouts not permitted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static connectSocketToNetlink(Ljava/io/FileDescriptor;)V
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/net/SocketException;
        }
    .end annotation

    .line 190
    const/4 v0, 0x0

    invoke-static {v0, v0}, Landroid/net/util/SocketUtils;->makeNetlinkSocketAddress(II)Ljava/net/SocketAddress;

    move-result-object v0

    invoke-static {p0, v0}, Landroid/system/Os;->connect(Ljava/io/FileDescriptor;Ljava/net/SocketAddress;)V

    .line 191
    return-void
.end method

.method public static createNetLinkInetDiagSocket()Ljava/io/FileDescriptor;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 177
    sget v0, Landroid/system/OsConstants;->AF_NETLINK:I

    sget v1, Landroid/system/OsConstants;->SOCK_DGRAM:I

    sget v2, Landroid/system/OsConstants;->SOCK_CLOEXEC:I

    or-int/2addr v1, v2

    sget v2, Landroid/system/OsConstants;->NETLINK_INET_DIAG:I

    invoke-static {v0, v1, v2}, Landroid/system/Os;->socket(III)Ljava/io/FileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static enoughBytesRemainForValidNlMsg(Ljava/nio/ByteBuffer;)Z
    .locals 2
    .param p0, "bytes"    # Ljava/nio/ByteBuffer;

    .line 81
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static netlinkSocketForProto(I)Ljava/io/FileDescriptor;
    .locals 4
    .param p0, "nlProto"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 168
    sget v0, Landroid/system/OsConstants;->AF_NETLINK:I

    sget v1, Landroid/system/OsConstants;->SOCK_DGRAM:I

    invoke-static {v0, v1, p0}, Landroid/system/Os;->socket(III)Ljava/io/FileDescriptor;

    move-result-object v0

    .line 169
    .local v0, "fd":Ljava/io/FileDescriptor;
    sget v1, Landroid/system/OsConstants;->SOL_SOCKET:I

    sget v2, Landroid/system/OsConstants;->SO_RCVBUF:I

    const/high16 v3, 0x10000

    invoke-static {v0, v1, v2, v3}, Landroid/system/Os;->setsockoptInt(Ljava/io/FileDescriptor;III)V

    .line 170
    return-object v0
.end method

.method private static parseNetlinkErrorMessage(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkErrorMessage;
    .locals 3
    .param p0, "bytes"    # Ljava/nio/ByteBuffer;

    .line 92
    invoke-static {p0}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v0

    .line 93
    .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    if-eqz v0, :cond_1

    iget-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 96
    :cond_0
    invoke-static {v0, p0}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkErrorMessage;

    move-result-object v1

    return-object v1

    .line 94
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public static receiveNetlinkAck(Ljava/io/FileDescriptor;)V
    .locals 6
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;,
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 106
    const/16 v0, 0x2000

    const-wide/16 v1, 0x12c

    invoke-static {p0, v0, v1, v2}, Lcom/android/net/module/util/netlink/NetlinkUtils;->recvMessage(Ljava/io/FileDescriptor;IJ)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 108
    .local v0, "bytes":Ljava/nio/ByteBuffer;
    invoke-static {v0}, Lcom/android/net/module/util/netlink/NetlinkUtils;->parseNetlinkErrorMessage(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkErrorMessage;

    move-result-object v1

    .line 109
    .local v1, "response":Lcom/android/net/module/util/netlink/NetlinkErrorMessage;
    const-string v2, "receiveNetlinkAck, errmsg="

    const-string v3, "NetlinkUtils"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->getNlMsgError()Lcom/android/net/module/util/netlink/StructNlMsgErr;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 110
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->getNlMsgError()Lcom/android/net/module/util/netlink/StructNlMsgErr;

    move-result-object v4

    iget v4, v4, Lcom/android/net/module/util/netlink/StructNlMsgErr;->error:I

    .line 111
    .local v4, "errno":I
    if-nez v4, :cond_0

    .line 119
    .end local v4    # "errno":I
    nop

    .line 130
    return-void

    .line 115
    .restart local v4    # "errno":I
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    new-instance v2, Landroid/system/ErrnoException;

    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-direct {v2, v3, v5}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 121
    .end local v4    # "errno":I
    :cond_1
    if-nez v1, :cond_2

    .line 122
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 123
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "raw bytes: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/android/net/module/util/netlink/NetlinkConstants;->hexify(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .local v4, "errmsg":Ljava/lang/String;
    goto :goto_0

    .line 125
    .end local v4    # "errmsg":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->toString()Ljava/lang/String;

    move-result-object v4

    .line 127
    .restart local v4    # "errmsg":Ljava/lang/String;
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    new-instance v2, Landroid/system/ErrnoException;

    sget v3, Landroid/system/OsConstants;->EPROTO:I

    invoke-direct {v2, v4, v3}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;I)V

    throw v2
.end method

.method public static recvMessage(Ljava/io/FileDescriptor;IJ)Ljava/nio/ByteBuffer;
    .locals 4
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "bufsize"    # I
    .param p2, "timeoutMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .line 207
    invoke-static {p2, p3}, Lcom/android/net/module/util/netlink/NetlinkUtils;->checkTimeout(J)V

    .line 209
    sget v0, Landroid/system/OsConstants;->SOL_SOCKET:I

    sget v1, Landroid/system/OsConstants;->SO_RCVTIMEO:I

    invoke-static {p2, p3}, Landroid/system/StructTimeval;->fromMillis(J)Landroid/system/StructTimeval;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Landroid/system/Os;->setsockoptTimeval(Ljava/io/FileDescriptor;IILandroid/system/StructTimeval;)V

    .line 211
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 212
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {p0, v0}, Landroid/system/Os;->read(Ljava/io/FileDescriptor;Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 213
    .local v1, "length":I
    if-ne v1, p1, :cond_0

    .line 214
    const-string v2, "NetlinkUtils"

    const-string v3, "maximum read"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 217
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 218
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 219
    return-object v0
.end method

.method public static sendMessage(Ljava/io/FileDescriptor;[BIIJ)I
    .locals 3
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .param p4, "timeoutMs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    .line 231
    invoke-static {p4, p5}, Lcom/android/net/module/util/netlink/NetlinkUtils;->checkTimeout(J)V

    .line 232
    sget v0, Landroid/system/OsConstants;->SOL_SOCKET:I

    sget v1, Landroid/system/OsConstants;->SO_SNDTIMEO:I

    invoke-static {p4, p5}, Landroid/system/StructTimeval;->fromMillis(J)Landroid/system/StructTimeval;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Landroid/system/Os;->setsockoptTimeval(Ljava/io/FileDescriptor;IILandroid/system/StructTimeval;)V

    .line 233
    invoke-static {p0, p1, p2, p3}, Landroid/system/Os;->write(Ljava/io/FileDescriptor;[BII)I

    move-result v0

    return v0
.end method

.method public static sendOneShotKernelMessage(I[B)V
    .locals 10
    .param p0, "nlProto"    # I
    .param p1, "msg"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/system/ErrnoException;
        }
    .end annotation

    .line 139
    const-string v0, "NetlinkUtils"

    const-string v1, "Error in NetlinkSocket.sendOneShotKernelMessage"

    const-string v2, "Error in NetlinkSocket.sendOneShotKernelMessage"

    .line 140
    .local v2, "errPrefix":Ljava/lang/String;
    invoke-static {p0}, Lcom/android/net/module/util/netlink/NetlinkUtils;->netlinkSocketForProto(I)Ljava/io/FileDescriptor;

    move-result-object v9

    .line 143
    .local v9, "fd":Ljava/io/FileDescriptor;
    :try_start_0
    invoke-static {v9}, Lcom/android/net/module/util/netlink/NetlinkUtils;->connectSocketToNetlink(Ljava/io/FileDescriptor;)V

    .line 144
    const/4 v5, 0x0

    array-length v6, p1

    const-wide/16 v7, 0x12c

    move-object v3, v9

    move-object v4, p1

    invoke-static/range {v3 .. v8}, Lcom/android/net/module/util/netlink/NetlinkUtils;->sendMessage(Ljava/io/FileDescriptor;[BIIJ)I

    .line 145
    invoke-static {v9}, Lcom/android/net/module/util/netlink/NetlinkUtils;->receiveNetlinkAck(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :try_start_1
    invoke-static {v9}, Landroid/net/util/SocketUtils;->closeSocket(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 157
    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 158
    nop

    .line 159
    :goto_0
    return-void

    .line 153
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 149
    :catch_1
    move-exception v3

    .line 150
    .local v3, "e":Ljava/net/SocketException;
    :try_start_2
    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 151
    new-instance v0, Landroid/system/ErrnoException;

    sget v4, Landroid/system/OsConstants;->EIO:I

    invoke-direct {v0, v1, v4, v3}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V

    .end local v2    # "errPrefix":Ljava/lang/String;
    .end local v9    # "fd":Ljava/io/FileDescriptor;
    .end local p0    # "nlProto":I
    .end local p1    # "msg":[B
    throw v0

    .line 146
    .end local v3    # "e":Ljava/net/SocketException;
    .restart local v2    # "errPrefix":Ljava/lang/String;
    .restart local v9    # "fd":Ljava/io/FileDescriptor;
    .restart local p0    # "nlProto":I
    .restart local p1    # "msg":[B
    :catch_2
    move-exception v3

    .line 147
    .local v3, "e":Ljava/io/InterruptedIOException;
    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 148
    new-instance v0, Landroid/system/ErrnoException;

    sget v4, Landroid/system/OsConstants;->ETIMEDOUT:I

    invoke-direct {v0, v1, v4, v3}, Landroid/system/ErrnoException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V

    .end local v2    # "errPrefix":Ljava/lang/String;
    .end local v9    # "fd":Ljava/io/FileDescriptor;
    .end local p0    # "nlProto":I
    .end local p1    # "msg":[B
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    .restart local v2    # "errPrefix":Ljava/lang/String;
    .restart local v9    # "fd":Ljava/io/FileDescriptor;
    .restart local p0    # "nlProto":I
    .restart local p1    # "msg":[B
    :goto_1
    :try_start_3
    invoke-static {v9}, Landroid/net/util/SocketUtils;->closeSocket(Ljava/io/FileDescriptor;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 157
    goto :goto_2

    .line 155
    :catch_3
    move-exception v1

    .line 158
    :goto_2
    throw v0
.end method
