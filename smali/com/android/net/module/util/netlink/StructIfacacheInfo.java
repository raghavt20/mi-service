public class com.android.net.module.util.netlink.StructIfacacheInfo extends com.android.net.module.util.Struct {
	 /* .source "StructIfacacheInfo.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public final Long cstamp;
	 /* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
	 /* order = 0x2 */
	 /* type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Long preferred;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x0 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Long tstamp;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x3 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Long valid;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x1 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
/* # direct methods */
 com.android.net.module.util.netlink.StructIfacacheInfo ( ) {
/* .locals 0 */
/* .param p1, "preferred" # J */
/* .param p3, "valid" # J */
/* .param p5, "cstamp" # J */
/* .param p7, "tstamp" # J */
/* .line 50 */
/* invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V */
/* .line 51 */
/* iput-wide p1, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->preferred:J */
/* .line 52 */
/* iput-wide p3, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->valid:J */
/* .line 53 */
/* iput-wide p5, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->cstamp:J */
/* .line 54 */
/* iput-wide p7, p0, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->tstamp:J */
/* .line 55 */
return;
} // .end method
public static com.android.net.module.util.netlink.StructIfacacheInfo parse ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 66 */
v0 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v1, 0x10 */
/* if-ge v0, v1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 69 */
} // :cond_0
/* const-class v0, Lcom/android/net/module/util/netlink/StructIfacacheInfo; */
com.android.net.module.util.Struct .parse ( v0,p0 );
/* check-cast v0, Lcom/android/net/module/util/netlink/StructIfacacheInfo; */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 0 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 77 */
(( com.android.net.module.util.netlink.StructIfacacheInfo ) p0 ).writeToByteBuffer ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V
/* .line 78 */
return;
} // .end method
