public class com.android.net.module.util.netlink.RtNetlinkNeighborMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "RtNetlinkNeighborMessage.java" */
	 /* # static fields */
	 public static final Object NDA_CACHEINFO;
	 public static final Object NDA_DST;
	 public static final Object NDA_IFINDEX;
	 public static final Object NDA_LLADDR;
	 public static final Object NDA_MASTER;
	 public static final Object NDA_PORT;
	 public static final Object NDA_PROBES;
	 public static final Object NDA_UNSPEC;
	 public static final Object NDA_VLAN;
	 public static final Object NDA_VNI;
	 /* # instance fields */
	 private com.android.net.module.util.netlink.StructNdaCacheInfo mCacheInfo;
	 private java.net.InetAddress mDestination;
	 private mLinkLayerAddr;
	 private com.android.net.module.util.netlink.StructNdMsg mNdmsg;
	 private Integer mNumProbes;
	 /* # direct methods */
	 private com.android.net.module.util.netlink.RtNetlinkNeighborMessage ( ) {
		 /* .locals 2 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .line 163 */
		 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 164 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.mNdmsg = v0;
		 /* .line 165 */
		 this.mDestination = v0;
		 /* .line 166 */
		 this.mLinkLayerAddr = v0;
		 /* .line 167 */
		 int v1 = 0; // const/4 v1, 0x0
		 /* iput v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I */
		 /* .line 168 */
		 this.mCacheInfo = v0;
		 /* .line 169 */
		 return;
	 } // .end method
	 private Integer getRequiredSpace ( ) {
		 /* .locals 2 */
		 /* .line 192 */
		 /* const/16 v0, 0x1c */
		 /* .line 193 */
		 /* .local v0, "spaceRequired":I */
		 v1 = this.mDestination;
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 194 */
			 /* nop */
			 /* .line 195 */
			 (( java.net.InetAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B
			 /* array-length v1, v1 */
			 /* add-int/lit8 v1, v1, 0x4 */
			 /* .line 194 */
			 v1 = 			 com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v1 );
			 /* add-int/2addr v0, v1 */
			 /* .line 197 */
		 } // :cond_0
		 v1 = this.mLinkLayerAddr;
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 198 */
			 /* array-length v1, v1 */
			 /* add-int/lit8 v1, v1, 0x4 */
			 v1 = 			 com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v1 );
			 /* add-int/2addr v0, v1 */
			 /* .line 203 */
		 } // :cond_1
	 } // .end method
	 public static newGetNeighborsRequest ( Integer p0 ) {
		 /* .locals 5 */
		 /* .param p0, "seqNo" # I */
		 /* .line 110 */
		 /* const/16 v0, 0x1c */
		 /* .line 111 */
		 /* .local v0, "length":I */
		 /* const/16 v1, 0x1c */
		 /* new-array v2, v1, [B */
		 /* .line 112 */
		 /* .local v2, "bytes":[B */
		 java.nio.ByteBuffer .wrap ( v2 );
		 /* .line 113 */
		 /* .local v3, "byteBuffer":Ljava/nio/ByteBuffer; */
		 java.nio.ByteOrder .nativeOrder ( );
		 (( java.nio.ByteBuffer ) v3 ).order ( v4 ); // invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
		 /* .line 115 */
		 /* new-instance v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* invoke-direct {v4}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V */
		 /* .line 116 */
		 /* .local v4, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* iput v1, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
		 /* .line 117 */
		 /* const/16 v1, 0x1e */
		 /* iput-short v1, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
		 /* .line 118 */
		 /* const/16 v1, 0x301 */
		 /* iput-short v1, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S */
		 /* .line 119 */
		 /* iput p0, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I */
		 /* .line 120 */
		 (( com.android.net.module.util.netlink.StructNlMsgHdr ) v4 ).pack ( v3 ); // invoke-virtual {v4, v3}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
		 /* .line 122 */
		 /* new-instance v1, Lcom/android/net/module/util/netlink/StructNdMsg; */
		 /* invoke-direct {v1}, Lcom/android/net/module/util/netlink/StructNdMsg;-><init>()V */
		 /* .line 123 */
		 /* .local v1, "ndmsg":Lcom/android/net/module/util/netlink/StructNdMsg; */
		 (( com.android.net.module.util.netlink.StructNdMsg ) v1 ).pack ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/net/module/util/netlink/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V
		 /* .line 125 */
	 } // .end method
	 public static newNewNeighborMessage ( Integer p0, java.net.InetAddress p1, Object p2, Integer p3, Object[] p4 ) {
		 /* .locals 5 */
		 /* .param p0, "seqNo" # I */
		 /* .param p1, "ip" # Ljava/net/InetAddress; */
		 /* .param p2, "nudState" # S */
		 /* .param p3, "ifIndex" # I */
		 /* .param p4, "llAddr" # [B */
		 /* .line 134 */
		 /* new-instance v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V */
		 /* .line 135 */
		 /* .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* const/16 v1, 0x1c */
		 /* iput-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
		 /* .line 136 */
		 /* const/16 v1, 0x105 */
		 /* iput-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S */
		 /* .line 137 */
		 /* iput p0, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I */
		 /* .line 139 */
		 /* new-instance v1, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage; */
		 /* invoke-direct {v1, v0}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 140 */
		 /* .local v1, "msg":Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage; */
		 /* new-instance v2, Lcom/android/net/module/util/netlink/StructNdMsg; */
		 /* invoke-direct {v2}, Lcom/android/net/module/util/netlink/StructNdMsg;-><init>()V */
		 this.mNdmsg = v2;
		 /* .line 141 */
		 /* nop */
		 /* .line 142 */
		 /* instance-of v3, p1, Ljava/net/Inet6Address; */
		 if ( v3 != null) { // if-eqz v3, :cond_0
		 } // :cond_0
	 } // :goto_0
	 /* int-to-byte v3, v3 */
	 /* iput-byte v3, v2, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_family:B */
	 /* .line 143 */
	 v2 = this.mNdmsg;
	 /* iput p3, v2, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_ifindex:I */
	 /* .line 144 */
	 v2 = this.mNdmsg;
	 /* iput-short p2, v2, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S */
	 /* .line 145 */
	 this.mDestination = p1;
	 /* .line 146 */
	 this.mLinkLayerAddr = p4;
	 /* .line 148 */
	 v2 = 	 /* invoke-direct {v1}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->getRequiredSpace()I */
	 /* new-array v2, v2, [B */
	 /* .line 149 */
	 /* .local v2, "bytes":[B */
	 /* array-length v3, v2 */
	 /* iput v3, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
	 /* .line 150 */
	 java.nio.ByteBuffer .wrap ( v2 );
	 /* .line 151 */
	 /* .local v3, "byteBuffer":Ljava/nio/ByteBuffer; */
	 java.nio.ByteOrder .nativeOrder ( );
	 (( java.nio.ByteBuffer ) v3 ).order ( v4 ); // invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
	 /* .line 152 */
	 (( com.android.net.module.util.netlink.RtNetlinkNeighborMessage ) v1 ).pack ( v3 ); // invoke-virtual {v1, v3}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 153 */
} // .end method
private static void packNlAttr ( Object p0, Object[] p1, java.nio.ByteBuffer p2 ) {
	 /* .locals 2 */
	 /* .param p0, "nlType" # S */
	 /* .param p1, "nlValue" # [B */
	 /* .param p2, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 207 */
	 /* new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr; */
	 /* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>()V */
	 /* .line 208 */
	 /* .local v0, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
	 /* iput-short p0, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
	 /* .line 209 */
	 this.nla_value = p1;
	 /* .line 210 */
	 v1 = this.nla_value;
	 /* array-length v1, v1 */
	 /* add-int/lit8 v1, v1, 0x4 */
	 /* int-to-short v1, v1 */
	 /* iput-short v1, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
	 /* .line 211 */
	 (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).pack ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 212 */
	 return;
} // .end method
public static com.android.net.module.util.netlink.RtNetlinkNeighborMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
	 /* .locals 6 */
	 /* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
	 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 62 */
	 /* new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage; */
	 /* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
	 /* .line 64 */
	 /* .local v0, "neighMsg":Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage; */
	 com.android.net.module.util.netlink.StructNdMsg .parse ( p1 );
	 this.mNdmsg = v1;
	 /* .line 65 */
	 /* if-nez v1, :cond_0 */
	 /* .line 66 */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 70 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* .line 71 */
/* .local v1, "baseOffset":I */
int v2 = 1; // const/4 v2, 0x1
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v2,p1 );
/* .line 72 */
/* .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 73 */
	 (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsInetAddress ( ); // invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
	 this.mDestination = v3;
	 /* .line 76 */
} // :cond_1
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 77 */
int v3 = 2; // const/4 v3, 0x2
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,p1 );
/* .line 78 */
if ( v2 != null) { // if-eqz v2, :cond_2
	 /* .line 79 */
	 v3 = this.nla_value;
	 this.mLinkLayerAddr = v3;
	 /* .line 82 */
} // :cond_2
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 83 */
int v3 = 4; // const/4 v3, 0x4
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,p1 );
/* .line 84 */
if ( v2 != null) { // if-eqz v2, :cond_3
	 /* .line 85 */
	 int v3 = 0; // const/4 v3, 0x0
	 v3 = 	 (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsInt ( v3 ); // invoke-virtual {v2, v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInt(I)I
	 /* iput v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I */
	 /* .line 88 */
} // :cond_3
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 89 */
int v3 = 3; // const/4 v3, 0x3
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,p1 );
/* .line 90 */
if ( v2 != null) { // if-eqz v2, :cond_4
	 /* .line 91 */
	 (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsByteBuffer ( ); // invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
	 com.android.net.module.util.netlink.StructNdaCacheInfo .parse ( v3 );
	 this.mCacheInfo = v3;
	 /* .line 94 */
} // :cond_4
/* const/16 v3, 0x1c */
/* .line 95 */
/* .local v3, "kMinConsumed":I */
v4 = this.mHeader;
/* iget v4, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
/* add-int/lit8 v4, v4, -0x1c */
v4 = com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v4 );
/* .line 97 */
/* .local v4, "kAdditionalSpace":I */
v5 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
/* if-ge v5, v4, :cond_5 */
/* .line 98 */
v5 = (( java.nio.ByteBuffer ) p1 ).limit ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I
(( java.nio.ByteBuffer ) p1 ).position ( v5 ); // invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 100 */
} // :cond_5
/* add-int v5, v1, v4 */
(( java.nio.ByteBuffer ) p1 ).position ( v5 ); // invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 103 */
} // :goto_0
} // .end method
/* # virtual methods */
public com.android.net.module.util.netlink.StructNdaCacheInfo getCacheInfo ( ) {
/* .locals 1 */
/* .line 188 */
v0 = this.mCacheInfo;
} // .end method
public java.net.InetAddress getDestination ( ) {
/* .locals 1 */
/* .line 176 */
v0 = this.mDestination;
} // .end method
public getLinkLayerAddress ( ) {
/* .locals 1 */
/* .line 180 */
v0 = this.mLinkLayerAddr;
} // .end method
public com.android.net.module.util.netlink.StructNdMsg getNdHeader ( ) {
/* .locals 1 */
/* .line 172 */
v0 = this.mNdmsg;
} // .end method
public Integer getProbes ( ) {
/* .locals 1 */
/* .line 184 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I */
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 218 */
(( com.android.net.module.util.netlink.RtNetlinkNeighborMessage ) p0 ).getHeader ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 219 */
v0 = this.mNdmsg;
(( com.android.net.module.util.netlink.StructNdMsg ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V
/* .line 221 */
v0 = this.mDestination;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 222 */
int v1 = 1; // const/4 v1, 0x1
(( java.net.InetAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B
com.android.net.module.util.netlink.RtNetlinkNeighborMessage .packNlAttr ( v1,v0,p1 );
/* .line 224 */
} // :cond_0
v0 = this.mLinkLayerAddr;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 225 */
int v1 = 2; // const/4 v1, 0x2
com.android.net.module.util.netlink.RtNetlinkNeighborMessage .packNlAttr ( v1,v0,p1 );
/* .line 227 */
} // :cond_1
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 231 */
v0 = this.mDestination;
final String v1 = ""; // const-string v1, ""
/* if-nez v0, :cond_0 */
/* move-object v0, v1 */
} // :cond_0
(( java.net.InetAddress ) v0 ).getHostAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
/* .line 232 */
/* .local v0, "ipLiteral":Ljava/lang/String; */
} // :goto_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "RtNetlinkNeighborMessage{ nlmsghdr{"; // const-string v3, "RtNetlinkNeighborMessage{ nlmsghdr{"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 234 */
v3 = this.mHeader;
/* if-nez v3, :cond_1 */
/* move-object v3, v1 */
} // :cond_1
v3 = this.mHeader;
java.lang.Integer .valueOf ( v4 );
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v3 ).toString ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
} // :goto_1
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, ndmsg{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 235 */
v3 = this.mNdmsg;
/* if-nez v3, :cond_2 */
/* move-object v3, v1 */
} // :cond_2
(( com.android.net.module.util.netlink.StructNdMsg ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNdMsg;->toString()Ljava/lang/String;
} // :goto_2
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "}, destination{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} linklayeraddr{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mLinkLayerAddr;
/* .line 237 */
com.android.net.module.util.netlink.NetlinkConstants .hexify ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} probes{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "} cacheinfo{" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 239 */
v3 = this.mCacheInfo;
/* if-nez v3, :cond_3 */
} // :cond_3
(( com.android.net.module.util.netlink.StructNdaCacheInfo ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->toString()Ljava/lang/String;
} // :goto_3
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "} }" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 232 */
} // .end method
