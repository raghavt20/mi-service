public class com.android.net.module.util.netlink.StructInetDiagSockId {
	 /* .source "StructInetDiagSockId.java" */
	 /* # static fields */
	 private static final Long INET_DIAG_NOCOOKIE;
	 private static final IPV4_PADDING;
	 public static final Integer STRUCT_SIZE;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 public final Long cookie;
	 public final Integer ifIndex;
	 public final java.net.InetSocketAddress locSocketAddress;
	 public final java.net.InetSocketAddress remSocketAddress;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.StructInetDiagSockId ( ) {
		 /* .locals 1 */
		 /* .line 57 */
		 /* const-class v0, Lcom/android/net/module/util/netlink/StructInetDiagSockId; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* .line 61 */
		 /* const/16 v0, 0xc */
		 /* new-array v0, v0, [B */
		 /* fill-array-data v0, :array_0 */
		 return;
		 /* :array_0 */
		 /* .array-data 1 */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
		 /* 0x0t */
	 } // .end array-data
} // .end method
public com.android.net.module.util.netlink.StructInetDiagSockId ( ) {
	 /* .locals 6 */
	 /* .param p1, "loc" # Ljava/net/InetSocketAddress; */
	 /* .param p2, "rem" # Ljava/net/InetSocketAddress; */
	 /* .line 69 */
	 int v3 = 0; // const/4 v3, 0x0
	 /* const-wide/16 v4, -0x1 */
	 /* move-object v0, p0 */
	 /* move-object v1, p1 */
	 /* move-object v2, p2 */
	 /* invoke-direct/range {v0 ..v5}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;-><init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IJ)V */
	 /* .line 70 */
	 return;
} // .end method
public com.android.net.module.util.netlink.StructInetDiagSockId ( ) {
	 /* .locals 0 */
	 /* .param p1, "loc" # Ljava/net/InetSocketAddress; */
	 /* .param p2, "rem" # Ljava/net/InetSocketAddress; */
	 /* .param p3, "ifIndex" # I */
	 /* .param p4, "cookie" # J */
	 /* .line 73 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 74 */
	 this.locSocketAddress = p1;
	 /* .line 75 */
	 this.remSocketAddress = p2;
	 /* .line 76 */
	 /* iput p3, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->ifIndex:I */
	 /* .line 77 */
	 /* iput-wide p4, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->cookie:J */
	 /* .line 78 */
	 return;
} // .end method
public static com.android.net.module.util.netlink.StructInetDiagSockId parse ( java.nio.ByteBuffer p0, Object p1 ) {
	 /* .locals 14 */
	 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .param p1, "family" # S */
	 /* .line 85 */
	 v0 = 	 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
	 /* const/16 v1, 0x30 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* if-ge v0, v1, :cond_0 */
	 /* .line 86 */
	 /* .line 89 */
} // :cond_0
v0 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) p0 ).order ( v0 ); // invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 90 */
v0 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
v0 = java.lang.Short .toUnsignedInt ( v0 );
/* .line 91 */
/* .local v0, "srcPort":I */
v1 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
v1 = java.lang.Short .toUnsignedInt ( v1 );
/* .line 95 */
/* .local v1, "dstPort":I */
final String v4 = "Failed to parse address: "; // const-string v4, "Failed to parse address: "
/* if-ne p1, v3, :cond_1 */
/* .line 96 */
int v3 = 4; // const/4 v3, 0x4
/* new-array v5, v3, [B */
/* .line 97 */
/* .local v5, "srcAddrByte":[B */
/* new-array v3, v3, [B */
/* .line 98 */
/* .local v3, "dstAddrByte":[B */
(( java.nio.ByteBuffer ) p0 ).get ( v5 ); // invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 101 */
v6 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* add-int/lit8 v6, v6, 0xc */
(( java.nio.ByteBuffer ) p0 ).position ( v6 ); // invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 102 */
(( java.nio.ByteBuffer ) p0 ).get ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 103 */
v6 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* add-int/lit8 v6, v6, 0xc */
(( java.nio.ByteBuffer ) p0 ).position ( v6 ); // invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 105 */
try { // :try_start_0
	 java.net.Inet4Address .getByAddress ( v5 );
	 /* .line 106 */
	 /* .local v6, "srcAddr":Ljava/net/InetAddress; */
	 java.net.Inet4Address .getByAddress ( v3 );
	 /* :try_end_0 */
	 /* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 110 */
	 /* .local v2, "dstAddr":Ljava/net/InetAddress; */
	 /* nop */
	 /* .line 111 */
} // .end local v3 # "dstAddrByte":[B
} // .end local v5 # "srcAddrByte":[B
/* .line 107 */
} // .end local v2 # "dstAddr":Ljava/net/InetAddress;
} // .end local v6 # "srcAddr":Ljava/net/InetAddress;
/* .restart local v3 # "dstAddrByte":[B */
/* .restart local v5 # "srcAddrByte":[B */
/* :catch_0 */
/* move-exception v6 */
/* .line 108 */
/* .local v6, "e":Ljava/net/UnknownHostException; */
v7 = com.android.net.module.util.netlink.StructInetDiagSockId.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .wtf ( v7,v4 );
/* .line 109 */
/* .line 111 */
} // .end local v3 # "dstAddrByte":[B
} // .end local v5 # "srcAddrByte":[B
} // .end local v6 # "e":Ljava/net/UnknownHostException;
} // :cond_1
/* if-ne p1, v3, :cond_2 */
/* .line 112 */
/* const/16 v3, 0x10 */
/* new-array v5, v3, [B */
/* .line 113 */
/* .restart local v5 # "srcAddrByte":[B */
/* new-array v3, v3, [B */
/* .line 114 */
/* .restart local v3 # "dstAddrByte":[B */
(( java.nio.ByteBuffer ) p0 ).get ( v5 ); // invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 115 */
(( java.nio.ByteBuffer ) p0 ).get ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
/* .line 120 */
int v6 = -1; // const/4 v6, -0x1
try { // :try_start_1
java.net.Inet6Address .getByAddress ( v2,v5,v6 );
/* .line 122 */
/* .local v7, "srcAddr":Ljava/net/InetAddress; */
java.net.Inet6Address .getByAddress ( v2,v3,v6 );
/* :try_end_1 */
/* .catch Ljava/net/UnknownHostException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 127 */
/* .restart local v2 # "dstAddr":Ljava/net/InetAddress; */
/* nop */
/* .line 128 */
} // .end local v3 # "dstAddrByte":[B
} // .end local v5 # "srcAddrByte":[B
/* move-object v6, v7 */
/* .line 133 */
} // .end local v7 # "srcAddr":Ljava/net/InetAddress;
/* .local v6, "srcAddr":Ljava/net/InetAddress; */
} // :goto_0
/* new-instance v8, Ljava/net/InetSocketAddress; */
/* invoke-direct {v8, v6, v0}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V */
/* .line 134 */
/* .local v8, "srcSocketAddr":Ljava/net/InetSocketAddress; */
/* new-instance v9, Ljava/net/InetSocketAddress; */
/* invoke-direct {v9, v2, v1}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V */
/* .line 136 */
/* .local v9, "dstSocketAddr":Ljava/net/InetSocketAddress; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) p0 ).order ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 137 */
v3 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* .line 138 */
/* .local v3, "ifIndex":I */
(( java.nio.ByteBuffer ) p0 ).getLong ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getLong()J
/* move-result-wide v4 */
/* .line 139 */
/* .local v4, "cookie":J */
/* new-instance v13, Lcom/android/net/module/util/netlink/StructInetDiagSockId; */
/* move-object v7, v13 */
/* move v10, v3 */
/* move-wide v11, v4 */
/* invoke-direct/range {v7 ..v12}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;-><init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IJ)V */
/* .line 124 */
} // .end local v2 # "dstAddr":Ljava/net/InetAddress;
} // .end local v4 # "cookie":J
} // .end local v6 # "srcAddr":Ljava/net/InetAddress;
} // .end local v8 # "srcSocketAddr":Ljava/net/InetSocketAddress;
} // .end local v9 # "dstSocketAddr":Ljava/net/InetSocketAddress;
/* .local v3, "dstAddrByte":[B */
/* .restart local v5 # "srcAddrByte":[B */
/* :catch_1 */
/* move-exception v6 */
/* .line 125 */
/* .local v6, "e":Ljava/net/UnknownHostException; */
v7 = com.android.net.module.util.netlink.StructInetDiagSockId.TAG;
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( v4 ); // invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .wtf ( v7,v4 );
/* .line 126 */
/* .line 129 */
} // .end local v3 # "dstAddrByte":[B
} // .end local v5 # "srcAddrByte":[B
} // .end local v6 # "e":Ljava/net/UnknownHostException;
} // :cond_2
v3 = com.android.net.module.util.netlink.StructInetDiagSockId.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Invalid address family: "; // const-string v5, "Invalid address family: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .wtf ( v3,v4 );
/* .line 130 */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 146 */
v0 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 147 */
v0 = this.locSocketAddress;
v0 = (( java.net.InetSocketAddress ) v0 ).getPort ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I
/* int-to-short v0, v0 */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 148 */
v0 = this.remSocketAddress;
v0 = (( java.net.InetSocketAddress ) v0 ).getPort ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I
/* int-to-short v0, v0 */
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 149 */
v0 = this.locSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 150 */
v0 = this.locSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
/* instance-of v0, v0, Ljava/net/Inet4Address; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 151 */
v0 = com.android.net.module.util.netlink.StructInetDiagSockId.IPV4_PADDING;
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 153 */
} // :cond_0
v0 = this.remSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 154 */
v0 = this.remSocketAddress;
(( java.net.InetSocketAddress ) v0 ).getAddress ( ); // invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
/* instance-of v0, v0, Ljava/net/Inet4Address; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 155 */
v0 = com.android.net.module.util.netlink.StructInetDiagSockId.IPV4_PADDING;
(( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 157 */
} // :cond_1
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 158 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->ifIndex:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 159 */
/* iget-wide v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->cookie:J */
(( java.nio.ByteBuffer ) p1 ).putLong ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;
/* .line 160 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 5 */
/* .line 164 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StructInetDiagSockId{ idiag_sport{"; // const-string v1, "StructInetDiagSockId{ idiag_sport{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.locSocketAddress;
/* .line 165 */
v1 = (( java.net.InetSocketAddress ) v1 ).getPort ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_dport{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.remSocketAddress;
/* .line 166 */
v1 = (( java.net.InetSocketAddress ) v1 ).getPort ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_src{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.locSocketAddress;
/* .line 167 */
(( java.net.InetSocketAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v1 ).getHostAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_dst{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.remSocketAddress;
/* .line 168 */
(( java.net.InetSocketAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;
(( java.net.InetAddress ) v1 ).getHostAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_if{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->ifIndex:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, idiag_cookie{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 171 */
/* iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->cookie:J */
/* const-wide/16 v3, -0x1 */
/* cmp-long v3, v1, v3 */
/* if-nez v3, :cond_0 */
final String v1 = "INET_DIAG_NOCOOKIE"; // const-string v1, "INET_DIAG_NOCOOKIE"
} // :cond_0
java.lang.Long .valueOf ( v1,v2 );
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 164 */
} // .end method
