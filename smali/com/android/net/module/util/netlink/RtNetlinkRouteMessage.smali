.class public Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "RtNetlinkRouteMessage.java"


# static fields
.field public static final RTA_DST:S = 0x1s

.field public static final RTA_GATEWAY:S = 0x5s

.field public static final RTA_OIF:S = 0x4s


# instance fields
.field private mDestination:Landroid/net/IpPrefix;

.field private mGateway:Ljava/net/InetAddress;

.field private mIfindex:I

.field private mRtmsg:Lcom/android/net/module/util/netlink/StructRtMsg;


# direct methods
.method private constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V
    .locals 1
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 64
    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mRtmsg:Lcom/android/net/module/util/netlink/StructRtMsg;

    .line 66
    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mDestination:Landroid/net/IpPrefix;

    .line 67
    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mGateway:Ljava/net/InetAddress;

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I

    .line 69
    return-void
.end method

.method private static matchRouteAddressFamily(Ljava/net/InetAddress;I)Z
    .locals 1
    .param p0, "address"    # Ljava/net/InetAddress;
    .param p1, "family"    # I

    .line 100
    instance-of v0, p0, Ljava/net/Inet4Address;

    if-eqz v0, :cond_0

    sget v0, Landroid/system/OsConstants;->AF_INET:I

    if-eq p1, v0, :cond_1

    :cond_0
    instance-of v0, p0, Ljava/net/Inet6Address;

    if-eqz v0, :cond_2

    sget v0, Landroid/system/OsConstants;->AF_INET6:I

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;
    .locals 9
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 115
    new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;

    invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 117
    .local v0, "routeMsg":Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructRtMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructRtMsg;

    move-result-object v1

    iput-object v1, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mRtmsg:Lcom/android/net/module/util/netlink/StructRtMsg;

    .line 118
    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 119
    :cond_0
    iget-short v1, v1, Lcom/android/net/module/util/netlink/StructRtMsg;->family:S

    .line 122
    .local v1, "rtmFamily":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 123
    .local v3, "baseOffset":I
    const/4 v4, 0x1

    invoke-static {v4, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v4

    .line 124
    .local v4, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    const/4 v5, 0x0

    if-eqz v4, :cond_3

    .line 125
    invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;

    move-result-object v6

    .line 127
    .local v6, "destination":Ljava/net/InetAddress;
    if-nez v6, :cond_1

    return-object v2

    .line 129
    :cond_1
    invoke-static {v6, v1}, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->matchRouteAddressFamily(Ljava/net/InetAddress;I)Z

    move-result v7

    if-nez v7, :cond_2

    return-object v2

    .line 130
    :cond_2
    new-instance v7, Landroid/net/IpPrefix;

    iget-object v8, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mRtmsg:Lcom/android/net/module/util/netlink/StructRtMsg;

    iget-short v8, v8, Lcom/android/net/module/util/netlink/StructRtMsg;->dstLen:S

    invoke-direct {v7, v6, v8}, Landroid/net/IpPrefix;-><init>(Ljava/net/InetAddress;I)V

    iput-object v7, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mDestination:Landroid/net/IpPrefix;

    .line 131
    .end local v6    # "destination":Ljava/net/InetAddress;
    goto :goto_0

    :cond_3
    sget v6, Landroid/system/OsConstants;->AF_INET:I

    if-ne v1, v6, :cond_4

    .line 132
    new-instance v6, Landroid/net/IpPrefix;

    sget-object v7, Lcom/android/net/module/util/NetworkStackConstants;->IPV4_ADDR_ANY:Ljava/net/Inet4Address;

    invoke-direct {v6, v7, v5}, Landroid/net/IpPrefix;-><init>(Ljava/net/InetAddress;I)V

    iput-object v6, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mDestination:Landroid/net/IpPrefix;

    goto :goto_0

    .line 133
    :cond_4
    sget v6, Landroid/system/OsConstants;->AF_INET6:I

    if-ne v1, v6, :cond_8

    .line 134
    new-instance v6, Landroid/net/IpPrefix;

    sget-object v7, Lcom/android/net/module/util/NetworkStackConstants;->IPV6_ADDR_ANY:Ljava/net/Inet6Address;

    invoke-direct {v6, v7, v5}, Landroid/net/IpPrefix;-><init>(Ljava/net/InetAddress;I)V

    iput-object v6, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mDestination:Landroid/net/IpPrefix;

    .line 140
    :goto_0
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 141
    const/4 v6, 0x5

    invoke-static {v6, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v4

    .line 142
    if-eqz v4, :cond_6

    .line 143
    invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;

    move-result-object v6

    iput-object v6, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mGateway:Ljava/net/InetAddress;

    .line 145
    if-nez v6, :cond_5

    return-object v2

    .line 147
    :cond_5
    invoke-static {v6, v1}, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->matchRouteAddressFamily(Ljava/net/InetAddress;I)Z

    move-result v6

    if-nez v6, :cond_6

    return-object v2

    .line 151
    :cond_6
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 152
    const/4 v2, 0x4

    invoke-static {v2, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 153
    .end local v4    # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-eqz v2, :cond_7

    .line 158
    invoke-virtual {v2, v5}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInt(I)I

    move-result v4

    iput v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I

    .line 161
    :cond_7
    return-object v0

    .line 136
    .end local v2    # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    .restart local v4    # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    :cond_8
    return-object v2
.end method


# virtual methods
.method public getDestination()Landroid/net/IpPrefix;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mDestination:Landroid/net/IpPrefix;

    return-object v0
.end method

.method public getGateway()Ljava/net/InetAddress;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mGateway:Ljava/net/InetAddress;

    return-object v0
.end method

.method public getInterfaceIndex()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I

    return v0
.end method

.method public getRtMsgHeader()Lcom/android/net/module/util/netlink/StructRtMsg;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mRtmsg:Lcom/android/net/module/util/netlink/StructRtMsg;

    return-object v0
.end method

.method protected pack(Ljava/nio/ByteBuffer;)V
    .locals 4
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 169
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 170
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mRtmsg:Lcom/android/net/module/util/netlink/StructRtMsg;

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructRtMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 172
    new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr;

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mDestination:Landroid/net/IpPrefix;

    invoke-virtual {v1}, Landroid/net/IpPrefix;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V

    .line 173
    .local v0, "destination":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 175
    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mGateway:Ljava/net/InetAddress;

    if-eqz v1, :cond_0

    .line 176
    new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v3, 0x5

    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[B)V

    move-object v1, v2

    .line 177
    .local v1, "gateway":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 179
    .end local v1    # "gateway":Lcom/android/net/module/util/netlink/StructNlAttr;
    :cond_0
    iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I

    if-eqz v1, :cond_1

    .line 180
    new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SI)V

    move-object v1, v2

    .line 181
    .local v1, "ifindex":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 183
    .end local v1    # "ifindex":Lcom/android/net/module/util/netlink/StructNlAttr;
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RtNetlinkRouteMessage{ nlmsghdr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    sget v2, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    .line 188
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, Rtmsg{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mRtmsg:Lcom/android/net/module/util/netlink/StructRtMsg;

    .line 189
    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructRtMsg;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, destination{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mDestination:Landroid/net/IpPrefix;

    .line 190
    invoke-virtual {v1}, Landroid/net/IpPrefix;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, gateway{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 191
    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mGateway:Ljava/net/InetAddress;

    if-nez v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, ifindex{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->mIfindex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "} }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 187
    return-object v0
.end method
