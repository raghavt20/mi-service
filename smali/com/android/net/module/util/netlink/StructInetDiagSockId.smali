.class public Lcom/android/net/module/util/netlink/StructInetDiagSockId;
.super Ljava/lang/Object;
.source "StructInetDiagSockId.java"


# static fields
.field private static final INET_DIAG_NOCOOKIE:J = -0x1L

.field private static final IPV4_PADDING:[B

.field public static final STRUCT_SIZE:I = 0x30

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final cookie:J

.field public final ifIndex:I

.field public final locSocketAddress:Ljava/net/InetSocketAddress;

.field public final remSocketAddress:Ljava/net/InetSocketAddress;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 57
    const-class v0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->TAG:Ljava/lang/String;

    .line 61
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->IPV4_PADDING:[B

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;)V
    .locals 6
    .param p1, "loc"    # Ljava/net/InetSocketAddress;
    .param p2, "rem"    # Ljava/net/InetSocketAddress;

    .line 69
    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;-><init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IJ)V

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IJ)V
    .locals 0
    .param p1, "loc"    # Ljava/net/InetSocketAddress;
    .param p2, "rem"    # Ljava/net/InetSocketAddress;
    .param p3, "ifIndex"    # I
    .param p4, "cookie"    # J

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->locSocketAddress:Ljava/net/InetSocketAddress;

    .line 75
    iput-object p2, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->remSocketAddress:Ljava/net/InetSocketAddress;

    .line 76
    iput p3, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->ifIndex:I

    .line 77
    iput-wide p4, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->cookie:J

    .line 78
    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;S)Lcom/android/net/module/util/netlink/StructInetDiagSockId;
    .locals 14
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;
    .param p1, "family"    # S

    .line 85
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x30

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    .line 86
    return-object v2

    .line 89
    :cond_0
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 90
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->toUnsignedInt(S)I

    move-result v0

    .line 91
    .local v0, "srcPort":I
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    invoke-static {v1}, Ljava/lang/Short;->toUnsignedInt(S)I

    move-result v1

    .line 95
    .local v1, "dstPort":I
    sget v3, Landroid/system/OsConstants;->AF_INET:I

    const-string v4, "Failed to parse address: "

    if-ne p1, v3, :cond_1

    .line 96
    const/4 v3, 0x4

    new-array v5, v3, [B

    .line 97
    .local v5, "srcAddrByte":[B
    new-array v3, v3, [B

    .line 98
    .local v3, "dstAddrByte":[B
    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 101
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/lit8 v6, v6, 0xc

    invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 102
    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 103
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/lit8 v6, v6, 0xc

    invoke-virtual {p0, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 105
    :try_start_0
    invoke-static {v5}, Ljava/net/Inet4Address;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v6

    .line 106
    .local v6, "srcAddr":Ljava/net/InetAddress;
    invoke-static {v3}, Ljava/net/Inet4Address;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v2
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    .local v2, "dstAddr":Ljava/net/InetAddress;
    nop

    .line 111
    .end local v3    # "dstAddrByte":[B
    .end local v5    # "srcAddrByte":[B
    goto :goto_0

    .line 107
    .end local v2    # "dstAddr":Ljava/net/InetAddress;
    .end local v6    # "srcAddr":Ljava/net/InetAddress;
    .restart local v3    # "dstAddrByte":[B
    .restart local v5    # "srcAddrByte":[B
    :catch_0
    move-exception v6

    .line 108
    .local v6, "e":Ljava/net/UnknownHostException;
    sget-object v7, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    return-object v2

    .line 111
    .end local v3    # "dstAddrByte":[B
    .end local v5    # "srcAddrByte":[B
    .end local v6    # "e":Ljava/net/UnknownHostException;
    :cond_1
    sget v3, Landroid/system/OsConstants;->AF_INET6:I

    if-ne p1, v3, :cond_2

    .line 112
    const/16 v3, 0x10

    new-array v5, v3, [B

    .line 113
    .restart local v5    # "srcAddrByte":[B
    new-array v3, v3, [B

    .line 114
    .restart local v3    # "dstAddrByte":[B
    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 115
    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 120
    const/4 v6, -0x1

    :try_start_1
    invoke-static {v2, v5, v6}, Ljava/net/Inet6Address;->getByAddress(Ljava/lang/String;[BI)Ljava/net/Inet6Address;

    move-result-object v7

    .line 122
    .local v7, "srcAddr":Ljava/net/InetAddress;
    invoke-static {v2, v3, v6}, Ljava/net/Inet6Address;->getByAddress(Ljava/lang/String;[BI)Ljava/net/Inet6Address;

    move-result-object v2
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    .line 127
    .restart local v2    # "dstAddr":Ljava/net/InetAddress;
    nop

    .line 128
    .end local v3    # "dstAddrByte":[B
    .end local v5    # "srcAddrByte":[B
    move-object v6, v7

    .line 133
    .end local v7    # "srcAddr":Ljava/net/InetAddress;
    .local v6, "srcAddr":Ljava/net/InetAddress;
    :goto_0
    new-instance v8, Ljava/net/InetSocketAddress;

    invoke-direct {v8, v6, v0}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 134
    .local v8, "srcSocketAddr":Ljava/net/InetSocketAddress;
    new-instance v9, Ljava/net/InetSocketAddress;

    invoke-direct {v9, v2, v1}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    .line 136
    .local v9, "dstSocketAddr":Ljava/net/InetSocketAddress;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 137
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    .line 138
    .local v3, "ifIndex":I
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    .line 139
    .local v4, "cookie":J
    new-instance v13, Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    move-object v7, v13

    move v10, v3

    move-wide v11, v4

    invoke-direct/range {v7 .. v12}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;-><init>(Ljava/net/InetSocketAddress;Ljava/net/InetSocketAddress;IJ)V

    return-object v13

    .line 124
    .end local v2    # "dstAddr":Ljava/net/InetAddress;
    .end local v4    # "cookie":J
    .end local v6    # "srcAddr":Ljava/net/InetAddress;
    .end local v8    # "srcSocketAddr":Ljava/net/InetSocketAddress;
    .end local v9    # "dstSocketAddr":Ljava/net/InetSocketAddress;
    .local v3, "dstAddrByte":[B
    .restart local v5    # "srcAddrByte":[B
    :catch_1
    move-exception v6

    .line 125
    .local v6, "e":Ljava/net/UnknownHostException;
    sget-object v7, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    return-object v2

    .line 129
    .end local v3    # "dstAddrByte":[B
    .end local v5    # "srcAddrByte":[B
    .end local v6    # "e":Ljava/net/UnknownHostException;
    :cond_2
    sget-object v3, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid address family: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    return-object v2
.end method


# virtual methods
.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 146
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 147
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->locSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 148
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->remSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 149
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->locSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 150
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->locSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    instance-of v0, v0, Ljava/net/Inet4Address;

    if-eqz v0, :cond_0

    .line 151
    sget-object v0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->IPV4_PADDING:[B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->remSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 154
    iget-object v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->remSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    instance-of v0, v0, Ljava/net/Inet4Address;

    if-eqz v0, :cond_1

    .line 155
    sget-object v0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->IPV4_PADDING:[B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 157
    :cond_1
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 158
    iget v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->ifIndex:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 159
    iget-wide v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->cookie:J

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 160
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StructInetDiagSockId{ idiag_sport{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->locSocketAddress:Ljava/net/InetSocketAddress;

    .line 165
    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_dport{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->remSocketAddress:Ljava/net/InetSocketAddress;

    .line 166
    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_src{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->locSocketAddress:Ljava/net/InetSocketAddress;

    .line 167
    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_dst{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->remSocketAddress:Ljava/net/InetSocketAddress;

    .line 168
    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_if{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->ifIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_cookie{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 171
    iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->cookie:J

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_0

    const-string v1, "INET_DIAG_NOCOOKIE"

    goto :goto_0

    :cond_0
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    return-object v0
.end method
