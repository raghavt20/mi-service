public class com.android.net.module.util.netlink.StructInetDiagReqV2 {
	 /* .source "StructInetDiagReqV2.java" */
	 /* # static fields */
	 public static final Integer INET_DIAG_REQ_V2_ALL_STATES;
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 private final com.android.net.module.util.netlink.StructInetDiagSockId mId;
	 private final Object mIdiagExt;
	 private final Object mPad;
	 private final Object mSdiagFamily;
	 private final Object mSdiagProtocol;
	 private final Integer mState;
	 /* # direct methods */
	 public com.android.net.module.util.netlink.StructInetDiagReqV2 ( ) {
		 /* .locals 1 */
		 /* .param p1, "protocol" # I */
		 /* .param p2, "id" # Lcom/android/net/module/util/netlink/StructInetDiagSockId; */
		 /* .param p3, "family" # I */
		 /* .param p4, "pad" # I */
		 /* .param p5, "extension" # I */
		 /* .param p6, "state" # I */
		 /* .line 51 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 52 */
		 /* int-to-byte v0, p3 */
		 /* iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagFamily:B */
		 /* .line 53 */
		 /* int-to-byte v0, p1 */
		 /* iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagProtocol:B */
		 /* .line 54 */
		 this.mId = p2;
		 /* .line 55 */
		 /* int-to-byte v0, p4 */
		 /* iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mPad:B */
		 /* .line 56 */
		 /* int-to-byte v0, p5 */
		 /* iput-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mIdiagExt:B */
		 /* .line 57 */
		 /* iput p6, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mState:I */
		 /* .line 58 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void pack ( java.nio.ByteBuffer p0 ) {
		 /* .locals 1 */
		 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 65 */
		 /* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagFamily:B */
		 (( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
		 /* .line 66 */
		 /* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagProtocol:B */
		 (( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
		 /* .line 67 */
		 /* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mIdiagExt:B */
		 (( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
		 /* .line 68 */
		 /* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mPad:B */
		 (( java.nio.ByteBuffer ) p1 ).put ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;
		 /* .line 69 */
		 /* iget v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mState:I */
		 (( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
		 /* .line 70 */
		 v0 = this.mId;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 (( com.android.net.module.util.netlink.StructInetDiagSockId ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->pack(Ljava/nio/ByteBuffer;)V
			 /* .line 71 */
		 } // :cond_0
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 4 */
		 /* .line 75 */
		 /* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagFamily:B */
		 com.android.net.module.util.netlink.NetlinkConstants .stringForAddressFamily ( v0 );
		 /* .line 76 */
		 /* .local v0, "familyStr":Ljava/lang/String; */
		 /* iget-byte v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mSdiagProtocol:B */
		 com.android.net.module.util.netlink.NetlinkConstants .stringForAddressFamily ( v1 );
		 /* .line 78 */
		 /* .local v1, "protocolStr":Ljava/lang/String; */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "StructInetDiagReqV2{ sdiag_family{"; // const-string v3, "StructInetDiagReqV2{ sdiag_family{"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const-string/jumbo v3, "}, sdiag_protocol{" */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const-string/jumbo v3, "}, idiag_ext{" */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-byte v3, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mIdiagExt:B */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v3 = ")}, pad{"; // const-string v3, ")}, pad{"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-byte v3, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mPad:B */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 /* const-string/jumbo v3, "}, idiag_states{" */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v3, p0, Lcom/android/net/module/util/netlink/StructInetDiagReqV2;->mState:I */
		 /* .line 83 */
		 java.lang.Integer .toHexString ( v3 );
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const-string/jumbo v3, "}, " */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 84 */
		 v3 = this.mId;
		 if ( v3 != null) { // if-eqz v3, :cond_0
			 (( com.android.net.module.util.netlink.StructInetDiagSockId ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->toString()Ljava/lang/String;
		 } // :cond_0
		 final String v3 = "inet_diag_sockid=null"; // const-string v3, "inet_diag_sockid=null"
	 } // :goto_0
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* const-string/jumbo v3, "}" */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 78 */
} // .end method
