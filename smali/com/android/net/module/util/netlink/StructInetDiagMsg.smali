.class public Lcom/android/net/module/util/netlink/StructInetDiagMsg;
.super Ljava/lang/Object;
.source "StructInetDiagMsg.java"


# static fields
.field public static final STRUCT_SIZE:I = 0x48


# instance fields
.field public id:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

.field public idiag_expires:J

.field public idiag_family:S

.field public idiag_inode:J

.field public idiag_retrans:S

.field public idiag_rqueue:J

.field public idiag_state:S

.field public idiag_timer:S

.field public idiag_uid:I

.field public idiag_wqueue:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructInetDiagMsg;
    .locals 3
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 68
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x48

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    .line 69
    return-object v2

    .line 71
    :cond_0
    new-instance v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;

    invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;-><init>()V

    .line 72
    .local v0, "struct":Lcom/android/net/module/util/netlink/StructInetDiagMsg;
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    invoke-static {v1}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->unsignedByte(B)S

    move-result v1

    iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S

    .line 73
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    invoke-static {v1}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->unsignedByte(B)S

    move-result v1

    iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_state:S

    .line 74
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    invoke-static {v1}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->unsignedByte(B)S

    move-result v1

    iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_timer:S

    .line 75
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    invoke-static {v1}, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->unsignedByte(B)S

    move-result v1

    iput-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_retrans:S

    .line 76
    iget-short v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S

    invoke-static {p0, v1}, Lcom/android/net/module/util/netlink/StructInetDiagSockId;->parse(Ljava/nio/ByteBuffer;S)Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    move-result-object v1

    iput-object v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->id:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    .line 77
    if-nez v1, :cond_1

    .line 78
    return-object v2

    .line 80
    :cond_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toUnsignedLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_expires:J

    .line 81
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toUnsignedLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_rqueue:J

    .line 82
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toUnsignedLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_wqueue:J

    .line 83
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I

    .line 84
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toUnsignedLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_inode:J

    .line 85
    return-object v0
.end method

.method private static unsignedByte(B)S
    .locals 1
    .param p0, "b"    # B

    .line 60
    and-int/lit16 v0, p0, 0xff

    int-to-short v0, v0

    return v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StructInetDiagMsg{ idiag_family{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_family:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_state{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_state:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_timer{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_timer:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_retrans{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_retrans:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, id{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->id:Lcom/android/net/module/util/netlink/StructInetDiagSockId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_expires{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_expires:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_rqueue{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_rqueue:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_wqueue{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_wqueue:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_uid{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_uid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, idiag_inode{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/net/module/util/netlink/StructInetDiagMsg;->idiag_inode:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
