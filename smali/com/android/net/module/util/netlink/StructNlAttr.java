public class com.android.net.module.util.netlink.StructNlAttr {
	 /* .source "StructNlAttr.java" */
	 /* # static fields */
	 public static final Integer NLA_F_NESTED;
	 public static final Integer NLA_HEADERLEN;
	 /* # instance fields */
	 public Object nla_len;
	 public Object nla_type;
	 public nla_value;
	 /* # direct methods */
	 public com.android.net.module.util.netlink.StructNlAttr ( ) {
		 /* .locals 1 */
		 /* .line 139 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 135 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
		 /* .line 139 */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNlAttr ( ) {
		 /* .locals 2 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # B */
		 /* .line 141 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 135 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
		 /* .line 142 */
		 /* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
		 /* .line 143 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* new-array v0, v0, [B */
		 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
		 /* .line 144 */
		 v0 = this.nla_value;
		 int v1 = 0; // const/4 v1, 0x0
		 /* aput-byte p2, v0, v1 */
		 /* .line 145 */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNlAttr ( ) {
		 /* .locals 1 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # I */
		 /* .line 165 */
		 java.nio.ByteOrder .nativeOrder ( );
		 /* invoke-direct {p0, p1, p2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SILjava/nio/ByteOrder;)V */
		 /* .line 166 */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNlAttr ( ) {
		 /* .locals 3 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # I */
		 /* .param p3, "order" # Ljava/nio/ByteOrder; */
		 /* .line 168 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 135 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
		 /* .line 169 */
		 /* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
		 /* .line 170 */
		 /* new-array v0, v0, [B */
		 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
		 /* .line 171 */
		 (( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
		 /* .line 172 */
		 /* .local v0, "buf":Ljava/nio/ByteBuffer; */
		 (( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
		 /* .line 174 */
		 /* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
		 try { // :try_start_0
			 (( java.nio.ByteBuffer ) v0 ).order ( p3 ); // invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 175 */
			 (( java.nio.ByteBuffer ) v0 ).putInt ( p2 ); // invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 177 */
			 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 178 */
			 /* nop */
			 /* .line 179 */
			 return;
			 /* .line 177 */
			 /* :catchall_0 */
			 /* move-exception v2 */
			 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 178 */
			 /* throw v2 */
		 } // .end method
		 public com.android.net.module.util.netlink.StructNlAttr ( ) {
			 /* .locals 1 */
			 /* .param p1, "type" # S */
			 /* .param p2, "mac" # Landroid/net/MacAddress; */
			 /* .line 191 */
			 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
			 /* .line 135 */
			 int v0 = 4; // const/4 v0, 0x4
			 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
			 /* .line 192 */
			 /* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
			 /* .line 193 */
			 (( android.net.MacAddress ) p2 ).toByteArray ( ); // invoke-virtual {p2}, Landroid/net/MacAddress;->toByteArray()[B
			 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
			 /* .line 194 */
			 return;
		 } // .end method
		 public com.android.net.module.util.netlink.StructNlAttr ( ) {
			 /* .locals 3 */
			 /* .param p1, "type" # S */
			 /* .param p2, "string" # Ljava/lang/String; */
			 /* .line 196 */
			 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
			 /* .line 135 */
			 int v0 = 4; // const/4 v0, 0x4
			 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
			 /* .line 197 */
			 /* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
			 /* .line 198 */
			 int v0 = 0; // const/4 v0, 0x0
			 /* .line 200 */
			 /* .local v0, "value":[B */
			 try { // :try_start_0
				 final String v1 = "UTF-8"; // const-string v1, "UTF-8"
				 (( java.lang.String ) p2 ).getBytes ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
				 /* .line 202 */
				 /* .local v1, "stringBytes":[B */
				 /* array-length v2, v1 */
				 /* add-int/lit8 v2, v2, 0x1 */
				 java.util.Arrays .copyOf ( v1,v2 );
				 /* :try_end_0 */
				 /* .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 ..:try_end_0} :catch_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* move-object v0, v2 */
			 } // .end local v1 # "stringBytes":[B
			 /* .line 206 */
			 /* :catchall_0 */
			 /* move-exception v1 */
			 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
			 /* .line 207 */
			 /* throw v1 */
			 /* .line 203 */
			 /* :catch_0 */
			 /* move-exception v1 */
			 /* .line 206 */
		 } // :goto_0
		 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
		 /* .line 207 */
		 /* nop */
		 /* .line 208 */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNlAttr ( ) {
		 /* .locals 1 */
		 /* .param p1, "type" # S */
		 /* .param p2, "ip" # Ljava/net/InetAddress; */
		 /* .line 186 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 135 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
		 /* .line 187 */
		 /* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
		 /* .line 188 */
		 (( java.net.InetAddress ) p2 ).getAddress ( ); // invoke-virtual {p2}, Ljava/net/InetAddress;->getAddress()[B
		 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
		 /* .line 189 */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNlAttr ( ) {
		 /* .locals 1 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # S */
		 /* .line 148 */
		 java.nio.ByteOrder .nativeOrder ( );
		 /* invoke-direct {p0, p1, p2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V */
		 /* .line 149 */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNlAttr ( ) {
		 /* .locals 3 */
		 /* .param p1, "type" # S */
		 /* .param p2, "value" # S */
		 /* .param p3, "order" # Ljava/nio/ByteOrder; */
		 /* .line 151 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 135 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
		 /* .line 152 */
		 /* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
		 /* .line 153 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* new-array v0, v0, [B */
		 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
		 /* .line 154 */
		 (( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
		 /* .line 155 */
		 /* .local v0, "buf":Ljava/nio/ByteBuffer; */
		 (( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
		 /* .line 157 */
		 /* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
		 try { // :try_start_0
			 (( java.nio.ByteBuffer ) v0 ).order ( p3 ); // invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 158 */
			 (( java.nio.ByteBuffer ) v0 ).putShort ( p2 ); // invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
			 /* :try_end_0 */
			 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
			 /* .line 160 */
			 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 161 */
			 /* nop */
			 /* .line 162 */
			 return;
			 /* .line 160 */
			 /* :catchall_0 */
			 /* move-exception v2 */
			 (( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
			 /* .line 161 */
			 /* throw v2 */
		 } // .end method
		 public com.android.net.module.util.netlink.StructNlAttr ( ) {
			 /* .locals 1 */
			 /* .param p1, "type" # S */
			 /* .param p2, "value" # [B */
			 /* .line 181 */
			 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
			 /* .line 135 */
			 int v0 = 4; // const/4 v0, 0x4
			 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
			 /* .line 182 */
			 /* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
			 /* .line 183 */
			 /* invoke-direct {p0, p2}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
			 /* .line 184 */
			 return;
		 } // .end method
		 public com.android.net.module.util.netlink.StructNlAttr ( ) {
			 /* .locals 6 */
			 /* .param p1, "type" # S */
			 /* .param p2, "nested" # [Lcom/android/net/module/util/netlink/StructNlAttr; */
			 /* .line 211 */
			 /* invoke-direct {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>()V */
			 /* .line 212 */
			 v0 = 			 com.android.net.module.util.netlink.StructNlAttr .makeNestedType ( p1 );
			 /* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
			 /* .line 214 */
			 int v0 = 0; // const/4 v0, 0x0
			 /* .line 215 */
			 /* .local v0, "payloadLength":I */
			 /* array-length v1, p2 */
			 int v2 = 0; // const/4 v2, 0x0
			 /* move v3, v2 */
		 } // :goto_0
		 /* if-ge v3, v1, :cond_0 */
		 /* aget-object v4, p2, v3 */
		 /* .local v4, "nla":Lcom/android/net/module/util/netlink/StructNlAttr; */
		 v5 = 		 (( com.android.net.module.util.netlink.StructNlAttr ) v4 ).getAlignedLength ( ); // invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
		 /* add-int/2addr v0, v5 */
	 } // .end local v4 # "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
	 /* add-int/lit8 v3, v3, 0x1 */
	 /* .line 216 */
} // :cond_0
/* new-array v1, v0, [B */
/* invoke-direct {p0, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;->setValue([B)V */
/* .line 218 */
(( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 219 */
/* .local v1, "buf":Ljava/nio/ByteBuffer; */
/* array-length v3, p2 */
} // :goto_1
/* if-ge v2, v3, :cond_1 */
/* aget-object v4, p2, v2 */
/* .line 220 */
/* .restart local v4 # "nla":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v4 ).pack ( v1 ); // invoke-virtual {v4, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 219 */
} // .end local v4 # "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 222 */
} // :cond_1
return;
} // .end method
public static com.android.net.module.util.netlink.StructNlAttr findNextAttrOfType ( Object p0, java.nio.ByteBuffer p1 ) {
/* .locals 3 */
/* .param p0, "attrType" # S */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 119 */
/* nop */
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_3
v0 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
/* if-lez v0, :cond_3 */
/* .line 120 */
com.android.net.module.util.netlink.StructNlAttr .peek ( p1 );
/* .line 121 */
/* .local v0, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
/* if-nez v0, :cond_0 */
/* .line 122 */
/* .line 124 */
} // :cond_0
/* iget-short v1, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
/* if-ne v1, p0, :cond_1 */
/* .line 125 */
com.android.net.module.util.netlink.StructNlAttr .parse ( p1 );
/* .line 127 */
} // :cond_1
v1 = (( java.nio.ByteBuffer ) p1 ).remaining ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I
v2 = (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
/* if-ge v1, v2, :cond_2 */
/* .line 128 */
/* .line 130 */
} // :cond_2
v1 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
v2 = (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v1, v2 */
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 131 */
} // .end local v0 # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
/* .line 132 */
} // :cond_3
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Object makeNestedType ( Object p0 ) {
/* .locals 1 */
/* .param p0, "type" # S */
/* .line 47 */
/* const v0, 0x8000 */
/* or-int/2addr v0, p0 */
/* int-to-short v0, v0 */
} // .end method
public static com.android.net.module.util.netlink.StructNlAttr parse ( java.nio.ByteBuffer p0 ) {
/* .locals 5 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 90 */
com.android.net.module.util.netlink.StructNlAttr .peek ( p0 );
/* .line 91 */
/* .local v0, "struct":Lcom/android/net/module/util/netlink/StructNlAttr; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
v2 = (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
/* if-ge v1, v2, :cond_0 */
/* .line 95 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 96 */
/* .local v1, "baseOffset":I */
/* add-int/lit8 v2, v1, 0x4 */
(( java.nio.ByteBuffer ) p0 ).position ( v2 ); // invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 98 */
/* iget-short v2, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
/* const v3, 0xffff */
/* and-int/2addr v2, v3 */
/* .line 99 */
/* .local v2, "valueLen":I */
/* add-int/lit8 v2, v2, -0x4 */
/* .line 100 */
/* if-lez v2, :cond_1 */
/* .line 101 */
/* new-array v3, v2, [B */
this.nla_value = v3;
/* .line 102 */
int v4 = 0; // const/4 v4, 0x0
(( java.nio.ByteBuffer ) p0 ).get ( v3, v4, v2 ); // invoke-virtual {p0, v3, v4, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
/* .line 103 */
v3 = (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v3, v1 */
(( java.nio.ByteBuffer ) p0 ).position ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 105 */
} // :cond_1
/* .line 92 */
} // .end local v1 # "baseOffset":I
} // .end local v2 # "valueLen":I
} // :cond_2
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public static com.android.net.module.util.netlink.StructNlAttr peek ( java.nio.ByteBuffer p0 ) {
/* .locals 6 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 59 */
int v0 = 0; // const/4 v0, 0x0
if ( p0 != null) { // if-eqz p0, :cond_2
v1 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 4; // const/4 v2, 0x4
/* if-ge v1, v2, :cond_0 */
/* .line 62 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 64 */
/* .local v1, "baseOffset":I */
/* new-instance v3, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* invoke-direct {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>()V */
/* .line 65 */
/* .local v3, "struct":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( java.nio.ByteBuffer ) p0 ).order ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 66 */
/* .local v4, "originalOrder":Ljava/nio/ByteOrder; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) p0 ).order ( v5 ); // invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 68 */
try { // :try_start_0
v5 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v5, v3, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
/* .line 69 */
v5 = (( java.nio.ByteBuffer ) p0 ).getShort ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S
/* iput-short v5, v3, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 71 */
(( java.nio.ByteBuffer ) p0 ).order ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 72 */
/* nop */
/* .line 74 */
(( java.nio.ByteBuffer ) p0 ).position ( v1 ); // invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 75 */
/* iget-short v5, v3, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
/* if-ge v5, v2, :cond_1 */
/* .line 77 */
/* .line 79 */
} // :cond_1
/* .line 71 */
/* :catchall_0 */
/* move-exception v0 */
(( java.nio.ByteBuffer ) p0 ).order ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 72 */
/* throw v0 */
/* .line 60 */
} // .end local v1 # "baseOffset":I
} // .end local v3 # "struct":Lcom/android/net/module/util/netlink/StructNlAttr;
} // .end local v4 # "originalOrder":Ljava/nio/ByteOrder;
} // :cond_2
} // :goto_0
} // .end method
private void setValue ( Object[] p0 ) {
/* .locals 1 */
/* .param p1, "value" # [B */
/* .line 382 */
this.nla_value = p1;
/* .line 383 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* array-length v0, p1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* add-int/lit8 v0, v0, 0x4 */
/* int-to-short v0, v0 */
/* iput-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
/* .line 384 */
return;
} // .end method
/* # virtual methods */
public Integer getAlignedLength ( ) {
/* .locals 1 */
/* .line 228 */
/* iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
v0 = com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v0 );
} // .end method
public Object getValueAsBe16 ( Object p0 ) {
/* .locals 3 */
/* .param p1, "defaultValue" # S */
/* .line 235 */
(( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 236 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 2; // const/4 v2, 0x2
/* if-eq v1, v2, :cond_0 */
/* .line 239 */
} // :cond_0
(( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 241 */
/* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
try { // :try_start_0
v2 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) v0 ).order ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 242 */
v2 = (( java.nio.ByteBuffer ) v0 ).getShort ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 244 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 242 */
/* .line 244 */
/* :catchall_0 */
/* move-exception v2 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 245 */
/* throw v2 */
/* .line 237 */
} // .end local v1 # "originalOrder":Ljava/nio/ByteOrder;
} // :cond_1
} // :goto_0
} // .end method
public Integer getValueAsBe32 ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "defaultValue" # I */
/* .line 252 */
(( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 253 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 4; // const/4 v2, 0x4
/* if-eq v1, v2, :cond_0 */
/* .line 256 */
} // :cond_0
(( java.nio.ByteBuffer ) v0 ).order ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 258 */
/* .local v1, "originalOrder":Ljava/nio/ByteOrder; */
try { // :try_start_0
v2 = java.nio.ByteOrder.BIG_ENDIAN;
(( java.nio.ByteBuffer ) v0 ).order ( v2 ); // invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 259 */
v2 = (( java.nio.ByteBuffer ) v0 ).getInt ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 261 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 259 */
/* .line 261 */
/* :catchall_0 */
/* move-exception v2 */
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 262 */
/* throw v2 */
/* .line 254 */
} // .end local v1 # "originalOrder":Ljava/nio/ByteOrder;
} // :cond_1
} // :goto_0
} // .end method
public Object getValueAsByte ( Object p0 ) {
/* .locals 3 */
/* .param p1, "defaultValue" # B */
/* .line 282 */
(( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 283 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 1; // const/4 v2, 0x1
/* if-eq v1, v2, :cond_0 */
/* .line 286 */
} // :cond_0
(( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
v1 = (( java.nio.ByteBuffer ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B
/* .line 284 */
} // :cond_1
} // :goto_0
} // .end method
public java.nio.ByteBuffer getValueAsByteBuffer ( ) {
/* .locals 2 */
/* .line 269 */
v0 = this.nla_value;
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 270 */
} // :cond_0
java.nio.ByteBuffer .wrap ( v0 );
/* .line 274 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v0 ).order ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 275 */
} // .end method
public java.net.InetAddress getValueAsInetAddress ( ) {
/* .locals 2 */
/* .line 316 */
v0 = this.nla_value;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 319 */
} // :cond_0
try { // :try_start_0
java.net.InetAddress .getByAddress ( v0 );
/* :try_end_0 */
/* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 320 */
/* :catch_0 */
/* move-exception v0 */
/* .line 321 */
/* .local v0, "ignored":Ljava/net/UnknownHostException; */
} // .end method
public Integer getValueAsInt ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "defaultValue" # I */
/* .line 304 */
(( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsInteger ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInteger()Ljava/lang/Integer;
/* .line 305 */
/* .local v0, "value":Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // :cond_0
/* move v1, p1 */
} // :goto_0
} // .end method
public java.lang.Integer getValueAsInteger ( ) {
/* .locals 3 */
/* .line 293 */
(( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getValueAsByteBuffer ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
/* .line 294 */
/* .local v0, "byteBuffer":Ljava/nio/ByteBuffer; */
if ( v0 != null) { // if-eqz v0, :cond_1
v1 = (( java.nio.ByteBuffer ) v0 ).remaining ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I
int v2 = 4; // const/4 v2, 0x4
/* if-eq v1, v2, :cond_0 */
/* .line 297 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) v0 ).getInt ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I
java.lang.Integer .valueOf ( v1 );
/* .line 295 */
} // :cond_1
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public android.net.MacAddress getValueAsMacAddress ( ) {
/* .locals 2 */
/* .line 334 */
v0 = this.nla_value;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 337 */
} // :cond_0
try { // :try_start_0
android.net.MacAddress .fromBytes ( v0 );
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 338 */
/* :catch_0 */
/* move-exception v0 */
/* .line 339 */
/* .local v0, "ignored":Ljava/lang/IllegalArgumentException; */
} // .end method
public java.lang.String getValueAsString ( ) {
/* .locals 5 */
/* .line 350 */
v0 = this.nla_value;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 353 */
} // :cond_0
/* array-length v2, v0 */
/* iget-short v3, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
/* add-int/lit8 v4, v3, -0x4 */
/* add-int/lit8 v4, v4, -0x1 */
/* if-ge v2, v4, :cond_1 */
/* .line 356 */
} // :cond_1
/* add-int/lit8 v3, v3, -0x4 */
/* add-int/lit8 v3, v3, -0x1 */
try { // :try_start_0
java.util.Arrays .copyOf ( v0,v3 );
/* .line 357 */
/* .local v0, "array":[B */
/* new-instance v2, Ljava/lang/String; */
final String v3 = "UTF-8"; // const-string v3, "UTF-8"
/* invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 358 */
} // .end local v0 # "array":[B
/* :catch_0 */
/* move-exception v0 */
/* .line 359 */
/* .local v0, "ignored":Ljava/lang/Exception; */
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 3 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 367 */
(( java.nio.ByteBuffer ) p1 ).order ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;
/* .line 368 */
/* .local v0, "originalOrder":Ljava/nio/ByteOrder; */
v1 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* .line 370 */
/* .local v1, "originalPosition":I */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) p1 ).order ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 372 */
try { // :try_start_0
/* iget-short v2, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 373 */
/* iget-short v2, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
(( java.nio.ByteBuffer ) p1 ).putShort ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 374 */
v2 = this.nla_value;
if ( v2 != null) { // if-eqz v2, :cond_0
(( java.nio.ByteBuffer ) p1 ).put ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 376 */
} // :cond_0
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 377 */
/* nop */
/* .line 378 */
v2 = (( com.android.net.module.util.netlink.StructNlAttr ) p0 ).getAlignedLength ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v2, v1 */
(( java.nio.ByteBuffer ) p1 ).position ( v2 ); // invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 379 */
return;
/* .line 376 */
/* :catchall_0 */
/* move-exception v2 */
(( java.nio.ByteBuffer ) p1 ).order ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 377 */
/* throw v2 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 388 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StructNlAttr{ nla_len{"; // const-string v1, "StructNlAttr{ nla_len{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nla_type{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-short v1, p0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nla_value{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.nla_value;
/* .line 391 */
com.android.net.module.util.netlink.NetlinkConstants .hexify ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 388 */
} // .end method
