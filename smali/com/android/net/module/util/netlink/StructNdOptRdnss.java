public class com.android.net.module.util.netlink.StructNdOptRdnss extends com.android.net.module.util.netlink.NdOption {
	 /* .source "StructNdOptRdnss.java" */
	 /* # static fields */
	 public static final Object MIN_OPTION_LEN;
	 private static final java.lang.String TAG;
	 public static final Integer TYPE;
	 /* # instance fields */
	 public final com.android.net.module.util.structs.RdnssOption header;
	 public final java.net.Inet6Address servers;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.StructNdOptRdnss ( ) {
		 /* .locals 1 */
		 /* .line 52 */
		 /* const-class v0, Lcom/android/net/module/util/netlink/StructNdOptRdnss; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNdOptRdnss ( ) {
		 /* .locals 7 */
		 /* .param p1, "servers" # [Ljava/net/Inet6Address; */
		 /* .param p2, "lifetime" # J */
		 /* .line 62 */
		 /* array-length v0, p1 */
		 /* mul-int/lit8 v0, v0, 0x2 */
		 /* add-int/lit8 v0, v0, 0x1 */
		 /* const/16 v1, 0x19 */
		 /* invoke-direct {p0, v1, v0}, Lcom/android/net/module/util/netlink/NdOption;-><init>(BI)V */
		 /* .line 64 */
		 final String v0 = "Recursive DNS Servers address array must not be null"; // const-string v0, "Recursive DNS Servers address array must not be null"
		 java.util.Objects .requireNonNull ( p1,v0 );
		 /* .line 65 */
		 /* array-length v0, p1 */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 69 */
			 /* new-instance v0, Lcom/android/net/module/util/structs/RdnssOption; */
			 /* const/16 v2, 0x19 */
			 /* array-length v1, p1 */
			 /* mul-int/lit8 v1, v1, 0x2 */
			 /* add-int/lit8 v1, v1, 0x1 */
			 /* int-to-byte v3, v1 */
			 int v4 = 0; // const/4 v4, 0x0
			 /* move-object v1, v0 */
			 /* move-wide v5, p2 */
			 /* invoke-direct/range {v1 ..v6}, Lcom/android/net/module/util/structs/RdnssOption;-><init>(BBSJ)V */
			 this.header = v0;
			 /* .line 71 */
			 (( java.net.Inet6Address ) p1 ).clone ( ); // invoke-virtual {p1}, [Ljava/net/Inet6Address;->clone()Ljava/lang/Object;
			 /* check-cast v0, [Ljava/net/Inet6Address; */
			 this.servers = v0;
			 /* .line 72 */
			 return;
			 /* .line 66 */
		 } // :cond_0
		 /* new-instance v0, Ljava/lang/IllegalArgumentException; */
		 final String v1 = "DNS server address array must not be empty"; // const-string v1, "DNS server address array must not be empty"
		 /* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
	 } // .end method
	 public static com.android.net.module.util.netlink.StructNdOptRdnss parse ( java.nio.ByteBuffer p0 ) {
		 /* .locals 7 */
		 /* .param p0, "buf" # Ljava/nio/ByteBuffer; */
		 /* .line 82 */
		 int v0 = 0; // const/4 v0, 0x0
		 if ( p0 != null) { // if-eqz p0, :cond_4
			 v1 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v2, 0x18 */
			 /* if-ge v1, v2, :cond_0 */
			 /* goto/16 :goto_1 */
			 /* .line 84 */
		 } // :cond_0
		 try { // :try_start_0
			 /* const-class v1, Lcom/android/net/module/util/structs/RdnssOption; */
			 com.android.net.module.util.Struct .parse ( v1,p0 );
			 /* check-cast v1, Lcom/android/net/module/util/structs/RdnssOption; */
			 /* .line 85 */
			 /* .local v1, "header":Lcom/android/net/module/util/structs/RdnssOption; */
			 /* iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->type:B */
			 /* const/16 v3, 0x19 */
			 /* if-ne v2, v3, :cond_3 */
			 /* .line 88 */
			 /* iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B */
			 int v3 = 3; // const/4 v3, 0x3
			 /* if-lt v2, v3, :cond_2 */
			 /* iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B */
			 /* rem-int/lit8 v2, v2, 0x2 */
			 if ( v2 != null) { // if-eqz v2, :cond_2
				 /* .line 92 */
				 /* iget-byte v2, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B */
				 /* add-int/lit8 v2, v2, -0x1 */
				 /* div-int/lit8 v2, v2, 0x2 */
				 /* .line 93 */
				 /* .local v2, "numOfDnses":I */
				 /* new-array v3, v2, [Ljava/net/Inet6Address; */
				 /* .line 94 */
				 /* .local v3, "servers":[Ljava/net/Inet6Address; */
				 int v4 = 0; // const/4 v4, 0x0
				 /* .local v4, "i":I */
			 } // :goto_0
			 /* if-ge v4, v2, :cond_1 */
			 /* .line 95 */
			 /* const/16 v5, 0x10 */
			 /* new-array v5, v5, [B */
			 /* .line 96 */
			 /* .local v5, "rawAddress":[B */
			 (( java.nio.ByteBuffer ) p0 ).get ( v5 ); // invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;
			 /* .line 97 */
			 java.net.InetAddress .getByAddress ( v5 );
			 /* check-cast v6, Ljava/net/Inet6Address; */
			 /* aput-object v6, v3, v4 */
			 /* .line 94 */
		 } // .end local v5 # "rawAddress":[B
		 /* add-int/lit8 v4, v4, 0x1 */
		 /* .line 99 */
	 } // .end local v4 # "i":I
} // :cond_1
/* new-instance v4, Lcom/android/net/module/util/netlink/StructNdOptRdnss; */
/* iget-wide v5, v1, Lcom/android/net/module/util/structs/RdnssOption;->lifetime:J */
/* invoke-direct {v4, v3, v5, v6}, Lcom/android/net/module/util/netlink/StructNdOptRdnss;-><init>([Ljava/net/Inet6Address;J)V */
/* .line 89 */
} // .end local v2 # "numOfDnses":I
} // .end local v3 # "servers":[Ljava/net/Inet6Address;
} // :cond_2
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Invalid length "; // const-string v4, "Invalid length "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v4, v1, Lcom/android/net/module/util/structs/RdnssOption;->length:B */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "buf":Ljava/nio/ByteBuffer;
/* throw v2 */
/* .line 86 */
/* .restart local p0 # "buf":Ljava/nio/ByteBuffer; */
} // :cond_3
/* new-instance v2, Ljava/lang/IllegalArgumentException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Invalid type "; // const-string v4, "Invalid type "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v4, v1, Lcom/android/net/module/util/structs/RdnssOption;->type:B */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "buf":Ljava/nio/ByteBuffer;
/* throw v2 */
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/nio/BufferUnderflowException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 100 */
} // .end local v1 # "header":Lcom/android/net/module/util/structs/RdnssOption;
/* .restart local p0 # "buf":Ljava/nio/ByteBuffer; */
/* :catch_0 */
/* move-exception v1 */
/* .line 104 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.android.net.module.util.netlink.StructNdOptRdnss.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Invalid RDNSS option: "; // const-string v4, "Invalid RDNSS option: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v2,v3 );
/* .line 105 */
/* .line 82 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_4
} // :goto_1
} // .end method
/* # virtual methods */
public java.nio.ByteBuffer toByteBuffer ( ) {
/* .locals 2 */
/* .line 118 */
/* const-class v0, Lcom/android/net/module/util/structs/RdnssOption; */
v0 = com.android.net.module.util.Struct .getSize ( v0 );
v1 = this.servers;
/* array-length v1, v1 */
/* mul-int/lit8 v1, v1, 0x10 */
/* add-int/2addr v0, v1 */
java.nio.ByteBuffer .allocate ( v0 );
/* .line 120 */
/* .local v0, "buf":Ljava/nio/ByteBuffer; */
(( com.android.net.module.util.netlink.StructNdOptRdnss ) p0 ).writeToByteBuffer ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/net/module/util/netlink/StructNdOptRdnss;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V
/* .line 121 */
(( java.nio.ByteBuffer ) v0 ).flip ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;
/* .line 122 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 128 */
/* new-instance v0, Ljava/util/StringJoiner; */
final String v1 = "["; // const-string v1, "["
final String v2 = "]"; // const-string v2, "]"
final String v3 = ","; // const-string v3, ","
/* invoke-direct {v0, v3, v1, v2}, Ljava/util/StringJoiner;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V */
/* .line 129 */
/* .local v0, "sj":Ljava/util/StringJoiner; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = this.servers;
/* array-length v3, v2 */
/* if-ge v1, v3, :cond_0 */
/* .line 130 */
/* aget-object v2, v2, v1 */
(( java.net.Inet6Address ) v2 ).getHostAddress ( ); // invoke-virtual {v2}, Ljava/net/Inet6Address;->getHostAddress()Ljava/lang/String;
(( java.util.StringJoiner ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/StringJoiner;->add(Ljava/lang/CharSequence;)Ljava/util/StringJoiner;
/* .line 129 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 132 */
} // .end local v1 # "i":I
} // :cond_0
v1 = this.header;
(( com.android.net.module.util.structs.RdnssOption ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/structs/RdnssOption;->toString()Ljava/lang/String;
(( java.util.StringJoiner ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/StringJoiner;->toString()Ljava/lang/String;
/* filled-new-array {v1, v2}, [Ljava/lang/Object; */
final String v2 = "NdOptRdnss(%s,servers:%s)"; // const-string v2, "NdOptRdnss(%s,servers:%s)"
java.lang.String .format ( v2,v1 );
} // .end method
protected void writeToByteBuffer ( java.nio.ByteBuffer p0 ) {
/* .locals 3 */
/* .param p1, "buf" # Ljava/nio/ByteBuffer; */
/* .line 110 */
v0 = this.header;
(( com.android.net.module.util.structs.RdnssOption ) v0 ).writeToByteBuffer ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/structs/RdnssOption;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V
/* .line 111 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.servers;
/* array-length v2, v1 */
/* if-ge v0, v2, :cond_0 */
/* .line 112 */
/* aget-object v1, v1, v0 */
(( java.net.Inet6Address ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/Inet6Address;->getAddress()[B
(( java.nio.ByteBuffer ) p1 ).put ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;
/* .line 111 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 114 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
