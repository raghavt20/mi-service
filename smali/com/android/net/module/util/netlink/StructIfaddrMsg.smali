.class public Lcom/android/net/module/util/netlink/StructIfaddrMsg;
.super Lcom/android/net/module/util/Struct;
.source "StructIfaddrMsg.java"


# static fields
.field public static final STRUCT_SIZE:I = 0x8


# instance fields
.field public final family:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x0
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final flags:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x2
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final index:I
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x4
        type = .enum Lcom/android/net/module/util/Struct$Type;->S32:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final prefixLen:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x1
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field

.field public final scope:S
    .annotation runtime Lcom/android/net/module/util/Struct$Field;
        order = 0x3
        type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type;
    .end annotation
.end field


# direct methods
.method public constructor <init>(SSSSI)V
    .locals 0
    .param p1, "family"    # S
    .param p2, "prefixLen"    # S
    .param p3, "flags"    # S
    .param p4, "scope"    # S
    .param p5, "index"    # I

    .line 54
    invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V

    .line 55
    iput-short p1, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->family:S

    .line 56
    iput-short p2, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->prefixLen:S

    .line 57
    iput-short p3, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->flags:S

    .line 58
    iput-short p4, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->scope:S

    .line 59
    iput p5, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->index:I

    .line 60
    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructIfaddrMsg;
    .locals 2
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 71
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 74
    :cond_0
    const-class v0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    invoke-static {v0, p0}, Lcom/android/net/module/util/Struct;->parse(Ljava/lang/Class;Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;

    return-object v0
.end method


# virtual methods
.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 82
    invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V

    .line 83
    return-void
.end method
