.class public Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "RtNetlinkNeighborMessage.java"


# static fields
.field public static final NDA_CACHEINFO:S = 0x3s

.field public static final NDA_DST:S = 0x1s

.field public static final NDA_IFINDEX:S = 0x8s

.field public static final NDA_LLADDR:S = 0x2s

.field public static final NDA_MASTER:S = 0x9s

.field public static final NDA_PORT:S = 0x6s

.field public static final NDA_PROBES:S = 0x4s

.field public static final NDA_UNSPEC:S = 0x0s

.field public static final NDA_VLAN:S = 0x5s

.field public static final NDA_VNI:S = 0x7s


# instance fields
.field private mCacheInfo:Lcom/android/net/module/util/netlink/StructNdaCacheInfo;

.field private mDestination:Ljava/net/InetAddress;

.field private mLinkLayerAddr:[B

.field private mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

.field private mNumProbes:I


# direct methods
.method private constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V
    .locals 2
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 163
    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    .line 165
    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    .line 166
    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 167
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I

    .line 168
    iput-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/android/net/module/util/netlink/StructNdaCacheInfo;

    .line 169
    return-void
.end method

.method private getRequiredSpace()I
    .locals 2

    .line 192
    const/16 v0, 0x1c

    .line 193
    .local v0, "spaceRequired":I
    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    if-eqz v1, :cond_0

    .line 194
    nop

    .line 195
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    .line 194
    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    if-eqz v1, :cond_1

    .line 198
    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_1
    return v0
.end method

.method public static newGetNeighborsRequest(I)[B
    .locals 5
    .param p0, "seqNo"    # I

    .line 110
    const/16 v0, 0x1c

    .line 111
    .local v0, "length":I
    const/16 v1, 0x1c

    new-array v2, v1, [B

    .line 112
    .local v2, "bytes":[B
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 113
    .local v3, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 115
    new-instance v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-direct {v4}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V

    .line 116
    .local v4, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    iput v1, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    .line 117
    const/16 v1, 0x1e

    iput-short v1, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    .line 118
    const/16 v1, 0x301

    iput-short v1, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S

    .line 119
    iput p0, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I

    .line 120
    invoke-virtual {v4, v3}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 122
    new-instance v1, Lcom/android/net/module/util/netlink/StructNdMsg;

    invoke-direct {v1}, Lcom/android/net/module/util/netlink/StructNdMsg;-><init>()V

    .line 123
    .local v1, "ndmsg":Lcom/android/net/module/util/netlink/StructNdMsg;
    invoke-virtual {v1, v3}, Lcom/android/net/module/util/netlink/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 125
    return-object v2
.end method

.method public static newNewNeighborMessage(ILjava/net/InetAddress;SI[B)[B
    .locals 5
    .param p0, "seqNo"    # I
    .param p1, "ip"    # Ljava/net/InetAddress;
    .param p2, "nudState"    # S
    .param p3, "ifIndex"    # I
    .param p4, "llAddr"    # [B

    .line 134
    new-instance v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V

    .line 135
    .local v0, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    const/16 v1, 0x1c

    iput-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    .line 136
    const/16 v1, 0x105

    iput-short v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S

    .line 137
    iput p0, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I

    .line 139
    new-instance v1, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;

    invoke-direct {v1, v0}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 140
    .local v1, "msg":Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;
    new-instance v2, Lcom/android/net/module/util/netlink/StructNdMsg;

    invoke-direct {v2}, Lcom/android/net/module/util/netlink/StructNdMsg;-><init>()V

    iput-object v2, v1, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    .line 141
    nop

    .line 142
    instance-of v3, p1, Ljava/net/Inet6Address;

    if-eqz v3, :cond_0

    sget v3, Landroid/system/OsConstants;->AF_INET6:I

    goto :goto_0

    :cond_0
    sget v3, Landroid/system/OsConstants;->AF_INET:I

    :goto_0
    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_family:B

    .line 143
    iget-object v2, v1, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    iput p3, v2, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_ifindex:I

    .line 144
    iget-object v2, v1, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    iput-short p2, v2, Lcom/android/net/module/util/netlink/StructNdMsg;->ndm_state:S

    .line 145
    iput-object p1, v1, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    .line 146
    iput-object p4, v1, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 148
    invoke-direct {v1}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->getRequiredSpace()I

    move-result v2

    new-array v2, v2, [B

    .line 149
    .local v2, "bytes":[B
    array-length v3, v2

    iput v3, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    .line 150
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 151
    .local v3, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 152
    invoke-virtual {v1, v3}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->pack(Ljava/nio/ByteBuffer;)V

    .line 153
    return-object v2
.end method

.method private static packNlAttr(S[BLjava/nio/ByteBuffer;)V
    .locals 2
    .param p0, "nlType"    # S
    .param p1, "nlValue"    # [B
    .param p2, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 207
    new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr;

    invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>()V

    .line 208
    .local v0, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    iput-short p0, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    .line 209
    iput-object p1, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    .line 210
    iget-object v1, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_len:S

    .line 211
    invoke-virtual {v0, p2}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 212
    return-void
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;
    .locals 6
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 62
    new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;

    invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 64
    .local v0, "neighMsg":Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;
    invoke-static {p1}, Lcom/android/net/module/util/netlink/StructNdMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNdMsg;

    move-result-object v1

    iput-object v1, v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    .line 65
    if-nez v1, :cond_0

    .line 66
    const/4 v1, 0x0

    return-object v1

    .line 70
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 71
    .local v1, "baseOffset":I
    const/4 v2, 0x1

    invoke-static {v2, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 72
    .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-eqz v2, :cond_1

    .line 73
    invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;

    move-result-object v3

    iput-object v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    .line 76
    :cond_1
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 77
    const/4 v3, 0x2

    invoke-static {v3, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 78
    if-eqz v2, :cond_2

    .line 79
    iget-object v3, v2, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    iput-object v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 82
    :cond_2
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 83
    const/4 v3, 0x4

    invoke-static {v3, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 84
    if-eqz v2, :cond_3

    .line 85
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInt(I)I

    move-result v3

    iput v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I

    .line 88
    :cond_3
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 89
    const/4 v3, 0x3

    invoke-static {v3, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 90
    if-eqz v2, :cond_4

    .line 91
    invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {v3}, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNdaCacheInfo;

    move-result-object v3

    iput-object v3, v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/android/net/module/util/netlink/StructNdaCacheInfo;

    .line 94
    :cond_4
    const/16 v3, 0x1c

    .line 95
    .local v3, "kMinConsumed":I
    iget-object v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    iget v4, v4, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    add-int/lit8 v4, v4, -0x1c

    invoke-static {v4}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(I)I

    move-result v4

    .line 97
    .local v4, "kAdditionalSpace":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    if-ge v5, v4, :cond_5

    .line 98
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_0

    .line 100
    :cond_5
    add-int v5, v1, v4

    invoke-virtual {p1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103
    :goto_0
    return-object v0
.end method


# virtual methods
.method public getCacheInfo()Lcom/android/net/module/util/netlink/StructNdaCacheInfo;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/android/net/module/util/netlink/StructNdaCacheInfo;

    return-object v0
.end method

.method public getDestination()Ljava/net/InetAddress;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    return-object v0
.end method

.method public getLinkLayerAddress()[B
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    return-object v0
.end method

.method public getNdHeader()Lcom/android/net/module/util/netlink/StructNdMsg;
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    return-object v0
.end method

.method public getProbes()I
    .locals 1

    .line 184
    iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I

    return v0
.end method

.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 218
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 219
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNdMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 221
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    .line 222
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    invoke-static {v1, v0, p1}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->packNlAttr(S[BLjava/nio/ByteBuffer;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    if-eqz v0, :cond_1

    .line 225
    const/4 v1, 0x2

    invoke-static {v1, v0, p1}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->packNlAttr(S[BLjava/nio/ByteBuffer;)V

    .line 227
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .line 231
    iget-object v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mDestination:Ljava/net/InetAddress;

    const-string v1, ""

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "ipLiteral":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RtNetlinkNeighborMessage{ nlmsghdr{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 234
    iget-object v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    if-nez v3, :cond_1

    move-object v3, v1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    sget v4, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, ndmsg{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 235
    iget-object v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNdmsg:Lcom/android/net/module/util/netlink/StructNdMsg;

    if-nez v3, :cond_2

    move-object v3, v1

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNdMsg;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}, destination{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "} linklayeraddr{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mLinkLayerAddr:[B

    .line 237
    invoke-static {v3}, Lcom/android/net/module/util/netlink/NetlinkConstants;->hexify([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "} probes{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mNumProbes:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "} cacheinfo{"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 239
    iget-object v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->mCacheInfo:Lcom/android/net/module/util/netlink/StructNdaCacheInfo;

    if-nez v3, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNdaCacheInfo;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "} }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 232
    return-object v1
.end method
