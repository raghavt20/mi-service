public class com.android.net.module.util.netlink.RtNetlinkAddressMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "RtNetlinkAddressMessage.java" */
	 /* # static fields */
	 public static final Object IFA_ADDRESS;
	 public static final Object IFA_CACHEINFO;
	 public static final Object IFA_FLAGS;
	 /* # instance fields */
	 private Integer mFlags;
	 private com.android.net.module.util.netlink.StructIfacacheInfo mIfacacheInfo;
	 private com.android.net.module.util.netlink.StructIfaddrMsg mIfaddrmsg;
	 private java.net.InetAddress mIpAddress;
	 /* # direct methods */
	 private com.android.net.module.util.netlink.RtNetlinkAddressMessage ( ) {
		 /* .locals 6 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .line 75 */
		 int v2 = 0; // const/4 v2, 0x0
		 int v3 = 0; // const/4 v3, 0x0
		 int v4 = 0; // const/4 v4, 0x0
		 int v5 = 0; // const/4 v5, 0x0
		 /* move-object v0, p0 */
		 /* move-object v1, p1 */
		 /* invoke-direct/range {v0 ..v5}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Lcom/android/net/module/util/netlink/StructIfaddrMsg;Ljava/net/InetAddress;Lcom/android/net/module/util/netlink/StructIfacacheInfo;I)V */
		 /* .line 76 */
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.RtNetlinkAddressMessage ( ) {
		 /* .locals 0 */
		 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* .param p2, "ifaddrMsg" # Lcom/android/net/module/util/netlink/StructIfaddrMsg; */
		 /* .param p3, "ipAddress" # Ljava/net/InetAddress; */
		 /* .param p4, "structIfacacheInfo" # Lcom/android/net/module/util/netlink/StructIfacacheInfo; */
		 /* .param p5, "flags" # I */
		 /* .line 68 */
		 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 69 */
		 this.mIfaddrmsg = p2;
		 /* .line 70 */
		 this.mIpAddress = p3;
		 /* .line 71 */
		 this.mIfacacheInfo = p4;
		 /* .line 72 */
		 /* iput p5, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I */
		 /* .line 73 */
		 return;
	 } // .end method
	 private Integer getRequiredSpace ( ) {
		 /* .locals 2 */
		 /* .line 196 */
		 /* const/16 v0, 0x18 */
		 /* .line 198 */
		 /* .local v0, "spaceRequired":I */
		 v1 = this.mIpAddress;
		 /* .line 199 */
		 (( java.net.InetAddress ) v1 ).getAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B
		 /* array-length v1, v1 */
		 /* add-int/lit8 v1, v1, 0x4 */
		 /* .line 198 */
		 v1 = 		 com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v1 );
		 /* add-int/2addr v0, v1 */
		 /* .line 201 */
		 /* const/16 v1, 0x14 */
		 v1 = 		 com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v1 );
		 /* add-int/2addr v0, v1 */
		 /* .line 204 */
		 /* add-int/lit8 v0, v0, 0x8 */
		 /* .line 205 */
	 } // .end method
	 public static newRtmNewAddressMessage ( Integer p0, java.net.InetAddress p1, Object p2, Integer p3, Object p4, Integer p5, Long p6, Long p7 ) {
		 /* .locals 22 */
		 /* .param p0, "seqNo" # I */
		 /* .param p1, "ip" # Ljava/net/InetAddress; */
		 /* .param p2, "prefixlen" # S */
		 /* .param p3, "flags" # I */
		 /* .param p4, "scope" # B */
		 /* .param p5, "ifIndex" # I */
		 /* .param p6, "preferred" # J */
		 /* .param p8, "valid" # J */
		 /* .line 168 */
		 /* move-object/from16 v0, p1 */
		 final String v1 = "IP address to be set via netlink message cannot be null"; // const-string v1, "IP address to be set via netlink message cannot be null"
		 java.util.Objects .requireNonNull ( v0,v1 );
		 /* .line 170 */
		 /* new-instance v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* invoke-direct {v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V */
		 /* .line 171 */
		 /* .local v1, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
		 /* const/16 v2, 0x14 */
		 /* iput-short v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
		 /* .line 172 */
		 /* const/16 v2, 0x105 */
		 /* iput-short v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S */
		 /* .line 173 */
		 /* move/from16 v2, p0 */
		 /* iput v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I */
		 /* .line 175 */
		 /* new-instance v3, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage; */
		 /* invoke-direct {v3, v1}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
		 /* .line 177 */
		 /* .local v3, "msg":Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage; */
		 /* instance-of v4, v0, Ljava/net/Inet6Address; */
		 if ( v4 != null) { // if-eqz v4, :cond_0
		 } // :cond_0
	 } // :goto_0
	 /* int-to-byte v4, v4 */
	 /* .line 180 */
	 /* .local v4, "family":B */
	 /* new-instance v11, Lcom/android/net/module/util/netlink/StructIfaddrMsg; */
	 /* int-to-short v6, v4 */
	 int v8 = 0; // const/4 v8, 0x0
	 /* move/from16 v12, p4 */
	 /* int-to-short v9, v12 */
	 /* move-object v5, v11 */
	 /* move/from16 v7, p2 */
	 /* move/from16 v10, p5 */
	 /* invoke-direct/range {v5 ..v10}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;-><init>(SSSSI)V */
	 this.mIfaddrmsg = v11;
	 /* .line 182 */
	 this.mIpAddress = v0;
	 /* .line 183 */
	 /* new-instance v5, Lcom/android/net/module/util/netlink/StructIfacacheInfo; */
	 /* const-wide/16 v18, 0x0 */
	 /* const-wide/16 v20, 0x0 */
	 /* move-object v13, v5 */
	 /* move-wide/from16 v14, p6 */
	 /* move-wide/from16 v16, p8 */
	 /* invoke-direct/range {v13 ..v21}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;-><init>(JJJJ)V */
	 this.mIfacacheInfo = v5;
	 /* .line 185 */
	 /* move/from16 v5, p3 */
	 /* iput v5, v3, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I */
	 /* .line 187 */
	 v6 = 	 /* invoke-direct {v3}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->getRequiredSpace()I */
	 /* new-array v6, v6, [B */
	 /* .line 188 */
	 /* .local v6, "bytes":[B */
	 /* array-length v7, v6 */
	 /* iput v7, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
	 /* .line 189 */
	 java.nio.ByteBuffer .wrap ( v6 );
	 /* .line 190 */
	 /* .local v7, "byteBuffer":Ljava/nio/ByteBuffer; */
	 java.nio.ByteOrder .nativeOrder ( );
	 (( java.nio.ByteBuffer ) v7 ).order ( v8 ); // invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
	 /* .line 191 */
	 (( com.android.net.module.util.netlink.RtNetlinkAddressMessage ) v3 ).pack ( v7 ); // invoke-virtual {v3, v7}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 192 */
} // .end method
public static com.android.net.module.util.netlink.RtNetlinkAddressMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
	 /* .locals 5 */
	 /* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
	 /* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 107 */
	 /* new-instance v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage; */
	 /* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
	 /* .line 109 */
	 /* .local v0, "addrMsg":Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage; */
	 com.android.net.module.util.netlink.StructIfaddrMsg .parse ( p1 );
	 this.mIfaddrmsg = v1;
	 /* .line 110 */
	 int v2 = 0; // const/4 v2, 0x0
	 /* if-nez v1, :cond_0 */
	 /* .line 113 */
} // :cond_0
v1 = (( java.nio.ByteBuffer ) p1 ).position ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I
/* .line 114 */
/* .local v1, "baseOffset":I */
int v3 = 1; // const/4 v3, 0x1
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,p1 );
/* .line 115 */
/* .local v3, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
/* if-nez v3, :cond_1 */
/* .line 116 */
} // :cond_1
(( com.android.net.module.util.netlink.StructNlAttr ) v3 ).getValueAsInetAddress ( ); // invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
this.mIpAddress = v4;
/* .line 117 */
/* if-nez v4, :cond_2 */
/* .line 120 */
} // :cond_2
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 121 */
int v4 = 6; // const/4 v4, 0x6
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v4,p1 );
/* .line 122 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 123 */
(( com.android.net.module.util.netlink.StructNlAttr ) v3 ).getValueAsByteBuffer ( ); // invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.android.net.module.util.netlink.StructIfacacheInfo .parse ( v4 );
this.mIfacacheInfo = v4;
/* .line 127 */
} // :cond_3
v4 = this.mIfaddrmsg;
/* iget-short v4, v4, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->flags:S */
/* iput v4, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I */
/* .line 130 */
(( java.nio.ByteBuffer ) p1 ).position ( v1 ); // invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 131 */
/* const/16 v4, 0x8 */
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v4,p1 );
/* .line 132 */
/* if-nez v3, :cond_4 */
/* .line 133 */
} // :cond_4
(( com.android.net.module.util.netlink.StructNlAttr ) v3 ).getValueAsInteger ( ); // invoke-virtual {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInteger()Ljava/lang/Integer;
/* .line 134 */
/* .local v4, "value":Ljava/lang/Integer; */
/* if-nez v4, :cond_5 */
/* .line 135 */
} // :cond_5
v2 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* iput v2, v0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I */
/* .line 137 */
} // .end method
/* # virtual methods */
public Integer getFlags ( ) {
/* .locals 1 */
/* .line 79 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I */
} // .end method
public com.android.net.module.util.netlink.StructIfacacheInfo getIfacacheInfo ( ) {
/* .locals 1 */
/* .line 94 */
v0 = this.mIfacacheInfo;
} // .end method
public com.android.net.module.util.netlink.StructIfaddrMsg getIfaddrHeader ( ) {
/* .locals 1 */
/* .line 84 */
v0 = this.mIfaddrmsg;
} // .end method
public java.net.InetAddress getIpAddress ( ) {
/* .locals 1 */
/* .line 89 */
v0 = this.mIpAddress;
} // .end method
protected void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 4 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 145 */
(( com.android.net.module.util.netlink.RtNetlinkAddressMessage ) p0 ).getHeader ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 146 */
v0 = this.mIfaddrmsg;
(( com.android.net.module.util.netlink.StructIfaddrMsg ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->pack(Ljava/nio/ByteBuffer;)V
/* .line 148 */
/* new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr; */
int v1 = 1; // const/4 v1, 0x1
v2 = this.mIpAddress;
/* invoke-direct {v0, v1, v2}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V */
/* .line 149 */
/* .local v0, "address":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 151 */
v1 = this.mIfacacheInfo;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 152 */
/* new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* .line 153 */
(( com.android.net.module.util.netlink.StructIfacacheInfo ) v1 ).writeToBytes ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->writeToBytes()[B
int v3 = 6; // const/4 v3, 0x6
/* invoke-direct {v2, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[B)V */
/* move-object v1, v2 */
/* .line 154 */
/* .local v1, "cacheInfo":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v1 ).pack ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 159 */
} // .end local v1 # "cacheInfo":Lcom/android/net/module/util/netlink/StructNlAttr;
} // :cond_0
/* new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* const/16 v2, 0x8 */
/* iget v3, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I */
/* invoke-direct {v1, v2, v3}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SI)V */
/* .line 160 */
/* .local v1, "flags":Lcom/android/net/module/util/netlink/StructNlAttr; */
(( com.android.net.module.util.netlink.StructNlAttr ) v1 ).pack ( p1 ); // invoke-virtual {v1, p1}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 161 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 210 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "RtNetlinkAddressMessage{ nlmsghdr{"; // const-string v1, "RtNetlinkAddressMessage{ nlmsghdr{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mHeader;
/* .line 211 */
java.lang.Integer .valueOf ( v2 );
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, Ifaddrmsg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIfaddrmsg;
/* .line 212 */
(( com.android.net.module.util.netlink.StructIfaddrMsg ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, IP Address{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mIpAddress;
/* .line 213 */
(( java.net.InetAddress ) v1 ).getHostAddress ( ); // invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, IfacacheInfo{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 214 */
v1 = this.mIfacacheInfo;
/* if-nez v1, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
(( com.android.net.module.util.netlink.StructIfacacheInfo ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructIfacacheInfo;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, Address Flags{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->mFlags:I */
/* .line 215 */
com.android.net.module.util.HexDump .toHexString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 210 */
} // .end method
