public class com.android.net.module.util.netlink.ConntrackMessage extends com.android.net.module.util.netlink.NetlinkMessage {
	 /* .source "ConntrackMessage.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;, */
	 /* Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;, */
	 /* Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Object CTA_IP_V4_DST;
public static final Object CTA_IP_V4_SRC;
public static final Object CTA_PROTO_DST_PORT;
public static final Object CTA_PROTO_NUM;
public static final Object CTA_PROTO_SRC_PORT;
public static final Object CTA_STATUS;
public static final Object CTA_TIMEOUT;
public static final Object CTA_TUPLE_IP;
public static final Object CTA_TUPLE_ORIG;
public static final Object CTA_TUPLE_PROTO;
public static final Object CTA_TUPLE_REPLY;
public static final Integer DYING_MASK;
public static final Integer ESTABLISHED_MASK;
public static final Integer IPS_ASSURED;
public static final Integer IPS_CONFIRMED;
public static final Integer IPS_DST_NAT;
public static final Integer IPS_DST_NAT_DONE;
public static final Integer IPS_DYING;
public static final Integer IPS_EXPECTED;
public static final Integer IPS_FIXED_TIMEOUT;
public static final Integer IPS_HELPER;
public static final Integer IPS_HW_OFFLOAD;
public static final Integer IPS_OFFLOAD;
public static final Integer IPS_SEEN_REPLY;
public static final Integer IPS_SEQ_ADJUST;
public static final Integer IPS_SRC_NAT;
public static final Integer IPS_SRC_NAT_DONE;
public static final Integer IPS_TEMPLATE;
public static final Integer IPS_UNTRACKED;
public static final Integer STRUCT_SIZE;
/* # instance fields */
public final com.android.net.module.util.netlink.StructNfGenMsg nfGenMsg;
public final Integer status;
public final Integer timeoutSec;
public final com.android.net.module.util.netlink.ConntrackMessage$Tuple tupleOrig;
public final com.android.net.module.util.netlink.ConntrackMessage$Tuple tupleReply;
/* # direct methods */
private com.android.net.module.util.netlink.ConntrackMessage ( ) {
	 /* .locals 2 */
	 /* .line 442 */
	 /* new-instance v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
	 /* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V */
	 /* invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
	 /* .line 443 */
	 /* new-instance v0, Lcom/android/net/module/util/netlink/StructNfGenMsg; */
	 /* int-to-byte v1, v1 */
	 /* invoke-direct {v0, v1}, Lcom/android/net/module/util/netlink/StructNfGenMsg;-><init>(B)V */
	 this.nfGenMsg = v0;
	 /* .line 447 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.tupleOrig = v0;
	 /* .line 448 */
	 this.tupleReply = v0;
	 /* .line 449 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I */
	 /* .line 450 */
	 /* iput v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->timeoutSec:I */
	 /* .line 451 */
	 return;
} // .end method
private com.android.net.module.util.netlink.ConntrackMessage ( ) {
	 /* .locals 0 */
	 /* .param p1, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
	 /* .param p2, "nfGenMsg" # Lcom/android/net/module/util/netlink/StructNfGenMsg; */
	 /* .param p3, "tupleOrig" # Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
	 /* .param p4, "tupleReply" # Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
	 /* .param p5, "status" # I */
	 /* .param p6, "timeoutSec" # I */
	 /* .line 455 */
	 /* invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V */
	 /* .line 456 */
	 this.nfGenMsg = p2;
	 /* .line 457 */
	 this.tupleOrig = p3;
	 /* .line 458 */
	 this.tupleReply = p4;
	 /* .line 459 */
	 /* iput p5, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I */
	 /* .line 460 */
	 /* iput p6, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->timeoutSec:I */
	 /* .line 461 */
	 return;
} // .end method
private static java.net.Inet4Address castToInet4Address ( java.net.InetAddress p0 ) {
	 /* .locals 1 */
	 /* .param p0, "address" # Ljava/net/InetAddress; */
	 /* .line 342 */
	 if ( p0 != null) { // if-eqz p0, :cond_1
		 /* instance-of v0, p0, Ljava/net/Inet4Address; */
		 /* if-nez v0, :cond_0 */
		 /* .line 343 */
	 } // :cond_0
	 /* move-object v0, p0 */
	 /* check-cast v0, Ljava/net/Inet4Address; */
	 /* .line 342 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static newIPv4TimeoutUpdateRequest ( Integer p0, java.net.Inet4Address p1, Integer p2, java.net.Inet4Address p3, Integer p4, Integer p5 ) {
/* .locals 16 */
/* .param p0, "proto" # I */
/* .param p1, "src" # Ljava/net/Inet4Address; */
/* .param p2, "sport" # I */
/* .param p3, "dst" # Ljava/net/Inet4Address; */
/* .param p4, "dport" # I */
/* .param p5, "timeoutSec" # I */
/* .line 193 */
/* new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr; */
int v3 = 1; // const/4 v3, 0x1
/* move-object/from16 v4, p1 */
/* invoke-direct {v2, v3, v4}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V */
/* new-instance v5, Lcom/android/net/module/util/netlink/StructNlAttr; */
int v6 = 2; // const/4 v6, 0x2
/* move-object/from16 v7, p3 */
/* invoke-direct {v5, v6, v7}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V */
/* filled-new-array {v2, v5}, [Lcom/android/net/module/util/netlink/StructNlAttr; */
/* invoke-direct {v1, v3, v2}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[Lcom/android/net/module/util/netlink/StructNlAttr;)V */
/* new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* new-instance v5, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* move/from16 v8, p0 */
/* int-to-byte v9, v8 */
/* invoke-direct {v5, v3, v9}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SB)V */
/* new-instance v9, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* move/from16 v10, p2 */
/* int-to-short v11, v10 */
v12 = java.nio.ByteOrder.BIG_ENDIAN;
/* invoke-direct {v9, v6, v11, v12}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V */
/* new-instance v11, Lcom/android/net/module/util/netlink/StructNlAttr; */
/* move/from16 v12, p4 */
/* int-to-short v13, v12 */
v14 = java.nio.ByteOrder.BIG_ENDIAN;
int v15 = 3; // const/4 v15, 0x3
/* invoke-direct {v11, v15, v13, v14}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V */
/* filled-new-array {v5, v9, v11}, [Lcom/android/net/module/util/netlink/StructNlAttr; */
/* invoke-direct {v2, v6, v5}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[Lcom/android/net/module/util/netlink/StructNlAttr;)V */
/* filled-new-array {v1, v2}, [Lcom/android/net/module/util/netlink/StructNlAttr; */
/* invoke-direct {v0, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[Lcom/android/net/module/util/netlink/StructNlAttr;)V */
/* .line 202 */
/* .local v0, "ctaTupleOrig":Lcom/android/net/module/util/netlink/StructNlAttr; */
/* new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr; */
int v2 = 7; // const/4 v2, 0x7
v5 = java.nio.ByteOrder.BIG_ENDIAN;
/* move/from16 v6, p5 */
/* invoke-direct {v1, v2, v6, v5}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SILjava/nio/ByteOrder;)V */
/* .line 204 */
/* .local v1, "ctaTimeout":Lcom/android/net/module/util/netlink/StructNlAttr; */
v2 = (( com.android.net.module.util.netlink.StructNlAttr ) v0 ).getAlignedLength ( ); // invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
v5 = (( com.android.net.module.util.netlink.StructNlAttr ) v1 ).getAlignedLength ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I
/* add-int/2addr v2, v5 */
/* .line 205 */
/* .local v2, "payloadLength":I */
/* add-int/lit8 v5, v2, 0x14 */
/* new-array v5, v5, [B */
/* .line 206 */
/* .local v5, "bytes":[B */
java.nio.ByteBuffer .wrap ( v5 );
/* .line 207 */
/* .local v9, "byteBuffer":Ljava/nio/ByteBuffer; */
java.nio.ByteOrder .nativeOrder ( );
(( java.nio.ByteBuffer ) v9 ).order ( v11 ); // invoke-virtual {v9, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;
/* .line 209 */
/* new-instance v11, Lcom/android/net/module/util/netlink/ConntrackMessage; */
/* invoke-direct {v11}, Lcom/android/net/module/util/netlink/ConntrackMessage;-><init>()V */
/* .line 210 */
/* .local v11, "ctmsg":Lcom/android/net/module/util/netlink/ConntrackMessage; */
v13 = this.mHeader;
/* array-length v14, v5 */
/* iput v14, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
/* .line 211 */
v13 = this.mHeader;
/* const/16 v14, 0x100 */
/* iput-short v14, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* .line 213 */
v13 = this.mHeader;
/* const/16 v14, 0x105 */
/* iput-short v14, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S */
/* .line 214 */
v13 = this.mHeader;
/* iput v3, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I */
/* .line 215 */
(( com.android.net.module.util.netlink.ConntrackMessage ) v11 ).pack ( v9 ); // invoke-virtual {v11, v9}, Lcom/android/net/module/util/netlink/ConntrackMessage;->pack(Ljava/nio/ByteBuffer;)V
/* .line 217 */
(( com.android.net.module.util.netlink.StructNlAttr ) v0 ).pack ( v9 ); // invoke-virtual {v0, v9}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 218 */
(( com.android.net.module.util.netlink.StructNlAttr ) v1 ).pack ( v9 ); // invoke-virtual {v1, v9}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 220 */
} // .end method
public static com.android.net.module.util.netlink.ConntrackMessage parse ( com.android.net.module.util.netlink.StructNlMsgHdr p0, java.nio.ByteBuffer p1 ) {
/* .locals 18 */
/* .param p0, "header" # Lcom/android/net/module/util/netlink/StructNlMsgHdr; */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 237 */
/* move-object/from16 v0, p1 */
/* invoke-static/range {p1 ..p1}, Lcom/android/net/module/util/netlink/StructNfGenMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNfGenMsg; */
/* .line 238 */
/* .local v8, "nfGenMsg":Lcom/android/net/module/util/netlink/StructNfGenMsg; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v8, :cond_0 */
/* .line 239 */
/* .line 242 */
} // :cond_0
v9 = /* invoke-virtual/range {p1 ..p1}, Ljava/nio/ByteBuffer;->position()I */
/* .line 243 */
/* .local v9, "baseOffset":I */
int v2 = 3; // const/4 v2, 0x3
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v2,v0 );
/* .line 244 */
/* .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
int v3 = 0; // const/4 v3, 0x0
/* .line 245 */
/* .local v3, "status":I */
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 246 */
v3 = (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsBe32 ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe32(I)I
/* move v10, v3 */
/* .line 245 */
} // :cond_1
/* move v10, v3 */
/* .line 249 */
} // .end local v3 # "status":I
/* .local v10, "status":I */
} // :goto_0
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 250 */
int v3 = 7; // const/4 v3, 0x7
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,v0 );
/* .line 251 */
int v3 = 0; // const/4 v3, 0x0
/* .line 252 */
/* .local v3, "timeoutSec":I */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 253 */
v3 = (( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsBe32 ( v4 ); // invoke-virtual {v2, v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe32(I)I
/* move v11, v3 */
/* .line 252 */
} // :cond_2
/* move v11, v3 */
/* .line 256 */
} // .end local v3 # "timeoutSec":I
/* .local v11, "timeoutSec":I */
} // :goto_1
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 257 */
int v3 = 1; // const/4 v3, 0x1
v3 = com.android.net.module.util.netlink.StructNlAttr .makeNestedType ( v3 );
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,v0 );
/* .line 258 */
int v3 = 0; // const/4 v3, 0x0
/* .line 259 */
/* .local v3, "tupleOrig":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 260 */
(( com.android.net.module.util.netlink.StructNlAttr ) v2 ).getValueAsByteBuffer ( ); // invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.android.net.module.util.netlink.ConntrackMessage .parseTuple ( v4 );
/* move-object v12, v3 */
/* .line 259 */
} // :cond_3
/* move-object v12, v3 */
/* .line 263 */
} // .end local v3 # "tupleOrig":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
/* .local v12, "tupleOrig":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
} // :goto_2
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 264 */
int v3 = 2; // const/4 v3, 0x2
v3 = com.android.net.module.util.netlink.StructNlAttr .makeNestedType ( v3 );
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v3,v0 );
/* .line 265 */
} // .end local v2 # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
/* .local v13, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
int v2 = 0; // const/4 v2, 0x0
/* .line 266 */
/* .local v2, "tupleReply":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
if ( v13 != null) { // if-eqz v13, :cond_4
/* .line 267 */
(( com.android.net.module.util.netlink.StructNlAttr ) v13 ).getValueAsByteBuffer ( ); // invoke-virtual {v13}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.android.net.module.util.netlink.ConntrackMessage .parseTuple ( v3 );
/* move-object v14, v2 */
/* .line 266 */
} // :cond_4
/* move-object v14, v2 */
/* .line 271 */
} // .end local v2 # "tupleReply":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
/* .local v14, "tupleReply":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
} // :goto_3
(( java.nio.ByteBuffer ) v0 ).position ( v9 ); // invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 272 */
/* const/16 v15, 0x14 */
/* .line 273 */
/* .local v15, "kMinConsumed":I */
/* move-object/from16 v7, p0 */
/* iget v2, v7, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I */
/* add-int/lit8 v2, v2, -0x14 */
v6 = com.android.net.module.util.netlink.NetlinkConstants .alignedLengthOf ( v2 );
/* .line 275 */
/* .local v6, "kAdditionalSpace":I */
v2 = /* invoke-virtual/range {p1 ..p1}, Ljava/nio/ByteBuffer;->remaining()I */
/* if-ge v2, v6, :cond_5 */
/* .line 276 */
/* .line 278 */
} // :cond_5
/* add-int v1, v9, v6 */
(( java.nio.ByteBuffer ) v0 ).position ( v1 ); // invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 280 */
/* new-instance v16, Lcom/android/net/module/util/netlink/ConntrackMessage; */
/* move-object/from16 v1, v16 */
/* move-object/from16 v2, p0 */
/* move-object v3, v8 */
/* move-object v4, v12 */
/* move-object v5, v14 */
/* move/from16 v17, v6 */
} // .end local v6 # "kAdditionalSpace":I
/* .local v17, "kAdditionalSpace":I */
/* move v6, v10 */
/* move v7, v11 */
/* invoke-direct/range {v1 ..v7}, Lcom/android/net/module/util/netlink/ConntrackMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Lcom/android/net/module/util/netlink/StructNfGenMsg;Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;II)V */
} // .end method
private static com.android.net.module.util.netlink.ConntrackMessage$Tuple parseTuple ( java.nio.ByteBuffer p0 ) {
/* .locals 6 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 318 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 320 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 321 */
/* .local v1, "tupleIpv4":Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4; */
int v2 = 0; // const/4 v2, 0x0
/* .line 323 */
/* .local v2, "tupleProto":Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto; */
v3 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 324 */
/* .local v3, "baseOffset":I */
int v4 = 1; // const/4 v4, 0x1
v4 = com.android.net.module.util.netlink.StructNlAttr .makeNestedType ( v4 );
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v4,p0 );
/* .line 325 */
/* .local v4, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 326 */
(( com.android.net.module.util.netlink.StructNlAttr ) v4 ).getValueAsByteBuffer ( ); // invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.android.net.module.util.netlink.ConntrackMessage .parseTupleIpv4 ( v5 );
/* .line 328 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 330 */
} // :cond_2
(( java.nio.ByteBuffer ) p0 ).position ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 331 */
int v5 = 2; // const/4 v5, 0x2
v5 = com.android.net.module.util.netlink.StructNlAttr .makeNestedType ( v5 );
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v5,p0 );
/* .line 332 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 333 */
(( com.android.net.module.util.netlink.StructNlAttr ) v4 ).getValueAsByteBuffer ( ); // invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;
com.android.net.module.util.netlink.ConntrackMessage .parseTupleProto ( v5 );
/* .line 335 */
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 337 */
} // :cond_4
/* new-instance v0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple; */
/* invoke-direct {v0, v1, v2}, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;-><init>(Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;)V */
} // .end method
private static com.android.net.module.util.netlink.ConntrackMessage$TupleIpv4 parseTupleIpv4 ( java.nio.ByteBuffer p0 ) {
/* .locals 6 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 348 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 350 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 351 */
/* .local v1, "src":Ljava/net/Inet4Address; */
int v2 = 0; // const/4 v2, 0x0
/* .line 353 */
/* .local v2, "dst":Ljava/net/Inet4Address; */
v3 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 354 */
/* .local v3, "baseOffset":I */
int v4 = 1; // const/4 v4, 0x1
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v4,p0 );
/* .line 355 */
/* .local v4, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 356 */
(( com.android.net.module.util.netlink.StructNlAttr ) v4 ).getValueAsInetAddress ( ); // invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
com.android.net.module.util.netlink.ConntrackMessage .castToInet4Address ( v5 );
/* .line 358 */
} // :cond_1
/* if-nez v1, :cond_2 */
/* .line 360 */
} // :cond_2
(( java.nio.ByteBuffer ) p0 ).position ( v3 ); // invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 361 */
int v5 = 2; // const/4 v5, 0x2
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v5,p0 );
/* .line 362 */
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 363 */
(( com.android.net.module.util.netlink.StructNlAttr ) v4 ).getValueAsInetAddress ( ); // invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;
com.android.net.module.util.netlink.ConntrackMessage .castToInet4Address ( v5 );
/* .line 365 */
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 367 */
} // :cond_4
/* new-instance v0, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4; */
/* invoke-direct {v0, v1, v2}, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;-><init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;)V */
} // .end method
private static com.android.net.module.util.netlink.ConntrackMessage$TupleProto parseTupleProto ( java.nio.ByteBuffer p0 ) {
/* .locals 8 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 372 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 374 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 375 */
/* .local v1, "protoNum":B */
int v2 = 0; // const/4 v2, 0x0
/* .line 376 */
/* .local v2, "srcPort":S */
int v3 = 0; // const/4 v3, 0x0
/* .line 378 */
/* .local v3, "dstPort":S */
v4 = (( java.nio.ByteBuffer ) p0 ).position ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I
/* .line 379 */
/* .local v4, "baseOffset":I */
int v5 = 1; // const/4 v5, 0x1
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v5,p0 );
/* .line 380 */
/* .local v5, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr; */
int v6 = 0; // const/4 v6, 0x0
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 381 */
v1 = (( com.android.net.module.util.netlink.StructNlAttr ) v5 ).getValueAsByte ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByte(B)B
/* .line 383 */
} // :cond_1
/* if-eq v1, v7, :cond_2 */
/* if-eq v1, v7, :cond_2 */
/* .line 385 */
} // :cond_2
(( java.nio.ByteBuffer ) p0 ).position ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 386 */
int v7 = 2; // const/4 v7, 0x2
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v7,p0 );
/* .line 387 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 388 */
v2 = (( com.android.net.module.util.netlink.StructNlAttr ) v5 ).getValueAsBe16 ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe16(S)S
/* .line 390 */
} // :cond_3
/* if-nez v2, :cond_4 */
/* .line 392 */
} // :cond_4
(( java.nio.ByteBuffer ) p0 ).position ( v4 ); // invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;
/* .line 393 */
int v7 = 3; // const/4 v7, 0x3
com.android.net.module.util.netlink.StructNlAttr .findNextAttrOfType ( v7,p0 );
/* .line 394 */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 395 */
v3 = (( com.android.net.module.util.netlink.StructNlAttr ) v5 ).getValueAsBe16 ( v6 ); // invoke-virtual {v5, v6}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe16(S)S
/* .line 397 */
} // :cond_5
/* if-nez v3, :cond_6 */
/* .line 399 */
} // :cond_6
/* new-instance v0, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto; */
/* invoke-direct {v0, v1, v2, v3}, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;-><init>(BSS)V */
} // .end method
public static java.lang.String stringForIpConntrackStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "flags" # I */
/* .line 479 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 481 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* and-int/lit8 v1, p0, 0x1 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 482 */
final String v1 = "IPS_EXPECTED"; // const-string v1, "IPS_EXPECTED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 484 */
} // :cond_0
/* and-int/lit8 v1, p0, 0x2 */
/* const-string/jumbo v2, "|" */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 485 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 486 */
} // :cond_1
final String v1 = "IPS_SEEN_REPLY"; // const-string v1, "IPS_SEEN_REPLY"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 488 */
} // :cond_2
/* and-int/lit8 v1, p0, 0x4 */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 489 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_3 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 490 */
} // :cond_3
final String v1 = "IPS_ASSURED"; // const-string v1, "IPS_ASSURED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 492 */
} // :cond_4
/* and-int/lit8 v1, p0, 0x8 */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 493 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_5 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 494 */
} // :cond_5
final String v1 = "IPS_CONFIRMED"; // const-string v1, "IPS_CONFIRMED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 496 */
} // :cond_6
/* and-int/lit8 v1, p0, 0x10 */
if ( v1 != null) { // if-eqz v1, :cond_8
/* .line 497 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_7 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 498 */
} // :cond_7
final String v1 = "IPS_SRC_NAT"; // const-string v1, "IPS_SRC_NAT"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 500 */
} // :cond_8
/* and-int/lit8 v1, p0, 0x20 */
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 501 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_9 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 502 */
} // :cond_9
final String v1 = "IPS_DST_NAT"; // const-string v1, "IPS_DST_NAT"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 504 */
} // :cond_a
/* and-int/lit8 v1, p0, 0x40 */
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 505 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_b */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 506 */
} // :cond_b
final String v1 = "IPS_SEQ_ADJUST"; // const-string v1, "IPS_SEQ_ADJUST"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 508 */
} // :cond_c
/* and-int/lit16 v1, p0, 0x80 */
if ( v1 != null) { // if-eqz v1, :cond_e
/* .line 509 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_d */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 510 */
} // :cond_d
final String v1 = "IPS_SRC_NAT_DONE"; // const-string v1, "IPS_SRC_NAT_DONE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 512 */
} // :cond_e
/* and-int/lit16 v1, p0, 0x100 */
if ( v1 != null) { // if-eqz v1, :cond_10
/* .line 513 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_f */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 514 */
} // :cond_f
final String v1 = "IPS_DST_NAT_DONE"; // const-string v1, "IPS_DST_NAT_DONE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 516 */
} // :cond_10
/* and-int/lit16 v1, p0, 0x200 */
if ( v1 != null) { // if-eqz v1, :cond_12
/* .line 517 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_11 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 518 */
} // :cond_11
final String v1 = "IPS_DYING"; // const-string v1, "IPS_DYING"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 520 */
} // :cond_12
/* and-int/lit16 v1, p0, 0x400 */
if ( v1 != null) { // if-eqz v1, :cond_14
/* .line 521 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_13 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 522 */
} // :cond_13
final String v1 = "IPS_FIXED_TIMEOUT"; // const-string v1, "IPS_FIXED_TIMEOUT"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 524 */
} // :cond_14
/* and-int/lit16 v1, p0, 0x800 */
if ( v1 != null) { // if-eqz v1, :cond_16
/* .line 525 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_15 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 526 */
} // :cond_15
final String v1 = "IPS_TEMPLATE"; // const-string v1, "IPS_TEMPLATE"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 528 */
} // :cond_16
/* and-int/lit16 v1, p0, 0x1000 */
if ( v1 != null) { // if-eqz v1, :cond_18
/* .line 529 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_17 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 530 */
} // :cond_17
final String v1 = "IPS_UNTRACKED"; // const-string v1, "IPS_UNTRACKED"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 532 */
} // :cond_18
/* and-int/lit16 v1, p0, 0x2000 */
if ( v1 != null) { // if-eqz v1, :cond_1a
/* .line 533 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_19 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 534 */
} // :cond_19
final String v1 = "IPS_HELPER"; // const-string v1, "IPS_HELPER"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 536 */
} // :cond_1a
/* and-int/lit16 v1, p0, 0x4000 */
if ( v1 != null) { // if-eqz v1, :cond_1c
/* .line 537 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1b */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 538 */
} // :cond_1b
final String v1 = "IPS_OFFLOAD"; // const-string v1, "IPS_OFFLOAD"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 540 */
} // :cond_1c
/* const v1, 0x8000 */
/* and-int/2addr v1, p0 */
if ( v1 != null) { // if-eqz v1, :cond_1e
/* .line 541 */
v1 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-lez v1, :cond_1d */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 542 */
} // :cond_1d
final String v1 = "IPS_HW_OFFLOAD"; // const-string v1, "IPS_HW_OFFLOAD"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 544 */
} // :cond_1e
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
/* # virtual methods */
public Object getMessageType ( ) {
/* .locals 1 */
/* .line 472 */
(( com.android.net.module.util.netlink.ConntrackMessage ) p0 ).getHeader ( ); // invoke-virtual {p0}, Lcom/android/net/module/util/netlink/ConntrackMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
/* iget-short v0, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S */
/* and-int/lit16 v0, v0, -0x101 */
/* int-to-short v0, v0 */
} // .end method
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 467 */
v0 = this.mHeader;
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
/* .line 468 */
v0 = this.nfGenMsg;
(( com.android.net.module.util.netlink.StructNfGenMsg ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNfGenMsg;->pack(Ljava/nio/ByteBuffer;)V
/* .line 469 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 549 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ConntrackMessage{nlmsghdr{"; // const-string v1, "ConntrackMessage{nlmsghdr{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 551 */
v1 = this.mHeader;
/* if-nez v1, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
v1 = this.mHeader;
java.lang.Integer .valueOf ( v2 );
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, nfgenmsg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.nfGenMsg;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, tuple_orig{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.tupleOrig;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, tuple_reply{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.tupleReply;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, status{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "("; // const-string v1, "("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I */
/* .line 556 */
com.android.net.module.util.netlink.ConntrackMessage .stringForIpConntrackStatus ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")}, timeout_sec{"; // const-string v1, ")}, timeout_sec{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->timeoutSec:I */
/* .line 557 */
java.lang.Integer .toUnsignedLong ( v1 );
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 549 */
} // .end method
