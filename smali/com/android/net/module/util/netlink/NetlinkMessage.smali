.class public Lcom/android/net/module/util/netlink/NetlinkMessage;
.super Ljava/lang/Object;
.source "NetlinkMessage.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NetlinkMessage"


# instance fields
.field protected final mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;


# direct methods
.method public constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V
    .locals 0
    .param p1, "nlmsghdr"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/android/net/module/util/netlink/NetlinkMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    .line 94
    return-void
.end method

.method public static parse(Ljava/nio/ByteBuffer;I)Lcom/android/net/module/util/netlink/NetlinkMessage;
    .locals 6
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;
    .param p1, "nlFamily"    # I

    .line 48
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    .line 49
    .local v0, "startPosition":I
    :goto_0
    invoke-static {p0}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v1

    .line 50
    .local v1, "nlmsghdr":Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    const/4 v2, 0x0

    if-nez v1, :cond_1

    .line 51
    return-object v2

    .line 54
    :cond_1
    iget v3, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    invoke-static {v3}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(I)I

    move-result v3

    .line 55
    .local v3, "messageLength":I
    add-int/lit8 v4, v3, -0x10

    .line 56
    .local v4, "payloadLength":I
    if-ltz v4, :cond_7

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    if-le v4, v5, :cond_2

    goto :goto_2

    .line 64
    :cond_2
    iget-short v2, v1, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    const/16 v5, 0xf

    if-gt v2, v5, :cond_3

    .line 65
    invoke-static {v1, p0, v4}, Lcom/android/net/module/util/netlink/NetlinkMessage;->parseCtlMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;I)Lcom/android/net/module/util/netlink/NetlinkMessage;

    move-result-object v2

    return-object v2

    .line 72
    :cond_3
    sget v2, Landroid/system/OsConstants;->NETLINK_ROUTE:I

    if-ne p1, v2, :cond_4

    .line 73
    invoke-static {v1, p0}, Lcom/android/net/module/util/netlink/NetlinkMessage;->parseRtMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkMessage;

    move-result-object v2

    .local v2, "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    goto :goto_1

    .line 74
    .end local v2    # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    :cond_4
    sget v2, Landroid/system/OsConstants;->NETLINK_INET_DIAG:I

    if-ne p1, v2, :cond_5

    .line 75
    invoke-static {v1, p0}, Lcom/android/net/module/util/netlink/NetlinkMessage;->parseInetDiagMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkMessage;

    move-result-object v2

    .restart local v2    # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    goto :goto_1

    .line 76
    .end local v2    # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    :cond_5
    sget v2, Landroid/system/OsConstants;->NETLINK_NETFILTER:I

    if-ne p1, v2, :cond_6

    .line 77
    invoke-static {v1, p0}, Lcom/android/net/module/util/netlink/NetlinkMessage;->parseNfMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkMessage;

    move-result-object v2

    .restart local v2    # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    goto :goto_1

    .line 79
    .end local v2    # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    :cond_6
    const/4 v2, 0x0

    .line 84
    .restart local v2    # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    :goto_1
    add-int v5, v0, v3

    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 86
    return-object v2

    .line 58
    .end local v2    # "parsed":Lcom/android/net/module/util/netlink/NetlinkMessage;
    :cond_7
    :goto_2
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    invoke-virtual {p0, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 59
    return-object v2
.end method

.method private static parseCtlMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;I)Lcom/android/net/module/util/netlink/NetlinkMessage;
    .locals 1
    .param p0, "nlmsghdr"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;
    .param p2, "payloadLength"    # I

    .line 114
    iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    packed-switch v0, :pswitch_data_0

    .line 120
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 121
    new-instance v0, Lcom/android/net/module/util/netlink/NetlinkMessage;

    invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    return-object v0

    .line 116
    :pswitch_0
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkErrorMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkErrorMessage;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static parseInetDiagMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkMessage;
    .locals 1
    .param p0, "nlmsghdr"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 152
    iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    packed-switch v0, :pswitch_data_0

    .line 155
    const/4 v0, 0x0

    return-object v0

    .line 154
    :pswitch_0
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/InetDiagMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/InetDiagMessage;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
    .end packed-switch
.end method

.method private static parseNfMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkMessage;
    .locals 1
    .param p0, "nlmsghdr"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 162
    iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    packed-switch v0, :pswitch_data_0

    .line 168
    :pswitch_0
    const/4 v0, 0x0

    return-object v0

    .line 167
    :pswitch_1
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/ConntrackMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x100
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static parseRtMessage(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NetlinkMessage;
    .locals 1
    .param p0, "nlmsghdr"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 129
    iget-short v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    sparse-switch v0, :sswitch_data_0

    .line 145
    const/4 v0, 0x0

    return-object v0

    .line 144
    :sswitch_0
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/NduseroptMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NduseroptMessage;

    move-result-object v0

    return-object v0

    .line 142
    :sswitch_1
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkNeighborMessage;

    move-result-object v0

    return-object v0

    .line 138
    :sswitch_2
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkRouteMessage;

    move-result-object v0

    return-object v0

    .line 135
    :sswitch_3
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkAddressMessage;

    move-result-object v0

    return-object v0

    .line 132
    :sswitch_4
    invoke-static {p0, p1}, Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;->parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/RtNetlinkLinkMessage;

    move-result-object v0

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_4
        0x11 -> :sswitch_4
        0x14 -> :sswitch_3
        0x15 -> :sswitch_3
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1c -> :sswitch_1
        0x1d -> :sswitch_1
        0x1e -> :sswitch_1
        0x44 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/android/net/module/util/netlink/NetlinkMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NetlinkMessage{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/NetlinkMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
