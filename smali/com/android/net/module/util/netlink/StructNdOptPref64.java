public class com.android.net.module.util.netlink.StructNdOptPref64 extends com.android.net.module.util.netlink.NdOption {
	 /* .source "StructNdOptPref64.java" */
	 /* # static fields */
	 public static final Object LENGTH;
	 public static final Integer STRUCT_SIZE;
	 private static final java.lang.String TAG;
	 public static final Integer TYPE;
	 /* # instance fields */
	 public final Integer lifetime;
	 public final android.net.IpPrefix prefix;
	 /* # direct methods */
	 static com.android.net.module.util.netlink.StructNdOptPref64 ( ) {
		 /* .locals 1 */
		 /* .line 52 */
		 /* const-class v0, Lcom/android/net/module/util/netlink/StructNdOptPref64; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 public com.android.net.module.util.netlink.StructNdOptPref64 ( ) {
		 /* .locals 3 */
		 /* .param p1, "prefix" # Landroid/net/IpPrefix; */
		 /* .param p2, "lifetime" # I */
		 /* .line 96 */
		 /* const/16 v0, 0x26 */
		 int v1 = 2; // const/4 v1, 0x2
		 /* invoke-direct {p0, v0, v1}, Lcom/android/net/module/util/netlink/NdOption;-><init>(BI)V */
		 /* .line 98 */
		 final String v0 = "prefix must not be null"; // const-string v0, "prefix must not be null"
		 java.util.Objects .requireNonNull ( p1,v0 );
		 /* .line 99 */
		 (( android.net.IpPrefix ) p1 ).getAddress ( ); // invoke-virtual {p1}, Landroid/net/IpPrefix;->getAddress()Ljava/net/InetAddress;
		 /* instance-of v0, v0, Ljava/net/Inet6Address; */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 102 */
			 v0 = 			 (( android.net.IpPrefix ) p1 ).getPrefixLength ( ); // invoke-virtual {p1}, Landroid/net/IpPrefix;->getPrefixLength()I
			 com.android.net.module.util.netlink.StructNdOptPref64 .prefixLengthToPlc ( v0 );
			 /* .line 103 */
			 this.prefix = p1;
			 /* .line 105 */
			 /* if-ltz p2, :cond_0 */
			 /* const v0, 0xfff8 */
			 /* if-gt p2, v0, :cond_0 */
			 /* .line 108 */
			 /* and-int/2addr v0, p2 */
			 /* iput v0, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->lifetime:I */
			 /* .line 109 */
			 return;
			 /* .line 106 */
		 } // :cond_0
		 /* new-instance v0, Ljava/lang/IllegalArgumentException; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "Invalid lifetime "; // const-string v2, "Invalid lifetime "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
		 /* throw v0 */
		 /* .line 100 */
	 } // :cond_1
	 /* new-instance v0, Ljava/lang/IllegalArgumentException; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Must be an IPv6 prefix: "; // const-string v2, "Must be an IPv6 prefix: "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
	 /* throw v0 */
} // .end method
private com.android.net.module.util.netlink.StructNdOptPref64 ( ) {
	 /* .locals 5 */
	 /* .param p1, "buf" # Ljava/nio/ByteBuffer; */
	 /* .line 113 */
	 v0 = 	 (( java.nio.ByteBuffer ) p1 ).get ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B
	 v1 = 	 (( java.nio.ByteBuffer ) p1 ).get ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B
	 v1 = 	 java.lang.Byte .toUnsignedInt ( v1 );
	 /* invoke-direct {p0, v0, v1}, Lcom/android/net/module/util/netlink/NdOption;-><init>(BI)V */
	 /* .line 114 */
	 /* iget-byte v0, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->type:B */
	 /* const/16 v1, 0x26 */
	 /* if-ne v0, v1, :cond_1 */
	 /* .line 115 */
	 /* iget v0, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->length:I */
	 int v1 = 2; // const/4 v1, 0x2
	 /* if-ne v0, v1, :cond_0 */
	 /* .line 117 */
	 v0 = 	 (( java.nio.ByteBuffer ) p1 ).getShort ( ); // invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S
	 v0 = 	 java.lang.Short .toUnsignedInt ( v0 );
	 /* .line 118 */
	 /* .local v0, "scaledLifetimePlc":I */
	 /* const v1, 0xfff8 */
	 /* and-int/2addr v1, v0 */
	 /* iput v1, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->lifetime:I */
	 /* .line 120 */
	 /* const/16 v1, 0x10 */
	 /* new-array v1, v1, [B */
	 /* .line 121 */
	 /* .local v1, "addressBytes":[B */
	 int v2 = 0; // const/4 v2, 0x0
	 /* const/16 v3, 0xc */
	 (( java.nio.ByteBuffer ) p1 ).get ( v1, v2, v3 ); // invoke-virtual {p1, v1, v2, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
	 /* .line 124 */
	 try { // :try_start_0
		 java.net.InetAddress .getByAddress ( v1 );
		 /* :try_end_0 */
		 /* .catch Ljava/net/UnknownHostException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 127 */
		 /* .local v2, "addr":Ljava/net/InetAddress; */
		 /* nop */
		 /* .line 128 */
		 /* new-instance v3, Landroid/net/IpPrefix; */
		 /* and-int/lit8 v4, v0, 0x7 */
		 v4 = 		 com.android.net.module.util.netlink.StructNdOptPref64 .plcToPrefixLength ( v4 );
		 /* invoke-direct {v3, v2, v4}, Landroid/net/IpPrefix;-><init>(Ljava/net/InetAddress;I)V */
		 this.prefix = v3;
		 /* .line 129 */
		 return;
		 /* .line 125 */
	 } // .end local v2 # "addr":Ljava/net/InetAddress;
	 /* :catch_0 */
	 /* move-exception v2 */
	 /* .line 126 */
	 /* .local v2, "e":Ljava/net/UnknownHostException; */
	 /* new-instance v3, Ljava/lang/AssertionError; */
	 final String v4 = "16-byte array not valid InetAddress?"; // const-string v4, "16-byte array not valid InetAddress?"
	 /* invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V */
	 /* throw v3 */
	 /* .line 115 */
} // .end local v0 # "scaledLifetimePlc":I
} // .end local v1 # "addressBytes":[B
} // .end local v2 # "e":Ljava/net/UnknownHostException;
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid length "; // const-string v2, "Invalid length "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->length:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 114 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid type "; // const-string v2, "Invalid type "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v2, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->type:B */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
static Object getScaledLifetimePlc ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "lifetime" # I */
/* .param p1, "prefixLengthCode" # I */
/* .line 92 */
/* const v0, 0xfff8 */
/* and-int/2addr v0, p0 */
/* and-int/lit8 v1, p1, 0x7 */
/* or-int/2addr v0, v1 */
/* int-to-short v0, v0 */
} // .end method
public static com.android.net.module.util.netlink.StructNdOptPref64 parse ( java.nio.ByteBuffer p0 ) {
/* .locals 5 */
/* .param p0, "buf" # Ljava/nio/ByteBuffer; */
/* .line 140 */
v0 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v1, 0x10 */
int v2 = 0; // const/4 v2, 0x0
/* if-ge v0, v1, :cond_0 */
/* .line 142 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Lcom/android/net/module/util/netlink/StructNdOptPref64; */
/* invoke-direct {v0, p0}, Lcom/android/net/module/util/netlink/StructNdOptPref64;-><init>(Ljava/nio/ByteBuffer;)V */
/* :try_end_0 */
/* .catch Ljava/lang/IllegalArgumentException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 143 */
/* :catch_0 */
/* move-exception v0 */
/* .line 147 */
/* .local v0, "e":Ljava/lang/IllegalArgumentException; */
v1 = com.android.net.module.util.netlink.StructNdOptPref64.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Invalid PREF64 option: "; // const-string v4, "Invalid PREF64 option: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v1,v3 );
/* .line 148 */
} // .end method
static Integer plcToPrefixLength ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "plc" # I */
/* .line 63 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 71 */
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid prefix length code "; // const-string v2, "Invalid prefix length code "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 69 */
/* :pswitch_0 */
/* const/16 v0, 0x20 */
/* .line 68 */
/* :pswitch_1 */
/* const/16 v0, 0x28 */
/* .line 67 */
/* :pswitch_2 */
/* const/16 v0, 0x30 */
/* .line 66 */
/* :pswitch_3 */
/* const/16 v0, 0x38 */
/* .line 65 */
/* :pswitch_4 */
/* const/16 v0, 0x40 */
/* .line 64 */
/* :pswitch_5 */
/* const/16 v0, 0x60 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static Integer prefixLengthToPlc ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "prefixLength" # I */
/* .line 76 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 84 */
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid prefix length "; // const-string v2, "Invalid prefix length "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 77 */
/* :sswitch_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 78 */
/* :sswitch_1 */
int v0 = 1; // const/4 v0, 0x1
/* .line 79 */
/* :sswitch_2 */
int v0 = 2; // const/4 v0, 0x2
/* .line 80 */
/* :sswitch_3 */
int v0 = 3; // const/4 v0, 0x3
/* .line 81 */
/* :sswitch_4 */
int v0 = 4; // const/4 v0, 0x4
/* .line 82 */
/* :sswitch_5 */
int v0 = 5; // const/4 v0, 0x5
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x20 -> :sswitch_5 */
/* 0x28 -> :sswitch_4 */
/* 0x30 -> :sswitch_3 */
/* 0x38 -> :sswitch_2 */
/* 0x40 -> :sswitch_1 */
/* 0x60 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public java.nio.ByteBuffer toByteBuffer ( ) {
/* .locals 1 */
/* .line 160 */
/* const/16 v0, 0x10 */
java.nio.ByteBuffer .allocate ( v0 );
/* .line 161 */
/* .local v0, "buf":Ljava/nio/ByteBuffer; */
(( com.android.net.module.util.netlink.StructNdOptPref64 ) p0 ).writeToByteBuffer ( v0 ); // invoke-virtual {p0, v0}, Lcom/android/net/module/util/netlink/StructNdOptPref64;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V
/* .line 162 */
(( java.nio.ByteBuffer ) v0 ).flip ( ); // invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;
/* .line 163 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 169 */
v0 = this.prefix;
/* iget v1, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->lifetime:I */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
final String v1 = "NdOptPref64(%s, %d)"; // const-string v1, "NdOptPref64(%s, %d)"
java.lang.String .format ( v1,v0 );
} // .end method
protected void writeToByteBuffer ( java.nio.ByteBuffer p0 ) {
/* .locals 3 */
/* .param p1, "buf" # Ljava/nio/ByteBuffer; */
/* .line 153 */
/* invoke-super {p0, p1}, Lcom/android/net/module/util/netlink/NdOption;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V */
/* .line 154 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/StructNdOptPref64;->lifetime:I */
v1 = this.prefix;
v1 = (( android.net.IpPrefix ) v1 ).getPrefixLength ( ); // invoke-virtual {v1}, Landroid/net/IpPrefix;->getPrefixLength()I
v1 = com.android.net.module.util.netlink.StructNdOptPref64 .prefixLengthToPlc ( v1 );
v0 = com.android.net.module.util.netlink.StructNdOptPref64 .getScaledLifetimePlc ( v0,v1 );
(( java.nio.ByteBuffer ) p1 ).putShort ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;
/* .line 155 */
v0 = this.prefix;
(( android.net.IpPrefix ) v0 ).getRawAddress ( ); // invoke-virtual {v0}, Landroid/net/IpPrefix;->getRawAddress()[B
int v1 = 0; // const/4 v1, 0x0
/* const/16 v2, 0xc */
(( java.nio.ByteBuffer ) p1 ).put ( v0, v1, v2 ); // invoke-virtual {p1, v0, v1, v2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
/* .line 156 */
return;
} // .end method
