.class public Lcom/android/net/module/util/netlink/ConntrackMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "ConntrackMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;,
        Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;,
        Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;
    }
.end annotation


# static fields
.field public static final CTA_IP_V4_DST:S = 0x2s

.field public static final CTA_IP_V4_SRC:S = 0x1s

.field public static final CTA_PROTO_DST_PORT:S = 0x3s

.field public static final CTA_PROTO_NUM:S = 0x1s

.field public static final CTA_PROTO_SRC_PORT:S = 0x2s

.field public static final CTA_STATUS:S = 0x3s

.field public static final CTA_TIMEOUT:S = 0x7s

.field public static final CTA_TUPLE_IP:S = 0x1s

.field public static final CTA_TUPLE_ORIG:S = 0x1s

.field public static final CTA_TUPLE_PROTO:S = 0x2s

.field public static final CTA_TUPLE_REPLY:S = 0x2s

.field public static final DYING_MASK:I = 0x21e

.field public static final ESTABLISHED_MASK:I = 0x1e

.field public static final IPS_ASSURED:I = 0x4

.field public static final IPS_CONFIRMED:I = 0x8

.field public static final IPS_DST_NAT:I = 0x20

.field public static final IPS_DST_NAT_DONE:I = 0x100

.field public static final IPS_DYING:I = 0x200

.field public static final IPS_EXPECTED:I = 0x1

.field public static final IPS_FIXED_TIMEOUT:I = 0x400

.field public static final IPS_HELPER:I = 0x2000

.field public static final IPS_HW_OFFLOAD:I = 0x8000

.field public static final IPS_OFFLOAD:I = 0x4000

.field public static final IPS_SEEN_REPLY:I = 0x2

.field public static final IPS_SEQ_ADJUST:I = 0x40

.field public static final IPS_SRC_NAT:I = 0x10

.field public static final IPS_SRC_NAT_DONE:I = 0x80

.field public static final IPS_TEMPLATE:I = 0x800

.field public static final IPS_UNTRACKED:I = 0x1000

.field public static final STRUCT_SIZE:I = 0x14


# instance fields
.field public final nfGenMsg:Lcom/android/net/module/util/netlink/StructNfGenMsg;

.field public final status:I

.field public final timeoutSec:I

.field public final tupleOrig:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

.field public final tupleReply:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 442
    new-instance v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;-><init>()V

    invoke-direct {p0, v0}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 443
    new-instance v0, Lcom/android/net/module/util/netlink/StructNfGenMsg;

    sget v1, Landroid/system/OsConstants;->AF_INET:I

    int-to-byte v1, v1

    invoke-direct {v0, v1}, Lcom/android/net/module/util/netlink/StructNfGenMsg;-><init>(B)V

    iput-object v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->nfGenMsg:Lcom/android/net/module/util/netlink/StructNfGenMsg;

    .line 447
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->tupleOrig:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    .line 448
    iput-object v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->tupleReply:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    .line 449
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I

    .line 450
    iput v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->timeoutSec:I

    .line 451
    return-void
.end method

.method private constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Lcom/android/net/module/util/netlink/StructNfGenMsg;Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;II)V
    .locals 0
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p2, "nfGenMsg"    # Lcom/android/net/module/util/netlink/StructNfGenMsg;
    .param p3, "tupleOrig"    # Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    .param p4, "tupleReply"    # Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    .param p5, "status"    # I
    .param p6, "timeoutSec"    # I

    .line 455
    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 456
    iput-object p2, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->nfGenMsg:Lcom/android/net/module/util/netlink/StructNfGenMsg;

    .line 457
    iput-object p3, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->tupleOrig:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    .line 458
    iput-object p4, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->tupleReply:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    .line 459
    iput p5, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I

    .line 460
    iput p6, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->timeoutSec:I

    .line 461
    return-void
.end method

.method private static castToInet4Address(Ljava/net/InetAddress;)Ljava/net/Inet4Address;
    .locals 1
    .param p0, "address"    # Ljava/net/InetAddress;

    .line 342
    if-eqz p0, :cond_1

    instance-of v0, p0, Ljava/net/Inet4Address;

    if-nez v0, :cond_0

    goto :goto_0

    .line 343
    :cond_0
    move-object v0, p0

    check-cast v0, Ljava/net/Inet4Address;

    return-object v0

    .line 342
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static newIPv4TimeoutUpdateRequest(ILjava/net/Inet4Address;ILjava/net/Inet4Address;II)[B
    .locals 16
    .param p0, "proto"    # I
    .param p1, "src"    # Ljava/net/Inet4Address;
    .param p2, "sport"    # I
    .param p3, "dst"    # Ljava/net/Inet4Address;
    .param p4, "dport"    # I
    .param p5, "timeoutSec"    # I

    .line 193
    new-instance v0, Lcom/android/net/module/util/netlink/StructNlAttr;

    new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr;

    new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v3, 0x1

    move-object/from16 v4, p1

    invoke-direct {v2, v3, v4}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V

    new-instance v5, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v6, 0x2

    move-object/from16 v7, p3

    invoke-direct {v5, v6, v7}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SLjava/net/InetAddress;)V

    filled-new-array {v2, v5}, [Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[Lcom/android/net/module/util/netlink/StructNlAttr;)V

    new-instance v2, Lcom/android/net/module/util/netlink/StructNlAttr;

    new-instance v5, Lcom/android/net/module/util/netlink/StructNlAttr;

    move/from16 v8, p0

    int-to-byte v9, v8

    invoke-direct {v5, v3, v9}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SB)V

    new-instance v9, Lcom/android/net/module/util/netlink/StructNlAttr;

    move/from16 v10, p2

    int-to-short v11, v10

    sget-object v12, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-direct {v9, v6, v11, v12}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V

    new-instance v11, Lcom/android/net/module/util/netlink/StructNlAttr;

    move/from16 v12, p4

    int-to-short v13, v12

    sget-object v14, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    const/4 v15, 0x3

    invoke-direct {v11, v15, v13, v14}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SSLjava/nio/ByteOrder;)V

    filled-new-array {v5, v9, v11}, [Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v5

    invoke-direct {v2, v6, v5}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[Lcom/android/net/module/util/netlink/StructNlAttr;)V

    filled-new-array {v1, v2}, [Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(S[Lcom/android/net/module/util/netlink/StructNlAttr;)V

    .line 202
    .local v0, "ctaTupleOrig":Lcom/android/net/module/util/netlink/StructNlAttr;
    new-instance v1, Lcom/android/net/module/util/netlink/StructNlAttr;

    const/4 v2, 0x7

    sget-object v5, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    move/from16 v6, p5

    invoke-direct {v1, v2, v6, v5}, Lcom/android/net/module/util/netlink/StructNlAttr;-><init>(SILjava/nio/ByteOrder;)V

    .line 204
    .local v1, "ctaTimeout":Lcom/android/net/module/util/netlink/StructNlAttr;
    invoke-virtual {v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v2

    invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlAttr;->getAlignedLength()I

    move-result v5

    add-int/2addr v2, v5

    .line 205
    .local v2, "payloadLength":I
    add-int/lit8 v5, v2, 0x14

    new-array v5, v5, [B

    .line 206
    .local v5, "bytes":[B
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 207
    .local v9, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 209
    new-instance v11, Lcom/android/net/module/util/netlink/ConntrackMessage;

    invoke-direct {v11}, Lcom/android/net/module/util/netlink/ConntrackMessage;-><init>()V

    .line 210
    .local v11, "ctmsg":Lcom/android/net/module/util/netlink/ConntrackMessage;
    iget-object v13, v11, Lcom/android/net/module/util/netlink/ConntrackMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    array-length v14, v5

    iput v14, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    .line 211
    iget-object v13, v11, Lcom/android/net/module/util/netlink/ConntrackMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    const/16 v14, 0x100

    iput-short v14, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    .line 213
    iget-object v13, v11, Lcom/android/net/module/util/netlink/ConntrackMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    const/16 v14, 0x105

    iput-short v14, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_flags:S

    .line 214
    iget-object v13, v11, Lcom/android/net/module/util/netlink/ConntrackMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    iput v3, v13, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_seq:I

    .line 215
    invoke-virtual {v11, v9}, Lcom/android/net/module/util/netlink/ConntrackMessage;->pack(Ljava/nio/ByteBuffer;)V

    .line 217
    invoke-virtual {v0, v9}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 218
    invoke-virtual {v1, v9}, Lcom/android/net/module/util/netlink/StructNlAttr;->pack(Ljava/nio/ByteBuffer;)V

    .line 220
    return-object v5
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage;
    .locals 18
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 237
    move-object/from16 v0, p1

    invoke-static/range {p1 .. p1}, Lcom/android/net/module/util/netlink/StructNfGenMsg;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNfGenMsg;

    move-result-object v8

    .line 238
    .local v8, "nfGenMsg":Lcom/android/net/module/util/netlink/StructNfGenMsg;
    const/4 v1, 0x0

    if-nez v8, :cond_0

    .line 239
    return-object v1

    .line 242
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v9

    .line 243
    .local v9, "baseOffset":I
    const/4 v2, 0x3

    invoke-static {v2, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 244
    .local v2, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    const/4 v3, 0x0

    .line 245
    .local v3, "status":I
    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 246
    invoke-virtual {v2, v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe32(I)I

    move-result v3

    move v10, v3

    goto :goto_0

    .line 245
    :cond_1
    move v10, v3

    .line 249
    .end local v3    # "status":I
    .local v10, "status":I
    :goto_0
    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 250
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 251
    const/4 v3, 0x0

    .line 252
    .local v3, "timeoutSec":I
    if-eqz v2, :cond_2

    .line 253
    invoke-virtual {v2, v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe32(I)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 252
    :cond_2
    move v11, v3

    .line 256
    .end local v3    # "timeoutSec":I
    .local v11, "timeoutSec":I
    :goto_1
    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 257
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->makeNestedType(S)S

    move-result v3

    invoke-static {v3, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v2

    .line 258
    const/4 v3, 0x0

    .line 259
    .local v3, "tupleOrig":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    if-eqz v2, :cond_3

    .line 260
    invoke-virtual {v2}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-static {v4}, Lcom/android/net/module/util/netlink/ConntrackMessage;->parseTuple(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    move-result-object v3

    move-object v12, v3

    goto :goto_2

    .line 259
    :cond_3
    move-object v12, v3

    .line 263
    .end local v3    # "tupleOrig":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    .local v12, "tupleOrig":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    :goto_2
    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 264
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/android/net/module/util/netlink/StructNlAttr;->makeNestedType(S)S

    move-result v3

    invoke-static {v3, v0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v13

    .line 265
    .end local v2    # "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    .local v13, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    const/4 v2, 0x0

    .line 266
    .local v2, "tupleReply":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    if-eqz v13, :cond_4

    .line 267
    invoke-virtual {v13}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {v3}, Lcom/android/net/module/util/netlink/ConntrackMessage;->parseTuple(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    move-result-object v2

    move-object v14, v2

    goto :goto_3

    .line 266
    :cond_4
    move-object v14, v2

    .line 271
    .end local v2    # "tupleReply":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    .local v14, "tupleReply":Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    :goto_3
    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 272
    const/16 v15, 0x14

    .line 273
    .local v15, "kMinConsumed":I
    move-object/from16 v7, p0

    iget v2, v7, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_len:I

    add-int/lit8 v2, v2, -0x14

    invoke-static {v2}, Lcom/android/net/module/util/netlink/NetlinkConstants;->alignedLengthOf(I)I

    move-result v6

    .line 275
    .local v6, "kAdditionalSpace":I
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-ge v2, v6, :cond_5

    .line 276
    return-object v1

    .line 278
    :cond_5
    add-int v1, v9, v6

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 280
    new-instance v16, Lcom/android/net/module/util/netlink/ConntrackMessage;

    move-object/from16 v1, v16

    move-object/from16 v2, p0

    move-object v3, v8

    move-object v4, v12

    move-object v5, v14

    move/from16 v17, v6

    .end local v6    # "kAdditionalSpace":I
    .local v17, "kAdditionalSpace":I
    move v6, v10

    move v7, v11

    invoke-direct/range {v1 .. v7}, Lcom/android/net/module/util/netlink/ConntrackMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Lcom/android/net/module/util/netlink/StructNfGenMsg;Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;II)V

    return-object v16
.end method

.method private static parseTuple(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;
    .locals 6
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 318
    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 320
    :cond_0
    const/4 v1, 0x0

    .line 321
    .local v1, "tupleIpv4":Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;
    const/4 v2, 0x0

    .line 323
    .local v2, "tupleProto":Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 324
    .local v3, "baseOffset":I
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->makeNestedType(S)S

    move-result v4

    invoke-static {v4, p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v4

    .line 325
    .local v4, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-eqz v4, :cond_1

    .line 326
    invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-static {v5}, Lcom/android/net/module/util/netlink/ConntrackMessage;->parseTupleIpv4(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;

    move-result-object v1

    .line 328
    :cond_1
    if-nez v1, :cond_2

    return-object v0

    .line 330
    :cond_2
    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 331
    const/4 v5, 0x2

    invoke-static {v5}, Lcom/android/net/module/util/netlink/StructNlAttr;->makeNestedType(S)S

    move-result v5

    invoke-static {v5, p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v4

    .line 332
    if-eqz v4, :cond_3

    .line 333
    invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-static {v5}, Lcom/android/net/module/util/netlink/ConntrackMessage;->parseTupleProto(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;

    move-result-object v2

    .line 335
    :cond_3
    if-nez v2, :cond_4

    return-object v0

    .line 337
    :cond_4
    new-instance v0, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    invoke-direct {v0, v1, v2}, Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;-><init>(Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;)V

    return-object v0
.end method

.method private static parseTupleIpv4(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;
    .locals 6
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 348
    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 350
    :cond_0
    const/4 v1, 0x0

    .line 351
    .local v1, "src":Ljava/net/Inet4Address;
    const/4 v2, 0x0

    .line 353
    .local v2, "dst":Ljava/net/Inet4Address;
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 354
    .local v3, "baseOffset":I
    const/4 v4, 0x1

    invoke-static {v4, p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v4

    .line 355
    .local v4, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-eqz v4, :cond_1

    .line 356
    invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    invoke-static {v5}, Lcom/android/net/module/util/netlink/ConntrackMessage;->castToInet4Address(Ljava/net/InetAddress;)Ljava/net/Inet4Address;

    move-result-object v1

    .line 358
    :cond_1
    if-nez v1, :cond_2

    return-object v0

    .line 360
    :cond_2
    invoke-virtual {p0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 361
    const/4 v5, 0x2

    invoke-static {v5, p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v4

    .line 362
    if-eqz v4, :cond_3

    .line 363
    invoke-virtual {v4}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    invoke-static {v5}, Lcom/android/net/module/util/netlink/ConntrackMessage;->castToInet4Address(Ljava/net/InetAddress;)Ljava/net/Inet4Address;

    move-result-object v2

    .line 365
    :cond_3
    if-nez v2, :cond_4

    return-object v0

    .line 367
    :cond_4
    new-instance v0, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;

    invoke-direct {v0, v1, v2}, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleIpv4;-><init>(Ljava/net/Inet4Address;Ljava/net/Inet4Address;)V

    return-object v0
.end method

.method private static parseTupleProto(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;
    .locals 8
    .param p0, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 372
    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 374
    :cond_0
    const/4 v1, 0x0

    .line 375
    .local v1, "protoNum":B
    const/4 v2, 0x0

    .line 376
    .local v2, "srcPort":S
    const/4 v3, 0x0

    .line 378
    .local v3, "dstPort":S
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    .line 379
    .local v4, "baseOffset":I
    const/4 v5, 0x1

    invoke-static {v5, p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v5

    .line 380
    .local v5, "nlAttr":Lcom/android/net/module/util/netlink/StructNlAttr;
    const/4 v6, 0x0

    if-eqz v5, :cond_1

    .line 381
    invoke-virtual {v5, v6}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsByte(B)B

    move-result v1

    .line 383
    :cond_1
    sget v7, Landroid/system/OsConstants;->IPPROTO_TCP:I

    if-eq v1, v7, :cond_2

    sget v7, Landroid/system/OsConstants;->IPPROTO_UDP:I

    if-eq v1, v7, :cond_2

    return-object v0

    .line 385
    :cond_2
    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 386
    const/4 v7, 0x2

    invoke-static {v7, p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v5

    .line 387
    if-eqz v5, :cond_3

    .line 388
    invoke-virtual {v5, v6}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe16(S)S

    move-result v2

    .line 390
    :cond_3
    if-nez v2, :cond_4

    return-object v0

    .line 392
    :cond_4
    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 393
    const/4 v7, 0x3

    invoke-static {v7, p0}, Lcom/android/net/module/util/netlink/StructNlAttr;->findNextAttrOfType(SLjava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v5

    .line 394
    if-eqz v5, :cond_5

    .line 395
    invoke-virtual {v5, v6}, Lcom/android/net/module/util/netlink/StructNlAttr;->getValueAsBe16(S)S

    move-result v3

    .line 397
    :cond_5
    if-nez v3, :cond_6

    return-object v0

    .line 399
    :cond_6
    new-instance v0, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/net/module/util/netlink/ConntrackMessage$TupleProto;-><init>(BSS)V

    return-object v0
.end method

.method public static stringForIpConntrackStatus(I)Ljava/lang/String;
    .locals 3
    .param p0, "flags"    # I

    .line 479
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 481
    .local v0, "sb":Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    .line 482
    const-string v1, "IPS_EXPECTED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    :cond_0
    and-int/lit8 v1, p0, 0x2

    const-string/jumbo v2, "|"

    if-eqz v1, :cond_2

    .line 485
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    :cond_1
    const-string v1, "IPS_SEEN_REPLY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    :cond_2
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_4

    .line 489
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    :cond_3
    const-string v1, "IPS_ASSURED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    :cond_4
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_6

    .line 493
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_5

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    :cond_5
    const-string v1, "IPS_CONFIRMED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    :cond_6
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_8

    .line 497
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    :cond_7
    const-string v1, "IPS_SRC_NAT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    :cond_8
    and-int/lit8 v1, p0, 0x20

    if-eqz v1, :cond_a

    .line 501
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_9

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    :cond_9
    const-string v1, "IPS_DST_NAT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    :cond_a
    and-int/lit8 v1, p0, 0x40

    if-eqz v1, :cond_c

    .line 505
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    :cond_b
    const-string v1, "IPS_SEQ_ADJUST"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    :cond_c
    and-int/lit16 v1, p0, 0x80

    if-eqz v1, :cond_e

    .line 509
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    :cond_d
    const-string v1, "IPS_SRC_NAT_DONE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    :cond_e
    and-int/lit16 v1, p0, 0x100

    if-eqz v1, :cond_10

    .line 513
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    :cond_f
    const-string v1, "IPS_DST_NAT_DONE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    :cond_10
    and-int/lit16 v1, p0, 0x200

    if-eqz v1, :cond_12

    .line 517
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_11

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    :cond_11
    const-string v1, "IPS_DYING"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    :cond_12
    and-int/lit16 v1, p0, 0x400

    if-eqz v1, :cond_14

    .line 521
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_13

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    :cond_13
    const-string v1, "IPS_FIXED_TIMEOUT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    :cond_14
    and-int/lit16 v1, p0, 0x800

    if-eqz v1, :cond_16

    .line 525
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_15

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    :cond_15
    const-string v1, "IPS_TEMPLATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    :cond_16
    and-int/lit16 v1, p0, 0x1000

    if-eqz v1, :cond_18

    .line 529
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    :cond_17
    const-string v1, "IPS_UNTRACKED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 532
    :cond_18
    and-int/lit16 v1, p0, 0x2000

    if-eqz v1, :cond_1a

    .line 533
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 534
    :cond_19
    const-string v1, "IPS_HELPER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    :cond_1a
    and-int/lit16 v1, p0, 0x4000

    if-eqz v1, :cond_1c

    .line 537
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    :cond_1b
    const-string v1, "IPS_OFFLOAD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    :cond_1c
    const v1, 0x8000

    and-int/2addr v1, p0

    if-eqz v1, :cond_1e

    .line 541
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 542
    :cond_1d
    const-string v1, "IPS_HW_OFFLOAD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 544
    :cond_1e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getMessageType()S
    .locals 1

    .line 472
    invoke-virtual {p0}, Lcom/android/net/module/util/netlink/ConntrackMessage;->getHeader()Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    move-result-object v0

    iget-short v0, v0, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->nlmsg_type:S

    and-int/lit16 v0, v0, -0x101

    int-to-short v0, v0

    return v0
.end method

.method public pack(Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .line 467
    iget-object v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V

    .line 468
    iget-object v0, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->nfGenMsg:Lcom/android/net/module/util/netlink/StructNfGenMsg;

    invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNfGenMsg;->pack(Ljava/nio/ByteBuffer;)V

    .line 469
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConntrackMessage{nlmsghdr{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 551
    iget-object v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    if-nez v1, :cond_0

    const-string v1, ""

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->mHeader:Lcom/android/net/module/util/netlink/StructNlMsgHdr;

    sget v2, Landroid/system/OsConstants;->NETLINK_NETFILTER:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, nfgenmsg{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->nfGenMsg:Lcom/android/net/module/util/netlink/StructNfGenMsg;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, tuple_orig{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->tupleOrig:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, tuple_reply{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->tupleReply:Lcom/android/net/module/util/netlink/ConntrackMessage$Tuple;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}, status{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->status:I

    .line 556
    invoke-static {v1}, Lcom/android/net/module/util/netlink/ConntrackMessage;->stringForIpConntrackStatus(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")}, timeout_sec{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/net/module/util/netlink/ConntrackMessage;->timeoutSec:I

    .line 557
    invoke-static {v1}, Ljava/lang/Integer;->toUnsignedLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 549
    return-object v0
.end method
