public class com.android.net.module.util.netlink.StructRtMsg extends com.android.net.module.util.Struct {
	 /* .source "StructRtMsg.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public final Object dstLen;
	 /* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
	 /* order = 0x1 */
	 /* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object family;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x0 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Long flags;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x8 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object protocol;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x5 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object scope;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x6 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object srcLen;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x2 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object table;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x4 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object tos;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x3 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object type;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x7 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
/* # direct methods */
 com.android.net.module.util.netlink.StructRtMsg ( ) {
/* .locals 0 */
/* .param p1, "family" # S */
/* .param p2, "dstLen" # S */
/* .param p3, "srcLen" # S */
/* .param p4, "tos" # S */
/* .param p5, "table" # S */
/* .param p6, "protocol" # S */
/* .param p7, "scope" # S */
/* .param p8, "type" # S */
/* .param p9, "flags" # J */
/* .line 61 */
/* invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V */
/* .line 62 */
/* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->family:S */
/* .line 63 */
/* iput-short p2, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->dstLen:S */
/* .line 64 */
/* iput-short p3, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->srcLen:S */
/* .line 65 */
/* iput-short p4, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->tos:S */
/* .line 66 */
/* iput-short p5, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->table:S */
/* .line 67 */
/* iput-short p6, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->protocol:S */
/* .line 68 */
/* iput-short p7, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->scope:S */
/* .line 69 */
/* iput-short p8, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->type:S */
/* .line 70 */
/* iput-wide p9, p0, Lcom/android/net/module/util/netlink/StructRtMsg;->flags:J */
/* .line 71 */
return;
} // .end method
public static com.android.net.module.util.netlink.StructRtMsg parse ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 82 */
v0 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v1, 0xc */
/* if-ge v0, v1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 85 */
} // :cond_0
/* const-class v0, Lcom/android/net/module/util/netlink/StructRtMsg; */
com.android.net.module.util.Struct .parse ( v0,p0 );
/* check-cast v0, Lcom/android/net/module/util/netlink/StructRtMsg; */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 0 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 93 */
(( com.android.net.module.util.netlink.StructRtMsg ) p0 ).writeToByteBuffer ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructRtMsg;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V
/* .line 94 */
return;
} // .end method
