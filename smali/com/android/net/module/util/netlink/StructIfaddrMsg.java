public class com.android.net.module.util.netlink.StructIfaddrMsg extends com.android.net.module.util.Struct {
	 /* .source "StructIfaddrMsg.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public final Object family;
	 /* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
	 /* order = 0x0 */
	 /* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object flags;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x2 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Integer index;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x4 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->S32:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object prefixLen;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x1 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
public final Object scope;
/* .annotation runtime Lcom/android/net/module/util/Struct$Field; */
/* order = 0x3 */
/* type = .enum Lcom/android/net/module/util/Struct$Type;->U8:Lcom/android/net/module/util/Struct$Type; */
} // .end annotation
} // .end field
/* # direct methods */
public com.android.net.module.util.netlink.StructIfaddrMsg ( ) {
/* .locals 0 */
/* .param p1, "family" # S */
/* .param p2, "prefixLen" # S */
/* .param p3, "flags" # S */
/* .param p4, "scope" # S */
/* .param p5, "index" # I */
/* .line 54 */
/* invoke-direct {p0}, Lcom/android/net/module/util/Struct;-><init>()V */
/* .line 55 */
/* iput-short p1, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->family:S */
/* .line 56 */
/* iput-short p2, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->prefixLen:S */
/* .line 57 */
/* iput-short p3, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->flags:S */
/* .line 58 */
/* iput-short p4, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->scope:S */
/* .line 59 */
/* iput p5, p0, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->index:I */
/* .line 60 */
return;
} // .end method
public static com.android.net.module.util.netlink.StructIfaddrMsg parse ( java.nio.ByteBuffer p0 ) {
/* .locals 2 */
/* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 71 */
v0 = (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
/* const/16 v1, 0x8 */
/* if-ge v0, v1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 74 */
} // :cond_0
/* const-class v0, Lcom/android/net/module/util/netlink/StructIfaddrMsg; */
com.android.net.module.util.Struct .parse ( v0,p0 );
/* check-cast v0, Lcom/android/net/module/util/netlink/StructIfaddrMsg; */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 0 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 82 */
(( com.android.net.module.util.netlink.StructIfaddrMsg ) p0 ).writeToByteBuffer ( p1 ); // invoke-virtual {p0, p1}, Lcom/android/net/module/util/netlink/StructIfaddrMsg;->writeToByteBuffer(Ljava/nio/ByteBuffer;)V
/* .line 83 */
return;
} // .end method
