.class public Lcom/android/net/module/util/netlink/NduseroptMessage;
.super Lcom/android/net/module/util/netlink/NetlinkMessage;
.source "NduseroptMessage.java"


# static fields
.field static final NDUSEROPT_SRCADDR:I = 0x1

.field public static final STRUCT_SIZE:I = 0x10


# instance fields
.field public final family:B

.field public final icmp_code:B

.field public final icmp_type:B

.field public final ifindex:I

.field public final option:Lcom/android/net/module/util/netlink/NdOption;

.field public final opts_len:I

.field public final srcaddr:Ljava/net/InetAddress;


# direct methods
.method constructor <init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)V
    .locals 7
    .param p1, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p2, "buf"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .line 68
    const-string v0, "ND option extends past end of buffer"

    invoke-direct {p0, p1}, Lcom/android/net/module/util/netlink/NetlinkMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;)V

    .line 71
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 72
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 73
    .local v1, "start":I
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    iput-byte v2, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->family:B

    .line 74
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    .line 75
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    invoke-static {v3}, Ljava/lang/Short;->toUnsignedInt(S)I

    move-result v3

    iput v3, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->opts_len:I

    .line 76
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->ifindex:I

    .line 77
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    iput-byte v5, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_type:B

    .line 78
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    iput-byte v5, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_code:B

    .line 79
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/lit8 v5, v5, 0x6

    invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 89
    sget-object v5, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 91
    :try_start_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 92
    .local v5, "slice":Ljava/nio/ByteBuffer;
    invoke-virtual {v5, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 93
    invoke-static {v5}, Lcom/android/net/module/util/netlink/NdOption;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NdOption;

    move-result-object v6

    iput-object v6, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->option:Lcom/android/net/module/util/netlink/NdOption;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    .end local v5    # "slice":Ljava/nio/ByteBuffer;
    add-int/lit8 v5, v1, 0x10

    add-int/2addr v5, v3

    .line 98
    .local v5, "newPosition":I
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    if-ge v5, v3, :cond_2

    .line 101
    invoke-virtual {p2, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 102
    .end local v5    # "newPosition":I
    nop

    .line 105
    invoke-static {p2}, Lcom/android/net/module/util/netlink/StructNlAttr;->parse(Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/StructNlAttr;

    move-result-object v0

    .line 106
    .local v0, "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
    if-eqz v0, :cond_1

    iget-short v3, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_type:S

    const/4 v5, 0x1

    if-ne v3, v5, :cond_1

    iget-object v3, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    if-eqz v3, :cond_1

    .line 109
    sget v3, Landroid/system/OsConstants;->AF_INET6:I

    if-ne v2, v3, :cond_0

    .line 111
    const/4 v2, 0x0

    iget-object v3, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    invoke-static {v2, v3, v4}, Ljava/net/Inet6Address;->getByAddress(Ljava/lang/String;[BI)Ljava/net/Inet6Address;

    move-result-object v2

    iput-object v2, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->srcaddr:Ljava/net/InetAddress;

    goto :goto_0

    .line 113
    :cond_0
    iget-object v2, v0, Lcom/android/net/module/util/netlink/StructNlAttr;->nla_value:[B

    invoke-static {v2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v2

    iput-object v2, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->srcaddr:Ljava/net/InetAddress;

    .line 115
    :goto_0
    return-void

    .line 107
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid source address in ND useropt"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 99
    .end local v0    # "nla":Lcom/android/net/module/util/netlink/StructNlAttr;
    .restart local v5    # "newPosition":I
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 97
    .end local v5    # "newPosition":I
    :catchall_0
    move-exception v2

    add-int/lit8 v3, v1, 0x10

    iget v4, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->opts_len:I

    add-int/2addr v3, v4

    .line 98
    .local v3, "newPosition":I
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    if-lt v3, v4, :cond_3

    .line 99
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 101
    :cond_3
    invoke-virtual {p2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 102
    .end local v3    # "newPosition":I
    throw v2
.end method

.method public static parse(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)Lcom/android/net/module/util/netlink/NduseroptMessage;
    .locals 3
    .param p0, "header"    # Lcom/android/net/module/util/netlink/StructNlMsgHdr;
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .line 128
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    goto :goto_0

    .line 129
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v1

    .line 131
    .local v1, "oldOrder":Ljava/nio/ByteOrder;
    :try_start_0
    new-instance v2, Lcom/android/net/module/util/netlink/NduseroptMessage;

    invoke-direct {v2, p0, p1}, Lcom/android/net/module/util/netlink/NduseroptMessage;-><init>(Lcom/android/net/module/util/netlink/StructNlMsgHdr;Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 131
    return-object v2

    .line 138
    :catchall_0
    move-exception v0

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 139
    throw v0

    .line 132
    :catch_0
    move-exception v2

    .line 136
    .local v2, "e":Ljava/lang/Exception;
    nop

    .line 138
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 136
    return-object v0

    .line 128
    .end local v1    # "oldOrder":Ljava/nio/ByteOrder;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_0
    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 7

    .line 144
    iget-byte v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->family:B

    .line 145
    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    iget v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->opts_len:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->ifindex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-byte v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_type:B

    invoke-static {v0}, Ljava/lang/Byte;->toUnsignedInt(B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-byte v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->icmp_code:B

    .line 146
    invoke-static {v0}, Ljava/lang/Byte;->toUnsignedInt(B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v0, p0, Lcom/android/net/module/util/netlink/NduseroptMessage;->srcaddr:Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    filled-new-array/range {v1 .. v6}, [Ljava/lang/Object;

    move-result-object v0

    .line 144
    const-string v1, "Nduseroptmsg(%d, %d, %d, %d, %d, %s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
