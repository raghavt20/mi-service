public class com.android.net.module.util.netlink.StructNlMsgErr {
	 /* .source "StructNlMsgErr.java" */
	 /* # static fields */
	 public static final Integer STRUCT_SIZE;
	 /* # instance fields */
	 public Integer error;
	 public com.android.net.module.util.netlink.StructNlMsgHdr msg;
	 /* # direct methods */
	 public com.android.net.module.util.netlink.StructNlMsgErr ( ) {
		 /* .locals 0 */
		 /* .line 28 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Boolean hasAvailableSpace ( java.nio.ByteBuffer p0 ) {
		 /* .locals 2 */
		 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
		 /* .line 32 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 v0 = 			 (( java.nio.ByteBuffer ) p0 ).remaining ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I
			 /* const/16 v1, 0x14 */
			 /* if-lt v0, v1, :cond_0 */
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :goto_0
} // .end method
public static com.android.net.module.util.netlink.StructNlMsgErr parse ( java.nio.ByteBuffer p0 ) {
	 /* .locals 2 */
	 /* .param p0, "byteBuffer" # Ljava/nio/ByteBuffer; */
	 /* .line 43 */
	 v0 = 	 com.android.net.module.util.netlink.StructNlMsgErr .hasAvailableSpace ( p0 );
	 /* if-nez v0, :cond_0 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 48 */
} // :cond_0
/* new-instance v0, Lcom/android/net/module/util/netlink/StructNlMsgErr; */
/* invoke-direct {v0}, Lcom/android/net/module/util/netlink/StructNlMsgErr;-><init>()V */
/* .line 49 */
/* .local v0, "struct":Lcom/android/net/module/util/netlink/StructNlMsgErr; */
v1 = (( java.nio.ByteBuffer ) p0 ).getInt ( ); // invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
/* iput v1, v0, Lcom/android/net/module/util/netlink/StructNlMsgErr;->error:I */
/* .line 50 */
com.android.net.module.util.netlink.StructNlMsgHdr .parse ( p0 );
this.msg = v1;
/* .line 51 */
} // .end method
/* # virtual methods */
public void pack ( java.nio.ByteBuffer p0 ) {
/* .locals 1 */
/* .param p1, "byteBuffer" # Ljava/nio/ByteBuffer; */
/* .line 64 */
/* iget v0, p0, Lcom/android/net/module/util/netlink/StructNlMsgErr;->error:I */
(( java.nio.ByteBuffer ) p1 ).putInt ( v0 ); // invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
/* .line 65 */
v0 = this.msg;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 66 */
	 (( com.android.net.module.util.netlink.StructNlMsgHdr ) v0 ).pack ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->pack(Ljava/nio/ByteBuffer;)V
	 /* .line 68 */
} // :cond_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 72 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StructNlMsgErr{ error{"; // const-string v1, "StructNlMsgErr{ error{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/android/net/module/util/netlink/StructNlMsgErr;->error:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}, msg{" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 74 */
v1 = this.msg;
/* if-nez v1, :cond_0 */
final String v1 = ""; // const-string v1, ""
} // :cond_0
(( com.android.net.module.util.netlink.StructNlMsgHdr ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/android/net/module/util/netlink/StructNlMsgHdr;->toString()Ljava/lang/String;
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "} }" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 72 */
} // .end method
